﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System.Xml;
using System.Web;

namespace Garments_ERP.Service.Service
{
   public class EnquiryService:IEnquiryService
    {
       cl_Enquiry obj = new cl_Enquiry();

        public long AddUpdate(List<string> Style_Img, List<string> RawItemImg, CRM_EnquiryMasterEntity Entity, List<CRM_EnquiryRawitemDetailEntity> RawItem)
        {
            try
            {
                return obj.AddUpdate(Style_Img, RawItemImg, Entity, RawItem);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public CRM_EnquiryMasterEntity getbyid(long id)
        {
            try
            {
                return obj.getbyid(id);
            }
            catch (Exception)
            {

                throw;
            }

        }

        public List<CRM_EnquiryStyleDetailsEntity> GetEnqryStyleData(long EnquiryId)
        {
            try
            {
                return obj.GetEnqryStyleData(EnquiryId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CRM_EnquiryRawitemDetailEntity> GetEnqryStyleRawItemData(long EnquiryId,long styleid)
        {
            try
            {
                return obj.GetEnqryStyleRawItemData(EnquiryId, styleid);
            }
            catch (Exception)
            {

                throw;
            }
        }






        public object StatusCount(int Type,string attribute,XmlDocument doc)
        {
            try
            {
                return obj.StatusCount(Type, attribute,doc);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_EnquiryTypeMasterEntity> getEnqiryType()
        {
            try
            {
                return obj.getEnqiryType();
            }
            catch (Exception)
            {

                throw;
            }
        }


      public List<CRM_EnquiryMasterEntity> get()
       {
           try
           {
               return obj.get();
           }
           catch (Exception)
           {

               throw;
           }
       }

        public List<CRM_EnquiryMasterEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId,int Userid,int roleid)
        {
            try
            {
                return obj.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public object SearchList(XmlDocument doc,int type, string id,long screenId)
        {
            try
            {
                return obj.SearchList(doc, type, id, screenId);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<CRM_EnquiryMasterEntity> get(DateTime from, DateTime to)
        {
            try
            {
                return obj.get(from, to);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<CRM_EnquiryMasterEntity> get(DateTime from, DateTime to,int IsStatus,int Company_ID,int BranchId,int Userid,int roleid)
      {
          try
          {
              return obj.get(from, to, IsStatus, Company_ID, BranchId, Userid, roleid);
          }
          catch (Exception)
          {

              throw;
          }
      }
      public List<CRM_EnquiryMasterEntity> getEnquirynobycustomerid(long customerid,int Company_ID,int BranchId)
      {
          try
          {
              return obj.getEnquirynobycustomerid(customerid, Company_ID, BranchId);
          }
          catch (Exception)
          {

              throw;
          }
      }
      public  long Insert(CRM_EnquiryMasterEntity entity)
      {
          try
          {
              return obj.Insert(entity);
          }
          catch (Exception)
          {

              throw;
          }
      }
       
       public  bool Delete(long id,int Userid)
       {
           try
           {
               return obj.Delete(id, Userid);
           }
           catch (Exception)
           {

               throw;
           }
       }

        public bool Rejectedenquiry(long id,int Uid)
        {
            try
            {
                return obj.Rejectedenquiry(id, Uid);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(CRM_EnquiryMasterEntity entity)
       {
           try
           {
               return obj.Update(entity);
           }
           catch (Exception)
           {

               throw;
           }
       }

       public  string getnextenqno()
      {
          try
          {
              return obj.getnextenqno();
          }
          catch (Exception)
          {

              throw;
          }
      }
    }
}
