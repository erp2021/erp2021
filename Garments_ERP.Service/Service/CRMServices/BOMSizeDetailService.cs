﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class BOMSizeDetailService
    {
        cl_BOMSizeDetail obj = new cl_BOMSizeDetail();
        public List<M_BOMSizeDetailMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public M_BOMSizeDetailMasterEntity GetById(long id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public decimal Insert(M_BOMSizeDetailMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(M_BOMSizeDetailMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(long id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
