﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Garments_ERP.Service.Service
{
    public class BillOfMaterialService
    {
        cl_BillOfMaterial obj = new cl_BillOfMaterial();

        public object InsertAutoPR(XmlDocument doc, int type, string AttibId)
        {
            try
            {
                return obj.InsertAutoPR(doc, type, AttibId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_BillOfMaterialMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<M_BillOfMaterialMasterEntity> getbyBOM(int companyid, int branchid)
        {
            try
            {
                return obj.getbyBOM(companyid, branchid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Base_BillOfMaterialMasterEntity getBase(int Company_ID, int BranchId)
        {
            try
            {
                return obj.getBase(Company_ID, BranchId);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public Base_BillOfMaterialMasterEntity get(DateTime from, DateTime to, int Company_ID, int BranchId)
        {
            try
            {
                return obj.get(from, to, Company_ID, BranchId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public long Insert(M_BillOfMaterialMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public M_BillOfMaterialMasterEntity GetByEnquiry(long id)
        {
            try
            {
                return obj.GetByEnquiry(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public M_BillOfMaterialMasterEntity GetById(long id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public M_BillOfMaterialMasterEntity GetDetailsForPRByBOM(long bomid)
        {
            try
            {
                return obj.GetDetailsForPRByBOM(bomid);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(M_BillOfMaterialMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool UpdatePRDone(M_BillOfMaterialMasterEntity entity)
        {
            try
            {
                return obj.UpdatePRDone(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(long id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public string getnextbomno()
        {
            try
            {
                return obj.getnextbomno();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CRM_EnquiryMasterEntity> getenquiryList(int Company_ID,int BranchId, long custid)
        {
            try
            {
                return obj.getenquiryList(Company_ID, BranchId, custid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CRM_CustomerPOMasterentity> POList(int Company_ID, int BranchId, long custid)
        {
            try
            {
                return obj.POList(Company_ID, BranchId, custid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CRM_CustomerPOMasterentity> EnqPOList(int Company_ID, int BranchId, long EnqId)
        {
            try
            {
                return obj.EnqPOList(Company_ID, BranchId, EnqId);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
