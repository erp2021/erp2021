﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;

namespace Garments_ERP.Service.Service
{
  public  class EnquiryRawItemDetailService:IEnquiryRawItemDetailService
    {
      cl_EnquiryRawItemDetail obj = new cl_EnquiryRawItemDetail();
        public long Insert(CRM_EnquiryRawitemDetailEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }

      public bool Update(CRM_EnquiryRawitemDetailEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
      public bool Delete(long id)
      {
          try
          {
              return obj.Delete(id);
          }
          catch (Exception)
          {

              throw;
          }
      }
      public List<CRM_EnquiryRawitemDetailEntity> getbyenquiryid(long id)
        {
            try
            {
                return obj.getbyenquiryid(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
