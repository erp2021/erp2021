﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class BillOfMaterialDetailService
    {
        cl_BillOfMaterialDetail obj = new cl_BillOfMaterialDetail();
        public List<M_BillOfMaterialDetailEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public long Insert(M_BillOfMaterialDetailEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<M_BillOfMaterialDetailEntity> GetById(long id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_BOMSizeDetailMasterEntity> GetBOMStyle(long bomid)
        {
            try
            {
                return obj.GetBOMStyle(bomid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_BillOfMaterialMasterEntity> GetBOMList(long styleid)
        {
            try
            {
                return obj.GetBOMList(styleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public M_StyleMasterEntity GetStyleDetails(long itemid)
        {
            try
            {
                return obj.GetStyleDetails(itemid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Update(M_BillOfMaterialDetailEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(long id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
