﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System.Xml;

namespace Garments_ERP.Service.Service
{
  public  class QuotationStyleDetailService:IQuotationStyleDetailService
    {

      cl_QuotationStyleDetail obj = new cl_QuotationStyleDetail();

        public object EditAttributeValue(string MyJson, int type, XmlDocument doc)
        {
            try
            {
                return obj.EditAttributeValue(MyJson, type, doc);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public CRM_QuotationStyleDetailEntity getbyquotationid(long id)
      {
          try
          {
              return obj.getbyquotationid(id);
          }
          catch (Exception)
          {

              throw;
          }
      }
      public bool Update(CRM_QuotationStyleDetailEntity entity)
      {
          try
          {
              return obj.Update(entity);
          }
          catch (Exception)
          {

              throw;
          }
      }
        public long Insert(CRM_QuotationStyleDetailEntity entity)
        {

            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public long InsertStyleImage(CRM_QuotationStyleImageEntity entity)
        {

            try
            {
                return obj.InsertStyleImage(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public long updateStyleImage(CRM_QuotationStyleImageEntity entity)
        {

            try
            {
                return obj.updateStyleImage(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        


    }
}
