﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class SupplierQuotationService
    {
        cl_SupplierQuotation obj = new cl_SupplierQuotation();

        public List<SRM_QuotationMasterEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<SRM_QuotationMasterEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.get(from, to, IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool RejectedQuo(long id, int Uid)
        {
            try
            {
                return obj.RejectedQuo(id, Uid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string getnextQuotatioNo()
        {
            try
            {
                return obj.getnextQuotatioNo();
            }
            catch (Exception)
            {

                throw;
            }

        }
        public bool Update(SRM_QuotationMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public SRM_QuotationMasterEntity getbyid(long id)
        {
            try
            {
                return obj.getbyid(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<SRM_QuotationMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<SRM_QuotationMasterEntity> get(DateTime from,DateTime to)
        {
            try
            {
                return obj.get(from, to);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public bool Delete(long id,int Userid)
        {
            try
            {
                return obj.Delete(id, Userid);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(SRM_QuotationMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<SRM_QuotationMasterEntity> getquotationnobysupplierid(long supplierid)
        {
            try
            {
                return obj.getquotationnobysupplierid(supplierid);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public object QuoteGet(long id)
        {
            try
            {
                return obj.QuoteGet(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
