﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class SupplierPRService
    {
        cl_SupplierPurchaseRequest obj = new cl_SupplierPurchaseRequest();
        public string getnextprno()
        {
            try
            {
               return obj.getnextprno();
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public bool Update(SRM_PurchaseRequestMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public SRM_PurchaseRequestMasterEntity getbyid(long id)
        {
            try
            {
                return obj.getbyid(id);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public List<SRM_PurchaseRequestMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public List<SRM_PurchaseRequestMasterEntity> get(DateTime from,DateTime to)
        {
            try
            {
                return obj.get(from, to);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(long id,int Userid)
        {
            try
            {
                return obj.Delete(id, Userid);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public bool RejectedPR(long id, int Uid)
        {
            try
            {
                return obj.RejectedPR(id, Uid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<EmployeeEntity> GetAccountHead(int deptid, int Company_ID, int BranchId)
        {
            try
            {
                return obj.GetAccountHead(deptid, Company_ID, BranchId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<SRM_PurchaseRequestMasterEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<SRM_PurchaseRequestMasterEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.get(from, to, IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public long Insert(SRM_PurchaseRequestMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public List<SRM_PurchaseRequestMasterEntity> getList(int companyid, int branchid)
        {
            try
            {
                return obj.getList(companyid, branchid);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
