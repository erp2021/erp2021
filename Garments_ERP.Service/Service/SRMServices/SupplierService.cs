﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using Garments_ERP.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Garments_ERP.Service.Service
{
    public class SupplierService : ISupplierService
    {
        cl_Supplier obj = new cl_Supplier();
        private M_SupplierMasterEntity db = new M_SupplierMasterEntity();
        public List<M_SupplierMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public M_SupplierMasterEntity GetById(long id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(M_SupplierMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public bool Update(M_SupplierMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(long id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }


        //code for AutoExtender
        public M_SupplierMasterEntity GetSuppliername(long Suppliername)
        {
            try
            {
                return obj.GetSuppliername(Suppliername);
            }
            catch (Exception)
            {

                throw;
            }
        }


    }
}
