﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using Garments_ERP.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class SRM_GRNInwardService : ISRM_GRNInwardService
    {
        cl_GRNInward obj = new cl_GRNInward();

        public List<SRM_GRNInwardEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<SRM_GRNInwardEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.get(from, to, IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_Dynamic_ControlsEntity> GetControlList()
        {
            try
            {
                return obj.GetControlList();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public SRM_GRNInwardEntity getbyid(long id)
        {
            try
            {
                return obj.getbyid(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public SRM_GRNItemEntity getbyitemid(long id)
        {
            try
            {
                return obj.getbyitemid(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<SRM_GRNInwardEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<SRM_GRNInwardEntity> get(DateTime from,DateTime to)
        {
            try
            {
                return obj.get(from,to);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(SRM_GRNInwardEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public string getnextpono()
        {
            try
            {
                return obj.getnextpono();
            }
            catch (Exception)
            {

                throw;
            }
        }


        public bool Update(SRM_GRNInwardEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
       public bool UpdateGRNItem(SRM_GRNItemEntity entity)
        {
            try
            {
                return obj.UpdateGRNItem(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
       public bool DeleteGRNItem(long id)
       {
           try
           {
               return obj.DeleteGRNItem(id);
           }
           catch (Exception)
           {

               throw;
           }
       }

       public bool Delete(long id,int Userid)
       {
           try
           {
               return obj.Delete(id, Userid);
           }
           catch (Exception)
           {

               throw;
           }

       }

        public string getnextgrnno()
        {
            try
            {
                return obj.getnextgrnno();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string getnextchnno()
        {
            try
            {
                return obj.getnextchnno();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string getnextinvno()
        {
            try
            {
                return obj.getnextinvno();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetbysupplierAdd(long id)
        {
            try
            {
                return obj.GetbysupplierAdd(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public object GRNReportGet(long id)
        {
            try
            {
                return obj.GRNReportGet(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<SRM_GRNInwardEntity> getItemBal(long poid, long itemid,int IAttr)
        {
            try
            {
                return obj.getItemBal(poid,itemid, IAttr);
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
