﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
using Garments_ERP.Data.Admin;
namespace Garments_ERP.Service.Service
{
    public class SupplierBankService
    {
        cl_SupplierBank obj = new cl_SupplierBank();
        public List<M_SupplierBankDetailsEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public M_SupplierBankDetailsEntity GetById(long id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(M_SupplierBankDetailsEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(M_SupplierBankDetailsEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}
