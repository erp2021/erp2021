﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Service
{
    public class SupplierContactService
    {
        cl_SupplierContact obj = new cl_SupplierContact();
        public List<M_SupplierContactDetailsEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public List<M_SupplierContactDetailsEntity> GetById(long id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(M_SupplierContactDetailsEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public bool Update(M_SupplierContactDetailsEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
