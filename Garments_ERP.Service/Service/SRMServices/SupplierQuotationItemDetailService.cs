﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class SupplierQuotationItemDetailService
    {
        cl_SupplierQuotationItemDetail obj = new cl_SupplierQuotationItemDetail();
        public long Insert(SRM_QuotationItemDetailEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<SRM_QuotationItemDetailEntity> getbyquotationid(long id)
        {
            try
            {
                return obj.getbyquotationid(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(SRM_QuotationItemDetailEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool deleterawitem(long id)
        {
            try
            {
                return obj.deleterawitem(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
