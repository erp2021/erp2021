﻿using Garments_ERP.Data.Classes.Accounts;
using Garments_ERP.Entity;
using Garments_ERP.Entity.Accounts;
using System;
using System.Collections.Generic;

namespace Garments_ERP.Service.Service.Accounts
{
    public class CounterSalesService
    {
        cl_CounterSales obj = new cl_CounterSales();
        public List<Acc_Voucher_Customer_Details_Entity> Get_NameContact(int companyid, int branchid, string inputStr)
        {
            try
            {
                return obj.Get_NameContact(companyid, branchid, inputStr);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<M_ItemMasterEntity> Get_ItemsWithHSN(int companyid, int branchid, string inputStr)
        {
            try
            {
                return obj.Get_ItemsWithHSN(companyid, branchid, inputStr);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public string getnextcountersalesno()
        {
            try
            {
                return obj.getnextcountersalesno();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public Int32 SaveCounterSales(Acc_Voucher_Entity v_, List<Acc_Voucher_Items_Detail_Entity> vi_, Acc_Voucher_Customer_Details_Entity vc_)
        {
            try
            {
                return obj.SaveCounterSales(v_, vi_, vc_);
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public List<Acc_Voucher_Entity> Get_AllCountersales(int branch_id)
        {
            try
            {
                return obj.Get_AllCountersales(branch_id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public Sales_Entity Get_Countersales_ById(int voucher_id, int branch_id)
        {
            try
            {
                 return obj.Get_Countersales_ById(voucher_id, branch_id);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }

}
