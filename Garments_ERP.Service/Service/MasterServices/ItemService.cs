﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System.Xml;
using Garments_ERP.Data.Data;

namespace Garments_ERP.Service.Service
{
    public class ItemService : IItemService
    {
        cl_Item obj = new cl_Item();

        public List<M_ProductInfoEntity> GetRackData(long ItemId, int ItemAttr)
        {
            try
            {
                return obj.GetRackData(ItemId, ItemAttr);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_InventoryListEntity> GetInventory(string MyJson, int type, XmlDocument doc)
        {
            try
            {
                return obj.GetInventory(MyJson, type, doc);
            }
            catch (Exception)
            {

                throw;
            }

        }


        public List<M_ItemMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }

        }


        public List<M_ItemMasterEntity> getFinishedItem()
        {
            try
            {
                return obj.getFinishedItem();
            }
            catch (Exception)
            {

                throw;
            }

        }

        public long Insert(M_ItemMasterEntity entity,string AttributeValList)
        {
            try
            {
                return obj.Insert(entity, AttributeValList);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Update(M_ItemMasterEntity entity,string AttributeValList)
        {
            try
            {
                return obj.Update(entity, AttributeValList);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public M_ItemMasterEntity GetById(long id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public M_ItemMasterEntity GetOutstandingById(long id)
        {
            try
            {
                return obj.GetOutstandingById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Delete(long id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public long InsertImage(M_ItemMasterImageEntity entity)
        {
            try
            {
                return obj.InsertImage(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public long InsertImageData(M_ImageMasterEntity entity)
        {
            try
            {
                return obj.InsertImageData(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public M_ItemMasterImageEntity GetImageById(long id)
        {
            try
            {
                return obj.GetImageById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
       public bool UpdateImage(M_ItemMasterImageEntity entity)
        {
            try
            {
                return obj.UpdateImage(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool DeleteImage(long id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<M_ItemMasterEntity> GetByCategoryId(long id)
        {
            try
            {
                return obj.GetByCategoryId(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public object GetById(object id)
        {
            throw new NotImplementedException();
        }

        public long InsertItemAttrib(XmlDocument doc)
        {
            try
            {
                return obj.InsertItemAttrib(doc);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
