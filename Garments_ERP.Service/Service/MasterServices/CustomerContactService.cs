﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class CustomerContactService
    {
        cl_CustomerContact obj = new cl_CustomerContact();
       
        public long Insert(M_CustomerContactDetailsEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<M_CustomerContactDetailsEntity> GetById(long id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public bool Update(M_CustomerContactDetailsEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
