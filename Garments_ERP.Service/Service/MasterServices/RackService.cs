﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
   public class RackService
    {
       cl_Rack obj = new cl_Rack();
        public long SaveRackAllocation(string RackStr, long WarehouseId,long InwardId,int inventoryMode , int Userid,int Company_ID, int BranchId)
        {
            try
            {
                return obj.SaveRackAllocation(RackStr, WarehouseId, InwardId, inventoryMode, Userid, Company_ID, BranchId);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<M_RackMasterEntity> get()
       {
           try
           {
               return obj.get();
           }
           catch (Exception)
           {
               
               throw;
           }
       }
       public M_RackMasterEntity GetById(int id)
       {
           try
           {
               return obj.GetById(id);
           }
           catch (Exception)
           {
               
               throw;
           }
       }
       public int Insert(M_RackMasterEntity entity)
       {
           try
           {
               return obj.Insert(entity);
           }
           catch (Exception)
           {
               
               throw;
           }
       }
       public bool Update(M_RackMasterEntity entity)
       {
           try
           {
               return obj.Update(entity);
           }
           catch (Exception)
           {
               
               throw;
           }
       }
       public bool Delete(int id)
       {
           try
           {
               return obj.Delete(id);
           }
           catch (Exception)
           {
               
               throw;
           }
       }

    }
}
