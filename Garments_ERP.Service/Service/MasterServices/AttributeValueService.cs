﻿using Garments_ERP.Data.Classes.MasterClasses;
using Garments_ERP.Entity.MasterEntities;
using Garments_ERP.Service.Interface.MasterInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service.MasterServices
{
    public class AttributeValueService: IAttributeValueService
    {
        cl_AttributeValue obj = new cl_AttributeValue();
        #region Methods
        public List<M_Attribute_Value_MasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }

        }
        public int Insert(M_Attribute_Value_MasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(M_Attribute_Value_MasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public M_Attribute_Value_MasterEntity GetById(int id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public object getAttributeValue(string MyJson, int type)
        {
            try
            {
                return obj.getAttributeValue(MyJson, type);
            }
            catch (Exception)
            {

                throw;
            }
        }
        
        #endregion
    }
}
