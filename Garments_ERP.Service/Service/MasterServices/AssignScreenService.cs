﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class AssignScreenService
    {
        cl_AssignScreen obj=new cl_AssignScreen();
        public List<M_ScreeRoleEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_ScreenEntity> GetScreenData(long RoleId)
        {
            try
            {
                return obj.GetScreenData(RoleId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Insert(string MainScr, string SubScr,long roleid)
        {
            try
            {
                return obj.Insert(MainScr, SubScr, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool ActiveDeactive(int id, string Status)
        {
            try
            {
                return obj.ActiveDeactive(id, Status);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
