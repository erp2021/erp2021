﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
   public class ProcessService
    {
       cl_Process obj = new cl_Process();
       public List<M_ProcessMasterEntity> get()
       {
           try
           {
               return obj.get();
           }
           catch (Exception)
           {
               
               throw;
           }
       }
       public M_ProcessMasterEntity GetById(long id)
       {
           try
           {
               return obj.GetById(id);
           }
           catch (Exception)
           {
               
               throw;
           }
       }
       public decimal Insert(M_ProcessMasterEntity entity)
       {
           try
           {
               return obj.Insert(entity);
           }
           catch (Exception)
           {
               
               throw;
           }
       }
       public bool Update(M_ProcessMasterEntity entity)
       {
           try
           {
               return obj.Update(entity);
           }
           catch (Exception)
           {
               
               throw;
           }
       }
       public bool Delete(long id)
       {
           try
           {
               return obj.Delete(id);
           }
           catch (Exception)
           {

               throw;
           }
       }
    }
}
