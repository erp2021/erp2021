﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Service
{
  public  class SegmentTypeService:ISegmentTypeService
  {
      cl_SegmentType obj = new cl_SegmentType();
      public  List<M_SegmentTypeMasterEntity> get()
      {
          try
          {
              return obj.get();
          }
          catch (Exception)
          {

              throw;
          }
      }
      public int Insert(M_SegmentTypeMasterEntity entity)
      {
          try
          {
              return obj.Insert(entity);
          }
          catch(Exception)
          {
              throw;
          }
      }
      public bool Update(M_SegmentTypeMasterEntity entity)
      {
          try
          {
              return obj.Update(entity);
          }
          catch (Exception)
          {
              throw;
          }
      }
      public bool Delete(int id)
      {
          try
          {
              return obj.Delete(id);
          }
          catch (Exception)
          {
              throw;
          }
      }
      public bool CheckSegmentTypeExistance(string Segmenttype, int? Id,int  companyid,int branchid)
      {
          try
          {
              return obj.CheckSegmentTypeExistance(Segmenttype, Id, companyid, branchid);
          }
          catch(Exception)
          {
              throw;
          }
      }
    }
}
