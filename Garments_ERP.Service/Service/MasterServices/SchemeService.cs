﻿using Garments_ERP.Data.Classes.MasterClasses;
using Garments_ERP.Entity.MasterEntities;
using Garments_ERP.Service.Interface.MasterInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service.MasterServices
{
    public class SchemeService : ISchemeService
    {
        cl_Scheme obj = new cl_Scheme();
        public List<M_SchemeMaster_Entity> get(int comid, int branchid)
        {
            try
            {
                return obj.get(comid, branchid);
            }
            catch (Exception)
            {

                throw;
            }
        }
     
        public M_SchemeMaster_Entity getById(long id)
        {
            try
            {
                return obj.getById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public long InsertUpdate(M_SchemeMaster_Entity entity)
        {
            try
            {
                return obj.InsertUpdate(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
       
        public bool IsExists(int compid, int branchid, long itemid, string st_date, string ed_date, long scheme_id)
        {
            try
            {
                return obj.IsExists(compid,branchid,itemid,st_date,ed_date,scheme_id);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}

