﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;

namespace Garments_ERP.Service.Service
{
    public class LedgerAccountServices: ILedgerAccountService
    {
        cl_LedgerAccount obj = new cl_LedgerAccount();
        
        public List<M_Ledger_BillingDetailsEntity> getAddInfo(int id)
        {
            try
            {
                return obj.getAddInfo(id);
            }
            catch (Exception)
            {
               throw;
            }
        }


        public List<M_LedgersEntity> getCustList(int CBS)
        {
            try
            {
                return obj.getCustList(CBS);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<M_LedgersEntity> getSupplierList(int CB)
        {
            try
            {
                return obj.getSupplierList(CB);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_LedgersEntity> getCustomerByWO(long woID)
        {
            try
            {
                return obj.getCustomerByWO(woID);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<M_LedgersEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_ContactAddressEntity> getAddresssbycustomerid(long custid)
        {
            try
            {
                return obj.getAddresssbycustomerid(custid);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public long Insert(M_LedgersEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public M_LedgersEntity GetById(long id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(M_LedgersEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(long id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Updateadddress(M_ContactAddressEntity entity)
        {
            try
            {
                return obj.Updateadddress(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool deleteaddress(long id)
        {
            try
            {
                return obj.deleteaddress(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_Ledger_BillingDetailsEntity> GetAddressById(long custid)
        {
            try
            {
                return obj.GetAddressById(custid);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
