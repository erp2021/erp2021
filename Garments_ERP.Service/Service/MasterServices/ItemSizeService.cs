﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Service
{
    public class ItemSizeService : IItemSizeService
    {
        cl_ItemSize obj = new cl_ItemSize();
        public List<M_ItemSizeMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public M_ItemSizeMasterEntity GetById(long id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public int Insert(M_ItemSizeMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Update(M_ItemSizeMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }



        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }


        
        //public bool CheckExistance(string Itemsize, int? Id, int itemsizeid)
        //{
        //    try
        //    {
        //        return obj.CheckExistance(Itemsize, Id, itemsizeid);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}
        public int InsertSize(M_ItemSizeMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
