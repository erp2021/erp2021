﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Service
{
    public class ItemshadeService
    {
        cl_ItemShade obj = new cl_ItemShade();

        public List<M_ItemShadeMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public int Insert(M_ItemShadeMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool Update(M_ItemShadeMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool CheckItemShadeExistance(string Itemshade, int? Id)
        {
            try
            {
                return obj.CheckItemShadeExistance(Itemshade, Id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public List<M_ItemShapeMasterEntity> getShape()
        {
            try
            {
                return obj.getShape();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public int InsertShape(M_ItemShapeMasterEntity entity)
        {
            try
            {
                return obj.InsertShape(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool UpdateShape(M_ItemShapeMasterEntity entity)
        {
            try
            {
                return obj.UpdateShape(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool DeleteShape(int id)
        {
            try
            {
                return obj.DeleteShape(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool CheckItemShapeExistance(string Itemshape, int? Id)
        {
            try
            {
                return obj.CheckItemShapeExistance(Itemshape, Id);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
