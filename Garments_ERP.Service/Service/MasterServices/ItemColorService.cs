﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Service
{
  public  class ItemColorService
    {
      cl_ItemColor obj = new cl_ItemColor();

      public List<M_ItemColorEntity> get()
      {
          try
          {
              return obj.get();
          }
          catch (Exception)
          {

              throw;
          }
      }
      public int Insert(M_ItemColorEntity entity)
      {
          try
          {
              return obj.Insert(entity);
          }
          catch (Exception ex)
          {
              throw;
          }
      }

      public bool Update(M_ItemColorEntity entity)
      {
          try
          {
              return obj.Update(entity);
          }
          catch (Exception ex)
          {
              throw;
          }
      }
      public bool Delete(int id)
      {
          try
          {
              return obj.Delete(id);
          }
          catch (Exception ex)
          {
              throw;
          }
      }
      public bool CheckItemColorExistance(string ItemColor, int? Id)
      {
          try
          {
              return obj.CheckItemColorExistance(ItemColor, Id);
          }
          catch (Exception ex)
          {
              throw;
          }
      }
    }
}
