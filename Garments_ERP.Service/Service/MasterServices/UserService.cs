﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using Garments_ERP.Service.Interface;
namespace Garments_ERP.Service.Service
{
  public  class UserService:IUserService
    {
      cl_User obj = new cl_User();
      public M_UserEntity Authenticateuser(M_UserEntity entity)
      {
          try
          {
              return obj.Authenticateuser(entity);
          }
          catch (Exception)
          {

              throw;
          }
      }
      public int SaveUser(M_UserEntity entity)
      {
            try
            {
                return obj.SaveUser(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }


      public  List<M_UserEntity> get()
       {
           try
           {
               return obj.get();
           }
           catch (Exception)
           {

               throw;
           }
       }

      public List<M_UserTypeEntity> getUsertype()
      {
          try
          {
              return obj.getUsertype();
          }
          catch (Exception)
          {

              throw;
          }
      }
      public bool DeleteUser(int id)
      {
          try
          {
              return obj.DeleteUser(id);
          }
          catch (Exception)
          {

              throw;
          }
      }
      public int Insert(M_UserEntity entity)
      {
          try
          {
              return obj.Insert(entity);
          }
          catch (Exception)
          {

              throw;
          }
          
      }

     public M_UserEntity IsEmailValid(string email)
      {
          try
          {
              return obj.IsEmailValid(email);
          }
          catch (Exception)
          {

              throw;
          }
          
      }


     public bool ValidateResetPasswordLink(M_UserEntity model)
     {
         try
         {
             return obj.ValidateResetPasswordLink(model);
         }
         catch (Exception)
         {

             throw;
         }
     }
     public int ChangePassword(M_UserEntity model)
     {
         try
         {
             return obj.ChangePassword(model);
         }
         catch (Exception)
         {

             throw;
         }
     }

     public List<M_ModuleEntity> GetModules()
     {
         try
         {
             return obj.GetModules();
         }
         catch (Exception)
         {

             throw;
         }
     }
     public List<M_UserPermissionsEntity> getbyUserid(int userid)
     {
         try
         {
             return obj.getbyUserid(userid);
         }
         catch (Exception)
         {

             throw;
         }
     }
    public int Insert(List<M_UserPermissionsEntity> list)
     {
         try
         {
             return obj.Insert(list);
         }
         catch (Exception)
         {

             throw;
         }
     }
    public int Update(List<M_UserPermissionsEntity> list)
    {
        try
        {
            return obj.Update(list);
        }
        catch (Exception)
        {

            throw;
        }
    }

     public M_UserEntity GetByid(int id)
    {
        try
        {
            return obj.GetByid(id);
        }
        catch (Exception)
        {

            throw;
        }
    }
  public bool UpdateUser(M_UserEntity entity)
     {
         try
         {
             return obj.UpdateUser(entity);
         }
         catch (Exception)
         {

             throw;
         }
     }
  }
}
