﻿using Garments_ERP.Data.Classes.MasterClasses;
using Garments_ERP.Entity;
using Garments_ERP.Service.Interface.MasterInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service.MasterServices
{
    public class BrokerService:IBrokerService
    {
        cl_Broker obj = new cl_Broker();
        public List<M_BrokerMasterEntity> GetAll()
        {
            try
            {
                return obj.GetAll();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public M_BrokerMasterEntity GetById(long id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(M_BrokerMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(M_BrokerMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(long id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
