﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;

namespace Garments_ERP.Service.Service
{
    public class ItemGroupService
    {
        cl_Itemgroup obj = new cl_Itemgroup();
        public List<M_ItemGroupEntity> Get()
        {
            try
            {
                return obj.Get();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public int Insert(M_ItemGroupEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Update(M_ItemGroupEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool CheckExistance(string Itemgroup, int? Id)
        {
            try
            {
                return obj.CheckExistance(Itemgroup, Id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
