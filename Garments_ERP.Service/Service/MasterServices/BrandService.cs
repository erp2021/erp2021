﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;

namespace Garments_ERP.Service.Service
{
    public class BrandService
    {
        cl_Brand obj = new cl_Brand();

        public List<M_BrandMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(M_BrandMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Update(M_BrandMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool CheckBrandNameExistance(string BrandName, int? Id)
        {
            try
            {
                return obj.CheckBrandNameExistance(BrandName, Id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
