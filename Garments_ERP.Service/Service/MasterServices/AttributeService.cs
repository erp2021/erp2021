﻿using Garments_ERP.Data.Classes.MasterClasses;
using Garments_ERP.Entity.MasterEntities;
using Garments_ERP.Service.Interface.MasterInterfaces;
using System;
using System.Collections.Generic;

namespace Garments_ERP.Service.Service.MasterServices
{
    public class AttributeService : IAttributeService
    {
        cl_Attribute obj = new cl_Attribute();
        #region Methods
        public List<M_Attribute_MasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }

        }
        public int Insert(M_Attribute_MasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(M_Attribute_MasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public M_Attribute_MasterEntity GetById(int id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
    }
}
