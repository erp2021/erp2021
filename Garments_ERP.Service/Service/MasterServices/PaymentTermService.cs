﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
   public class PaymentTermService
    {
        cl_PaymentTerm obj = new cl_PaymentTerm();
        public List<M_PaymentTermMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public int Insert(M_PaymentTermMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool Update(M_PaymentTermMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool CheckPaymentTermExistance(string paymentterm, int? Id)
        {
            try
            {
                return obj.CheckPaymentTermExistance(paymentterm, Id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
