﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System.Xml;
using Garments_ERP.Service.Interface.MasterInterfaces;
using Garments_ERP.Data.Classes.MasterClasses;

namespace Garments_ERP.Service.Service.MasterServices
{
    public class CompanyBranchService: ICompanyBranch
    {
        Cl_CompanyBranch obj = new Cl_CompanyBranch();
        public List<M_BranchEntity> getComdetail()
        {
            try
            {
                return obj.getComdetail();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(M_BranchEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public M_BranchEntity GetById(long id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(M_BranchEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public bool Delete(long id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetPanNo(int CompanyId)
        {
            try
            {
                return obj.GetPanNo(CompanyId);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
