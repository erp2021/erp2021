﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;

namespace Garments_ERP.Service.Service
{
    public class TaxService
    {
        cl_Tax obj = new cl_Tax();

        public List<M_TaxMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public M_TaxMasterEntity getbyid(long id)
        {
            try
            {
                return obj.getbyid(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public int Insert(M_TaxMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Update(M_TaxMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool CheckTaxNameExistance(string TaxName, int? Id)
        {
            try
            {
                return obj.CheckTaxNameExistance(TaxName, Id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
