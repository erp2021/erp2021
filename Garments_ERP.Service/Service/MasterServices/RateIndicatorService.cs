﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;

namespace Garments_ERP.Service.Service
{
    public class RateIndicatorService
    {
        cl_RateIndicator obj = new cl_RateIndicator();

        public List<M_RateIndicatorMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(M_RateIndicatorMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(M_RateIndicatorMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool CheckRateIndicatorNameExistance(string RateIndicatorName, int? Id)
        {
            try
            {
                return obj.CheckRateIndicatorNameExistance(RateIndicatorName, Id);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
