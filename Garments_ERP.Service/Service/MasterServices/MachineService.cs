﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Service
{
  public  class MachineService:IMachineService
    {
      cl_Machine obj = new cl_Machine();
      public long Insert(MachineEntity entity)
      {
          try
          {
              return obj.Insert(entity);
          }
          catch (Exception)
          {

              throw;
          }
      }
      public  bool Update(MachineEntity entity)
      {
          try
          {
              return obj.Update(entity);
          }
          catch (Exception)
          {

              throw;
          }

      }
       public bool Delete(long id)
      {
          try
          {
              return obj.Delete(id);
          }
          catch (Exception)
          {

              throw;
          }
      }
       public MachineEntity GetById(long id)
       {
           try
           {
               return obj.GetById(id);
           }
           catch (Exception)
           {

               throw;
           }
       }
       public List<MachineEntity> Get()
       {
           try
           {
               return obj.Get();
           }
           catch (Exception)
           {

               throw;
           }
       }


    }
}
