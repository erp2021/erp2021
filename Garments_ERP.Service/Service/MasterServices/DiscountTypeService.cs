﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;

namespace Garments_ERP.Service.Service
{
    public class DiscountTypeService
    {
        cl_DiscountType obj = new cl_DiscountType();

        public List<M_DiscountTypeMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(M_DiscountTypeMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(M_DiscountTypeMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool CheckDiscountTypeNameExistance(string DiscountType, int? Id)
        {
            try
            {
                return obj.CheckDiscountTypeNameExistance(DiscountType, Id);
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
