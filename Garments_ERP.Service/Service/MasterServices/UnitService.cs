﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using Garments_ERP.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class UnitService : IUnitService
    {
        cl_Unit obj = new cl_Unit();
        public List<M_UnitMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public M_UnitMasterEntity GetById(int id)
        {
            try
            {
                return obj.GetById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public int Insert(M_UnitMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(M_UnitMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool CheckUnitExistance(string ItemUnit, int? Id)
        {
            try
            {
                return obj.CheckUnitExistance(ItemUnit, Id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
