﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using Garments_ERP.Service.Interface;
namespace Garments_ERP.Service.Service
{
   public class ProcessCycleService:IProcesscycleService
    {
       cl_Processcycle obj = new cl_Processcycle();
       public List<M_ProcessCycleEntity> get()
       {
           try
           {
               return obj.get();
           }
           catch(Exception ex)
           {
               throw;
           }
       }
        public List<ProcessCycleTransactionEntity> Getbycycleid(int id)
        {
            try
            {
                return obj.Getbycycleid(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public int Insert(M_ProcessCycleEntity entity)
       {
           try
           {
              return obj.Insert(entity);
           }
           catch(Exception ex)
               {
                   throw;
               }
       }
       public bool Update(M_ProcessCycleEntity entity)
       {
           try
           {
               return obj.Update(entity);
           }
           catch (Exception ex)
           {
               throw;
           }
         }
        public bool Delete(int id)
       {
           try
           {
               return obj.Delete(id);
           }
           catch (Exception ex)
           {
               throw;
           }
       }
       public M_ProcessCycleEntity getbyid(int id)
        {
            try
            {
                return obj.getbyid(id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }


    }
}
