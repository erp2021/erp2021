﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class WarehouseRoleService
    {
        cl_WarehouseRole obj = new cl_WarehouseRole();
        public List<M_RoleEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(M_WarehouseRoleMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public M_WarehouseRoleMasterEntity getbyid(long id)
        {
            try
            {
                return obj.getbyid(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(M_WarehouseRoleMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(long id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
