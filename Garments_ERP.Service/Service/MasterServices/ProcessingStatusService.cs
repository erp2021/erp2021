﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;

namespace Garments_ERP.Service.Service
{
    public class ProcessingStatusService
    {
        cl_ProcessingStatus obj = new cl_ProcessingStatus();

        public List<M_ProcessingStatusEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(M_ProcessingStatusEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Update(M_ProcessingStatusEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool CheckProcessingStatusExistance(string ProcessingStatus, int? Id)
        {
            try
            {
                return obj.CheckProcessingStatusExistance(ProcessingStatus, Id);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
