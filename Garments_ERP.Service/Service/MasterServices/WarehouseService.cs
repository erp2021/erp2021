﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using Garments_ERP.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class WarehouseService:IWarehouseService
    {
        cl_WarehouseMaster obj = new cl_WarehouseMaster();
        public List<M_WarehouseEntity> get(int companyid, int branchid)
        {
            try
            {
                return obj.get(companyid, branchid);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(M_WarehouseEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public M_WarehouseEntity getbyid(long id)
        {
            try
            {
                return obj.getbyid(id);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public bool Update(M_WarehouseEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public bool Delete(long id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}
