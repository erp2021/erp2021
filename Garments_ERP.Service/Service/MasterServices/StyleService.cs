﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;

namespace Garments_ERP.Service.Service
{
  public  class StyleService:IStyleService
    {
      cl_Style obj = new cl_Style();
      public List<M_StyleMasterEntity> get()
      {
          try
          {
              return obj.get();
          }
          catch (Exception)
          {

              throw;
          }
      }
      public long  Insert(M_StyleMasterEntity entity)
      {
          try
          {
              return obj.Insert(entity);
          }
          catch (Exception)
          {

              throw;
          }
      }
      public bool Update(M_StyleMasterEntity entity)
      {
          try
          {
              return obj.Update(entity);
          }
          catch (Exception)
          {

              throw;
          }
      }
      public bool Delete(int id)
      {
          try
          {
              return obj.Delete(id);
          }
          catch(Exception)
          {
              throw;
          }
      }

      public bool CheckStyleNameExistance(string Stylename, int? Id,int companyid,int branchid)
      {
          try
          {
              return obj.CheckStyleNameExistance(Stylename, Id, companyid, branchid);
          }
          catch(Exception)
          {
              throw;
          }
      }
    }
}
