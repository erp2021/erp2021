﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using Garments_ERP.Entity.ReportsSPEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class ReportsService
    {
        cl_Reports obj = new cl_Reports();
       public List<SP_WarehouseStockDetailsEntity> GetWarehouseStock()
        {
            try
            {
                return obj.GetWarehouseStock();
            }
            catch (Exception)
            {
                
                throw;
            }
        }
       public List<SP_WarehouseStockDetailsEntity> GetDateWiseWarehouseStock(long itemcategory)
        {
            try
            {
                return obj.GetDateWiseWarehouseStock(itemcategory);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<SP_GetItemInwardDetailsEntity> GetDateWiseInwardDetail(DateTime from, DateTime to)
        {
            try
            {
                return obj.GetDateWiseItemInwardDetail(from, to);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<SP_GetItemInwardDetailsEntity> GetItemInwardDetail(long itemname)
        {
            try
            {
                return obj.GetItemInwardDetail(itemname);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<SP_GetInventoryDetailsEntity> GetInventoryDetails()
        {
            try
            {
                return obj.GetInventoryDetails();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
