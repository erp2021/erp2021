﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;

namespace Garments_ERP.Service.Service.ProductionServices
{
    public class DailyProductionPlanService
    {
        cl_DailyProductionPlan planEntity = new cl_DailyProductionPlan();

        public List<MfgDailyProductionPlanEntity> Get()
        {
            try
            {
                return planEntity.Get();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public long Insert(MfgDailyProductionPlanEntity entity)
        {
            try
            {
                return planEntity.Insert(entity);
            }
            catch (Exception ec)
            {
                throw ec;
            }
        }

        public MfgDailyProductionPlanEntity Getbyid(long id)
        {
            try
            {
                return planEntity.Getbyid(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
