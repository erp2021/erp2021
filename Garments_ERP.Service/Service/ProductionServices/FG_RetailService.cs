﻿using Garments_ERP.Data.Classes.ProductionClasses;
using Garments_ERP.Entity;
using Garments_ERP.Entity.ProductionEntities;
using Garments_ERP.Service.Interface.ProductionInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service.ProductionServices
{
    public class FG_RetailService : IFG_RetailService
    {
        cl_FGTransferToRetail _obj = new cl_FGTransferToRetail();
        public List<MFG_WORK_ORDEREntity> Get_WorkOrders(int companyid, int branchid, string inputStr, long CustID)
        {
            try
            {
                return _obj.Get_WorkOrders(companyid, branchid, inputStr, CustID);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<FG_TransferMasterEntity> Get_FGTransfers(int companyid, int branchid, string inputStr, long woID)
        {
            try
            {
                return _obj.Get_FGTransfers(companyid, branchid, inputStr, woID);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<FG_TransferDetailEntity> Get_FGTransferDetails(int companyid, int branchid, long fg_transferid)
        {
            try
            {
                return _obj.Get_FGTransferDetails(companyid, branchid, fg_transferid);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public Int64 SaveFG_Retail(FG_Transfer_To_RetailEntity FG_TREntity)
        {
            try
            {
                return _obj.SaveFG_Retail(FG_TREntity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<FG_Transfer_To_RetailEntity> Get(int company_id, int branch_id)
        {
            try
            {
                return _obj.Get(company_id, branch_id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public FG_Transfer_To_RetailEntity getbyid(long id)
        {
            try
            {
                return _obj.getbyid(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Delete(long id)
        {
            try
            {
                return _obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<SRM_GRNInwardEntity> GetGRNList(long Bomid)
        {
            try
            {
                return _obj.GetGRNList(Bomid);
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
