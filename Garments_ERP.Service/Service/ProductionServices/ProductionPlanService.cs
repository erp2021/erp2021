﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;
namespace Garments_ERP.Service.Service
{
  public  class ProductionPlanService
    {
      cl_ProductionPlan obj = new cl_ProductionPlan();

        public List<MFG_PRODUCTION_PLANEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<MFG_PRODUCTION_PLANEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.get(from, to, IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public bool RejectedPP(long id, int Uid)
        {
            try
            {
                return obj.RejectedPP(id, Uid);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public bool Delete(long id, int Uid)
        {
            try
            {
                return obj.Delete(id, Uid);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public string getnextPNno()
        {
            try
            {
                return obj.getnextPNno();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<MFG_PRODUCTION_PLANEntity> getWODetials(int WOID)
        {
            try
            {
                return obj.getWODetials(WOID);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public M_BillOfMaterialDetailEntity GetPerDayQty(int WOID,int ItemId,int iAttr)
        {
            try
            {
                return obj.GetPerDayQty(WOID, ItemId, iAttr);
            }
            catch (Exception)
            {

                throw;
            }
        }


        

        public List<MFG_PRODUCTION_PLANEntity> Get()
      {
          try
          {
              return obj.Get();
          }
          catch (Exception)
          {

              throw;
          }
      }
       public long Insert(MFG_PRODUCTION_PLANEntity entity)
       {
           try
           {
               return obj.Insert(entity);
           }
           catch (Exception ec)
           {
               throw ec;
           }
       }
       public bool Update(MFG_PRODUCTION_PLANEntity entity)
       {
           try
           {
               return obj.Update(entity);
           }
           catch (Exception)
           {

               throw;
           }
       }
       public MFG_PRODUCTION_PLANEntity Getbyid(long id)
       {
           try
           {
               return obj.Getbyid(id);
           }
           catch (Exception)
           {

               throw;
           }
       }

        public MFG_WorkOrderDetailsForProductionPlan GetWorkOrderDetails(long workOrderID,long planID)
        {
            try
            {
                return obj.GetWorkOrderDetails(workOrderID, planID);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public MfgDailyProductionPlanEntity GetMonthlyPlanDetails( long monthlyPlanID)
        {
            try
            {
                return obj.GetMonthlyPlanDetails(monthlyPlanID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //GetMonthlyPlanDetails


        /*
        public bool UpdateProductionPlanDetail(MFG_PRODUCTION_PLAN_DETAILSEntity plandetailentity)
        {
            try
            {
                return obj.UpdateProductionPlanDetail(plandetailentity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool DeleteProductionPlanDetail(long id)
        {
            try
            {
                return obj.DeleteProductionPlanDetail(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(long id)
        {
            try
            {
                return obj.Delete(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        */
    }
}
