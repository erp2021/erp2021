﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
using Garments_ERP.Service.Interface;
using Garments_ERP.Data.Admin;

namespace Garments_ERP.Service.Service.ProductionServices
{
   public class DailyProductionPlanEntryServices
    {
        cl_DailyProductionPlanEntry planEntity = new cl_DailyProductionPlanEntry();

        public List<MfgDailyProductionPlanEntryEntity> Get()
        {
            try
            {
                return planEntity.Get();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public long Insert(MfgDailyProductionPlanEntryEntity entity)
        {
            try
            {
                return planEntity.Insert(entity);
            }
            catch (Exception ec)
            {
                throw ec;
            }
        }

        public MfgDailyProductionPlanEntryEntity Getbyid(long id)
        {
            try
            {
                return planEntity.Getbyid(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
