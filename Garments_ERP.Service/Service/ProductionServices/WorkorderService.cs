﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using Garments_ERP.Service.Interface;
using Garments_ERP.Entity.ProductionEntities;

namespace Garments_ERP.Service.Service
{
    public class WorkorderService : IWorkorderService
    {
        cl_Workorder obj = new cl_Workorder();
        public List<M_ProductInfoEntity> GetBatchData(long id, long ItemId, int ItemAttr)
        {
            try
            {
                return obj.GetBatchData(id, ItemId,ItemAttr);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public M_ItemMaster_AttributeMaserEntity GetItemData(long id)
        {
            try
            {
                return obj.GetItemData(id);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public bool RejectedWO(long id, int Uid)
        {
            try
            {
                return obj.RejectedWO(id, Uid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<MFG_WORK_ORDEREntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<MFG_WORK_ORDEREntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.get(from, to, IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }



        public List<MFG_WORK_ORDEREntity> Get()
        {
            try
            {
                return obj.Get();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<MFG_WORK_ORDEREntity> GetWOMI(int comid,int branchid)
        {
            try
            {
                return obj.GetWOMI(comid, branchid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<MFG_WORK_ORDEREntity> Get(DateTime from, DateTime to)
        {
            try
            {
                return obj.Get(from, to);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<MFG_WorkOrderProcessDetailsEntity> GetbyWOId(int id)
        {
            try
            {
                return obj.GetbyWOId(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<SP_GetWO_ProcessMaterialDetailsEntity> GetWOProcessDetails()
        {
            try
            {
                return obj.GetWOProcessDetails();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<SP_GetProc_DetailsForProc_InchargeEntity> GetWOProcessDetailsForIncharge(int inId)
        {
            try
            {
                return obj.GetWOProcessDetailsForIncharge(inId);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long InsertWOProcesses(MFG_WorkOrderProcessDetailsEntity entity)
        {
            try
            {
                return obj.InsertWOProcesses(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool InsertWOProcessMaterial(long id)
        {
            try
            {
                return obj.InsertWOProcessMaterial(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(MFG_WORK_ORDEREntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool MIDone(long id)
        {
            try
            {
                return obj.MIDone(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(long id,int Uid)
        {
            try
            {
                return obj.Delete(id, Uid);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(MFG_WORK_ORDEREntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public string getnextWOno()
        {
            try
            {
                return obj.getnextWOno();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public MFG_WORK_ORDEREntity GetbyId(long id)
        {
            try
            {
                return obj.GetbyId(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        

        public List<MFG_WORK_ORDEREntity> GetbyCustomerId(long id)
        {
            try
            {
                return obj.GetbyCustomerId(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<MFG_WORK_ORDEREntity> GetCompletedWorkOrdersbyCustomerId(long id)
        {
            try
            {
                return obj.GetCompletedWorkOrderbyCustomerId(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<KeyValuePair<long, string>> GetMonthlyPlansByWorkOrderID(long workOrderID)
        {
            try
            {
                return obj.GetMonthlyPlansByWorkOrderID(workOrderID);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public string GetWorkOrdersQuantity(long workOrderID)
        {
            try
            {
                return obj.GetWorkOrdersQuantity(workOrderID);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<MFG_WORK_ORDEREntity> getWOSEBal(long id)
        {
            try
            {
                return obj.getWOSEBal(id);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<CRM_CustomerPOMasterentity> getPOListbycustomerid(long custid)
        {
            try
            {
                return obj.getPOListbycustomerid(custid);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
