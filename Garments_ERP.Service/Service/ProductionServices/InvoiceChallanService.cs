﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class InvoiceChallanService
    {
        cl_InvoiceChallan obj = new cl_InvoiceChallan();

        public List<T_Invoice_ChallanEntity> Get()
        {
            try
            {
                return obj.Get();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public long Insert(T_Invoice_ChallanEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_Product_InfoEntity> GetBatachLot(long ItemId, int ItemAttrId, long poid)
        {
            try
            {
                return obj.GetBatachLot(ItemId, ItemAttrId, poid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string getnextInvoiceNo()
        {
            try
            {
                return obj.getnextInvoiceNo();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CRM_CustomerPOMasterentity> getPOListbycustomerid(long custid)
        {
            try
            {
                return obj.getPOListbycustomerid(custid);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
