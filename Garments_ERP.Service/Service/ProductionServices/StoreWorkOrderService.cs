﻿using Garments_ERP.Data.Classes;
using Garments_ERP.Entity;
using Garments_ERP.Service.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class StoreWorkOrderService: IStoreWorkOrderService
    {
        cl_StoreWorkOrder obj = new cl_StoreWorkOrder();
        public string getnextSWOno()
        {
            try
            {
                return obj.getnextSWOno();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DataTable CheckBOM(long StoreItemId, long ItemSizeId, decimal ItemQty,int companyid,int branchid)
        {
            try
            {
                return obj.CheckBOM(StoreItemId, ItemSizeId, ItemQty, companyid, branchid);
            }
            catch(Exception)
            {
                throw;
            }
        }

        public long AddUpdate(MFG_Store_WorkOrderEntity entity, List<MFG_Store_WO_ItemDetailsEntity> SWO_ItemEntity, List<SRM_PurchaseRequestItemDetailEntity> SWO_BOM_ItemEntity)
        {
            try
            {
                return obj.AddUpdate(entity, SWO_ItemEntity, SWO_BOM_ItemEntity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<MFG_Store_WorkOrderEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<MFG_Store_WO_ItemDetailsEntity> GetStoreWO_ItemData(long Store_WO_Id)
        {
            try
            {
                return obj.GetStoreWO_ItemData(Store_WO_Id);
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
