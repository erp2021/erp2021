﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Data.Classes.ProductionClasses;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.ProductionEntities;
using Garments_ERP.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Service
{
    public class MaterialIssueService:IMaterialIssueService
    {
        cl_MaterialIssue obj = new cl_MaterialIssue();
        cl_RawMaterialIssue robj = new cl_RawMaterialIssue();

        public List<MFG_MaterialIssueMasterEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<MFG_MaterialIssueMasterEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.get(from, to, IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public MFG_WorkOrderDetailsForProductionPlan GetWorkOrderDetails(long workOrderID, long planID,long MIid)
        {
            try
            {
                return obj.GetWorkOrderDetails(workOrderID, planID, MIid);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public MFG_MaterialIssueItemDetailsEntity getbyitemid(long itemid, long MIid)
        {
            try
            {
                return obj.getbyitemid(itemid, MIid);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<SP_GetDetailsForMaterialIssueEntity> GetWOProcessMaterialDetails(long woId)
        {
            try
            {
                return obj.GetWOProcessMaterialDetails(woId);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public string getnextTno()
        {
            try
            {
                return obj.getnextTno();
            }
            catch (Exception)
            {

                throw;
            }
        }
        
        public string getnextmino()
        {
            try
            {
                return obj.getnextmino();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(MFG_MaterialIssueMasterEntity entity)
        {
            try
            {
                return obj.Update(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public MFG_MaterialIssueMasterEntity getById(long id)
        {
            try
            {
                return obj.getById(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<MFG_MaterialIssueMasterEntity> get()
        {
            try
            {
                return obj.get();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Delete(long id,int Uid)
        {
            try
            {
                return obj.Delete(id,Uid);
            }
            catch (Exception)
            {

                throw;
            }
        }
        //public long Insert(MFG_MaterialIssueMasterEntity entity)
        //{
        //    try
        //    {
        //        return obj.Insert(entity);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}
        public bool deleterawitem(long id)
        {
            try
            {
                return obj.deleterawitem(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public long Insert1(MFG_MaterialIssueMasterEntity entity)
        {
            try
            {
                return robj.Insert1(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        

        public long Insert(M_ItemOutwardMaster entity)
        {
            try
            {
                return robj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public long Insert(MFG_MaterialIssueMasterEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
