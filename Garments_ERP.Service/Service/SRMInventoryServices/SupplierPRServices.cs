﻿using Garments_ERP.Data.Classes;
using Garments_ERP.Entity;
using Garments_ERP.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Garments_ERP.Service.Service
{
    public class SupplierPRServices: ISupplierPRServices
    {
        cl_SupplierPR obj = new cl_SupplierPR();
        public string getnextprno()
        {
            try
            {
                return obj.getnextprno();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public object InventoryPR(int type, string Jobject, string Attribute)
        {
            try
            {
                return obj.InventoryPR(type,Jobject,Attribute);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public long Insert(SRM_PurchaseRequestEntity entity)
        {
            try
            {
                return obj.Insert(entity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<EmployeeEntity> GetAccountHead(int deptid, int Company_ID, int BranchId)
        {
            try
            {
                return obj.GetAccountHead(deptid, Company_ID, BranchId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public object StatusCount(int Type, string attribute, XmlDocument doc)
        {
            try
            {
                return obj.StatusCount(Type, attribute, doc);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Delete(long id, int Userid)
        {
            try
            {
                return obj.Delete(id, Userid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool RejectedPR(long id, int Uid)
        {
            try
            {
                return obj.RejectedPR(id, Uid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<SRM_PurchaseRequestEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<SRM_PurchaseRequestEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            try
            {
                return obj.get(from, to, IsStatus, Company_ID, BranchId, Userid, roleid);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public SRM_PurchaseRequestEntity getbyid(long id)
        {
            try
            {
                return obj.getbyid(id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<SRM_PRItemDetailEntity> getbyPRItemid(long id)
        {
            try
            {
                return obj.getbyPRItemid(id);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
