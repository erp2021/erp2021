﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
   public interface IItemColorService
    {
       List<M_ItemColorEntity> get();
       int Insert(M_ItemColorEntity entity);

       bool Update(M_ItemColorEntity entity);
       bool Delete(int id);
       bool CheckItemColorExistance(string ItemColor, int? Id);
    }
}
