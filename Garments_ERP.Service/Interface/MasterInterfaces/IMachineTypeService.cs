﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface IMachineTypeService
    {
        List<M_MachineTypeMasterEntity> get();
        long Insert(M_MachineTypeMasterEntity entity);
        bool Update(M_MachineTypeMasterEntity entity);
        bool Delete(long id);
    }
}
