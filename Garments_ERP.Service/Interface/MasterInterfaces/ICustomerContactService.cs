﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface ICustomerContactService
    {
        List<M_CustomerContactDetailsEntity> GetById(long id);
        long Insert(M_CustomerContactDetailsEntity entity);
        bool Update(M_CustomerContactDetailsEntity entity);
    }
}
