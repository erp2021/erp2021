﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
    public interface IItemshadeService
    {
        List<M_ItemShadeMasterEntity> get();
        int Insert(M_ItemShadeMasterEntity entity);
        bool Update(M_ItemShadeMasterEntity entity);
        bool Delete(int id);
        bool CheckItemShadeExistance(string Itemshade, int? Id);
        List<M_ItemShapeMasterEntity> getShape();
        int InsertShape(M_ItemShapeMasterEntity entity);
        bool UpdateShape(M_ItemShapeMasterEntity entity);
        bool DeleteShape(int id);
        bool CheckItemShapeExistance(string Itemshape, int? Id);
    }
}
