﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface IUnitService
    {
        List<M_UnitMasterEntity> get();  
        M_UnitMasterEntity GetById(int id);
        int Insert(M_UnitMasterEntity entity);
        bool Update(M_UnitMasterEntity entity);
        bool Delete(int id);
        bool CheckUnitExistance(string ItemUnit, int? Id);
    }
}
