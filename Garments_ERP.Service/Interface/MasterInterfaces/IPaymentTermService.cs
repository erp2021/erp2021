﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface IPaymentTermService
    {
        List<M_PaymentTermMasterEntity> get();
        int Insert(M_PaymentTermMasterEntity entity);
        bool CheckPaymentTermExistance(string paymentterm, int? Id);
        bool Update(M_PaymentTermMasterEntity entity);
        bool Delete(int id);
    }
}
