﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface IWarehouseRoleService
    {
        List<M_WarehouseRoleMasterEntity> get();
        M_WarehouseRoleMasterEntity getbyid(long id);
        long Insert(M_WarehouseRoleMasterEntity entity);
        bool Update(M_WarehouseRoleMasterEntity entity);
        bool Delete(long id);
    }
}
