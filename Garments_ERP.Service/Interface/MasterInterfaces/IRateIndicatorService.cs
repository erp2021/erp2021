﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;

namespace Garments_ERP.Service.Interface
{
    public interface IRateIndicatorService
    {
        long Insert(M_RateIndicatorMasterEntity entity);
        List<M_RateIndicatorMasterEntity> get();
        bool Update(M_RateIndicatorMasterEntity entity);
        bool Delete(int id);
        bool CheckRateIndicatorNameExistance(string RateIndicatorName, int? Id);

    }
}
