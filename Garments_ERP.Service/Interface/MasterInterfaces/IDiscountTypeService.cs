﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;

namespace Garments_ERP.Service.Interface
{
    public interface IDiscountTypeService
    {
        List<M_DiscountTypeMasterEntity> get();
        long Insert(M_DiscountTypeMasterEntity entity);
        bool Update(M_DiscountTypeMasterEntity entity);
        bool Delete(int id);
        bool CheckDiscountTypeNameExistance(string DiscountType, int? Id);
    }
}
