﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface.MasterInterfaces
{
   public interface IBrokerService
    {
        List<M_BrokerMasterEntity> GetAll();
        M_BrokerMasterEntity GetById(long id);
        long Insert(M_BrokerMasterEntity entity);
        bool Update(M_BrokerMasterEntity entity);
        bool Delete(long id);
    }
}
