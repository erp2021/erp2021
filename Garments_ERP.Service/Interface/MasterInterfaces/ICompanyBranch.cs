﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface.MasterInterfaces
{
    interface ICompanyBranch
    {
       List<M_BranchEntity> getComdetail();
        string GetPanNo(int CompanyId);
    }
}
