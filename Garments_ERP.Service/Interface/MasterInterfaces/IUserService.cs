﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
  public  interface IUserService
    {
      M_UserEntity Authenticateuser(M_UserEntity entity);
      List<M_UserEntity> get();
      List<M_UserTypeEntity> getUsertype();
      int Insert(M_UserEntity entity);
      M_UserEntity IsEmailValid(string email);
      bool ValidateResetPasswordLink(M_UserEntity model);
      int ChangePassword(M_UserEntity model);

      List<M_UserPermissionsEntity> getbyUserid(int userid);
      int Insert(List<M_UserPermissionsEntity> list);
      int Update(List<M_UserPermissionsEntity> list);
      List<M_ModuleEntity> GetModules();
      M_UserEntity GetByid(int id);
      bool UpdateUser(M_UserEntity entity);
      bool DeleteUser(int id);
     
    }
}
