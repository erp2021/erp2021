﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
    public interface IItemCategoryService
    {
        List<M_ItemCategoryMasterEntity> get();
        long Insert(M_ItemCategoryMasterEntity entity);
        bool Update(M_ItemCategoryMasterEntity entity);
        bool Delete(int id);
        bool CheckExistance(string ItemCategory, int? Id, int Itemgroupid,int companyid,int branchid);
        List<M_ItemCategoryMasterEntity> getRawSemifinsh();
    }
}
