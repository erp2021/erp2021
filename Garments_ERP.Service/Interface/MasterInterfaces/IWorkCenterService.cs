﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface IWorkCenterService
    {
        List<M_WorkCenterMasterEntity> get();
        M_WorkCenterMasterEntity GetById(int id);
        int Insert(M_WorkCenterMasterEntity entity);
        bool Update(M_WorkCenterMasterEntity entity);
        bool Delete(int id);
        bool CheckWorkCenterExistance(string WorkCenter, int? Id);
    }
}
