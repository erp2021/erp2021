﻿using Garments_ERP.Entity.MasterEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface.MasterInterfaces
{
   public interface ISchemeService
    {
        List<M_SchemeMaster_Entity> get(int comid, int branchid);
        long InsertUpdate(M_SchemeMaster_Entity entity);
        M_SchemeMaster_Entity getById(long id);
    }
}
