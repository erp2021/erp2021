﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface IEmployeeService
    {
        List<M_ProcessMasterEntity> get();
        List<EmployeeAddressDetailEntity> getaddress();
        EmployeeAddressDetailEntity getaddressbyid(long id);
        int Insert(EmployeeEntity entity);
        long InsertAddress(EmployeeAddressDetailEntity entity);
        bool Delete(int id);
    }
}
