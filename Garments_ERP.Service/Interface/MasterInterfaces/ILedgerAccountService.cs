﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;

namespace Garments_ERP.Service.Interface
{
    public interface ILedgerAccountService
    {
        List<M_LedgersEntity> get();
        List<M_ContactAddressEntity> getAddresssbycustomerid(long custid);
        long Insert(M_LedgersEntity entity);
        M_LedgersEntity GetById(long id);
        bool Update(M_LedgersEntity entity);
        bool Delete(long id);
        bool Updateadddress(M_ContactAddressEntity entity);
        bool deleteaddress(long id);
        List<M_LedgersEntity> getCustomerByWO(long woID);
        List<M_LedgersEntity> getCustList(int CBS);
        List<M_Ledger_BillingDetailsEntity> GetAddressById(long custid);
        List<M_Ledger_BillingDetailsEntity> getAddInfo(int id);
        
    }
}
