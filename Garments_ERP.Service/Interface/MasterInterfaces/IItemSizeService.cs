﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
  public  interface IItemSizeService
    {
      List<M_ItemSizeMasterEntity> get();
    }
}
