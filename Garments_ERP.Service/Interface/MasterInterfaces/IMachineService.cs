﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
   public interface IMachineService
    {
       long Insert(MachineEntity entity);
       bool Update(MachineEntity entity);
       bool Delete(long id);
       MachineEntity GetById(long id);
       List<MachineEntity> Get();
    }
}
