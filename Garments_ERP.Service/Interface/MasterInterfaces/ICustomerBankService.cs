﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface ICustomerBankService
    {
        M_CustomerBankDetailsEntity GetById(long id);
        long Insert(M_CustomerBankDetailsEntity entity);
        bool Update(M_CustomerBankDetailsEntity entity);
    }
}
