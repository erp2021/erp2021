﻿using Garments_ERP.Entity.MasterEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface.MasterInterfaces
{
    public interface IInwardItemService
    {
        long Insert(M_ItemInwardMasterEntity entity);
        List<M_ItemInwardMasterEntity> get();
        M_ItemInwardMasterEntity GetById(long id);
        bool Update(M_ItemInwardMasterEntity entity);
        bool Delete(long id);
    }
}
