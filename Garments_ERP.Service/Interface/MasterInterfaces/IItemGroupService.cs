﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
    public interface IItemGroupService
    {
        List<M_ItemGroupEntity> Get();
        int Insert(M_ItemGroupEntity entity);

        bool Update(M_ItemGroupEntity entity);
        bool Delete(int id);
        bool CheckExistance(string Itemgroup, int? Id);
    }
}
