﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;

namespace Garments_ERP.Service.Interface
{
    public interface IItemSubCategoryService
    {
        List<M_ItemSubCategoryMasterEntity> GetById(long id);
        M_ItemSubCategoryMasterEntity bysubcategoryid(long id);
        List<M_ItemSubCategoryMasterEntity> get();
        int Insert(M_ItemSubCategoryMasterEntity entity);
        bool Update(M_ItemSubCategoryMasterEntity entity);
        bool Delete(int id);
        bool CheckExistance(string Itemsubcategory, int? Id, int itemcategoryid,int companyid,int branchid);
        List<M_ItemMasterEntity> GetRawItemList(int subcatid);
    }
}
