﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
  public  interface IStyleSizeDetailService
    {
      long InsertQuotationSizeDetail(CRM_QuotationStyleSizeDetailEntity entity);
      long InsertEnquirySizeDetail(CRM_EnquiryStyleSizeDetailEntity entity);
    }
}
