﻿using Garments_ERP.Entity.MasterEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface.MasterInterfaces
{
    public interface IAttributeService
    {
        int Insert(M_Attribute_MasterEntity entity);
        List<M_Attribute_MasterEntity> get();
        M_Attribute_MasterEntity GetById(int id);
        bool Update(M_Attribute_MasterEntity entity);
        bool Delete(int id);

    }
}
