﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;

namespace Garments_ERP.Service.Interface
{
    public interface IBrandService
    {
        List<M_BrandMasterEntity> get();
        int Insert(M_BrandMasterEntity entity);

        bool Update(M_BrandMasterEntity entity);
        bool Delete(int id);
        bool CheckBrandNameExistance(string BrandName, int? Id);
    }
}
