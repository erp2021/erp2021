﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
   public interface ISegmentTypeService
    {
       List<M_SegmentTypeMasterEntity> get();
       int Insert(M_SegmentTypeMasterEntity entity);
       bool CheckSegmentTypeExistance(string Segmenttype, int? Id,int companyid,int branchid);
       bool Update(M_SegmentTypeMasterEntity entity);
       bool Delete(int id);
    }
}
