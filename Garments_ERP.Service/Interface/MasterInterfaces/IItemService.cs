﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
    public interface IItemService
    {
        long Insert(M_ItemMasterEntity entity,string AttributeValList);
        List<M_ItemMasterEntity> get();
        M_ItemMasterEntity GetById(long id);
        bool Update(M_ItemMasterEntity entity,string AttributeValList);
        bool Delete(long id);
        long InsertImage(M_ItemMasterImageEntity entity);
        M_ItemMasterImageEntity GetImageById(long id);
        bool UpdateImage(M_ItemMasterImageEntity entity);
        bool DeleteImage(long id);
        List<M_ItemMasterEntity> GetByCategoryId(long id);
        long InsertItemAttrib(XmlDocument doc);
        List<M_ItemMasterEntity> getFinishedItem();
        List<M_InventoryListEntity> GetInventory(string MyJson, int type, XmlDocument doc);
        List<M_ProductInfoEntity> GetRackData(long ItemId, int ItemAttr);

    }
}
