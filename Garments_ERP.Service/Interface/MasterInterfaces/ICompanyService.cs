﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface.MasterInterfaces
{
    interface ICompanyService
    {
        List<M_companyEntity> get();
        List<M_BranchEntity> GetBranchListById(int compid);
    }
}
