﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;

namespace Garments_ERP.Service.Interface
{
   public  interface ITaxService
    {
        List<M_TaxMasterEntity> get();
        int Insert(M_TaxMasterEntity entity);
        M_TaxMasterEntity getbyid(long id);
        bool Update(M_TaxMasterEntity entity);
        bool Delete(int id);
        bool TaxNameExistance(string TaxName, int? Id);
    }
}
