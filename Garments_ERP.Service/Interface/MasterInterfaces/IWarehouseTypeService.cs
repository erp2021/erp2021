﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface IWarehouseTypeService
    {
        List<M_WarehouseTypeMasterEntity> get();
        M_WarehouseTypeMasterEntity getbyid(long id);
        long Insert(M_WarehouseTypeMasterEntity entity);
        bool Update(M_WarehouseTypeMasterEntity entity);
        bool Delete(long id);
    }
}
