﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
 public interface ICustomerService
    {
     List<M_CustomerMasterEntity> get();
     List<M_ContactAddressEntity> getAddresssbycustomerid(long custid);
     long Insert(M_CustomerMasterEntity entity);
     M_CustomerMasterEntity GetById(long id);
     bool Update(M_CustomerMasterEntity entity);
     bool Delete(long id);
     bool Updateadddress(M_ContactAddressEntity entity);
     bool deleteaddress(long id);
     List<M_CustomerMasterEntity> getCustomerByWO(long woID);
    }
}
