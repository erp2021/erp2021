﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
  public  interface IQuotationService
    {

        long AddUpdate(List<string> Style_Img, List<string> RawItemImg, CRM_QuotationMasterEntity Entity, List<CRM_QuotationRawItemDetailEntity> RawItem);
      object _getGSTTaxlist();
      string getnextQuotatioNo();
      List<CRM_QuotationMasterEntity> get();
      long Insert(CRM_QuotationMasterEntity entity);
      CRM_QuotationMasterEntity getbyid(long id);
      bool Update(CRM_QuotationMasterEntity entity);
      bool Delete(long id,int Userid);
      List<CRM_QuotationMasterEntity> get(DateTime from, DateTime to);
      List<CRM_QuotationMasterEntity> getquotationnobycustomer(long customerid,int Company_ID,int BranchId);
        object QuoteGet(long id);
        bool RejectedQuo(long id, int Uid);
        List<CRM_QuotationMasterEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid);
        List<CRM_QuotationMasterEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid);
        List<CRM_QuotationRawItemDetailEntity> GetQuoStyleRawItemData(long QuoId, long styleid);
    }
}
