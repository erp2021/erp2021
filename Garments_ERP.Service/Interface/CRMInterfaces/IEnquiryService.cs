﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
  public  interface IEnquiryService
    {

        long AddUpdate(List<string> Style_Img, List<string> RawItemImg, CRM_EnquiryMasterEntity Entity, List<CRM_EnquiryRawitemDetailEntity> RawItem);
        CRM_EnquiryMasterEntity getbyid(long id);
        List<CRM_EnquiryStyleDetailsEntity> GetEnqryStyleData(long EnquiryId);
        List<CRM_EnquiryRawitemDetailEntity> GetEnqryStyleRawItemData(long EnquiryId,long styleid);




        List<CRM_EnquiryMasterEntity> get();
        List<CRM_EnquiryMasterEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid);
        long Insert(CRM_EnquiryMasterEntity entity);
      
      bool Update(CRM_EnquiryMasterEntity entity);
      string getnextenqno();
      bool Delete(long id,int Userid);
        bool Rejectedenquiry(long id,int Uid);
        List<CRM_EnquiryMasterEntity> getEnquirynobycustomerid(long customerid,int Company_ID,int BranchId);
      List<CRM_EnquiryMasterEntity> get(DateTime from, DateTime to);
        List<CRM_EnquiryMasterEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid);
        object SearchList(XmlDocument doc, int type, string id,long screenId);
        object StatusCount(int Type,string attribute,XmlDocument doc);
        List<M_EnquiryTypeMasterEntity> getEnqiryType();
    }
}
