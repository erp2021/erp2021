﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
   public interface IEnquiryRawItemDetailService
    {
       List<CRM_EnquiryRawitemDetailEntity> getbyenquiryid(long id);
       long Insert(CRM_EnquiryRawitemDetailEntity entity);
       bool Update(CRM_EnquiryRawitemDetailEntity entity);
       bool Delete(long id);
    }
}
