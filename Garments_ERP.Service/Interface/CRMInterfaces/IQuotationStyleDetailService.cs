﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
  public  interface IQuotationStyleDetailService
    {
      long Insert(CRM_QuotationStyleDetailEntity entity);
      CRM_QuotationStyleDetailEntity getbyquotationid(long id);
      bool Update(CRM_QuotationStyleDetailEntity entity);
        object EditAttributeValue(string MyJson, int type, XmlDocument doc);
        long InsertStyleImage(CRM_QuotationStyleImageEntity entity);
        long updateStyleImage(CRM_QuotationStyleImageEntity entity);
    }
}
