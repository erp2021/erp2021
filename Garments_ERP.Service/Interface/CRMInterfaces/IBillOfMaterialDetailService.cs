﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface IBillOfMaterialDetailService
    {
        List<M_BillOfMaterialDetailEntity> get();
        long Insert(M_BillOfMaterialDetailEntity entity);
        List<M_BillOfMaterialDetailEntity> GetById(long id);
        bool Update(M_BillOfMaterialDetailEntity entity);
        bool Delete(long id);
    }
}
