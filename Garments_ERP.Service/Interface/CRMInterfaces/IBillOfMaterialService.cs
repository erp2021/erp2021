﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
   public interface IBillOfMaterialService
    {
       List<M_BillOfMaterialMasterEntity> get();
       Base_BillOfMaterialMasterEntity getBase();
        long Insert(M_BillOfMaterialMasterEntity entity);
        M_BillOfMaterialMasterEntity GetByEnquiry(long id);
        M_BillOfMaterialMasterEntity GetById(long id);
        M_BillOfMaterialMasterEntity GetDetailsForPRByBOM(long bomid);
        bool Update(M_BillOfMaterialMasterEntity entity);
        bool UpdatePRDone(M_BillOfMaterialMasterEntity entity);
        bool Delete(long id);
        string getnextbomno();
        Base_BillOfMaterialMasterEntity get(DateTime from, DateTime to);
    }
}
