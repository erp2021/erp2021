﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
  public  interface ICustomerPOService
    {
      List<CRM_CustomerPOMasterentity> Get();
      List<CRM_CustomerPOMasterentity> Get(DateTime from, DateTime to);
      long Insert(CRM_CustomerPOMasterentity entity);
      bool Update(CRM_CustomerPOMasterentity entity);
      bool Delete(long id);
      string getnextPONo();
      CRM_CustomerPOMasterentity getbyid(long id);
      List<CRM_CustomerPOMasterentity> Getbycustomerid(long custid);
      List<CRM_CustomerPOMasterentity> getPOListbycustomerid(long custid);
      object getbyPONO(string PONO);

    }
}
