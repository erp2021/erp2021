﻿using Garments_ERP.Entity;
using Garments_ERP.Entity.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface.Accounts
{
    public interface ICounterSalesService
    {
        List<Acc_Voucher_Customer_Details_Entity> Get_NameContact();
        List<M_ItemMasterEntity> Get_ItemsWithHSN();
        Int32 SaveCounterSales(Acc_Voucher_Entity v_, List<Acc_Voucher_Items_Detail_Entity> vi_, Acc_Voucher_Customer_Details_Entity vc_);
    }
}
