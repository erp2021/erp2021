﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface ISupplierPOService
    {
        string getnextPONo();
        List<SRM_PurchaseOrderMasterEntity> Get();
        List<SRM_PurchaseOrderMasterEntity> Getbysupplierid(long suppid);
        SRM_PurchaseOrderMasterEntity getbyid(long id);
        long Insert(SRM_PurchaseOrderMasterEntity entity);
        bool Update(SRM_PurchaseOrderMasterEntity entity);

        List<SRM_PurchaseOrderMasterEntity> Get(DateTime from, DateTime to);
        bool Delete(long id);
        object getbyPONO(string PONO);
    }
}
