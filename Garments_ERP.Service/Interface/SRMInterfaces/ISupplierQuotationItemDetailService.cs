﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
   public interface ISupplierQuotationItemDetailService
    {
       long Insert(SRM_QuotationItemDetailEntity record);
       bool Update(SRM_QuotationItemDetailEntity entity);
       List<SRM_QuotationItemDetailEntity> getbyquotationid(long id);
       List<SRM_QuotationItemDetailEntity> getbyquotationid();
       bool deleterawitem(long id);
    }
}
