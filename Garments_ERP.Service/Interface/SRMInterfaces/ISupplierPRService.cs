﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
   public interface ISupplierPRService
    {
       string getnextprno();
       bool Update(SRM_PurchaseRequestMasterEntity entity);
       SRM_PurchaseRequestMasterEntity getbyid(long id);
       List<SRM_PurchaseRequestMasterEntity> get();
       List<SRM_PurchaseRequestMasterEntity> getList();
       List<SRM_PurchaseRequestMasterEntity> get(DateTime from, DateTime to);
       bool Delete(long id);
       long Insert(SRM_PurchaseRequestMasterEntity entity);
    }
}
