﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface ISupplierService
    {
        List<M_SupplierMasterEntity> get();
        M_SupplierMasterEntity GetById(long id);
        long Insert(M_SupplierMasterEntity entity);
        bool Update(M_SupplierMasterEntity entity);
        bool Delete(long id);
        M_SupplierMasterEntity GetSuppliername(long Suppliername);

    }
}
