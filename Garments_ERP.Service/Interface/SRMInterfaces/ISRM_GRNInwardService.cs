﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface ISRM_GRNInwardService
    {
        List<SRM_GRNInwardEntity> get();
        long Insert(SRM_GRNInwardEntity entity);
        bool Update(SRM_GRNInwardEntity entity);
        string getnextpono();
        SRM_GRNInwardEntity getbyid(long id);
         SRM_GRNItemEntity getbyitemid(long id);
        bool UpdateGRNItem(SRM_GRNItemEntity entity);
        bool DeleteGRNItem(long id);
        bool Delete(long id,int Userid);
        List<SRM_GRNInwardEntity> get(DateTime from, DateTime to);
        string getnextgrnno();
        string getnextchnno();
        string getnextinvno();
        string GetbysupplierAdd(long id);
        List<SRM_GRNInwardEntity> getItemBal(long poid, long itemid,int IAttr);
        List<M_Dynamic_ControlsEntity> GetControlList();
        List<SRM_GRNInwardEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid);
        List<SRM_GRNInwardEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid);
    }
}
