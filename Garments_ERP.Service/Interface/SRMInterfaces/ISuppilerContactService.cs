﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface ISuppilerContactService
    {
        List<M_SupplierContactDetailsEntity> get();
        List<M_SupplierContactDetailsEntity> GetById(long id);
        long Insert(M_SupplierContactDetailsEntity entity);
        bool Update(M_SupplierContactDetailsEntity entity);
    }
}
