﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
namespace Garments_ERP.Service.Interface
{
  public  interface IProductionPlanService
    {
      List<MFG_PRODUCTION_PLANEntity> Get();
      long Insert(MFG_PRODUCTION_PLANEntity entity);
      bool Update(MFG_PRODUCTION_PLANEntity entity);
      MFG_PRODUCTION_PLANEntity Getbyid(long id);
      bool UpdateProductionPlanDetail(MFG_PRODUCTION_PLAN_DETAILSEntity plandetailentity);
      bool DeleteProductionPlanDetail(long id);
      bool Delete(long id);
        string getnextPNno();

    }
}
