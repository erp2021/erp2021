﻿using Garments_ERP.Entity;
using Garments_ERP.Entity.ProductionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface.ProductionInterfaces
{
    public interface IFG_RetailService
    {
        List<MFG_WORK_ORDEREntity> Get_WorkOrders(int companyid, int branchid, string inputStr, long CustID);
        List<FG_TransferMasterEntity> Get_FGTransfers(int companyid, int branchid, string inputStr, long woID);
        List<FG_TransferDetailEntity> Get_FGTransferDetails(int companyid, int branchid, long fg_transferid);
        Int64 SaveFG_Retail(FG_Transfer_To_RetailEntity FG_TREntity);
        List<FG_Transfer_To_RetailEntity> Get(int company_id, int branch_id);
        FG_Transfer_To_RetailEntity getbyid(long id);
        bool Delete(long id);
        List<SRM_GRNInwardEntity> GetGRNList(long Bomid);
    }
}
