﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface.ProductionInterfaces
{
    public interface IFinishedGoodsService
    {
        int Insert(M_ItemMasterEntity entity);
        List<M_ItemMasterEntity> get();
        M_ItemMasterEntity GetById(long id);

    }
}
