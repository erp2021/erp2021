﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
   public interface ITransferService
    {
       string getnextTFNo();
       List<FG_TransferMasterEntity> Get(DateTime from, DateTime to);
       List<FG_TransferMasterEntity> Get();
       FG_TransferMasterEntity getbyid(long id);
       long Insert(FG_TransferMasterEntity entity);
       bool Update(FG_TransferMasterEntity entity);
       bool Delete(long id);
    }
}
