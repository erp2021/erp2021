﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface IStoreWorkOrderService
    {
        string getnextSWOno();
        DataTable CheckBOM(long StoreItemId, long ItemSizeId, decimal ItemQty,int companyid,int branchid);
        long AddUpdate(MFG_Store_WorkOrderEntity entity, List<MFG_Store_WO_ItemDetailsEntity> SWO_ItemEntity, List<SRM_PurchaseRequestItemDetailEntity> SWO_BOM_ItemEntity);
        List<MFG_Store_WorkOrderEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid);
        List<MFG_Store_WO_ItemDetailsEntity> GetStoreWO_ItemData(long Store_WO_Id);
    }
}
