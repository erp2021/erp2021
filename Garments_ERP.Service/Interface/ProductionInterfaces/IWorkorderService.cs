﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
using Garments_ERP.Entity.ProductionEntities;

namespace Garments_ERP.Service.Interface
{
    public interface IWorkorderService
    {
        string getnextWOno();
        List<MFG_WORK_ORDEREntity> Get();
        List<MFG_WORK_ORDEREntity> Get(DateTime from, DateTime to);
        List<MFG_WorkOrderProcessDetailsEntity> GetbyWOId(int id);
        List<SP_GetWO_ProcessMaterialDetailsEntity> GetWOProcessDetails();
        List<SP_GetProc_DetailsForProc_InchargeEntity> GetWOProcessDetailsForIncharge(int inId);
        long InsertWOProcesses(MFG_WorkOrderProcessDetailsEntity entity);
        bool InsertWOProcessMaterial(long id);
        long Insert(MFG_WORK_ORDEREntity entity);
        bool Delete(long id,int Uid);
        bool MIDone(long id);
        bool Update(MFG_WORK_ORDEREntity entity);
        MFG_WORK_ORDEREntity GetbyId(long id);
        List<MFG_WORK_ORDEREntity> getWOSEBal(long id);
        List<CRM_CustomerPOMasterentity> getPOListbycustomerid(long custid);
        List<MFG_WORK_ORDEREntity> GetWOMI(int comid,int branchid);
        bool RejectedWO(long id, int Uid);
        List<MFG_WORK_ORDEREntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid);
        List<MFG_WORK_ORDEREntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid);
        M_ItemMaster_AttributeMaserEntity GetItemData(long id);
        List<M_ProductInfoEntity> GetBatchData(long id, long ItemId, int ItemAttr);

    }
}
