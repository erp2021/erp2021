﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface IReturnOrderService
    {
        string getnextROno();
        List<T_ISSUE_RETURNS_DETAILEntity> GetInvoiceItem(long Id);
        long insert(T_ISSUE_RETURNSEntity entity);
        List<T_ISSUE_RETURNSEntity> get();
        List<T_ISSUE_RETURNS_DETAILEntity> GetSPOitem(long Id);
        List<T_ISSUE_RETURNS_DETAILEntity> GetTransferitem(long Id);
        T_ISSUE_RETURNSEntity GetById(long id);
    }
}
