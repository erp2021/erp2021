﻿using Garments_ERP.Entity;
using Garments_ERP.Entity.ReportsSPEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Service.Interface
{
    public interface IReportsService
    {
        List<SP_WarehouseStockDetailsEntity> GetDateWiseWarehouseStock(long itemcategory);
        List<SP_WarehouseStockDetailsEntity> GetWarehouseStock();
        List<SP_GetItemInwardDetailsEntity> GetDateWiseItemInwardDetail(DateTime from, DateTime to);
        List<SP_GetItemInwardDetailsEntity> GetItemInwardDetail(long itemname);
        List<SP_GetInventoryDetailsEntity> GetInventoryDetails();
    }
}
