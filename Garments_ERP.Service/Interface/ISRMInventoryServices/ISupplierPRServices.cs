﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Garments_ERP.Service.Interface
{
    public interface ISupplierPRServices
    {
        string getnextprno();
        object InventoryPR(int type, string Jobject, string Attribute);
        long Insert(SRM_PurchaseRequestEntity entity);
        List<EmployeeEntity> GetAccountHead(int deptid, int Company_ID, int BranchId);
        object StatusCount(int Type, string attribute, XmlDocument doc);

        bool Delete(long id, int Userid);
        bool RejectedPR(long id, int Uid);

        List<SRM_PurchaseRequestEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid);
        List<SRM_PurchaseRequestEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid);
        SRM_PurchaseRequestEntity getbyid(long id);
        List<SRM_PRItemDetailEntity> getbyPRItemid(long id);
    }
}
