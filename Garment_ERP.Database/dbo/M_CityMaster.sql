﻿CREATE TABLE [dbo].[M_CityMaster] (
    [ID]      INT           IDENTITY (1, 1) NOT NULL,
    [Name]    NVARCHAR (50) NULL,
    [StateID] INT           NULL,
    CONSTRAINT [PK_M_CityMaster] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_M_CityMaster_M_StateMaster] FOREIGN KEY ([StateID]) REFERENCES [dbo].[M_StateMaster] ([ID])
);

