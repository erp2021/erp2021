﻿CREATE TABLE [dbo].[MFG_PRODUCTION_WEEKLY_PLAN_DETAILS] (
    [ID]                  BIGINT          IDENTITY (1, 1) NOT NULL,
    [PlanID]              BIGINT          NOT NULL,
    [WeekID]              INT             NULL,
    [SizeID]              BIGINT          NULL,
    [StyleID]             BIGINT          NULL,
    [Weekly_Target_Qty]   NUMERIC (18, 2) NULL,
    [Weekly_Produced_Qty] NUMERIC (18, 2) NULL,
    [UnitID]              INT             NULL,
    [MachineID]           BIGINT          NULL,
    [Inspection]          INT             NULL,
    [QC]                  INT             NULL,
    [CreatedDate]         DATETIME        NULL,
    [CreatedBy]           INT             NULL,
    CONSTRAINT [PK_MFG_PRODUCTION_WEEKLY_PLAN_DETAILS] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_MFG_PRODUCTION_WEEKLY_PLAN_DETAILS_Employee] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[Employee] ([ID]),
    CONSTRAINT [FK_MFG_PRODUCTION_WEEKLY_PLAN_DETAILS_M_ItemSizeMaster] FOREIGN KEY ([SizeID]) REFERENCES [dbo].[M_ItemSizeMaster] ([Id]),
    CONSTRAINT [FK_MFG_PRODUCTION_WEEKLY_PLAN_DETAILS_M_StyleMaster] FOREIGN KEY ([StyleID]) REFERENCES [dbo].[M_StyleMaster] ([Id]),
    CONSTRAINT [FK_MFG_PRODUCTION_WEEKLY_PLAN_DETAILS_M_UnitMaster] FOREIGN KEY ([UnitID]) REFERENCES [dbo].[M_UnitMaster] ([Id]),
    CONSTRAINT [FK_MFG_PRODUCTION_WEEKLY_PLAN_DETAILS_MachineMaster] FOREIGN KEY ([MachineID]) REFERENCES [dbo].[MachineMaster] ([Id])
);

