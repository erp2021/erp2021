﻿CREATE TABLE [dbo].[M_ItemInwardMaster] (
    [ID]            BIGINT        IDENTITY (1, 1) NOT NULL,
    [ItemInward_ID] BIGINT        NULL,
    [Jobwork_ID]    NUMERIC (18)  NULL,
    [Item_ID]       BIGINT        NULL,
    [UOM]           INT           NULL,
    [ItemQty]       DECIMAL (18)  NULL,
    [BalanceQty]    DECIMAL (18)  NULL,
    [Remarks]       VARCHAR (200) NULL,
    [CreatedBy]     INT           NULL,
    [CreatedDate]   DATETIME      NULL,
    [UpdatedBy]     INT           NULL,
    [UpdatedDate]   DATETIME      NULL,
    [IsActive]      BIT           NULL,
    CONSTRAINT [PK_M_ItemInwardMaster] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_M_ItemInwardMaster_M_ItemMaster] FOREIGN KEY ([Item_ID]) REFERENCES [dbo].[M_ItemMaster] ([Id]),
    CONSTRAINT [FK_M_ItemInwardMaster_M_UnitMaster] FOREIGN KEY ([UOM]) REFERENCES [dbo].[M_UnitMaster] ([Id]),
    CONSTRAINT [FK_M_ItemInwardMaster_SRM_GRNItem] FOREIGN KEY ([ItemInward_ID]) REFERENCES [dbo].[SRM_GRNItem] ([ID])
);

