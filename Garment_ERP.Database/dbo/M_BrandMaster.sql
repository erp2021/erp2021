﻿CREATE TABLE [dbo].[M_BrandMaster] (
    [Id]               BIGINT         IDENTITY (1, 1) NOT NULL,
    [Brandname]        NVARCHAR (70)  NULL,
    [Brandshortname]   NVARCHAR (50)  NULL,
    [Branddescription] NVARCHAR (150) NULL,
    [Createddate]      DATETIME       NULL,
    [Createdby]        INT            NULL,
    [Updateddate]      DATETIME       NULL,
    [UpdatedBy]        INT            NULL,
    [IsActive]         BIT            NULL,
    CONSTRAINT [PK_BrandMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

