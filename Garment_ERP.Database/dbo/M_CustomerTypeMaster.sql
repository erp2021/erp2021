﻿CREATE TABLE [dbo].[M_CustomerTypeMaster] (
    [Id]       INT           IDENTITY (1, 1) NOT NULL,
    [TypeName] NVARCHAR (20) NULL,
    [Typedesc] NVARCHAR (50) NULL,
    [IsActive] BIT           NULL,
    CONSTRAINT [PK_M_CustomerTypeMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

