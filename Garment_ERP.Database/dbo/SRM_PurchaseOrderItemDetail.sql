﻿CREATE TABLE [dbo].[SRM_PurchaseOrderItemDetail] (
    [Id]              BIGINT          IDENTITY (1, 1) NOT NULL,
    [PO_ID]           BIGINT          NULL,
    [ItemId]          BIGINT          NULL,
    [ItemDesc]        NVARCHAR (255)  NULL,
    [UnitId]          INT             NULL,
    [TotalQty]        DECIMAL (18, 2) NULL,
    [Provision]       NUMERIC (18)    NULL,
    [Wastage]         NUMERIC (18)    NULL,
    [StockQty]        DECIMAL (18, 2) NULL,
    [POQty]           DECIMAL (18, 2) NULL,
    [Rate]            DECIMAL (18, 2) NULL,
    [Amount]          DECIMAL (18, 2) NULL,
    [CreatedBy]       INT             NULL,
    [CreatedDate]     DATETIME        NULL,
    [UpdatedBy]       INT             NULL,
    [UpdatedDate]     DATETIME        NULL,
    [IsActive]        BIT             NULL,
    [RateIndicatorId] INT             NULL,
    [TaxId]           INT             NULL,
    [TaxValue]        DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_SRM_PurchaseOrderItemDetail] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_SRM_PurchaseOrderItemDetail_M_ItemMaster] FOREIGN KEY ([ItemId]) REFERENCES [dbo].[M_ItemMaster] ([Id]),
    CONSTRAINT [FK_SRM_PurchaseOrderItemDetail_M_RateIndicatorMaster] FOREIGN KEY ([RateIndicatorId]) REFERENCES [dbo].[M_RateIndicatorMaster] ([Id]),
    CONSTRAINT [FK_SRM_PurchaseOrderItemDetail_M_TaxMaster] FOREIGN KEY ([TaxId]) REFERENCES [dbo].[M_TaxMaster] ([Id]),
    CONSTRAINT [FK_SRM_PurchaseOrderItemDetail_M_UnitMaster] FOREIGN KEY ([UnitId]) REFERENCES [dbo].[M_UnitMaster] ([Id]),
    CONSTRAINT [FK_SRM_PurchaseOrderItemDetail_SRM_PurchaseOrderMaster] FOREIGN KEY ([PO_ID]) REFERENCES [dbo].[SRM_PurchaseOrderMaster] ([Id])
);

