﻿CREATE TABLE [dbo].[CRM_EnquiryStyleImage] (
    [Id]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [Enquiryid]   BIGINT         NULL,
    [styleimg]    NVARCHAR (200) NULL,
    [Createddate] BIT            NULL,
    [Createdby]   INT            NULL,
    [Updateddate] DATETIME       NULL,
    [Updatedby]   INT            NULL,
    [IsActive]    BIT            NULL,
    CONSTRAINT [PK_CRM_EnquiryStyleImage] PRIMARY KEY CLUSTERED ([Id] ASC)
);

