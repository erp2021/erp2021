﻿CREATE TABLE [dbo].[M_ItemCategoryMaster] (
    [Id]               BIGINT         IDENTITY (1, 1) NOT NULL,
    [Itemcategory]     NVARCHAR (100) NULL,
    [IsActive]         BIT            NULL,
    [Createddate]      DATETIME       NULL,
    [CreatedBy]        INT            NULL,
    [Updateddate]      DATETIME       NULL,
    [UpdatedBy]        INT            NULL,
    [Itemgroupid]      INT            NULL,
    [Itemcategorydesc] NVARCHAR (200) NULL,
    CONSTRAINT [PK_M_ItemCategoryMaster] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_M_ItemCategoryMaster_M_ItemGroupMaster] FOREIGN KEY ([Itemgroupid]) REFERENCES [dbo].[M_ItemGroupMaster] ([Id])
);

