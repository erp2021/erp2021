﻿CREATE TABLE [dbo].[CRM_QuotationStyleSizeDetail] (
    [Id]               BIGINT          IDENTITY (1, 1) NOT NULL,
    [Quotationstyleid] BIGINT          NULL,
    [Sizetype]         NVARCHAR (40)   NULL,
    [Sizepcs]          DECIMAL (18, 2) NULL,
    [Sizeper]          DECIMAL (18, 2) NULL,
    [Createddate]      DATETIME        NULL,
    [Createdby]        INT             NULL,
    [Updateddate]      DATETIME        NULL,
    [Updatedby]        INT             NULL,
    [IsActive]         BIT             NULL,
    CONSTRAINT [PK_CRM_QuotationStyleSizeDetail] PRIMARY KEY CLUSTERED ([Id] ASC)
);

