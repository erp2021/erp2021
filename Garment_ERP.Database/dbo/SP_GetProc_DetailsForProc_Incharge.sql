﻿CREATE proc SP_GetProc_DetailsForProc_Incharge
@InchargeID int
as
Select PM.SHORT_NAME as ProcessName,SM.Stylename,WO.WorkOrderNo,WOPM.RequiredQty,WO.Date from MFG_WorkOrderProcessDetails WOPD
Inner join MFG_WORK_ORDER WO on WO.WorkOrderID=WOPD.WO_ID
Inner join M_PROCESS_Master PM on PM.Process_Id=WOPD.ProcessID
Inner join M_StyleMaster SM on SM.Id=WO.StyleID
Inner join MFG_WorkOrderProcessMaterials WOPM on WOPM.WOProcessID=WOPD.WOProcessID
where WOPD.ProcessInchargeID=@InchargeID and WOPD.IsActive=1
