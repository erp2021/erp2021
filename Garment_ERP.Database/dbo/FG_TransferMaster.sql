﻿CREATE TABLE [dbo].[FG_TransferMaster] (
    [Id]                 BIGINT        IDENTITY (1, 1) NOT NULL,
    [TransferNo]         NVARCHAR (50) NULL,
    [TransferDate]       DATETIME      NULL,
    [ProductionPlanId]   BIGINT        NULL,
    [ProductionPlanDate] DATETIME      NULL,
    [RefWorkOrderId]     BIGINT        NULL,
    [WarehouseId]        BIGINT        NULL,
    [RackId]             INT           NULL,
    [CreatedBy]          INT           NULL,
    [CreatedDate]        DATETIME      NULL,
    [UpdatedBy]          INT           NULL,
    [UpdatedDate]        DATETIME      NULL,
    [IsActive]           BIT           NULL,
    [Comment]            NVARCHAR (50) NULL,
    [DepartmentId]       INT           NULL,
    [RefWorkOrderDate]   DATETIME      NULL,
    CONSTRAINT [PK_FG_TransferMaster] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_FG_TransferMaster_M_DepartmentMaster] FOREIGN KEY ([DepartmentId]) REFERENCES [dbo].[M_DepartmentMaster] ([Id]),
    CONSTRAINT [FK_FG_TransferMaster_M_RackMaster] FOREIGN KEY ([RackId]) REFERENCES [dbo].[M_RackMaster] ([Id]),
    CONSTRAINT [FK_FG_TransferMaster_M_WAREHOUSE] FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[M_WAREHOUSE] ([ID]),
    CONSTRAINT [FK_FG_TransferMaster_MFG_WORK_ORDER] FOREIGN KEY ([RefWorkOrderId]) REFERENCES [dbo].[MFG_WORK_ORDER] ([WorkOrderID])
);

