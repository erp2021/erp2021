﻿CREATE TABLE [dbo].[MFG_WorkOrderProcessMaterials] (
    [WOProcessMaterial_ID] BIGINT       IDENTITY (1, 1) NOT NULL,
    [WOProcessID]          BIGINT       NULL,
    [ItemSubCategoryID]    BIGINT       NULL,
    [ItemID]               BIGINT       NULL,
    [UnitID]               INT          NULL,
    [RequiredQty]          DECIMAL (18) NULL,
    [IssuedQty]            DECIMAL (18) CONSTRAINT [DF_MFG_WorkOrderProcessMaterials_IssuedQty] DEFAULT ((0)) NULL,
    [ItemCheckedDate]      DATETIME     NULL,
    [ItemCheckedby]        INT          NULL,
    [WOStartDate]          DATETIME     NULL,
    [WOStartedBY]          INT          NULL,
    [WOEndDate]            DATETIME     NULL,
    [WOEndedBy]            INT          NULL,
    [IsActive]             BIT          NULL,
    CONSTRAINT [PK_MFG_WorkOrderProcessMaterials] PRIMARY KEY CLUSTERED ([WOProcessMaterial_ID] ASC),
    CONSTRAINT [FK_MFG_WorkOrderProcessMaterials_MFG_WorkOrderProcessDetails] FOREIGN KEY ([WOProcessID]) REFERENCES [dbo].[MFG_WorkOrderProcessDetails] ([WOProcessID])
);

