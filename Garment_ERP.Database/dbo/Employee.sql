﻿CREATE TABLE [dbo].[Employee] (
    [ID]         INT          IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (50) NULL,
    [Gender]     VARCHAR (50) NULL,
    [EmpID]      VARCHAR (50) NOT NULL,
    [Age]        INT          NULL,
    [CREATED_BY] NUMERIC (18) NULL,
    [CREATED_ON] DATETIME     NULL,
    [UPDATED_BY] NUMERIC (18) NULL,
    [UPDATED_ON] DATETIME     NULL,
    [IsActive]   BIT          NULL,
    CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED ([ID] ASC)
);

