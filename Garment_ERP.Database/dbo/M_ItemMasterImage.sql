﻿CREATE TABLE [dbo].[M_ItemMasterImage] (
    [Id]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [ItemId]      BIGINT         NULL,
    [image]       NVARCHAR (200) NULL,
    [Createddate] DATETIME       CONSTRAINT [DF_M_ItemMasterImage_Createddate] DEFAULT (getdate()) NULL,
    [Createdby]   INT            NULL,
    [Updateddate] DATETIME       NULL,
    [Updatedby]   INT            NULL,
    [IsActive]    BIT            NULL,
    CONSTRAINT [PK_M_ItemMasterImage] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_M_ItemMasterImage_M_ItemMaster] FOREIGN KEY ([ItemId]) REFERENCES [dbo].[M_ItemMaster] ([Id])
);

