﻿CREATE TABLE [dbo].[M_CustomerBankDetails] (
    [Id]          BIGINT        IDENTITY (1, 1) NOT NULL,
    [Customerid]  BIGINT        NULL,
    [Bankname]    NVARCHAR (50) NULL,
    [Accountno]   NVARCHAR (40) NULL,
    [Createddate] DATETIME      NULL,
    [Createdby]   NCHAR (10)    NULL,
    [Updateddate] DATETIME      NULL,
    [Updatedby]   INT           NULL,
    [IsActive]    BIT           NULL,
    [Bankcode]    NVARCHAR (20) NULL,
    CONSTRAINT [PK_M_CustomerBankDetails] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_M_CustomerBankDetails_M_CustomerMaster] FOREIGN KEY ([Customerid]) REFERENCES [dbo].[M_CustomerMaster] ([Id])
);

