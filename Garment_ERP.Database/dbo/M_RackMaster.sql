﻿CREATE TABLE [dbo].[M_RackMaster] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Rack]         NVARCHAR (50)  NULL,
    [Rackdesc]     NVARCHAR (100) NULL,
    [Createdate]   DATETIME       NULL,
    [Createdby]    INT            NULL,
    [Updatedate]   DATETIME       NULL,
    [Updatedby]    INT            NULL,
    [IsActive]     BIT            NULL,
    [DepartmentId] INT            NULL,
    [Comment]      NVARCHAR (255) NULL,
    [WarehouseId]  BIGINT         NULL,
    CONSTRAINT [PK_M_RackMaster] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_M_RackMaster_M_DepartmentMaster] FOREIGN KEY ([DepartmentId]) REFERENCES [dbo].[M_DepartmentMaster] ([Id]),
    CONSTRAINT [FK_M_RackMaster_M_WAREHOUSE] FOREIGN KEY ([WarehouseId]) REFERENCES [dbo].[M_WAREHOUSE] ([ID])
);

