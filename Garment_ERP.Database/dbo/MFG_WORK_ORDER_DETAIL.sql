﻿CREATE TABLE [dbo].[MFG_WORK_ORDER_DETAIL] (
    [ID]            BIGINT          IDENTITY (1, 1) NOT NULL,
    [WorkOrderID]   BIGINT          NOT NULL,
    [RefStyleID]    INT             NULL,
    [SizeID]        BIGINT          NULL,
    [Quantity]      NUMERIC (18, 2) NULL,
    [ProductionQty] DECIMAL (18)    NULL,
    [BalanceQty]    DECIMAL (18)    NULL,
    [UnitID]        INT             NULL,
    [IsActive]      BIT             NULL,
    CONSTRAINT [PK_P_WORK_ORDER_DETAIL] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_MFG_WORK_ORDER_DETAIL_M_ItemSizeMaster] FOREIGN KEY ([SizeID]) REFERENCES [dbo].[M_ItemSizeMaster] ([Id]),
    CONSTRAINT [FK_MFG_WORK_ORDER_DETAIL_M_UnitMaster] FOREIGN KEY ([UnitID]) REFERENCES [dbo].[M_UnitMaster] ([Id]),
    CONSTRAINT [FK_P_WORK_ORDER_DETAIL_P_WORK_ORDER] FOREIGN KEY ([WorkOrderID]) REFERENCES [dbo].[MFG_WORK_ORDER] ([WorkOrderID])
);

