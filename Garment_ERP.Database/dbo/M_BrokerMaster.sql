﻿CREATE TABLE [dbo].[M_BrokerMaster] (
    [Id]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [BrokerName]  NVARCHAR (60)  NULL,
    [Phone]       NVARCHAR (20)  NULL,
    [Address]     NVARCHAR (100) NULL,
    [Cityid]      INT            NULL,
    [Stateid]     INT            NULL,
    [Countryid]   INT            NULL,
    [Email]       NVARCHAR (20)  NULL,
    [Remark]      NVARCHAR (50)  NULL,
    [PANNo]       NVARCHAR (30)  NULL,
    [GSTNo]       NVARCHAR (50)  NULL,
    [Createddate] DATETIME       NULL,
    [CreatedBy]   INT            NULL,
    [Updateddate] DATETIME       NULL,
    [UpdatedBy]   INT            NULL,
    [IsActive]    BIT            NULL,
    CONSTRAINT [PK_M_BrokerMaster] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_M_BrokerMaster_M_BrokerMaster] FOREIGN KEY ([Countryid]) REFERENCES [dbo].[M_CountryMaster] ([ID]),
    CONSTRAINT [FK_M_BrokerMaster_M_CityMaster] FOREIGN KEY ([Cityid]) REFERENCES [dbo].[M_CityMaster] ([ID]),
    CONSTRAINT [FK_M_BrokerMaster_M_StateMaster] FOREIGN KEY ([Stateid]) REFERENCES [dbo].[M_StateMaster] ([ID])
);

