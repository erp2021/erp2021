﻿CREATE TABLE [dbo].[M_ProcessingStatus] (
    [Id]                   INT            IDENTITY (1, 1) NOT NULL,
    [ProcessingStatusName] NVARCHAR (80)  NULL,
    [ProcessingStatusdesc] NVARCHAR (100) NULL,
    [IsActive]             BIT            NULL,
    [Createddate]          DATETIME       NULL,
    [Createdby]            INT            NULL,
    [Updateddate]          DATETIME       NULL,
    [UpdatedBy]            INT            NULL,
    CONSTRAINT [PK_M_ProcessingStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);

