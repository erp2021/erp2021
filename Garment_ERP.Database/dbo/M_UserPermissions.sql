﻿CREATE TABLE [dbo].[M_UserPermissions] (
    [Id]       INT IDENTITY (1, 1) NOT NULL,
    [ModuleId] INT NOT NULL,
    [IsRead]   BIT NOT NULL,
    [IsCreate] BIT NOT NULL,
    [IsEdit]   BIT NOT NULL,
    [IsDelete] BIT NOT NULL,
    [UserId]   INT NULL,
    CONSTRAINT [PK_UserPermissions] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_M_UserPermissions_M_UserMaster] FOREIGN KEY ([UserId]) REFERENCES [dbo].[M_UserMaster] ([Id]),
    CONSTRAINT [FK_UserPermissions_ModuleMaster] FOREIGN KEY ([ModuleId]) REFERENCES [dbo].[M_ModuleMaster] ([ModuleId])
);

