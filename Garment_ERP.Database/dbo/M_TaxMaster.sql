﻿CREATE TABLE [dbo].[M_TaxMaster] (
    [Id]             INT            IDENTITY (1, 1) NOT NULL,
    [Taxname]        NVARCHAR (70)  NULL,
    [Percentage]     DECIMAL (18)   NULL,
    [Taxdescription] NVARCHAR (150) NULL,
    [Createddate]    DATETIME       NULL,
    [Createdby]      INT            NULL,
    [Updateddate]    DATETIME       NULL,
    [UpdatedBy]      INT            NULL,
    [IsActive]       BIT            NULL,
    CONSTRAINT [PK_M_TaxMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

