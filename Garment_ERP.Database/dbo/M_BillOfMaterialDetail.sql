﻿CREATE TABLE [dbo].[M_BillOfMaterialDetail] (
    [ID]                BIGINT          IDENTITY (1, 1) NOT NULL,
    [BOM_ID]            BIGINT          NOT NULL,
    [ItemSubCategoryId] INT             NULL,
    [ItemId]            BIGINT          NULL,
    [ItemDescription]   NVARCHAR (255)  NULL,
    [SupplierId]        BIGINT          NULL,
    [UnitId]            INT             NULL,
    [RequiredQty]       DECIMAL (18, 2) NULL,
    [CreatedBy]         INT             NULL,
    [CreatedDate]       DATETIME        NULL,
    [UpdatedBy]         INT             NULL,
    [UpdatedDate]       DATETIME        NULL,
    [IsActive]          BIT             NULL,
    [Comment]           NVARCHAR (255)  NULL,
    CONSTRAINT [PK_M_BillOfMaterialDetail] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_M_BillOfMaterialDetail_M_BillOfMaterialMaster] FOREIGN KEY ([BOM_ID]) REFERENCES [dbo].[M_BillOfMaterialMaster] ([BOM_ID]),
    CONSTRAINT [FK_M_BillOfMaterialDetail_M_ItemMaster] FOREIGN KEY ([ItemId]) REFERENCES [dbo].[M_ItemMaster] ([Id]),
    CONSTRAINT [FK_M_BillOfMaterialDetail_M_ItemSubCategoryMaster] FOREIGN KEY ([ItemSubCategoryId]) REFERENCES [dbo].[M_ItemSubCategoryMaster] ([Id]),
    CONSTRAINT [FK_M_BillOfMaterialDetail_M_SupplierMaster] FOREIGN KEY ([SupplierId]) REFERENCES [dbo].[M_SupplierMaster] ([Id]),
    CONSTRAINT [FK_M_BillOfMaterialDetail_M_UnitMaster] FOREIGN KEY ([UnitId]) REFERENCES [dbo].[M_UnitMaster] ([Id])
);

