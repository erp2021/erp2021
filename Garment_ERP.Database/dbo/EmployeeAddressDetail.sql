﻿CREATE TABLE [dbo].[EmployeeAddressDetail] (
    [Id]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [EmployeeId]  INT            NULL,
    [Addess]      NVARCHAR (200) NULL,
    [Cityid]      INT            NULL,
    [Stateid]     INT            NULL,
    [Countryid]   INT            NULL,
    [Createddate] DATETIME       NULL,
    [CreatedBy]   INT            NULL,
    [Updateddate] DATETIME       NULL,
    [Updatedby]   INT            NULL,
    [IsActive]    BIT            NULL,
    CONSTRAINT [PK_EmployeeAddressDetail] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_EmployeeAddressDetail_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employee] ([ID]),
    CONSTRAINT [FK_EmployeeAddressDetail_M_CityMaster] FOREIGN KEY ([Cityid]) REFERENCES [dbo].[M_CityMaster] ([ID]),
    CONSTRAINT [FK_EmployeeAddressDetail_M_CountryMaster] FOREIGN KEY ([Countryid]) REFERENCES [dbo].[M_CountryMaster] ([ID]),
    CONSTRAINT [FK_EmployeeAddressDetail_M_StateMaster] FOREIGN KEY ([Stateid]) REFERENCES [dbo].[M_StateMaster] ([ID])
);

