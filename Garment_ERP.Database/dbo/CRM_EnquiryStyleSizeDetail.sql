﻿CREATE TABLE [dbo].[CRM_EnquiryStyleSizeDetail] (
    [Id]             BIGINT          IDENTITY (1, 1) NOT NULL,
    [Enquirystyleid] BIGINT          NULL,
    [Sizetype]       NVARCHAR (40)   NULL,
    [SIzeper]        DECIMAL (18, 2) NULL,
    [Sizepcs]        DECIMAL (18, 2) NULL,
    [Createddate]    DATETIME        NULL,
    [CreatedBy]      INT             NULL,
    [Updateddate]    DATETIME        NULL,
    [IsActive]       BIT             NULL,
    CONSTRAINT [PK_CRM_EnquiryStyleSizeDetail] PRIMARY KEY CLUSTERED ([Id] ASC)
);

