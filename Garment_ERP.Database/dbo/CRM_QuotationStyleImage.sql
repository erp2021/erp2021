﻿CREATE TABLE [dbo].[CRM_QuotationStyleImage] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [Quotationimg] NVARCHAR (200) NULL,
    [Quotationid]  BIGINT         NULL,
    [Createdate]   DATETIME       NULL,
    [Createdby]    INT            NULL,
    [Updateddate]  DATETIME       NULL,
    [Updatedby]    INT            NULL,
    [IsActive]     BIT            NULL,
    CONSTRAINT [PK_CRM_QuotationStyleImage] PRIMARY KEY CLUSTERED ([Id] ASC)
);

