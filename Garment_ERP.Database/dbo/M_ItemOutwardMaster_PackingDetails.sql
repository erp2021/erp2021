﻿CREATE TABLE [dbo].[M_ItemOutwardMaster_PackingDetails] (
    [Pack_ID]           BIGINT          IDENTITY (1, 1) NOT NULL,
    [PackingType_ID]    INT             NULL,
    [Outward_ID]        BIGINT          NULL,
    [ItemSubCategoryID] INT             NULL,
    [Item_ID]           BIGINT          NULL,
    [UOM_ID]            INT             NULL,
    [ItemQty]           DECIMAL (18, 2) NULL,
    [IsActive]          BIT             NULL,
    CONSTRAINT [PK_M_ItemOutwardMaster_PackingDetails] PRIMARY KEY CLUSTERED ([Pack_ID] ASC)
);

