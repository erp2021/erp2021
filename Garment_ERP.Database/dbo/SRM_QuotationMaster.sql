﻿CREATE TABLE [dbo].[SRM_QuotationMaster] (
    [Id]            BIGINT          IDENTITY (1, 1) NOT NULL,
    [PR_No]         NVARCHAR (50)   NULL,
    [SupplierId]    BIGINT          NULL,
    [QuotationNo]   NVARCHAR (50)   NULL,
    [QuotationDate] DATETIME        NULL,
    [Validity]      DATETIME        NULL,
    [Narration]     NVARCHAR (200)  NULL,
    [CreatedBy]     INT             NULL,
    [CreatedDate]   DATETIME        NULL,
    [UpdatedBy]     INT             NULL,
    [UpdatedDate]   DATETIME        NULL,
    [IsActive]      BIT             NULL,
    [GSTTax]        DECIMAL (18, 2) NULL,
    [OtherTax]      DECIMAL (18, 2) NULL,
    [TotalCost]     DECIMAL (18, 2) NULL,
    [PR_Id]         BIGINT          NULL,
    CONSTRAINT [PK_SRM_QuotationMaster] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_SRM_QuotationMaster_M_SupplierMaster] FOREIGN KEY ([SupplierId]) REFERENCES [dbo].[M_SupplierMaster] ([Id]),
    CONSTRAINT [FK_SRM_QuotationMaster_SRM_PurchaseRequestMaster] FOREIGN KEY ([PR_Id]) REFERENCES [dbo].[SRM_PurchaseRequestMaster] ([Id])
);

