﻿CREATE TABLE [dbo].[M_MachineTypeMaster] (
    [Machine_TypeId]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [Machine_TypeName]        NVARCHAR (255) NULL,
    [Machine_TypeDescription] NVARCHAR (255) NULL,
    [IsActive]                BIT            NULL,
    CONSTRAINT [PK_M_MachineTypeMaster] PRIMARY KEY CLUSTERED ([Machine_TypeId] ASC)
);

