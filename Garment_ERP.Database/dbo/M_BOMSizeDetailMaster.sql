﻿CREATE TABLE [dbo].[M_BOMSizeDetailMaster] (
    [Id]       BIGINT          IDENTITY (1, 1) NOT NULL,
    [BOMD_Id]  BIGINT          NULL,
    [SizeId]   BIGINT          NULL,
    [Quantity] DECIMAL (18, 2) NULL,
    [IsActive] BIT             NULL,
    CONSTRAINT [PK_M_BOMSizeDetailMaster] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_M_BOMSizeDetailMaster_M_BillOfMaterialDetail] FOREIGN KEY ([BOMD_Id]) REFERENCES [dbo].[M_BillOfMaterialDetail] ([ID]),
    CONSTRAINT [FK_M_BOMSizeDetailMaster_M_ItemSizeMaster] FOREIGN KEY ([SizeId]) REFERENCES [dbo].[M_ItemSizeMaster] ([Id])
);

