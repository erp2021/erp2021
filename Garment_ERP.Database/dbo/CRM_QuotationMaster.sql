﻿CREATE TABLE [dbo].[CRM_QuotationMaster] (
    [Id]             BIGINT          IDENTITY (1, 1) NOT NULL,
    [EnquiryID]      BIGINT          NOT NULL,
    [Enquiryrefno]   VARCHAR (50)    NULL,
    [QuotationNo]    VARCHAR (50)    NULL,
    [Quotationdate]  DATETIME        NULL,
    [customerid]     BIGINT          NULL,
    [narration]      VARCHAR (100)   NULL,
    [Approvalstatus] BIT             NULL,
    [Approvaldate]   DATETIME        NULL,
    [Currencyid]     INT             NULL,
    [Exchangerate]   DECIMAL (18, 2) NULL,
    [Costprice]      DECIMAL (18, 2) NULL,
    [SalePrice]      DECIMAL (18, 2) NULL,
    [Tax]            DECIMAL (18, 2) NULL,
    [Subtotal]       DECIMAL (18, 2) NULL,
    [Dateofcosting]  DATETIME        NULL,
    [Comment]        VARCHAR (100)   NULL,
    [Createddate]    DATETIME        NULL,
    [CreatedBy]      INT             NULL,
    [Updateddate]    DATETIME        NULL,
    [UpdatedBy]      INT             NULL,
    [IsActive]       BIT             NULL,
    [Tentivedate]    DATETIME        NULL,
    [Sampling]       BIT             NULL,
    [Total]          DECIMAL (18, 2) NULL,
    [Addressid]      BIGINT          NULL,
    CONSTRAINT [PK_CRM_QuotationMaster] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CRM_QuotationMaster_CRM_EnquiryMaster] FOREIGN KEY ([EnquiryID]) REFERENCES [dbo].[CRM_EnquiryMaster] ([Id]),
    CONSTRAINT [FK_CRM_QuotationMaster_M_CustomerMaster] FOREIGN KEY ([customerid]) REFERENCES [dbo].[M_CustomerMaster] ([Id])
);

