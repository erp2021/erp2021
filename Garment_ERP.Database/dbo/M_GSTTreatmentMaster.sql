﻿CREATE TABLE [dbo].[M_GSTTreatmentMaster] (
    [Id]            INT           IDENTITY (1, 1) NOT NULL,
    [TreatmentName] NVARCHAR (50) NULL,
    [IsEnable]      BIT           NULL,
    [CreatedDate]   DATETIME      NULL,
    [UpdatedDate]   DATETIME      NULL,
    CONSTRAINT [PK_GSTTreatmentMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

