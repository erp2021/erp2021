﻿CREATE TABLE [dbo].[MFG_PRODUCTION_DAILY_PLAN_DETAILS] (
    [ID]           BIGINT          IDENTITY (1, 1) NOT NULL,
    [DailyPlanID]  BIGINT          NOT NULL,
    [SizeID]       BIGINT          NOT NULL,
    [Target_Qty]   NUMERIC (18, 2) NULL,
    [Produced_Qty] NUMERIC (18, 2) NULL,
    [CreatedDate]  DATETIME        NULL,
    [CreatedBy]    INT             NULL,
    CONSTRAINT [PK_MFG_PRODUCTION_DAILY_PLAN_DETAILS] PRIMARY KEY CLUSTERED ([ID] ASC)
);

