﻿CREATE TABLE [dbo].[M_SHIFT] (
    [ID]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [CODE]           VARCHAR (20)   NULL,
    [SHORT_NAME]     VARCHAR (50)   NULL,
    [START_TIME]     TIME (7)       NULL,
    [END_TIME]       TIME (7)       NULL,
    [EFFECTIVE_DATE] DATETIME       NULL,
    [IS_ENABLED]     BIT            NULL,
    [CREATED_BY]     INT            NULL,
    [CREATED_ON]     DATETIME       NULL,
    [UPDATED_BY]     INT            NULL,
    [UPDATED_ON]     DATETIME       NULL,
    [DepartmentId]   INT            NULL,
    [Comment]        NVARCHAR (255) NULL,
    CONSTRAINT [PK_M_SHIFT] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_M_SHIFT_M_DepartmentMaster] FOREIGN KEY ([DepartmentId]) REFERENCES [dbo].[M_DepartmentMaster] ([Id])
);

