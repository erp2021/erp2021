﻿CREATE TABLE [dbo].[MFG_PRODUCTION_PLAN_DETAILS] (
    [ID]         BIGINT          IDENTITY (1, 1) NOT NULL,
    [PlanID]     BIGINT          NOT NULL,
    [StyleID]    BIGINT          NOT NULL,
    [SizeID]     BIGINT          NULL,
    [MachineID]  BIGINT          NULL,
    [Target_Qty] NUMERIC (18, 2) NULL,
    [Actual_Qty] NUMERIC (18, 2) NULL,
    [UnitID]     INT             NULL,
    CONSTRAINT [PK_P_PRODUCTION_PLAN_DETAILS] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_MFG_PRODUCTION_PLAN_DETAILS_M_ItemSizeMaster] FOREIGN KEY ([SizeID]) REFERENCES [dbo].[M_ItemSizeMaster] ([Id]),
    CONSTRAINT [FK_MFG_PRODUCTION_PLAN_DETAILS_M_StyleMaster] FOREIGN KEY ([StyleID]) REFERENCES [dbo].[M_StyleMaster] ([Id]),
    CONSTRAINT [FK_MFG_PRODUCTION_PLAN_DETAILS_M_UnitMaster] FOREIGN KEY ([UnitID]) REFERENCES [dbo].[M_UnitMaster] ([Id]),
    CONSTRAINT [FK_MFG_PRODUCTION_PLAN_DETAILS_MachineMaster] FOREIGN KEY ([MachineID]) REFERENCES [dbo].[MachineMaster] ([Id]),
    CONSTRAINT [FK_MFG_PRODUCTION_PLAN_DETAILS_MFG_PRODUCTION_PLAN] FOREIGN KEY ([PlanID]) REFERENCES [dbo].[MFG_PRODUCTION_PLAN] ([ID])
);

