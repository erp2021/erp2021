﻿CREATE TABLE [dbo].[T_PURCHASE_ORDER_LOGISTIC] (
    [ID]                   NUMERIC (18)    IDENTITY (1, 1) NOT NULL,
    [PO_ID]                NUMERIC (18)    NOT NULL,
    [WREHOUSE_ID]          NUMERIC (18)    NULL,
    [PROVIDER_TYPE_ID]     NUMERIC (18)    NULL,
    [PROVIDER_ID]          NUMERIC (18)    NULL,
    [SERVICE_TYPE_ID]      NUMERIC (18)    NULL,
    [PRICE_TYPE_ID]        NUMERIC (18)    NULL,
    [SUPPLIER_CONTRACT_ID] NUMERIC (18)    NULL,
    [RATE_INDICATOR]       NUMERIC (18)    NULL,
    [RATE]                 NUMERIC (18, 6) NULL,
    CONSTRAINT [PK_T_PURCHASE_ORDER_LOGISTIC] PRIMARY KEY CLUSTERED ([ID] ASC)
);

