﻿CREATE TABLE [dbo].[FG_TransferDetail] (
    [Id]            BIGINT          IDENTITY (1, 1) NOT NULL,
    [FG_TransferId] BIGINT          NULL,
    [SizeId]        BIGINT          NULL,
    [WorkOrderQty]  DECIMAL (18, 2) NULL,
    [ProducedQty]   DECIMAL (18, 2) NULL,
    [BalanceQty]    DECIMAL (18, 2) NULL,
    [CreatedBy]     INT             NULL,
    [CreatedDate]   DATETIME        NULL,
    [UpdatedBy]     INT             NULL,
    [UpdatedDate]   DATETIME        NULL,
    [IsActive]      BIT             NULL,
    [IsQC_Done]     CHAR (10)       NULL,
    CONSTRAINT [PK_FG_TransferDetail] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_FG_TransferDetail_FG_TransferMaster] FOREIGN KEY ([FG_TransferId]) REFERENCES [dbo].[FG_TransferMaster] ([Id]),
    CONSTRAINT [FK_FG_TransferDetail_M_ItemSizeMaster] FOREIGN KEY ([SizeId]) REFERENCES [dbo].[M_ItemSizeMaster] ([Id])
);

