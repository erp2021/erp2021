﻿CREATE TABLE [dbo].[T_PURCHASE_ORDER_ITEM_TAX] (
    [ID]                       NUMERIC (18)    IDENTITY (1, 1) NOT NULL,
    [PURCHASE_ORDER_ITEM_ID]   NUMERIC (18)    NOT NULL,
    [SEQ_NO]                   INT             NULL,
    [TAX_NAME]                 VARCHAR (50)    NULL,
    [SURCHARGE_FLAG]           CHAR (1)        NULL,
    [TAXRATE_ID]               NUMERIC (18)    NULL,
    [TAXABLE_AMOUNT_SOURCE_ID] NUMERIC (18)    NULL,
    [TAXABLE_AMOUNT]           NUMERIC (18, 6) NULL,
    [TAX_RATE]                 NUMERIC (18, 6) NULL,
    [TAX_AMOUNT]               NUMERIC (18, 6) NULL,
    CONSTRAINT [PK_T_PURCHASE_ORDER_ITEM_TAX] PRIMARY KEY CLUSTERED ([ID] ASC)
);

