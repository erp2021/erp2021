﻿CREATE TABLE [dbo].[M_ItemColorMaster] (
    [Id]            INT           IDENTITY (1, 1) NOT NULL,
    [ItemColor]     NVARCHAR (50) NULL,
    [ItemColordesc] NVARCHAR (80) NULL,
    [Createdate]    DATETIME      NULL,
    [Createdby]     INT           NULL,
    [Updateddate]   DATETIME      NULL,
    [Updatedby]     INT           NULL,
    [IsActive]      BIT           NULL,
    CONSTRAINT [PK_M_ItemColorMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

