﻿CREATE TABLE [dbo].[M_PackingTypeMaster] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [Packingtype]     NVARCHAR (50)  NULL,
    [Packingtypedesc] NVARCHAR (100) NULL,
    [Createddate]     DATETIME       NULL,
    [Createdby]       INT            NULL,
    [Updateddate]     DATETIME       NULL,
    [Updatedby]       INT            NULL,
    [IsActive]        BIT            NULL,
    CONSTRAINT [PK_M_PackingTypeMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

