﻿create proc SP_GetWO_ProcessMaterialDetails
as
select WOPM.WOProcessID, WOPM.WOProcessMaterial_ID,ISM.Itemsubcategory,
IM.ItemName,UM.ItemUnit,WOPM.IssuedQty,WOPM.RequiredQty,WOPM.IsActive,WOPM.ItemCheckedby,
WOPM.ItemCheckedDate,WOPM.WOEndDate,WOPM.WOEndedBy,WOPM.WOStartDate,WOPM.WOStartedBY
 from MFG_WorkOrderProcessMaterials WOPM
-- Inner Join MFG_WorkOrderProcessDetails WOPD on WOPD.ProcessID=WOPM.WOProcessID
 Inner JOin M_ItemMaster IM on IM.Id=WOPM.ItemID
 Inner Join M_ItemSubCategoryMaster ISM on ISM.Id= WOPM.ItemSubCategoryID
 Inner Join M_UnitMaster UM on UM.Id =WOPM.UnitID

