﻿CREATE TABLE [dbo].[M_ItemOutwardMaster] (
    [ID]                BIGINT       IDENTITY (1, 1) NOT NULL,
    [Inward_ID]         BIGINT       NULL,
    [MI_ID]             BIGINT       NULL,
    [Process_ID]        NUMERIC (18) NULL,
    [Sale_ID]           BIGINT       NULL,
    [ItemSubCategoryID] INT          NULL,
    [Item_ID]           BIGINT       NULL,
    [UOM]               INT          NULL,
    [ItemQty]           DECIMAL (18) NULL,
    [OutwardBy]         INT          NULL,
    [OutwardDate]       DATETIME     NULL,
    [UpdatedBy]         INT          NULL,
    [UpdatedDate]       DATETIME     NULL,
    [IsActive]          BIT          NULL,
    CONSTRAINT [PK_M_ItemOutwardMaster] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_M_ItemOutwardMaster_M_ItemInwardMaster] FOREIGN KEY ([Inward_ID]) REFERENCES [dbo].[M_ItemInwardMaster] ([ID]),
    CONSTRAINT [FK_M_ItemOutwardMaster_M_ItemSubCategoryMaster] FOREIGN KEY ([ItemSubCategoryID]) REFERENCES [dbo].[M_ItemSubCategoryMaster] ([Id]),
    CONSTRAINT [FK_M_ItemOutwardMaster_M_PROCESS_Master] FOREIGN KEY ([Process_ID]) REFERENCES [dbo].[M_PROCESS_Master] ([Process_Id])
);

