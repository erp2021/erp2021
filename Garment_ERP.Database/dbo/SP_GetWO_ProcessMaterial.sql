﻿CREATE proc [dbo].[SP_GetWO_ProcessMaterial]
@workorderID bigint
as
select distinct MWOPRD.WOProcessID, ISCM.Id ItemSubCategory, BOMD.Itemid, BOMD.RequiredQty  * MWO.RequiredQty ProcessRequiredQty, 0.0 IssuedQty, BOMD.UnitId ,pct.Processsequence 
--, ICM.Itemcategorydesc, pm.Process_Description, PCM.Process_cycle_Name, pct.ItemcategoryIn, pct.ItemcategoryOut 
from  MFG_WORK_ORDER MWO
inner join MFG_WorkOrderProcessDetails MWOPRD on MWOPRD.WO_ID = @workorderID
inner join M_ProcessCycleMaster PCM on PCM.Prooce_Cycle_id = MWO.ProcessCycleID
inner join ProcessCycleTransaction PCT on PCT.Processcycleid = PCM.Prooce_Cycle_id
inner join M_PROCESS_Master PM on PM.Process_Id  = MWOPRD.ProcessID and pm.Process_Id = PCt.Processid 
inner join CRM_EnquiryMaster EM on EM.Id = MWO.EnquiryID
inner join CRM_EnquiryRawitemDetail ERM on ERM.Enquiryid = MWO.EnquiryID and EM.Id = ERM.Enquiryid
inner join M_BillOfMaterialMaster BOM on BOM.EnquiryId = EM.Id
inner join M_BillOfMaterialDetail BOMD on BOMD.BOM_ID = BOM.BOM_ID and BOMD.ItemSubCategoryId = ERM.Itemsubcategoryid 
inner join M_ItemCategoryMaster ICM on ICM.Id = PCT.ItemcategoryIn and ICM.Id = ERM.Itemcategoryid 
inner join M_ItemSubCategoryMaster ISCM on Iscm.itemcategoryid = ICM.Id and BOMD.ItemSubCategoryId = ISCM.Id
order by pct.Processsequence 
--where ICM.Itemcategorydesc is not null


--select * from M_BillOfMaterialDetail where Enquiryid = 2

--select * from M_ItemMaster where id = 6