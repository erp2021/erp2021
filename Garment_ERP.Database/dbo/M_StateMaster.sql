﻿CREATE TABLE [dbo].[M_StateMaster] (
    [ID]        INT           IDENTITY (1, 1) NOT NULL,
    [Name]      NVARCHAR (50) NULL,
    [CountryID] INT           NULL,
    [StateCode] INT           NULL,
    CONSTRAINT [PK_M_StateMaster] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_M_StateMaster_M_CountryMaster] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[M_CountryMaster] ([ID])
);

