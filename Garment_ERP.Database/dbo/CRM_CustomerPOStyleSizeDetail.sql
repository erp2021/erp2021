﻿CREATE TABLE [dbo].[CRM_CustomerPOStyleSizeDetail] (
    [Id]          BIGINT          IDENTITY (1, 1) NOT NULL,
    [POstyleid]   BIGINT          NULL,
    [Sizetype]    NVARCHAR (40)   NULL,
    [SIzepcs]     DECIMAL (18, 2) NULL,
    [Sizeper]     DECIMAL (18, 2) NULL,
    [Createddate] DATETIME        NULL,
    [Createdby]   INT             NULL,
    [Updateddate] DATETIME        NULL,
    [Updatedby]   INT             NULL,
    [IsActive]    BIT             NULL,
    [Sizeid]      BIGINT          NULL,
    CONSTRAINT [PK_CRM_CustomerPOStyleSizeDetail] PRIMARY KEY CLUSTERED ([Id] ASC)
);

