﻿CREATE TABLE [dbo].[M_PaymentTermMaster] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [PaymentTerm]    NVARCHAR (50) NULL,
    [PaymenTermdesc] NVARCHAR (80) NULL,
    [Createdate]     DATETIME      NULL,
    [Createdby]      INT           NULL,
    [Updateddate]    DATETIME      NULL,
    [Updatedby]      INT           NULL,
    [IsActive]       BIT           NULL,
    CONSTRAINT [PK_M_PaymentTermMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

