﻿CREATE TABLE [dbo].[M_WorkCenterMaster] (
    [Id]                 INT            IDENTITY (1, 1) NOT NULL,
    [WorkCenterName]     NVARCHAR (50)  NULL,
    [WorkCenterLocation] NVARCHAR (255) NULL,
    [DepartmentId]       INT            NULL,
    [Comment]            NVARCHAR (255) NULL,
    [CreatedBy]          INT            NULL,
    [CreatedDate]        DATETIME       NULL,
    [UpdatedBy]          INT            NULL,
    [UpdatedDate]        DATETIME       NULL,
    [IsActive]           BIT            NULL,
    CONSTRAINT [PK_M_WorkCycleMaster] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_M_WorkCenterMaster_M_DepartmentMaster] FOREIGN KEY ([DepartmentId]) REFERENCES [dbo].[M_DepartmentMaster] ([Id])
);

