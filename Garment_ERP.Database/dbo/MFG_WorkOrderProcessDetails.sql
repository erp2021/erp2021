﻿CREATE TABLE [dbo].[MFG_WorkOrderProcessDetails] (
    [WOProcessID]       BIGINT       IDENTITY (1, 1) NOT NULL,
    [WO_ID]             BIGINT       NULL,
    [ProcessID]         NUMERIC (18) NULL,
    [ProcessInchargeID] INT          NULL,
    [CreatedBy]         BIGINT       NULL,
    [CreatedDate]       DATETIME     NULL,
    [UpdatedBy]         BIGINT       NULL,
    [UpdatedDate]       DATETIME     NULL,
    [IsActive]          BIT          NULL,
    CONSTRAINT [PK_MFG_WorkOrderProcessDetails] PRIMARY KEY CLUSTERED ([WOProcessID] ASC),
    CONSTRAINT [FK_MFG_WorkOrderProcessDetails_Employee] FOREIGN KEY ([ProcessInchargeID]) REFERENCES [dbo].[Employee] ([ID]),
    CONSTRAINT [FK_MFG_WorkOrderProcessDetails_M_PROCESS_Master] FOREIGN KEY ([ProcessID]) REFERENCES [dbo].[M_PROCESS_Master] ([Process_Id]),
    CONSTRAINT [FK_MFG_WorkOrderProcessDetails_MFG_WORK_ORDER] FOREIGN KEY ([WO_ID]) REFERENCES [dbo].[MFG_WORK_ORDER] ([WorkOrderID])
);

