﻿ Create procedure [dbo].[SP_GetDashboard]
 as
 begin
 declare @inquiries numeric(18,0),        
		 @salesorders numeric(18,0),
		 @supplierpo numeric(18,0),
		 @workorders numeric(18,0)

select @inquiries=(select count(Id) from CRM_EnquiryMaster where IsActive=1)
select @salesorders=(select count(Id) from CRM_CustomerPOMaster where IsActive=1)
select @supplierpo=(select count(Id) from SRM_PurchaseOrderMaster where IsActive=1)
select @workorders=(select count(WorkOrderID) from MFG_WORK_ORDER where IsActive=1)

select @inquiries as inquiries,@salesorders as salesorders,@supplierpo as supplierpo,@workorders as workorders  
end