﻿CREATE TABLE [dbo].[M_ItemGroupMaster] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [Itemgroup]     NVARCHAR (100) NULL,
    [IsActive]      BIT            NULL,
    [Itemgroupdesc] NVARCHAR (200) NULL,
    [Createddate]   DATETIME       NULL,
    [Createdby]     INT            NULL,
    [Updateddate]   DATETIME       NULL,
    [Updatedby]     INT            NULL,
    CONSTRAINT [PK_M_ItemGroupMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

