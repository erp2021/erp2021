﻿CREATE TABLE [dbo].[SRM_PurchaseRequestItemDetail] (
    [Id]             BIGINT          IDENTITY (1, 1) NOT NULL,
    [PR_ID]          BIGINT          NULL,
    [ItemCategoryId] INT             NULL,
    [ItemId]         BIGINT          NULL,
    [RequiredQty]    DECIMAL (18, 2) NULL,
    [ItemDesc]       NVARCHAR (255)  NULL,
    [RequiredDate]   DATETIME        NULL,
    [UnitId]         INT             NULL,
    [Comment]        NVARCHAR (255)  NULL,
    [CreatedBy]      INT             NULL,
    [CreatedDate]    DATETIME        NULL,
    [UpdatedBy]      INT             NULL,
    [UpdatedDate]    DATETIME        NULL,
    [IsActive]       BIT             NULL,
    CONSTRAINT [PK_SRM_PurchaseRequestItemDetail] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_SRM_PurchaseRequestItemDetail_M_ItemMaster] FOREIGN KEY ([ItemId]) REFERENCES [dbo].[M_ItemMaster] ([Id]),
    CONSTRAINT [FK_SRM_PurchaseRequestItemDetail_M_ItemSubCategoryMaster] FOREIGN KEY ([ItemCategoryId]) REFERENCES [dbo].[M_ItemSubCategoryMaster] ([Id]),
    CONSTRAINT [FK_SRM_PurchaseRequestItemDetail_M_UnitMaster] FOREIGN KEY ([UnitId]) REFERENCES [dbo].[M_UnitMaster] ([Id]),
    CONSTRAINT [FK_SRM_PurchaseRequestItemDetail_SRM_PurchaseRequestMaster] FOREIGN KEY ([PR_ID]) REFERENCES [dbo].[SRM_PurchaseRequestMaster] ([Id])
);

