﻿CREATE TABLE [dbo].[MFG_PRODUCTION_DAILY_PLAN] (
    [ID]                 BIGINT          IDENTITY (1, 1) NOT NULL,
    [MonthlyPlanID]      BIGINT          NOT NULL,
    [ProductionDate]     DATETIME        NOT NULL,
    [StyleID]            BIGINT          NULL,
    [Daily_Target_Qty]   NUMERIC (18, 2) NULL,
    [Daily_Produced_Qty] NUMERIC (18, 2) NULL,
    [UnitID]             INT             NULL,
    [MachineID]          BIGINT          NULL,
    [WorkCenterID]       BIGINT          NULL,
    [ToolID]             BIGINT          NULL,
    [OperatorID]         INT             NULL,
    [DepartmentID]       INT             NULL,
    [Inspection]         INT             NULL,
    [QC]                 INT             NULL,
    [CreatedDate]        DATETIME        NULL,
    [CreatedBy]          INT             NULL,
    CONSTRAINT [PK_MFG_PRODUCTION_DAILY_PLAN] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_MFG_PRODUCTION_DAILY_PLAN_M_StyleMaster] FOREIGN KEY ([StyleID]) REFERENCES [dbo].[M_StyleMaster] ([Id]),
    CONSTRAINT [FK_MFG_PRODUCTION_DAILY_PLAN_M_UnitMaster] FOREIGN KEY ([UnitID]) REFERENCES [dbo].[M_UnitMaster] ([Id]),
    CONSTRAINT [FK_MFG_PRODUCTION_DAILY_PLAN_MachineMaster] FOREIGN KEY ([MachineID]) REFERENCES [dbo].[MachineMaster] ([Id]),
    CONSTRAINT [FK_MFG_PRODUCTION_DAILY_PLAN_MFG_PRODUCTION_PLAN] FOREIGN KEY ([MonthlyPlanID]) REFERENCES [dbo].[MFG_PRODUCTION_PLAN] ([ID])
);

