﻿CREATE TABLE [dbo].[M_StyleMaster] (
    [Id]               BIGINT         IDENTITY (1, 1) NOT NULL,
    [StyleNo]          NVARCHAR (50)  NULL,
    [Stylename]        NVARCHAR (100) NULL,
    [StyleShortname]   NVARCHAR (200) NULL,
    [Styledescription] NVARCHAR (200) NULL,
    [Createddate]      DATETIME       NULL,
    [Createdby]        INT            NULL,
    [Updateddate]      DATETIME       NULL,
    [UpdatedBy]        INT            NULL,
    [IsActive]         BIT            NULL,
    CONSTRAINT [PK_M_StyleMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

