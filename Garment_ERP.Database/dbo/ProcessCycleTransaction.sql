﻿CREATE TABLE [dbo].[ProcessCycleTransaction] (
    [Id]              BIGINT         IDENTITY (1, 1) NOT NULL,
    [Processcycleid]  INT            NULL,
    [Processid]       NUMERIC (18)   NULL,
    [Processsequence] INT            NULL,
    [ItemcategoryIn]  BIGINT         NULL,
    [ItemcategoryOut] BIGINT         NULL,
    [Createddate]     DATETIME       NULL,
    [Createdby]       INT            NULL,
    [Updateddate]     DATETIME       NULL,
    [Updatedby]       INT            NULL,
    [IsActive]        BIT            NULL,
    [comment]         NVARCHAR (200) NULL,
    CONSTRAINT [PK_ProcessCycleTransaction] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ProcessCycleTransaction_M_ItemCategoryMaster] FOREIGN KEY ([ItemcategoryIn]) REFERENCES [dbo].[M_ItemCategoryMaster] ([Id]),
    CONSTRAINT [FK_ProcessCycleTransaction_M_ItemCategoryMaster1] FOREIGN KEY ([ItemcategoryOut]) REFERENCES [dbo].[M_ItemCategoryMaster] ([Id]),
    CONSTRAINT [FK_ProcessCycleTransaction_M_PROCESS_Master] FOREIGN KEY ([Processid]) REFERENCES [dbo].[M_PROCESS_Master] ([Process_Id])
);

