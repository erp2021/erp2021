﻿CREATE TABLE [dbo].[M_UserTypeMaster] (
    [Id]       INT           IDENTITY (1, 1) NOT NULL,
    [UserType] NVARCHAR (30) NULL,
    [IsActive] BIT           NULL,
    CONSTRAINT [PK_M_UserTypeMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

