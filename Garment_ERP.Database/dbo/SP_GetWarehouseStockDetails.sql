﻿
CREATE procedure [dbo].[SP_GetWarehouseStockDetails]
as 
begin
  select WI.SHORT_NAME,
  IC.Id,
 IC.Itemcategory,
 GI.ItemId,
 II.ItemName,
 Sum(GRN_Qty) as Accepted_Qty,
 IU.ItemUnit from SRM_GRNInward I
 INNER JOIN SRM_GRNItem GI ON I.ID=GI.ItemInwardId
 INNER JOIN M_ItemMaster II ON II.Id=GI.ItemId
 INNER JOIN M_WAREHOUSE WI ON WI.ID=I.WarehouseId
 INNER JOIN M_ItemCategoryMaster IC ON IC.Id=II.ItemCategoryid
 INNER JOIN M_UnitMaster IU ON IU.Id=II.Unit Group By GI.ItemId, WI.SHORT_NAME,IC.Id,IC.Itemcategory,GI.ItemId,II.ItemName,IU.ItemUnit

 END
