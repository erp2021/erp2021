﻿CREATE TABLE [dbo].[M_ContactAddressMaster] (
    [Id]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [customerid]  BIGINT         NULL,
    [Addess]      NVARCHAR (200) NULL,
    [Cityid]      INT            NULL,
    [Stateid]     INT            NULL,
    [Countryid]   INT            NULL,
    [Createddate] DATETIME       NULL,
    [CreatedBy]   INT            NULL,
    [Updateddate] DATETIME       NULL,
    [Updatedby]   INT            NULL,
    [IsActive]    BIT            NULL,
    CONSTRAINT [PK_M_ContactAddressMaster] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_M_ContactAddressMaster_M_CityMaster] FOREIGN KEY ([Cityid]) REFERENCES [dbo].[M_CityMaster] ([ID]),
    CONSTRAINT [FK_M_ContactAddressMaster_M_StateMaster] FOREIGN KEY ([Stateid]) REFERENCES [dbo].[M_StateMaster] ([ID])
);

