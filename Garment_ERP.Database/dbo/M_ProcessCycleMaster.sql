﻿CREATE TABLE [dbo].[M_ProcessCycleMaster] (
    [Prooce_Cycle_id]    INT            IDENTITY (1, 1) NOT NULL,
    [Process_cycle_Name] NVARCHAR (255) NULL,
    [IsActive]           BIT            NULL,
    [Processcysledesc]   NVARCHAR (300) NULL,
    [Createddate]        DATETIME       NULL,
    [Createdby]          INT            NULL,
    [Updateddate]        DATETIME       NULL,
    [Updatedby]          INT            NULL,
    CONSTRAINT [PK_M_ProcessCycleMaster] PRIMARY KEY CLUSTERED ([Prooce_Cycle_id] ASC)
);

