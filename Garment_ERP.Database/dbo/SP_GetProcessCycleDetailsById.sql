﻿
Create Proc SP_GetProcessCycleDetailsById
@ProcessCycleId bigint
as
begin
Select PCM.Process_cycle_Name,PCM.Processcysledesc,
PCT.Id as Proc_Tran_ID, PCT.Processid,PCT.Processsequence,PCT.ItemcategoryOut,
MPC.Id as Proc_Tran_DetailId,MPC.ProCycTran_ID,MPC.ItemcategoryIn,
PM.SHORT_NAME as ProcessName,
IICM.Itemcategory as OutCategory,OICM.Itemcategory as InCategory
 from M_ProcessCycleMaster PCM 
Inner join ProcessCycleTransaction PCT on PCT.Processcycleid=13
Inner join M_ProcessInCategoryDetails MPC on MPC.ProCycTran_ID=PCT.Id
Inner join M_PROCESS_Master PM on PM.Process_Id=PCT.Processid
Inner join M_ItemCategoryMaster IICM on IICM.Id=PCT.ItemcategoryOut 
Inner join M_ItemCategoryMaster OICM on OICM.Id=MPC.ItemcategoryIn
end