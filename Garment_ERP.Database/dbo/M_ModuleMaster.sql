﻿CREATE TABLE [dbo].[M_ModuleMaster] (
    [ModuleId]   INT          IDENTITY (1, 1) NOT NULL,
    [ModuleName] VARCHAR (50) NULL,
    CONSTRAINT [PK_ModuleMaster] PRIMARY KEY CLUSTERED ([ModuleId] ASC)
);

