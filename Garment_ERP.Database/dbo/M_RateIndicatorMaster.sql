﻿CREATE TABLE [dbo].[M_RateIndicatorMaster] (
    [Id]                INT            IDENTITY (1, 1) NOT NULL,
    [RateIndicatorName] NVARCHAR (50)  NULL,
    [RateIndicatorDesc] NVARCHAR (200) NULL,
    [ShortName]         NVARCHAR (50)  NULL,
    [CreatedBy]         INT            NULL,
    [CreatedDate]       DATETIME       NULL,
    [UpdatedBy]         INT            NULL,
    [UpdatedDate]       DATETIME       NULL,
    [IsActive]          BIT            NULL,
    CONSTRAINT [PK_M_RateIndicatorMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

