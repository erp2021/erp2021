﻿CREATE TABLE [dbo].[MFG_ALLOCATION] (
    [ID]           BIGINT          IDENTITY (1, 1) NOT NULL,
    [PlanID]       BIGINT          NOT NULL,
    [WorkOrderID]  BIGINT          NOT NULL,
    [StyleID]      BIGINT          NULL,
    [SizeID]       BIGINT          NULL,
    [RM_ItemID]    BIGINT          NULL,
    [AllocatedQty] NUMERIC (18, 2) NULL,
    [UnitID]       INT             NULL,
    [Approved]     INT             NULL,
    CONSTRAINT [PK_P_ALLOCATION] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_MFG_ALLOCATION_M_ItemMaster] FOREIGN KEY ([RM_ItemID]) REFERENCES [dbo].[M_ItemMaster] ([Id]),
    CONSTRAINT [FK_MFG_ALLOCATION_M_ItemSizeMaster] FOREIGN KEY ([SizeID]) REFERENCES [dbo].[M_ItemSizeMaster] ([Id]),
    CONSTRAINT [FK_MFG_ALLOCATION_M_StyleMaster] FOREIGN KEY ([StyleID]) REFERENCES [dbo].[M_StyleMaster] ([Id]),
    CONSTRAINT [FK_MFG_ALLOCATION_M_UnitMaster] FOREIGN KEY ([UnitID]) REFERENCES [dbo].[M_UnitMaster] ([Id]),
    CONSTRAINT [FK_MFG_ALLOCATION_MFG_PRODUCTION_PLAN] FOREIGN KEY ([PlanID]) REFERENCES [dbo].[MFG_PRODUCTION_PLAN] ([ID]),
    CONSTRAINT [FK_MFG_ALLOCATION_MFG_WORK_ORDER] FOREIGN KEY ([WorkOrderID]) REFERENCES [dbo].[MFG_WORK_ORDER] ([WorkOrderID])
);

