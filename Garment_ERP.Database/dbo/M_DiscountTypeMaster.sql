﻿CREATE TABLE [dbo].[M_DiscountTypeMaster] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [DiscountTypeName] NVARCHAR (50)  NULL,
    [DiscountTypeDesc] NVARCHAR (200) NULL,
    [ShortName]        NVARCHAR (50)  NULL,
    [CreatedBy]        INT            NULL,
    [CreatedDate]      DATETIME       NULL,
    [UpdatedBy]        INT            NULL,
    [UpdatedDate]      DATETIME       NULL,
    [IsActive]         BIT            NULL,
    CONSTRAINT [PK_M_DiscountTypeMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

