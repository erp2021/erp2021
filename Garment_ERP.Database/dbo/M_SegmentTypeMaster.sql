﻿CREATE TABLE [dbo].[M_SegmentTypeMaster] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [Segmenttype]     NVARCHAR (50)  NULL,
    [Segmenttypedesc] NVARCHAR (100) NULL,
    [Createddate]     DATETIME       NULL,
    [Createdby]       INT            NULL,
    [Updatedby]       INT            NULL,
    [Updateddate]     DATETIME       NULL,
    [IsActive]        BIT            NULL,
    CONSTRAINT [PK_M_SegmentTypeMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

