﻿CREATE TABLE [dbo].[M_WarehouseTypeMaster] (
    [WAREHOUSE_TYPE_ID] INT           IDENTITY (1, 1) NOT NULL,
    [TypeName]          NVARCHAR (20) NULL,
    [Typedesc]          NVARCHAR (50) NULL,
    [IsActive]          BIT           NULL,
    CONSTRAINT [PK_M_WarehouseTypeMaster] PRIMARY KEY CLUSTERED ([WAREHOUSE_TYPE_ID] ASC)
);

