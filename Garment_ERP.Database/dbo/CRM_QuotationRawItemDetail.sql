﻿CREATE TABLE [dbo].[CRM_QuotationRawItemDetail] (
    [Id]                BIGINT         IDENTITY (1, 1) NOT NULL,
    [Quotationid]       BIGINT         NULL,
    [Itemcategoryid]    BIGINT         NULL,
    [Itemsubcategoryid] INT            NULL,
    [Itemid]            BIGINT         NULL,
    [Supplier]          NVARCHAR (50)  NULL,
    [Unitid]            INT            NULL,
    [Createdate]        DATETIME       NULL,
    [Createdby]         INT            NULL,
    [Updateddate]       DATETIME       NULL,
    [Updatedby]         INT            NULL,
    [IsActive]          BIT            NULL,
    [imagepath]         NVARCHAR (200) NULL,
    [Itemdesc]          NVARCHAR (200) NULL,
    CONSTRAINT [PK_CRM_QuotationRawItemDetail] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CRM_QuotationRawItemDetail_CRM_QuotationRawItemDetail] FOREIGN KEY ([Itemcategoryid]) REFERENCES [dbo].[M_ItemCategoryMaster] ([Id]),
    CONSTRAINT [FK_CRM_QuotationRawItemDetail_M_ItemSubCategoryMaster] FOREIGN KEY ([Itemsubcategoryid]) REFERENCES [dbo].[M_ItemSubCategoryMaster] ([Id])
);

