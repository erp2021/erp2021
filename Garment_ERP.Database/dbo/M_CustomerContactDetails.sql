﻿CREATE TABLE [dbo].[M_CustomerContactDetails] (
    [Id]                BIGINT        IDENTITY (1, 1) NOT NULL,
    [Customerid]        BIGINT        NULL,
    [Contactpersonname] NVARCHAR (50) NULL,
    [Mobileno]          NVARCHAR (12) NULL,
    [Telephoneno]       NVARCHAR (10) NULL,
    [Faxno]             NVARCHAR (20) NULL,
    [Department]        INT           NULL,
    [Remark]            NVARCHAR (80) NULL,
    [Createddate]       DATETIME      NULL,
    [Createdby]         INT           NULL,
    [Updateddate]       DATETIME      NULL,
    [Updatedby]         INT           NULL,
    [IsActive]          BIT           NULL,
    [Email]             NVARCHAR (30) NULL,
    CONSTRAINT [PK_M_CustomerContactDetails] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_M_CustomerContactDetails_M_CustomerMaster] FOREIGN KEY ([Customerid]) REFERENCES [dbo].[M_CustomerMaster] ([Id]),
    CONSTRAINT [FK_M_CustomerContactDetails_M_DepartmentMaster] FOREIGN KEY ([Department]) REFERENCES [dbo].[M_DepartmentMaster] ([Id])
);

