﻿

CREATE proc [dbo].[SP_GetInventoryDetails]
as
begin
select GIM.GRNNo as InwardFrom,II.ItemName,UM.ItemUnit,IIM.ItemQty,IIM.BalanceQty from M_ItemInwardMaster IIM
 INNER JOIN SRM_GRNItem GI ON GI.ID=IIM.ItemInward_ID
 Inner Join SRM_GRNInward GIM ON GIM.ID=GI.ItemInwardId
 INNER JOIN M_ItemMaster II ON II.Id=IIM.Item_ID
 Inner Join M_UnitMaster UM On UM.Id=IIM.UOM
 
 END