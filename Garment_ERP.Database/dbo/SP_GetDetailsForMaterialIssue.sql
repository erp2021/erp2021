﻿CREATE proc [dbo].[SP_GetDetailsForMaterialIssue]
@workorderID bigint
as
select MWOPRD.ProcessID,PM.SHORT_NAME as ProcessName, MWOPM.ItemSubCategoryID, ISCM.Itemsubcategory, 
	   MWOPM.ItemID,IM.ItemName, MWOPM.UnitID,UM.ItemUnit, MWOPM.RequiredQty, MWOPM.IssuedQty,INM.BalanceQty
from  MFG_WorkOrderProcessMaterials MWOPM
inner join MFG_WorkOrderProcessDetails MWOPRD on MWOPRD.WO_ID =@workorderID and MWOPRD.WOProcessID=MWOPM.WOProcessID
inner join M_PROCESS_Master PM on PM.Process_Id  = MWOPRD.ProcessID
inner join M_ItemSubCategoryMaster ISCM on Iscm.Id = MWOPM.ItemSubCategoryID
inner join M_ItemMaster IM on IM.Id=MWOPM.ItemID
inner join M_ItemInwardMaster INM on INM.Item_ID=IM.Id and INM.BalanceQty > 0
inner join M_UnitMaster UM on UM.Id=MWOPM.UnitID

