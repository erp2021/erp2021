﻿CREATE TABLE [dbo].[CRM_EnquiryRawitemDetail] (
    [Id]                BIGINT         IDENTITY (1, 1) NOT NULL,
    [Enquiryid]         BIGINT         NULL,
    [Itemcategoryid]    BIGINT         NULL,
    [Itemsubcategoryid] INT            NULL,
    [Itemid]            BIGINT         NULL,
    [Supplier]          NVARCHAR (50)  NULL,
    [Unitid]            INT            NULL,
    [Createdate]        DATETIME       NULL,
    [Createdby]         INT            NULL,
    [Updateddate]       DATETIME       NULL,
    [Updatedby]         INT            NULL,
    [IsActive]          BIT            NULL,
    [Itemdesc]          NVARCHAR (300) NULL,
    CONSTRAINT [PK_CRM_EnquiryRawitemDetail] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CRM_EnquiryRawitemDetail_M_ItemCategoryMaster] FOREIGN KEY ([Itemcategoryid]) REFERENCES [dbo].[M_ItemCategoryMaster] ([Id]),
    CONSTRAINT [FK_CRM_EnquiryRawitemDetail_M_ItemMaster] FOREIGN KEY ([Itemid]) REFERENCES [dbo].[M_ItemMaster] ([Id]),
    CONSTRAINT [FK_CRM_EnquiryRawitemDetail_M_ItemSubCategoryMaster] FOREIGN KEY ([Itemsubcategoryid]) REFERENCES [dbo].[M_ItemSubCategoryMaster] ([Id])
);

