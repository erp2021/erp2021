﻿CREATE TABLE [dbo].[M_UserMaster] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [Username]    NVARCHAR (50) NOT NULL,
    [Password]    NVARCHAR (50) NOT NULL,
    [Createddate] DATETIME      CONSTRAINT [DF_M_UserMaster_Createddate] DEFAULT (getdate()) NULL,
    [Createdby]   INT           NULL,
    [Updateddate] DATETIME      CONSTRAINT [DF_M_UserMaster_Updateddate] DEFAULT (getdate()) NULL,
    [Updatedby]   INT           NULL,
    [IsActive]    BIT           NULL,
    [Deptid]      INT           NULL,
    [Email]       NVARCHAR (50) NULL,
    [Usertypeid]  INT           NULL,
    [LinkToken]   VARCHAR (10)  NULL,
    CONSTRAINT [PK_M_UserMaster] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_M_UserMaster_M_UserTypeMaster] FOREIGN KEY ([Usertypeid]) REFERENCES [dbo].[M_UserTypeMaster] ([Id])
);

