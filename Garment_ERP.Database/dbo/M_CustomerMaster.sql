﻿CREATE TABLE [dbo].[M_CustomerMaster] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [contactname]  NVARCHAR (60)  NULL,
    [Customertype] INT            NULL,
    [Phone]        NVARCHAR (20)  NULL,
    [Address]      NVARCHAR (100) NULL,
    [Cityid]       INT            NULL,
    [Stateid]      INT            NULL,
    [Countryid]    INT            NULL,
    [GSTNo]        NVARCHAR (15)  NULL,
    [Email]        NVARCHAR (20)  NULL,
    [Remark]       NVARCHAR (50)  NULL,
    [CSTRegno]     NVARCHAR (30)  NULL,
    [Tinno]        NVARCHAR (30)  NULL,
    [Createddate]  DATETIME       NULL,
    [CreatedBy]    INT            NULL,
    [Updateddate]  DATETIME       NULL,
    [UpdatedBy]    INT            NULL,
    [IsActive]     BIT            NULL,
    CONSTRAINT [PK_M_CustomerMaster] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_M_CustomerMaster_M_CustomerTypeMaster] FOREIGN KEY ([Customertype]) REFERENCES [dbo].[M_CustomerTypeMaster] ([Id])
);

