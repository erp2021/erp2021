﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity.Accounts;
namespace Garments_ERP.Entity.Accounts
{
    public class Acc_Voucher_Entity
    {
        public int Voucher_ID { get; set; }
        public Nullable<bool> Is_Cancelled { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        public Nullable<int> Voucher_Type_ID { get; set; }
        public string Voucher_No { get; set; }
        public System.DateTime Voucher_Date { get; set; }
        public string vc_date { get; set; }
        public Nullable<System.DateTime> Effective_Date { get; set; }
        public Nullable<int> Voucher_Ledger_Id { get; set; }
        public string Ref_Dr_Cr { get; set; }
        public Nullable<decimal> Voucher_Amount { get; set; }
        public string Reference_No { get; set; }
        public string Narration { get; set; }
        public string Voucher_Status { get; set; }
        public string Voucher_Country { get; set; }
        public string POL { get; set; }
        public string POD { get; set; }
        public string Cost_Center_Name { get; set; }
        public string Ledger_Name { get; set; }
        public Nullable<int> Approved_or_Rejected_By { get; set; }
        public Nullable<System.DateTime> Approved_or_Rejected_Date { get; set; }
        public Nullable<int> Working_Period_Id { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<int> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public int Deleted_By { get; set; }
        public Nullable<System.DateTime> Deleted_Date { get; set; }
        public int Cancelled_By { get; set; }
        public Nullable<System.DateTime> Cancelled_Date { get; set; }
        public Nullable<int> GST_Name_Id { get; set; }
        public string IP_Address { get; set; }
        public Nullable<int> GST_Branch_State_ID { get; set; }
        public Nullable<bool> Eway_Bill { get; set; }
        public Nullable<int> Branch_Id { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public virtual Acc_Voucher_Type_Entity Acc_Voucher_Type { get; set; }
        public virtual List<Acc_Voucher_Customer_Details_Entity> Acc_Voucher_Customer_Details { get; set; }
        public virtual List<Acc_Voucher_Items_Detail_Entity> Acc_Voucher_Items_Detail { get; set; }
        public virtual M_UserEntity Userentity { get; set; }
    }
}
