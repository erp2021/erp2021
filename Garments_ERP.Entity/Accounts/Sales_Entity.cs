﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.Accounts
{
    public class Sales_Entity
    {
        public Acc_Voucher_Entity Voucher { get; set; }
        public List<Acc_Voucher_Items_Detail_Entity> VoucherItems { get; set; }
        public Acc_Voucher_Customer_Details_Entity Voucher_Customer { get; set; }
    }
}
