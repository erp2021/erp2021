﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.Accounts
{
   public class Acc_Voucher_Items_Detail_Entity
    {
        public long Voucher_Item_Detail_ID { get; set; }
        public Nullable<int> Voucher_ID { get; set; }
        public string Barcode { get; set; }
        public string Hsncode { get; set; }
        public Nullable<long> Item_ID { get; set; }
        public string Item_Description { get; set; }
        public Nullable<int> Item_UOM_ID { get; set; }
        public Nullable<decimal> Sales_Qty { get; set; }
        public Nullable<decimal> Rate { get; set; }
        public Nullable<int> Scheme_ID { get; set; }
        public Nullable<int> Discount_Type_ID { get; set; }
        public Nullable<decimal> Discount_Rate { get; set; }
        public Nullable<decimal> Taxable_Amount { get; set; }
        public Nullable<decimal> GST_Rate { get; set; }
        public Nullable<decimal> GST_Amount { get; set; }
        public Nullable<decimal> Total_Amount { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<int> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public int Deleted_By { get; set; }
        public Nullable<System.DateTime> Deleted_Date { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }

        public virtual Acc_Voucher_Entity Acc_Voucher { get; set; }
    }
}
