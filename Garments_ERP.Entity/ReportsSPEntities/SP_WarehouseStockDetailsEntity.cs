﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class SP_WarehouseStockDetailsEntity
    {
        public string SHORT_NAME { get; set; }
        public long Id { get; set; }
        public string Itemcategory { get; set; }
        public Nullable<long> ItemId { get; set; }
        public string ItemName { get; set; }
        public Nullable<decimal> Accepted_Qty { get; set; }
        public string ItemUnit { get; set; }
    }
}
