﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.ReportsSPEntities
{
    public class SP_GetInventoryDetailsEntity
    {
        public string InwardFrom { get; set; }
        public string ItemName { get; set; }
        public string ItemUnit { get; set; }
        public Nullable<decimal> ItemQty { get; set; }
        public Nullable<decimal> BalanceQty { get; set; }
    }
}
