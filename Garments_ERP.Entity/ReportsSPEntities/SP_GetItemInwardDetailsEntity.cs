﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class SP_GetItemInwardDetailsEntity
    {
        public string SHORT_NAME { get; set; }
        public string GRNNo { get; set; }
        public Nullable<System.DateTime> InwardDate { get; set; }
        public Nullable<long> ItemId { get; set; }
        public string ItemName { get; set; }
        public Nullable<decimal> GRN_Qty { get; set; }
        public Nullable<decimal> PO_Qty { get; set; }
        public Nullable<decimal> Challan_Qty { get; set; }
        public Nullable<decimal> Balance_Qty { get; set; }
        public Nullable<decimal> Accepted_Qty { get; set; }
    }
}
