﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_ImageMasterEntity
    {
        public long Id { get; set; }
        public Nullable<long> ItemId { get; set; }
        public Nullable<int> Item_Attribute_ID { get; set; }
        public Nullable<int> SubCategoryId { get; set; }
        public Nullable<int> StyleId { get; set; }
        public string image { get; set; }
        public Nullable<int> IsCustomize { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
    }
}
