﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Garments_ERP.Entity
{
  public  class M_ItemShadeMasterEntity
    {

        public int Id { get; set; }

        [Required(ErrorMessage = "Enter Item Shade")]
        [System.Web.Mvc.Remote("CheckItemShade", "ItemShade", ErrorMessage = "Item Shade already in use!", AdditionalFields = "Id")]
        public string Itemshade { get; set; }
        public string Itemshadedesc { get; set; }
        public Nullable<System.DateTime> Createdate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }
    }
}
