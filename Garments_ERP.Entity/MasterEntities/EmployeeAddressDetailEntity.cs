﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class EmployeeAddressDetailEntity
    {
        public long EmpAddressId { get; set; }
        public Nullable<int> UserID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public Nullable<int> City { get; set; }
        public Nullable<int> State { get; set; }
        public Nullable<int> Country { get; set; }
        public string Pincode { get; set; }
        public string PhoneNo { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        
        public virtual EmployeeEntity Employee { get; set; }
        public virtual M_CityMasterEntity M_CityMaster { get; set; }
        public virtual M_CountryMasterEntity M_CountryMaster { get; set; }
        public virtual M_StateMasterEntity M_StateMaster { get; set; }
    }
}
