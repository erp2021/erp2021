﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_RackMasterEntity
    {
        public int Id { get; set; }
        public string Rack { get; set; }
        public string Rackdesc { get; set; }
        public Nullable<System.DateTime> Createdate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updatedate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> DepartmentId { get; set; }
        public string Comment { get; set; }
        public Nullable<long> WarehouseId { get; set; }
        public virtual M_WarehouseEntity M_WAREHOUSE { get; set; }
        public virtual List<M_ItemMasterEntity> M_ItemMaster { get; set; }
        public virtual M_DepartmentMasterEntity M_DepartmentMaster { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }
    }
}
