﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_PaymentTermMasterEntity
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Enter Payment Term")]
        [System.Web.Mvc.Remote("CheckPaymentTerm", "PaymentTerm", ErrorMessage = "Payment Term already exist!", AdditionalFields = "Id")]
        public string PaymentTerm { get; set; }
        public string PaymenTermdesc { get; set; }
        public Nullable<System.DateTime> Createdate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }
        public virtual List<SRM_PurchaseOrderMasterEntity> SRM_PurchaseOrderMaster { get; set; }
    }
}
