﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
  public  class M_ItemCategoryMasterEntity
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Enter item category name")]
        [System.Web.Mvc.Remote("CheckItemCategory", "ItemCategory", ErrorMessage = "Item category is already exists.", AdditionalFields = "Id, Itemgroupid")]
        public string Itemcategory { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public string Itemcategorydesc { get; set; }
        public int Itemgroupid { get; set; }
        public virtual M_ItemGroupEntity M_ItemGroupentity { get; set; }
        public List<M_ItemCategoryMasterEntity> itemcategorylist { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }
    }
}
