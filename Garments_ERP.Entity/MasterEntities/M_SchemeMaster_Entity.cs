﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.MasterEntities
{
   public class M_SchemeMaster_Entity
    {
        public long Scheme_ID { get; set; }
        public string Scheme_Name { get; set; }
        public Nullable<int> Segment_Type_ID { get; set; }
        public Nullable<long> Style_ID { get; set; }
        public Nullable<long> Item_ID { get; set; }
        public Nullable<decimal> Discount_Percentage { get; set; }
        public Nullable<System.DateTime> Start_Date { get; set; }
        public Nullable<System.DateTime> End_Date { get; set; }
        public string st_date { get; set; }
        public string en_date { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Updated_Date { get; set; }
        public Nullable<int> Updated_By { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<int> Company_Id { get; set; }
        public Nullable<int> Branch_Id { get; set; }
        public virtual M_ItemMasterEntity M_ItemMaster { get; set; }
        public virtual M_SegmentTypeMasterEntity M_SegmentTypeMaster { get; set; }
        public virtual M_StyleMasterEntity M_StyleMaster { get; set; }
    }
}
