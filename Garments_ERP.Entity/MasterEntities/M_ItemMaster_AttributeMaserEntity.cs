﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_ItemMaster_AttributeMaserEntity
    {
        public Nullable<long> ItemId { get; set; }
        public Nullable<int> Item_Attribute_ID { get; set; }
        public string ItemName { get; set; }
        public string HSNCode { get; set; }
        public Nullable<long> UnitId { get; set; }
        public string UnitName { get; set; }
        public Nullable<decimal> Quantity { get; set; }
    }
}
