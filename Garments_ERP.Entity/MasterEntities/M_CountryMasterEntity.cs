﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_CountryMasterEntity
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string CountryCode { get; set; }

        public virtual List<M_StateMasterEntity> M_StateMaster { get; set; }
    }
}
