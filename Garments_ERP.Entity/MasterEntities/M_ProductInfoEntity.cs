﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_ProductInfoEntity
    {
        public Nullable<long> inwardId { get; set; }
        public string Warehouse { get; set; }
        public string Rack { get; set; }
        public string ExpiryDate { get; set; }
        public string LotNo { get; set; }
        public string BatchNo { get; set; }
        public string SerialNo { get; set; }
        public Nullable<decimal> TotalQty { get; set; }
        public Nullable<decimal> ConsumedQty { get; set; }
        public Nullable<decimal> BalancedQty { get; set; }


    }
}
