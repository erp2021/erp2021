﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
 public   class M_InspectiontypeEntity
    {
        public int Id { get; set; }
        public string Inspectiontype { get; set; }
        public string Inspectiontypedescription { get; set; }
        public Nullable<int> Department { get; set; }
        public string Comment { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
    }
}
