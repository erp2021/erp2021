﻿using Garments_ERP.Entity.MasterEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class T_ITEM_TRANSFEREntity
    {
        public Nullable<long> TRANSFER_ID { get; set; }

        public Nullable<long> INVENTORY_ID { get; set; }
        public string STOCK_TRANSFER_NO { get; set; }
        public Nullable<System.DateTime> STOCK_TRANSFER_DATE { get; set; }
        public Nullable<long> WAREHOUSE_FROM { get; set; }
        public Nullable<long> WAREHOUSE_STORAGE_FROM { get; set; }
        public Nullable<long> WAREHOUSE_TO { get; set; }
        public Nullable<long> MODE_OF_DISPATCH { get; set; }
        public string DISPATCH_THRU { get; set; }
        public Nullable<System.DateTime> DISPATCH_DATE { get; set; }
        public string TRANSPOTER_NAME { get; set; }
        public string PERSON_NAME { get; set; }
        public Nullable<long> SENDER_BY { get; set; }
        public Nullable<System.DateTime> RECEIVED_DATE { get; set; }
        public Nullable<int> RECEIVED_PERSON { get; set; }
        public Nullable<long> INVENTORY_MODE { get; set; }
        public Nullable<long> WO_ID { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public string MODIFY_BY { get; set; }
        public Nullable<System.DateTime> MODIFY_ON { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> IsStatus { get; set; }


        public virtual MFG_MaterialIssueMasterEntity MaterialIssue { get; set; }
        public virtual M_WarehouseEntity FromWarehouse { get; set; }
        public virtual M_WarehouseEntity ToWarehouse { get; set; }
        public virtual EmployeeEntity Sender_Employee { get; set; }
        public virtual EmployeeEntity Received_Employee { get; set; }
        public virtual MFG_WORK_ORDEREntity WorkOrder { get; set; }

        public virtual List<T_ITEM_TRANSFER_DETAILEntity> ITDEntiy { get; set; }
    }
}
