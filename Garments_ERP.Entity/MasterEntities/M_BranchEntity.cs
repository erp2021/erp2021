﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_BranchEntity
    {
        public int Branch_ID { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public string Branch_Name { get; set; }
        public string Branch_Address { get; set; }
        public string Country_Code { get; set; }
        public Nullable<int> City_ID { get; set; }
        public Nullable<int> State_ID { get; set; }
        public string Pincode { get; set; }
        public string Tel_No { get; set; }
        public string mob_No { get; set; }
        public string Fax_No { get; set; }
        public string Email { get; set; }
        public string Base_Currency_Name { get; set; }
        public string Base_Currency_Description { get; set; }
        public Nullable<int> No_Of_Decimal { get; set; }
        public string Currency_Change_Name { get; set; }
        public string Service_Tax { get; set; }
        public string Tan_No { get; set; }
        public string GSTIN_No { get; set; }
        public Nullable<int> Currency_Id { get; set; }
        public string GST_Applicable { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<DateTime> Created_Date { get; set; }
        public Nullable<int> Modified_By { get; set; }
        public Nullable<DateTime> Modified_Date { get; set; }
        public Nullable<int> Deleted_By { get; set; }
        public Nullable<DateTime> Deleted_Date { get; set; }

        public virtual M_companyEntity companyEntity { get; set; }
     

    }
}
