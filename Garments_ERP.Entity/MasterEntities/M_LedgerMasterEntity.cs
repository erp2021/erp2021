﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_LedgerMasterEntity
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Enter Ledger Name")]
        [System.Web.Mvc.Remote("CheckCustomerName", "LedgerAccount", ErrorMessage = "Ledger Name Already In Use!", AdditionalFields = "Id")]

        public string Ledgername { get; set; }
        public string Ledgertype { get; set; }
        [StringLength(10)]
        public string Phone { get; set; }
        [RegularExpression(@"^([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$", ErrorMessage = "Enter valid email")]
        public string Email { get; set; }
        public string Remark { get; set; }
        public string CSTRegno { get; set; }
        public string Tinno { get; set; }
        public string Brand { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        
        public List<M_CustomerTypeMasterEntity> M_CustomerTypeMaster { get; set; }
        public List<M_DepartmentMasterEntity> deptlist { get; set; }
        public List<M_LedgerBillingMasterEntity> M_LedgerBillingMaster { get; set; }
        public List<M_LedgerDocumentMasterEntity> M_LedgerDocumentMaster { get; set; }
        public virtual List<CRM_EnquiryMasterEntity> CRM_EnquiryMaster { get; set; }
        public virtual List<CRM_QuotationMasterEntity> CRM_QuotationMaster { get; set; }
    }
}
