﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_LedgersEntity
    {
        public int Ledger_Id { get; set; }
        public string LedgerType_Id { get; set; }
        public string Ledger_Name { get; set; }
        public string Ledger_Description { get; set; }
        public string PAN_no { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<int> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<int> Deleted_By { get; set; }
        public Nullable<System.DateTime> Deleted_Date { get; set; }
        public Nullable<int> Department { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }


        public List<M_DepartmentMasterEntity> deptlist { get; set; }
        public List<M_Ledger_BillingDetailsEntity> M_Ledger_BillingDetails { get; set; }
        public List<M_CompanyBranch_MappingEntity> M_CompanyBranch_Mapping { get; set; }
        public virtual M_CityMasterEntity M_Cityentity { get; set; }
        public virtual M_StateMasterEntity M_Stateentity { get; set; }
        public virtual M_CountryMasterEntity M_Countryentity { get; set; }
        public List<M_LedgerDocumentMasterEntity> M_LedgerDocumentMaster { get; set; }
        public List<M_CustomerTypeMasterEntity> M_CustomerTypeMaster { get; set; }
        public virtual M_DepartmentMasterEntity M_Departmententity { get; set; }
    }
}
