﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_BrokerMasterEntity
    {
        public long Id { get; set; }
        public string BrokerName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public Nullable<int> Cityid { get; set; }
        public Nullable<int> Stateid { get; set; }
        public Nullable<int> Countryid { get; set; }
        public string Email { get; set; }
        public string Remark { get; set; }
        public string PANNo { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string GSTNo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CRM_EnquiryMasterEntity> CRM_EnquiryMaster { get; set; }
        public virtual M_CountryMasterEntity M_CountryMaster { get; set; }
        public virtual M_CityMasterEntity M_CityMaster { get; set; }
        public virtual M_StateMasterEntity M_StateMaster { get; set; }
    }
}
