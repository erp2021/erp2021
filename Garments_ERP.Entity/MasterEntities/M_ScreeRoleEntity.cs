﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_ScreeRoleEntity
    {
        public long screenRoleId { get; set; }
        public Nullable<long> ParentScreenId { get; set; }
        public Nullable<long> ChildScreenId { get; set; }
        public Nullable<long> RoleId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }

        public virtual M_ScreenEntity ParentScrEnt { get; set; }
        public virtual M_ScreenEntity ChildScrEnt { get; set; }
        public virtual M_RoleEntity RoleEnt { get; set; }
    }
}
