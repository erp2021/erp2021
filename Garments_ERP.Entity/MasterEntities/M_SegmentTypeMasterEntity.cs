﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Garments_ERP.Entity;

namespace Garments_ERP.Entity
{
  public  class M_SegmentTypeMasterEntity
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Enter Segment Type")]
        [System.Web.Mvc.Remote("CheckSegmentType", "SegmentType", ErrorMessage = "Segment Type already in use!", AdditionalFields = "Id")]
        public string Segmenttype { get; set; }
        public string Segmenttypedesc { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<bool> IsActive { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }

    }
}
