﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_WorkCenterMasterEntity
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Enter work center name")]
        [System.Web.Mvc.Remote("CheckWorkCenter", "WorkCenter", ErrorMessage = "Work center already exists.", AdditionalFields = "Id")]
        public string WorkCenterName { get; set; }
        public string WorkCenterLocation { get; set; }
        public Nullable<int> DepartmentId { get; set; }
        public string Comment { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }

        public virtual M_DepartmentMasterEntity M_DepartmentMaster { get; set; }
    }
}
