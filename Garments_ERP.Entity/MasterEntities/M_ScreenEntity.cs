﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_ScreenEntity
    {
        public long screenId { get; set; }
        public string ScreenName { get; set; }
        public string Discription { get; set; }
        public Nullable<long> ParentId { get; set; }
        public string URL { get; set; }
        public Nullable<int> Sequence { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<bool> CHKsatus { get; set; }
        public string FAIcon { get; set; }
        public Nullable<int> IsSub { get; set; }
        public virtual M_ScreenEntity ScreenEntity { get; set; }
        public virtual List<M_ScreenEntity> ScreenList { get; set; }
    }
}
