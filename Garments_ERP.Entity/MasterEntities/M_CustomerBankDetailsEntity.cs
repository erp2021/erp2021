﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_CustomerBankDetailsEntity
    {
        public long Id { get; set; }
        public Nullable<long> Customerid { get; set; }
        public string Bankname { get; set; }
        public string Accountno { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public string Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Bankcode { get; set; }

        public virtual M_CustomerMasterEntity M_CustomerMaster { get; set; }
    }
}
