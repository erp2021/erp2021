﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_CustomerTypeMasterEntity
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
        public string Typedesc { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
