﻿using Garments_ERP.Entity.MasterEntities;
using Garments_ERP.Entity.ProductionEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_ItemMasterEntity
    {

        public long Id { get; set; }
        public string ItemCode { get; set; }
        [Required(ErrorMessage = "Enter Item name")]
        [System.Web.Mvc.Remote("CheckItemName", "Item", ErrorMessage = "Item name already in use!", AdditionalFields = "Id")]
        public string ItemName { get; set; }
        public string Specification { get; set; }
        public Nullable<int> Unit { get; set; }
        public Nullable<int> CurrencyId { get; set; }
        public Nullable<decimal> ItemRate { get; set; }
        public Nullable<decimal> Taxrate { get; set; }
        public Nullable<decimal> OpeningStock { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<long> ItemCategoryid { get; set; }
        public Nullable<int> Itemcolorid { get; set; }
        public string Companyname { get; set; }
        public Nullable<int> Itemsubcategoryid { get; set; }
        public string Brand { get; set; }
        public string Shortname { get; set; }
        public string Batchperserial { get; set; }
        public Nullable<int> Warehouseid { get; set; }
        public Nullable<int> Rackid { get; set; }
        public Nullable<int> Shapeid { get; set; }
        public string EOQ { get; set; }
        public Nullable<bool> QCrequired { get; set; }
        public Nullable<int> Packingtypeid { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> Length { get; set; }
        public Nullable<decimal> Width { get; set; }
        public Nullable<decimal> Height { get; set; }
        public string ItemImage { get; set; }
        public string HScode { get; set; }
        public string HSchapter { get; set; }
        public Nullable<int> Itemgroupid { get; set; }
        public Nullable<int> ShadeId { get; set; }
        public Nullable<int> ItemStyleid { get; set; }
        public string ItemStyleNo { get; set; }
        public string ItemStyledescription { get; set; }
        public Nullable<Boolean> Status { get; set; }
        public virtual M_StyleMasterEntity StyleName { get; set; }
        public virtual M_ItemCategoryMasterEntity M_ItemCategoryMaster { get; set; }
        public virtual List<SRM_GRNItemEntity> SRM_GRNItem { get; set; }
        public virtual M_ItemColorEntity M_ItemColorMaster { get; set; }
        public virtual M_ItemSubCategoryMasterEntity M_ItemSubCategoryMaster { get; set; }
        public virtual M_UnitMasterEntity M_UnitMaster { get; set; }
        public virtual M_RackMasterEntity M_RackMaster { get; set; }
        public virtual List<M_ItemMasterImageEntity> M_ItemImages { get; set; }
        public virtual M_ItemShadeMasterEntity M_ItemShadeMaster { get; set; }
        public virtual M_ItemShapeMasterEntity M_ItemShapeMaster { get; set; }
        public virtual List<M_Item_Attribute_MasterEntity> M_Item_Attribute_Master { get; set; }
        public virtual List<M_FinishedGoodsEntity> finishitemlist { get; set; }

        public virtual List<M_ItemInwardMasterEntity> inwarditemlist { get; set; }
        public virtual List<M_Product_InfoEntity> productinfolist { get; set; }
        public virtual List<M_ItemOutwardMasterEntity> outwardlist { get; set; }
        public virtual List<M_FinishedGoodsEntity> M_FG { get; set; }
        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }
        public string Item_HSN { get; set; }
    }
}
