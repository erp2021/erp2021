﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Garments_ERP.Entity;

namespace Garments_ERP.Entity
{
public class M_StyleMasterEntity
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Enter Style name")]
        [System.Web.Mvc.Remote("CheckStyle", "Style", ErrorMessage = "Style name already in use!", AdditionalFields = "Id")]
        public string Stylename { get; set; }
        public string Styledescription { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string StyleNo { get; set; }
        public string StyleShortname { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }
    }
}
