﻿using Garments_ERP.Entity.MasterEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
  public  class M_ProcessCycleEntity
    {
        public int Prooce_Cycle_id { get; set; }
        [Required(ErrorMessage = "Enter Process Cycle Name")]
        [System.Web.Mvc.Remote("CheckProcesscycleName", "ProcessCycle", ErrorMessage = "ProcessCycle already in use!", AdditionalFields = "Prooce_Cycle_id")]
        public string Process_cycle_Name { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Processcysledesc { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }
        public List<ProcessCycleTransactionEntity> processcyclelist { get; set; }
        public virtual List<M_ProcessInCategoryDetailsEntity> M_ProcessInCategoryDetails { get; set; }
    }
}
