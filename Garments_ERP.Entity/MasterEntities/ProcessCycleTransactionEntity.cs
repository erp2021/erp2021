﻿using Garments_ERP.Entity.MasterEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
  public  class ProcessCycleTransactionEntity
    {
        public long Id { get; set; }
        public Nullable<int> Processcycleid { get; set; }
        public Nullable<decimal> Processid { get; set; }
        public Nullable<int> Processsequence { get; set; }
        public Nullable<long> ItemcategoryIn { get; set; }
        public Nullable<long> ItemcategoryOut { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string comment { get; set; }
        public string incategeories { get; set; }
        public virtual M_ItemCategoryMasterEntity M_ItemCategoryentity1 { get; set; }
        public virtual M_ItemCategoryMasterEntity M_ItemCategoryentity2 { get; set; }
        public virtual M_ProcessMasterEntity M_PROCESS_entity { get; set; }
        public virtual List<M_ProcessInCategoryDetailsEntity> M_ProcessInCategoryDetails { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Comapany_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }
    }
}
