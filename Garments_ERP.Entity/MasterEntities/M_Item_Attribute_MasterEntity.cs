﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_Item_Attribute_MasterEntity
    {
        public int Item_Attribute_ID { get; set; }
        public Nullable<long> Item_ID { get; set; }
        public string Attribute_ID { get; set; }
        public string Attribute_Value_ID { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<int> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<decimal> openingstock { get; set; }
        public Nullable<decimal> rate { get; set; }
        public Nullable<decimal> ROL { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<Boolean> Status { get; set; }

        public virtual M_ItemMasterEntity M_ItemMaster { get; set; }
        public virtual M_UserEntity M_UserMaster { get; set; }
    }
}
