﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Garments_ERP.Entity;

namespace Garments_ERP.Entity
{
    public class M_BrandMasterEntity
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Enter Brand Name")]
        [System.Web.Mvc.Remote("CheckBrandName", "Brand", ErrorMessage = "Brand name already in use!", AdditionalFields = "Id")]
        public string Brandname { get; set; }
        public string Brandshortname { get; set; }
        public string Branddescription { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        
        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> companyid { get; set; }
        public Nullable<int> branchid { get; set; }

    }
}
