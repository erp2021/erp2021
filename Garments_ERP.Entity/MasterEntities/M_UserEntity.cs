﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Garments_ERP.Entity
{
  public  class M_UserEntity
    {
        public int Id { get; set; }
        public string UserId { get; set; }
      [Required]
      [System.Web.Mvc.Remote("Checkusername", "User", ErrorMessage = "User name already in use!", AdditionalFields = "Id")]
        public string Username { get; set; }
      [Required]
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }
        [EmailAddress(ErrorMessage = "Please enter a valid Email address")] 
        public string Email { get; set; }
        public string LinkToken { get; set; }
        public string HashToken { get; set; }
        public bool RememberMe { get; set; }

        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public string APassword { get; set; }
        public  Nullable<int> RoleID { get; set; }
        public Nullable<int> DeptID { get; set; }
        public List<M_UserPermissionsEntity> permissionlist { get; set; }

        public EmployeeEntity empentity { get; set; }
        
    }
}
