﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class EmployeeEntity
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string EmpID { get; set; }
        public Nullable<int> Age { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<decimal> CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public Nullable<decimal> UPDATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_ON { get; set; }
        public string DOB { get; set; }
        public string PlaceOfBirth { get; set; }
        public string BloodGroup { get; set; }
        public string MaritalStatus { get; set; }
        public string Nationality { get; set; }
        public string Religion { get; set; }
        public string MotherTongue { get; set; }
        public string Caste { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<long> EmpTypeId { get; set; }

        public virtual M_RoleEntity roleEntity { get; set; }
        public virtual M_DepartmentMasterEntity DeptEntity { get; set; }
        public virtual M_Employee_TypeEntity EmpType { get; set; }
        public virtual EmployeeAddressDetailEntity empaddEnity { get; set; }
        public virtual List<M_ProcessMasterEntity> M_PROCESS_Master { get; set; }
    }
}
