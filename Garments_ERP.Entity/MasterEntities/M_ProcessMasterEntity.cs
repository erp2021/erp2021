﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_ProcessMasterEntity
    {
        public decimal Process_Id { get; set; }
        public string CODE { get; set; }
        [Required(ErrorMessage = "Enter Process Name")]
        [System.Web.Mvc.Remote("CheckProcessname", "Process", ErrorMessage = "Process name already in use!", AdditionalFields = "Process_Id")]
        public string SHORT_NAME { get; set; }
        public string Process_Description { get; set; }
        public string Process_Direction_Job { get; set; }
        public string Process_Produce_Output { get; set; }
        public string Process_produce_BarCode { get; set; }
        public Nullable<decimal> Process_Wastage { get; set; }
        public Nullable<decimal> Process_Duration_Per { get; set; }
        public string Process_Duration_Per_UOM { get; set; }
        public Nullable<decimal> Process_Duration_Nos { get; set; }
        public Nullable<decimal> Process_Duration { get; set; }
        public Nullable<long> Process_Machine_Type_Id { get; set; }
        public Nullable<int> Process_Incharge_ID { get; set; }
        public Nullable<bool> IS_ENABLED { get; set; }
        public Nullable<int> MEMBER_TYPE { get; set; }
        public Nullable<decimal> CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public Nullable<decimal> UPDATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_ON { get; set; }
        public Nullable<long> LFT { get; set; }
        public Nullable<long> RGT { get; set; }
        public Nullable<long> ItemCategoryId { get; set; }
               
        public virtual M_ItemCategoryMasterEntity M_ItemCategoryMaster { get; set; }
        public virtual M_MachineTypeMasterEntity M_MachineTypeMaster { get; set; }
        public virtual EmployeeEntity EmployeeMaster { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }
    }
}
