﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_CityMasterEntity
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Nullable<int> StateID { get; set; }

        public virtual M_StateMasterEntity M_StateMaster { get; set; }
    }
}
