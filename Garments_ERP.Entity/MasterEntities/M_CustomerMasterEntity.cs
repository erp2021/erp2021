﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_CustomerMasterEntity
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Enter Customer Name")]
        [System.Web.Mvc.Remote("CheckCustomerName", "Customer", ErrorMessage = "Contactname name already in use!", AdditionalFields = "Id")]

        public string contactname { get; set; }
        public Nullable<int> Customertype { get; set; }
        [StringLength(10)]
        public string Phone { get; set; }
        public string Address { get; set; }
        public Nullable<int> Cityid { get; set; }
        public List<M_CityMasterEntity> citylist { get; set; }

        public Nullable<int> Stateid { get; set; }
        public List<M_StateMasterEntity> statelist { get; set; }
        public List<M_CountryMasterEntity> countrylist { get; set; }

        public Nullable<int> Countryid { get; set; }
        [RegularExpression(@"^[0-9]{2}[(a-z)(A-Z)]{5}\d{4}[(a-z)(A-Z)]{1}\d{1}Z[(0-9)(a-zA-Z)]{1}", ErrorMessage = "Enter valid GSTIN.")]
        public string GSTNo { get; set; }
        [RegularExpression(@"^([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$", ErrorMessage = "Enter valid email")]
        public string Email { get; set; }
        public string Remark { get; set; }
        public string CSTRegno { get; set; }
        public string Tinno { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public List<M_DepartmentMasterEntity> deptlist { get; set; }
        public List<M_ContactAddressEntity> contactaddresslist { get; set; }
        public virtual List<CRM_EnquiryMasterEntity> CRM_EnquiryMaster { get; set; }
        public virtual List<CRM_QuotationMasterEntity> CRM_QuotationMaster { get; set; }
        public virtual M_CustomerBankDetailsEntity M_CustomerBankDetails { get; set; }
        public virtual List<M_CustomerContactDetailsEntity> M_CustomerContactDetails { get; set; }
        public virtual M_CustomerTypeMasterEntity M_CustomerTypeMaster { get; set; }

    }
}
