﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_Dynamic_ControlsEntity
    {
        public long Dynamic_Controls_Id { get; set; }
        public string Control_Name { get; set; }
        public string Control_Description { get; set; }
        public int Control_Type_Id { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<int> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<int> Deleted_By { get; set; }
        public Nullable<System.DateTime> Deleted_Date { get; set; }
        public int IsPage { get; set; }
        public Nullable<int> PageORPageGroupID { get; set; }
    }
}
