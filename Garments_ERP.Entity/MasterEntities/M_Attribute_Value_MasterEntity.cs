﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.MasterEntities
{
    public class M_Attribute_Value_MasterEntity
    {
        public int Attribute_Value_ID { get; set; }
        public Nullable<int> Attribute_ID { get; set; }
        public string Attribute_Value { get; set; }
        public string Attribute_Value_Desc { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<int> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<Boolean> Status { get; set; }
        public virtual M_Attribute_MasterEntity M_Attribute_Master { get; set; }
        public virtual M_UserEntity M_UserMaster { get; set; }
        
        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }
    }
}
