﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_LedgerDocumentMasterEntity
    {
        public long Id { get; set; }
        public long LedgerId { get; set; }
        public string Filename { get; set; }
        public string Filesize { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> Isdelete { get; set; }
        public string Filetype { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }
        public virtual M_LedgerMasterEntity M_LedgerMaster { get; set; }
    }
}
