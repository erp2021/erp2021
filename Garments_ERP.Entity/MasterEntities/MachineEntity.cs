﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
  public  class MachineEntity
    {
        public long Id { get; set; }
        public string Machinename { get; set; }
        public string Machinedesc { get; set; }
        public Nullable<long> Machinetypeid { get; set; }
        public Nullable<int> Vendortypeid { get; set; }
        public string MachineSn { get; set; }
        public Nullable<System.DateTime> Purchasedate { get; set; }
        public Nullable<decimal> Purchaseprice { get; set; }
        public string Pricecurrency { get; set; }
        public Nullable<int> Inspectiontypeid { get; set; }
        public string Inspectionduration { get; set; }
        public string Durationnos { get; set; }
        public Nullable<System.DateTime> Inspectiondate { get; set; }
        public Nullable<System.DateTime> Inspectionnextdate { get; set; }
        public Nullable<int> Productioncapacity { get; set; }
        public Nullable<int> CapacityUOMid { get; set; }
        public string Capacityduration { get; set; }
        public Nullable<int> Runwidth { get; set; }
        public Nullable<int> Runheight { get; set; }
        public Nullable<int> Runlength { get; set; }
        public Nullable<int> LwhUOMid { get; set; }
        public string Remark { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }

        public virtual M_MachineTypeMasterEntity M_MachineTypeentity { get; set; }
    }
}
