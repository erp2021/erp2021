﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_RackAllocationEntity
    {
        public long Id { get; set; }
        public Nullable<long> warehouseid { get; set; }
        public Nullable<long> ItemId { get; set; }
        public Nullable<int> Item_Attribute_Id { get; set; }
        public string BatchLot { get; set; }
        public Nullable<decimal> BatchLotQty { get; set; }
        public string RackId { get; set; }
        public Nullable<decimal> RackQty { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> Createdate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updatedate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<long> GRNId { get; set; }

        public virtual M_WarehouseEntity warehouse {get;set;}
        public virtual M_ItemMasterEntity ItemEntity { get; set; }

    }
}
