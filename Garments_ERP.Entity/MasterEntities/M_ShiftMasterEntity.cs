﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_ShiftMasterEntity
    {
        public long ID { get; set; }
        public string CODE { get; set; }
        [Required(ErrorMessage = "Please enter shift name.")]
        public string SHORT_NAME { get; set; }
         [Required(ErrorMessage = "Please enter start time.")]
        public Nullable<System.TimeSpan> START_TIME { get; set; }
         [Required(ErrorMessage = "Please enter end time.")]
        public Nullable<System.TimeSpan> END_TIME { get; set; }
        public Nullable<System.DateTime> EFFECTIVE_DATE { get; set; }
        public Nullable<bool> IS_ENABLED { get; set; }
        public Nullable<int> CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public Nullable<int> UPDATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_ON { get; set; }
         [Required(ErrorMessage = "Please select department.")]
        public Nullable<int> DepartmentId { get; set; }
        public string Comment { get; set; }

        public virtual M_DepartmentMasterEntity M_DepartmentMaster { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }
    }
}
