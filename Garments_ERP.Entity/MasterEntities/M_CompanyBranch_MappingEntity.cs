﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_CompanyBranch_MappingEntity
    {
        public int Id { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }
        public Nullable<int> Ledger_Id { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<int> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<int> Deleted_By { get; set; }
        public Nullable<System.DateTime> Deleted_Date { get; set; }

        public virtual M_companyEntity M_company { get; set; }
        public virtual M_BranchEntity M_Branch { get; set; }
    }
}
