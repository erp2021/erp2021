﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_MachineTypeMasterEntity
    {
        public long Machine_TypeId { get; set; }
        [Required(ErrorMessage = "Enter Machine type Name")]
        [System.Web.Mvc.Remote("Checkmachinetypename", "Machinetype", ErrorMessage = "Machine type name already in use!", AdditionalFields = "Machine_TypeId")]
        public string Machine_TypeName { get; set; }
        public string Machine_TypeDescription { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public Nullable<int> companyid { get; set; }
        public Nullable<int> branchid { get; set; }

        public virtual ICollection<M_ProcessMasterEntity> M_PROCESS_Master { get; set; }
    }
}
