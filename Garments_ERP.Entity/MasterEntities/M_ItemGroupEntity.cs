﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
   public class M_ItemGroupEntity
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Enter item group name")]
        [System.Web.Mvc.Remote("CheckItemGroup", "ItemGroup", ErrorMessage = "Item group is already exists.", AdditionalFields = "Id")]
        public string Itemgroup { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Itemgroupdesc { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public List<M_ItemGroupEntity> itemgrouplist { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }

    }
}
