﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_UnitMasterEntity
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Enter unit name")]
        [System.Web.Mvc.Remote("CheckUnit", "Unit", ErrorMessage = "Unit is already exists.", AdditionalFields = "Id")]
        public string ItemUnit { get; set; }
        public string Unitdesc { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }        
        public Nullable<int> DepartmentId { get; set; }
        public string Comment { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public virtual List<M_ItemMasterEntity> M_ItemMaster { get; set; }
        public virtual List<SRM_GRNItemEntity> SRM_GRNItem { get; set; }
        public virtual List<M_BillOfMaterialDetailEntity> M_BillOfMaterialDetail { get; set; }
        public virtual M_DepartmentMasterEntity M_DepartmentMaster { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }
    }
}
