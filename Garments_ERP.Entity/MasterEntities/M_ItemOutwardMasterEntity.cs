﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.MasterEntities
{
    public class M_ItemOutwardMasterEntity
    {
        public long ID { get; set; }
        public Nullable<long> Inward_ID { get; set; }
        public Nullable<decimal> Process_ID { get; set; }
        public Nullable<long> Sale_ID { get; set; }
        public Nullable<long> Item_ID { get; set; }
        public Nullable<int> UOM { get; set; }
        public Nullable<decimal> ItemQty { get; set; }
        public Nullable<int> OutwardBy { get; set; }
        public Nullable<System.DateTime> OutwardDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> Item_Attribute_ID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<long> MI_ID { get; set; }
        public Nullable<int> ItemSubCategoryID { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public virtual M_ItemInwardMasterEntity M_ItemInwardMaster { get; set; }
        public virtual M_ProcessMasterEntity M_PROCESS_Master { get; set; }
        public virtual M_ItemSubCategoryMasterEntity M_ItemSubCategoryMaster { get; set; }
    }
}
