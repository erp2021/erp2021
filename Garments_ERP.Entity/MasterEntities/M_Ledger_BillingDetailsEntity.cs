﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_Ledger_BillingDetailsEntity
    {
        public int Id { get; set; }
        public Nullable<int> Ledger_Id { get; set; }
        public string Billing_Name { get; set; }
        public string Unit { get; set; }
        public string Contact_No { get; set; }
        public string Telephone_no { get; set; }
        public string Fax_no { get; set; }
        public string Email { get; set; }
        public string GSTIN { get; set; }
        public string Address { get; set; }
        public Nullable<int> Country_Id { get; set; }
        public Nullable<int> State_Id { get; set; }
        public Nullable<int> City_Id { get; set; }
        public string Bank_Name { get; set; }
        public string Account_no { get; set; }
        public string Bank_code { get; set; }
        public Nullable<bool> Is_SEZ { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<int> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<int> Deleted_By { get; set; }
        public Nullable<System.DateTime> Deleted_Date { get; set; }
        //added ne company and branch,done by Rahul ,on 03-01-2020
        public string Company { get; set; }
        public string Branch { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }



        public virtual M_CityMasterEntity M_Cityentity { get; set; }
        public virtual M_StateMasterEntity M_Stateentity { get; set; }
        public virtual M_CountryMasterEntity M_Countryentity { get; set; }
    }
}
