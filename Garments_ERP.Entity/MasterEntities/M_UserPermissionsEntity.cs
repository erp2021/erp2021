﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
   public class M_UserPermissionsEntity
    {
        public int Id { get; set; }
        public int ModuleId { get; set; }
        public bool IsRead { get; set; }
        public bool IsCreate { get; set; }
        public bool IsEdit { get; set; }
        public bool IsDelete { get; set; }
        public Nullable<int> UserId { get; set; }

        public virtual M_ModuleEntity M_Moduleentity { get; set; }
        public virtual M_UserEntity M_Userentity { get; set; }
    }
}
