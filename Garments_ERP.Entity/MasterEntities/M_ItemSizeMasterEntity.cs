﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public  class M_ItemSizeMasterEntity
    {
        public long Id { get; set; }
        public Nullable<long> Itemid { get; set; }               
        public long? Styleid { get; set; }
        [Required(ErrorMessage = "Enter Item Size")]
        [System.Web.Mvc.Remote("CheckItemSize", "ItemSize", ErrorMessage = "Item size already in use!", AdditionalFields = "Id")]
        public string Sizetype { get; set; }
        public string Sizedesc { get; set; }
        public Nullable<System.DateTime> Createddat { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }

       
        public Nullable<long> ItemNameid { get; set; }

        public Nullable<int> Itemsubcategoryid { get; set; }

        public virtual M_StyleMasterEntity M_StyleMaster { get; set; }

        public virtual M_ItemMasterEntity M_ItemMaster { get; set; }
       
    }
}
