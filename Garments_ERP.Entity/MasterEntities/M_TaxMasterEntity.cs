﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Garments_ERP.Entity;

namespace Garments_ERP.Entity
{
    public class M_TaxMasterEntity
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Enter Tax Name")]
        [System.Web.Mvc.Remote("CheckTaxName", "Tax", ErrorMessage = "Tax name already in use!", AdditionalFields = "Id")]
        public string Taxname { get; set; }
        public Nullable<decimal> Percentage { get; set; }
        public string Taxdescription { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
