﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
  public  class StyleSizeEntity
    {
      public long Id { get; set; }
        public Nullable<int> Reqqty { get; set; }
        public string Sizetype { get; set; }
        public Nullable<long> SizeId { get; set; }
        public Nullable<decimal> Sizeper { get; set; }
        public Nullable<decimal> Sizepcs { get; set; }
    }
}
