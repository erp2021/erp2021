﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.MasterEntities
{
   public class M_ProcessInCategoryDetailsEntity
    {
        public long Id { get; set; }
        public Nullable<long> ProCycTran_ID { get; set; }
        public Nullable<long> ItemcategoryIn { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public virtual ProcessCycleTransactionEntity ProcessCycleTransaction { get; set; }
        public virtual M_ItemCategoryMasterEntity M_ItemCategoryMaster { get; set; }
    }
}
