﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_Product_InfoEntity
    {
        public long Product_Info_Id { get; set; }
        public Nullable<long> ItemInward_ID { get; set; }
        public Nullable<long> Dynamic_Controls_Id { get; set; }
        public string Dynamic_Controls_value { get; set; }
        public Nullable<bool> Is_Deleted { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<int> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<int> Deleted_By { get; set; }
        public Nullable<System.DateTime> Deleted_Date { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
    }
}
