﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Garments_ERP.Entity;

namespace Garments_ERP.Entity
{
    public class M_DiscountTypeMasterEntity
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Enter DiscountType name")]
        [System.Web.Mvc.Remote("CheckDiscountType", "DiscountType", ErrorMessage = "DiscountType already in use!", AdditionalFields = "Id")]
        public string DiscountTypeName { get; set; }
        public string DiscountTypeDesc { get; set; }
        public string ShortName { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public virtual List<SRM_QuotationItemDetailEntity> SRM_QuotationItemDetail { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> Branch_ID { get; set; }



    }
}
