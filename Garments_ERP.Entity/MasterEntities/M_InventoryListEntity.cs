﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_InventoryListEntity
    {
        public Nullable<long> Id { get; set; }
        public Nullable<long> Item_Attribute_ID { get; set; }
        public string ItemName { get; set; }
        public Nullable<DateTime> ItemCreateddate { get; set; }
        public Nullable<DateTime> ItemUpdateddate { get; set; }
        public Nullable<long> Unit { get; set; }
        public string UnitName { get; set; }
        public Nullable<long> ItemCategoryid { get; set; }
        public string Itemcategory { get; set; }
        public Nullable<long> Itemsubcategoryid { get; set; }
        public string Itemsubcategory { get; set; }
        public Nullable<long> Itemgroupid { get; set; }
        public string Itemgroup { get; set; }
        public Nullable<long> ItemStyleid { get; set; }
        public string ItemStyleNo { get; set; }
        public string ItemStyledescription { get; set; }
        public Nullable<decimal> Taxrate { get; set; }
        public Nullable<decimal> OpeningStock { get; set; }
        public Nullable<decimal> PurchaseINQty { get; set; }
        public Nullable<decimal> ProcessQty { get; set; }
        public string BatchInfo { get; set; }
        public Nullable<decimal> TotalStock { get; set; }
        public Nullable<decimal> OutQty { get; set; }
        public Nullable<decimal> BalStock { get; set; }
        public string HSNCode { get; set; }
        public Nullable<int> RACount { get; set; }
    }
}
