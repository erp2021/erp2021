﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_ItemShapeMasterEntity
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Enter Item Shape")]
        [System.Web.Mvc.Remote("CheckItemShape", "ItemShape", ErrorMessage = "Item Shape already exist!", AdditionalFields = "Id")]
        public string Itemshape { get; set; }
        public string Itemshapedesc { get; set; }
        public Nullable<System.DateTime> Createdate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
