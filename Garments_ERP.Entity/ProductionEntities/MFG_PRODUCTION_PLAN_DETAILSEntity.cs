﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
  public  class MFG_PRODUCTION_PLAN_DETAILSEntity
    {
        public long ID { get; set; }
        public long PlanID { get; set; }
        public long StyleID { get; set; }
        public Nullable<long> SizeID { get; set; }
        public Nullable<long> MachineID { get; set; }
        public Nullable<decimal> Target_Qty { get; set; }
        public Nullable<decimal> Actual_Qty { get; set; }
        public Nullable<int> UnitID { get; set; }

        public string Attribute_ID { get; set; }
        public string Attribute_Value_ID { get; set; }
        public Nullable<decimal> rate { get; set; }

        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }


        public virtual M_ItemSizeMasterEntity M_ItemSizeMaster { get; set; }
        public virtual M_StyleMasterEntity M_StyleMaster { get; set; }
        public virtual M_UnitMasterEntity M_UnitMaster { get; set; }
        public virtual MachineEntity MachineMaster { get; set; }
        public virtual MFG_PRODUCTION_PLANEntity MFG_PRODUCTION_PLAN { get; set; }

    }
}
