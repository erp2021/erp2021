﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.ProductionEntities
{
    public class FG_Transfer_To_RetailEntity
    {
        public long FG_TR_ID { get; set; }
        public Nullable<int> Customer_ID { get; set; }
        public Nullable<long> WorkOrderID { get; set; }
        public Nullable<long> Item_ID { get; set; }
        public Nullable<long> FG_Transfer_ID { get; set; }
        public Nullable<long> WarehouseId { get; set; }
        public Nullable<int> RackId { get; set; }
        public Nullable<int> Branch_ID { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<long> POid { get; set; }
        public Nullable<long> GRNID { get; set; }
        public Nullable<int> Item_Attribute_Id { get; set; }
        public string HSNCode { get; set; }
        public Nullable<int> Unit_Id { get; set; }
        public Nullable<decimal> PO_Qty { get; set; }
        public Nullable<decimal> TransferQty { get; set; }

        public string Customer_Name { get; set; }
        public virtual List<FG_Transfer_To_Retail_DetailsEntity> FG_Transfer_To_Retail_Details { get; set; }
        public virtual FG_TransferMasterEntity FG_TransferMaster { get; set; }
        public virtual M_ItemMasterEntity M_ItemMaster { get; set; }
        public virtual M_RackMasterEntity M_RackMaster { get; set; }
        public virtual M_WarehouseEntity M_WAREHOUSE { get; set; }
        public virtual MFG_WORK_ORDEREntity MFG_WORK_ORDER { get; set; }
        public virtual CRM_CustomerPOMasterentity POEntity { get; set; }
        public virtual SRM_GRNInwardEntity GRNEntity { get; set; }
    }
}
