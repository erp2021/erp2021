﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class MFG_MaterialIssueStyleImageEntity
    {
        public long Id { get; set; }
        public string StyleImgName { get; set; }
        public Nullable<long> ItemID { get; set; }
        public long MaterialIssueId { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public virtual M_ItemMasterEntity M_ItemMaster { get; set; }
        public virtual MFG_MaterialIssueMasterEntity MFG_MaterialIssueMaster { get; set; }
    }
}
