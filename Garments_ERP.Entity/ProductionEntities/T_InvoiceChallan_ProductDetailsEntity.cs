﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class T_InvoiceChallan_ProductDetailsEntity
    {
        public long Invoice_ProductId { get; set; }
        public Nullable<long> InvoiceId { get; set; }
        public Nullable<long> ItemId { get; set; }
        public Nullable<int> Item_Attribute_ID { get; set; }
        public string BaleNo { get; set; }
        public Nullable<long> UOM { get; set; }
        public string HSNNO { get; set; }
        public Nullable<decimal> Qty { get; set; }
        public Nullable<decimal> Rate { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<decimal> GST { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<decimal> POQty { get; set; }
        public Nullable<decimal> BalQty { get; set; }
        public string Batchid { get; set; }
        public string itemPIval { get; set; }

    }
}
