﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class T_ISSUE_RETURNS_DETAILEntity
    {
        public decimal ID { get; set; }
        public Nullable<decimal> RETURNS_ID { get; set; }
        public Nullable<decimal> ISSUED_QTY { get; set; }
        public Nullable<decimal> RETURNED_QTY { get; set; }
        public string UNIT { get; set; }
        public string ITEM_STATUS { get; set; }
        public Nullable<decimal> ITEM_ID { get; set; }
        public Nullable<decimal> ISSUE_ID { get; set; }
        public Nullable<decimal> INVENTORY_ID { get; set; }
        public Nullable<System.DateTime> RETURN_DATE { get; set; }
        public Nullable<decimal> BEFORE_WEIGHT { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> ITEM_ATTRIBUTE_ID { get; set; }
        public string HSN_CODE { get; set; }
        public Nullable<decimal> PO_TRANSFER_QTY { get; set; }
        public Nullable<long> BATCH_LOT_ID { get; set; }
        public Nullable<decimal> BALANCED_QTY { get; set; }

        public virtual M_ItemMasterEntity ItemMaster { get; set; }
        public virtual List<M_Product_InfoEntity> BatchList { get; set; }
        public virtual M_UnitMasterEntity unitent { get; set; }
    }
}
