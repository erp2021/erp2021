﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class MFG_Store_WorkOrderEntity
    {
        public long Store_WO_Id { get; set; }
        public string Store_WO_No { get; set; }
        public Nullable<System.DateTime> Store_WO_Date { get; set; }
        public Nullable<long> Store_Name { get; set; }
        public string StoreName { get; set; }
        public Nullable<long> Store_Warehouse { get; set; }
        public string StoreWarehouse { get; set; }
        public Nullable<long> Customer_Id { get; set; }
        public string CustomerName { get; set; }
        public string PO_ref_No { get; set; }
        public Nullable<long> PO_Id { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<long> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Updated_By { get; set; }
        public Nullable<System.DateTime> Updated__Date { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<bool> Approvalstatus { get; set; }
        public Nullable<System.DateTime> ApproveDate { get; set; }
        public string Comment { get; set; }
        public Nullable<int> IsStatus { get; set; }
    }
}
