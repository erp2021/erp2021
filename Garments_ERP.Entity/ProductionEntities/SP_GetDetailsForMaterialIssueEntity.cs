﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.ProductionEntities
{
    public class SP_GetDetailsForMaterialIssueEntity
    {
        public Nullable<decimal> ProcessID { get; set; }
        public string ProcessName { get; set; }
        public Nullable<long> ItemSubCategoryID { get; set; }
        public string Itemsubcategory { get; set; }
        public Nullable<long> ItemID { get; set; }
        public string ItemName { get; set; }
        public Nullable<int> UnitID { get; set; }
        public string ItemUnit { get; set; }
        public Nullable<decimal> RequiredQty { get; set; }
        public Nullable<decimal> IssuedQty { get; set; }
        public Nullable<decimal> BalanceQty { get; set; }
        public List<SP_GetDetailsForMaterialIssueEntity> processList { get; set; }
    }
}
