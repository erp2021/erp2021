﻿using Garments_ERP.Entity.ProductionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class MFG_WORK_ORDEREntity
    {
        public long WorkOrderID { get; set; }
        public long PlanID { get; set; }
        public long CustomerID { get; set; }
        public long POID { get; set; }
        public long StyleID { get; set; }
        public string WorkOrderNo { get; set; }
        public string StyleType { get; set; }
        public Nullable<long> DepartmentID { get; set; }
        public Nullable<decimal> RevisionNo { get; set; }
        public Nullable<decimal> RequiredQty { get; set; }
        public Nullable<int> UnitID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> Approved { get; set; }
        public Nullable<System.DateTime> ApproveDate { get; set; }
        public string Comment { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsMIdone { get; set; }
        public string wodate { get; set; }
        public Nullable<long> EnquiryID { get; set; }
        public Nullable<int> ProcessCycleID { get; set; }

        public Nullable<int> IsStatus { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        /// <summary>
        /// Addded by ramesh to get list of work orders with item
        /// </summary>
        public long? Itemid { get; set; }
        public string ItemName { get; set; }
        public CRM_CustomerPOMasterentity customerpo { get; set; }
        public CRM_QuotationMasterEntity quotationdetails { get; set; }
        public M_BillOfMaterialMasterEntity bomdetails { get; set; }
        public virtual List<ProcessCycleTransactionEntity> processtranlist { get; set; }
        public List<MFG_WORK_ORDER_DETAILEntity> workorderdetaillist { get; set; }
        public virtual M_CustomerMasterEntity M_Customerentity { get; set; }
        public virtual M_StyleMasterEntity M_Styleentity { get; set; }
        public virtual M_UnitMasterEntity M_Unitentity { get; set; }
        public virtual CRM_EnquiryMasterEntity CRM_EnquiryMaster { get; set; }
        // public virtual M_ProcessCycleEntity M_ProcessCycleMaster { get; set; }
        public virtual List<M_ProcessMasterEntity> M_ProcessMasterList { get; set; }
        public virtual List<MFG_WorkOrderProcessDetailsEntity> MFG_WorkOrderProcessDetails { get; set; }
        public virtual M_LedgersEntity LedgerEntity { get; set; }
    }
}
