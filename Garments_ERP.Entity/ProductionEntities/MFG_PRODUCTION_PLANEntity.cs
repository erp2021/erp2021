﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
  public  class MFG_PRODUCTION_PLANEntity
    {
        public long ID { get; set; }
        public string PlanNo { get; set; }
        public Nullable<System.DateTime> PlanDate { get; set; }
        public string PlanDateString { get; set; }
        public long CustomerID { get; set; }

        public long WorkOrderID { get; set; }
        public string WorkOrderNo { get; set; }
        public decimal POID { get; set; }
        public long StyleID { get; set; }
        public string StyleName { get; set; }
        public Nullable<decimal> SampleReferenceNo { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public string StartDateString { get; set; }
        public Nullable<int> ProcessCycleID { get; set; }
        public Nullable<decimal> Target_Qty { get; set; }
        public Nullable<int> UnitID { get; set; }
        public Nullable<System.DateTime> TargetDate { get; set; }
        public string TargetDateString { get; set; }
        public Nullable<long> WorkCenterID { get; set; }
        public Nullable<int> DepartmentID { get; set; }
        public Nullable<long> ToolID { get; set; }
        public Nullable<int> OperatorID { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<DateTime> ApprovedDate { get; set; }
        public Nullable<int> Approved { get; set; }
        public string Comment { get; set; }
        public Nullable<int> IsStatus { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> PerDayProduction { get; set; }
        public bool IsActive { get; set; }
        public List<MFG_PRODUCTION_PLAN_DETAILSEntity> detaillist { get; set; }
        public virtual M_UnitMasterEntity M_UnitMaster { get; set; }
        public virtual M_StyleMasterEntity M_StyleMaster { get; set; }
        public virtual M_DepartmentMasterEntity M_DepartmentMaster { get; set; }
        public virtual MFG_PRODUCTION_PLAN_DETAILSEntity MFG_PRODUCTION_PLAN_DETAILS { get; set; }
        public virtual MFG_WORK_ORDEREntity MFG_WORK_ORDER { get; set; }
        public virtual M_WorkCenterMasterEntity WCentity { get; set; }
    }


}
