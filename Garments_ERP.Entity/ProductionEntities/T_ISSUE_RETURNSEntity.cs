﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class T_ISSUE_RETURNSEntity
    {
        public decimal ID { get; set; }
        public Nullable<System.DateTime> RETURNS_DATE { get; set; }
        public string RETURNS_DESC { get; set; }
        public Nullable<decimal> ISSUE_ID { get; set; }
        public Nullable<decimal> CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public Nullable<decimal> UPDATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_ON { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> IsStatus { get; set; }
        public string RETURN_NO { get; set; }
        public string STYPE { get; set; }
        public Nullable<long> BUYER_SELLER { get; set; }
        public Nullable<long> PO_ID { get; set; }
        public string POno { get; set; }
        public Nullable<System.DateTime> PO_DATE { get; set; }
        public Nullable<long> WO_ID { get; set; }
        public string wono { get; set; }
        public Nullable<System.DateTime> WO_DATE { get; set; }
        public Nullable<long> INVOICE_ID { get; set; }
        public string invno { get; set; }
        public Nullable<System.DateTime> INV_DATE { get; set; }
        public Nullable<long> MATERIAL_ISSUE_ID { get; set; }
        public string MI_NO { get; set; }
        public Nullable<long> TRANSFER_ID { get; set; }
        public string Tno { get; set; }
        public Nullable<System.DateTime> TRANSFER_DATE { get; set; }
        public Nullable<int> MODE_ID { get; set; }
        public string itemName { get; set; }
        public Nullable<decimal> returnQty { get; set; }
        public virtual M_LedgersEntity MLedgerEntity { get; set; }
        public virtual EmployeeEntity empentity { get; set; }
        public virtual CRM_CustomerPOMasterentity Cpoentity { get; set; }
        public virtual SRM_PurchaseOrderMasterEntity Spoentity { get; set; }
        public virtual MFG_WORK_ORDEREntity WOEntity { get; set; }
        public virtual T_Invoice_ChallanEntity TInvoiceEntity { get; set; }
        public virtual MFG_MaterialIssueMasterEntity MaterialIssue { get; set; }
        public virtual T_ITEM_TRANSFEREntity T_ItemTranferEntity { get; set; }
        public virtual List<T_ISSUE_RETURNS_DETAILEntity> T_ISSUE_RTRN_DETAILEntity { get; set; }

    }
}
