﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.ProductionEntities
{
   public class SP_GetProcessMaterialEntity
    {
        public long WOProcessID { get; set; }
        public int ItemSubCategory { get; set; }
        public Nullable<long> Itemid { get; set; }
        public Nullable<decimal> ProcessRequiredQty { get; set; }
        public decimal IssuedQty { get; set; }
        public Nullable<int> UnitId { get; set; }
        public Nullable<int> Processsequence { get; set; }
    }
}
