﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.ProductionEntities
{
    public class SP_GetProc_DetailsForProc_InchargeEntity
    {
        public Nullable<decimal> ProcessID { get; set; }
        public string ProcessName { get; set; }
        public string WorkOrderNo { get; set; }
        public long WorkOrderID { get; set; }

    }
}
