﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_JobworkEntity
    {
        public long jobworkid { get; set; }
        public string jobworkno { get; set; }
        public Nullable<long> processInchargeId { get; set; }
        public Nullable<long> woid { get; set; }
        public string transferId { get; set; }
        public Nullable<long> Senderby { get; set; }
        public string JobStatus { get; set; }
        public Nullable<System.DateTime> startjobdate { get; set; }
        public string startjobtime { get; set; }
        public Nullable<System.DateTime> endjobdate { get; set; }
        public string endjobtime { get; set; }
        public Nullable<long> MachineId { get; set; }
        public Nullable<long> ProcessId { get; set; }
        public Nullable<long> OutItemName { get; set; }
        public Nullable<long> outAttributeId { get; set; }
        public Nullable<decimal> OutQty { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> IsStatus { get; set; }

        public Nullable<decimal> AcceptQty { get; set; }
        public Nullable<int> QC { get; set; }

        public virtual MFG_WORK_ORDEREntity WOEntity { get; set; }
        public virtual T_ITEM_TRANSFEREntity TransferEntity { get; set; }
        public virtual MachineEntity machineEntity { get; set; }
        public virtual M_ProcessMasterEntity ProcessEntity { get; set; }
        public virtual List<M_Jobwork_MaterialEntity> JBmaterial { get; set; }
        public virtual EmployeeEntity EmployeeEntity { get; set; }
        public virtual M_ItemMasterEntity ItemEntity { get; set; }


    }
}
