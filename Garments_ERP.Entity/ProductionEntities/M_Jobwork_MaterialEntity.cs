﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_Jobwork_MaterialEntity
    {
        public long j_materialId { get; set; }
        public Nullable<long> jobworkid { get; set; }
        public Nullable<long> ItemId { get; set; }
        public Nullable<long> unitid { get; set; }
        public Nullable<long> BatchId { get; set; }
        public Nullable<decimal> BatchQty { get; set; }
        public Nullable<decimal> ConsumedQty { get; set; }
        public Nullable<decimal> BalanceQty { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> ItemAttributeId { get; set; }

        public virtual M_ItemMasterEntity ItemEntity { get; set; }
        public virtual M_UnitMasterEntity UnitEntity { get; set; }
        public virtual M_Product_InfoEntity BatchEntity { get; set; }
        public virtual M_Product_InfoEntity BatchEntity1 { get; set; }

    }
}
