﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class MFG_Store_WO_ItemDetailsEntity
    {
        public long SWO_Item_DetailsId { get; set; }
        public Nullable<long> Store_WO_Id { get; set; }
        public string BrandName { get; set; }
        public Nullable<long> ItemCategoryId { get; set; }
        public string itemcatName { get; set; }
        public Nullable<long> ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemCode { get; set; }
        public Nullable<long> Item_Attribute_Id { get; set; }
        public string Item_Attribute_Value { get; set; }
        public Nullable<long> UnitId { get; set; }
        public string UnitName { get; set; }
        public Nullable<decimal> Quantity { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<long> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Updated_By { get; set; }
        public Nullable<System.DateTime> Updated__Date { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
    }
}
