﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.ProductionEntities
{
    public class MFG_WorkOrderProcessMaterialsEntity
    {
        public long WOProcessMaterial_ID { get; set; }
        public Nullable<long> WOProcessID { get; set; }
        public Nullable<long> ItemSubCategoryID { get; set; }
        public Nullable<long> ItemID { get; set; }
        public Nullable<decimal> RequiredQty { get; set; }
        public Nullable<decimal> IssuedQty { get; set; }
        public Nullable<System.DateTime> ItemCheckedDate { get; set; }
        public Nullable<int> ItemCheckedby { get; set; }
        public Nullable<System.DateTime> WOStartDate { get; set; }
        public Nullable<int> WOStartedBY { get; set; }
        public Nullable<System.DateTime> WOEndDate { get; set; }
        public Nullable<int> WOEndedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> UnitID { get; set; }
        public virtual MFG_WorkOrderProcessDetailsEntity MFG_WorkOrderProcessDetails { get; set; }
    }
}
