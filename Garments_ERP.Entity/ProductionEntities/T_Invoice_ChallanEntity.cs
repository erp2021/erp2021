﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class T_Invoice_ChallanEntity
    {
        public long InvoiceId { get; set; }
        public string InvoiceNo { get; set; }
        public Nullable<System.DateTime> InvoiceDate { get; set; }
        public string ChallanNo { get; set; }
        public Nullable<System.DateTime> ChallanDate { get; set; }
        public Nullable<long> PO_Id { get; set; }
        public Nullable<System.DateTime> PO_Date { get; set; }
        public Nullable<long> LedgerId { get; set; }
        public string DeliveryAdd { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<decimal> GST { get; set; }
        public Nullable<decimal> GSTAmount { get; set; }
        public Nullable<decimal> GrossTotal { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> UpdateBy { get; set; }
        public Nullable<System.DateTime> UpdateOn { get; set; }
        public Nullable<int> PaymentTerm { get; set; }
        public Nullable<System.DateTime> DuoOn { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> IsStatus { get; set; }
        public string productname { get; set; }
        public Nullable<decimal> PoQty { get; set; }
        public Nullable<decimal> InvQty { get; set; }


        public virtual CRM_CustomerPOMasterentity poent { get; set; }
        public virtual M_LedgersEntity LedgerEnt { get; set; }
        public virtual List<T_InvoiceChallan_ProductDetailsEntity> TInv_ProdEntity { get; set; }
        public virtual List<T_POOtherChargeEntity> POOtherChargeEntity { get; set; }
        public virtual M_PaymentTermMasterEntity PaytermEnt { get; set; }
    }
}
