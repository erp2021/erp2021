﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class MFG_MaterialIssueItemDetailsEntity
    {
        public long Id { get; set; }
        public long MaterialIssueid { get; set; }
        public long ItemID { get; set; }
        public int UOM_ID { get; set; }
        public Nullable<decimal> ReqQty { get; set; }
        public Nullable<decimal> AvailableQty { get; set; }
        public Nullable<decimal> IssueQty { get; set; }
        public Nullable<decimal> BalanceQty { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> ItemSubCategoryID { get; set; }
        public Nullable<decimal> Process_ID { get; set; }
        public int loggedouser { get; set; }
        public Nullable<int> Item_Attribute_ID { get; set; }
        public string stocktype { get; set; }
        public string itemBTval { get; set; }
        public string itemBTId { get; set; }

        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public virtual M_ItemMasterEntity M_ItemMaster { get; set; }
        public virtual M_UnitMasterEntity M_UnitMaster { get; set; }
        public virtual MFG_MaterialIssueMasterEntity MFG_MaterialIssueMaster { get; set; }
        public virtual M_ItemSubCategoryMasterEntity M_ItemSubCategoryMaster { get; set; }
        public virtual M_ProcessMasterEntity M_PROCESS_Master { get; set; }


    }

    public class StockTypePair
    {
        public string Name { get; set; }
        public string ID { get; set; }
    }
}
