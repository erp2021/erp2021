﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
  public  class MFG_WORK_ORDER_DETAILEntity
    {
        public long ID { get; set; }
        public long WorkOrderID { get; set; }
        public Nullable<int> RefStyleID { get; set; }
        public Nullable<long> SizeID { get; set; }
        public Nullable<decimal> Quantity { get; set; }
        public Nullable<int> UnitID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<decimal> ProductionQty { get; set; }
        public Nullable<decimal> BalanceQty { get; set; }
        public string Attribute_ID { get; set; }
        public string Attribute_Value_ID { get; set; }
        public Nullable<decimal> rate { get; set; }
        public Nullable<long> ItemId { get; set; }

        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public virtual M_ItemSizeMasterEntity M_ItemSizeMasterentity { get; set; }
        public virtual M_UnitMasterEntity M_Unitentity { get; set; }
        public virtual MFG_WORK_ORDEREntity MFG_WORK_ORDER { get; set; }

    }
}
