﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class MFG_Store_WO_BOM_ItemEntity
    {
        public Nullable<long> SWO_ItemId { get; set; }
        public Nullable<long> BOM_ID { get; set; }
        public Nullable<long> ItemSubCategoryId { get; set; }
        public string Itemsubcategory { get; set; }
        public Nullable<long> ItemId { get; set; }
        public string ItemName { get; set; }
        public Nullable<long> Item_Attribute_ID { get; set; }
        public Nullable<long> SupplierId { get; set; }
        public string SupplierName { get; set; }
        public Nullable<long> UnitId { get; set; }
        public string ItemUnit { get; set; }
        public Nullable<decimal> RequiredQty { get; set; }
        public Nullable<decimal> PerQty { get; set; }
        public Nullable<decimal> AvailableInStock { get; set; }
        public Nullable<long> Styleid { get; set; }
    }
}
