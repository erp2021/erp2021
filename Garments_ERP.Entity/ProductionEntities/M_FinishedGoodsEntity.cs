﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity.ProductionEntities
{
    public class M_FinishedGoodsEntity
    {
        public List<M_FinishedGoodsEntity> processcyclelist;

        public int Finished_Item_ID { get; set; }

        public int Package_Type_id { get; set; }
        public int Item_Attr_Id { get; set; }

        public int Item_sub_catg_id { get; set; }

        public int itmUOMid { get; set; }
        public decimal Batch_qty { get; set; }

        public int Item_ID { get; set; }
        public string itmUOM_val { get; set; }
        public decimal IsuueQty { get; set; }
        public int Finished_Goods_Product_ID { get; set; }
        public int Item_inward_id { get; set; }

        public string ItemName { get; set; }



        public Nullable<bool> IsActive { get; set; }

        //New Code Added by Rahul on 09-01-2020
        public Nullable<int> companyid { get; set; }
        public Nullable<int> branchid { get; set; }

        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }

        
    }
}
