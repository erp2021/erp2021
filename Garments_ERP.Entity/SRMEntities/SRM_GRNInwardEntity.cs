﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class SRM_GRNInwardEntity
    {
     public long ID { get; set; }
        public Nullable<long> WarehouseId { get; set; }
        public Nullable<long> SupplierId { get; set; }
        public string GRNNo { get; set; }
        public Nullable<System.DateTime> InwardDate { get; set; }
        public string ChallanNO { get; set; }
        public decimal TotProduceQty { get; set; }
        public Nullable<System.DateTime> ChallanDate { get; set; }
        public string InvoiceNo { get; set; }
        public Nullable<System.DateTime> InvoiceDate { get; set; }
        public Nullable<int> DepartmentId { get; set; }
        public Nullable<int> InwardBy { get; set; }
        public string InvoiceCreated { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> ProcessingStatusId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<long> POid { get; set; }
        public string POno { get; set; }
        public Nullable<decimal> InvoiceAmount { get; set; }
        public string EwayBillNo { get; set; }
        public string TruckNo { get; set; }
        public string Transporter { get; set; }

        public Nullable<int> IsStatus { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public string RackAllocation { get; set; }
        public Nullable<int> IsReadymade { get; set; }

        //public Nullable<int> BatchNO { get; set; }
        //public string SerialNo { get; set; }
        public List<M_ItemMasterEntity> itemlist { get; set; }
        public List<M_UnitMasterEntity> unitlist { get; set; }
        public virtual M_DepartmentMasterEntity M_DepartmentMaster { get; set; }
        public virtual SRM_PurchaseOrderMasterEntity SRM_PurchaseOrderMaster { get; set; }
        public virtual M_ProcessingStatusEntity M_ProcessingStatus { get; set; }
        public virtual M_SupplierMasterEntity M_SupplierMaster { get; set; }
        public virtual M_WarehouseEntity M_WarehouseMaster { get; set; }
        public virtual List<SRM_GRNItemEntity> SRM_GRNItemlist { get; set; }
        public virtual M_LedgersEntity M_Ledgersentity { get; set; }

        public virtual List<T_POOtherChargeEntity> POOtherChargeEntity { get; set; }
    }
}
