﻿using Garments_ERP.Entity.CommonEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class SRM_PurchaseOrderMasterEntity
    {

        public long Id { get; set; }
        public string PO_NO { get; set; }
        public Nullable<System.DateTime> PO_Date { get; set; }
        public Nullable<long> QuotationId { get; set; }
        public Nullable<System.DateTime> QuotationDate { get; set; }
        public Nullable<long> SupplierId { get; set; }
        public Nullable<long> ContactPersonId { get; set; }
        public string Comment { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Craetedby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public string POType { get; set; }
        public Nullable<decimal> ItemTotal { get; set; }
        public Nullable<decimal> TaxTotal { get; set; }
        public Nullable<decimal> GrandTotal { get; set; }
        public Nullable<System.DateTime> POValidityFrom { get; set; }
        public Nullable<System.DateTime> POValidityTo { get; set; }
        public Nullable<long> DeliveryLoaction { get; set; }
        public Nullable<int> PaymentTerm { get; set; }
        public string Narration { get; set; }
        public Nullable<bool> Approvalstatus { get; set; }
        public Nullable<System.DateTime> Approvaldate { get; set; }
        public Nullable<int> IsStatus { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<decimal> totalcost { get; set; }
        public Nullable<int> IsReadymade { get; set; }

        public List<M_RateIndicatorMasterEntity> rateindicatorlist { get; set; }
        public List<M_TaxMasterEntity> taxlist { get; set; }
        public M_TaxMasterEntity taxvaluelist { get; set; }
        public List<SRM_QuotationItemDetailEntity> rawitemlist { get; set; }
        public List<M_SupplierContactDetailsEntity> M_SupplierContacts { get; set; }
        public virtual M_SupplierContactDetailsEntity M_SupplierContactDetails { get; set; }
        public virtual M_SupplierMasterEntity M_SupplierMaster { get; set; }
        public SRM_PurchaseOrderItemDetailEntity itemdetailentity { get; set; }
        public List<SRM_PurchaseOrderItemDetailEntity> orderitemlist { get; set; }
        public virtual List<SRM_PurchaseOrderItemDetailEntity> SRM_PurchaseOrderItemDetail { get; set; }
        public virtual SRM_QuotationMasterEntity SRM_QuotationMaster { get; set; }
        public virtual M_PaymentTermMasterEntity M_PaymentTermMaster { get; set; }
        public virtual M_WarehouseEntity M_WAREHOUSE { get; set; }
        public List<M_ItemMasterEntity> Items { get; set; }
        public List<M_UnitMasterEntity> Unitlist { get; set; }
        public virtual M_LedgersEntity M_Ledgersentity { get; set; }
        public virtual M_Ledger_BillingDetailsEntity M_Ledger_BillDEntity { get; set; }
        public virtual List<MFG_PO_DeliveryDetail_Entity> MFG_PO_DeliveryDetail { get; set; }

        public virtual List<T_POOtherChargeEntity> POOtherChargeEntity { get; set; }
    }
}
