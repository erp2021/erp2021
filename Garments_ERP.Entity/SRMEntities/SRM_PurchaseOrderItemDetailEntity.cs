﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class SRM_PurchaseOrderItemDetailEntity
    {
        public long Id { get; set; }
        public Nullable<long> PO_ID { get; set; }
        public Nullable<long> ItemId { get; set; }
        public string ItemDesc { get; set; }
        public Nullable<int> UnitId { get; set; }
        public Nullable<decimal> TotalQty { get; set; }
        public Nullable<decimal> Provision { get; set; }
        public Nullable<decimal> Wastage { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<decimal> StockQty { get; set; }
        public Nullable<decimal> POQty { get; set; }
        public Nullable<decimal> Rate { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> RateIndicatorId { get; set; }
        public Nullable<int> TaxId { get; set; }
        public Nullable<decimal> TaxValue { get; set; }
        public Nullable<int> Item_Attribute_ID { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public virtual M_ItemMasterEntity M_ItemMaster { get; set; }
        public virtual M_RateIndicatorMasterEntity M_RateIndicatorMaster { get; set; }
        public virtual M_UnitMasterEntity M_UnitMaster { get; set; }
        public virtual SRM_PurchaseOrderMasterEntity SRM_PurchaseOrderMaster { get; set; }
        public virtual M_TaxMasterEntity M_TaxMaster { get; set; }
        public string rawitemimg { get; set; }

        public List<string> rawitemimg_list { get; set; }
        public Nullable<int> IsReadymade { get; set; }
    }
}
