﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Garments_ERP.Entity
{
    public class M_SupplierMasterEntity
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Enter Supplier Name")]
        [System.Web.Mvc.Remote("CheckSupplierName", "Supplier", ErrorMessage = "Supplier name already in use!", AdditionalFields = "Id")]
        public string Suppliername { get; set; }
        public string Suppliercode { get; set; }
        public Nullable<int> Suppliertype { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public Nullable<int> Cityid { get; set; }
        public Nullable<int> Stateid { get; set; }
        public Nullable<int> Countryid { get; set; }
        [RegularExpression(@"^[0-9]{2}[(a-z)(A-Z)]{5}\d{4}[(a-z)(A-Z)]{1}\d{1}Z[(0-9)(a-zA-Z)]{1}", ErrorMessage = "Enter valid GSTIN.")]
        public string GSTNo { get; set; }
        [RegularExpression(@"^([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$", ErrorMessage = "Enter valid email")]
        public string Email { get; set; }
        public string Remark { get; set; }
        public string CSTRegno { get; set; }
        public string Tinno { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public List<M_DepartmentMasterEntity> deptlist { get; set; }

        public virtual List<SRM_GRNInwardEntity> SRM_GRNInward { get; set; }
        public virtual List<M_SupplierContactDetailsEntity> M_SupplierContactDetails { get; set; }
        public virtual M_SupplierBankDetailsEntity M_SupplierBankDetails { get; set; }
        public virtual M_SupplierTypeMasterEntity M_SupplierTypeMaster { get; set; }
    }
}
