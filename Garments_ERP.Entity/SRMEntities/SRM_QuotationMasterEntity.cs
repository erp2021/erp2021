﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class SRM_QuotationMasterEntity
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Please select PR No.")]
        public string PR_No { get; set; }
        [Required(ErrorMessage = "Please select supplier.")]
        public Nullable<long> SupplierId { get; set; }
        public string QuotationNo { get; set; }
        public Nullable<System.DateTime> QuotationDate { get; set; }
        public Nullable<System.DateTime> Validity { get; set; }
        public string Narration { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<decimal> GSTTax { get; set; }
        public Nullable<decimal> OtherTax { get; set; }
        public Nullable<decimal> TotalCost { get; set; }
        public Nullable<long> PR_Id { get; set; }
       public string quotdate { get; set; }
        public Nullable<bool> POStatus { get; set; }
        public string Comment { get; set; }
        public Nullable<bool> Approvalstatus { get; set; }
        public Nullable<System.DateTime> Approvaldate { get; set; }
        public Nullable<int> IsStatus { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> IsReadymade { get; set; }
        public Nullable<decimal> GrandTotal { get; set; }

        public List<SRM_PurchaseRequestItemDetailEntity> rawitemlist { get; set; }
        public List<M_DiscountTypeMasterEntity> discounttypelist { get; set; }
        public List<M_RateIndicatorMasterEntity> rateindicatorlist { get; set; }
        public virtual M_SupplierMasterEntity M_SupplierMaster { get; set; }
        public virtual List<SRM_QuotationItemDetailEntity> SRM_QuotationItemDetail { get; set; }
        public virtual SRM_PurchaseRequestMasterEntity SRM_PurchaseRequestMaster { get; set; }
        public virtual M_LedgersEntity M_Ledgersentity { get; set; }
        public virtual M_Ledger_BillingDetailsEntity M_Ledger_Billentity { get; set; }
        public virtual List<T_POOtherChargeEntity> POOtherChargeEntity { get; set; }

    }
}
