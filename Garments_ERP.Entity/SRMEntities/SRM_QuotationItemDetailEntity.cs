﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class SRM_QuotationItemDetailEntity
    {
        public long Id { get; set; }
        public Nullable<long> QuotationId { get; set; }
        public Nullable<long> ItemId { get; set; }
        public string ItemDesc { get; set; }
        public Nullable<decimal> TotalQty { get; set; }
        public Nullable<decimal> ItemRate { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public Nullable<int> DiscountTypeId { get; set; }
        public Nullable<int> RateIndicatorId { get; set; }
        public Nullable<decimal> DiscountRate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> UnitId { get; set; }
        public string rawitemimg { get; set; }
        public Nullable<int> Item_Attribute_ID { get; set; }

        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public List<string> rawitemimg_list { get; set; }
        public Nullable<int> IsReadymade { get; set; }

        public List<M_TaxMasterEntity> taxlist { get; set; }
        public virtual M_DiscountTypeMasterEntity M_DiscountTypeMaster { get; set; }
        public virtual M_ItemMasterEntity M_ItemMaster { get; set; }
        public virtual M_RateIndicatorMasterEntity M_RateIndicatorMaster { get; set; }
        public virtual SRM_QuotationMasterEntity SRM_QuotationMaster { get; set; }
        public virtual M_UnitMasterEntity M_UnitMaster { get; set; }
    }
}
