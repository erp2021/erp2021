﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class SRM_PurchaseRequestEntity
    {
        public long Id { get; set; }
        public string PR_No { get; set; }
        public Nullable<System.DateTime> PR_Date { get; set; }
        public string Priority { get; set; }
        public string AdditionalInfo { get; set; }
        public Nullable<int> AccountHeadId { get; set; }
        public Nullable<int> DepartmentId { get; set; }
        public string Comment { get; set; }
        public Nullable<bool> ApprovalStatus { get; set; }
        public Nullable<System.DateTime> ApprovalDate { get; set; }
        public Nullable<int> IsStatus { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }

        public virtual M_DepartmentMasterEntity DepartmentMaster { get; set; }

        public virtual List<SRM_PRItemDetailEntity> ItemList { get; set; }
        public virtual EmployeeEntity Employee { get; set; }
    }
}
