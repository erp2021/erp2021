﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
   public class SRM_GRNItemEntity
    {
        public long ID { get; set; }
        public Nullable<int> ItemSourceType { get; set; }
        public Nullable<long> ItemInwardId { get; set; }
        public string PONo { get; set; }
        public Nullable<long> ItemId { get; set; }
        public Nullable<decimal> GRN_Qty { get; set; }
        public Nullable<decimal> PO_Qty { get; set; }
        public Nullable<decimal> Balance_Qty { get; set; }
        public Nullable<decimal> Challan_Qty { get; set; }
        public Nullable<decimal> Accepted_Qty { get; set; }
        public Nullable<bool> QC_Done { get; set; }
        public Nullable<int> QC_Id { get; set; }
        public Nullable<int> UnitId { get; set; }
        public Nullable<int> WarehouseId { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> BatchNO { get; set; }
        public string SerialNo { get; set; }
        public Nullable<int> Item_Attribute_ID { get; set; }
        public string itemPIId { get; set; }
        public string itemPIval { get; set; }
        public string BatchList { get; set; }

        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> IsStatus { get; set; }
        public Nullable<decimal> gstRate { get; set; }
        public string HSNCode { get; set; }

        public List<string> rawitemimg_list { get; set; }
        public Nullable<int> IsReadymade { get; set; }


        public virtual M_ItemMasterEntity M_ItemMaster { get; set; }
         public virtual M_UnitMasterEntity M_UnitMaster { get; set; }
        public virtual M_WarehouseMasterEntity M_WarehouseMaster { get; set; }
        public virtual SRM_GRNInwardEntity SRM_GRNInward { get; set; }
    }
}
