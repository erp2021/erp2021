﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_BOMSizeDetailMasterEntity
    {
        public long Id { get; set; }
        public Nullable<long> BOMD_Id { get; set; }
        public Nullable<long> SizeId { get; set; }
        public Nullable<decimal> Quantity { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public string Attribute_ID { get; set; }
        public string Attribute_Value_ID { get; set; }
        public Nullable<decimal> rate { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }

        public Nullable<int> Segmenttypeid { get; set; }
        public Nullable<long> Styleid { get; set; }
        public Nullable<long> Itemid { get; set; }
        public string Styleno { get; set; }
        public string Styledesc { get; set; }
        public string Brand { get; set; }
        public Nullable<System.DateTime> Createdate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<decimal> Totalqty { get; set; }
        public Nullable<decimal> SellRate { get; set; }
        public Nullable<decimal> GST { get; set; }
        public Nullable<decimal> Subtotal { get; set; }
        public List<string> itemimg_list { get; set; }

        public virtual M_BillOfMaterialDetailEntity M_BillOfMaterialDetail { get; set; }
        public virtual M_ItemSizeMasterEntity M_ItemSizeMaster { get; set; }
        public virtual M_StyleMasterEntity stylemaster { get; set; }
        public virtual M_ItemMasterEntity itemmaster { get; set; }
    }
}
