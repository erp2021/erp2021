﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
  public class CRM_QuotationMasterEntity
    {
        public long Id { get; set; }
        public string Enquiryrefno { get; set; }
        public long EnquiryID { get; set; }
        public string QuotationNo { get; set; }
        public Nullable<System.DateTime> Quotationdate { get; set; }
        public Nullable<long> customerid { get; set; }
        public string narration { get; set; }
        public Nullable<bool> Approvalstatus { get; set; }
        public Nullable<System.DateTime> Approvaldate { get; set; }
        public Nullable<int> Currencyid { get; set; }
        public Nullable<decimal> Exchangerate { get; set; }
        public Nullable<decimal> Costprice { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
        public Nullable<decimal> Tax { get; set; }
        public Nullable<System.DateTime> Dateofcosting { get; set; }
        public string Comment { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> Tentivedate { get; set; }
        public Nullable<bool> Sampling { get; set; }
        public Nullable<decimal> Subtotal { get; set; }
        public Nullable<decimal> Total { get; set; }
        public List<string> imglist { get; set; }
        public Nullable<long> Addressid { get; set; }
        public Nullable<bool> POStatus { get; set; }
        public string ProductName { get; set; }
        public Nullable<decimal> ProductQty { get; set; }
        public string StyleName { get; set; }
        public string ProductAttrib { get; set; }
        public Nullable<int> IsStatus { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> IsReadmade { get; set; }
        public Nullable<decimal> GrandTotal { get; set; }


        public List<CRM_QuotationStyleImageEntity> imgelist { get; set; }
        public CRM_QuotationStyleDetailEntity style { get; set; }

        public List<CRM_QuotationRawItemDetailEntity> rawitemlist { get; set; }
        public virtual M_CustomerMasterEntity CustomerMaster { get; set; }
        public virtual M_LedgersEntity mLedgers { get; set; }
        public virtual List<T_POOtherChargeEntity> POOtherChargeEntity { get; set; }

        public List<CRM_QuotationStyleDetailEntity> StyleEnt { get; set; }



    }
}
