﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
    public class M_BillOfMaterialMasterEntity
    {

        public long BOM_ID { get; set; }
        public string BOM_No { get; set; }
        public Nullable<decimal> RevisionNo { get; set; }
        public Nullable<long> EnquiryId { get; set; }
        public Nullable<long> StyleId { get; set; }
        public Nullable<long> SizeId { get; set; }
        public Nullable<long> CustomerId { get; set; }
        public Nullable<long> CustomerAddressId { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsPRDone { get; set; }
        public Nullable<long> Poid { get; set; }//New Add 300719
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public List<string> styleimglist { get; set; }
        public Nullable<int> IsStatus { get; set; }
        public Nullable<int> IsReadymate { get; set; }

        public Nullable<int> Prooce_Cycle_id { get; set; }
        public CRM_EnquiryStyleDetailEntity style { get; set; }
        public List<M_ItemMasterEntity> itemlist_ { get; set; }
        public List<M_UnitMasterEntity> unitlist_ { get; set; }
        public List<CRM_EnquiryRawitemDetailEntity> rawitemlist { get; set; }
        public List<CRM_EnquiryRawitemDetailEntity> itemlist { get; set; }
        public virtual CRM_EnquiryMasterEntity CRM_EnquiryMaster { get; set; }
        public List<CRM_EnquiryMasterEntity> Enquirylist { get; set; }
        public virtual List<M_BillOfMaterialDetailEntity> M_BillOfMaterialDetail { get; set; }
       // public List<M_ItemMasterEntity> ItemList { get; set; }
        public virtual M_BillOfMaterialDetailEntity M_BillOfMaterialDetails { get; set; }
        public virtual M_BOMSizeDetailMasterEntity M_BOMSizeDetails { get; set; }
        public virtual M_ContactAddressEntity M_ContactAddressMaster { get; set; }
        public virtual M_CustomerMasterEntity M_CustomerMaster { get; set; }
        public virtual M_ItemSizeMasterEntity M_ItemSizeMaster { get; set; }
        public virtual M_StyleMasterEntity M_StyleMaster { get; set; }
        public virtual M_ProcessCycleEntity M_ProcessCycleMaster { get; set; }

        public List<CRM_QuotationRawItemDetailEntity> porawitemlist { get; set; } //New Add 300719
        public List<CRM_CustoemrPOStyleSizeDetailEntity> poitemlist { get; set; }
        public virtual CRM_CustomerPOMasterentity CRM_POMaster { get; set; }
        public virtual M_Ledger_BillingDetailsEntity MLedgerBillEntity { get; set; }
        public virtual M_LedgersEntity MLedgerEntity { get; set; }
        public List<M_BOM_ProcessCycleEntity> BOM_ProcessCycle { get; set; }
    }
}
