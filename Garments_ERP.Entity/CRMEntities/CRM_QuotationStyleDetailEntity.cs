﻿using Garments_ERP.Entity.MasterEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
  public  class CRM_QuotationStyleDetailEntity
    {
        public long Id { get; set; }
        public Nullable<long> Quotationid { get; set; }
        public Nullable<int> Segmenttypeid { get; set; }
        public Nullable<long> Styleid { get; set; }
        public Nullable<long> Itemid { get; set; }
        public string ItemName { get; set; }
        public Nullable<int> Styleshadeid { get; set; }
        public Nullable<int> Stylecolorid { get; set; }
        public string Styleimage { get; set; }
        public Nullable<int> Reqqty { get; set; }
        public string Styleno { get; set; }
        public string Styledesc { get; set; }
        public string Brand { get; set; }
        public Nullable<System.DateTime> Createdate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Sizetype { get; set; }
        public Nullable<decimal> Sizeper { get; set; }
        public Nullable<decimal> Totalqty { get; set; }
        public List<StyleSizeEntity> stylesizelist { get; set; }
        public Nullable<long> Sizeid { get; set; }
        public string Attribute_ID { get; set; }
        public string Attribute_Value_ID { get; set; }
        public Nullable<decimal> rate { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public List<string> itemimg_list { get; set; }
        public Nullable<decimal> SellRate { get; set; }
        public Nullable<decimal> GST { get; set; }
        public Nullable<decimal> Subtotal { get; set; }
        public Nullable<long> Item_Attribute_ID { get; set; }
        public Nullable<int> StyleImgCount { get; set; }
        public string ItemUnit { get; set; }

        public Nullable<int> NopOrderItemId { get; set; }


        public virtual List<CRM_QuotationStyleImageEntity> quotstyleimg { get; set; }
        public virtual M_SegmentTypeMasterEntity SegTypeEnt { get; set; }
        public virtual M_StyleMasterEntity styleEnt { get; set; }
        public virtual M_ItemMasterEntity ItemEnt { get; set; }

        public virtual M_Attribute_MasterEntity AttrEnt { get; set; }
        public virtual M_Attribute_Value_MasterEntity AttrValEnt { get; set; }

        public virtual List<CRM_QuotationStyleImageEntity> StyleImage { get; set; }

    }
}
