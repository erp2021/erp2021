﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
   public class CRM_EnquiryStyleSizeDetailEntity
    {
        public long Id { get; set; }
        public Nullable<long> Enquirystyleid { get; set; }
        public string Sizetype { get; set; }
        public Nullable<decimal> SIzeper { get; set; }
        public Nullable<decimal> Sizepcs { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
