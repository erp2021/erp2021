﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
   public class CRM_CustoemrPOStyleSizeDetailEntity
    {
        public long Id { get; set; }
        public Nullable<long> POstyleid { get; set; }
        public string Sizetype { get; set; }
        public Nullable<decimal> SIzepcs { get; set; }
        public Nullable<decimal> Sizeper { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<long> Sizeid { get; set; }
        public string Attribute_ID { get; set; }
        public string Attribute_Value_ID { get; set; }
        public Nullable<decimal> rate { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }

        public List<string> itemimg_list { get; set; }
        public Nullable<decimal> SellRate { get; set; }
        public Nullable<decimal> GST { get; set; }
        public Nullable<decimal> Subtotal { get; set; }
    }
}
