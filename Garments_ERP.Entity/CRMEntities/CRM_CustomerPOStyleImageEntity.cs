﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
   public class CRM_CustomerPOStyleImageEntity
    {
        public long Id { get; set; }
        public string styleimg { get; set; }
        public Nullable<long> POid { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
