﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
public  class CRM_EnquiryMasterEntity
    {

        public long EnquiryId { get; set; }
        public string Enquiryno { get; set; }
        public Nullable<System.DateTime> Enquirydate { get; set; }
        public Nullable<long> customerid { get; set; }
        public string narration { get; set; }
        public Nullable<bool> Approvalstatus { get; set; }
        public Nullable<System.DateTime> Approvaldate { get; set; }
        public Nullable<int> Currencyid { get; set; }
        public Nullable<decimal> Exchangerate { get; set; }
        public Nullable<decimal> Costprice { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
        public Nullable<System.DateTime> Dateofcosting { get; set; }
        public string Comment { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> Tentivedate { get; set; }
        public Nullable<bool> Sampling { get; set; }
        public Nullable<long> Addressid { get; set; }
        public List<string> styleimglist { get; set; }
        public Nullable<long> BrokerId { get; set; }
        public string ProductName { get; set; }
        public Nullable<decimal> ProductQty { get; set; }
        public string StyleName { get; set; }
        public string ProductAttrib { get; set; }
        public Nullable<int> IsStatus { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<long> Enquirytype { get; set; }
        public Nullable<int> IsReadmade { get; set; }

        
        public virtual List<CRM_EnquiryStyleImageEntity> imgelist { get; set; }
        public virtual CRM_EnquiryStyleDetailEntity style { get; set; }
        public virtual List<CRM_EnquiryRawitemDetailEntity> rawitemlist { get; set;}
        public virtual M_CustomerMasterEntity M_CustomerMasterentity { get; set; }
        public virtual M_BrokerMasterEntity M_BrokerMaster { get; set; }
        public virtual M_LedgersEntity M_Ledgersentity { get; set; }
        public virtual M_LedgersEntity M_Ledgersentitybrok { get; set; }
        public virtual M_EnquiryTypeMasterEntity EnqType { get; set; }

        public virtual List<CRM_EnquiryStyleDetailsEntity> StyleEnt { get; set; }
    }
}
