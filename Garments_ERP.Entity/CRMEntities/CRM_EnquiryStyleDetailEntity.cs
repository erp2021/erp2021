﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
   public class CRM_EnquiryStyleDetailEntity
    {
        public long Id { get; set; }    
        public Nullable<long> Enquiryid { get; set; }
        public Nullable<int> Segmenttypeid { get; set; }
        public Nullable<long> Styleid { get; set; }
        public Nullable<long> Itemid { get; set; }
        public string ItemName { get; set; }
        public Nullable<int> Styleshadeid { get; set; }
        public Nullable<int> Stylecolorid { get; set; }
        public string Styleimage { get; set; }
        public Nullable<long> SizeId { get; set; }
        public Nullable<DateTime> Createdate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Styleno { get; set; }
        public string Styledesc { get; set; }
        public string Brand { get; set; }
        public Nullable<decimal> Reqqty { get; set; }
        public string Sizetype { get; set; }
        public Nullable<decimal> Sizeper { get; set; }
        public Nullable <decimal> Totalqty { get; set; }
        public Nullable<long> Sizeid { get; set; }
        public string Attribute_ID { get; set; }
        public string Attribute_Value_ID { get; set; }
        public Nullable<decimal> rate { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public List<string> itemimg_list { get; set; }

        public Nullable<long> Item_Attribute_ID { get; set; }

        public Nullable<int> StyleImgCount { get; set; }

        public virtual M_SegmentTypeMasterEntity segTMent { get; set; }
        public virtual M_StyleMasterEntity styleM { get; set; }
        public virtual M_ItemMasterEntity itemM { get; set; }

        public virtual List<StyleSizeEntity> stylesizelist { get; set; }

        public virtual List<CRM_EnquiryRawitemDetailEntity> MyRawItem { get; set; }


    }
}
