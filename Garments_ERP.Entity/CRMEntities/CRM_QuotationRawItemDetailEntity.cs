﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Entity
{
  public class CRM_QuotationRawItemDetailEntity
    {
        public long Id { get; set; }
        public Nullable<long> Quotationid { get; set; }
        public Nullable<long> Itemcategoryid { get; set; }
        public string ItemCategoryName { get; set; }
        public Nullable<int> Itemsubcategoryid { get; set; }
        public string SubCategoryName { get; set; }
        public Nullable<long> Itemid { get; set; }
        public string Supplier { get; set; }

        public Nullable<int> Unitid { get; set; }
        public Nullable<System.DateTime> Createdate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string imagepath { get; set; }
        public string Itemdesc { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public List<string> rawitemimg_list { get; set; }

        public Nullable<long> QuoStyleId { get; set; }

        public Nullable<int> RawImgCount { get; set; }

        public virtual M_ItemCategoryMasterEntity M_ItemCategoryMasterentity { get; set; }
        public virtual M_ItemMasterEntity M_ItemMasterentity { get; set; }
        public virtual M_ItemSubCategoryMasterEntity M_ItemSubCategoryMasterentity { get; set; }
        public virtual List<M_ItemMasterEntity> itemlist { get; set; }

    }
}
