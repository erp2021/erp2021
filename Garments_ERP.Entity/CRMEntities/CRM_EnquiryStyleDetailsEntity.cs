﻿using Garments_ERP.Entity.MasterEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Garments_ERP.Entity
{
    public class CRM_EnquiryStyleDetailsEntity
    {
        public long Id { get; set; }
        public Nullable<long> Enquiryid { get; set; }
        public Nullable<int> Segmenttypeid { get; set; }
        public Nullable<long> Styleid { get; set; }
        public Nullable<long> Itemid { get; set; }
        public string ItemName { get; set; }
        public string Styleno { get; set; }
        public string Styledesc { get; set; }
        public string Brand { get; set; }
        public Nullable<decimal> Reqqty { get; set; }
        public Nullable<long> Item_Attribute_ID { get; set; }
        public Nullable<long> Sizeid { get; set; }
        public Nullable<decimal> rate { get; set; }
        public Nullable<int> StyleImgCount { get; set; }
        public string Attribute_ID { get; set; }
        public string Attribute_Value_ID { get; set; }
        public Nullable<decimal> Totalqty { get; set; }
        public Nullable<long> NopOrderItemId { get; set; }
        public Nullable<DateTime> Createdate { get; set; }
        public Nullable<long> Createdby { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public string ItemUnit { get; set; }


        public virtual M_SegmentTypeMasterEntity SegTypeEnt { get; set; }
        public virtual M_StyleMasterEntity styleEnt { get; set; }
        public virtual M_ItemMasterEntity ItemEnt { get; set; }
        public virtual M_Attribute_MasterEntity AttrEnt { get; set; }
        public virtual M_Attribute_Value_MasterEntity AttrValEnt { get; set; }

        public virtual List<CRM_EnquiryStyleImageEntity> StyleImage { get; set; } 
    }
}
