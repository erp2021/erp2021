﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_Dashboard
    {
        public List<SP_GetDashboardEntity> Get(int companyid, int branchid, int userid)
        {
            List<SP_GetDashboardEntity> list = new List<SP_GetDashboardEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SP_GetDashboard(companyid, branchid, userid).ToList();
                foreach (var data in record)
                {
                    SP_GetDashboardEntity entity = new SP_GetDashboardEntity();
                    entity.aaction = data.aaction;
                    entity.color = data.color;
                    entity.title = data.title;
                    entity.countid = data.countid;
                    entity.opens = data.opens;
                    entity.pending = data.pending;
                    entity.inprogress = data.inprogress;
                    entity.completed = data.completed;
                    entity.Rejected = data.Rejected;
                    list.Add(entity);
                }
            }
            return list;
        }
    }
}
