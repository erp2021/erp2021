﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;

namespace Garments_ERP.Data.Admin
{
    public class cl_Customer
    {
        public List<M_CustomerMasterEntity> get()
        {
            List<M_CustomerMasterEntity> list = new List<M_CustomerMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_CustomerMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_CustomerMasterEntity entity = new M_CustomerMasterEntity();
                    entity.Id = item.Id;
                    entity.contactname = item.contactname;
                    entity.Customertype = item.Customertype;
                    entity.Phone = item.Phone;
                    entity.Address = item.Address;
                    entity.Cityid = item.Cityid;
                    entity.Stateid = item.Stateid;
                    entity.Countryid = item.Countryid;
                    entity.GSTNo = item.GSTNo;
                    entity.Email = item.Email;
                    entity.Remark = item.Remark;
                    entity.CSTRegno = item.CSTRegno;
                    entity.Tinno = item.Tinno;
                    entity.Createddate = item.Createddate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    list.Add(entity);
                }

            }
            return list;
        }
        public List<M_ContactAddressEntity> getAddresssbycustomerid(long custid)
        {
            List<M_ContactAddressEntity> list = new List<M_ContactAddressEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ContactAddressMaster.Where(x => x.customerid == custid).ToList();
                foreach (var item in records)
                {
                    M_ContactAddressEntity entity = new M_ContactAddressEntity();
                    entity.Id = item.Id;
                    entity.customerid = item.Id;
                    entity.Addess = item.Addess;
                    entity.Cityid = item.Cityid;
                    entity.Stateid = item.Stateid;
                    entity.Countryid = item.Countryid;
                    entity.Createddate = item.Createddate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    list.Add(entity);
                }

            }
            return list;
        }





        public long Insert(M_CustomerMasterEntity entity)
        {
            long id = 0;
            using (var db=new GarmentERPDBEntities())
            {
                M_CustomerMaster item = new M_CustomerMaster();
                item.Id = entity.Id;
                item.Customertype = entity.Customertype;
                item.contactname = entity.contactname;
                item.Address = entity.Address;
                item.Cityid = entity.Cityid;
                item.Countryid = entity.Countryid;
                item.CreatedBy = entity.CreatedBy;
                item.Createddate = entity.Createddate;
                item.CSTRegno = entity.CSTRegno;
                item.Email = entity.Email;
                item.GSTNo = entity.GSTNo;
                item.IsActive = entity.IsActive;
                item.Phone = entity.Phone;
                item.Remark = entity.Remark;
                item.Stateid = entity.Stateid;
                item.Tinno = entity.Tinno;
                item.UpdatedBy = entity.UpdatedBy;
                item.Updateddate = entity.Updateddate;
                db.M_CustomerMaster.Add(item);
                db.SaveChanges();
                id = item.Id;
            }
            return id;
        }
        


    }
}
