﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;

namespace Garments_ERP.Data.Admin
{
  public  class cl_QuotationRawItemDetail
    {

      public long Insert(CRM_QuotationRawItemDetailEntity record)
      {
          long id = 0;

          using (var db = new GarmentERPDBEntities())
          {
              CRM_QuotationRawItemDetail entity = new CRM_QuotationRawItemDetail();
              entity.Id = record.Id;
              entity.Quotationid = record.Quotationid;
              entity.Itemcategoryid = record.Itemcategoryid;
              entity.Itemsubcategoryid = record.Itemsubcategoryid;
              entity.Itemid = record.Itemid;
              entity.Supplier = record.Supplier;
              entity.Unitid = record.Unitid;
              entity.Itemdesc = record.Itemdesc;
              entity.Createdate = record.Createdate;
              entity.Createdby = record.Createdby;
              entity.Company_ID = record.Company_ID;
              entity.BranchId = record.BranchId;
              entity.IsActive = true;
              if(record.imagepath!="")
              {
                  entity.imagepath = record.imagepath;
              }
              db.CRM_QuotationRawItemDetail.Add(entity);
              db.SaveChanges();
              id = entity.Id;
                if (id > 0)
                {

                    if (record.rawitemimg_list.Count > 0)
                    {
                        foreach (var img in record.rawitemimg_list)
                        {
                            if (img != "")
                            {
                                CRM_QuotationRawItemImage imgrecord = new CRM_QuotationRawItemImage();
                                imgrecord.Id = 0;
                                imgrecord.Quotationrawitemdetailid = id;
                                imgrecord.image = img;
                                imgrecord.Createddate = DateTime.Now;
                                imgrecord.IsActive = true;
                                imgrecord.Company_ID = record.Company_ID;
                                imgrecord.BranchId = record.BranchId;
                                imgrecord.Createdby = record.Createdby;
                                db.CRM_QuotationRawItemImage.Add(imgrecord);
                                db.SaveChanges();
                            }
                        }
                    }
                }

            }
          return id;
      }


      public bool Update(CRM_QuotationRawItemDetailEntity entity)
      {
          bool isupdated = false;

          using (var db = new GarmentERPDBEntities())
          {
              var record = db.CRM_QuotationRawItemDetail.Find(entity.Id);
              record.Id = entity.Id;
              //record.Enquiryid = entity.Enquiryid;
              record.Itemcategoryid = entity.Itemcategoryid;
              record.Itemsubcategoryid = entity.Itemsubcategoryid;
              record.Itemid = entity.Itemid;
              record.Supplier = entity.Supplier;
              record.Unitid = entity.Unitid;
              record.Updateddate = entity.Updateddate;
              record.Updatedby = entity.Updatedby;
              record.Itemdesc = entity.Itemdesc;
              record.IsActive = true;

              db.SaveChanges();
              isupdated = true;

                if (entity.rawitemimg_list.Count > 0)
                {
                    var imgrec = db.CRM_QuotationRawItemImage.Where(x => x.Quotationrawitemdetailid == entity.Quotationid).ToList();
                    if (imgrec != null)
                    {
                        foreach (var img in imgrec)
                        {
                            var imgrecord = db.CRM_QuotationRawItemImage.Find(img.Id);
                            imgrecord.IsActive = false;
                            imgrecord.Updateddate = DateTime.Now;
                            db.SaveChanges();
                        }
                    }

                    foreach (var img in entity.rawitemimg_list)
                    {
                        if (img != "")
                        {
                            CRM_QuotationRawItemImage imgrecord = new CRM_QuotationRawItemImage();
                            imgrecord.Id = 0;
                            imgrecord.Quotationrawitemdetailid =entity.Id;
                            imgrecord.image = img;
                            imgrecord.Createddate = DateTime.Now;
                            imgrecord.IsActive = true;
                            imgrecord.Company_ID = entity.Company_ID;
                            imgrecord.BranchId = entity.BranchId;
                            imgrecord.Createdby = entity.Createdby;
                            db.CRM_QuotationRawItemImage.Add(imgrecord);
                            db.SaveChanges();
                        }
                    }
                }
            }

          return isupdated;

      }
      public List<CRM_QuotationRawItemDetailEntity> getbyquotationid(long id)
      {
          List<CRM_QuotationRawItemDetailEntity> entitylist = new List<CRM_QuotationRawItemDetailEntity>();
          using (var db = new GarmentERPDBEntities())
          {
              var records = db.CRM_QuotationRawItemDetail.Where(x => x.Quotationid == id && x.IsActive == true).ToList();
              foreach (var item in records)
              {
                  CRM_QuotationRawItemDetailEntity entity = new CRM_QuotationRawItemDetailEntity();
                  entity.Id = item.Id;
                  entity.Quotationid = item.Quotationid;
                  entity.Itemcategoryid = item.Itemcategoryid;
                  entity.Itemsubcategoryid = item.Itemsubcategoryid;
                    List<M_ItemMasterEntity> itementitylist = new List<M_ItemMasterEntity>();
                    var rawitemlist = db.M_ItemMaster.Where(x => x.Itemsubcategoryid == entity.Itemsubcategoryid).ToList();
                    if (rawitemlist != null)
                    {
                        foreach (var rawitem in rawitemlist)
                        {
                            M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                            itementity.Id = rawitem.Id;
                            itementity.ItemName = rawitem.ItemName;
                            itementitylist.Add(itementity);
                        }
                    }
                    entity.itemlist = itementitylist;
                    entity.Itemid = item.Itemid;
                  entity.Supplier = item.Supplier;
                  entity.Unitid = item.Unitid;

                  entity.Createdate = item.Createdate;
                  entity.Createdby = item.Createdby;
                  entity.Updateddate = item.Updateddate;
                  entity.Updatedby = item.Updatedby;
                  //entity. = item.Itemdesc;
                  if (item.Itemdesc == null)
                  {
                      entity.Itemdesc = "";
                  }
                  else
                  {
                      entity.Itemdesc = item.Itemdesc;
                  }
                  M_ItemCategoryMasterEntity itemcategory = new M_ItemCategoryMasterEntity();
                  if (item.M_ItemCategoryMaster != null)
                  {
                      itemcategory.Id = item.M_ItemCategoryMaster.Id;
                      itemcategory.Itemcategory = item.M_ItemCategoryMaster.Itemcategory;
                      entity.M_ItemCategoryMasterentity = itemcategory;

                  }
                  M_ItemSubCategoryMasterEntity itemsub = new M_ItemSubCategoryMasterEntity();
                  if (item.M_ItemSubCategoryMaster != null)
                  {
                      itemsub.Id = item.M_ItemSubCategoryMaster.Id;
                      itemsub.Itemsubcategory = item.M_ItemSubCategoryMaster.Itemsubcategory;
                      entity.M_ItemSubCategoryMasterentity = itemsub;

                  }

                    //M_ItemMasterEntity item_ = new M_ItemMasterEntity();
                    //if (item.M_ItemMaster != null)
                    //{
                    //    item_.Id = item.M_ItemMaster.Id;
                    //    item_.ItemName = item.M_ItemMaster.ItemName;
                    //    entity.M_ItemMasterentity = item_;
                    //}


                    entity.imagepath = item.imagepath;
                    var rawitemimg_ = db.CRM_QuotationRawItemImage.Where(x => x.Quotationrawitemdetailid == item.Id && x.IsActive == true).ToList();
                    List<string> imgdata = new List<string>();
                    if (rawitemimg_ != null)
                    {
                        foreach (var img in rawitemimg_)
                        {
                            imgdata.Add(img.image);
                        }
                    }
                    entity.rawitemimg_list = imgdata;

                    entitylist.Add(entity);
              }

          }
          return entitylist;
      }

      public bool deleterawitem(long id)
      {
          bool isdeleted = false;
         using(var db=new GarmentERPDBEntities())
         {
             var record = db.CRM_QuotationRawItemDetail.Find(id);
             record.IsActive = false;
             db.SaveChanges();
             isdeleted = true;
         }

          return isdeleted;
      }

    }
}
