﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_BillOfMaterialDetail
    {
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;

        public List<M_BillOfMaterialDetailEntity> get()
        {
            List<M_BillOfMaterialDetailEntity> list = new List<M_BillOfMaterialDetailEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_BillOfMaterialDetail.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_BillOfMaterialDetailEntity entity = new M_BillOfMaterialDetailEntity();
                    entity.ID = item.ID;
                    entity.ItemDescription = item.ItemDescription;
                    entity.ItemId = item.ItemId;
                    if (item.M_ItemMaster != null)
                    {
                        M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                        itementity.Id = item.M_ItemMaster.Id;
                        itementity.ItemName = item.M_ItemMaster.ItemName;
                        itementity.ItemCode = item.M_ItemMaster.ItemCode;
                        itementity.Itemsubcategoryid = item.M_ItemMaster.Itemsubcategoryid;
                        itementity.ItemCategoryid = item.M_ItemMaster.ItemCategoryid;
                        entity.M_ItemMaster = itementity;
                    }
                    entity.ItemSubCategoryId = item.ItemSubCategoryId;
                    if (item.M_ItemSubCategoryMaster != null)
                    {
                        M_ItemSubCategoryMasterEntity itemsubcateentity = new M_ItemSubCategoryMasterEntity();
                        itemsubcateentity.Id = item.M_ItemSubCategoryMaster.Id;
                        itemsubcateentity.Itemsubcategory = item.M_ItemSubCategoryMaster.Itemsubcategory;
                        itemsubcateentity.itemcategoryid = item.M_ItemSubCategoryMaster.itemcategoryid;
                        entity.M_ItemSubCategoryMaster = itemsubcateentity;
                    }
                    entity.RequiredQty = item.RequiredQty;
                   // entity.SupplierName = item.SupplierName;
                    //entity.SupplierId = item.SupplierId;
                    //if (item.M_SupplierMaster != null)
                    //{
                    //    M_SupplierMasterEntity supplierentity = new M_SupplierMasterEntity();
                    //    supplierentity.Id = item.M_SupplierMaster.Id;
                    //    supplierentity.Suppliername = item.M_SupplierMaster.Suppliername;
                    //    supplierentity.Suppliertype = item.M_SupplierMaster.Suppliertype;
                    //    supplierentity.Email = item.M_SupplierMaster.Email;
                    //    supplierentity.GSTNo = item.M_SupplierMaster.GSTNo;
                    //    entity.M_SupplierMaster = supplierentity;
                    //}
                    entity.UnitId = item.UnitId;
                    if (item.M_BillOfMaterialMaster != null)
                    {
                        M_BillOfMaterialMasterEntity bomentity = new M_BillOfMaterialMasterEntity();
                        bomentity.BOM_ID = item.M_BillOfMaterialMaster.BOM_ID;
                        bomentity.BOM_No = item.M_BillOfMaterialMaster.BOM_No;
                        bomentity.CustomerId = item.M_BillOfMaterialMaster.CustomerId;
                        bomentity.EnquiryId = item.M_BillOfMaterialMaster.EnquiryId;
                        entity.M_BillOfMaterialMaster = bomentity;
                    }
                    entity.BOM_ID = item.BOM_ID;
                    entity.Comment = item.Comment;
                    var sizelist = db.M_BOMSizeDetailMaster.Where(x => x.BOMD_Id == item.ID && x.IsActive == true).ToList();
                    List<M_BOMSizeDetailMasterEntity> sizeentitylis = new List<M_BOMSizeDetailMasterEntity>();
                    foreach (var size in sizelist)
                    {
                        M_BOMSizeDetailMasterEntity bomsizeentity = new M_BOMSizeDetailMasterEntity();
                        bomsizeentity.Id = size.Id;
                        bomsizeentity.BOMD_Id = size.BOMD_Id;
                        bomsizeentity.SizeId = size.SizeId;
                        bomsizeentity.Quantity = size.Quantity;
                        sizeentitylis.Add(bomsizeentity);
                    }
                    entity.sizelist_ = sizeentitylis; 
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.IsActive = item.IsActive;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    list.Add(entity);
                }
            }
            return list;
        }


        public List<M_BillOfMaterialDetailEntity> GetById(long id)
        {
            List<M_BillOfMaterialDetailEntity> list = new List<M_BillOfMaterialDetailEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                int cnt = 0;
                var records = db.M_BillOfMaterialDetail.Where(x => x.BOM_ID == id && x.IsActive == true).ToList();
                foreach (var item in records)
                {
                    M_BillOfMaterialDetailEntity entity = new M_BillOfMaterialDetailEntity();
                    entity.ID = item.ID;
                    entity.ItemDescription = item.ItemDescription;
                    entity.ItemId = item.ItemId;
                    entity.Item_Attribute_ID = item.Item_Attribute_ID;
                    entity.PerQty = item.PerQty;
                    int IAid = Convert.ToInt32(item.Item_Attribute_ID);
                    
                    if (item.M_ItemMaster != null)
                    {
                        M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                        int ISid = Convert.ToInt32(item.M_ItemMaster.ItemCategoryid);
                        itementity.Id = item.M_ItemMaster.Id;
                        itementity.ItemName = getItemName(item.M_ItemMaster.Id, IAid, ISid);
                        itementity.ItemCode = item.M_ItemMaster.ItemCode;
                        itementity.Itemsubcategoryid = item.M_ItemMaster.Itemsubcategoryid;
                        itementity.ItemCategoryid = item.M_ItemMaster.ItemCategoryid;
                        entity.M_ItemMaster = itementity;
                    }
                    entity.ItemSubCategoryId = item.ItemSubCategoryId;
                    if (item.M_ItemSubCategoryMaster != null)
                    {
                        M_ItemSubCategoryMasterEntity itemsubcateentity = new M_ItemSubCategoryMasterEntity();
                        itemsubcateentity.Id = item.M_ItemSubCategoryMaster.Id;
                        itemsubcateentity.Itemsubcategory = item.M_ItemSubCategoryMaster.Itemsubcategory;
                        itemsubcateentity.itemcategoryid = item.M_ItemSubCategoryMaster.itemcategoryid;
                        entity.M_ItemSubCategoryMaster = itemsubcateentity;
                    }
                    entity.RequiredQty = item.RequiredQty;
                    entity.SupplierId = item.SupplierId;
                    if (item.SupplierId!=null)
                    {
                        var supname = db.M_Ledgers.Where(x => x.Ledger_Id == item.SupplierId).Select(x => x.Ledger_Name).FirstOrDefault();
                        M_LedgersEntity ledent = new M_LedgersEntity();
                        ledent.Ledger_Name = supname;
                        entity.LedgerEntity = ledent;
                    }
                    else
                    {
                        var enq = db.M_BillOfMaterialMaster.Where(x => x.BOM_ID == item.BOM_ID).Select(x => x.EnquiryId).FirstOrDefault();
                        var POid = enq == null ? db.M_BillOfMaterialMaster.Where(x => x.BOM_ID == item.BOM_ID).Select(x => x.Poid).FirstOrDefault() : null;
                        if (enq != null)
                        {
                            var supname = db.CRM_EnquiryRawitemDetail.Where(x => x.Enquiryid == enq).Select(x => x.Supplier).FirstOrDefault();
                            M_LedgersEntity ledent = new M_LedgersEntity();
                            ledent.Ledger_Name = supname;
                            ledent.Ledger_Id = db.M_Ledgers.Where(x => x.Ledger_Name == supname).Select(x => x.Ledger_Id).FirstOrDefault();
                            entity.SupplierId = db.M_Ledgers.Where(x => x.Ledger_Name == supname).Select(x => x.Ledger_Id).FirstOrDefault();
                            entity.LedgerEntity = ledent;
                        }
                        else
                        {
                            var Quot =db.CRM_CustomerPOMaster.Where(x => x.Id == POid).Select(x => x.QuotID).FirstOrDefault();
                            enq = db.CRM_QuotationMaster.Where(x => x.Id == Quot).Select(x => x.EnquiryID).FirstOrDefault();
                            var supname = db.CRM_EnquiryRawitemDetail.Where(x => x.Enquiryid == enq).Select(x => x.Supplier).FirstOrDefault();
                            M_LedgersEntity ledent = new M_LedgersEntity();
                            ledent.Ledger_Name = supname;
                            ledent.Ledger_Id = db.M_Ledgers.Where(x => x.Ledger_Name == supname).Select(x => x.Ledger_Id).FirstOrDefault();
                            entity.SupplierId = db.M_Ledgers.Where(x => x.Ledger_Name == supname).Select(x => x.Ledger_Id).FirstOrDefault();
                            entity.LedgerEntity = ledent;
                        }
                    }

                    entity.UnitId = item.UnitId;
                    M_UnitMasterEntity uent = new M_UnitMasterEntity();
                    if (item.UnitId!=null)
                    {  
                        uent.ItemUnit = db.M_UnitMaster.Where(x => x.Id == item.UnitId).Select(x => x.ItemUnit).FirstOrDefault();
                    }
                    else
                    {
                        uent.ItemUnit = "";
                    }
                    entity.M_UnitMaster = uent;

                    if (item.M_BillOfMaterialMaster != null)
                    {
                        M_BillOfMaterialMasterEntity bomentity = new M_BillOfMaterialMasterEntity();
                        bomentity.BOM_ID = item.M_BillOfMaterialMaster.BOM_ID;
                        bomentity.BOM_No = item.M_BillOfMaterialMaster.BOM_No;
                        bomentity.CustomerId = item.M_BillOfMaterialMaster.CustomerId;
                        bomentity.EnquiryId = item.M_BillOfMaterialMaster.EnquiryId;
                        entity.M_BillOfMaterialMaster = bomentity;
                    }
                    entity.BOM_ID = item.BOM_ID;
                    entity.Comment = item.Comment;
                    //var sizelist = db.M_BOMSizeDetailMaster.Where(x => x.BOMD_Id == item.ID && x.IsActive == true).ToList();
                    //List<M_BOMSizeDetailMasterEntity> sizeentitylis = new List<M_BOMSizeDetailMasterEntity>();
                    //foreach (var size in sizelist)
                    //{
                    //    M_BOMSizeDetailMasterEntity bomsizeentity = new M_BOMSizeDetailMasterEntity();
                    //    bomsizeentity.Id = size.Id;
                    //    bomsizeentity.BOMD_Id = size.BOMD_Id;
                    //    bomsizeentity.SizeId = size.SizeId;
                    //    bomsizeentity.Quantity = size.Quantity;
                    //    sizeentitylis.Add(bomsizeentity);
                    //}
                    //entity.sizelist_ = sizeentitylis;                  
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.IsActive = item.IsActive;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    
                    List<string> rawimgdata = new List<string>();
                    var rawimg = db.M_BOMRawItemImage.Where(x => x.BOMrawitemdetailid == item.ID).ToList();
                    if (rawimg != null)
                    {
                        foreach (var img in rawimg)
                        {
                            if (img.image != "")
                            {
                                rawimgdata.Add(img.image);
                            }
                        }
                    }
                    entity.rawitemimg_list = rawimgdata;

                    cnt = cnt + 1;
                    list.Add(entity);

                }
            }
            return list;
        }


        public List<M_BOMSizeDetailMasterEntity> GetBOMStyle(long bomid)
        {
            try
            {
                List<M_BOMSizeDetailMasterEntity> list = new List<M_BOMSizeDetailMasterEntity>();
                using (var db = new GarmentERPDBEntities())
                {
                    var tot = 0;
                    var bomstyle = db.M_BOMSizeDetailMaster.Where(x => x.BOMD_Id == bomid && x.IsActive==true).ToList();
                    foreach(var data in bomstyle)
                    {
                        M_BOMSizeDetailMasterEntity entity = new M_BOMSizeDetailMasterEntity();
                        entity.Id = data.Id;
                        entity.BOMD_Id = data.BOMD_Id;
                        entity.SizeId = data.SizeId;
                        entity.Quantity = data.Quantity;
                        entity.IsActive = data.IsActive;
                        entity.Attribute_ID = data.Attribute_ID;
                        entity.Attribute_Value_ID = data.Attribute_Value_ID;
                        entity.rate = data.rate;
                        entity.Company_ID = data.Company_ID;
                        entity.BranchId = data.BranchId;
                        entity.Segmenttypeid = data.Segmenttypeid;
                        entity.Styleid = data.Styleid;
                        entity.Itemid = data.Itemid;
                        entity.Styleno = data.Styleno;
                        entity.Styledesc = data.Styledesc;
                        entity.Brand = data.Brand;
                        entity.Createdate = data.Createdate;
                        entity.Createdby = data.Createdby;
                        entity.Updateddate = data.Updateddate;
                        entity.Updatedby = data.Updatedby;
                        if (data.Totalqty != null)
                        {
                            entity.Totalqty = data.Totalqty;
                        }
                        else
                        {
                            entity.Totalqty= tot + data.Quantity;
                        }
                        entity.SellRate = data.SellRate;
                        entity.GST = data.GST;
                        entity.Subtotal = data.Subtotal;
                        list.Add(entity);
                    }
                }

                return list;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string getItemName(long itemid, int attibid, int subcatid)
        {
            string itemname = "";
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 32; //Check Bill Of Material Create Or Not Using Customer PO 
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = Convert.ToString(subcatid);
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "<root><Row><ItemId>" + itemid + "</ItemId><AttributeId>" + attibid + "</AttributeId></Row></root>";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                foreach (DataTable table in dt.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        itemname = Convert.ToString(dr["ItemName"]);
                    }
                }

            }
            return itemname;
        }

        public long Insert(M_BillOfMaterialDetailEntity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_BillOfMaterialDetail item = new M_BillOfMaterialDetail();
                item.ID = entity.ID;
                item.ItemId = entity.ItemId;
                item.Item_Attribute_ID = entity.Item_Attribute_ID;
                item.BOM_ID = entity.BOM_ID;
                item.Comment = entity.Comment;
                item.ItemDescription = entity.ItemDescription;
                item.ItemSubCategoryId = entity.ItemSubCategoryId;
                item.RequiredQty = entity.RequiredQty;
                if(entity.SupplierName!="")
                {
                    long subid = db.M_Ledgers.Where(x => x.Ledger_Name.Contains(entity.SupplierName)).Select(x => x.Ledger_Id).FirstOrDefault();
                    item.SupplierId = subid >0 ? subid : 0;
                }
                else
                {
                    item.SupplierId = entity.SupplierId;
                }
                item.UnitId = entity.UnitId;
                item.CreatedBy = entity.CreatedBy;
                item.CreatedDate = DateTime.Now;
                item.IsActive = entity.IsActive;
                item.PerQty = entity.PerQty;
                item.Company_ID = entity.Company_ID;
                item.BranchId = entity.BranchId;
                db.M_BillOfMaterialDetail.Add(item);
                db.SaveChanges();
                id = item.ID;
                if (id > 0)
                {
                    if (entity.rawitemimg_list.Count > 0)
                    {
                        foreach (var img in entity.rawitemimg_list)
                        {
                            if (img != "")
                            {
                                M_BOMRawItemImage imgrecord = new M_BOMRawItemImage();
                                imgrecord.Id = 0;
                                imgrecord.BOMrawitemdetailid = id;
                                imgrecord.image = img;
                                imgrecord.Createddate = DateTime.Now;
                                imgrecord.IsActive = true;
                                imgrecord.Company_ID = entity.Company_ID;
                                imgrecord.BranchId = entity.BranchId;
                                imgrecord.Createdby = entity.CreatedBy;
                                db.M_BOMRawItemImage.Add(imgrecord);
                                db.SaveChanges();
                            }
                        }
                    }
                }

            }
            return id;
        }
        public bool Update(M_BillOfMaterialDetailEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_BillOfMaterialDetail.Where(x => x.ID == entity.ID).FirstOrDefault();
                record.BOM_ID = entity.BOM_ID;
                record.ID = entity.ID;
                record.Comment = entity.Comment;
                record.ItemDescription = entity.ItemDescription;
                record.ItemId = entity.ItemId;
                record.Item_Attribute_ID = entity.Item_Attribute_ID;
                record.ItemSubCategoryId = entity.ItemSubCategoryId;
                record.RequiredQty = entity.RequiredQty;
                record.UnitId = entity.UnitId;
                record.IsActive = entity.IsActive;
                record.UpdatedBy = entity.UpdatedBy;
                record.UpdatedDate =DateTime.Now;
                try
                {
                    db.SaveChanges();
                    IsUpdated = true;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return IsUpdated;
        }
        public bool Delete(long id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var contact = db.M_BillOfMaterialDetail.Where(x => x.ID == id).FirstOrDefault();
                contact.IsActive = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
                return IsDeleted;

            }
        }

        public List<M_BillOfMaterialMasterEntity> GetBOMList(long styleid)
        {
            try
            {
                List<M_BillOfMaterialMasterEntity> list = new List<M_BillOfMaterialMasterEntity>();
                using (var db = new GarmentERPDBEntities())
                {
                    var bomRec = db.M_BillOfMaterialMaster.Where(x => x.StyleId == styleid && x.IsActive==true).ToList();
                    foreach(var rec in bomRec)
                    {
                        M_BillOfMaterialMasterEntity ent = new M_BillOfMaterialMasterEntity();
                        ent.BOM_ID = rec.BOM_ID;
                        ent.BOM_No = rec.BOM_No;
                        list.Add(ent);

                    }
                }
                return list;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public M_StyleMasterEntity GetStyleDetails(long itemid)
        {
            try
            {
                M_StyleMasterEntity record = new M_StyleMasterEntity();
                using (var db = new GarmentERPDBEntities())
                {
                    var Enqstyle = db.CRM_EnquiryStyleDetail.Where(x => x.Itemid == itemid).FirstOrDefault();
                    if (Enqstyle != null)
                    {
                        long styleid = Convert.ToInt64(Enqstyle.Styleid);
                        var StyleRec = db.M_StyleMaster.Where(x => x.Id == styleid).FirstOrDefault();
                        record.Id = StyleRec.Id;
                        record.Stylename = StyleRec.Stylename;
                        record.StyleNo = StyleRec.StyleNo;
                        record.Styledescription = StyleRec.Styledescription;
                    }
                    else
                    {
                        var entity = db.M_ItemMaster.Where(x => x.Id == itemid).FirstOrDefault();
                        if (entity.ItemStyleid != null && entity.ItemStyleid > 0)
                        {
                            long styleid = Convert.ToInt64(entity.ItemStyleid);
                            var StyleRec = db.M_StyleMaster.Where(x => x.Id == styleid).FirstOrDefault();
                            record.Id = StyleRec.Id;
                            record.Stylename = StyleRec.Stylename;
                            record.StyleNo = StyleRec.StyleNo;
                            record.Styledescription = StyleRec.Styledescription;
                        }
                        else
                        {
                                record.Id = 0;
                        }
                    }
                }
                return record;
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
