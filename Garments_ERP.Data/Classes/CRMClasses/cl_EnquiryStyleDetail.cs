﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;

namespace Garments_ERP.Data.Admin
{
    public class cl_EnquiryStyleDetail
    {
        cl_GRNInward grnobj = new cl_GRNInward();
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;
        public CRM_EnquiryStyleDetailEntity getbyenquiryid(long id)
        {
            CRM_EnquiryStyleDetailEntity entity = new CRM_EnquiryStyleDetailEntity();
            using(var db=new GarmentERPDBEntities())
            {
                var recordlist = db.CRM_EnquiryStyleDetail.Where(x => x.Enquiryid == id).Select(x=>new {x.Enquiryid,x.Segmenttypeid,x.Styleid,x.Itemid,x.Brand,x.Styleno,x.Styledesc,x.Totalqty,x.SizeId }).Distinct();
                var record = recordlist.FirstOrDefault();

                if(record!=null)
                {
                 
                    entity.Enquiryid = record.Enquiryid;
                    entity.Segmenttypeid = record.Segmenttypeid;
                    var segName = db.M_SegmentTypeMaster.Where(x => x.Id == record.Segmenttypeid).Select(x => x.Segmenttype).FirstOrDefault();
                    M_SegmentTypeMasterEntity segent = new M_SegmentTypeMasterEntity();
                    segent.Segmenttype = segName;
                    entity.segTMent = segent;

                    entity.Styleid = record.Styleid;
                    var stylename = db.M_StyleMaster.Where(x => x.Id ==record.Styleid).Select(x => x.Stylename).FirstOrDefault();
                    M_StyleMasterEntity styent = new M_StyleMasterEntity();
                    styent.Stylename = stylename;
                    entity.styleM = styent;


                    entity.Itemid = record.Itemid;
                    var itemname = db.M_ItemMaster.Where(x => x.Id == record.Itemid).Select(x => x.ItemName).FirstOrDefault();
                    M_ItemMasterEntity itement = new M_ItemMasterEntity();
                    int IAid = Convert.ToInt32(record.SizeId);
                    int ISid = 0;
                    long itemid = Convert.ToInt64(record.Itemid);
                    itement.ItemName = grnobj.getItemName(itemid, IAid, ISid);

                    entity.itemM = itement;
                    entity.Brand = record.Brand;
                    entity.Styleno = record.Styleno;
                    entity.Styledesc = record.Styledesc;
                    entity.Totalqty = record.Totalqty;
                }

                List<StyleSizeEntity> sizelist = new List<StyleSizeEntity>();
                var StyleD = db.M_ItemSizeMaster.Where(x => x.Itemid == record.Itemid && x.Styleid == record.Styleid).Select(x => x.Id).ToArray();
                var StyleT = db.M_ItemSizeMaster.Where(x => x.Itemid == record.Itemid && x.Styleid == record.Styleid).Select(x => x.Sizetype).ToArray();
                
            }
          return entity;
         }

        public bool Update(CRM_EnquiryStyleDetailEntity entity)
        {
            bool isdeleted = false;
            using(var db=new GarmentERPDBEntities())
            {
                var record = db.CRM_EnquiryStyleDetail.Find(entity.Id);
                record.Segmenttypeid = entity.Segmenttypeid;
                record.Styleid = entity.Styleid;
                record.Itemid = entity.Itemid;
                record.Styleshadeid = 0;
                record.Stylecolorid = 0;
                record.Brand = entity.Brand;
                record.Styleno = entity.Styleno;
                record.Styledesc = entity.Styledesc;
                record.Styleimage = entity.Styleimage;
                record.Reqqty = entity.Reqqty;
                record.Totalqty = entity.Totalqty;
                record.Updateddate = entity.Updateddate;
                record.Updatedby = entity.Updatedby;
                record.Attribute_ID = entity.Attribute_ID;
                record.Attribute_Value_ID = entity.Attribute_Value_ID;
                record.rate = entity.rate;
                record.IsActive = true;
                record.SizeId = entity.SizeId;
                db.SaveChanges();
                isdeleted = true;
                if (entity.itemimg_list.Count >0)
                {
                    var imgrec = db.M_ImageMaster.Where(x => x.ItemId == entity.Itemid && x.Item_Attribute_ID==entity.SizeId && x.IsCustomize==entity.Enquiryid).ToList();
                    if(imgrec!=null)
                    {
                        foreach(var img in imgrec)
                        {
                            var imgrecord = db.M_ImageMaster.Find(img.Id);
                            imgrecord.IsActive = false;
                            imgrecord.Updateddate = DateTime.Now;
                            db.SaveChanges();
                        }
                    }

                    foreach (var img in entity.itemimg_list)
                    {
                        if (img != "")
                        {
                            M_ImageMaster record1 = new M_ImageMaster();
                            record1.ItemId = entity.Itemid;
                            record1.Item_Attribute_ID = (int)entity.SizeId;
                            record1.StyleId = (int)entity.Styleid;
                            record1.image = img;
                            record1.IsCustomize = (int)entity.Enquiryid;
                            record1.Createddate = entity.Createdate;
                            record1.Createdby = entity.Createdby;

                            //New Code Added by Rahul on 09-01-2020
                            //record.Updatedby = entity.Updatedby;
                            //record.Updateddate = entity.Updateddate;
                            record1.Company_ID = entity.Company_ID;
                            record1.BranchId = entity.BranchId;
                            record1.IsActive = entity.IsActive;
                            db.M_ImageMaster.Add(record1);
                            db.SaveChanges();
                        }
                    }
                }

            }
          return isdeleted;

        }
     
        public long Insert(CRM_EnquiryStyleDetailEntity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                CRM_EnquiryStyleDetail record = new CRM_EnquiryStyleDetail();
                record.Id = entity.Id;
                record.Enquiryid = entity.Enquiryid;
                record.Segmenttypeid = entity.Segmenttypeid;
                record.Styleid = entity.Styleid;
                record.Itemid = entity.Itemid;
                record.Styleshadeid = 0;
                record.Stylecolorid = 0;
                record.Brand = entity.Brand;
                record.Styleno = entity.Styleno;
                record.Styledesc = entity.Styledesc;
                record.Styleimage = entity.Styleimage;
                record.Reqqty = entity.Reqqty;
                record.Totalqty = entity.Totalqty;
                record.Createdby = entity.Createdby;
                record.Createdate = entity.Createdate;
                record.Attribute_ID = entity.Attribute_ID;
                record.Attribute_Value_ID = entity.Attribute_Value_ID;
                record.rate = entity.rate;
                record.IsActive = true;
                record.SizeId = entity.SizeId;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.BranchId;
                db.CRM_EnquiryStyleDetail.Add(record);
                db.SaveChanges();
                id = record.Id;

                if(id>0)
                {
                    if(entity.itemimg_list.Count>0)
                    {
                        foreach(var img in entity.itemimg_list)
                        {
                            if (img != "")
                            {
                                M_ImageMaster record1 = new M_ImageMaster();
                                record1.ItemId = entity.Itemid;
                                record1.Item_Attribute_ID =(int)entity.SizeId;
                                record1.StyleId =(int)entity.Styleid;
                                record1.image = img;
                                record1.IsCustomize =(int)entity.Enquiryid;
                                record1.Createddate = entity.Createdate;
                                record1.Createdby = entity.Createdby;

                                //New Code Added by Rahul on 09-01-2020
                                //record.Updatedby = entity.Updatedby;
                                //record.Updateddate = entity.Updateddate;
                                record1.Company_ID = entity.Company_ID;
                                record1.BranchId = entity.BranchId;
                                record1.IsActive = entity.IsActive;
                                db.M_ImageMaster.Add(record1);
                                db.SaveChanges();
                            }
                        }
                    }
                }
                
            }
            return id;
        }

        public object EditAttributeValue(string MyJson, int type, XmlDocument doc)
        {
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = type;
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = MyJson;
                cmd.Parameters.Add("attributexml", SqlDbType.VarChar).Value = doc.InnerXml;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

            }
            return dt;
        }

        

    }
}
