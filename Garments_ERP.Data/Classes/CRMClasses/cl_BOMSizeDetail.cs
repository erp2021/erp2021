﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_BOMSizeDetail
    {
        public List<M_BOMSizeDetailMasterEntity> get()
        {
            List<M_BOMSizeDetailMasterEntity> list = new List<M_BOMSizeDetailMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_BOMSizeDetailMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_BOMSizeDetailMasterEntity entity = new M_BOMSizeDetailMasterEntity();
                    entity.Id = item.Id;
                    entity.BOMD_Id = item.BOMD_Id;
                    ////if (item.M_BillOfMaterialDetail != null)
                    ////{
                    ////    M_BillOfMaterialDetailEntity bomdentity = new M_BillOfMaterialDetailEntity();
                    ////    bomdentity.ID = item.M_BillOfMaterialDetail.ID;
                    ////    bomdentity.RequiredQty = item.M_BillOfMaterialDetail.RequiredQty;
                    ////    entity.M_BillOfMaterialDetail = bomdentity;
                    ////}
                    entity.SizeId = item.SizeId;
                    entity.Quantity = item.Quantity;
                    entity.IsActive = item.IsActive;
                    list.Add(entity);
                }
            }
            return list;
        }
        public M_BOMSizeDetailMasterEntity GetById(long id)
        {
            M_BOMSizeDetailMasterEntity entity = new M_BOMSizeDetailMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.M_BOMSizeDetailMaster.Find(id);
                entity.Id = item.Id;
                entity.BOMD_Id = item.BOMD_Id;
                //if (item.M_BillOfMaterialDetail != null)
                //{
                //    M_BillOfMaterialDetailEntity bomdentity = new M_BillOfMaterialDetailEntity();
                //    bomdentity.ID = item.M_BillOfMaterialDetail.ID;
                //    bomdentity.RequiredQty = item.M_BillOfMaterialDetail.RequiredQty;
                //    entity.M_BillOfMaterialDetail = bomdentity;
                //}
                entity.SizeId = item.SizeId;
                entity.Quantity = item.Quantity;
                entity.IsActive = item.IsActive;
            }

            return entity;
        }
        public decimal Insert(M_BOMSizeDetailMasterEntity entity)
        {
            decimal insertedid = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_BOMSizeDetailMaster record = new M_BOMSizeDetailMaster();
                record.Id = entity.Id;
                record.BOMD_Id = entity.BOMD_Id;
                record.Quantity = entity.Quantity;
                record.SizeId = entity.SizeId;
                record.IsActive = entity.IsActive;
                record.Attribute_ID = entity.Attribute_ID;
                record.Attribute_Value_ID = entity.Attribute_Value_ID;
                record.rate = entity.rate;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.BranchId;
                record.Segmenttypeid = entity.Segmenttypeid;
                record.Styleid = entity.Styleid;
                record.Itemid = entity.Itemid;
                record.Styleno = entity.Styleno;
                record.Styledesc = entity.Styledesc;
                record.Brand = entity.Brand;
                record.Createdby = entity.Createdby;
                record.Createdate = DateTime.Now;
                record.Totalqty = entity.Totalqty;
                record.SellRate = entity.SellRate;
                record.GST = entity.GST;
                record.Subtotal = entity.Subtotal;
                try
                {
                    db.M_BOMSizeDetailMaster.Add(record);
                    db.SaveChanges();
                    insertedid = record.Id;

                    if (insertedid > 0)
                    {
                        if (entity.itemimg_list.Count > 0)
                        {
                            foreach (var img in entity.itemimg_list)
                            {
                                if (img != "")
                                {
                                    M_ImageMaster record1 = new M_ImageMaster();
                                    record1.ItemId = entity.Itemid;
                                    record1.Item_Attribute_ID = (int)entity.SizeId;
                                    record1.StyleId = (int)entity.Styleid;
                                    record1.image = img;
                                    record1.IsCustomize = (int)entity.BOMD_Id;
                                    record1.Createddate = entity.Createdate;
                                    record1.Createdby = entity.Createdby;

                                    //New Code Added by Rahul on 09-01-2020
                                    //record.Updatedby = entity.Updatedby;
                                    //record.Updateddate = entity.Updateddate;
                                    record1.Company_ID = entity.Company_ID;
                                    record1.BranchId = entity.BranchId;
                                    record1.IsActive = entity.IsActive;
                                    db.M_ImageMaster.Add(record1);
                                    db.SaveChanges();
                                }
                            }
                        }
                    }

                }
                catch (Exception)
                {

                    throw;
                }
            }
            return insertedid;
        }
        public bool Update(M_BOMSizeDetailMasterEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_BOMSizeDetailMaster.Where(x => x.Id == entity.Id).FirstOrDefault();
                if (record != null)
                {
                    record.Id = entity.Id;
                    record.BOMD_Id = entity.BOMD_Id;
                    record.Quantity = entity.Quantity;
                    record.SizeId = entity.SizeId;
                    record.IsActive = entity.IsActive;
                    record.Attribute_ID = entity.Attribute_ID;
                    record.Attribute_Value_ID = entity.Attribute_Value_ID;
                    record.rate = entity.rate;
                    record.Segmenttypeid = entity.Segmenttypeid;
                    record.Styleid = entity.Styleid;
                    record.Itemid = entity.Itemid;
                    record.Styleno = entity.Styleno;
                    record.Styledesc = entity.Styledesc;
                    record.Brand = entity.Brand;
                    record.Totalqty = entity.Totalqty;
                    record.SellRate = entity.SellRate;
                    record.GST = entity.GST;
                    record.Subtotal = entity.Subtotal;

                    try
                    {
                        db.SaveChanges();
                        IsUpdated = true;
                        if (entity.Id > 0)
                        {
                            if (entity.itemimg_list.Count > 0)
                            {
                                foreach (var img in entity.itemimg_list)
                                {
                                    if (img != "")
                                    {
                                        M_ImageMaster record1 = new M_ImageMaster();
                                        record1.ItemId = entity.Itemid;
                                        record1.Item_Attribute_ID = (int)entity.SizeId;
                                        record1.StyleId = (int)entity.Styleid;
                                        record1.image = img;
                                        record1.IsCustomize = (int)entity.BOMD_Id;
                                        record1.Createddate = entity.Createdate;
                                        record1.Createdby = entity.Createdby;

                                        //New Code Added by Rahul on 09-01-2020
                                        //record.Updatedby = entity.Updatedby;
                                        //record.Updateddate = entity.Updateddate;
                                        record1.Company_ID = entity.Company_ID;
                                        record1.BranchId = entity.BranchId;
                                        record1.IsActive = entity.IsActive;
                                        db.M_ImageMaster.Add(record1);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
                else
                {
                    IsUpdated = false ;
                }
               
            }
            return IsUpdated;
        }
        public bool Delete(long id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.M_BOMSizeDetailMaster.Where(x => x.Id == id).FirstOrDefault();
                item.IsActive = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
            } return IsDeleted;
        }
    }
}
