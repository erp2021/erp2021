﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
namespace Garments_ERP.Data.Admin
{
   public class cl_QuotationStyleDetail
    {
        cl_GRNInward grnobj = new cl_GRNInward();
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;
        public object EditAttributeValue(string MyJson, int type, XmlDocument doc)
        {
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = type;
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = MyJson;
                cmd.Parameters.Add("attributexml", SqlDbType.VarChar).Value = doc.InnerXml;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

            }
            return dt;
        }

        public long InsertStyleImage(CRM_QuotationStyleImageEntity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                CRM_QuotationStyleImage imgstyle = new CRM_QuotationStyleImage();
                imgstyle.Quotationid = entity.Quotationid;
                imgstyle.Quotationimg = entity.Quotationimg;
                imgstyle.Createdate = DateTime.Now;
                imgstyle.IsActive = true;
                imgstyle.Createdby = entity.CreatedBy;
                imgstyle.Company_ID = entity.Company_ID;
                imgstyle.BranchId = entity.BranchId;
                db.CRM_QuotationStyleImage.Add(imgstyle);
                db.SaveChanges();
                id = imgstyle.Id;
            }
            return id;
        }

        public long updateStyleImage(CRM_QuotationStyleImageEntity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.CRM_QuotationStyleImage.Where(x=>x.Quotationid==entity.Quotationid);
                foreach (var data in record)
                {
                    var imgstyle = db.CRM_QuotationStyleImage.Find(data.Id);
                    imgstyle.Quotationid = entity.Quotationid;
                    imgstyle.Updateddate = DateTime.Now;
                    imgstyle.IsActive = false;
                    db.SaveChanges();
                }
                id = 1;
            }
            return id;
        }

        



       public long Insert(CRM_QuotationStyleDetailEntity entity)
       {
           long id = 0;
           using (var db = new GarmentERPDBEntities())
           {
               CRM_QuotationStyleDetail record = new CRM_QuotationStyleDetail();
               record.Id = entity.Id;
               record.Quotationid = entity.Quotationid;
               record.Segmenttypeid = entity.Segmenttypeid;
               record.Styleid = entity.Styleid;
               record.Itemid = entity.Itemid;
               record.Styleshadeid =0;
               record.Stylecolorid = 0;
               record.Brand = entity.Brand;
               record.Styleno = entity.Styleno;
               record.Styledesc = entity.Styledesc;
               record.Styleimage = entity.Styleimage;
               record.Reqqty = entity.Reqqty;
               record.Totalqty = entity.Totalqty;
               record.Createdby = entity.Createdby;
               record.Createdate = DateTime.Now;
               record.Attribute_ID = entity.Attribute_ID;
               record.Attribute_Value_ID = entity.Attribute_Value_ID;
               record.rate = entity.rate;
               record.IsActive = true;
               record.Sizeid = entity.Sizeid;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.BranchId;
                record.SellRate = entity.SellRate;
                record.GST = entity.GST;
                record.Subtotal = entity.Subtotal;
               db.CRM_QuotationStyleDetail.Add(record);
               db.SaveChanges();
               id = record.Id;

                if (id > 0)
                {
                    if (entity.itemimg_list.Count > 0)
                    {
                        foreach (var img in entity.itemimg_list)
                        {
                            if (img != "")
                            {
                                M_ImageMaster record1 = new M_ImageMaster();
                                record1.ItemId = entity.Itemid;
                                record1.Item_Attribute_ID = (int)entity.Sizeid;
                                record1.StyleId = (int)entity.Styleid;
                                record1.image = img;
                                record1.IsCustomize = (int)entity.Quotationid;
                                record1.Createddate = entity.Createdate;
                                record1.Createdby = entity.Createdby;

                                //New Code Added by Rahul on 09-01-2020
                                //record.Updatedby = entity.Updatedby;
                                //record.Updateddate = entity.Updateddate;
                                record1.Company_ID = entity.Company_ID;
                                record1.BranchId = entity.BranchId;
                                record1.IsActive = entity.IsActive;
                                db.M_ImageMaster.Add(record1);
                                db.SaveChanges();
                            }
                        }
                    }
                }

            }
           return id;
       }

       public CRM_QuotationStyleDetailEntity getbyquotationid(long id)
       {
           CRM_QuotationStyleDetailEntity entity = new CRM_QuotationStyleDetailEntity();
           using (var db = new GarmentERPDBEntities())
           {
               var recordlist = db.CRM_QuotationStyleDetail.Where(x => x.Quotationid == id && x.IsActive==true).ToList();
               var record = recordlist.FirstOrDefault();

               if (record != null)
               {
                   entity.Id = record.Id;
                   entity.Quotationid = record.Quotationid;
                   entity.Segmenttypeid = record.Segmenttypeid;
                    var segName = db.M_SegmentTypeMaster.Where(x => x.Id == record.Segmenttypeid).Select(x => x.Segmenttype).FirstOrDefault();
                    M_SegmentTypeMasterEntity segent = new M_SegmentTypeMasterEntity();
                    segent.Segmenttype = segName;
                    entity.SegTypeEnt = segent;

                    entity.Styleid = record.Styleid;
                    var stylename = db.M_StyleMaster.Where(x => x.Id == record.Styleid).Select(x => x.Stylename).FirstOrDefault();
                    M_StyleMasterEntity styent = new M_StyleMasterEntity();
                    styent.Stylename = stylename;
                    entity.styleEnt = styent;

                    entity.Itemid = record.Itemid;
                    var itemname = db.M_ItemMaster.Where(x => x.Id == record.Itemid).Select(x => x.ItemName).FirstOrDefault();
                    M_ItemMasterEntity itement = new M_ItemMasterEntity();
                    int IAid = Convert.ToInt32(record.Sizeid);
                    int ISid = 0;
                    long itemid = Convert.ToInt64(record.Itemid);
                    itement.ItemName = grnobj.getItemName(itemid, IAid, ISid);
                    entity.ItemEnt = itement;

                    entity.Styleshadeid = record.Styleshadeid;
                   entity.Stylecolorid = record.Stylecolorid;
                   entity.Brand = record.Brand;
                   entity.Styleno = record.Styleno;
                   entity.Styledesc = record.Styledesc;
                   entity.Styleimage = record.Styleimage;
                   entity.Reqqty = record.Reqqty;
                   entity.Totalqty = record.Totalqty;
                   entity.Createdby = record.Createdby;
                   entity.Createdate = record.Createdate;
                   entity.Updateddate = record.Updateddate;
                   entity.Updatedby = record.Updatedby;
                   entity.Sizetype = record.Sizetype;
                   entity.Sizeper = record.Sizeper;
                    entity.SellRate = record.SellRate;
                    entity.GST = record.GST;
                    entity.Subtotal = record.Subtotal;
                }

               List<StyleSizeEntity> sizelist = new List<StyleSizeEntity>();
               int toalqty = 0;
               foreach (var size in recordlist)
               {

                   StyleSizeEntity sizeentity = new StyleSizeEntity();
                   sizeentity.Id = size.Id;
                   sizeentity.Sizetype = size.Sizetype;
                   sizeentity.Sizeper = size.Sizeper;
                   sizeentity.Reqqty = size.Reqqty;
                   toalqty = toalqty + (int)size.Reqqty;
                   sizelist.Add(sizeentity);

               }
               entity.Totalqty = toalqty;
               entity.stylesizelist = sizelist;

           }
           return entity;
       }


       public bool Update(CRM_QuotationStyleDetailEntity entity)
       {
           bool isupdate = false;
           using (var db = new GarmentERPDBEntities())
           {
               var record = db.CRM_QuotationStyleDetail.Find(entity.Id);
                record.Quotationid = entity.Quotationid;
                record.Segmenttypeid = entity.Segmenttypeid;
                record.Styleid = entity.Styleid;
                record.Itemid = entity.Itemid;
                record.Styleshadeid = 0;
                record.Stylecolorid = 0;
                record.Brand = entity.Brand;
                record.Styleno = entity.Styleno;
                record.Styledesc = entity.Styledesc;
                record.Styleimage = entity.Styleimage;
                record.Reqqty = entity.Reqqty;
                record.Totalqty = entity.Totalqty;
                record.Updatedby = entity.Updatedby;
                record.Updateddate = entity.Updateddate;
                record.Attribute_ID = entity.Attribute_ID;
                record.Attribute_Value_ID = entity.Attribute_Value_ID;
                record.rate = entity.rate;
                record.IsActive = true;
                record.Sizeid = entity.Sizeid;
                record.SellRate = entity.SellRate;
                record.GST = entity.GST;
                record.Subtotal = entity.Subtotal;    
                db.SaveChanges();
                isupdate = true;
                if (entity.itemimg_list.Count > 0)
                {
                    var imgrec = db.M_ImageMaster.Where(x => x.ItemId == entity.Itemid && x.Item_Attribute_ID == entity.Sizeid && x.IsCustomize == entity.Quotationid).ToList();
                    if (imgrec != null)
                    {
                        foreach (var img in imgrec)
                        {
                            var imgrecord = db.M_ImageMaster.Find(img.Id);
                            imgrecord.IsActive = false;
                            imgrecord.Updateddate = DateTime.Now;
                            db.SaveChanges();
                        }
                    }

                    foreach (var img in entity.itemimg_list)
                    {
                        if (img != "")
                        {
                            M_ImageMaster record1 = new M_ImageMaster();
                            record1.ItemId = entity.Itemid;
                            record1.Item_Attribute_ID = (int)entity.Sizeid;
                            record1.StyleId = (int)entity.Styleid;
                            record1.image = img;
                            record1.IsCustomize = (int)entity.Quotationid;
                            record1.Createddate = entity.Createdate;
                            record1.Createdby = entity.Createdby;

                            //New Code Added by Rahul on 09-01-2020
                            //record.Updatedby = entity.Updatedby;
                            //record.Updateddate = entity.Updateddate;
                            record1.Company_ID = entity.Company_ID;
                            record1.BranchId = entity.BranchId;
                            record1.IsActive = entity.IsActive;
                            db.M_ImageMaster.Add(record1);
                            db.SaveChanges();
                        }
                    }
                }


            }
           return isupdate;

       }
    }
}
