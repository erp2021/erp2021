﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Garments_ERP.Data.Classes
{
    public class cl_SupplierPR
    {
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;
        cl_role rlobj = new cl_role();
        ////List Of Quotation With Status and User and Form Date And To Date Wise
        public List<SRM_PurchaseRequestEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<SRM_PurchaseRequestEntity> list = new List<SRM_PurchaseRequestEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_PurchaseRequest.Where(x => x.IsActive == true && x.PR_Date >= from && x.PR_Date <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy == Userid).OrderByDescending(x => x.Id).ToList();
                if (records.Count >= 0 && roleid != 4)
                {
                    records = db.SRM_PurchaseRequest.Where(x => x.IsActive == true && x.PR_Date >= from && x.PR_Date <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).OrderByDescending(x => x.Id).ToList();

                }

                foreach (var item in records)
                {
                    SRM_PurchaseRequestEntity entity = new SRM_PurchaseRequestEntity();
                    entity.Id = item.Id;
                    entity.AccountHeadId = item.AccountHeadId;
                    if (item.AccountHeadId != null)
                    {
                        EmployeeEntity empentity = new EmployeeEntity();
                        empentity.Name = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.Name).FirstOrDefault();
                        empentity.ID = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.ID).FirstOrDefault();
                        empentity.EmpID = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.EmpID).FirstOrDefault();
                        empentity.UserId = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.UserId).FirstOrDefault();
                        entity.Employee = empentity;
                    }
                    entity.AdditionalInfo = item.AdditionalInfo;
                    entity.ApprovalStatus = item.ApprovalStatus;
                    entity.DepartmentId = item.DepartmentId;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                        deptentity.Id = item.M_DepartmentMaster.Id;
                        deptentity.DepartmentNo = item.M_DepartmentMaster.DepartmentNo;
                        deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        deptentity.Departmentdesc = item.M_DepartmentMaster.Departmentdesc;
                        deptentity.Shortname = item.M_DepartmentMaster.Shortname;
                        entity.DepartmentMaster = deptentity;
                    }
                    entity.PR_Date = item.PR_Date;
                    entity.PR_No = item.PR_No;
                    entity.Priority = item.Priority;
                    entity.Comment = item.Comment;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;

                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    var itemdata = db.SRM_PRItemDetail.Where(x => x.PR_ID == item.Id && x.IsActive==true).ToList();
                    if(itemdata.Count>0)
                    {
                        List<SRM_PRItemDetailEntity> itemlit = new List<SRM_PRItemDetailEntity>();
                        foreach (var data in itemdata)
                        {
                            SRM_PRItemDetailEntity rec = new SRM_PRItemDetailEntity();
                            rec.PR_ID = data.PR_ID;
                            rec.ItemSubCategoryId = data.ItemSubCategoryId;
                            if(data.ItemSubCategoryId>0)
                            {
                                M_ItemSubCategoryMasterEntity subent = new M_ItemSubCategoryMasterEntity();
                                subent.Id =Convert.ToInt32(data.ItemSubCategoryId);
                                subent.Itemsubcategory = data.M_ItemSubCategoryMaster.Itemsubcategory;

                            }
                            rec.ItemId = data.ItemId;
                            if (data.ItemId > 0)
                            {
                                M_ItemMasterEntity itment = new M_ItemMasterEntity();
                                itment.Id = Convert.ToInt32(data.ItemId);
                                itment.ItemName = data.M_ItemSubCategoryMaster.Itemsubcategory;

                            }
                            rec.Item_Attribute_ID = data.Item_Attribute_ID;
                            rec.RequiredQty = data.RequiredQty;
                            rec.UnitId = data.UnitId;
                            rec.IsActive = data.IsActive;
                            rec.CreatedBy = entity.CreatedBy;
                            rec.CreatedDate = DateTime.Now;
                            rec.Company_ID = data.Company_ID;
                            rec.BranchId = data.BranchId;
                            rec.HSNCode = data.HSNCode;
                            rec.Isreadymade = 1;
                            itemlit.Add(rec);
                        }
                        entity.ItemList = itemlit;
                    }

                    list.Add(entity);
                }
                if (roleid != 4)
                {
                    var Userlist = rlobj.GetUserRole().Where(x => x.roleId == 4 || x.roleId == roleid).ToList();
                    list = list.Where(x => x.IsActive == true && x.PR_Date >= from && x.PR_Date <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && Userlist.Any(y => y.UserId == x.CreatedBy)).OrderByDescending(x => x.Id).ToList();

                }
            }
            return list;
        }

        //Get Account Head Name With Department Wise
        public List<EmployeeEntity> GetAccountHead(int deptid, int Company_ID, int BranchId)
        {
            List<EmployeeEntity> list = new List<EmployeeEntity>();
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.M_User_Department.Where(x => x.DepartmentId == deptid && x.CompanyId == Company_ID && x.BranchId == BranchId && x.IsActive == true).ToList();
                    foreach (var item in record)
                    {
                        EmployeeEntity ent = new EmployeeEntity();
                        ent.UserId = Convert.ToInt32(item.UserId);
                        ent.Name = db.Employees.Where(x => x.UserId == item.UserId && x.IsActive == true).Select(x => x.Name).FirstOrDefault();
                        list.Add(ent);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return list;
        }


        public string getnextprno()
        {
            string no;
            no = "PR-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SRM_PurchaseRequest.FirstOrDefault() != null ? db.SRM_PurchaseRequest.Max(x => x.Id) + 1 : 1;
                no = no + "-" + record.ToString();
            }

            return no;
        }

        public object InventoryPR(int type, string Jobject, string Attribute)
        {
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    XmlDocument doc = new XmlDocument();
                    doc = JsonConvert.DeserializeXmlNode("{\"Row\":" + Jobject + "}", "root"); // JSON needs to be an objec
                    var record = db.Database.SqlQuery<string>("SP_InventoryPR @type, @attribute, @attributexml", 
                        new SqlParameter("type", type),
                        new SqlParameter("attribute", Attribute),
                        new SqlParameter("attributexml", doc.InnerXml)).ToList();

                    var xmlString = string.Join("", record);
                    var xml = new XmlDocument();

                    //Note: You need to add a root element to your result
                    xml.LoadXml($"{xmlString}");
                    var json = JsonConvert.SerializeXmlNode(xml);
                    object JsonData= JsonConvert.DeserializeObject(json);
                    return JsonData;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public object StatusCount(int type, string attribute, XmlDocument doc)
        {
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.Database.SqlQuery<string>("SP_StatusCount @type, @attribute, @attributexml",
                        new SqlParameter("type", type),
                        new SqlParameter("attribute", attribute),
                        new SqlParameter("attributexml", doc.InnerXml)).ToList();

                    var xmlString = string.Join("", record);
                    var xml = new XmlDocument();

                    //Note: You need to add a root element to your result
                    xml.LoadXml($"{xmlString}");
                    var json = JsonConvert.SerializeXmlNode(xml);
                    object JsonData = JsonConvert.DeserializeObject(json);
                    return JsonData;
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public long Insert(SRM_PurchaseRequestEntity entity)
        {
            long id = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    SRM_PurchaseRequest record = new SRM_PurchaseRequest();
                    record.AccountHeadId = entity.AccountHeadId;
                    record.AdditionalInfo = entity.AdditionalInfo;
                    record.ApprovalStatus = entity.ApprovalStatus;
                    record.DepartmentId = entity.DepartmentId;
                    record.PR_Date = entity.PR_Date;
                    record.PR_No = Convert.ToString(getnextprno());
                    record.Priority = entity.Priority;
                    record.IsActive = entity.IsActive;
                    record.Comment = entity.Comment;
                    record.CreatedBy = entity.CreatedBy;
                    record.CreatedDate = DateTime.Now;
                    record.ApprovalDate = entity.ApprovalDate;
                    record.IsStatus = entity.IsStatus;
                    record.Company_ID = entity.Company_ID;
                    record.BranchId = entity.BranchId;
                    record.PR_No = "RM-" + Convert.ToString(getnextprno());
                    db.SRM_PurchaseRequest.Add(record);
                    db.SaveChanges();
                    id = record.Id;
                    if(id>0)
                    {
                        foreach(var data in entity.ItemList)
                        {
                            SRM_PRItemDetail rec = new SRM_PRItemDetail();
                            rec.PR_ID = id;
                            rec.ItemSubCategoryId = data.ItemSubCategoryId;
                            rec.ItemId = data.ItemId;
                            rec.Item_Attribute_ID = data.Item_Attribute_ID;
                            rec.RequiredQty = data.RequiredQty;
                            rec.UnitId = data.UnitId;
                            rec.IsActive = data.IsActive;
                            rec.CreatedBy = entity.CreatedBy;
                            rec.CreatedDate = DateTime.Now;
                            rec.Company_ID = data.Company_ID;
                            rec.BranchId = data.BranchId;
                            rec.HSNCode = data.HSNCode;
                            rec.Isreadymade = 1;
                            db.SRM_PRItemDetail.Add(rec);
                            db.SaveChanges();
                            id = rec.Id;
                            if(id>0)
                            {
                                if (data.rawitemimg_list.Count > 0)
                                {
                                    foreach (var img in data.rawitemimg_list)
                                    {
                                        if (img != "")
                                        {
                                            M_BOMRawItemImage imgrecord = new M_BOMRawItemImage();
                                            imgrecord.Id = 0;
                                            imgrecord.BOMrawitemdetailid = id;
                                            imgrecord.image = img;
                                            imgrecord.Createddate = DateTime.Now;
                                            imgrecord.IsActive = true;
                                            imgrecord.Company_ID = entity.Company_ID;
                                            imgrecord.BranchId = entity.BranchId;
                                            imgrecord.Createdby = entity.CreatedBy;
                                            db.M_BOMRawItemImage.Add(imgrecord);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return id;
        }

        public bool Delete(long id, int Userid)
        {
            bool isdeleted = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.SRM_PurchaseRequest.Find(id);
                    record.IsActive = false;
                    record.UpdatedBy = Userid;
                    record.UpdatedDate = DateTime.Now;
                    db.SaveChanges();
                    isdeleted = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isdeleted;
        }

        //Reject PR to update Status
        public bool RejectedPR(long id, int Uid)
        {
            bool isdeleted = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.SRM_PurchaseRequest.Find(id);

                    record.IsStatus = 5;
                    record.UpdatedDate = DateTime.Now;
                    record.UpdatedBy = Uid;
                    db.SaveChanges();
                    isdeleted = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isdeleted;
        }

        
        //List Of Quotation With Status and User Wise
        public List<SRM_PurchaseRequestEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<SRM_PurchaseRequestEntity> list = new List<SRM_PurchaseRequestEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_PurchaseRequest.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy == Userid).OrderByDescending(x => x.Id).ToList();
                if (records.Count >= 0 && roleid != 4)
                {
                    records = db.SRM_PurchaseRequest.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).OrderByDescending(x => x.Id).ToList();
                }

                foreach (var item in records)
                {
                    SRM_PurchaseRequestEntity entity = new SRM_PurchaseRequestEntity();
                    entity.Id = item.Id;
                    entity.AccountHeadId = item.AccountHeadId;
                    if (item.AccountHeadId != null)
                    {
                        EmployeeEntity empentity = new EmployeeEntity();
                        empentity.Name = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.Name).FirstOrDefault();
                        empentity.ID = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.ID).FirstOrDefault();
                        empentity.EmpID = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.EmpID).FirstOrDefault();
                        empentity.UserId = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.UserId).FirstOrDefault();
                        entity.Employee = empentity;
                    }
                    entity.AdditionalInfo = item.AdditionalInfo;
                    entity.ApprovalStatus = item.ApprovalStatus;
                    entity.DepartmentId = item.DepartmentId;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                        deptentity.Id = item.M_DepartmentMaster.Id;
                        deptentity.DepartmentNo = item.M_DepartmentMaster.DepartmentNo;
                        deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        deptentity.Departmentdesc = item.M_DepartmentMaster.Departmentdesc;
                        deptentity.Shortname = item.M_DepartmentMaster.Shortname;
                        entity.DepartmentMaster = deptentity;
                    }
                    entity.PR_Date = item.PR_Date;
                    entity.PR_No = item.PR_No;
                    entity.Priority = item.Priority;
                    entity.Comment = item.Comment;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;

                    list.Add(entity);
                }
                if (roleid != 4)
                {
                    var Userlist = rlobj.GetUserRole().Where(x => x.roleId == 4 || x.roleId == roleid).ToList();
                    list = list.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && Userlist.Any(y => y.UserId == x.CreatedBy)).OrderByDescending(x => x.Id).ToList();
                }
            }

            return list;

        }

        public SRM_PurchaseRequestEntity getbyid(long id)
        {
            SRM_PurchaseRequestEntity entity = new SRM_PurchaseRequestEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.SRM_PurchaseRequest.Find(id);
                entity.Id = item.Id;
                entity.AccountHeadId = item.AccountHeadId;
                if (item.AccountHeadId != null)
                {
                    EmployeeEntity empentity = new EmployeeEntity();
                    empentity.Name = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.Name).FirstOrDefault();
                    empentity.ID = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.ID).FirstOrDefault();
                    empentity.EmpID = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.EmpID).FirstOrDefault();
                    empentity.UserId = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.UserId).FirstOrDefault();
                    entity.Employee = empentity;
                }
                entity.AdditionalInfo = item.AdditionalInfo;
                entity.ApprovalStatus = item.ApprovalStatus;
                entity.ApprovalDate = item.ApprovalDate;
                entity.DepartmentId = item.DepartmentId;
                if (item.M_DepartmentMaster != null)
                {
                    M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                    deptentity.Id = item.M_DepartmentMaster.Id;
                    deptentity.DepartmentNo = item.M_DepartmentMaster.DepartmentNo;
                    deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                    deptentity.Departmentdesc = item.M_DepartmentMaster.Departmentdesc;
                    deptentity.Shortname = item.M_DepartmentMaster.Shortname;
                    entity.DepartmentMaster = deptentity;
                }
                entity.PR_Date = item.PR_Date;
                entity.PR_No = item.PR_No;
                entity.Priority = item.Priority;
                entity.Comment = item.Comment;
                entity.CreatedDate = item.CreatedDate;
                entity.CreatedBy = item.CreatedBy;
                entity.UpdatedDate = item.UpdatedDate;
                entity.UpdatedBy = item.UpdatedBy;
                entity.IsActive = item.IsActive;
                entity.IsStatus = item.IsStatus;
                entity.Company_ID = item.Company_ID;
                entity.BranchId = item.BranchId;
                entity.IsStatus = item.IsStatus;
                entity.Company_ID = item.Company_ID;
                entity.BranchId = item.BranchId;
            }

            return entity;
        }

        public List<SRM_PRItemDetailEntity> getbyPRItemid(long id)
        {
            List<SRM_PRItemDetailEntity> list = new List<SRM_PRItemDetailEntity>();
            using (var db = new GarmentERPDBEntities())
            {

                var records = db.SRM_PRItemDetail.Where(m => m.PR_ID == id && m.IsActive == true).ToList();
                foreach (var item in records)
                {
                    SRM_PRItemDetailEntity entity = new SRM_PRItemDetailEntity();
                    entity.Id = item.Id;
                    entity.ItemSubCategoryId = item.ItemSubCategoryId;


                    if (item.M_ItemSubCategoryMaster != null)
                    {
                        M_ItemSubCategoryMasterEntity itemcatentity = new M_ItemSubCategoryMasterEntity();
                        itemcatentity.Id = item.M_ItemSubCategoryMaster.Id;
                        itemcatentity.Itemsubcategory = item.M_ItemSubCategoryMaster.Itemsubcategory;
                        entity.ItemSubCategoryMaster = itemcatentity;
                    }
                    entity.ItemId = item.ItemId;
                    int IAid = Convert.ToInt32(item.Item_Attribute_ID);
                    int ISid = Convert.ToInt32(item.ItemSubCategoryId);
                    if (item.M_ItemMaster != null)
                    {
                        M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                        itementity.Id = item.M_ItemMaster.Id;
                        itementity.ItemName = getItemName(item.M_ItemMaster.Id, IAid, ISid);
                        itementity.ItemRate = item.M_ItemMaster.ItemRate;
                        itementity.HScode = item.M_ItemMaster.HScode;
                        entity.ItemMaster = itementity;
                    }
                    entity.Item_Attribute_ID = item.Item_Attribute_ID > 0 ? item.Item_Attribute_ID : 0;
                    entity.PR_ID = item.PR_ID;
                    entity.ItemDesc = item.ItemDesc;
                    entity.Comment = item.Comment;
                    entity.UnitId = item.UnitId;
                    entity.RequiredQty = item.RequiredQty;
                    if (item.M_UnitMaster != null)
                    {
                        M_UnitMasterEntity unitentity = new M_UnitMasterEntity();
                        unitentity.Id = item.M_UnitMaster.Id;
                        unitentity.ItemUnit = item.M_UnitMaster.ItemUnit;
                        entity.UnitMaster = unitentity;
                    }
                    entity.RequiredDate = item.RequiredDate;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    
                    entity.Isreadymade = item.Isreadymade;
                    entity.HSNCode = item.HSNCode;
                    list.Add(entity);
                }

            }

            return list;
        }

        public string getItemName(long itemid, int attibid, int subcatid)
        {
            string itemname = "";
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 32; //Check Bill Of Material Create Or Not Using Customer PO 
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = Convert.ToString(subcatid);
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "<root><Row><ItemId>" + itemid + "</ItemId><AttributeId>" + attibid + "</AttributeId></Row></root>";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                foreach (DataTable table in dt.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        itemname = Convert.ToString(dr["ItemName"]);
                    }
                }

            }
            return itemname;
        }
    }
}
