﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Classes.MasterClasses
{
    public class cl_InwardItem
    {
        public long Insert(M_ItemInwardMasterEntity entity)
        {
            long insertedid = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_ItemInwardMaster record = new M_ItemInwardMaster();
                record.ID = entity.ID;
                record.ItemInward_ID = entity.ItemInward_ID;
                record.Item_ID = entity.Item_ID;
                record.ItemQty = entity.ItemQty;
                record.Jobwork_ID = entity.Jobwork_ID;
                record.Remarks = entity.Remarks;
                record.BalanceQty = entity.BalanceQty;
                record.CreatedBy = entity.CreatedBy;
                record.CreatedDate = entity.CreatedDate;
                record.UOM = entity.UOM;
                record.UpdatedBy = entity.UpdatedBy;
                record.UpdatedDate = entity.UpdatedDate;
                record.IsActive = entity.IsActive;
                db.M_ItemInwardMaster.Add(record);
                db.SaveChanges();
                insertedid = record.ID;
            }
            return insertedid;
        }
        public List<M_ItemInwardMasterEntity> get()
        {
            List<M_ItemInwardMasterEntity> list = new List<M_ItemInwardMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ItemInwardMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_ItemInwardMasterEntity entity = new M_ItemInwardMasterEntity();
                    entity.ID = item.ID;
                    entity.ItemInward_ID = item.ItemInward_ID;
                    entity.Item_ID = item.Item_ID;
                    if (item.M_ItemMaster != null)
                    {
                        M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                        itementity.Id = item.M_ItemMaster.Id;
                        itementity.ItemName = item.M_ItemMaster.ItemName;
                        entity.M_ItemMaster = itementity;
                    }
                    entity.ItemQty = item.ItemQty;
                    entity.UOM = item.UOM;
                    if (item.M_UnitMaster != null)
                    {
                        M_UnitMasterEntity unitentity = new M_UnitMasterEntity();
                        unitentity.Id = item.M_UnitMaster.Id;
                        unitentity.ItemUnit = item.M_UnitMaster.ItemUnit;
                        entity.M_UnitMaster = unitentity;
                    }
                    entity.Jobwork_ID = item.Jobwork_ID;
                    entity.Remarks = item.Remarks;
                    entity.BalanceQty = item.BalanceQty;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.IsActive = item.IsActive;
                    
                    list.Add(entity);
                }
            }
            return list;
        }
        public M_ItemInwardMasterEntity GetById(long id)
        {
            M_ItemInwardMasterEntity entity = new M_ItemInwardMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ItemInwardMaster.Where(x => x.ID == id && x.IsActive == true);
                foreach (var item in records)
                {
                    entity.ID = item.ID;
                    entity.ItemInward_ID = item.ItemInward_ID;
                    entity.Item_ID = item.Item_ID;
                    if (item.M_ItemMaster != null)
                    {
                        M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                        itementity.Id = item.M_ItemMaster.Id;
                        itementity.ItemName = item.M_ItemMaster.ItemName;
                        entity.M_ItemMaster = itementity;
                    }
                    entity.ItemQty = item.ItemQty;
                    entity.UOM = item.UOM;
                    if (item.M_UnitMaster != null)
                    {
                        M_UnitMasterEntity unitentity = new M_UnitMasterEntity();
                        unitentity.Id = item.M_UnitMaster.Id;
                        unitentity.ItemUnit = item.M_UnitMaster.ItemUnit;
                        entity.M_UnitMaster = unitentity;
                    }
                    entity.Jobwork_ID = item.Jobwork_ID;
                    entity.Remarks = item.Remarks;
                    entity.BalanceQty = item.BalanceQty;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.IsActive = item.IsActive;
                }
            }
            return entity;
        }
        public bool Update(M_ItemInwardMasterEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ItemInwardMaster.Where(x => x.ID == entity.ID).FirstOrDefault();
                record.ID = entity.ID;
                record.ItemInward_ID = entity.ItemInward_ID;
                record.Item_ID = entity.Item_ID;
                record.ItemQty = entity.ItemQty;
                record.Jobwork_ID = entity.Jobwork_ID;
                record.Remarks = entity.Remarks;
                record.BalanceQty = entity.BalanceQty;
                record.UOM = entity.UOM;
                record.UpdatedBy = entity.UpdatedBy;
                record.UpdatedDate = entity.UpdatedDate;
                record.IsActive = entity.IsActive;
                try
                {
                    db.SaveChanges();
                    IsUpdated = true;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return IsUpdated;
        }
        public bool Delete(long id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.M_ItemInwardMaster.Where(x => x.ID == id).FirstOrDefault();
                item.IsActive = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
                return IsDeleted;

            }
        }
    }
}
