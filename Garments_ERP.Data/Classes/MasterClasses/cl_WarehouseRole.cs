﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_WarehouseRole
    {
        public List<M_RoleEntity> get()
        {
            List<M_RoleEntity> list = new List<M_RoleEntity>();

            using (var db = new GarmentERPDBEntities())
            {
               
                var records = db.M_Role.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_RoleEntity entity = new M_RoleEntity();
                    entity.roleId = item.roleId;
                    entity.roleName = item.roleName;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedOn = item.CreatedOn;
                    entity.IsActive = item.IsActive;
                    entity.LFT = item.LFT;
                    entity.ParentId = item.ParentId;
                    entity.RGT = item.RGT;
                    entity.Discription = item.Discription;
                    entity.UpdateBy = item.UpdateBy;
                    entity.UpdateOn = item.UpdateOn;
                    list.Add(entity);
                }
            }
            return list;
        }


        public M_WarehouseRoleMasterEntity getbyid(long id)
        {
            M_WarehouseRoleMasterEntity entity = new M_WarehouseRoleMasterEntity();
            //using (var db = new GarmentERPDBEntities())
            //{
            //    var item = db.M_ROLE.Find(id);
            //    entity.ID = item.ID;
            //    entity.CODE = item.CODE;
            //    entity.CREATED_BY = item.CREATED_BY;
            //    entity.CREATED_ON = item.CREATED_ON;
            //    entity.IS_ENABLED = item.IS_ENABLED;
            //    entity.LFT = item.LFT;
            //    entity.MEMBER_TYPE = item.MEMBER_TYPE;
            //    entity.RGT = item.RGT;
            //    entity.SHORT_NAME = item.SHORT_NAME;
            //    entity.UPDATED_BY = item.UPDATED_BY;
            //    entity.UPDATED_ON = item.UPDATED_ON;   
            //}
            return entity;
        }


        public long Insert(M_WarehouseRoleMasterEntity entity)
        {
            long insertid = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    //M_ROLE record = new M_ROLE();
                    //record.ID = entity.ID;
                    //record.CODE = entity.CODE;
                    //record.CREATED_BY = entity.CREATED_BY;
                    //record.CREATED_ON = entity.CREATED_ON;
                    //record.IS_ENABLED = entity.IS_ENABLED;
                    //record.LFT = entity.LFT;
                    //record.MEMBER_TYPE = entity.MEMBER_TYPE;
                    //record.RGT = entity.RGT;
                    //record.SHORT_NAME = entity.SHORT_NAME;
                    //record.UPDATED_BY = entity.UPDATED_BY;
                    //record.UPDATED_ON = entity.UPDATED_ON;
                    //db.M_ROLE.Add(record);
                    //db.SaveChanges();
                    //insertid = record.ID;

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return insertid;
        }

        public bool Update(M_WarehouseRoleMasterEntity entity)
        {
            bool updated = false;

            //using (var db = new GarmentERPDBEntities())
            //{
            //    var record = db.M_ROLE.Find(entity.ID);
            //    record.ID = entity.ID;
            //    record.CODE = entity.CODE;
            //    record.CREATED_BY = entity.CREATED_BY;
            //    record.CREATED_ON = entity.CREATED_ON;
            //    record.IS_ENABLED = entity.IS_ENABLED;
            //    record.LFT = entity.LFT;
            //    record.MEMBER_TYPE = entity.MEMBER_TYPE;
            //    record.RGT = entity.RGT;
            //    record.SHORT_NAME = entity.SHORT_NAME;
            //    record.UPDATED_BY = entity.UPDATED_BY;
            //    record.UPDATED_ON = entity.UPDATED_ON;
            //    db.SaveChanges();
            //}

            return updated;
        }
        public bool Delete(long id)
        {
            bool deleted = false;

            using (var db = new GarmentERPDBEntities())
            {
                //var record = db.M_ROLE.Find(id);
                //record.IS_ENABLED = false;
                //db.SaveChanges();
                //deleted = true;
            }

            return deleted;
        }
    }
}
