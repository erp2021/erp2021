﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;

namespace Garments_ERP.Data.Admin
{
    public class cl_ItemSubcategory
    {

        public List<M_ItemMasterEntity> GetRawItemList(int subcatid)
        {
            List<M_ItemMasterEntity> list = new List<M_ItemMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var itemdata = db.M_ItemMaster.Where(x => x.Itemsubcategoryid == subcatid).ToList();
                foreach(var data in itemdata)
                {
                    M_ItemMasterEntity mitementity = new M_ItemMasterEntity();
                    mitementity.Id = data.Id;
                    mitementity.ItemName = data.ItemName;
                    mitementity.Company_ID = data.Company_ID;
                    mitementity.Branch_ID = data.BranchId;

                    list.Add(mitementity);
                }
            }

            return list;
        }


        public List<M_ItemSubCategoryMasterEntity> get()
        {

            List<M_ItemSubCategoryMasterEntity> list = new List<M_ItemSubCategoryMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ItemSubCategoryMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_ItemSubCategoryMasterEntity entity = new M_ItemSubCategoryMasterEntity();
                    entity.Id = item.Id;
                    entity.Itemsubcategory = item.Itemsubcategory;
                    entity.itemcategoryid = item.itemcategoryid;
                    entity.itemsubcategorydesc = item.itemsubcategorydesc;
                    entity.Createddate = item.Createddate;
                    entity.Createdby = item.Createdby;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    entity.IsActive = item.IsActive;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    M_ItemCategoryMasterEntity itemcategory = new M_ItemCategoryMasterEntity();
                    if (item.M_ItemCategoryMaster != null)
                    {
                        itemcategory.Id = item.M_ItemCategoryMaster.Id;
                        itemcategory.Itemcategory = item.M_ItemCategoryMaster.Itemcategory;
                        entity.M_ItemCategoryMasterentity = itemcategory;

                    }
                    list.Add(entity);
                }
            }
            return list;
        }
        public List<M_ItemSubCategoryMasterEntity> getSubctegorybyCategoryid(long categoryid)
        {
            List<M_ItemSubCategoryMasterEntity> list = new List<M_ItemSubCategoryMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ItemSubCategoryMaster.Where(x => x.itemcategoryid == categoryid).ToList();
                foreach (var item in records)
                {
                    M_ItemSubCategoryMasterEntity entity = new M_ItemSubCategoryMasterEntity();
                    entity.Id = item.Id;
                    entity.Itemsubcategory = item.Itemsubcategory;
                    entity.itemcategoryid = item.itemcategoryid;
                    entity.itemsubcategorydesc = item.itemsubcategorydesc;
                    entity.Createddate = item.Createddate;
                    entity.Createdby = item.Createdby;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    entity.IsActive = item.IsActive;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    M_ItemCategoryMasterEntity itemcategory = new M_ItemCategoryMasterEntity();
                    if (item.M_ItemCategoryMaster != null)
                    {
                        itemcategory.Id = item.M_ItemCategoryMaster.Id;
                        itemcategory.Itemcategory = item.M_ItemCategoryMaster.Itemcategory;
                        entity.M_ItemCategoryMasterentity = itemcategory;

                    }
                    list.Add(entity);
                }
            }
            return list;
        }
        public int Insert(M_ItemSubCategoryMasterEntity entity)
        {
            int id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_ItemSubCategoryMaster record = new M_ItemSubCategoryMaster();
                record.Id = entity.Id;
                record.Itemsubcategory = entity.Itemsubcategory;
                record.itemcategoryid = entity.itemcategoryid;
                record.itemsubcategorydesc = entity.itemsubcategorydesc;
                record.Createddate = entity.Createddate;
                record.Createdby = entity.Createdby;

                //New Code Added by Rahul on 09-01-2020
                //record.Updateddate = entity.Updateddate;
                //record.Updatedby = entity.Updatedby;
                record.BranchId = entity.Branch_ID;
                record.Company_ID = entity.Company_ID;

                record.IsActive = true;
                db.M_ItemSubCategoryMaster.Add(record);
                db.SaveChanges();
                id = record.Id;
            }

            return id;
        }
        public bool Update(M_ItemSubCategoryMasterEntity entity)
        {
            bool isupdate = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ItemSubCategoryMaster.Find(entity.Id);
                record.Itemsubcategory = entity.Itemsubcategory;
                record.itemsubcategorydesc = entity.itemsubcategorydesc;

                //New Code Added by Rahul on 09-01-2020
                record.Updateddate = entity.Updateddate;
                record.Updatedby = entity.Updatedby;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                db.SaveChanges();
                isupdate = true;
            }
            return isupdate;
        }
        public List<M_ItemSubCategoryMasterEntity> GetById(long id)
        {
            List<M_ItemSubCategoryMasterEntity> entitylist = new List<M_ItemSubCategoryMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ItemSubCategoryMaster.Where(x => x.itemcategoryid == id && x.IsActive == true);
                foreach (var item in records)
                {
                    M_ItemSubCategoryMasterEntity entity = new M_ItemSubCategoryMasterEntity();
                    entity.Id = item.Id;
                    entity.Itemsubcategory = item.Itemsubcategory;
                    entity.itemcategoryid = item.itemcategoryid;
                    entity.itemsubcategorydesc = item.itemsubcategorydesc;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    M_ItemCategoryMasterEntity itemcategory = new M_ItemCategoryMasterEntity();
                    if (item.M_ItemCategoryMaster != null)
                    {
                        itemcategory.Id = item.M_ItemCategoryMaster.Id;
                        itemcategory.Itemcategory = item.M_ItemCategoryMaster.Itemcategory;
                        entity.M_ItemCategoryMasterentity = itemcategory;

                    }
                    entitylist.Add(entity);
                    // entity.IsActive = item.IsActive;
                }
            }
            return entitylist;
        }
        public M_ItemSubCategoryMasterEntity bysubcategoryid(long id)
        {
            M_ItemSubCategoryMasterEntity entity = new M_ItemSubCategoryMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ItemSubCategoryMaster.Where(x => x.Id == id && x.IsActive == true);
                foreach (var item in records)
                {
                    entity.Id = item.Id;
                    entity.Itemsubcategory = item.Itemsubcategory;
                    entity.itemcategoryid = item.itemcategoryid;
                    entity.itemsubcategorydesc = item.itemsubcategorydesc;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    M_ItemCategoryMasterEntity itemcategory = new M_ItemCategoryMasterEntity();
                    if (item.M_ItemCategoryMaster != null)
                    {
                        itemcategory.Id = item.M_ItemCategoryMaster.Id;
                        itemcategory.Itemcategory = item.M_ItemCategoryMaster.Itemcategory;
                        entity.M_ItemCategoryMasterentity = itemcategory;

                    }
                }
            }
            return entity;
        }
        public bool Delete(int id)
        {
            bool isdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ItemSubCategoryMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                isdelete = true;
            }

            return isdelete;

        }
        public bool CheckExistance(string Itemsubcategory, int? Id, int itemcategoryid,int companyid,int branchid)
        {
            bool result = true;

            using (var db = new GarmentERPDBEntities())
            {
                if (Id == null)
                {
                    var itemcatestatus = db.M_ItemSubCategoryMaster.Where(x => x.Itemsubcategory == Itemsubcategory && x.itemcategoryid == itemcategoryid && x.IsActive == true && x.Company_ID==companyid && x.BranchId==branchid).FirstOrDefault();
                    if (itemcatestatus != null)
                        result = false;
                }
                else
                {
                    var itemcatestatus = db.M_ItemSubCategoryMaster.Where(x => x.Itemsubcategory == Itemsubcategory && x.Id != Id && x.itemcategoryid == itemcategoryid && x.IsActive == true && x.Company_ID == companyid && x.BranchId == branchid).FirstOrDefault();
                    if (itemcatestatus != null)
                        result = false;
                }
            }
            return result;
        }

    }
}
