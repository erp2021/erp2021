﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_Process
    {
        public List<M_ProcessMasterEntity> get()
        {
            List<M_ProcessMasterEntity> list = new List<M_ProcessMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_PROCESS_Master.ToList().Where(x => x.IS_ENABLED == true);
                foreach (var item in records)
                {
                    M_ProcessMasterEntity entity = new M_ProcessMasterEntity();
                    entity.Process_Id = item.Process_Id;
                    entity.CODE = item.CODE;
                    entity.CREATED_BY = item.CREATED_BY;
                    entity.CREATED_ON = item.CREATED_ON;
                    entity.IS_ENABLED = item.IS_ENABLED;
                    entity.LFT = item.LFT;
                    entity.MEMBER_TYPE = item.MEMBER_TYPE;
                    entity.Process_Description = item.Process_Description;
                    entity.Process_Direction_Job = item.Process_Direction_Job;
                    entity.Process_Duration = item.Process_Duration;
                    entity.Process_Duration_Nos = item.Process_Duration_Nos;
                    entity.Process_Duration_Per = item.Process_Duration_Per;
                    entity.Process_Duration_Per_UOM = item.Process_Duration_Per_UOM;
                    entity.Process_Incharge_ID = item.Process_Incharge_ID;
                    if (item.M_UserMaster != null)
                    {
                        EmployeeEntity empentity = new EmployeeEntity();
                        empentity.ID = db.Employees.Where(x => x.UserId == item.M_UserMaster.Id).Select(x=>x.ID).FirstOrDefault();
                        empentity.Name = db.Employees.Where(x => x.UserId == item.M_UserMaster.Id).Select(x => x.Name).FirstOrDefault();
                        empentity.Gender = db.Employees.Where(x => x.UserId == item.M_UserMaster.Id).Select(x => x.Gender).FirstOrDefault();
                        entity.EmployeeMaster = empentity;
                    }
                    entity.Process_Machine_Type_Id = item.Process_Machine_Type_Id;
                    if (item.M_MachineTypeMaster != null)
                    {
                        M_MachineTypeMasterEntity machineentity = new M_MachineTypeMasterEntity();
                        machineentity.Machine_TypeId = item.M_MachineTypeMaster.Machine_TypeId;
                        machineentity.Machine_TypeName = item.M_MachineTypeMaster.Machine_TypeName;
                        entity.M_MachineTypeMaster = machineentity;
                    }
                    entity.ItemCategoryId = item.ItemCategoryId;
                    if (item.M_ItemCategoryMaster != null)
                    {
                        M_ItemCategoryMasterEntity itementity = new M_ItemCategoryMasterEntity();
                        itementity.Id = item.M_ItemCategoryMaster.Id;
                        itementity.Itemcategory = item.M_ItemCategoryMaster.Itemcategory;
                        entity.M_ItemCategoryMaster = itementity;
                    }
                    entity.Process_produce_BarCode = item.Process_produce_BarCode;
                    entity.Process_Produce_Output = item.Process_Produce_Output;
                    entity.Process_Wastage = item.Process_Wastage;
                    entity.RGT = item.RGT;
                    entity.SHORT_NAME = item.SHORT_NAME;
                    entity.UPDATED_BY = item.UPDATED_BY;
                    entity.UPDATED_ON = item.UPDATED_ON;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    list.Add(entity);
                }
            }
            return list;
        }

        public M_ProcessMasterEntity GetById(long id)
        {
            M_ProcessMasterEntity entity = new M_ProcessMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_PROCESS_Master.ToList().Where(x => x.Process_Id == id && x.IS_ENABLED == true);
                foreach (var item in records)
                {
                    entity.Process_Id = item.Process_Id;
                    entity.CODE = item.CODE;
                    entity.CREATED_BY = item.CREATED_BY;
                    entity.CREATED_ON = item.CREATED_ON;
                    entity.IS_ENABLED = item.IS_ENABLED;
                    entity.LFT = item.LFT;
                    entity.MEMBER_TYPE = item.MEMBER_TYPE;
                    entity.Process_Description = item.Process_Description;
                    entity.Process_Direction_Job = item.Process_Direction_Job;
                    entity.Process_Duration = item.Process_Duration;
                    entity.Process_Duration_Nos = item.Process_Duration_Nos;
                    entity.Process_Duration_Per = item.Process_Duration_Per;
                    entity.Process_Duration_Per_UOM = item.Process_Duration_Per_UOM;
                    if (item.M_UserMaster != null)
                    {
                        EmployeeEntity empentity = new EmployeeEntity();
                        empentity.ID = db.Employees.Where(x => x.UserId == item.M_UserMaster.Id).Select(x => x.ID).FirstOrDefault();
                        empentity.Name = db.Employees.Where(x => x.UserId == item.M_UserMaster.Id).Select(x => x.Name).FirstOrDefault();
                        empentity.Gender = db.Employees.Where(x => x.UserId == item.M_UserMaster.Id).Select(x => x.Gender).FirstOrDefault();
                        entity.EmployeeMaster = empentity;
                    }
                    entity.Process_Incharge_ID = item.Process_Incharge_ID;
                    entity.Process_Machine_Type_Id = item.Process_Machine_Type_Id;
                    if (item.M_MachineTypeMaster != null)
                    {
                        M_MachineTypeMasterEntity machineentity = new M_MachineTypeMasterEntity();
                        machineentity.Machine_TypeId = item.M_MachineTypeMaster.Machine_TypeId;
                        machineentity.Machine_TypeName = item.M_MachineTypeMaster.Machine_TypeName;
                        entity.M_MachineTypeMaster = machineentity;
                    }
                    entity.ItemCategoryId = item.ItemCategoryId;
                    if (item.M_ItemCategoryMaster != null)
                    {
                        M_ItemCategoryMasterEntity itementity = new M_ItemCategoryMasterEntity();
                        itementity.Id = item.M_ItemCategoryMaster.Id;
                        itementity.Itemcategory = item.M_ItemCategoryMaster.Itemcategory;
                        entity.M_ItemCategoryMaster = itementity;
                    }
                    entity.Process_produce_BarCode = item.Process_produce_BarCode;
                    entity.Process_Produce_Output = item.Process_Produce_Output;
                    entity.Process_Wastage = item.Process_Wastage;
                    entity.RGT = item.RGT;
                    entity.SHORT_NAME = item.SHORT_NAME;
                    entity.UPDATED_BY = item.UPDATED_BY;
                    entity.UPDATED_ON = item.UPDATED_ON;
                    entity.Branch_ID = item.BranchId;
                    entity.Company_ID = item.Company_ID;
                }
            }
            return entity;
        }
        public decimal Insert(M_ProcessMasterEntity entity)
        {
            decimal insertedid = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_PROCESS_Master record = new M_PROCESS_Master();
                record.Process_Id = entity.Process_Id;
                record.CODE = entity.CODE;
                record.CREATED_BY = entity.CREATED_BY;
                record.CREATED_ON = entity.CREATED_ON;
                record.IS_ENABLED = entity.IS_ENABLED;
                record.LFT = entity.LFT;
                record.MEMBER_TYPE = entity.MEMBER_TYPE;
                record.Process_Description = entity.Process_Description;
                record.Process_Direction_Job = entity.Process_Direction_Job;
                record.Process_Duration = entity.Process_Duration;
                record.Process_Duration_Nos = entity.Process_Duration_Nos;
                record.Process_Duration_Per = entity.Process_Duration_Per;
                record.Process_Duration_Per_UOM = entity.Process_Duration_Per_UOM;
                record.Process_Incharge_ID = entity.Process_Incharge_ID;
                record.Process_Machine_Type_Id = entity.Process_Machine_Type_Id;
                record.Process_produce_BarCode = entity.Process_produce_BarCode;
                record.Process_Produce_Output = entity.Process_Produce_Output;
                record.Process_Wastage = entity.Process_Wastage;
                record.ItemCategoryId = entity.ItemCategoryId;
                record.RGT = entity.RGT;
                record.SHORT_NAME = entity.SHORT_NAME;

                //New Code Added by Rahul on 09-01-2020
                //record.UPDATED_BY = entity.UPDATED_BY;
                //record.UPDATED_ON = entity.UPDATED_ON;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                try
                {
                    db.M_PROCESS_Master.Add(record);
                    db.SaveChanges();
                    insertedid = record.Process_Id;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return insertedid;
        }
        public bool Update(M_ProcessMasterEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_PROCESS_Master.Where(x => x.Process_Id == entity.Process_Id).FirstOrDefault();
                record.Process_Id = entity.Process_Id;
                record.CODE = entity.CODE;
                record.SHORT_NAME = entity.SHORT_NAME;
                record.IS_ENABLED = entity.IS_ENABLED;
                record.ItemCategoryId = entity.ItemCategoryId;
                record.LFT = entity.LFT;
                record.MEMBER_TYPE = entity.MEMBER_TYPE;
                record.Process_Description = entity.Process_Description;
                record.Process_Direction_Job = entity.Process_Direction_Job;
                record.Process_Duration = entity.Process_Duration;
                record.Process_Duration_Nos = entity.Process_Duration_Nos;
                record.Process_Duration_Per = entity.Process_Duration_Per;
                record.Process_Duration_Per_UOM = entity.Process_Duration_Per_UOM;
                record.Process_Incharge_ID = entity.Process_Incharge_ID;
                record.Process_Machine_Type_Id = entity.Process_Machine_Type_Id;
                record.Process_produce_BarCode = entity.Process_produce_BarCode;
                record.Process_Produce_Output = entity.Process_Produce_Output;
                record.Process_Wastage = entity.Process_Wastage;
                record.RGT = entity.RGT;
                record.UPDATED_ON = entity.UPDATED_ON;

                //New Code Added by Rahul on 09-01-2020
                record.UPDATED_BY = entity.UPDATED_BY;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;
                try
                {
                    db.SaveChanges();
                    IsUpdated = true;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return IsUpdated;
        }
        public bool Delete(long id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.M_PROCESS_Master.Where(x => x.Process_Id == id).FirstOrDefault();
                item.IS_ENABLED = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
            } return IsDeleted;
        }
    }
}
