﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;

namespace Garments_ERP.Data.Admin
{
    public class cl_Style
    {

        public long Insert(M_StyleMasterEntity entity)
        {
            long insertedid = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_StyleMaster record = new M_StyleMaster();
                record.Id = entity.Id;
                record.StyleNo = entity.StyleNo;
                record.Stylename = entity.Stylename;
                record.StyleShortname = entity.StyleShortname;
                record.Styledescription = entity.Styledescription;

                record.Createddate = entity.Createddate;
                record.Createdby = entity.Createdby;
                //New Code Added by Rahul on 09-01-2020
                //record.Updateddate = entity.Updateddate;
                //record.UpdatedBy = entity.UpdatedBy;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                record.IsActive = entity.IsActive;

                db.M_StyleMaster.Add(record);
                db.SaveChanges();
                insertedid = record.Id;
            }
            return insertedid;
        }

        public List<M_StyleMasterEntity> get()
        {

            List<M_StyleMasterEntity> list = new List<M_StyleMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_StyleMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_StyleMasterEntity entity = new M_StyleMasterEntity();
                    entity.Id = item.Id;
                    entity.StyleNo = item.StyleNo;
                    entity.Stylename = item.Stylename;
                    entity.StyleShortname = item.StyleShortname;
                    entity.Styledescription = item.Styledescription;

                    entity.Createddate = item.Createddate;
                    entity.Createdby = item.Createdby;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    list.Add(entity);
                }
            }
            return list;

        }

        public bool Update(M_StyleMasterEntity entity)
        {
            bool isupdated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_StyleMaster.Find(entity.Id);
                record.StyleNo = entity.StyleNo;
                record.Stylename = entity.Stylename;
                record.StyleShortname = entity.StyleShortname;
                record.Styledescription = entity.Styledescription;

                //New Code Added by Rahul on 09-01-2020
                record.UpdatedBy = entity.UpdatedBy;
                record.Updateddate = entity.Updateddate;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;
                
                db.SaveChanges();
                isupdated = true;
            }

            return isupdated;

        }
        public bool Delete(int id)
        {
            bool isdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_StyleMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                isdelete = true;
            }
            return isdelete;
        }
        public bool CheckStyleNameExistance(string Stylename, int? Id,int companyid,int branchid)
        {
            bool result = true;

            using (var db = new GarmentERPDBEntities())
            {
                if (Id == null)
                {
                    var stylename = db.M_StyleMaster.Where(x => x.Stylename == Stylename && x.IsActive == true && x.Company_ID==companyid && x.BranchId==branchid).FirstOrDefault();
                    if (stylename != null)
                        result = false;
                }
                else
                {
                    var stylename = db.M_StyleMaster.Where(x => x.Stylename == Stylename && x.Id != Id && x.IsActive == true && x.Company_ID == companyid && x.BranchId == branchid).FirstOrDefault();
                    if (stylename != null)
                        result = false;
                }


            }
            return result;
        }

    }
}
