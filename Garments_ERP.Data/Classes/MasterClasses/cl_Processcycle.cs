﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;

namespace Garments_ERP.Data.Admin
{
    public class cl_Processcycle
    {

        public List<M_ProcessCycleEntity> get()
        {
            List<M_ProcessCycleEntity> list = new List<M_ProcessCycleEntity>();

            using (var db = new GarmentERPDBEntities())
            {

                var records = db.M_ProcessCycleMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_ProcessCycleEntity entity = new M_ProcessCycleEntity();
                    entity.Prooce_Cycle_id = item.Prooce_Cycle_id;
                    entity.Process_cycle_Name = item.Process_cycle_Name;
                    entity.Processcysledesc = item.Processcysledesc;
                    entity.Createddate = item.Createddate;
                    entity.Createdby = item.Createdby;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    list.Add(entity);

                }
            }
            return list;
        }

        public List<ProcessCycleTransactionEntity> Getbycycleid(int id)
        {
            using (var db = new GarmentERPDBEntities())
            {
                var recordlist = db.ProcessCycleTransactions.Where(x => x.Processcycleid == id && x.IsActive == true).ToList();
                List<ProcessCycleTransactionEntity> translist = new List<ProcessCycleTransactionEntity>();
                foreach (var record in recordlist)
                {
                    ProcessCycleTransactionEntity tranrecord = new ProcessCycleTransactionEntity();
                    tranrecord.Id = record.Id;
                    tranrecord.ItemcategoryIn = record.ItemcategoryIn;
                    M_ItemCategoryMasterEntity cateentity1 = new M_ItemCategoryMasterEntity();
                    if (record.M_ItemCategoryMaster != null)
                    {
                        cateentity1.Id = record.M_ItemCategoryMaster.Id;
                        cateentity1.Itemcategory = record.M_ItemCategoryMaster.Itemcategory;
                        tranrecord.M_ItemCategoryentity1 = cateentity1;
                    }
                    M_ItemCategoryMasterEntity cateentity2 = new M_ItemCategoryMasterEntity();
                    if (record.M_ItemCategoryMaster1 != null)
                    {
                        cateentity2.Id = record.M_ItemCategoryMaster1.Id;
                        cateentity2.Itemcategory = record.M_ItemCategoryMaster1.Itemcategory;
                        tranrecord.M_ItemCategoryentity2 = cateentity2;
                    }
                    M_ProcessMasterEntity processentity = new M_ProcessMasterEntity();
                    if (record.M_PROCESS_Master != null)
                    {
                        processentity.Process_Id = record.M_PROCESS_Master.Process_Id;
                        processentity.Process_Incharge_ID = record.M_PROCESS_Master.Process_Incharge_ID;
                        processentity.SHORT_NAME = record.M_PROCESS_Master.SHORT_NAME;
                        tranrecord.M_PROCESS_entity = processentity;
                    }
                    tranrecord.ItemcategoryOut = record.ItemcategoryOut;
                    tranrecord.Processid = record.Processid;
                    tranrecord.Processsequence = record.Processsequence;
                    tranrecord.comment = record.comment;
                    tranrecord.Comapany_ID = record.Company_ID;
                    tranrecord.Branch_ID = record.BranchId;
                    translist.Add(tranrecord);

                }
                return translist;
            }
        }
        public M_ProcessCycleEntity getbyid(int id)
        {
            M_ProcessCycleEntity entity = new M_ProcessCycleEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.M_ProcessCycleMaster.Find(id);
                entity.Prooce_Cycle_id = item.Prooce_Cycle_id;
                entity.Process_cycle_Name = item.Process_cycle_Name;
                entity.Processcysledesc = item.Processcysledesc;
                entity.Createddate = item.Createddate;
                entity.Createdby = item.Createdby;
                entity.Updateddate = item.Updateddate;
                entity.Updatedby = item.Updatedby;
                entity.Company_ID = item.Company_ID;
                entity.Branch_ID = item.BranchId;

                var recordlist = db.ProcessCycleTransactions.Where(x => x.Processcycleid == id && x.IsActive == true && x.Company_ID==item.Company_ID && x.BranchId==item.BranchId).ToList();
                List<ProcessCycleTransactionEntity> translist = new List<ProcessCycleTransactionEntity>();
                foreach (var record in recordlist)
                {
                    ProcessCycleTransactionEntity tranrecord = new ProcessCycleTransactionEntity();
                    tranrecord.Id = record.Id;

                    List<M_ProcessInCategoryDetailsEntity> PinCent = new List<M_ProcessInCategoryDetailsEntity>();
                    var iteminid =db.M_ProcessInCategoryDetails.Where(x=>x.ProCycTran_ID== record.Id).Select(x=>x.ItemcategoryIn).ToList();
                    foreach(var itemindata in iteminid)
                    {
                        M_ProcessInCategoryDetailsEntity incatent = new M_ProcessInCategoryDetailsEntity();
                        incatent.ItemcategoryIn = itemindata;
                        M_ItemCategoryMasterEntity cateentity1 = new M_ItemCategoryMasterEntity();
                        var Initem = db.M_ItemCategoryMaster.Where(x => x.Id ==itemindata).SingleOrDefault();
                        cateentity1.Id = Initem.Id;
                        cateentity1.Itemcategory = Initem.Itemcategory;
                        incatent.M_ItemCategoryMaster = cateentity1;
                        PinCent.Add(incatent);
                    }

                    tranrecord.M_ProcessInCategoryDetails = PinCent;
                    M_ItemCategoryMasterEntity cateentity2 = new M_ItemCategoryMasterEntity();
                    if (record.M_ItemCategoryMaster1 != null)
                    {
                        cateentity2.Id = record.M_ItemCategoryMaster1.Id;
                        cateentity2.Itemcategory = record.M_ItemCategoryMaster1.Itemcategory;
                        tranrecord.M_ItemCategoryentity2 = cateentity2;

                    }
                    M_ProcessMasterEntity processentity = new M_ProcessMasterEntity();
                    if (record.M_PROCESS_Master != null)
                    {
                        processentity.Process_Id = record.M_PROCESS_Master.Process_Id;
                        processentity.Process_Incharge_ID = record.M_PROCESS_Master.Process_Incharge_ID;
                        processentity.SHORT_NAME = record.M_PROCESS_Master.SHORT_NAME;
                        tranrecord.M_PROCESS_entity = processentity;

                    }

                    tranrecord.ItemcategoryOut = record.ItemcategoryOut;
                    tranrecord.Processid = record.Processid;
                    tranrecord.Processsequence = record.Processsequence;
                    tranrecord.comment = record.comment;
                    tranrecord.Comapany_ID = record.Company_ID;
                    tranrecord.Branch_ID = record.BranchId;
                    translist.Add(tranrecord);

                }
                entity.processcyclelist = translist;
            }
            return entity;
        }


        public int Insert(M_ProcessCycleEntity entity)
        {
           
            int insertid = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    M_ProcessCycleMaster record = new M_ProcessCycleMaster();
                    record.Process_cycle_Name = entity.Process_cycle_Name;
                    record.Processcysledesc = entity.Processcysledesc;
                    record.Createddate = entity.Createddate;
                    record.Createdby = entity.Createdby;

                    //New Code Added by Rahul on 09-01-2020
                    //record.Updateddate = entity.Updateddate;
                    //record.Updatedby = entity.Updatedby;
                    record.Company_ID = entity.Company_ID;
                    record.BranchId = entity.Branch_ID;

                    record.IsActive = true;
                    db.M_ProcessCycleMaster.Add(record);
                    db.SaveChanges();
                    insertid = record.Prooce_Cycle_id;
                    if (insertid > 0)
                    {

                        // ProcessCycleTransactionEntity tranentity = new ProcessCycleTransactionEntity();
                        foreach (var item in entity.processcyclelist)
                        {
                            item.IsActive = true;
                            ProcessCycleTransaction tranrecord = new ProcessCycleTransaction();
                         
                            tranrecord.Processcycleid = insertid;
                            //  tranrecord.ItemcategoryIn = item.ItemcategoryIn;
                            tranrecord.ItemcategoryOut = item.ItemcategoryOut;
                            tranrecord.Processid = item.Processid;
                            tranrecord.Processsequence = item.Processsequence;

                            //New Code Added by Rahul on 09-01-2020
                            tranrecord.Createdby = item.Createdby;
                            tranrecord.Company_ID = item.Comapany_ID;
                            tranrecord.BranchId = item.Branch_ID;

                            tranrecord.IsActive = true;
                            string[] values = item.incategeories.Split(',');
                            int j = 0;
                            List<M_ProcessInCategoryDetailsEntity> incatelist = new List<M_ProcessInCategoryDetailsEntity>();
                            foreach (var value in values)
                            {
                                M_ProcessInCategoryDetailsEntity incateentity = new M_ProcessInCategoryDetailsEntity();
                                incateentity.ItemcategoryIn = Convert.ToInt32(value);
                                incateentity.IsActive = true;
                                incatelist.Add(incateentity);
                                j++;
                            }

                            //for (int i = 0; i < values.Length; i++)
                            //{
                            //    M_ProcessInCategoryDetailsEntity incateentity = new M_ProcessInCategoryDetailsEntity();
                            //    incateentity.ItemcategoryIn = Convert.ToInt32(values[i]);
                            //    incatelist.Add(incateentity);
                            //}
                            db.ProcessCycleTransactions.Add(tranrecord);
                            db.SaveChanges();
                            var tid = tranrecord.Id;
                            if (tid > 0)
                            {

                                foreach (var incaterecord in incatelist)
                                {
                                    M_ProcessInCategoryDetails incategory = new M_ProcessInCategoryDetails();
                                    incategory.ProCycTran_ID = tid;
                                    incategory.Id = incaterecord.Id;
                                    incategory.ItemcategoryIn = incaterecord.ItemcategoryIn;
                                    incategory.IsActive = true;
                                    incategory.Company_ID = entity.Company_ID;
                                    incategory.BranchId = entity.Branch_ID;
                                    db.M_ProcessInCategoryDetails.Add(incategory);
                                    db.SaveChanges();
                                    var inid = incategory.Id;
                                    // values[i] = values[i].Trim();
                                }
                            }

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return insertid;
        }

        public bool Update(M_ProcessCycleEntity entity)
        {
            bool updated = false;

            using (var db = new GarmentERPDBEntities())
            {
                // var record = db.M_ProcessCycleMaster.Find(entity.Prooce_Cycle_id);

                //record.Process_cycle_Name = entity.Process_cycle_Name;
                //record.Processcysledesc = entity.Processcysledesc;
                //record.Createddate = entity.Createddate;
                //record.Createdby = entity.Createdby;
                //record.Updateddate = DateTime.Now;
                //record.Updatedby = entity.Updatedby;
                //db.SaveChanges();
                //updated = true;
                //   if(updated==true)
                //  {
                foreach (var item in entity.processcyclelist)
                {

                    item.IsActive = true;
                    ProcessCycleTransaction tranrecord = db.ProcessCycleTransactions.Find(item.Id);
                    tranrecord.Id = item.Id;
                    tranrecord.ItemcategoryIn = item.ItemcategoryIn;
                    tranrecord.ItemcategoryOut = item.ItemcategoryOut;
                    tranrecord.Processid = item.Processid;
                    tranrecord.Processsequence = item.Processsequence;
                    tranrecord.comment = item.comment;
                    tranrecord.IsActive = true;

                    db.SaveChanges();
                    updated = true;
                }

            }

            return updated;
        }
        public bool Delete(int id)
        {
            bool deleted = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ProcessCycleMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                deleted = true;
            }

            return deleted;
        }

    }
}
