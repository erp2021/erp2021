﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_MachineType
    {

        public List<M_MachineTypeMasterEntity> get()
        {
            List<M_MachineTypeMasterEntity> list = new List<M_MachineTypeMasterEntity>();
            using (var db=new GarmentERPDBEntities())
            {
                var records=db.M_MachineTypeMaster.ToList().Where(x=>x.IsActive==true);
                foreach (var item in records)
                {
                    M_MachineTypeMasterEntity entity = new M_MachineTypeMasterEntity();
                    entity.Machine_TypeId = item.Machine_TypeId;
                    entity.Machine_TypeName = item.Machine_TypeName;
                    entity.Machine_TypeDescription = item.Machine_TypeDescription;
                    entity.companyid = item.Company_ID;
                    entity.branchid = item.BranchId;
                    list.Add(entity);
                }              
                
            }
            return list;
        }
    }
}
