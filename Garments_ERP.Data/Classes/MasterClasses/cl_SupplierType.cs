﻿
using Garments_ERP.Entity;
using Garments_ERP.Data.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
   public class cl_SupplierType
    {
       public List<M_SupplierTypeMasterEntity> get()
       {
           List<M_SupplierTypeMasterEntity> list = new List<M_SupplierTypeMasterEntity>();
           using (var db=new GarmentERPDBEntities())
           {
               var records = db.M_SupplierTypeMaster.ToList().Where(x => x.IsActive == true);
               foreach (var item in records)
               {
                   M_SupplierTypeMasterEntity entity = new M_SupplierTypeMasterEntity();
                   entity.Id = item.Id;
                   entity.TypeName = item.TypeName;
                   entity.Typedesc = item.Typedesc;
                   list.Add(entity);
               }
           }
           return list;
       }
    }
}
