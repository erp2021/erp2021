﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_role
    {
        public List<M_RoleEntity> get()
        {

            List<M_RoleEntity> list = new List<M_RoleEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_Role.ToList();
                foreach (var item in records)
                {
                    M_RoleEntity entity = new M_RoleEntity();
                    entity.roleId = item.roleId;
                    entity.roleName = item.roleName;
                    entity.Discription = item.Discription;
                    entity.CreatedOn = item.CreatedOn;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdateOn = item.UpdateOn;
                    entity.UpdateBy = item.UpdateBy;
                    entity.IsActive = item.IsActive;
                    entity.ParentId = item.ParentId;
                    list.Add(entity);
                }
            }
            return list;
        }

        public bool ActiveDeactive(int id, string Status)
        {
            var isstatus = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_Role.Find(id);
                record.IsActive = Status == "Deactive" ? false : true;
                db.SaveChanges();
                isstatus = true;
            }
            return isstatus;
        }

        public long Insert(M_RoleEntity entity)
        {
            long id = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    M_Role item = new M_Role();
                    item.roleId = entity.roleId;
                    item.roleName = entity.roleName;
                    item.Discription = entity.Discription;
                    item.ParentId = entity.ParentId;
                    item.IsActive = true;
                    item.CreatedBy = entity.CreatedBy;
                    item.CreatedOn = DateTime.Now;
                    db.Configuration.ValidateOnSaveEnabled = false;
                    db.M_Role.Add(item);
                    db.SaveChanges();
                    db.Configuration.ValidateOnSaveEnabled = true;
                    id = item.roleId;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return id;
        }

        public bool Update(M_RoleEntity entity)
        {
            var isstatus = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.M_Role.Find(entity.roleId);
                    record.roleId = entity.roleId;
                    record.roleName = entity.roleName;
                    record.Discription = entity.Discription;
                    record.IsActive = true;
                    record.UpdateBy = entity.UpdateBy;
                    record.UpdateOn = DateTime.Now;
                    db.SaveChanges();
                    isstatus = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isstatus;
        }

        public List<M_User_RoleEntity> GetUserRole()
        {
            List<M_User_RoleEntity> list = new List<M_User_RoleEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_User_Role.Where(x => x.IsActive == true).ToList();
                if(record!=null)
                {
                    foreach(var data in record)
                    {
                        M_User_RoleEntity entity = new M_User_RoleEntity();
                        entity.UserRoleId = data.UserRoleId;
                        entity.roleId = data.roleId;
                        entity.UserId = data.UserId;
                        entity.DepartmentId = data.DepartmentId;
                        entity.CompanyId = data.CompanyId;
                        entity.BranchId = data.BranchId;
                        entity.IsActive = data.IsActive;
                        entity.CreatedOn = data.CreatedOn;
                        entity.CreatedBy = data.CreatedBy;
                        entity.UpdatedOn = data.UpdatedOn;
                        entity.UpdatedBy = data.UpdatedBy;
                        list.Add(entity);
                    }
                }
            }
            return list;
        }

    }
}
