﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System.Xml;
using Garments_ERP.Data.Data;



namespace Garments_ERP.Data.Admin
{
    public class cl_ItemCategory
    {
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;

        public List<M_ItemCategoryMasterEntity> get()
        {
            List<M_ItemCategoryMasterEntity> list = new List<M_ItemCategoryMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ItemCategoryMaster.ToList().Where(x => x.IsActive == true);

                foreach (var item in records)
                {
                    M_ItemCategoryMasterEntity entity = new M_ItemCategoryMasterEntity();
                    entity.Id = item.Id;
                    entity.Itemcategory = item.Itemcategory;
                    entity.Itemcategorydesc = item.Itemcategorydesc;
                    entity.IsActive = item.IsActive;
                    entity.Createddate = item.Createddate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    M_ItemGroupEntity group = new M_ItemGroupEntity();
                    if (item.M_ItemGroupMaster != null)
                    {
                        group.Id = item.M_ItemGroupMaster.Id;
                        group.Itemgroup = item.M_ItemGroupMaster.Itemgroup;
                        entity.M_ItemGroupentity = group;
                    }
                    if (item.Itemgroupid != null)
                    {
                        entity.Itemgroupid = (int)item.Itemgroupid;
                    }
                    list.Add(entity);

                }


            }
            return list;
        }

        public List<M_ItemCategoryMasterEntity> getRawSemifinsh()
        {
            List<M_ItemCategoryMasterEntity> list = new List<M_ItemCategoryMasterEntity>();
            DataSet dt = new DataSet();
            using (var db = new GarmentERPDBEntities())
            {
                using (SqlConnection cs = new SqlConnection(con))
                {

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cs;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SP_Attribute";
                    cmd.Parameters.Add("type", SqlDbType.Int).Value = 31; //Get List Of Raw Material, Semi Finished Category
                    cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = "";
                    cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "";
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    foreach (DataTable table in dt.Tables)
                    {
                        foreach (DataRow dr in table.Rows)
                        {
                            M_ItemCategoryMasterEntity entity = new M_ItemCategoryMasterEntity();
                            entity.Id = Convert.ToInt64(dr["Id"]);
                            entity.Itemcategory = Convert.ToString(dr["Itemcategory"]);
                            entity.Itemcategorydesc = Convert.ToString(dr["Itemcategorydesc"]);
                            entity.IsActive = Convert.ToBoolean(dr["IsActive"]);
                            int grpid = Convert.ToInt32(dr["Itemgroupid"]);
                            M_ItemGroupEntity group = new M_ItemGroupEntity();
                            if (grpid >0)
                            {
                                var grpdata = db.M_ItemGroupMaster.Where(x => x.Id == grpid).FirstOrDefault();
                                group.Id = grpdata.Id;
                                group.Itemgroup = grpdata.Itemgroup;
                                entity.M_ItemGroupentity = group;
                            }
                            list.Add(entity);
                        }
                    }
                }
            }
            return list;
        }

        public long Insert(M_ItemCategoryMasterEntity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_ItemCategoryMaster record = new M_ItemCategoryMaster();
                record.Id = entity.Id;
                record.Itemcategory = entity.Itemcategory;
                record.Itemcategorydesc = entity.Itemcategorydesc;
                record.IsActive = true;
                record.Createddate = entity.Createddate;
                record.CreatedBy = entity.CreatedBy;

                //New Code Added by Rahul on 09-01-2020
                //record.Updateddate = entity.Updateddate;
                //record.UpdatedBy = entity.UpdatedBy;
                record.Itemgroupid = entity.Itemgroupid;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;
                db.M_ItemCategoryMaster.Add(record);
                db.SaveChanges();
                id = record.Id;
            }
            return id;

        }
        public bool Update(M_ItemCategoryMasterEntity entity)
        {

            bool isupdated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ItemCategoryMaster.Find(entity.Id);
                record.Itemgroupid = entity.Itemgroupid;
                record.Itemcategory = entity.Itemcategory;
                record.Itemcategorydesc = entity.Itemcategorydesc;
                record.Updateddate = DateTime.Now;
                //New Code Added by Rahul on 09-01-2020
                record.UpdatedBy = entity.UpdatedBy;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                db.SaveChanges();
                isupdated = true;
            }
            return isupdated;
        }

        public bool Delete(int id)
        {
            var isdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ItemCategoryMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                isdelete = true;
            }
            return isdelete;
        }
        public bool CheckExistance(string ItemCategory, int? Id, int Itemgroupid,int companyid,int branchid)
        {
            bool result = true;

            using (var db = new GarmentERPDBEntities())
            {
                if (Id == null)
                {
                    var itemcatestatus = db.M_ItemCategoryMaster.Where(x => x.Itemcategory == ItemCategory && x.Itemgroupid == Itemgroupid && x.IsActive == true && x.Company_ID==companyid && x.BranchId==branchid).FirstOrDefault();
                    if (itemcatestatus != null)
                    {
                        result = false;
                    }
                    else
                    {
                        var isexistrecord = db.M_ItemCategoryMaster.Where(x => x.Itemcategory == ItemCategory && x.IsActive == true && x.Company_ID == companyid && x.BranchId == branchid).FirstOrDefault();
                        if (isexistrecord != null)
                        {
                            result = false;
                        }
                    }
                }
                else
                {
                    var itemcatestatus = db.M_ItemCategoryMaster.Where(x => x.Itemcategory == ItemCategory && x.Id != Id && x.Itemgroupid == Itemgroupid && x.IsActive == true && x.Company_ID == companyid && x.BranchId == branchid).FirstOrDefault();
                    if (itemcatestatus != null)
                        result = false;
                }
            }
            return result;
        }

    }
}
