﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_WarehouseMaster
    {
        public List<M_WarehouseEntity> get(int companyid,int branchid)
        {
            List<M_WarehouseEntity> list = new List<M_WarehouseEntity>();

            using (var db = new GarmentERPDBEntities())
            {
               
                var records = db.M_WAREHOUSE.ToList().Where(x => x.IS_ENABLED == true && x.Company_ID==companyid && x.BranchId==branchid);
                foreach (var item in records)
                {
                    M_WarehouseEntity entity = new M_WarehouseEntity();
                    entity.ID = item.ID;
                    entity.WAREHOUSE_CODE = item.WAREHOUSE_CODE;
                    entity.SHORT_NAME = item.SHORT_NAME;
                    entity.ADDRESS_ID = item.ADDRESS_ID;
                    entity.CREATED_BY = item.CREATED_BY;
                    entity.CREATED_ON = item.CREATED_ON;
                    entity.IS_ENABLED = item.IS_ENABLED;
                    //if (item.M_ROLE != null)
                    //{
                    //    M_WarehouseRoleMasterEntity roleentity = new M_WarehouseRoleMasterEntity();
                    //    roleentity.ID = item.M_ROLE.ID;
                    //    roleentity.SHORT_NAME = item.M_ROLE.SHORT_NAME;
                    //    entity.M_ROLE = roleentity;
                    //}
                    entity.ORDINAL = item.ORDINAL;
                    entity.OWNER_DEPT_ID = item.OWNER_DEPT_ID;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                        deptentity.Id = item.M_DepartmentMaster.Id;
                        deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        entity.M_DepartmentMaster = deptentity;
                    }
                    entity.OWNER_ENTITY_ID = item.OWNER_ENTITY_ID;
                    if (item.OWNER_ENTITY_ID != null)
                    {
                        EmployeeEntity empentity = new EmployeeEntity();
                        empentity.ID = db.Employees.Where(x => x.UserId == item.OWNER_ENTITY_ID).Select(x => x.ID).FirstOrDefault();
                        empentity.Name = db.Employees.Where(x => x.UserId == item.OWNER_ENTITY_ID).Select(x => x.Name).FirstOrDefault();
                        empentity.Gender = db.Employees.Where(x => x.UserId == item.OWNER_ENTITY_ID).Select(x => x.Gender).FirstOrDefault();
                        entity.Employee = empentity;
                    }

                    entity.UPDATED_BY = item.UPDATED_BY;
                    entity.UPDATED_ON = item.UPDATED_ON;
                    entity.WAREHOUSE_ROLE = item.WAREHOUSE_ROLE;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    M_RoleEntity ent = new M_RoleEntity();
                    ent.roleName = db.M_Role.Where(x => x.roleId == item.WAREHOUSE_ROLE).Select(x => x.roleName).FirstOrDefault();
                    entity.M_ROLE = ent;

                    entity.WAREHOUSE_TYPE_ID = item.WAREHOUSE_TYPE_ID;
                    if (item.M_WarehouseTypeMaster != null)
                    {
                        M_WarehouseTypeMasterEntity warehousetypeentity = new M_WarehouseTypeMasterEntity();
                        warehousetypeentity.WAREHOUSE_TYPE_ID = item.M_WarehouseTypeMaster.WAREHOUSE_TYPE_ID;
                        warehousetypeentity.TypeName = item.M_WarehouseTypeMaster.TypeName;
                        warehousetypeentity.Typedesc = item.M_WarehouseTypeMaster.Typedesc;
                        entity.M_WarehouseTypeMaster = warehousetypeentity;
                    }
                    list.Add(entity);

                }
            }
            return list;
        }


        public M_WarehouseEntity getbyid(long id)
        {
            M_WarehouseEntity entity = new M_WarehouseEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.M_WAREHOUSE.Find(id);
                entity.ID = item.ID;
                entity.WAREHOUSE_CODE = item.WAREHOUSE_CODE;
                entity.SHORT_NAME = item.SHORT_NAME;
                entity.ADDRESS_ID = item.ADDRESS_ID;
                entity.CREATED_BY = item.CREATED_BY;
                entity.CREATED_ON = item.CREATED_ON;
                entity.IS_ENABLED = item.IS_ENABLED;
                //if (item.M_ROLE != null)
                //{
                //    M_WarehouseRoleMasterEntity roleentity = new M_WarehouseRoleMasterEntity();
                //    roleentity.ID = item.M_ROLE.ID;
                //    roleentity.SHORT_NAME = item.M_ROLE.SHORT_NAME;
                //    entity.M_ROLE = roleentity;
                //}
                entity.ORDINAL = item.ORDINAL;
                entity.OWNER_DEPT_ID = item.OWNER_DEPT_ID;
                if (item.M_DepartmentMaster != null)
                {
                    M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                    deptentity.Id = item.M_DepartmentMaster.Id;
                    deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                    entity.M_DepartmentMaster = deptentity;
                }
                entity.OWNER_ENTITY_ID = item.OWNER_ENTITY_ID;
                if (item.OWNER_ENTITY_ID != null)
                {
                    EmployeeEntity empentity = new EmployeeEntity();
                    empentity.ID = db.Employees.Where(x => x.UserId == item.OWNER_ENTITY_ID).Select(x => x.ID).FirstOrDefault();
                    empentity.Name = db.Employees.Where(x => x.UserId == item.OWNER_ENTITY_ID).Select(x => x.Name).FirstOrDefault();
                    empentity.Gender = db.Employees.Where(x => x.UserId == item.OWNER_ENTITY_ID).Select(x => x.Gender).FirstOrDefault();
                    entity.Employee = empentity;
                }
                entity.Company_ID = item.Company_ID;
                entity.Branch_ID = item.BranchId;
                entity.UPDATED_BY = item.UPDATED_BY;
                entity.UPDATED_ON = item.UPDATED_ON;
                entity.WAREHOUSE_ROLE = item.WAREHOUSE_ROLE;
                entity.WAREHOUSE_TYPE_ID = item.WAREHOUSE_TYPE_ID;
                if (item.M_WarehouseTypeMaster != null)
                {
                    M_WarehouseTypeMasterEntity warehousetypeentity = new M_WarehouseTypeMasterEntity();
                    warehousetypeentity.WAREHOUSE_TYPE_ID = item.M_WarehouseTypeMaster.WAREHOUSE_TYPE_ID;
                    warehousetypeentity.TypeName = item.M_WarehouseTypeMaster.TypeName;
                    warehousetypeentity.Typedesc = item.M_WarehouseTypeMaster.Typedesc;
                    entity.M_WarehouseTypeMaster = warehousetypeentity;
                }
            }
            return entity;
        }


        public long Insert(M_WarehouseEntity entity)
        {
            long insertid = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    M_WAREHOUSE record = new M_WAREHOUSE();
                    record.ID = entity.ID;
                    record.ADDRESS_ID = 1;
                    record.CREATED_BY = entity.CREATED_BY;
                    record.CREATED_ON = entity.CREATED_ON;
                    record.IS_ENABLED = entity.IS_ENABLED;
                    record.ORDINAL = entity.ORDINAL;
                    record.OWNER_DEPT_ID = entity.OWNER_DEPT_ID;
                    record.OWNER_ENTITY_ID = entity.OWNER_ENTITY_ID;
                    record.SHORT_NAME = entity.SHORT_NAME;
                    record.UPDATED_BY = entity.UPDATED_BY;
                    record.UPDATED_ON = entity.UPDATED_ON;
                    record.WAREHOUSE_CODE = entity.WAREHOUSE_CODE;
                    record.WAREHOUSE_ROLE = entity.WAREHOUSE_ROLE;
                    record.WAREHOUSE_TYPE_ID = entity.WAREHOUSE_TYPE_ID;

                    //New Code Added by Rahul on 09-01-2020
                    record.Company_ID = entity.Company_ID;
                    record.BranchId = entity.Branch_ID;

                    db.M_WAREHOUSE.Add(record);
                    db.SaveChanges();
                    insertid = record.ID;

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return insertid;
        }

        public bool Update(M_WarehouseEntity entity)
        {
            bool updated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_WAREHOUSE.Find(entity.ID);
                record.ID = entity.ID;
                record.ADDRESS_ID = 1;
                record.IS_ENABLED = entity.IS_ENABLED;
                record.ORDINAL = entity.ORDINAL;
                record.OWNER_DEPT_ID = entity.OWNER_DEPT_ID;
                record.OWNER_ENTITY_ID = entity.OWNER_ENTITY_ID;
                record.SHORT_NAME = entity.SHORT_NAME;
                record.UPDATED_BY = entity.UPDATED_BY;
                record.UPDATED_ON = entity.UPDATED_ON;
                record.WAREHOUSE_CODE = entity.WAREHOUSE_CODE;
                record.WAREHOUSE_ROLE = entity.WAREHOUSE_ROLE;
                record.WAREHOUSE_TYPE_ID = entity.WAREHOUSE_TYPE_ID;

                //New Code Added by Rahul on 09-01-2020
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                db.SaveChanges();
                updated = true;
            }

            return updated;
        }
        public bool Delete(long id)
        {
            bool deleted = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_WAREHOUSE.Find(id);
                record.IS_ENABLED = false;
                db.SaveChanges();
                deleted = true;
            }

            return deleted;
        }
    }
}
