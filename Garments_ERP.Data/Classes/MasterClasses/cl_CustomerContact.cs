﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_CustomerContact
    {
        public long Insert(M_CustomerContactDetailsEntity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_CustomerContactDetails item = new M_CustomerContactDetails();
                item.Contactpersonname = entity.Contactpersonname;
                item.Createdby = entity.Createdby;
                item.Createddate = entity.Createddate;
                item.Customerid = entity.Customerid;
                item.Email = entity.Email;
                item.Department = entity.Department;
                item.Faxno = entity.Faxno;
                item.Id = entity.Id;
                item.IsActive = entity.IsActive;
                item.Mobileno = entity.Mobileno;
                item.Remark = entity.Remark;
                item.Telephoneno = entity.Telephoneno;
                item.Updatedby = entity.Updatedby;
                item.Updateddate = entity.Updateddate;
                db.M_CustomerContactDetails.Add(item);
                db.SaveChanges();
                id = item.Id;
            }
            return id;
        }
        public List<M_CustomerContactDetailsEntity> GetById(long id)
        {
            List<M_CustomerContactDetailsEntity> list = new List<M_CustomerContactDetailsEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_CustomerContactDetails.Where(x => x.Customerid == id && x.IsActive == true);
                foreach (var item in records)
                {
                    M_CustomerContactDetailsEntity entity = new M_CustomerContactDetailsEntity();
                    entity.Id = item.Id;
                    entity.Contactpersonname = item.Contactpersonname;
                    entity.Mobileno = item.Mobileno;
                    entity.Email = item.Email;
                    entity.Customerid = item.Customerid;
                    entity.Faxno = item.Faxno;
                    entity.Department = item.Department;
                    entity.Remark = item.Remark;
                    entity.Telephoneno = item.Telephoneno;                  
                    if (item.M_DepartmentMaster !=null)
                    {
                        M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                        deptentity.Id = item.M_DepartmentMaster.Id;
                        deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        deptentity.Shortname = item.M_DepartmentMaster.Shortname;
                        entity.M_DepartmentMaster = deptentity;
                    }
                    if (item.M_CustomerMaster != null)
                    {
                        M_CustomerMasterEntity customerentity = new M_CustomerMasterEntity();
                        customerentity.Id = item.M_CustomerMaster.Id;
                        customerentity.contactname = item.M_CustomerMaster.contactname;
                        entity.M_CustomerMaster = customerentity;
                    }
                    list.Add(entity);
                }
            }
            return list;
        }
        public bool Update(M_CustomerContactDetailsEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_CustomerContactDetails.Where(x => x.Id == entity.Id).FirstOrDefault();
                record.Id = entity.Id;
                record.IsActive = entity.IsActive;
                record.Contactpersonname = entity.Contactpersonname;
                record.Department = entity.Department;
                record.Email = entity.Email;
                record.Faxno = entity.Faxno;
                record.Mobileno = entity.Mobileno;
                record.Remark = entity.Remark;
                record.Telephoneno = entity.Telephoneno;
                record.Updateddate = entity.Updateddate;
                record.Updatedby = entity.Updatedby;
                record.Customerid = entity.Customerid;
                record.Updateddate = entity.Updateddate;
                try
                {
                    db.SaveChanges();
                    IsUpdated = true;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return IsUpdated;
        }
    }
}
