﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
namespace Garments_ERP.Data.Admin
{
  public  class cl_Machinetypemaster
    {

        public List<M_MachineTypeMasterEntity> Get()
        {
            List<M_MachineTypeMasterEntity> list = new List<M_MachineTypeMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_MachineTypeMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_MachineTypeMasterEntity entity = new M_MachineTypeMasterEntity();
                    entity.Machine_TypeId = item.Machine_TypeId;
                    entity.Machine_TypeName = item.Machine_TypeName;
                    entity.Machine_TypeDescription = item.Machine_TypeDescription;
                    entity.companyid = item.Company_ID;
                    entity.branchid = item.BranchId;
                    list.Add(entity);
                }

            }
            return list;

        }

        public long Insert(M_MachineTypeMasterEntity entity)
        {
            long id = 0;

            M_MachineTypeMaster record = new M_MachineTypeMaster();
            record.Machine_TypeId = entity.Machine_TypeId;
            record.Machine_TypeName = entity.Machine_TypeName;
            record.Machine_TypeDescription = entity.Machine_TypeDescription;
            record.Company_ID = entity.companyid;
            record.BranchId = entity.branchid;
            //record.IsActive = true;
            //record.Createddate = DateTime.Now;
           // record.Createdby = entity.Createdby;
            record.IsActive = true;
            using (var db = new GarmentERPDBEntities())
            {
                db.M_MachineTypeMaster.Add(record);
                db.SaveChanges();
                id = record.Machine_TypeId;
            }


            return id;

        }

        public bool Update(M_MachineTypeMasterEntity entity)
        {
            bool isupdated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_MachineTypeMaster.Find(entity.Machine_TypeId);
                record.Machine_TypeId = entity.Machine_TypeId;
                record.Machine_TypeName = entity.Machine_TypeName;
                record.Machine_TypeDescription = entity.Machine_TypeDescription;
                record.Company_ID = entity.companyid;
                record.BranchId = entity.branchid;
                db.SaveChanges();
                isupdated = true;
            }

            return isupdated;

        }


        public bool Delete(long id)
        {
            bool isdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_MachineTypeMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                isdelete = true;
            }
            return isdelete;
        }

    }
}
