﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;

namespace Garments_ERP.Data.Admin
{
    public class cl_SegmentType
    {

        public List<M_SegmentTypeMasterEntity> get()
        {

            List<M_SegmentTypeMasterEntity> list = new List<M_SegmentTypeMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_SegmentTypeMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_SegmentTypeMasterEntity entity = new M_SegmentTypeMasterEntity();
                    entity.Id = item.Id;
                    entity.Segmenttype = item.Segmenttype;
                    entity.Segmenttypedesc = item.Segmenttypedesc;
                    entity.Createddate = item.Createddate;
                    entity.Createdby = item.Createdby;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    entity.IsActive = item.IsActive;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    list.Add(entity);
                }
            }
            return list;

        }
        public int Insert(M_SegmentTypeMasterEntity entity)
        {
            int id = 0;

            M_SegmentTypeMaster record = new M_SegmentTypeMaster();
            record.Id = entity.Id;
            record.Segmenttype = entity.Segmenttype;
            record.Segmenttypedesc = entity.Segmenttypedesc;
            record.Createddate = DateTime.Now;
            record.Createdby = entity.Createdby;
            //New Code Added by Rahul on 09-01-2020
            //record.Updateddate = entity.Updateddate;
            //record.Updatedby = entity.Updatedby;
            record.Company_ID = entity.Company_ID;
            record.BranchId = entity.Branch_ID;
            record.IsActive = true;
            using (var db = new GarmentERPDBEntities())
            {
                db.M_SegmentTypeMaster.Add(record);
                db.SaveChanges();
                id = record.Id;
            }

            return id;

        }

        public bool CheckSegmentTypeExistance(string Segmenttype, int? Id,int companyid,int branchid)
        {
            bool result = true;

            using (var db = new GarmentERPDBEntities())
            {
                if (Id == null)
                {
                    var itemcolor = db.M_SegmentTypeMaster.Where(x => x.Segmenttype == Segmenttype && x.IsActive == true && x.Company_ID==companyid && x.BranchId==branchid).FirstOrDefault();
                    if (itemcolor != null)
                        result = false;
                }
                else
                {
                    var itemcolor = db.M_SegmentTypeMaster.Where(x => x.Segmenttype == Segmenttype && x.Id != Id && x.IsActive == true && x.Company_ID == companyid && x.BranchId == branchid).FirstOrDefault();
                    if (itemcolor != null)
                        result = false;
                }


            }
            return result;
        }

        public bool Update(M_SegmentTypeMasterEntity entity)
        {
            bool isupdated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_SegmentTypeMaster.Find(entity.Id);
                record.Segmenttype = entity.Segmenttype;
                record.Segmenttypedesc = entity.Segmenttypedesc;

                //New Code Added by Rahul on 09-01-2020
                record.Updateddate = entity.Updateddate;
                record.Updatedby = entity.Updatedby;
                record.BranchId = entity.Branch_ID;
                record.Company_ID = entity.Company_ID;

                db.SaveChanges();
                isupdated = true;
            }
            return isupdated;
        }


        public bool Delete(int id)
        {
            bool isdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_SegmentTypeMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                isdelete = true;
            }
            return isdelete;
        }

    }
}
