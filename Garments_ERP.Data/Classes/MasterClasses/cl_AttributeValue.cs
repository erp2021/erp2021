﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Garments_ERP.Data.Classes.MasterClasses
{
   public class cl_AttributeValue
    {
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;
        public List<M_Attribute_Value_MasterEntity> get()
        {
            List<M_Attribute_Value_MasterEntity> list = new List<M_Attribute_Value_MasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_Attribute_Value_Master.ToList().Where(x => x.Is_Active == true);
                foreach (var item in records)
                {
                    M_Attribute_Value_MasterEntity entity = new M_Attribute_Value_MasterEntity();
                    var attributeid = Convert.ToString(item.Attribute_Value_ID);
                    long data = db.CRM_EnquiryStyleDetail.Where(x => x.Attribute_ID.Contains(attributeid)).Select(x => x.Id).FirstOrDefault();
                    if (data > 0)
                    {
                        entity.Status = true;
                    }
                    else
                    {
                        entity.Status = false;
                    }
                    entity.Attribute_Value_ID = item.Attribute_Value_ID;
                    entity.Attribute_ID = item.Attribute_ID;
                    M_Attribute_MasterEntity attrentity = new M_Attribute_MasterEntity();
                    if (item.M_Attribute_Master != null)
                    {
                        attrentity.Attribute_Name = item.M_Attribute_Master.Attribute_Name;
                        attrentity.Attribute_ID = item.M_Attribute_Master.Attribute_ID;
                        entity.M_Attribute_Master = attrentity;
                    }
                    entity.Attribute_Value = item.Attribute_Value;
                    entity.Attribute_Value_Desc = item.Attribute_Value_Desc;
                    M_UserEntity userentity = new M_UserEntity();
                    if (item.M_UserMaster != null)
                    {
                        userentity.Id = item.M_UserMaster.Id;
                        userentity.Username = item.M_UserMaster.Username;
                        entity.M_UserMaster = userentity;
                    }
                    entity.Created_By = item.Created_By;
                    entity.Created_Date = item.Created_Date;
                    entity.Modified_By = item.Modified_By;
                    //M_UserEntity userentity1 = new M_UserEntity();
                    //if (item.M_UserMaster1 != null)
                    //{
                    //    userentity.Id = item.M_UserMaster1.Id;
                    //    userentity.Username = item.M_UserMaster1.Username;
                    //    entity.M_UserMaster1 = userentity;
                    //}
                    entity.Modified_Date = item.Modified_Date;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    list.Add(entity);
                }
            }
            return list;
        }
        public M_Attribute_Value_MasterEntity GetById(int id)
        {
            M_Attribute_Value_MasterEntity entity = new M_Attribute_Value_MasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_Attribute_Value_Master.ToList().Where(x => x.Attribute_Value_ID == id && x.Is_Active == true);
                foreach (var item in records)
                {
                    var attributeid = Convert.ToString(item.Attribute_Value_ID);
                    long data = db.CRM_EnquiryStyleDetail.Where(x => x.Attribute_ID.Contains(attributeid)).Select(x => x.Id).FirstOrDefault();
                    if (data > 0)
                    {
                        entity.Status = true;
                    }
                    else
                    {
                        entity.Status = false;
                    }
                    entity.Attribute_Value_ID = item.Attribute_Value_ID;
                    entity.Attribute_ID = item.Attribute_ID;
                    entity.Attribute_Value = item.Attribute_Value;
                    entity.Attribute_Value_Desc = item.Attribute_Value_Desc;
                    entity.Created_By = item.Created_By;
                    entity.Created_Date = item.Created_Date;
                    entity.Modified_By = item.Modified_By;
                    entity.Modified_Date = item.Modified_Date;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                }
            }
            return entity;
        }
        public int Insert(M_Attribute_Value_MasterEntity entity)
        {
            int insertedid = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_Attribute_Value_Master record = new M_Attribute_Value_Master();
                record.Attribute_Value_ID = entity.Attribute_Value_ID;
                record.Attribute_ID = entity.Attribute_ID;
                record.Attribute_Value = entity.Attribute_Value;
                record.Attribute_Value_Desc = entity.Attribute_Value_Desc;
                record.Created_By = entity.Created_By;
                record.Created_Date = entity.Created_Date;

                //New Code Added by Rahul on 09-01-2020
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                record.Is_Active = entity.Is_Active;
                db.M_Attribute_Value_Master.Add(record);
                db.SaveChanges();
                insertedid = record.Attribute_Value_ID;
            }
            return insertedid;
        }
        public bool Update(M_Attribute_Value_MasterEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_Attribute_Value_Master.Where(x => x.Attribute_Value_ID == entity.Attribute_Value_ID).FirstOrDefault();
                record.Attribute_Value_ID = entity.Attribute_Value_ID;
                record.Attribute_ID = entity.Attribute_ID;
                record.Attribute_Value = entity.Attribute_Value;
                record.Attribute_Value_Desc = entity.Attribute_Value_Desc;
                record.Modified_By = entity.Modified_By;
                record.Modified_Date = entity.Modified_Date;
                //New Code Added by Rahul on 09-01-2020
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                record.Is_Active = entity.Is_Active;
                try
                {
                    db.SaveChanges();
                    IsUpdated = true;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return IsUpdated;
        }
        public bool Delete(int id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var attribute = db.M_Attribute_Value_Master.Where(x => x.Attribute_Value_ID == id).FirstOrDefault();
                attribute.Is_Active = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
                return IsDeleted;

            }
        }

        public object getAttributeValue(string MyJson, int type)
        {
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = type;
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = MyJson;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                
            }
            return dt;
        }

       
    }
}
