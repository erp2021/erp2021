﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_CustomerType
    {
        public List<M_CustomerTypeMasterEntity> get()
        {
            List<M_CustomerTypeMasterEntity> list = new List<M_CustomerTypeMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_CustomerTypeMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_CustomerTypeMasterEntity entity = new M_CustomerTypeMasterEntity();
                    entity.Id = item.Id;
                    entity.TypeName = item.TypeName;
                    entity.Typedesc = item.Typedesc;
                    list.Add(entity);
                }
            }
            return list;
        }
    }
}
