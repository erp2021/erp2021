﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Classes.MasterClasses
{
    public class cl_Scheme
    {
        public List<M_SchemeMaster_Entity> get(int comid, int branchid)
        {
            List<M_SchemeMaster_Entity> list = new List<M_SchemeMaster_Entity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_SchemeMaster.ToList().Where(x => x.Is_Active == true && x.Company_Id == comid && x.Branch_Id == branchid);

                foreach (var item in records)
                {
                    M_SchemeMaster_Entity entity = new M_SchemeMaster_Entity();
                    entity.Scheme_ID = item.Scheme_ID;
                    entity.Scheme_Name = item.Scheme_Name;
                    entity.Segment_Type_ID = item.Segment_Type_ID;
                    if (item.Segment_Type_ID > 0)
                    {
                        M_SegmentTypeMasterEntity segEnt = new M_SegmentTypeMasterEntity();
                        segEnt.Segmenttype = db.M_SegmentTypeMaster.Where(x => x.Id == item.Segment_Type_ID).Select(x => x.Segmenttype).FirstOrDefault();
                        entity.M_SegmentTypeMaster = segEnt;
                    }
                    entity.Style_ID = item.Style_ID;
                    if (item.Style_ID > 0)
                    {
                        M_StyleMasterEntity styleEnt = new M_StyleMasterEntity();
                        styleEnt.Stylename = db.M_StyleMaster.Where(x => x.Id == item.Style_ID).Select(x => x.Stylename).FirstOrDefault();
                        entity.M_StyleMaster = styleEnt;
                    }
                    entity.Item_ID = item.Item_ID;
                    if (item.Item_ID > 0)
                    {
                        M_ItemMasterEntity itemEnt = new M_ItemMasterEntity();
                        itemEnt.ItemName = db.M_ItemMaster.Where(x => x.Id == item.Item_ID).Select(x => x.ItemName).FirstOrDefault();
                        entity.M_ItemMaster = itemEnt;
                    }
                    entity.Discount_Percentage = item.Discount_Percentage;
                    if (item.Start_Date != null)
                    {
                        entity.Start_Date = Convert.ToDateTime(item.Start_Date).Date;
                        string stdate = Convert.ToDateTime(item.Start_Date).ToString("MM/dd/yyyy");
                        entity.st_date = stdate;
                        //entity.Start_Date = item.Start_Date;
                    }
                    if (item.End_Date != null)
                    {
                        entity.End_Date = Convert.ToDateTime(item.End_Date).Date;
                        string endate = Convert.ToDateTime(item.End_Date).ToString("MM/dd/yyyy");
                        entity.en_date = endate;
                        //entity.Start_Date = item.Start_Date;
                    }
                    //entity.Start_Date = item.Start_Date;
                    //entity.End_Date = item.End_Date;
                    entity.Created_By = item.Created_By;
                    entity.Updated_By = item.Updated_By;
                    entity.Created_Date = item.Created_Date;
                    entity.Updated_Date = item.Updated_Date;
                    list.Add(entity);
                }
            }
            return list;
        }
        public M_SchemeMaster_Entity getById(long id)
        {
            M_SchemeMaster_Entity entity = new M_SchemeMaster_Entity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_SchemeMaster.ToList().Where(x => x.Is_Active == true && x.Scheme_ID == id);

                foreach (var item in records)
                {
                    entity.Scheme_ID = item.Scheme_ID;
                    entity.Scheme_Name = item.Scheme_Name;
                    entity.Segment_Type_ID = item.Segment_Type_ID;
                    if (item.Segment_Type_ID > 0)
                    {
                        M_SegmentTypeMasterEntity segEnt = new M_SegmentTypeMasterEntity();
                        segEnt.Segmenttype = db.M_SegmentTypeMaster.Where(x => x.Id == item.Segment_Type_ID).Select(x => x.Segmenttype).FirstOrDefault();
                        entity.M_SegmentTypeMaster = segEnt;
                    }
                    entity.Style_ID = item.Style_ID;
                    if (item.Style_ID > 0)
                    {
                        M_StyleMasterEntity styleEnt = new M_StyleMasterEntity();
                        styleEnt.Stylename = db.M_StyleMaster.Where(x => x.Id == item.Style_ID).Select(x => x.Stylename).FirstOrDefault();
                        entity.M_StyleMaster = styleEnt;
                    }
                    entity.Item_ID = item.Item_ID;
                    if (item.Item_ID > 0)
                    {
                        M_ItemMasterEntity itemEnt = new M_ItemMasterEntity();
                        itemEnt.ItemName = db.M_ItemMaster.Where(x => x.Id == item.Item_ID).Select(x => x.ItemName).FirstOrDefault();
                        entity.M_ItemMaster = itemEnt;
                    }
                    entity.Discount_Percentage = item.Discount_Percentage;
                    if (item.Start_Date != null)
                    {
                        entity.Start_Date = Convert.ToDateTime(item.Start_Date).Date;
                        string stdate = Convert.ToDateTime(item.Start_Date).ToString("MM/dd/yyyy");
                        entity.st_date = stdate;
                        //entity.Start_Date = item.Start_Date;
                    }
                    if (item.End_Date != null)
                    {
                        entity.End_Date = Convert.ToDateTime(item.End_Date).Date;
                        string endate = Convert.ToDateTime(item.End_Date).ToString("MM/dd/yyyy");
                        entity.en_date = endate;
                        //entity.Start_Date = item.Start_Date;
                    }

                    //entity.Start_Date = item.Start_Date;
                    //entity.End_Date = item.End_Date;
                    entity.Created_By = item.Created_By;
                    entity.Updated_By = item.Updated_By;
                    entity.Created_Date = item.Created_Date;
                    entity.Updated_Date = item.Updated_Date;
                }
            }
            return entity;
        }
        public long InsertUpdate(M_SchemeMaster_Entity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_SchemeMaster item = new M_SchemeMaster();
                item.Scheme_ID = entity.Scheme_ID;
                item.Scheme_Name = entity.Scheme_Name;
                item.Segment_Type_ID = entity.Segment_Type_ID;
                item.Style_ID = entity.Style_ID;
                item.Item_ID = entity.Item_ID;
                item.Discount_Percentage = entity.Discount_Percentage;
                item.Start_Date = entity.Start_Date;
                item.End_Date = entity.End_Date;
                item.Company_Id = entity.Company_Id;
                item.Branch_Id = entity.Branch_Id;
                item.Is_Active = true;
                if (entity.Scheme_ID > 0)
                {
                    var record = db.M_SchemeMaster.Where(x => x.Scheme_ID == entity.Scheme_ID).FirstOrDefault();
                    record.Scheme_ID = entity.Scheme_ID;
                    record.Scheme_Name = entity.Scheme_Name;
                    record.Segment_Type_ID = entity.Segment_Type_ID;
                    record.Style_ID = entity.Style_ID;
                    record.Item_ID = entity.Item_ID;
                    record.Discount_Percentage = entity.Discount_Percentage;
                    record.Start_Date = entity.Start_Date;
                    record.End_Date = entity.End_Date;
                    record.Company_Id = entity.Company_Id;
                    record.Branch_Id = entity.Branch_Id;
                    record.Is_Active = true;
                    record.Updated_By = entity.Created_By;
                    record.Updated_Date = DateTime.Now; ;
                    db.SaveChanges();
                }
                else
                {
                    item.Created_By = entity.Created_By;
                    item.Created_Date = DateTime.Now;
                    db.M_SchemeMaster.Add(item);
                    db.SaveChanges();
                }

                id = item.Scheme_ID;
            }
            return id;
        }
        public bool Delete(int id)
        {
            bool isdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_SchemeMaster.Find(id);
                record.Is_Active = false;
                db.SaveChanges();
                isdelete = true;
            }
            return isdelete;
        }
        public bool IsExists(int compid, int branchid, long itemid, string st_date, string ed_date, long scheme_id)
        {
            bool isExist = false;
            M_SchemeMaster_Entity entity = new M_SchemeMaster_Entity();
            entity.Start_Date = Convert.ToDateTime(st_date);
            entity.End_Date = Convert.ToDateTime(ed_date);
            using (var db = new GarmentERPDBEntities())
            {
                string str = "select * from M_SchemeMaster where Company_Id=" + compid + " and Branch_Id=" + branchid + " and Item_ID=" + itemid + " and End_Date between '" + st_date + "' and '" + ed_date + "'";
                var records = db.M_SchemeMaster.SqlQuery(str).ToList();
                if (records != null)
                {
                    if (scheme_id > 0)
                    {
                        if (records.Count == 1)
                        {
                            isExist = false;
                        }
                        else
                        {
                            isExist = true;
                        }

                    }
                    else
                    {
                        if (records.Count > 0)
                        {
                            isExist = true;
                        }

                    }
                    //}

                }

            }
            return isExist;
        }
    }
}
