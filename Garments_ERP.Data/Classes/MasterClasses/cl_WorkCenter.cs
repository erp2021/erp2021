﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_WorkCenter
    {
        public List<M_WorkCenterMasterEntity> get()
        {
            List<M_WorkCenterMasterEntity> list = new List<M_WorkCenterMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_WorkCenterMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_WorkCenterMasterEntity entity = new M_WorkCenterMasterEntity();
                    entity.Id = item.Id;
                    entity.WorkCenterName = item.WorkCenterName;
                    entity.WorkCenterLocation = item.WorkCenterLocation;
                    entity.Comment = item.Comment;
                    entity.DepartmentId = item.DepartmentId;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                        deptentity.Id = item.M_DepartmentMaster.Id;
                        deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        deptentity.DepartmentNo = item.M_DepartmentMaster.DepartmentNo;
                        entity.M_DepartmentMaster = deptentity;
                    }
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.IsActive = item.IsActive;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    list.Add(entity);
                }
            }
            return list;
        }
        public M_WorkCenterMasterEntity GetById(int id)
        {
            M_WorkCenterMasterEntity entity = new M_WorkCenterMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_WorkCenterMaster.Where(x => x.Id == id && x.IsActive == true);
                foreach (var item in records)
                {
                    entity.Id = item.Id;
                    entity.WorkCenterName = item.WorkCenterName;
                    entity.WorkCenterLocation = item.WorkCenterLocation;
                    entity.Comment = item.Comment;
                    entity.DepartmentId = item.DepartmentId;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                        deptentity.Id = item.M_DepartmentMaster.Id;
                        deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        deptentity.DepartmentNo = item.M_DepartmentMaster.DepartmentNo;
                        entity.M_DepartmentMaster = deptentity;
                    }
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.IsActive = item.IsActive;
                    entity.Branch_ID = item.BranchId;
                    entity.Company_ID = item.Company_ID;
                }
            }
            return entity;
        }
        public int Insert(M_WorkCenterMasterEntity entity)
        {
            int insertedid = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_WorkCenterMaster record = new M_WorkCenterMaster();
                record.Id = entity.Id;
                record.WorkCenterName = entity.WorkCenterName;
                record.WorkCenterLocation = entity.WorkCenterLocation;
                record.Comment = entity.Comment;
                record.DepartmentId = entity.DepartmentId;
                record.CreatedBy = entity.CreatedBy;
                record.CreatedDate = entity.CreatedDate;

                //New Code Added by Rahul on 09-01-2020
                //record.UpdatedDate = entity.UpdatedDate;
                //record.UpdatedBy = entity.UpdatedBy;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;
                
                record.IsActive = entity.IsActive;
                db.M_WorkCenterMaster.Add(record);
                db.SaveChanges();
                insertedid = record.Id;
            }
            return insertedid;
        }
        public bool Update(M_WorkCenterMasterEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_WorkCenterMaster.Where(x => x.Id == entity.Id).FirstOrDefault();
                record.Id = entity.Id;
                record.WorkCenterName = entity.WorkCenterName;
                record.WorkCenterLocation = entity.WorkCenterLocation;
                record.Comment = entity.Comment;
                record.DepartmentId = entity.DepartmentId;              
                record.UpdatedDate = entity.UpdatedDate;
                record.UpdatedBy = entity.UpdatedBy;

                //New Code Added by Rahul on 09-01-2020
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                record.IsActive = entity.IsActive;
                try
                {
                    db.SaveChanges();
                    IsUpdated = true;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return IsUpdated;
        }
        public bool Delete(int id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var unit = db.M_WorkCenterMaster.Where(x => x.Id == id).FirstOrDefault();
                unit.IsActive = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
                return IsDeleted;

            }
        }
        public bool CheckWorkCenterExistance(string WorkCenter, int? Id,int companyid,int branchid)
        {
            bool result = true;

            using (var db = new GarmentERPDBEntities())
            {
                if (Id == null)
                {
                    var workcenter = db.M_WorkCenterMaster.Where(x => x.WorkCenterName == WorkCenter && x.IsActive == true && x.Company_ID==companyid && x.BranchId==branchid).FirstOrDefault();
                    if (workcenter != null)
                        result = false;
                }
                else
                {
                    var workcenter = db.M_WorkCenterMaster.Where(x => x.WorkCenterName == WorkCenter && x.Id != Id && x.IsActive == true && x.Company_ID == companyid && x.BranchId == branchid).FirstOrDefault();
                    if (workcenter != null)
                        result = false;
                }


            }
            return result;
        }
    }
}
