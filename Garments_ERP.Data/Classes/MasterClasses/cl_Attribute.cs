﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Garments_ERP.Data.Classes.MasterClasses
{
    public class cl_Attribute
    {
        public List<M_Attribute_MasterEntity> get()
        {
            List<M_Attribute_MasterEntity> list = new List<M_Attribute_MasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_Attribute_Master.ToList().Where(x => x.Is_Active == true).OrderByDescending(x=>x.Attribute_ID);
                foreach (var item in records)
                {
                    M_Attribute_MasterEntity entity = new M_Attribute_MasterEntity();
                    var attributeid = Convert.ToString(item.Attribute_ID);
                    long data = db.CRM_EnquiryStyleDetail.Where(x => x.Attribute_ID.Contains(attributeid)).Select(x=>x.Id).FirstOrDefault();
                    if (data >0)
                    {
                        entity.Status = true;
                    }
                    else
                    {
                        entity.Status = false;
                    }
                    entity.Attribute_ID = item.Attribute_ID;
                    entity.Attribute_Name = item.Attribute_Name;
                    entity.Attribute_Desc = item.Attribute_Desc;
                    M_UserEntity userentity = new M_UserEntity();
                    if (item.M_UserMaster != null)
                    {
                        userentity.Id = item.M_UserMaster.Id;
                        userentity.Username = item.M_UserMaster.Username;
                        entity.M_UserMaster = userentity;
                    }
                    entity.Created_By = item.Created_By;
                    entity.Created_Date = item.Created_Date;
                    entity.Modified_By = item.Modified_By;
                    M_UserEntity userentity1 = new M_UserEntity();
                    if (item.M_UserMaster1 != null)
                    {
                        userentity.Id = item.M_UserMaster1.Id;
                        userentity.Username = item.M_UserMaster1.Username;
                        entity.M_UserMaster1 = userentity;
                    }
                    entity.Modified_Date = item.Modified_Date;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    list.Add(entity);
                }
            }
            return list;
        }
        public M_Attribute_MasterEntity GetById(int id)
        {
            M_Attribute_MasterEntity entity = new M_Attribute_MasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_Attribute_Master.ToList().Where(x => x.Attribute_ID == id && x.Is_Active == true);
                foreach (var item in records)
                {
                    var attributeid = Convert.ToString(item.Attribute_ID);
                    long data = db.CRM_EnquiryStyleDetail.Where(x => x.Attribute_ID.Contains(attributeid)).Select(x => x.Id).FirstOrDefault();
                    if (data > 0)
                    {
                        entity.Status = true;
                    }
                    else
                    {
                        entity.Status = false;
                    }
                    entity.Attribute_ID = item.Attribute_ID;
                    entity.Attribute_Name = item.Attribute_Name;
                    entity.Attribute_Desc = item.Attribute_Desc;
                    entity.Created_By = item.Created_By;
                    entity.Created_Date = item.Created_Date;
                    entity.Modified_By = item.Modified_By;
                    entity.Modified_Date = item.Modified_Date;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                }
            }
            return entity;
        }
        public int Insert(M_Attribute_MasterEntity entity)
        {
            int insertedid = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_Attribute_Master record = new M_Attribute_Master();
                record.Attribute_ID = entity.Attribute_ID;
                record.Attribute_Name = entity.Attribute_Name;
                record.Attribute_Desc = entity.Attribute_Desc;
                record.Created_By = entity.Created_By;
                record.Created_Date = entity.Created_Date;

                //New Code Added by Rahul on 09-01-2020
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                record.Is_Active = entity.Is_Active;
                db.M_Attribute_Master.Add(record);
                db.SaveChanges();
                insertedid = record.Attribute_ID;
            }
            return insertedid;
        }
        public bool Update(M_Attribute_MasterEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_Attribute_Master.Where(x => x.Attribute_ID == entity.Attribute_ID).FirstOrDefault();
                record.Attribute_ID = entity.Attribute_ID;
                record.Attribute_Name = entity.Attribute_Name;
                record.Attribute_Desc = entity.Attribute_Desc;
                //New Code Added by Rahul on 09-01-2020
                record.Modified_By = entity.Modified_By;
                record.Modified_Date = entity.Modified_Date;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;
                record.Is_Active = entity.Is_Active;
                try
                {
                    db.SaveChanges();
                    IsUpdated = true;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return IsUpdated;
        }
        public bool Delete(int id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var attribute = db.M_Attribute_Master.Where(x => x.Attribute_ID == id).FirstOrDefault();
                attribute.Is_Active = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
                return IsDeleted;

            }
        }
    }
}
