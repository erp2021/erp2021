﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_Unit
    {
        public List<M_UnitMasterEntity> get()
        {
            List<M_UnitMasterEntity> list = new List<M_UnitMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_UnitMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_UnitMasterEntity entity = new M_UnitMasterEntity();
                    entity.Id = item.Id;
                    entity.ItemUnit = item.ItemUnit;
                    entity.Unitdesc = item.Unitdesc;
                    entity.Comment = item.Comment;
                    entity.DepartmentId = item.DepartmentId;

                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                        deptentity.Id = item.M_DepartmentMaster.Id;
                        deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        deptentity.DepartmentNo = item.M_DepartmentMaster.DepartmentNo;
                        entity.M_DepartmentMaster = deptentity;
                    }
                    entity.CreatedBy = item.CreatedBy;
                    entity.Createddate = item.Createddate;
                    entity.Updatedby = item.Updatedby;
                    entity.Updateddate = item.Updateddate;
                    entity.IsActive = item.IsActive;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    list.Add(entity);
                }
            }
            return list;
        }
        public M_UnitMasterEntity GetById(int id)
        {
            M_UnitMasterEntity entity = new M_UnitMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_UnitMaster.Where(x => x.Id == id && x.IsActive == true);
                foreach (var item in records)
                {
                    entity.Id = item.Id;
                    entity.ItemUnit = item.ItemUnit;
                    entity.Unitdesc = item.Unitdesc;
                    entity.Comment = item.Comment;
                    entity.DepartmentId = item.DepartmentId;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                        deptentity.Id = item.M_DepartmentMaster.Id;
                        deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        deptentity.DepartmentNo = item.M_DepartmentMaster.DepartmentNo;
                        entity.M_DepartmentMaster = deptentity;
                    }
                    entity.CreatedBy = item.CreatedBy;
                    entity.Createddate = item.Createddate;
                    entity.Updatedby = item.Updatedby;
                    entity.Updateddate = item.Updateddate;
                    entity.IsActive = item.IsActive;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                }
            }
            return entity;
        }
        public int Insert(M_UnitMasterEntity entity)
        {
            int insertedid = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_UnitMaster record = new M_UnitMaster();
                record.Id = entity.Id;
                record.ItemUnit = entity.ItemUnit;
                record.Unitdesc = entity.Unitdesc;
                record.Comment = entity.Comment;
                record.DepartmentId = entity.DepartmentId;
                record.CreatedBy = entity.CreatedBy;
                record.Createddate = entity.Createddate;
                //New Code Added by Rahul on 09-01-2020
                //record.Updateddate = entity.Updateddate;
                //record.Updatedby = entity.Updatedby;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                record.IsActive = entity.IsActive;
                db.M_UnitMaster.Add(record);
                db.SaveChanges();
                insertedid = record.Id;
            }
            return insertedid;
        }
        public bool Update(M_UnitMasterEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_UnitMaster.Where(x => x.Id == entity.Id).FirstOrDefault();
                record.Id = entity.Id;
                record.ItemUnit = entity.ItemUnit;
                record.Unitdesc = entity.Unitdesc;
                record.Comment = entity.Comment;
                record.DepartmentId = entity.DepartmentId;             
                record.Updateddate = entity.Updateddate;
                record.Updatedby = entity.Updatedby;

                //New Code Added by Rahul on 09-01-2020
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                record.IsActive = entity.IsActive;
                try
                {
                    db.SaveChanges();
                    IsUpdated = true;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return IsUpdated;
        }
        public bool Delete(int id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var unit = db.M_UnitMaster.Where(x => x.Id == id).FirstOrDefault();
                unit.IsActive = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
                return IsDeleted;

            }
        }
        public bool CheckUnitExistance(string ItemUnit, int? Id)
        {
            bool result = true;

            using (var db = new GarmentERPDBEntities())
            {
                if (Id == null)
                {
                    var workcenter = db.M_UnitMaster.Where(x => x.ItemUnit == ItemUnit && x.IsActive == true).FirstOrDefault();
                    if (workcenter != null)
                        result = false;
                }
                else
                {
                    var workcenter = db.M_UnitMaster.Where(x => x.ItemUnit == ItemUnit && x.Id != Id && x.IsActive == true).FirstOrDefault();
                    if (workcenter != null)
                        result = false;
                }


            }
            return result;
        }
    }
}
