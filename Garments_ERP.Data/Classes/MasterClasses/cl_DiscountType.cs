﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;

namespace Garments_ERP.Data.Admin
{
    public class cl_DiscountType
    {
        public long Insert(M_DiscountTypeMasterEntity entity)
        {
            long insertedid = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_DiscountTypeMaster record = new M_DiscountTypeMaster();
                record.Id = entity.Id;
                record.DiscountTypeName = entity.DiscountTypeName;
                record.ShortName = entity.ShortName;
                record.DiscountTypeDesc = entity.DiscountTypeDesc;

                record.CreatedDate = entity.CreatedDate;
                record.CreatedBy = entity.CreatedBy;
                //New Code Added by Rahul on 09-01-2020
                //record.UpdatedDate = entity.UpdatedDate;
                //record.UpdatedBy = entity.UpdatedBy;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;
                record.IsActive = entity.IsActive;
                db.M_DiscountTypeMaster.Add(record);
                db.SaveChanges();
                insertedid = record.Id;
            }
            return insertedid;
        }

        public List<M_DiscountTypeMasterEntity> get()
        {

            List<M_DiscountTypeMasterEntity> list = new List<M_DiscountTypeMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_DiscountTypeMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_DiscountTypeMasterEntity entity = new M_DiscountTypeMasterEntity();
                    entity.Id = item.Id;
                    entity.DiscountTypeName = item.DiscountTypeName;
                    entity.ShortName = item.ShortName;
                    entity.DiscountTypeDesc = item.DiscountTypeDesc;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    list.Add(entity);
                }
            }
            return list;

        }

        public bool Update(M_DiscountTypeMasterEntity entity)
        {
            bool isupdated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_DiscountTypeMaster.Find(entity.Id);
                record.DiscountTypeName = entity.DiscountTypeName;
                record.ShortName = entity.ShortName;
                record.DiscountTypeDesc = entity.DiscountTypeDesc;

                //New Code Added by Rahul on 09-01-2020
                record.UpdatedDate = entity.UpdatedDate;
                record.UpdatedBy = entity.UpdatedBy;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                db.SaveChanges();
                isupdated = true;
            }

            return isupdated;

        }
        public bool Delete(int id)
        {
            bool isdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_DiscountTypeMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                isdelete = true;
            }
            return isdelete;
        }
        public bool CheckDiscountTypeNameExistance(string DiscountType, int? Id)
        {
            bool result = true;

            using (var db = new GarmentERPDBEntities())
            {
                if (Id == null)
                {
                    var discountname = db.M_DiscountTypeMaster.Where(x => x.DiscountTypeName == DiscountType && x.IsActive == true).FirstOrDefault();
                    if (discountname != null)
                        result = false;
                }
                else
                {
                    var discountname = db.M_DiscountTypeMaster.Where(x => x.DiscountTypeName == DiscountType && x.Id != Id && x.IsActive == true).FirstOrDefault();
                    if (discountname != null)
                        result = false;
                }


            }
            return result;
        }
    }
}
