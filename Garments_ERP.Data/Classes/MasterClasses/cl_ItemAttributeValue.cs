﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Garments_ERP.Data.Classes.MasterClasses
{
    public class cl_ItemAttributeValue
    {
        public List<M_Item_Attribute_MasterEntity> get()
        {
            List<M_Item_Attribute_MasterEntity> list = new List<M_Item_Attribute_MasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_Item_Attribute_Master.ToList().Where(x => x.Is_Active == true);
                foreach (var item in records)
                {
                    M_Item_Attribute_MasterEntity entity = new M_Item_Attribute_MasterEntity();
                    entity.Item_Attribute_ID = item.Item_Attribute_ID;
                    entity.Attribute_Value_ID = item.Attribute_Value_ID;
                    entity.Attribute_ID = item.Attribute_ID;
                    entity.Item_ID = item.Item_ID;
                    M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                    if (item.M_ItemMaster != null)
                    {
                        itementity.Id = item.M_ItemMaster.Id;
                        itementity.ItemName = item.M_ItemMaster.ItemName;
                        entity.M_ItemMaster = itementity;
                    }
                    //  entity.Attribute_Value_Desc = item.Attribute_Value_Desc;
                    //M_UserEntity userentity = new M_UserEntity();
                    //if (item.M_UserMaster != null)
                    //{
                    //    userentity.Id = item.M_UserMaster.Id;
                    //    userentity.Username = item.M_UserMaster.Username;
                    //    entity.M_UserMaster = userentity;
                    //}
                    entity.Created_By = item.Created_By;
                    entity.Created_Date = item.Created_Date;
                    entity.Modified_By = item.Modified_By;
                    //M_UserEntity userentity1 = new M_UserEntity();
                    //if (item.M_UserMaster1 != null)
                    //{
                    //    userentity.Id = item.M_UserMaster1.Id;
                    //    userentity.Username = item.M_UserMaster1.Username;
                    //    entity.M_UserMaster1 = userentity;
                    //}
                    entity.Modified_Date = item.Modified_Date;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    list.Add(entity);
                }
            }
            return list;
        }
        public M_Item_Attribute_MasterEntity GetById(int id)
        {
            M_Item_Attribute_MasterEntity entity = new M_Item_Attribute_MasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_Item_Attribute_Master.ToList().Where(x => x.Item_Attribute_ID == id && x.Is_Active == true);
                foreach (var item in records)
                {
                    entity.Item_Attribute_ID = item.Item_Attribute_ID;
                    entity.Attribute_Value_ID = item.Attribute_Value_ID;
                    entity.Attribute_ID = item.Attribute_ID;
                    entity.Item_ID = item.Item_ID; entity.Created_By = item.Created_By;
                    entity.Created_Date = item.Created_Date;
                    entity.Modified_By = item.Modified_By;
                    entity.Modified_Date = item.Modified_Date;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                }
            }
            return entity;
        }
        public int Insert(M_Item_Attribute_MasterEntity entity)
        {
            int insertedid = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_Item_Attribute_Master record = new M_Item_Attribute_Master();
                record.Item_Attribute_ID = entity.Item_Attribute_ID;
                record.Attribute_Value_ID = entity.Attribute_Value_ID;
                record.Attribute_ID = entity.Attribute_ID;
                record.Item_ID = entity.Item_ID;
                record.Created_By = entity.Created_By;
                record.Created_Date = entity.Created_Date;
                record.Is_Active = entity.Is_Active;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.BranchId;
                db.M_Item_Attribute_Master.Add(record);
                db.SaveChanges();
                insertedid = record.Item_Attribute_ID;
            }
            return insertedid;
        }
        public bool Update(M_Item_Attribute_MasterEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_Item_Attribute_Master.Where(x => x.Item_Attribute_ID == entity.Item_Attribute_ID).FirstOrDefault();
                record.Item_Attribute_ID = entity.Item_Attribute_ID;
                record.Attribute_Value_ID = entity.Attribute_Value_ID;
                record.Attribute_ID = entity.Attribute_ID;
                record.Item_ID = entity.Item_ID;
                record.Modified_By = entity.Modified_By;
                record.Modified_Date = entity.Modified_Date;
                record.Is_Active = entity.Is_Active; try
                {
                    db.SaveChanges();
                    IsUpdated = true;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return IsUpdated;
        }
        public bool Delete(int id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var attribute = db.M_Item_Attribute_Master.Where(x => x.Item_Attribute_ID == id).FirstOrDefault();
                attribute.Is_Active = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
                return IsDeleted;

            }
        }
    }
}
