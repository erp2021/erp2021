﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
namespace Garments_ERP.Data.Admin
{
    public class cl_User
    {

        public int Insert(M_UserEntity entity)
        {
            int id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_UserMaster record = new M_UserMaster();
                record.Id = entity.Id;
                record.Username = entity.Username;
                record.Password = entity.Password;
                record.Email = entity.Email;
                record.Createdby = entity.Createdby;
                record.Createddate = DateTime.Now;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.BranchId;
                record.IsActive = true;
                db.M_UserMaster.Add(record);
                db.SaveChanges();
                id = record.Id;
            }

            return id;
        }

        public int SaveUser(M_UserEntity entity)
        {
            int id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_UserMaster record = new M_UserMaster();
                record.Id = entity.Id;
                record.Username = entity.Username;
                record.Password = entity.Password;
                record.Email = entity.Email;
                record.IsActive = true;
                record.Createdby = entity.Createdby;
                record.Createddate = entity.Createddate;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.BranchId;
                record.APassword = entity.APassword;
                db.M_UserMaster.Add(record);
                db.SaveChanges();
                id = record.Id;
                if (id > 0)
                {
                    M_User_Role UR = new M_User_Role();
                    UR.roleId = Convert.ToInt64(entity.RoleID);
                    UR.UserId = Convert.ToInt64(id);
                    UR.DepartmentId = Convert.ToInt64(entity.DeptID);
                    UR.CompanyId = Convert.ToInt32(entity.Company_ID);
                    UR.BranchId = Convert.ToInt32(entity.BranchId);
                    UR.CreatedOn = DateTime.Now;
                    UR.CreatedBy = entity.Createdby;
                    UR.IsActive = true;
                    db.M_User_Role.Add(UR);
                    db.SaveChanges();

                    M_User_Department UD = new M_User_Department();
                    UD.DepartmentId = Convert.ToInt64(entity.DeptID);
                    UD.UserId = Convert.ToInt64(id);
                    UD.RoleId = Convert.ToInt64(entity.RoleID);
                    UD.CompanyId = Convert.ToInt64(entity.Company_ID);
                    UD.BranchId = Convert.ToInt64(entity.BranchId);
                    UD.CreatedOn = DateTime.Now;
                    UD.CreatedBy = entity.Createdby;
                    UD.IsActive = true;
                    db.M_User_Department.Add(UD);
                    db.SaveChanges();
                }

            }
            return id;
        }


        public bool UpdateUser(M_UserEntity entity)
        {
            bool isupdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_UserMaster.Find(entity.Id);
                record.Username = entity.Username;
                record.Password = entity.Password;
                record.Email = entity.Email;
                //New Code Added by Rahul on 09-01-2020
                record.Updatedby = entity.Updatedby;
                record.Updateddate = DateTime.Now;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.BranchId;

                db.SaveChanges();
                isupdated = true;
            }
            return isupdated;
        }

        public List<M_UserEntity> get()
        {
            List<M_UserEntity> list = new List<M_UserEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_UserMaster.ToList().Where(x => x.IsActive == true);
                foreach (var record in records)

                {
                    M_UserEntity entity = new M_UserEntity();
                    entity.Id = record.Id;
                    entity.Username = record.Username;
                    entity.Email = record.Email;
                    list.Add(entity);
                }
            }
            return list;
        }

        public M_UserEntity GetByid(int id)
        {
            M_UserEntity userentity = new M_UserEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_UserMaster.Find(id);
                if (record != null)
                {
                    userentity.Id = record.Id;
                    userentity.Email = record.Email;
                    userentity.Username = record.Username;

                    userentity.Password = record.Password;


                }
            }

            return userentity;
        }


        public List<M_UserTypeEntity> getUsertype()
        {
            List<M_UserTypeEntity> list = new List<M_UserTypeEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_UserTypeMaster.ToList().Where(x => x.IsActive == true);
                foreach (var record in records)
                {
                    M_UserTypeEntity entity = new M_UserTypeEntity();
                    entity.Id = record.Id;
                    entity.UserType = record.UserType;

                    list.Add(entity);
                }
            }
            return list;
        }
        public M_UserEntity Authenticateuser(M_UserEntity entity)
        {
            M_UserEntity user = new M_UserEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_UserMaster.Where(x => x.Username == entity.Username && x.Password == entity.Password && x.IsActive == true).FirstOrDefault();
                if (record != null)
                {

                    user.Id = record.Id;
                    user.Username = record.Username;
                    user.Email = record.Email;
                    user.Company_ID = record.Company_ID;
                    user.BranchId = record.BranchId;
                    var RoleID = db.M_User_Role.Where(x => x.UserId == record.Id).Select(x => x.roleId).FirstOrDefault();
                    user.RoleID = RoleID != null ? Convert.ToInt32(RoleID) : 0;
                    var DeptID = db.M_User_Role.Where(x => x.UserId == record.Id).Select(x => x.DepartmentId).FirstOrDefault();
                    user.DeptID = DeptID != null ? Convert.ToInt32(DeptID) : 0;

                    EmployeeEntity ent = new EmployeeEntity();
                    if (RoleID > 1)
                    {
                        var data = db.Employees.Where(x => x.UserId == record.Id && x.IsActive == true).FirstOrDefault();
                        ent.Name = data!=null? data.Name:"";
                    }
                    else
                    {
                        ent.Name ="Admin";
                    }

                    user.empentity = ent;


                }

            }

            return user;
        }

        public bool DeleteUser(int id)
        {
            var deleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_UserMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                deleted = true;
            }
            return deleted;
        }

        public M_UserEntity IsEmailValid(string email)
        {
            M_UserEntity model = new M_UserEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var validEmail = db.M_UserMaster.Where(x => x.Email == email).FirstOrDefault();
                if (validEmail != null)
                {


                    model.UserId = validEmail.Id.ToString();
                    model.Email = validEmail.Email;
                    model.HashToken = RandomString(10);
                    validEmail.LinkToken = model.HashToken;
                    db.SaveChanges();
                }
            }
            return model;
        }

        public static string RandomString(int length)
        {
            string alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string small_alphabets = "abcdefghijklmnopqrstuvwxyz";
            string numbers = "1234567890";

            string characters = numbers;

            characters += alphabets + small_alphabets + numbers;

            string otp = string.Empty;
            for (int i = 0; i < length; i++)
            {
                string character = string.Empty;
                do
                {
                    int index = new Random().Next(0, characters.Length);
                    character = characters.ToCharArray()[index].ToString();
                } while (otp.IndexOf(character) != -1);
                otp += character;
            }
            return otp;
        }

        public bool ValidateResetPasswordLink(M_UserEntity model)
        {
            var IsValid = false;
            using (var db = new GarmentERPDBEntities())
            {
                int uid = Convert.ToInt32(model.UserId);
                var IsValidLink = db.M_UserMaster.Where(x => x.Id == uid && x.LinkToken == model.HashToken).FirstOrDefault();
                if (IsValidLink != null)
                {
                    IsValid = true;
                }
                return IsValid;
            }
        }


        public int ChangePassword(M_UserEntity model)
        {
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    //var PasswordDetails = db.UserMasters.Where(x => x.EmployeeId == model.EmployeeId).FirstOrDefault();
                    int uid = Convert.ToInt32(model.UserId);
                    var PasswordDetails = db.M_UserMaster.Where(x => x.Id == uid).FirstOrDefault();
                    PasswordDetails.Password = model.Password;
                    PasswordDetails.LinkToken = null;
                    db.SaveChanges();
                    return PasswordDetails.Id;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_ModuleEntity> GetModules()
        {
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ModuleMaster.ToList();
                List<M_ModuleEntity> list = new List<M_ModuleEntity>();
                foreach (var item in records)
                {
                    M_ModuleEntity model = new M_ModuleEntity();
                    model.ModuleId = item.ModuleId;
                    model.ModuleName = item.ModuleName;

                    list.Add(model);
                }
                return list;
            }
        }


        public List<M_UserPermissionsEntity> getbyUserid(int userid)
        {
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_UserPermissions.Where(x => x.UserId == userid).ToList();
                var user = db.M_UserMaster.Find(userid);
                List<M_UserPermissionsEntity> list = MAP(records, user);
                return list;
            }
        }


        public int Insert(List<M_UserPermissionsEntity> list)
        {
            using (var db = new GarmentERPDBEntities())
            {
                int i = 0;
                if (list.Count > 0)
                {
                    List<M_UserPermissions> UsersList = new List<M_UserPermissions>();
                    foreach (var item in list)
                    {
                        M_UserPermissions model = new M_UserPermissions();
                        //  model.CompanyId = item.CompanyId;
                        model.Id = item.Id;
                        model.IsCreate = item.IsCreate;
                        model.IsDelete = item.IsDelete;
                        model.IsEdit = item.IsEdit;
                        model.IsRead = item.IsRead;
                        model.ModuleId = item.ModuleId;
                        model.UserId = item.UserId;
                        UsersList.Add(model);
                    }

                    db.M_UserPermissions.AddRange(UsersList);
                    db.SaveChanges();
                    i++;
                }

                return i;
            }
        }

        public int Update(List<M_UserPermissionsEntity> list)
        {
            using (var db = new GarmentERPDBEntities())
            {
                int i = 0;
                if (list.Count > 0)
                {
                    List<M_UserPermissions> UsersList = new List<M_UserPermissions>();
                    foreach (var item in list)
                    {
                        M_UserPermissions model = new M_UserPermissions();
                        //model.CompanyId = item.CompanyId;
                        model.Id = item.Id;
                        model.IsCreate = item.IsCreate;
                        model.IsDelete = item.IsDelete;
                        model.IsEdit = item.IsEdit;
                        model.IsRead = item.IsRead;
                        model.ModuleId = item.ModuleId;
                        model.UserId = item.UserId;
                        UsersList.Add(model);
                    }
                    int id = (int)list[0].UserId;
                    db.M_UserPermissions.RemoveRange(db.M_UserPermissions.Where(x => x.UserId == id));
                    db.SaveChanges();

                    db.M_UserPermissions.AddRange(UsersList);
                    db.SaveChanges();
                    i++;
                }

                return i;
            }
        }

        public List<M_UserPermissionsEntity> MAP(List<M_UserPermissions> UserPermissionList, M_UserMaster User)
        {
            using (var db = new GarmentERPDBEntities())
            {
                List<M_UserPermissionsEntity> list = new List<M_UserPermissionsEntity>();
                var Modules = GetModules();
                var UsersPermissions = UserPermissionList.AsEnumerable();
                foreach (var Module in Modules)
                {
                    M_UserPermissionsEntity model = new M_UserPermissionsEntity();
                    var item = UsersPermissions.Where(x => x.ModuleId == Module.ModuleId).FirstOrDefault();
                    if (item != null)
                    {
                        // model.CompanyId = item.CompanyId;
                        model.Id = item.Id;
                        model.IsCreate = item.IsCreate;
                        model.IsDelete = item.IsDelete;
                        model.IsEdit = item.IsEdit;
                        model.IsRead = item.IsRead;
                        model.ModuleId = Module.ModuleId;
                        model.UserId = item.UserId;
                        M_ModuleEntity module = new M_ModuleEntity();
                        if (item.M_ModuleMaster != null)
                        {
                            module.ModuleId = item.M_ModuleMaster.ModuleId;
                            module.ModuleName = item.M_ModuleMaster.ModuleName;
                        }
                        model.M_Moduleentity = module;

                    }
                    else
                    {
                        // model.CompanyId = User.CompanyID;
                        model.Id = 0;
                        model.IsCreate = false;
                        model.IsDelete = false;
                        model.IsEdit = false;
                        model.IsRead = false;
                        model.ModuleId = Module.ModuleId;
                        model.UserId = User.Id;
                        model.M_Moduleentity = Module;
                    }
                    M_UserEntity Users = new M_UserEntity();
                    if (User != null)
                    {
                        Users.Id = User.Id;
                        Users.Username = User.Username;
                        // Users.EmployeeName = User.EmployeeName;
                    }
                    model.M_Userentity = Users;

                    list.Add(model);
                }
                return list;
            }
        }



    }
}
