﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Entity;
using Garments_ERP.Data.Data;
namespace Garments_ERP.Data.Admin
{
 public   class cl_StyleSizeDetail
    {
   public  long InsertQuotationSizeDetail(CRM_QuotationStyleSizeDetailEntity entity)
     {
        long insertid = 0;
        try
         {
            using(var db=new GarmentERPDBEntities())
            {
                CRM_QuotationStyleSizeDetail item = new CRM_QuotationStyleSizeDetail();
                item.Id = entity.Id;
                item.Quotationstyleid = entity.Quotationstyleid;
                item.Sizepcs = entity.Sizepcs;
                item.Sizeper = entity.Sizeper;
                item.Sizetype = entity.Sizetype;
                item.Createdby = entity.Createdby;
                item.Createddate = entity.Createddate;
                item.Updateddate = entity.Updateddate;
                item.Updatedby = entity.Updatedby;
                db.CRM_QuotationStyleSizeDetail.Add(item);
                db.SaveChanges();
                insertid = item.Id;

            }
         }
       catch(Exception ex)
         {

         }

         return insertid;
     }
    public long InsertEnquirySizeDetail(CRM_EnquiryStyleSizeDetailEntity entity)
   {
       long insertid = 0;
       try
       {
           using (var db = new GarmentERPDBEntities())
           {
               CRM_EnquiryStyleSizeDetail item = new CRM_EnquiryStyleSizeDetail();
               item.Id = entity.Id;
               item.Enquirystyleid = entity.Enquirystyleid;
               item.Sizepcs = entity.Sizepcs;
               item.SIzeper = entity.SIzeper;
               item.Sizetype = entity.Sizetype;
               item.CreatedBy = entity.CreatedBy;
               item.Createddate = entity.Createddate;
               item.Updateddate = entity.Updateddate;
               // = entity.Updatedby;
               db.CRM_EnquiryStyleSizeDetail.Add(item);
               db.SaveChanges();
               insertid = item.Id;

           }
       }
        catch(Exception ex)
       {

       }
       return insertid;
   }
 }
}
