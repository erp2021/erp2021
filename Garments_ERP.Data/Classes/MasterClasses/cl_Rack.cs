﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_Rack
    {

        public long SaveRackAllocation(string RackStr, long WarehouseId,long InwardId, int inventoryMode, int Userid,int Company_ID,int BranchId)
        {
            try
            {
                long id = 0;
                using (var db = new GarmentERPDBEntities())
                {
                    string[] rackdata = RackStr.Split('#');
                    foreach(var data in rackdata)
                    {
                        if(data!="")
                        {
                            string[] Rackitem = data.Split('~');
                            M_RackAllocation rackent = new M_RackAllocation();
                            rackent.warehouseid = WarehouseId;
                            rackent.InventoryMode = inventoryMode;
                            rackent.InwardId = InwardId;
                            rackent.ItemId = Convert.ToInt64(Rackitem[0]);
                            rackent.Item_Attribute_Id = Convert.ToInt32(Rackitem[1]);
                            rackent.BatchLot = Convert.ToString(Rackitem[2]);
                            rackent.BatchLotQty = Convert.ToDecimal(Rackitem[3]);
                            rackent.RackId = Convert.ToString(Rackitem[4]);
                            rackent.RackQty = Convert.ToDecimal(Rackitem[5]);
                            rackent.IsActive = true;
                            rackent.Createdate = DateTime.Now;
                            rackent.Createdby = Userid;
                            rackent.Company_ID = Company_ID;
                            rackent.BranchId = BranchId;
                            db.M_RackAllocation.Add(rackent);
                            db.SaveChanges();
                            id = rackent.Id;
                        }
                    }
                }
                return id;
            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<M_RackMasterEntity> get()
        {
            List<M_RackMasterEntity> list = new List<M_RackMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_RackMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_RackMasterEntity entity = new M_RackMasterEntity();
                    entity.Id = item.Id;
                    entity.Rack = item.Rack;
                    entity.Rackdesc = item.Rackdesc;
                    entity.Comment = item.Comment;
                    entity.DepartmentId = item.DepartmentId;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                        deptentity.Id = item.M_DepartmentMaster.Id;
                        deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        deptentity.DepartmentNo = item.M_DepartmentMaster.DepartmentNo;
                        entity.M_DepartmentMaster = deptentity;
                    }
                    entity.WarehouseId = item.WarehouseId;
                    if (item.WarehouseId > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseId).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WAREHOUSE = locentity;
                        }
                    }
                    
                    entity.Createdate = item.Createdate;
                    entity.Createdby = item.Createdby;
                    entity.Updatedate = item.Updatedate;
                    entity.Updatedby = item.Updatedby;
                    entity.IsActive = item.IsActive;
                    entity.Comment = item.Comment;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    list.Add(entity);
                }
            }
            return list;
        }
        public M_RackMasterEntity GetById(int id)
        {
            M_RackMasterEntity entity = new M_RackMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_RackMaster.ToList().Where(x => x.Id == id && x.IsActive == true);
                foreach (var item in records)
                {

                    entity.Id = item.Id;
                    entity.Rack = item.Rack;
                    entity.Rackdesc = item.Rackdesc;
                    entity.Comment = item.Comment;
                    entity.DepartmentId = item.DepartmentId;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                        deptentity.Id = item.M_DepartmentMaster.Id;
                        deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        deptentity.DepartmentNo = item.M_DepartmentMaster.DepartmentNo;
                        entity.M_DepartmentMaster = deptentity;
                    }
                    entity.WarehouseId = item.WarehouseId;
                    if (item.WarehouseId > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseId).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WAREHOUSE = locentity;
                        }
                    }
                    
                    entity.Createdate = item.Createdate;
                    entity.Createdby = item.Createdby;
                    entity.Updatedate = item.Updatedate;
                    entity.Updatedby = item.Updatedby;
                    entity.IsActive = item.IsActive;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                }
            }
            return entity;
        }
        public int Insert(M_RackMasterEntity entity)
        {
            int insertedid = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_RackMaster record = new M_RackMaster();
                record.Id = entity.Id;
                record.Rack = entity.Rack;
                record.Rackdesc = entity.Rackdesc;
                record.Comment = entity.Comment;
                record.DepartmentId = entity.DepartmentId;
                record.WarehouseId = entity.WarehouseId;
                record.Createdby = entity.Createdby;
                record.Createdate = entity.Createdate;
                //record.Updatedate = entity.Updatedate;
                //record.Updatedby = entity.Updatedby;
                //New Code Added by Rahul on 09-01-2020
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                record.IsActive = entity.IsActive;
                db.M_RackMaster.Add(record);
                db.SaveChanges();
                insertedid = record.Id;
            }
            return insertedid;
        }
        public bool Update(M_RackMasterEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_RackMaster.Where(x => x.Id == entity.Id).FirstOrDefault();
                record.Id = entity.Id;
                record.Rack = entity.Rack;
                record.Rackdesc = entity.Rackdesc;
                record.Comment = entity.Comment;
                record.DepartmentId = entity.DepartmentId;
                record.WarehouseId = entity.WarehouseId;
                //record.Createdby = entity.Createdby;
                //record.Createdate = entity.Createdate;
                record.Updatedate = entity.Updatedate;
                record.Updatedby = entity.Updatedby;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;
                record.IsActive = entity.IsActive;
                try
                {
                    db.SaveChanges();
                    IsUpdated = true;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return IsUpdated;
        }
        public bool Delete(int id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var rack = db.M_RackMaster.Where(x => x.Id == id).FirstOrDefault();
                rack.IsActive = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
                return IsDeleted;

            }
        }
    }
}
