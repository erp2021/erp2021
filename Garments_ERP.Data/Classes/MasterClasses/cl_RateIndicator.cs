﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;

namespace Garments_ERP.Data.Admin
{
    public class cl_RateIndicator
    {
        public long Insert(M_RateIndicatorMasterEntity entity)
        {
            long insertedid = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_RateIndicatorMaster record = new M_RateIndicatorMaster();
                record.Id = entity.Id;
                record.RateIndicatorName = entity.RateIndicatorName;
                record.ShortName = entity.ShortName;
                record.RateIndicatorDesc = entity.RateIndicatorDesc;

                record.CreatedDate = entity.CreatedDate;
                record.CreatedBy = entity.CreatedBy;

                //New Code Added by Rahul on 09-01-2020
                //record.UpdatedDate = entity.UpdatedDate;
                //record.UpdatedBy = entity.UpdatedBy;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                record.IsActive = entity.IsActive;

                db.M_RateIndicatorMaster.Add(record);
                db.SaveChanges();
                insertedid = record.Id;
            }
            return insertedid;
        }

        public List<M_RateIndicatorMasterEntity> get()
        {

            List<M_RateIndicatorMasterEntity> list = new List<M_RateIndicatorMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_RateIndicatorMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_RateIndicatorMasterEntity entity = new M_RateIndicatorMasterEntity();
                    entity.Id = item.Id;
                    entity.RateIndicatorName = item.RateIndicatorName;
                    entity.ShortName = item.ShortName;
                    entity.RateIndicatorDesc = item.RateIndicatorDesc;

                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    list.Add(entity);
                }
            }
            return list;

        }

        public bool Update(M_RateIndicatorMasterEntity entity)
        {
            bool isupdated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_RateIndicatorMaster.Find(entity.Id);
                record.RateIndicatorName = entity.RateIndicatorName;
                record.ShortName = entity.ShortName;
                record.RateIndicatorDesc = entity.RateIndicatorDesc;
                //New Code Added by Rahul on 09-01-2020
                record.UpdatedBy = entity.UpdatedBy;
                record.UpdatedDate = entity.UpdatedDate;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                db.SaveChanges();
                isupdated = true;
            }

            return isupdated;

        }
        public bool Delete(int id)
        {
            bool isdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_RateIndicatorMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                isdelete = true;
            }
            return isdelete;
        }
        public bool CheckRateIndicatorNameExistance(string RateIndicatorName, int? Id)
        {
            bool result = true;

            using (var db = new GarmentERPDBEntities())
            {
                if (Id == null)
                {
                    var ratename = db.M_RateIndicatorMaster.Where(x => x.RateIndicatorName == RateIndicatorName && x.IsActive == true).FirstOrDefault();
                    if (ratename != null)
                        result = false;
                }
                else
                {
                    var ratename = db.M_RateIndicatorMaster.Where(x => x.RateIndicatorName == RateIndicatorName && x.Id != Id && x.IsActive == true).FirstOrDefault();
                    if (ratename != null)
                        result = false;
                }


            }
            return result;
        }
    }
}
