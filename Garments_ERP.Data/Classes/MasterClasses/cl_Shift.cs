﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_Shift
    {
        public List<M_ShiftMasterEntity> get()
        {
            List<M_ShiftMasterEntity> list = new List<M_ShiftMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                
                var records = db.M_SHIFT.ToList();
                foreach(var item in records)
                {
                    M_ShiftMasterEntity entity = new M_ShiftMasterEntity();
                    entity.ID = item.ID;                   
                    entity.SHORT_NAME = item.SHORT_NAME;
                    entity.CODE = item.CODE;
                    entity.START_TIME = item.START_TIME;
                    entity.END_TIME = item.END_TIME;
                    entity.EFFECTIVE_DATE = item.EFFECTIVE_DATE;
                    entity.DepartmentId = item.DepartmentId;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                        deptentity.Id = item.M_DepartmentMaster.Id;
                        deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        entity.M_DepartmentMaster = deptentity;
                    }
                    entity.Comment = item.Comment;
                    entity.CREATED_BY = item.CREATED_BY;
                    entity.CREATED_ON = item.CREATED_ON;
                    entity.IS_ENABLED = item.IS_ENABLED;                    
                    entity.UPDATED_BY = item.UPDATED_BY;
                    entity.UPDATED_ON = item.UPDATED_ON;                    
                    list.Add(entity);

                }   
            }
            return list;
        }


        public M_ShiftMasterEntity getbyid(long id)
        {
            M_ShiftMasterEntity entity = new M_ShiftMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.M_SHIFT.Find(id);
                entity.ID = item.ID;
                entity.SHORT_NAME = item.SHORT_NAME;
                entity.CODE = item.CODE;
                entity.START_TIME = item.START_TIME;
                entity.END_TIME = item.END_TIME;
                entity.EFFECTIVE_DATE = item.EFFECTIVE_DATE;
                entity.DepartmentId = item.DepartmentId;
                if (item.M_DepartmentMaster != null)
                {
                    M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                    deptentity.Id = item.M_DepartmentMaster.Id;
                    deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                    entity.M_DepartmentMaster = deptentity;
                }
                entity.Comment = item.Comment;
                entity.CREATED_BY = item.CREATED_BY;
                entity.CREATED_ON = item.CREATED_ON;
                entity.IS_ENABLED = item.IS_ENABLED;
                entity.UPDATED_BY = item.UPDATED_BY;
                entity.UPDATED_ON = item.UPDATED_ON;      
            }
            return entity;
        }


        public long Insert(M_ShiftMasterEntity entity)
        {
            long insertid = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    M_SHIFT record = new M_SHIFT();
                    record.ID = entity.ID;
                    record.CODE = entity.CODE;
                    record.CREATED_BY = entity.CREATED_BY;
                    record.CREATED_ON = entity.CREATED_ON;
                    record.IS_ENABLED = entity.IS_ENABLED;
                    record.START_TIME = entity.START_TIME;
                    record.END_TIME = entity.END_TIME;
                    record.EFFECTIVE_DATE = entity.EFFECTIVE_DATE;
                    record.Comment = entity.Comment;
                    record.DepartmentId = entity.DepartmentId;
                    record.SHORT_NAME = entity.SHORT_NAME;
                    //record.UPDATED_BY = entity.UPDATED_BY;
                    //record.UPDATED_ON = entity.UPDATED_ON;  
                    record.Company_ID = entity.Company_ID;
                    record.BranchId = entity.Branch_ID;                
                    db.M_SHIFT.Add(record);
                    db.SaveChanges();
                    insertid = record.ID;

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return insertid;
        }

        public bool Update(M_ShiftMasterEntity entity)
        {
            bool updated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_SHIFT.Find(entity.ID);
                record.ID = entity.ID;
                record.CODE = entity.CODE;               
                record.IS_ENABLED = entity.IS_ENABLED;
                record.START_TIME = entity.START_TIME;
                record.END_TIME = entity.END_TIME;
                record.EFFECTIVE_DATE = entity.EFFECTIVE_DATE;
                record.DepartmentId = entity.DepartmentId;
                record.Comment = entity.Comment;
                record.SHORT_NAME = entity.SHORT_NAME;
                record.UPDATED_BY = entity.UPDATED_BY;
                record.UPDATED_ON = entity.UPDATED_ON;

                //New Code Added by Rahul on 09-01-2020
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;
                     
                db.SaveChanges();
                updated = true;
            }

            return updated;
        }
        public bool Delete(long id)
        {
            bool deleted = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_SHIFT.Find(id);
                record.IS_ENABLED = false;
                db.SaveChanges();
                deleted = true;
            }

            return deleted;
        }
    }
}
