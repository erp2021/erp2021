﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_Employee
    {
        public List<EmployeeEntity> get(int comid,int branchid)
        {
            List<EmployeeEntity> list = new List<EmployeeEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                string str = "select ID,[Name],Gender,EmpID,Age,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,IsActive,DOB,PlaceOfBirth,BloodGroup,MaritalStatus,Nationality,Religion,";
                str = str + "MotherTongue,Caste,UserId,EmpTypeId from employee where IsActive=1 and UserId in(select Id from M_UserMaster where Company_ID="+ comid + " and BranchId="+ branchid + ")";

                var records = db.Employees.SqlQuery(str).ToList();

                foreach (var item in records)
                {
                    EmployeeEntity entity = new EmployeeEntity();
                    entity.ID = item.ID;
                    entity.Name = item.Name;
                    entity.Gender = item.Gender;
                    entity.EmpID = item.EmpID;
                    entity.Age = item.Age;
                    entity.CREATED_BY = item.CREATED_BY;
                    entity.CREATED_ON = item.CREATED_ON;
                    entity.IsActive = item.IsActive;
                    entity.DOB = item.DOB;
                    entity.PlaceOfBirth = item.PlaceOfBirth;
                    entity.BloodGroup = item.BloodGroup;
                    entity.MaritalStatus = item.MaritalStatus;
                    entity.Nationality = item.Nationality;
                    entity.Religion = item.Religion;
                    entity.MotherTongue = item.MotherTongue;
                    entity.Caste = item.Caste;
                    entity.UserId = item.UserId;
                    entity.EmpTypeId = item.EmpTypeId;
                    if(item.EmpTypeId>0)
                    {
                        M_Employee_TypeEntity typeEnt = new M_Employee_TypeEntity();
                        typeEnt.ShortName = db.M_Employee_Type.Where(x => x.EmpTypeId == item.EmpTypeId).Select(x => x.ShortName).FirstOrDefault();
                        entity.EmpType = typeEnt;
                    }

                    var roleid = db.M_User_Role.Where(x => x.UserId == item.UserId && x.IsActive == true).Select(x => x.roleId).FirstOrDefault();
                    M_RoleEntity role = new M_RoleEntity();
                    role.roleName = db.M_Role.Where(x => x.roleId == roleid).Select(x => x.roleName).FirstOrDefault();
                    entity.roleEntity = role;


                    var deptid = db.M_User_Department.Where(x => x.UserId == item.UserId && x.IsActive == true).Select(x => x.DepartmentId).FirstOrDefault();
                    M_DepartmentMasterEntity dept = new M_DepartmentMasterEntity();
                    dept.Departmentname = db.M_DepartmentMaster.Where(x => x.Id == deptid).Select(x => x.Departmentname).FirstOrDefault();
                    entity.DeptEntity = dept;

                    var AddressData = db.M_Emp_Address.Where(x => x.IsActive == true && x.UserID == item.UserId).FirstOrDefault();
                    EmployeeAddressDetailEntity addent = new EmployeeAddressDetailEntity();
                    addent.UserID = AddressData.UserID;
                    addent.Address1 = AddressData.Address1;
                    addent.Address2 = AddressData.Address2;
                    addent.City = AddressData.City;
                    addent.State = AddressData.State;
                    addent.Country = AddressData.Country;
                    addent.Pincode = AddressData.Pincode;
                    addent.MobileNo = AddressData.MobileNo;
                    addent.PhoneNo = AddressData.PhoneNo;
                    addent.Email = AddressData.Email;
                    addent.IsActive = AddressData.IsActive;
                    entity.empaddEnity = addent;
                    list.Add(entity);
                }
            }
            return list;
        }

        public EmployeeEntity getIdByUserData(int id)
        {
            EmployeeEntity entity = new EmployeeEntity();
            using (var db = new GarmentERPDBEntities())
            {
                string str = "select ID,[Name],Gender,EmpID,Age,CREATED_BY,CREATED_ON,UPDATED_BY,UPDATED_ON,IsActive,DOB,PlaceOfBirth,BloodGroup,MaritalStatus,Nationality,Religion,";
                str = str + "MotherTongue,Caste,UserId,EmpTypeId from employee where IsActive=1 and UserId="+ id + "";

                var item = db.Employees.SqlQuery(str).FirstOrDefault();
                entity.UserId = item.UserId;
                entity.ID = item.ID;
                entity.Name = item.Name;
                entity.Gender = item.Gender;
                entity.EmpID = item.EmpID;
                entity.Age = item.Age;
                entity.CREATED_BY = item.CREATED_BY;
                entity.CREATED_ON = item.CREATED_ON;
                entity.IsActive = item.IsActive;
                entity.DOB = item.DOB;
                entity.PlaceOfBirth = item.PlaceOfBirth;
                entity.BloodGroup = item.BloodGroup;
                entity.MaritalStatus = item.MaritalStatus;
                entity.Nationality = item.Nationality;
                entity.Religion = item.Religion;
                entity.MotherTongue = item.MotherTongue;
                entity.Caste = item.Caste;
                entity.EmpTypeId = item.EmpTypeId;
                if (item.EmpTypeId > 0)
                {
                    M_Employee_TypeEntity typeEnt = new M_Employee_TypeEntity();
                    typeEnt.ShortName = db.M_Employee_Type.Where(x => x.EmpTypeId == item.EmpTypeId).Select(x => x.ShortName).FirstOrDefault();
                    entity.EmpType = typeEnt;
                }

                var roleid = db.M_User_Role.Where(x => x.UserId == item.UserId && x.IsActive == true).Select(x => x.roleId).FirstOrDefault();
                M_RoleEntity role = new M_RoleEntity();
                role.roleName = db.M_Role.Where(x => x.roleId == roleid).Select(x => x.roleName).FirstOrDefault();
                role.roleId = Convert.ToInt64(roleid);
                entity.roleEntity = role;


                var deptid = db.M_User_Department.Where(x => x.UserId == item.UserId && x.IsActive == true).Select(x => x.DepartmentId).FirstOrDefault();
                M_DepartmentMasterEntity dept = new M_DepartmentMasterEntity();
                dept.Departmentname = db.M_DepartmentMaster.Where(x => x.Id == deptid).Select(x => x.Departmentname).FirstOrDefault();
                dept.Id = Convert.ToInt32(deptid);
                entity.DeptEntity = dept;

                var AddressData = db.M_Emp_Address.Where(x => x.IsActive == true && x.UserID == item.UserId).FirstOrDefault();
                EmployeeAddressDetailEntity addent = new EmployeeAddressDetailEntity();
                addent.UserID = AddressData.UserID;
                addent.Address1 = AddressData.Address1;
                addent.Address2 = AddressData.Address2;
                addent.City = AddressData.City;
                addent.State = AddressData.State;
                addent.Country = AddressData.Country;
                addent.Pincode = AddressData.Pincode;
                addent.MobileNo = AddressData.MobileNo;
                addent.PhoneNo = AddressData.PhoneNo;
                addent.Email = AddressData.Email;
                addent.IsActive = AddressData.IsActive;
                entity.empaddEnity = addent;
            }
            return entity;
        }

        public List<M_Employee_TypeEntity> getEmployeeType()
        {
            List<M_Employee_TypeEntity> list = new List<M_Employee_TypeEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_Employee_Type.Where(x=>x.IsActive==true).ToList();
                foreach(var data in record)
                {
                    M_Employee_TypeEntity emptype = new M_Employee_TypeEntity();
                    emptype.EmpTypeId = data.EmpTypeId;
                    emptype.ShortName = data.ShortName;
                    emptype.Discription = data.Discription;
                    emptype.IsActive = data.IsActive;
                    list.Add(emptype);

                }
            }

            return list;
        }


        public int Insert(EmployeeEntity entity)
        {
            int id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                Employee item = new Employee();
                item.ID = entity.ID;
                item.EmpID = entity.EmpID;
                item.Name = entity.Name;
                item.Gender = entity.Gender;
                item.Age = entity.Age;
                item.CREATED_BY = entity.CREATED_BY;
                item.CREATED_ON = entity.CREATED_ON;
                item.IsActive = entity.IsActive;
                item.DOB = entity.DOB;
                item.PlaceOfBirth = entity.PlaceOfBirth;
                item.BloodGroup = entity.BloodGroup;
                item.MaritalStatus = entity.MaritalStatus;
                item.Nationality = entity.Nationality;
                item.Religion = entity.Religion;
                item.MotherTongue = entity.MotherTongue;
                item.Caste = entity.Caste;
                item.UserId = entity.UserId;
                item.EmpTypeId = entity.EmpTypeId;
                db.Employees.Add(item);
                db.SaveChanges();
                id = item.ID;
            }
            return id;
        }


        public bool Update(EmployeeEntity entity,int DeptID, int RoleID)
        {
            bool status = false;
            using (var db = new GarmentERPDBEntities())
            {
                var urid = db.M_User_Role.Where(x => x.UserId == entity.UserId).Select(x => x.UserRoleId).FirstOrDefault();
                var Uitem = db.M_User_Role.Find(urid);
                Uitem.roleId = Convert.ToInt64(RoleID);
                db.SaveChanges();

                var udid = db.M_User_Department.Where(x => x.UserId == entity.UserId).Select(x => x.UserDeptId).FirstOrDefault();
                var UDitem = db.M_User_Department.Find(udid);
                UDitem.DepartmentId = Convert.ToInt64(DeptID);
                db.SaveChanges();

                var id= db.Employees.Where(x => x.UserId == entity.UserId).Select(x=>x.ID).FirstOrDefault();
                var item = db.Employees.Find(id);
                item.EmpID = entity.EmpID;
                item.Name = entity.Name;
                item.Gender = entity.Gender;
                item.Age = entity.Age;
                item.UPDATED_BY = entity.UPDATED_BY;
                item.UPDATED_ON = entity.UPDATED_ON;
                item.IsActive = entity.IsActive;
                item.DOB = entity.DOB;
                item.PlaceOfBirth = entity.PlaceOfBirth;
                item.BloodGroup = entity.BloodGroup;
                item.MaritalStatus = entity.MaritalStatus;
                item.Nationality = entity.Nationality;
                item.Religion = entity.Religion;
                item.MotherTongue = entity.MotherTongue;
                item.Caste = entity.Caste;
                item.UserId = entity.UserId;
                item.EmpTypeId = entity.EmpTypeId;
                db.SaveChanges();
                status = true;
            }
            return status;
        }

        public long InsertAddress(EmployeeAddressDetailEntity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_Emp_Address item = new M_Emp_Address();
                item.EmpAddressId = entity.EmpAddressId;
                item.UserID = entity.UserID;
                item.Address1 = entity.Address1;
                item.Address2 = entity.Address2;
                item.City = entity.City;
                item.State = entity.State;
                item.Country = entity.Country;
                item.Pincode = entity.Pincode;
                item.MobileNo = entity.MobileNo;
                item.PhoneNo = entity.PhoneNo;
                item.Email = entity.Email;
                item.IsActive = entity.IsActive;
                item.CreatedBy = entity.CreatedBy;
                item.CreatedOn = entity.CreatedOn;
                db.M_Emp_Address.Add(item);
                db.SaveChanges();
                id = item.EmpAddressId;
            }
            return id;
        }

        public bool UpdateAddress(EmployeeAddressDetailEntity entity)
        {
            bool Status = false;
            using (var db = new GarmentERPDBEntities())
            {
                var id = db.M_Emp_Address.Where(x => x.UserID == entity.UserID).Select(x => x.EmpAddressId).FirstOrDefault();
                var item = db.M_Emp_Address.Find(id);
                item.UserID = entity.UserID;
                item.Address1 = entity.Address1;
                item.Address2 = entity.Address2;
                item.City = entity.City;
                item.State = entity.State;
                item.Country = entity.Country;
                item.Pincode = entity.Pincode;
                item.MobileNo = entity.MobileNo;
                item.PhoneNo = entity.PhoneNo;
                item.Email = entity.Email;
                item.IsActive = entity.IsActive;
                item.UpdatedBy = entity.UpdatedBy;
                item.UpdatedOn = entity.UpdatedOn;
                db.SaveChanges();
                Status = true;
            }
            return Status;
        }


        public List<EmployeeAddressDetailEntity> getaddress()
        {
            //List<EmployeeAddressDetailEntity> list = new List<EmployeeAddressDetailEntity>();
            //using (var db = new GarmentERPDBEntities())
            //{
            //    //var records = db.EmployeeAddressDetails.ToList();
            //    //foreach (var item in records)
            //    //{
            //    //    EmployeeAddressDetailEntity entity = new EmployeeAddressDetailEntity();
            //    //    entity.Id = item.Id;
            //    //    entity.Addess = item.Addess;
            //    //    entity.Cityid = item.Cityid;
            //    //    if (item.M_CityMaster != null)
            //    //    {
            //    //        M_CityMasterEntity cityentity = new M_CityMasterEntity();
            //    //        cityentity.ID = item.M_CityMaster.ID;
            //    //        cityentity.Name = item.M_CityMaster.Name;
            //    //        entity.M_CityMaster = cityentity;
            //    //    }
            //    //    entity.Countryid = item.Countryid;
            //    //    if (item.M_CountryMaster != null)
            //    //    {
            //    //        M_CountryMasterEntity countryentity = new M_CountryMasterEntity();
            //    //        countryentity.ID = item.M_CountryMaster.ID;
            //    //        countryentity.Name = item.M_CountryMaster.Name;
            //    //        entity.M_CountryMaster = countryentity;
            //    //    }
            //    //    entity.CreatedBy = item.CreatedBy;
            //    //    entity.Createddate = item.Createddate;
            //    //    entity.EmployeeId = item.EmployeeId;
            //    //    if (item.Employee != null)
            //    //    {
            //    //        EmployeeEntity empentity = new EmployeeEntity();
            //    //        empentity.ID = item.Employee.ID;
            //    //        empentity.Name = item.Employee.Name;
            //    //        empentity.EmpID = item.Employee.EmpID;
            //    //        entity.Employee = empentity;
            //    //    }
            //    //    entity.IsActive = item.IsActive;
            //    //    entity.Stateid = item.Stateid;
            //    //    if (item.M_StateMaster != null)
            //    //    {
            //    //        M_StateMasterEntity stateentity = new M_StateMasterEntity();
            //    //        stateentity.ID = item.M_StateMaster.ID;
            //    //        stateentity.Name = item.M_StateMaster.Name;
            //    //        entity.M_StateMaster = stateentity;
            //    //    }
            //    //    entity.Updatedby = item.Updatedby;
            //    //    entity.Updateddate = item.Updateddate;
            //    //    list.Add(entity);
            //    list = null;
            //    }
            //}
            return null;
        }
        public EmployeeAddressDetailEntity getaddressbyid(long id)
        {
            EmployeeAddressDetailEntity entity = new EmployeeAddressDetailEntity();
            using (var db = new GarmentERPDBEntities())
            {
                //var item = db.EmployeeAddressDetails.Find(id);
                //entity.Id = item.Id;
                //entity.Addess = item.Addess;
                //entity.Cityid = item.Cityid;
                //if (item.M_CityMaster != null)
                //{
                //    M_CityMasterEntity cityentity = new M_CityMasterEntity();
                //    cityentity.ID = item.M_CityMaster.ID;
                //    cityentity.Name = item.M_CityMaster.Name;
                //    entity.M_CityMaster = cityentity;
                //}
                //entity.Countryid = item.Countryid;
                //if (item.M_CountryMaster != null)
                //{
                //    M_CountryMasterEntity countryentity = new M_CountryMasterEntity();
                //    countryentity.ID = item.M_CountryMaster.ID;
                //    countryentity.Name = item.M_CountryMaster.Name;
                //    entity.M_CountryMaster = countryentity;
                //}
                //entity.CreatedBy = item.CreatedBy;
                //entity.Createddate = item.Createddate;
                //entity.EmployeeId = item.EmployeeId;
                //if (item.Employee != null)
                //{
                //    EmployeeEntity empentity = new EmployeeEntity();
                //    empentity.ID = item.Employee.ID;
                //    empentity.Name = item.Employee.Name;
                //    empentity.EmpID = item.Employee.EmpID;
                //    entity.Employee = empentity;
                //}
                //entity.IsActive = item.IsActive;
                //entity.Stateid = item.Stateid;
                //if (item.M_StateMaster != null)
                //{
                //    M_StateMasterEntity stateentity = new M_StateMasterEntity();
                //    stateentity.ID = item.M_StateMaster.ID;
                //    stateentity.Name = item.M_StateMaster.Name;
                //    entity.M_StateMaster = stateentity;
                //}
                //entity.Updatedby = item.Updatedby;
                //entity.Updateddate = item.Updateddate;

            }
            return entity;
        }
        public bool Delete(int id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var emp = db.Employees.Where(x => x.ID == id).FirstOrDefault();
                emp.IsActive = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
                return IsDeleted;

            }
        }
    }
}
