﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
namespace Garments_ERP.Data.Admin
{
  public  class cl_Inspectiontype
    {
      public List<M_InspectiontypeEntity> Get()
      {
         List<M_InspectiontypeEntity> list = new List<M_InspectiontypeEntity>();
         using(var db=new GarmentERPDBEntities())
         {
             var record = db.M_InspectionTypeMaster.ToList();
             foreach (var item in record)
             {
                 M_InspectiontypeEntity entity = new M_InspectiontypeEntity();
                 entity.Id = item.Id;
                 entity.Inspectiontype = item.Inspectiontype;
                 entity.Inspectiontypedescription = item.Inspectiontypedescription;
                 list.Add(entity);

             }
         }
       return list;
      }

    }
}
