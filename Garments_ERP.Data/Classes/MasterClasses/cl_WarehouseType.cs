﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_WarehouseType
    {
        public List<M_WarehouseTypeMasterEntity> get()
        {
            List<M_WarehouseTypeMasterEntity> list = new List<M_WarehouseTypeMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                
                var records = db.M_WarehouseTypeMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_WarehouseTypeMasterEntity entity = new M_WarehouseTypeMasterEntity();
                    entity.WAREHOUSE_TYPE_ID = item.WAREHOUSE_TYPE_ID;
                    entity.TypeName = item.TypeName;
                    entity.Typedesc = item.Typedesc;
                    entity.IsActive = item.IsActive;
                    list.Add(entity);

                }
            }
            return list;
        }


        public M_WarehouseTypeMasterEntity getbyid(long id)
        {
            M_WarehouseTypeMasterEntity entity = new M_WarehouseTypeMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.M_WarehouseTypeMaster.Find(id);
                entity.WAREHOUSE_TYPE_ID = item.WAREHOUSE_TYPE_ID;
                entity.TypeName = item.TypeName;
                entity.Typedesc = item.Typedesc;
                entity.IsActive = item.IsActive;
            }
            return entity;
        }


        public long Insert(M_WarehouseTypeMasterEntity entity)
        {
            long insertid = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    M_WarehouseTypeMaster record = new M_WarehouseTypeMaster();                   
                    record.WAREHOUSE_TYPE_ID = entity.WAREHOUSE_TYPE_ID;
                    record.TypeName = entity.TypeName;
                    record.Typedesc = entity.Typedesc;
                    db.M_WarehouseTypeMaster.Add(record);
                    db.SaveChanges();
                    insertid = record.WAREHOUSE_TYPE_ID;

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return insertid;
        }

        public bool Update(M_WarehouseTypeMasterEntity entity)
        {
            bool updated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_WarehouseTypeMaster.Find(entity.WAREHOUSE_TYPE_ID);                
                record.WAREHOUSE_TYPE_ID = entity.WAREHOUSE_TYPE_ID;
                record.TypeName = entity.TypeName;
                record.Typedesc = entity.Typedesc;
                record.IsActive = entity.IsActive;
                db.SaveChanges();
            }

            return updated;
        }
        public bool Delete(long id)
        {
            bool deleted = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_WarehouseTypeMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                deleted = true;
            }

            return deleted;
        }
    }
}
