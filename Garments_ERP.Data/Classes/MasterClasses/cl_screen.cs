﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_screen
    {
        public List<M_ScreenEntity> get()
        {

            List<M_ScreenEntity> list = new List<M_ScreenEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_Screen.ToList();
                foreach (var item in records)
                {
                    M_ScreenEntity entity = new M_ScreenEntity();
                    entity.screenId = item.screenId;
                    entity.ScreenName = item.ScreenName;
                    entity.Discription = item.Discription;
                    entity.URL = item.URL;
                    entity.CreatedOn = item.CreatedOn;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdateOn = item.UpdateOn;
                    entity.UpdateBy = item.UpdateBy;
                    entity.IsActive = item.IsActive;
                    entity.ParentId = item.ParentId;
                    M_ScreenEntity screntity = new M_ScreenEntity();
                    if (item.ParentId==0)
                    {   
                        screntity.ScreenName = db.M_Screen.Where(x => x.screenId == item.screenId).Select(x => x.ScreenName).FirstOrDefault();
                    }
                    else
                    {
                        screntity.ScreenName = db.M_Screen.Where(x => x.screenId == item.ParentId).Select(x => x.ScreenName).FirstOrDefault();
                    }
                    entity.ScreenEntity = screntity;
                    list.Add(entity);
                }
            }
            return list;
        }

        public long Insert(M_ScreenEntity entity)
        {
            long id = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    M_Screen item = new M_Screen();
                    item.screenId = entity.screenId;
                    item.ScreenName = entity.ScreenName;
                    item.Discription = entity.Discription;
                    item.ParentId = entity.ParentId;
                    item.URL = entity.URL;
                    item.Sequence = entity.Sequence;
                    item.IsActive = true;
                    item.CreatedBy = entity.CreatedBy;
                    item.CreatedOn = DateTime.Now;
                    item.IsSub = entity.IsSub;
                    db.Configuration.ValidateOnSaveEnabled = false;
                    db.M_Screen.Add(item);
                    db.SaveChanges();
                    db.Configuration.ValidateOnSaveEnabled = true;
                    id = item.screenId;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return id;
        }

        public bool ActiveDeactive(int id,string Status)
        {
            var isstatus = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_Screen.Find(id);
                record.IsActive = Status == "Deactive" ? true : false;
                db.SaveChanges();
                isstatus = true;
            }
            return isstatus;
        }

        public bool Update(M_ScreenEntity entity)
        {
            var isstatus = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.M_Screen.Find(entity.screenId);
                    record.screenId = entity.screenId;
                    record.ScreenName = entity.ScreenName;
                    record.Discription = entity.Discription;
                    record.ParentId = entity.ParentId;
                    record.URL = entity.URL;
                    record.Sequence = entity.Sequence;
                    record.IsActive = true;
                    record.UpdateBy = entity.UpdateBy;
                    record.IsSub = entity.IsSub;
                    record.UpdateOn = DateTime.Now;
                    db.SaveChanges();
                    isstatus = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isstatus;
        }
    }
}
