﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;

namespace Garments_ERP.Data.Admin
{
    public class cl_Customer
    {

        public List<M_CustomerMasterEntity> getCustomerByWO(long woID)
        {
           
            List<M_CustomerMasterEntity> list = new List<M_CustomerMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var CustId = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == woID && x.IsActive==true).Select(x => x.CustomerID).SingleOrDefault();

                var records = db.M_CustomerMaster.ToList().Where(x => x.IsActive == true && x.Id== CustId);
                foreach (var item in records)
                {
                    M_CustomerMasterEntity entity = new M_CustomerMasterEntity();
                    entity.Id = item.Id;
                    entity.contactname = item.contactname;
                    entity.Customertype = item.Customertype;
                    list.Add(entity);
                }

            }
            return list;
        }


        public List<M_CustomerMasterEntity> get()
        {
            List<M_CustomerMasterEntity> list = new List<M_CustomerMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_CustomerMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_CustomerMasterEntity entity = new M_CustomerMasterEntity();
                    entity.Id = item.Id;
                    entity.contactname = item.contactname;
                    entity.Customertype = item.Customertype;
                    if (item.M_CustomerTypeMaster != null)
                    {
                        M_CustomerTypeMasterEntity typeentity = new M_CustomerTypeMasterEntity();
                        typeentity.Id = item.M_CustomerTypeMaster.Id;
                        typeentity.TypeName = item.M_CustomerTypeMaster.TypeName;
                        entity.M_CustomerTypeMaster = typeentity;
                    }
                    entity.Phone = item.Phone;
                    entity.Address = item.Address;
                    entity.Cityid = item.Cityid;
                    entity.Stateid = item.Stateid;
                    entity.Countryid = item.Countryid;
                    entity.GSTNo = item.GSTNo;
                    entity.Email = item.Email;
                    entity.Remark = item.Remark;
                    entity.CSTRegno = item.CSTRegno;
                    entity.Tinno = item.Tinno;
                    entity.Createddate = item.Createddate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    list.Add(entity);
                }

            }
            return list;
        }
        public M_CustomerMasterEntity GetById(long id)
        {
            M_CustomerMasterEntity entity = new M_CustomerMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_CustomerMaster.Where(x => x.Id == id && x.IsActive == true);
                foreach (var item in records)
                {
                    // M_CustomerMasterEntity entity = new M_CustomerMasterEntity();
                    entity.Id = item.Id;
                    entity.contactname = item.contactname;
                    entity.Customertype = item.Customertype;
                    if (item.M_CustomerTypeMaster != null)
                    {
                        M_CustomerTypeMasterEntity typeentity = new M_CustomerTypeMasterEntity();
                        typeentity.Id = item.M_CustomerTypeMaster.Id;
                        typeentity.TypeName = item.M_CustomerTypeMaster.TypeName;
                        entity.M_CustomerTypeMaster = typeentity;
                    }
                    entity.Phone = item.Phone;
                    entity.Address = item.Address;
                    entity.Cityid = item.Cityid;
                    entity.Stateid = item.Stateid;
                    entity.Countryid = item.Countryid;
                    entity.GSTNo = item.GSTNo;
                    entity.Email = item.Email;
                    entity.Remark = item.Remark;
                    entity.CSTRegno = item.CSTRegno;
                    entity.Tinno = item.Tinno;
                    entity.Createddate = item.Createddate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;

                    var recordaddresslist = db.M_ContactAddressMaster.Where(x => x.customerid == item.Id && x.IsActive==true).ToList();
                    List<M_ContactAddressEntity> contactaddresslist = new List<M_ContactAddressEntity>();
                    foreach (var itemaddress in recordaddresslist)
                    {
                        M_ContactAddressEntity addressentity = new M_ContactAddressEntity();
                        addressentity.Id = itemaddress.Id;
                        addressentity.Addess = itemaddress.Addess;
                        addressentity.Stateid = itemaddress.Stateid;
                        addressentity.Cityid = itemaddress.Cityid;
                        M_CityMasterEntity cityentity = new M_CityMasterEntity();
                        if(itemaddress.M_CityMaster!=null)
                        {
                            cityentity.ID = itemaddress.M_CityMaster.ID;
                            cityentity.Name = itemaddress.M_CityMaster.Name;
                            addressentity.M_Cityentity = cityentity;
                        }
                        M_StateMasterEntity stateentity = new M_StateMasterEntity();
                        if(itemaddress.M_StateMaster!=null)
                        {
                            stateentity.ID = itemaddress.M_StateMaster.ID;
                            stateentity.Name = itemaddress.M_StateMaster.Name;
                            addressentity.M_Stateentity = stateentity;
                        }
                        addressentity.Countryid = itemaddress.Countryid;

                        contactaddresslist.Add(addressentity);
                        
                    }
                    entity.contactaddresslist = contactaddresslist;
                    //list.Add(entity);
                }

            }
            return entity;
        }
        public List<M_ContactAddressEntity> getAddresssbycustomerid(long custid)
        {
            List<M_ContactAddressEntity> list = new List<M_ContactAddressEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ContactAddressMaster.Where(x => x.customerid == custid).ToList();
                foreach (var item in records)
                {
                    M_ContactAddressEntity entity = new M_ContactAddressEntity();
                    entity.Id = item.Id;
                    entity.customerid = item.Id;
                    entity.Addess = item.Addess;
                    entity.Cityid = item.Cityid;
                    entity.Stateid = item.Stateid;
                    entity.Countryid = item.Countryid;
                    entity.Createddate = item.Createddate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    list.Add(entity);
                }

            }
            return list;
        }

        public bool  deleteaddress(long id)
        {
            bool isdeleted = false;

            using(var db=new GarmentERPDBEntities())
            {
                var record = db.M_ContactAddressMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                isdeleted = true;
            }

            return isdeleted;
        }

        public bool Updateadddress(M_ContactAddressEntity entity)
        {
            bool isupdated = false;

            using(var db=new GarmentERPDBEntities())
            {
                var record = db.M_ContactAddressMaster.Find(entity.Id);
                record.Addess = entity.Addess;
                record.Cityid = entity.Cityid;
                record.Stateid = entity.Stateid;
                db.SaveChanges();
                isupdated = true;
            }
            return isupdated;
        }

        public long Insert(M_CustomerMasterEntity entity)
        {
            long id = 0;
            long addId = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_CustomerMaster item = new M_CustomerMaster();
                item.Id = entity.Id;
                item.contactname = entity.contactname;
                item.Customertype = entity.Customertype;

                item.Phone = entity.Phone;
                item.Address = entity.Address;
                item.Cityid = entity.Cityid;
                item.Stateid = entity.Stateid;
                item.Countryid = entity.Countryid;
                item.GSTNo = entity.GSTNo;
                item.Email = entity.Email;
                item.Remark = entity.Remark;
                item.CSTRegno = entity.CSTRegno;
                item.Tinno = entity.Tinno;
                //item.CreatedBy = entity.CreatedBy;
                item.Createddate = entity.Createddate;
                //item.UpdatedBy = entity.UpdatedBy;
                item.Updateddate = entity.Updateddate;
                item.IsActive = entity.IsActive;
                db.M_CustomerMaster.Add(item);
                db.SaveChanges();
                id = item.Id;

                if(id>0)
                {
                    foreach(var address in entity.contactaddresslist)
                    {
                        M_ContactAddressMaster addrecord = new M_ContactAddressMaster();
                        string[] adresssrr = address.Addess.Split('~');

                        addrecord.customerid = id;
                        addrecord.Addess =Convert.ToString(adresssrr[0]);
                        addrecord.Cityid = address.Cityid;
                        addrecord.Stateid = address.Stateid;
                        addrecord.Countryid = address.Countryid;
                        addrecord.IsActive = true;
                        db.M_ContactAddressMaster.Add(addrecord);
                        db.SaveChanges();
                        if (Convert.ToString(adresssrr[1]) == "1")
                        {
                            addId = addrecord.Id;
                            bool IsUpdated1 = false;
                            using (var db1 = new GarmentERPDBEntities())
                            {
                                var record = db1.M_CustomerMaster.Where(x => x.Id == id).FirstOrDefault();
                                record.Address =Convert.ToString(addId);
                                db1.SaveChanges();
                                IsUpdated1 = true;
                            }
                        }
                    }

                }

            }
            return id;
        }
        public bool Update(M_CustomerMasterEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_CustomerMaster.Where(x => x.Id == entity.Id).FirstOrDefault();
                record.Id = entity.Id;
                record.Address = entity.Address;
                record.Cityid = entity.Cityid;
                record.contactname = entity.contactname;
                record.Countryid = entity.Countryid;
                record.CSTRegno = entity.CSTRegno;
                record.Customertype = entity.Customertype;
                record.Email = entity.Email;
                record.GSTNo = entity.GSTNo;
                record.IsActive = entity.IsActive;
                record.Phone = entity.Phone;
                record.Remark = entity.Remark;
                record.Stateid = entity.Stateid;
                record.Tinno = entity.Tinno;
                record.UpdatedBy = entity.UpdatedBy;
                record.Updateddate = entity.Updateddate;
                try
                {
                    db.SaveChanges();
                    IsUpdated = true;

                    foreach (var address in entity.contactaddresslist)
                    {
                        M_ContactAddressMaster addrecord = new M_ContactAddressMaster();
                        addrecord.customerid = record.Id;
                        addrecord.Addess = address.Addess;
                        addrecord.Cityid = address.Cityid;
                        addrecord.Stateid = address.Stateid;
                        addrecord.IsActive = true;
                        db.M_ContactAddressMaster.Add(addrecord);
                        db.SaveChanges();
                    }


                }
                catch (Exception)
                {
                    
                    throw;
                }
            }
            return IsUpdated;
        }
        public bool Delete(long id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var contact = db.M_CustomerMaster.Where(x => x.Id == id).FirstOrDefault();
                contact.IsActive = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
                return IsDeleted;

            }
        }
    }
}
