﻿using Garments_ERP.Entity;
using Garments_ERP.Data.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_Country
    {
        public List<M_CountryMasterEntity> get()
        {
            List<M_CountryMasterEntity> list = new List<M_CountryMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_CountryMaster.ToList();
                foreach (var item in records)
                {
                    M_CountryMasterEntity entity = new M_CountryMasterEntity();
                    entity.ID = item.ID;
                    entity.Name = item.Name;
                    entity.CountryCode = item.CountryCode;
                    list.Add(entity);
                }
            }

            return list;
        }
    }
}
