﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System.Data;
using System.Data.SqlClient;

namespace Garments_ERP.Data.Admin
{
    public class cl_LedgerAccount
    {
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;

       
        public List<M_Ledger_BillingDetailsEntity> getAddInfo(int id)
        {
            List<M_Ledger_BillingDetailsEntity> list = new List<M_Ledger_BillingDetailsEntity>();
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 8; //Get Of Address Details
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = id;
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                foreach (DataTable table in dt.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        M_Ledger_BillingDetailsEntity ledenty = new M_Ledger_BillingDetailsEntity();
                        ledenty.Id = Convert.ToInt32(dr["id"]);
                        ledenty.Email = Convert.ToString(dr["Email"]);
                        ledenty.Contact_No = Convert.ToString(dr["Contact_No"]);
                        ledenty.Company_ID = Convert.ToInt32(dr["Company_ID"]);
                        ledenty.Branch_ID = Convert.ToInt32(dr["BranchId"]);
                        list.Add(ledenty);
                    }
                }

            }
            return list;
        }


        public List<M_LedgersEntity> getCustList(int id)
        {
            List<M_LedgersEntity> list = new List<M_LedgersEntity>();
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 6; //List Of Customer,supplier, Broker
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = id;
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                foreach (DataTable table in dt.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        M_LedgersEntity ledenty = new M_LedgersEntity();
                        ledenty.Ledger_Id = Convert.ToInt32(dr["Ledger_Id"]);
                        ledenty.Ledger_Name = Convert.ToString(dr["Ledger_Name"]);
                        ledenty.Company_ID = Convert.ToInt32(dr["Company_ID"]);
                        ledenty.Branch_ID = Convert.ToInt32(dr["BranchId"]);
                        list.Add(ledenty);
                    }
                }

            }
            return list;
        }
        public List<M_LedgersEntity> getSupplierList(int id)
        {
            List<M_LedgersEntity> list = new List<M_LedgersEntity>();
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 6; //List Of Customer,supplier, Broker
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = id;
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                foreach (DataTable table in dt.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        M_LedgersEntity ledenty = new M_LedgersEntity();
                        ledenty.Ledger_Id = Convert.ToInt32(dr["Ledger_Id"]);
                        ledenty.Ledger_Name = Convert.ToString(dr["Ledger_Name"]);
                        ledenty.Company_ID = Convert.ToInt32(dr["Company_ID"]);
                        ledenty.Branch_ID = Convert.ToInt32(dr["BranchId"]);
                        list.Add(ledenty);
                    }
                }

            }
            return list;
        }

        public List<M_LedgersEntity> getCustomerByWO(long woID)
        {

            List<M_LedgersEntity> list = new List<M_LedgersEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var CustId = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == woID && x.IsActive == true).Select(x => x.CustomerID).SingleOrDefault();

                var records = db.M_Ledgers.ToList().Where(x => x.Is_Active == true && x.Ledger_Id == CustId);
                foreach (var item in records)
                {
                    M_LedgersEntity entity = new M_LedgersEntity();
                    entity.Ledger_Id = item.Ledger_Id;
                    entity.Ledger_Name = item.Ledger_Name;
                    entity.LedgerType_Id = item.LedgerType_Id;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    list.Add(entity);
                }

            }
            return list;
        }


        public List<M_LedgersEntity> get()
        {
            List<M_LedgersEntity> list = new List<M_LedgersEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_Ledgers.ToList().Where(x => x.Is_Active == true);
                foreach (var item in records)
                {
                    M_LedgersEntity entity = new M_LedgersEntity();
                    entity.Ledger_Id = item.Ledger_Id;
                    entity.Ledger_Name = item.Ledger_Name;
                    entity.LedgerType_Id = item.LedgerType_Id;
                    var _Ledgertype = db.M_CustomerTypeMaster.ToList().Where(x => item.LedgerType_Id.Contains(Convert.ToString(x.Id)));

                    List<M_CustomerTypeMasterEntity> Ledtype = new List<M_CustomerTypeMasterEntity>();
                    if (item.LedgerType_Id != null && _Ledgertype != null)
                    {
                        foreach (var data in _Ledgertype)
                        {
                            M_CustomerTypeMasterEntity typeentity = new M_CustomerTypeMasterEntity();
                            typeentity.Id = data.Id;
                            typeentity.TypeName = data.TypeName;
                            Ledtype.Add(typeentity);
                        }
                    }
                    entity.M_CustomerTypeMaster = Ledtype; 
                    entity.PAN_no = item.PAN_no;
                    entity.Department = item.Department;
                    List<M_DepartmentMasterEntity> depetype = new List<M_DepartmentMasterEntity>();
                    var _depttype = db.M_DepartmentMaster.ToList().Where(x =>x.Id==item.Department);
                    if (item.Department != null && _depttype != null)
                    {
                        foreach (var data in _depttype)
                        {
                            M_DepartmentMasterEntity dtypeentity = new M_DepartmentMasterEntity();
                            dtypeentity.Id = data.Id;
                            dtypeentity.Departmentname = data.Departmentname;
                            dtypeentity.Id = data.Id;
                            depetype.Add(dtypeentity);
                        }
                    }
                    entity.deptlist = depetype;

                    entity.Ledger_Description = item.Ledger_Description;
                    entity.Is_Active = item.Is_Active;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;
                    list.Add(entity);
                }

            }
            return list;
        }
        public M_LedgersEntity GetById(long id)
        {
            M_LedgersEntity entity = new M_LedgersEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_Ledgers.Where(x => x.Ledger_Id == id && x.Is_Active == true);
                foreach (var item in records)
                {
                    // M_CustomerMasterEntity entity = new M_CustomerMasterEntity();
                    entity.Ledger_Id = item.Ledger_Id;
                    entity.Ledger_Name = item.Ledger_Name;
                    entity.LedgerType_Id = item.LedgerType_Id;
                    entity.Ledger_Description = item.Ledger_Description;
                    entity.PAN_no = item.PAN_no;
                    entity.Department = item.Department;
                    M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                    var deptname = db.M_DepartmentMaster.Where(x => x.Id == item.Department).Select(x => x.Departmentname).FirstOrDefault();
                    deptentity.Departmentname = deptname;
                    entity.M_Departmententity = deptentity;
                    entity.Is_Active = item.Is_Active;
                    entity.Company_ID = item.Company_ID;
                    entity.Branch_ID = item.BranchId;

                    var recorBillinglist = db.M_Ledger_BillingDetails.Where(x => x.Ledger_Id == item.Ledger_Id && x.Is_Active == true  && x.Company_ID==item.Company_ID && x.BranchId==item.BranchId).ToList();
                    List<M_Ledger_BillingDetailsEntity> Billinglist = new List<M_Ledger_BillingDetailsEntity>();
                    foreach (var itembill in recorBillinglist)
                    {
                        M_Ledger_BillingDetailsEntity billentity = new M_Ledger_BillingDetailsEntity();
                        billentity.Id = itembill.Id;
                        billentity.Ledger_Id = itembill.Ledger_Id;
                        billentity.Billing_Name = itembill.Billing_Name;
                        billentity.Unit = itembill.Unit;
                        billentity.Contact_No = itembill.Contact_No;
                        billentity.Telephone_no = itembill.Telephone_no;
                        billentity.Fax_no = itembill.Fax_no;
                        billentity.Email = itembill.Email;
                        billentity.GSTIN = itembill.GSTIN;
                        billentity.Address = itembill.Address;
                        billentity.State_Id = itembill.State_Id;
                        billentity.City_Id = itembill.City_Id;
                        billentity.Is_SEZ = itembill.Is_SEZ;
                        billentity.Bank_Name = itembill.Bank_Name;
                        billentity.Bank_code = itembill.Bank_code;
                        billentity.Account_no = itembill.Account_no;
                        billentity.Branch = itembill.Branch;
                        billentity.Company = itembill.Company;
                        billentity.Is_Active = itembill.Is_Active;
                        billentity.Company_ID = itembill.Company_ID;
                        billentity.Branch_ID = itembill.BranchId;

                        M_CityMasterEntity cityentity = new M_CityMasterEntity();
                        if (itembill.City_Id != null)
                        {
                            string cityname = db.M_CityMaster.Where(x => x.ID == itembill.City_Id).Select(x => x.Name).SingleOrDefault();
                            cityentity.ID = Convert.ToInt32(itembill.City_Id);
                            cityentity.Name = cityname;
                            billentity.M_Cityentity = cityentity;
                        }

                        M_StateMasterEntity stateentity = new M_StateMasterEntity();
                        if (itembill.State_Id != null)
                        {
                            string statename = db.M_StateMaster.Where(x => x.ID == itembill.State_Id).Select(x => x.Name).SingleOrDefault();
                            stateentity.ID = Convert.ToInt32(itembill.State_Id);
                            stateentity.Name = statename;
                            billentity.M_Stateentity = stateentity;
                        }

                        M_CountryMasterEntity countryentity = new M_CountryMasterEntity();
                        if (itembill.Country_Id != null)
                        {
                            string Countryname = db.M_CountryMaster.Where(x => x.ID == itembill.Country_Id).Select(x => x.Name).SingleOrDefault();
                            countryentity.ID = Convert.ToInt32(itembill.Country_Id);
                            countryentity.Name = Countryname;
                            billentity.M_Countryentity = countryentity;
                        }

                        billentity.Country_Id = itembill.Country_Id;
                        
                        Billinglist.Add(billentity);

                    }
                    entity.M_Ledger_BillingDetails = Billinglist;

                    var recorDoclist = db.M_LedgerDocumentMaster.Where(x => x.LedgerId == item.Ledger_Id && x.IsActive == true).ToList();
                    List<M_LedgerDocumentMasterEntity> doclist = new List<M_LedgerDocumentMasterEntity>();
                    foreach(var doc in recorDoclist)
                    {
                        M_LedgerDocumentMasterEntity docentity = new M_LedgerDocumentMasterEntity();
                        docentity.Id = doc.Id;
                        docentity.LedgerId = doc.LedgerId;
                        docentity.Filename = doc.Filename;
                        docentity.Filetype = doc.Filetype;
                        docentity.IsActive = doc.IsActive;
                        doclist.Add(docentity);
                    }
                    entity.M_LedgerDocumentMaster = doclist;

                    var maplist = db.M_CompanyBranch_Mapping.Where(x => x.Ledger_Id == item.Ledger_Id && x.Is_Active == true).ToList();
                    List<M_CompanyBranch_MappingEntity> commap = new List<M_CompanyBranch_MappingEntity>();
                    foreach (var doc in maplist)
                    {
                        M_CompanyBranch_MappingEntity mapentity = new M_CompanyBranch_MappingEntity();
                        mapentity.Id = doc.Id;
                        mapentity.Is_Active = doc.Is_Active;
                        mapentity.Ledger_Id = doc.Ledger_Id;
                        mapentity.Company_ID = doc.Company_ID;
                        var companame = db.M_company.Where(x => x.Company_ID == doc.Company_ID).Select(x => x.Company_Name).SingleOrDefault();
                        M_companyEntity ce = new M_companyEntity();
                        ce.Company_Name = companame;
                        mapentity.M_company = ce;
                        
                        mapentity.Branch_ID = doc.Branch_ID;
                        var branchname = db.M_Branch.Where(x => x.Company_ID == doc.Company_ID && x.Branch_ID== doc.Branch_ID).Select(x => x.Branch_Name).SingleOrDefault();
                        M_BranchEntity be = new M_BranchEntity();
                        be.Branch_Name = branchname;
                        mapentity.M_Branch = be;
                        commap.Add(mapentity);
                    }
                    entity.M_CompanyBranch_Mapping = commap;
                }

            }
            return entity;
        }
        public List<M_ContactAddressEntity> getAddresssbycustomerid(long custid)
        {
            List<M_ContactAddressEntity> list = new List<M_ContactAddressEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ContactAddressMaster.Where(x => x.customerid == custid).ToList();
                foreach (var item in records)
                {
                    M_ContactAddressEntity entity = new M_ContactAddressEntity();
                    entity.Id = item.Id;
                    entity.customerid = item.Id;
                    entity.Addess = item.Addess;
                    entity.Cityid = item.Cityid;
                    entity.Stateid = item.Stateid;
                    entity.Countryid = item.Countryid;
                    entity.Createddate = item.Createddate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    list.Add(entity);
                }

            }
            return list;
        }

        public bool deleteaddress(long id)
        {
            bool isdeleted = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ContactAddressMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                isdeleted = true;
            }

            return isdeleted;
        }

        public bool Updateadddress(M_ContactAddressEntity entity)
        {
            bool isupdated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ContactAddressMaster.Find(entity.Id);
                record.Addess = entity.Addess;
                record.Cityid = entity.Cityid;
                record.Stateid = entity.Stateid;
                db.SaveChanges();
                isupdated = true;
            }
            return isupdated;
        }

        public long Insert(M_LedgersEntity entity)
        {
            long id = 0;
            long addId = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_Ledgers item = new M_Ledgers();
                item.Ledger_Id = entity.Ledger_Id;
                item.Ledger_Name = entity.Ledger_Name;
                item.LedgerType_Id = entity.LedgerType_Id;
                item.Ledger_Name = entity.Ledger_Name;
                item.Ledger_Description = entity.Ledger_Description;
                item.Department = entity.Department;
                item.PAN_no = entity.PAN_no;
                item.Is_Active = entity.Is_Active;
                //item.Created_By =Convert.ToInt32(1);
                //New Code Added by Rahul on 09-01-2020
                item.Created_By = entity.Created_By;
                item.Created_Date = entity.Created_Date;
                item.BranchId = entity.Branch_ID;
                item.Company_ID = entity.Company_ID;

                db.M_Ledgers.Add(item);
                db.SaveChanges();
                id = item.Ledger_Id;

                if (id > 0)
                {
                    foreach (var address in entity.M_Ledger_BillingDetails)
                    {
                        M_Ledger_BillingDetails addrecord = new M_Ledger_BillingDetails();
                        //string[] adresssrr = address.Addess.Split('~');

                        addrecord.Ledger_Id =Convert.ToInt32(id);
                        addrecord.Billing_Name = address.Billing_Name;
                        addrecord.Unit = address.Unit;
                        addrecord.Contact_No = address.Contact_No;
                        addrecord.Telephone_no = address.Telephone_no;
                        addrecord.Fax_no = address.Fax_no;
                        addrecord.Email = address.Email;
                        addrecord.GSTIN = address.GSTIN;
                        addrecord.Address = address.Address;
                        addrecord.Country_Id = address.Country_Id;
                        addrecord.State_Id = address.State_Id;
                        addrecord.City_Id = address.City_Id;
                        addrecord.Bank_Name = address.Bank_Name;
                        addrecord.Account_no = address.Account_no;
                        addrecord.Bank_code = address.Bank_code;
                        addrecord.Is_SEZ = address.Is_SEZ;
                        //New Code by Rahul on 03-01-2020
                        addrecord.Company = address.Company;
                        addrecord.Branch = address.Branch;
                        //addrecord.Created_Date = DateTime.Now;

                        //New Code Added by Rahul on 09-01-2020
                        addrecord.Created_By = address.Created_By;
                        addrecord.Created_Date = address.Created_Date;
                        addrecord.Company_ID = address.Company_ID;
                        addrecord.BranchId = address.Branch_ID;

                        addrecord.Is_Active = true;

                        db.M_Ledger_BillingDetails.Add(addrecord);
                        db.SaveChanges();
                       
                    }

                    //foreach(var map in entity.M_CompanyBranch_Mapping)
                    //{
                    //    M_CompanyBranch_Mapping maprecord = new M_CompanyBranch_Mapping();
                    //    //string[] adresssrr = address.Addess.Split('~');

                    //    maprecord.Ledger_Id = Convert.ToInt32(id);
                    //    maprecord.Company_ID = map.Company_ID;
                    //    maprecord.Branch_ID = map.Branch_ID;
                    //    maprecord.Created_Date = DateTime.Now;
                    //    maprecord.Is_Active = true;

                    //    db.M_CompanyBranch_Mapping.Add(maprecord);
                    //    db.SaveChanges();

                    //}
                    if (entity.M_LedgerDocumentMaster != null)
                    {
                        foreach (var docimg in entity.M_LedgerDocumentMaster)
                        {
                            M_LedgerDocumentMaster addrecord = new M_LedgerDocumentMaster();
                            //string[] adresssrr = address.Addess.Split('~');

                            addrecord.LedgerId = id;
                            addrecord.Filename = docimg.Filename;
                            addrecord.Filetype = docimg.Filetype;
                            //addrecord.Createddate = DateTime.Now;
                            //New Code Added by Rahul on 09-01-2020
                            addrecord.CreatedBy = docimg.CreatedBy;
                            addrecord.Createddate = docimg.Createddate;
                            addrecord.BranchId = docimg.Branch_ID;
                            addrecord.Company_ID = docimg.Company_ID;

                            addrecord.IsActive = true;
                            addrecord.Isdelete = false;
                            db.M_LedgerDocumentMaster.Add(addrecord);
                            db.SaveChanges();
                        }
                    }
                }

            }
            return id;
        }
        public bool Update(M_LedgersEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_Ledgers.Where(x => x.Ledger_Id == entity.Ledger_Id).FirstOrDefault();
                record.Ledger_Name = entity.Ledger_Name;
                record.LedgerType_Id = entity.LedgerType_Id;
                record.Department = entity.Department;
                record.PAN_no = entity.PAN_no;
                record.Ledger_Description = entity.Ledger_Description;
                record.Is_Active = entity.Is_Active;
                record.Modified_Date= entity.Modified_Date;
                record.Modified_By = entity.Modified_By;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                try
                {
                    db.SaveChanges();
                    IsUpdated = true;

                    foreach (var address in entity.M_Ledger_BillingDetails)
                    {
                        var addrecord = db.M_Ledger_BillingDetails.Where(x => x.Ledger_Id == entity.Ledger_Id && x.Id== address.Id).FirstOrDefault();
                        if (addrecord != null)
                        {
                            addrecord.Ledger_Id = record.Ledger_Id;
                            addrecord.Billing_Name = address.Billing_Name;
                            addrecord.Unit = address.Unit;
                            addrecord.Contact_No = address.Contact_No;
                            addrecord.Telephone_no = address.Telephone_no;
                            addrecord.Fax_no = address.Fax_no;
                            addrecord.Email = address.Email;
                            addrecord.GSTIN = address.GSTIN;
                            addrecord.Address = address.Address;
                            addrecord.City_Id = address.City_Id;
                            addrecord.State_Id = address.State_Id;
                            addrecord.Country_Id = address.Country_Id;
                            addrecord.Bank_Name = address.Bank_Name;
                            addrecord.Account_no = address.Account_no;
                            addrecord.Bank_code = address.Bank_code;
                            addrecord.Is_SEZ = address.Is_SEZ;

                            //New Code Added by Rahul on 09-01-2020
                            addrecord.Branch = address.Branch;
                            addrecord.Company = address.Company;
                            addrecord.Modified_By = address.Modified_By;
                            addrecord.Modified_Date = address.Modified_Date;
                            addrecord.Company_ID = address.Company_ID;
                            addrecord.BranchId = address.Branch_ID;

                            addrecord.Is_Active = address.Is_Active;
                            db.SaveChanges();
                        }
                        else
                        {
                            M_Ledger_BillingDetails addrecord1 = new M_Ledger_BillingDetails();
                            //string[] adresssrr = address.Addess.Split('~');

                            addrecord1.Ledger_Id = Convert.ToInt32(record.Ledger_Id);
                            addrecord1.Billing_Name = address.Billing_Name;
                            addrecord1.Unit = address.Unit;
                            addrecord1.Contact_No = address.Contact_No;
                            addrecord1.Telephone_no = address.Telephone_no;
                            addrecord1.Fax_no = address.Fax_no;
                            addrecord1.Email = address.Email;
                            addrecord1.GSTIN = address.GSTIN;
                            addrecord1.Address = address.Address;
                            addrecord1.Country_Id = address.Country_Id;
                            addrecord1.State_Id = address.State_Id;
                            addrecord1.City_Id = address.City_Id;
                            addrecord1.Bank_Name = address.Bank_Name;
                            addrecord1.Account_no = address.Account_no;
                            addrecord1.Bank_code = address.Bank_code;
                            addrecord1.Is_SEZ = address.Is_SEZ;

                            //New Code Added by Rahul on 09-01-2020
                            addrecord1.Branch = address.Branch;
                            addrecord1.Company = address.Company;
                            addrecord1.Modified_By = address.Modified_By;
                            addrecord1.Modified_Date = address.Modified_Date;
                            addrecord1.Company_ID = address.Company_ID;
                            addrecord1.BranchId = address.Branch_ID;

                            addrecord1.Created_Date = DateTime.Now;
                            addrecord1.Is_Active = true;

                            db.M_Ledger_BillingDetails.Add(addrecord1);
                            db.SaveChanges();
                        }
                    }

                    if (entity.M_LedgerDocumentMaster != null)
                    {
                        foreach (var docimg in entity.M_LedgerDocumentMaster)
                        {
                            var addrecord = db.M_LedgerDocumentMaster.Where(x => x.LedgerId == entity.Ledger_Id && x.Id == docimg.Id).FirstOrDefault();
                            //string[] adresssrr = address.Addess.Split('~');
                            if (addrecord != null)
                            {
                                addrecord.LedgerId = record.Ledger_Id;
                                addrecord.Filename = docimg.Filename;

                                //New Code Added by Rahul on 09-01-2020
                                addrecord.UpdatedBy = docimg.UpdatedBy;
                                addrecord.Updateddate = docimg.Updateddate;
                                addrecord.Company_ID = docimg.Company_ID;
                                addrecord.BranchId = docimg.Branch_ID;

                                //addrecord.Updateddate = DateTime.Now;
                                addrecord.IsActive = docimg.IsActive;
                                db.SaveChanges();
                            }
                            else
                            {
                                M_LedgerDocumentMaster addrecord2 = new M_LedgerDocumentMaster();
                                //string[] adresssrr = address.Addess.Split('~');

                                addrecord2.LedgerId = Convert.ToInt64(record.Ledger_Id);
                                addrecord2.Filename = docimg.Filename;
                                addrecord2.Createddate = DateTime.Now;
                                addrecord2.CreatedBy = docimg.CreatedBy;
                                addrecord2.Company_ID = docimg.Company_ID;
                                addrecord2.BranchId = docimg.Branch_ID;
                                addrecord2.Filesize = "";
                                addrecord2.IsActive = true;
                                db.M_LedgerDocumentMaster.Add(addrecord2);
                                db.SaveChanges();
                            }
                        }
                    }

                    //if(entity.M_CompanyBranch_Mapping!=null)
                    //{
                    //    foreach (var map in entity.M_CompanyBranch_Mapping)
                    //    {
                    //        var maprecord = db.M_CompanyBranch_Mapping.Where(x => x.Ledger_Id == entity.Ledger_Id).FirstOrDefault();
                    //        if (maprecord != null)
                    //        {
                    //            maprecord.Ledger_Id = Convert.ToInt32(record.Ledger_Id);
                    //            maprecord.Company_ID = map.Company_ID;
                    //            maprecord.Branch_ID = map.Branch_ID;
                    //            maprecord.Created_Date = DateTime.Now;
                    //            maprecord.Is_Active = map.Is_Active;
                    //            db.SaveChanges();
                    //        }
                    //        else
                    //        {
                    //            M_CompanyBranch_Mapping maprecord5 = new M_CompanyBranch_Mapping();
                    //            //string[] adresssrr = address.Addess.Split('~');

                    //            maprecord5.Ledger_Id = Convert.ToInt32(record.Ledger_Id);
                    //            maprecord5.Company_ID = map.Company_ID;
                    //            maprecord5.Branch_ID = map.Branch_ID;
                    //            maprecord5.Created_Date = DateTime.Now;
                    //            maprecord5.Is_Active = true;

                    //            db.M_CompanyBranch_Mapping.Add(maprecord5);
                    //            db.SaveChanges();
                    //        }
                    //    }
                    //}
                    
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return IsUpdated;
        }
        public bool Delete(long id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var contact = db.M_Ledgers.Where(x => x.Ledger_Id == id).FirstOrDefault();
                contact.Is_Active = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
                var contact1 = db.M_Ledger_BillingDetails.Where(x => x.Ledger_Id == id).FirstOrDefault();
                contact1.Is_Active = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }

                var CBmap = db.M_CompanyBranch_Mapping.Where(x => x.Ledger_Id == id).FirstOrDefault();
                CBmap.Is_Active = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }

                var docu = db.M_LedgerDocumentMaster.Where(x => x.LedgerId == id).FirstOrDefault();
                docu.IsActive = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
                return IsDeleted;

            }
        }
        public List<M_Ledger_BillingDetailsEntity> GetAddressById(long custid)
        {
            List<M_Ledger_BillingDetailsEntity> list = new List<M_Ledger_BillingDetailsEntity>();
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 7; //List Of Customer
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = Convert.ToString(custid);
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                foreach (DataTable table in dt.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        M_Ledger_BillingDetailsEntity ledenty = new M_Ledger_BillingDetailsEntity();
                        ledenty.Id = Convert.ToInt32(dr["id"]);
                        ledenty.Address = Convert.ToString(dr["Address"]);
                        ledenty.Contact_No= Convert.ToString(dr["Contact_No"]);
                        ledenty.Billing_Name = Convert.ToString(dr["Billing_Name"]);
                        list.Add(ledenty);
                    }
                }

            }

            return list;
        }
    }
}
