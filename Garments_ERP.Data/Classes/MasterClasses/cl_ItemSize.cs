﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;

namespace Garments_ERP.Data.Admin
{
    public class cl_ItemSize
    {
        public List<M_ItemSizeMasterEntity> get()
        {

            List<M_ItemSizeMasterEntity> list = new List<M_ItemSizeMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ItemSizeMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_ItemSizeMasterEntity entity = new M_ItemSizeMasterEntity();
                    entity.Id = item.Id;
                    entity.Itemid = item.Itemid;
                    if (item.M_ItemMaster != null)
                    {
                        M_ItemMasterEntity itemnameentity = new M_ItemMasterEntity();
                        itemnameentity.Id = item.M_ItemMaster.Id;
                        itemnameentity.ItemName = item.M_ItemMaster.ItemName;
                        entity.M_ItemMaster = itemnameentity;
                    }
                    entity.Styleid = item.Styleid;
                    if (item.M_StyleMaster != null)
                    {
                        M_StyleMasterEntity styleentity = new M_StyleMasterEntity();
                        styleentity.Id = item.M_StyleMaster.Id;
                        styleentity.Stylename = item.M_StyleMaster.Stylename;
                        entity.M_StyleMaster = styleentity;
                    }
                    entity.Sizetype = item.Sizetype;
                    entity.Sizedesc = item.Sizedesc;
                    entity.Createddat = item.Createddat;
                    entity.Createdby = item.Createdby;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    entity.IsActive = item.IsActive;
                    list.Add(entity);
                }
            }
            return list;

        }
        public M_ItemSizeMasterEntity GetById(long id)
        {
            try
            {
                M_ItemSizeMasterEntity entity = new M_ItemSizeMasterEntity();
                using (var db = new GarmentERPDBEntities())
                {
                    var records = db.M_ItemSizeMaster.Where(x => x.Id == id && x.IsActive == true);
                    foreach (var item in records)
                    {
                        entity.Id = item.Id;
                        entity.Styleid = item.Styleid;
                        entity.Itemid = item.Itemid;
                        entity.Sizetype = item.Sizetype;
                        entity.Sizedesc = item.Sizedesc;
                        entity.Createdby = item.Createdby;
                        entity.Createddat = item.Createddat;
                        entity.IsActive = item.IsActive;
                    }
                }
                return entity;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public int Insert(M_ItemSizeMasterEntity entity)
        {
            int id = 0;
            M_ItemSizeMaster record = new M_ItemSizeMaster();
            record.Id = entity.Id;
            record.Itemid = Convert.ToInt32(entity.Itemid);
            record.Styleid = entity.Styleid;
            record.Sizetype = entity.Sizetype;
            record.Sizedesc = entity.Sizedesc;
            record.Createddat = DateTime.Now;
            record.Createdby = entity.Createdby;
            record.IsActive = true;
            using (var db = new GarmentERPDBEntities())
            {
                db.M_ItemSizeMaster.Add(record);
                db.SaveChanges();
                id = Convert.ToInt32(record.Id);
            }


            return id;

        }

        public bool Update(M_ItemSizeMasterEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ItemSizeMaster.Where(x => x.Id == entity.Id).FirstOrDefault();
                record.Id = entity.Id;
                record.Itemid = Convert.ToInt32(entity.Itemid);
                record.Styleid = entity.Styleid;
                record.Sizetype = entity.Sizetype;
                record.Sizedesc = entity.Sizedesc;
                record.Updateddate = entity.Updateddate;
                record.Updatedby = entity.Updatedby;
                record.IsActive = entity.IsActive;
                try
                {
                    db.SaveChanges();
                    IsUpdated = true;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return IsUpdated;
        }
        //Item shape master method's
        public bool Delete(int id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_ItemSizeMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                IsDeleted = true;
            }

            return IsDeleted;

        }

    }
}
