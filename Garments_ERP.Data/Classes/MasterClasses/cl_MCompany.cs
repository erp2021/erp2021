﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;

namespace Garments_ERP.Data.Classes.MasterClasses
{
    public class cl_MCompany
    {
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;

        public List<M_companyEntity> get()
        {
            List<M_companyEntity> list = new List<M_companyEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_company.ToList().Where(x => x.Is_Active == true);
                foreach (var item in records)
                {
                    M_companyEntity entity = new M_companyEntity();
                    entity.Company_ID = item.Company_ID;
                    entity.Company_Name = item.Company_Name;
                    list.Add(entity);
                }
            }
            return list;
        }
        public string Insert(M_companyEntity entity,string password)
        {
            string msg = "";
            string CompnyName = "";
            using (var db = new GarmentERPDBEntities())
            { 
                M_company record = new M_company();
                record.Company_ID = entity.Company_ID;
                CompnyName = entity.Company_Name;
                record.Company_Name = entity.Company_Name;
                record.Legal_Name = entity.Legal_Name;
                record.Mobile_No = entity.Mobile_No;
                record.PhoneNumber = entity.PhoneNumber;
                record.FaxNo = entity.FaxNo;
                record.Company_Address = entity.Company_Address;
                record.Country_Code = entity.Country_Code;
                record.State_Id = entity.State_Id;
                record.City_Id = entity.City_Id;
                record.Pincode = entity.Pincode;
                record.EmailID = entity.EmailID;
                record.Pan_No = entity.Pan_No;
                record.GSTNo = entity.GSTNo;
                record.Website = entity.Website;
                record.Head_Office_Add = entity.Head_Office_Add;
                record.Created_Date = entity.Created_Date;
                record.Created_By = entity.Created_By;
                record.Is_Active = entity.Is_Active;
                record.ParentId = entity.ParentId;
                db.M_company.Add(record);
                db.SaveChanges();
                int id = record.Company_ID;
                if (id > 0)
                {
                    M_UserMaster record1 = new M_UserMaster();
                    record1.Username = entity.UserName;
                    record1.Password = password;
                    record1.Email = entity.EmailID;
                    record1.IsActive = true;
                    record1.Createdby = entity.Created_By;
                    record1.Createddate = DateTime.Now;
                    record1.APassword = "23456";
                    if (entity.ParentId > 0)
                    {
                        record1.Company_ID =Convert.ToInt32(entity.ParentId);
                        record1.BranchId = id;
                    }
                    else
                    {
                        record1.Company_ID = id;
                        record1.BranchId = 0;
                    }
                    db.M_UserMaster.Add(record1);
                    db.SaveChanges();
                    int Cid = record1.Id;
                    
                    if(Cid>0)
                    {
                        M_User_Role Userrole = new M_User_Role();
                        Userrole.roleId = 2;
                        Userrole.UserId = Convert.ToInt64(Cid);
                        Userrole.DepartmentId = 0;
                        if (entity.ParentId > 0)
                        {
                            Userrole.CompanyId = Convert.ToInt32(entity.ParentId);
                            Userrole.BranchId = id;
                        }
                        else
                        {
                            Userrole.CompanyId = id;
                            Userrole.BranchId = 0;
                        }
                        Userrole.CreatedBy = entity.Created_By;
                        Userrole.CreatedOn = DateTime.Now;
                        Userrole.IsActive = true;
                        db.M_User_Role.Add(Userrole);
                        db.SaveChanges();
                        long Rid = Userrole.UserRoleId;
                        if (Rid > 0)
                        {
                            msg = "Your Password :=23456";
                        }
                    }
                }

            }

            return msg;
        }
        public M_companyEntity GetById(long id)
        {
            M_companyEntity record = new M_companyEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.M_company.Find(id);
                record.Company_ID = item.Company_ID;
                record.Company_Name = item.Company_Name;
                record.Legal_Name = item.Legal_Name;
                if (item.ParentId > 0)
                {
                    var userid = db.M_User_Role.Where(x => x.CompanyId == item.ParentId && x.BranchId== item.Company_ID && x.roleId == 2).Select(x => x.UserId).FirstOrDefault();
                    record.UserName = db.M_UserMaster.Where(x => x.Company_ID == item.ParentId && x.BranchId == item.Company_ID && x.Id == userid).Select(x => x.Username).FirstOrDefault();
                }
                else
                {
                    var userid = db.M_User_Role.Where(x => x.CompanyId == item.Company_ID && x.BranchId == 0 && x.roleId == 2).Select(x => x.UserId).FirstOrDefault();
                    record.UserName = db.M_UserMaster.Where(x => x.Company_ID == item.Company_ID && x.BranchId == 0 && x.Id == userid).Select(x => x.Username).FirstOrDefault();
                }
                record.Mobile_No = item.Mobile_No;
                record.PhoneNumber = item.PhoneNumber;
                record.FaxNo = item.FaxNo;
                record.Company_Address = item.Company_Address;
                record.Country_Code = item.Country_Code;
                record.State_Id = item.State_Id;
                record.City_Id = item.City_Id;
                record.Pincode = item.Pincode;
                record.EmailID = item.EmailID;
                record.Pan_No = item.Pan_No;
                record.GSTNo = item.GSTNo;
                record.Website = item.Website;
                record.Created_Date = item.Created_Date;
                record.Created_By = item.Created_By;
                record.Is_Active = item.Is_Active;
                record.Head_Office_Add = item.Head_Office_Add;
                record.ParentId = item.ParentId;
            }

            return record;
        }
        public List<M_companyEntity> getComdetail()
        {
            List<M_companyEntity> list = new List<M_companyEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_company.ToList().Where(x => x.Is_Active == true);
                foreach (var item in records)
                {
                    M_companyEntity entity = new M_companyEntity();
                    entity.Company_ID = item.Company_ID;
                    entity.Company_Name = item.Company_Name;
                    entity.Legal_Name = item.Legal_Name;
                    if (item.ParentId > 0)
                    {
                        var userid = db.M_User_Role.Where(x => x.CompanyId == item.ParentId && x.BranchId == item.Company_ID && x.roleId == 2).Select(x => x.UserId).FirstOrDefault();
                        entity.UserName = db.M_UserMaster.Where(x => x.Company_ID == item.ParentId && x.BranchId == item.Company_ID && x.Id == userid).Select(x => x.Username).FirstOrDefault();
                    }
                    else
                    {
                        var userid = db.M_User_Role.Where(x => x.CompanyId == item.Company_ID && x.BranchId == 0 && x.roleId == 2).Select(x => x.UserId).FirstOrDefault();
                        entity.UserName = db.M_UserMaster.Where(x => x.Company_ID == item.Company_ID && x.BranchId == 0 && x.Id == userid).Select(x => x.Username).FirstOrDefault();
                    }
                    entity.Mobile_No = item.Mobile_No;
                    entity.PhoneNumber = item.PhoneNumber;
                    entity.FaxNo = item.FaxNo;
                    entity.Company_Address = item.Company_Address;
                    entity.Country_Code = item.Country_Code;
                    entity.State_Id = item.State_Id;
                    entity.Pincode = item.Pincode;
                    entity.City_Id = item.City_Id;
                    entity.EmailID = item.EmailID;
                    entity.Pan_No = item.Pan_No;
                    entity.GSTNo = item.GSTNo;
                    entity.Website = item.Website;
                    entity.Head_Office_Add = item.Head_Office_Add;
                    entity.ParentId = item.ParentId;
                    list.Add(entity);
                }
            }
            return list;
        }
        public bool Update(M_companyEntity entity)
        {
            bool updated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_company.Find(entity.Company_ID);
                record.Modified_Date = entity.Modified_Date;
                record.Modified_By = entity.Modified_By;
                record.Company_ID = entity.Company_ID;
                record.Company_Name = entity.Company_Name;
                record.Legal_Name = entity.Legal_Name;
                record.Company_Address = entity.Company_Address;
                record.Head_Office_Add = entity.Head_Office_Add;
                record.Country_Code = entity.Country_Code;
                record.Mobile_No = entity.Mobile_No;
                record.PhoneNumber = entity.PhoneNumber;
                record.EmailID = entity.EmailID;
                record.Pan_No = entity.Pan_No;
                record.State_Id = entity.State_Id;
                record.City_Id = entity.City_Id;
                record.Pincode = entity.Pincode;
                record.Website = entity.Website;
                db.SaveChanges();
                updated = true;

            }
            return updated;

        }

        public List<M_BranchEntity> GetBranchListById(int compid)
        {
            List<M_BranchEntity> list = new List<M_BranchEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_Branch.ToList().Where(x => x.Company_ID == compid && x.Is_Active == true);
                foreach(var item in record)
                {
                    M_BranchEntity entity=new M_BranchEntity();
                    entity.Branch_ID = item.Branch_ID;
                    entity.Branch_Name = item.Branch_Name;
                    list.Add(entity);

                }
            }
            return list;
        }
        public bool Delete(long id)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var contact = db.M_company.Where(x => x.Company_ID == id).FirstOrDefault();
                contact.Is_Active = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }
                var contact1 = db.M_Branch.Where(x => x.Company_ID == id).FirstOrDefault();
                contact1.Is_Active = false;
                try
                {
                    db.SaveChanges();
                    IsDeleted = true;
                }
                catch (Exception)
                {

                    throw;
                }


                return IsDeleted;

            }
        }
    }
}
