﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;

namespace Garments_ERP.Data.Admin
{
    public class cl_Brand
    {
        public List<M_BrandMasterEntity> get()
        {
            List<M_BrandMasterEntity> list = new List<M_BrandMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_BrandMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_BrandMasterEntity entity = new M_BrandMasterEntity();

                    entity.Id = item.Id;
                    entity.Brandname = item.Brandname;
                    entity.Brandshortname = item.Brandshortname;
                    entity.Branddescription = item.Branddescription;
                    entity.Createddate = item.Createddate;
                    entity.Createdby = item.Createdby;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.companyid = item.Company_ID;
                    entity.branchid = item.BranchId;
                    list.Add(entity);
                }

            }
            return list;
        }
        public long Insert(M_BrandMasterEntity entity)
        {
            long id = 0;

            M_BrandMaster record = new M_BrandMaster();
            record.Id = entity.Id;
            record.Brandname = entity.Brandname;
            record.Brandshortname = entity.Brandshortname;
            record.Branddescription = entity.Branddescription;
            //New Code Added by Rahul on 09-01-2020
            record.Createddate = entity.Createddate;            
            record.Createdby = entity.Createdby;
            record.Company_ID = entity.companyid;
            record.BranchId = entity.branchid;

            //record.Updateddate = entity.Updateddate;
            //record.UpdatedBy = entity.UpdatedBy;
            record.IsActive = true;
            using (var db = new GarmentERPDBEntities())
            {
                db.M_BrandMaster.Add(record);
                db.SaveChanges();
                id = record.Id;
            }

            return id;

        }

        public bool CheckBrandNameExistance(string BrandName, int? Id)
        {
            bool result = true;

            using (var db = new GarmentERPDBEntities())
            {
                if (Id == null)
                {
                    var brandname = db.M_BrandMaster.Where(x => x.Brandname == BrandName && x.IsActive == true).FirstOrDefault();
                    if (brandname != null)
                        result = false;
                }
                else
                {
                    var brandname = db.M_BrandMaster.Where(x => x.Brandname == BrandName && x.Id != Id && x.IsActive == true).FirstOrDefault();
                    if (brandname != null)
                        result = false;
                }


            }
            return result;
        }

        public bool Update(M_BrandMasterEntity entity)
        {
            bool isupdated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_BrandMaster.Find(entity.Id);
                record.Brandname = entity.Brandname;
                record.Brandshortname = entity.Brandshortname;
                record.Branddescription = entity.Branddescription;
                //New Code Added by Rahul on 09-01-2020
                record.UpdatedBy = entity.UpdatedBy;
                record.Updateddate = entity.Updateddate;
                record.Company_ID = entity.companyid;
                record.BranchId = entity.branchid;

                db.SaveChanges();
                isupdated = true;
            }

            return isupdated;

        }

        public bool Delete(int id)
        {
            bool isdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_BrandMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                isdelete = true;
            }
            return isdelete;
        }
    }
}
