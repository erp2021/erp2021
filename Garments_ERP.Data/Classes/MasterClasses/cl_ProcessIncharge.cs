﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    
    public class cl_ProcessIncharge
    {
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;

        public bool Update(T_ITEM_TRANSFEREntity entity)
        {
            bool isupdate = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var data = db.T_ITEM_TRANSFER.Find(entity.TRANSFER_ID);
                    data.IsStatus = entity.IsStatus;
                    data.MODIFY_BY = entity.MODIFY_BY;
                    data.MODIFY_ON = entity.MODIFY_ON;
                    db.SaveChanges();
                    isupdate = true;
                    
                    foreach(var item in entity.ITDEntiy)
                    {
                        var itemdata = db.T_ITEM_TRANSFER_DETAIL.Find(item.ID);
                        itemdata.InspectedQty = item.InspectedQty;
                        itemdata.RejectedQty = item.RejectedQty;
                        itemdata.MODIFY_BY = entity.MODIFY_BY;
                        itemdata.MODIFY_ON = entity.MODIFY_ON;
                        itemdata.QC = item.QC;
                        db.SaveChanges();
                        isupdate = true;
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }

            return isupdate;
        }

        //Get By Transfer Details
        public T_ITEM_TRANSFEREntity getbyid(long id)
        {
            try
            {
                T_ITEM_TRANSFEREntity entity = new T_ITEM_TRANSFEREntity();
                using (var db = new GarmentERPDBEntities())
                {
                    var item = db.T_ITEM_TRANSFER.Where(x => x.TRANSFER_ID == id).FirstOrDefault();
                    
                    entity.TRANSFER_ID = item.TRANSFER_ID;
                    entity.INVENTORY_ID = item.INVENTORY_ID;
                    entity.STOCK_TRANSFER_NO = item.STOCK_TRANSFER_NO;
                    entity.STOCK_TRANSFER_DATE = item.STOCK_TRANSFER_DATE;
                    entity.WAREHOUSE_FROM = item.WAREHOUSE_FROM;
                    if (item.WAREHOUSE_FROM != null)
                    {
                        M_WarehouseEntity warehouseent = new M_WarehouseEntity();
                        warehouseent.SHORT_NAME = db.M_WAREHOUSE.Where(x => x.ID == item.WAREHOUSE_FROM).Select(x => x.SHORT_NAME).FirstOrDefault();

                        entity.FromWarehouse = warehouseent;
                    }

                    entity.WAREHOUSE_STORAGE_FROM = item.WAREHOUSE_STORAGE_FROM;
                    entity.WAREHOUSE_TO = item.WAREHOUSE_TO;
                    if (item.WAREHOUSE_TO != null)
                    {
                        M_WarehouseEntity warehouseent = new M_WarehouseEntity();
                        warehouseent.SHORT_NAME = db.M_WAREHOUSE.Where(x => x.ID == item.WAREHOUSE_TO).Select(x => x.SHORT_NAME).FirstOrDefault();
                        entity.ToWarehouse = warehouseent;
                    }
                    entity.MODE_OF_DISPATCH = item.MODE_OF_DISPATCH;
                    entity.DISPATCH_THRU = item.DISPATCH_THRU;
                    entity.DISPATCH_DATE = item.DISPATCH_DATE;
                    entity.TRANSPOTER_NAME = item.TRANSPOTER_NAME;
                    entity.PERSON_NAME = item.PERSON_NAME;
                    entity.SENDER_BY = item.SENDER_BY;
                    if (item.SENDER_BY != null)
                    {
                        EmployeeEntity empent = new EmployeeEntity();
                        empent.Name = db.Employees.Where(x => x.UserId == item.SENDER_BY).Select(x => x.Name).FirstOrDefault();
                        entity.Sender_Employee = empent;
                    }
                    entity.RECEIVED_DATE = item.RECEIVED_DATE;
                    entity.RECEIVED_PERSON = item.RECEIVED_PERSON;
                    entity.INVENTORY_MODE = item.INVENTORY_MODE;
                    entity.WO_ID = item.WO_ID;
                    if (item.WO_ID != null)
                    {
                        MFG_WORK_ORDEREntity WOent = new MFG_WORK_ORDEREntity();
                        WOent.WorkOrderNo = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == item.WO_ID).Select(x => x.WorkOrderNo).FirstOrDefault();
                        WOent.Date = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == item.WO_ID).Select(x => x.Date).FirstOrDefault();
                        entity.WorkOrder = WOent;
                    }
                    entity.CREATED_BY = item.CREATED_BY;
                    entity.CREATED_ON = item.CREATED_ON;
                    entity.MODIFY_BY = item.MODIFY_BY;
                    entity.MODIFY_ON = item.MODIFY_ON;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsStatus = item.IsStatus;
                    List<T_ITEM_TRANSFER_DETAILEntity> TransferEnt = new List<T_ITEM_TRANSFER_DETAILEntity>();
                    var itemdata = db.T_ITEM_TRANSFER_DETAIL.Where(x => x.TRANSFER_ID == item.TRANSFER_ID).ToList();
                    foreach(var dt in itemdata)
                    {
                        T_ITEM_TRANSFER_DETAILEntity ent = new T_ITEM_TRANSFER_DETAILEntity();
                        ent.ID = dt.ID;
                        ent.TRANSFER_ID = dt.TRANSFER_ID;
                        ent.ITEM_ID = dt.ITEM_ID;

                        M_ItemMasterEntity itement = new M_ItemMasterEntity();
                        int ISid =(int) db.M_ItemMaster.Where(x => x.Id == dt.ITEM_ID).Select(x => x.Itemsubcategoryid).FirstOrDefault();
                        cl_BillOfMaterialDetail bomobj = new cl_BillOfMaterialDetail();
                        itement.ItemName = bomobj.getItemName((long)dt.ITEM_ID,(int)dt.ITEM_ATTRIBUTE_ID, ISid);
                        
                        ent.itement = itement;

                        ent.TRANSFER_QTY = dt.TRANSFER_QTY;
                        ent.WAREHOUSE_STORAGE_TO = dt.WAREHOUSE_STORAGE_TO;
                        ent.BATCH_NO_ID = dt.BATCH_NO_ID;

                        M_Product_InfoEntity batch = new M_Product_InfoEntity();
                        batch.Dynamic_Controls_value = db.M_Product_Info.Where(x => x.Dynamic_Controls_Id == 4 && x.ItemInward_ID == dt.BATCH_NO_ID).Select(x => x.Dynamic_Controls_value).FirstOrDefault();
                        ent.BatchInfo = batch;

                        M_Product_InfoEntity batch1 = new M_Product_InfoEntity();
                        batch1.Dynamic_Controls_value = db.M_Product_Info.Where(x => x.Dynamic_Controls_Id == 1 && x.ItemInward_ID == dt.BATCH_NO_ID).Select(x => x.Dynamic_Controls_value).FirstOrDefault();
                        ent.BatchInfo1 = batch1;


                        ent.UNIT_ID = dt.UNIT_ID;
                        M_UnitMasterEntity unit = new M_UnitMasterEntity();
                        unit.ItemUnit = db.M_UnitMaster.Where(x => x.Id == dt.UNIT_ID).Select(x => x.ItemUnit).FirstOrDefault();
                        ent.unit = unit;

                        ent.InspectedQty = dt.InspectedQty != null ? dt.InspectedQty : 0;
                        ent.RejectedQty = dt.RejectedQty != null ? dt.RejectedQty : 0;
                        ent.ITEM_ATTRIBUTE_ID = dt.ITEM_ATTRIBUTE_ID;
                        ent.QC = dt.QC;
                        ent.CREATED_BY = dt.CREATED_BY;
                        ent.CREATED_ON = dt.CREATED_ON;
                        ent.MODIFY_BY = dt.MODIFY_BY;
                        ent.MODIFY_ON = dt.MODIFY_ON;
                        ent.Company_ID = dt.Company_ID;
                        ent.BranchId = dt.BranchId;
                        TransferEnt.Add(ent);
                    }
                    entity.ITDEntiy = TransferEnt;
                }

                return entity;

            }
            catch (Exception)
            {

                throw;
            }

        }

        //List Of Transfer Material With Status and User Wise
        public List<T_ITEM_TRANSFEREntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<T_ITEM_TRANSFEREntity> list = new List<T_ITEM_TRANSFEREntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.T_ITEM_TRANSFER.Where(x => x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.RECEIVED_PERSON == Userid).ToList();
                //if (records.Count == 0 && roleid != 4)
                //{
                //    string str = "select TRANSFER_ID,INVENTORY_ID,STOCK_TRANSFER_NO,STOCK_TRANSFER_DATE,WAREHOUSE_FROM,WAREHOUSE_STORAGE_FROM,WAREHOUSE_TO,MODE_OF_DISPATCH,DISPATCH_THRU,DISPATCH_DATE, ";
                //    str = str + "TRANSPOTER_NAME,PERSON_NAME,SENDER_BY,RECEIVED_DATE,RECEIVED_PERSON,INVENTORY_MODE,WO_ID,CREATED_BY,CREATED_ON,MODIFY_BY,MODIFY_ON,Company_ID,BranchId,IsStatus";
                //    str = str + " from T_ITEM_TRANSFER where  IsStatus=" + IsStatus + " and Company_ID=" + Company_ID + " and BranchId=" + BranchId + " and CreatedBy in(select UserId from M_User_Role where roleId=4)";
                //    records = db.T_ITEM_TRANSFER.SqlQuery(str).ToList();
                //}
               
                foreach (var item in records)
                {
                    T_ITEM_TRANSFEREntity entity = new T_ITEM_TRANSFEREntity();
                    entity.TRANSFER_ID = item.TRANSFER_ID;
                    entity.INVENTORY_ID = item.INVENTORY_ID;
                    entity.STOCK_TRANSFER_NO = item.STOCK_TRANSFER_NO;
                    entity.STOCK_TRANSFER_DATE = item.STOCK_TRANSFER_DATE;
                    entity.WAREHOUSE_FROM = item.WAREHOUSE_FROM;
                    if (item.WAREHOUSE_FROM != null)
                    {
                        M_WarehouseEntity warehouseent = new M_WarehouseEntity();
                        warehouseent.SHORT_NAME = db.M_WAREHOUSE.Where(x => x.ID == item.WAREHOUSE_FROM).Select(x => x.SHORT_NAME).FirstOrDefault();

                        entity.FromWarehouse = warehouseent;
                    }

                    entity.WAREHOUSE_STORAGE_FROM = item.WAREHOUSE_STORAGE_FROM;
                    entity.WAREHOUSE_TO = item.WAREHOUSE_TO;
                    if (item.WAREHOUSE_TO != null)
                    {
                        M_WarehouseEntity warehouseent = new M_WarehouseEntity();
                        warehouseent.SHORT_NAME = db.M_WAREHOUSE.Where(x => x.ID == item.WAREHOUSE_TO).Select(x => x.SHORT_NAME).FirstOrDefault();
                        entity.ToWarehouse = warehouseent;
                    }
                    entity.MODE_OF_DISPATCH = item.MODE_OF_DISPATCH;
                    entity.DISPATCH_THRU = item.DISPATCH_THRU;
                    entity.DISPATCH_DATE = item.DISPATCH_DATE;
                    entity.TRANSPOTER_NAME = item.TRANSPOTER_NAME;
                    entity.PERSON_NAME = item.PERSON_NAME;
                    entity.SENDER_BY = item.SENDER_BY;
                    if (item.SENDER_BY != null)
                    {
                        EmployeeEntity empent = new EmployeeEntity();
                        empent.Name = db.Employees.Where(x => x.UserId == item.SENDER_BY).Select(x => x.Name).FirstOrDefault();
                        entity.Sender_Employee = empent;
                    }
                    entity.RECEIVED_DATE = item.RECEIVED_DATE;
                    entity.RECEIVED_PERSON = item.RECEIVED_PERSON;
                    entity.INVENTORY_MODE = item.INVENTORY_MODE;
                    entity.WO_ID = item.WO_ID;
                    if (item.WO_ID != null)
                    {
                        MFG_WORK_ORDEREntity WOent = new MFG_WORK_ORDEREntity();
                        WOent.WorkOrderNo = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == item.WO_ID).Select(x => x.WorkOrderNo).FirstOrDefault();
                        entity.WorkOrder = WOent;
                    }
                    entity.CREATED_BY = item.CREATED_BY;
                    entity.CREATED_ON = item.CREATED_ON;
                    entity.MODIFY_BY = item.MODIFY_BY;
                    entity.MODIFY_ON = item.MODIFY_ON;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsStatus = item.IsStatus;
                    list.Add(entity);

                }
            }

            return list;

        }


        ////List Of Transfer Material With Status and User and Form Date And To Date Wise
        public List<T_ITEM_TRANSFEREntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<T_ITEM_TRANSFEREntity> list = new List<T_ITEM_TRANSFEREntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.T_ITEM_TRANSFER.Where(x => x.STOCK_TRANSFER_DATE >= from && x.STOCK_TRANSFER_DATE <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.RECEIVED_PERSON == Userid).ToList();
                //if (records.Count == 0 && roleid != 4)
                //{
                //    string str = "select TRANSFER_ID,INVENTORY_ID,STOCK_TRANSFER_NO,STOCK_TRANSFER_DATE,WAREHOUSE_FROM,WAREHOUSE_STORAGE_FROM,WAREHOUSE_TO,MODE_OF_DISPATCH,DISPATCH_THRU,DISPATCH_DATE, ";
                //    str = str + "TRANSPOTER_NAME,PERSON_NAME,SENDER_BY,RECEIVED_DATE,RECEIVED_PERSON,INVENTORY_MODE,WO_ID,CREATED_BY,CREATED_ON,MODIFY_BY,MODIFY_ON,Company_ID,BranchId,IsStatus";
                //    str = str + " from T_ITEM_TRANSFER where STOCK_TRANSFER_DATE >= convert(datetime,'" + from + "',103) and STOCK_TRANSFER_DATE <=convert(datetime,'" + to + "',103)  and IsStatus=" + IsStatus + " and Company_ID=" + Company_ID + " and BranchId=" + BranchId + "";
                //    records = db.T_ITEM_TRANSFER.SqlQuery(str).ToList();
                //}
                foreach (var item in records)
                {
                    T_ITEM_TRANSFEREntity entity = new T_ITEM_TRANSFEREntity();
                    entity.TRANSFER_ID = item.TRANSFER_ID;
                    entity.INVENTORY_ID = item.INVENTORY_ID;
                    entity.STOCK_TRANSFER_NO = item.STOCK_TRANSFER_NO;
                    entity.STOCK_TRANSFER_DATE = item.STOCK_TRANSFER_DATE;
                    entity.WAREHOUSE_FROM = item.WAREHOUSE_FROM;
                    if (item.WAREHOUSE_FROM != null)
                    {
                        M_WarehouseEntity warehouseent = new M_WarehouseEntity();
                        warehouseent.SHORT_NAME = db.M_WAREHOUSE.Where(x => x.ID == item.WAREHOUSE_FROM).Select(x => x.SHORT_NAME).FirstOrDefault();

                        entity.FromWarehouse = warehouseent;
                    }

                    entity.WAREHOUSE_STORAGE_FROM = item.WAREHOUSE_STORAGE_FROM;
                    entity.WAREHOUSE_TO = item.WAREHOUSE_TO;
                    if (item.WAREHOUSE_TO != null)
                    {
                        M_WarehouseEntity warehouseent = new M_WarehouseEntity();
                        warehouseent.SHORT_NAME = db.M_WAREHOUSE.Where(x => x.ID == item.WAREHOUSE_TO).Select(x => x.SHORT_NAME).FirstOrDefault();
                        entity.ToWarehouse = warehouseent;
                    }
                    entity.MODE_OF_DISPATCH = item.MODE_OF_DISPATCH;
                    entity.DISPATCH_THRU = item.DISPATCH_THRU;
                    entity.DISPATCH_DATE = item.DISPATCH_DATE;
                    entity.TRANSPOTER_NAME = item.TRANSPOTER_NAME;
                    entity.PERSON_NAME = item.PERSON_NAME;
                    entity.SENDER_BY = item.SENDER_BY;
                    if (item.SENDER_BY != null)
                    {
                        EmployeeEntity empent = new EmployeeEntity();
                        empent.Name = db.Employees.Where(x => x.UserId == item.SENDER_BY).Select(x => x.Name).FirstOrDefault();
                        entity.Sender_Employee = empent;
                    }
                    entity.RECEIVED_DATE = item.RECEIVED_DATE;
                    entity.RECEIVED_PERSON = item.RECEIVED_PERSON;
                    entity.INVENTORY_MODE = item.INVENTORY_MODE;
                    entity.WO_ID = item.WO_ID;
                    if (item.WO_ID != null)
                    {
                        MFG_WORK_ORDEREntity WOent = new MFG_WORK_ORDEREntity();
                        WOent.WorkOrderNo = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == item.WO_ID).Select(x => x.WorkOrderNo).FirstOrDefault();
                        entity.WorkOrder = WOent;
                    }
                    entity.CREATED_BY = item.CREATED_BY;
                    entity.CREATED_ON = item.CREATED_ON;
                    entity.MODIFY_BY = item.MODIFY_BY;
                    entity.MODIFY_ON = item.MODIFY_ON;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsStatus = item.IsStatus;
                    list.Add(entity);

                }
            }
            return list;
        }

        //Reject Material to update Status
        public bool RejectedMaterial(long id, int Uid)
        {
            bool isdeleted = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.T_ITEM_TRANSFER.Find(id);
                    record.IsStatus = 5;
                    record.MODIFY_ON = DateTime.Now;
                    record.MODIFY_BY = Convert.ToString(Uid);
                    db.SaveChanges();
                    isdeleted = true;
                    var inventoryId = db.T_ITEM_TRANSFER.Where(x => x.TRANSFER_ID == id).Select(x => x.INVENTORY_ID).FirstOrDefault();
                    var Materildata = db.T_ITEM_TRANSFER_DETAIL.Where(x => x.TRANSFER_ID == id).ToList();
                    foreach(var dt in Materildata)
                    {
                        var outid = db.M_ItemOutwardMaster.Where(x => x.Inward_ID == inventoryId && x.Item_ID == dt.ITEM_ID && x.Item_Attribute_ID == dt.ITEM_ATTRIBUTE_ID).Select(x => x.ID);
                        var rec = db.M_ItemOutwardMaster.Find(outid);
                        rec.IsStatus = 5;
                        rec.UpdatedDate = DateTime.Now;
                        rec.UpdatedBy = Convert.ToInt32(Uid);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isdeleted;
        }


        public List<T_ITEM_TRANSFEREntity> get()
        {
            List<T_ITEM_TRANSFEREntity> list = new List<T_ITEM_TRANSFEREntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.T_ITEM_TRANSFER.ToList();
                foreach (var item in records)
                {
                    T_ITEM_TRANSFEREntity entity = new T_ITEM_TRANSFEREntity();
                    entity.TRANSFER_ID = item.TRANSFER_ID;
                    entity.INVENTORY_ID = item.INVENTORY_ID;
                    entity.STOCK_TRANSFER_NO = item.STOCK_TRANSFER_NO;
                    entity.STOCK_TRANSFER_DATE = item.STOCK_TRANSFER_DATE;
                    entity.WAREHOUSE_FROM = item.WAREHOUSE_FROM;
                    if(item.WAREHOUSE_FROM != null)
                    {
                        M_WarehouseEntity warehouseent = new M_WarehouseEntity();
                        warehouseent.SHORT_NAME = db.M_WAREHOUSE.Where(x => x.ID == item.WAREHOUSE_FROM).Select(x => x.SHORT_NAME).FirstOrDefault();
                        
                        entity.FromWarehouse = warehouseent;
                    }

                    entity.WAREHOUSE_STORAGE_FROM = item.WAREHOUSE_STORAGE_FROM;
                    entity.WAREHOUSE_TO = item.WAREHOUSE_TO;
                    if (item.WAREHOUSE_TO != null)
                    {
                        M_WarehouseEntity warehouseent = new M_WarehouseEntity();
                        warehouseent.SHORT_NAME = db.M_WAREHOUSE.Where(x => x.ID == item.WAREHOUSE_TO).Select(x => x.SHORT_NAME).FirstOrDefault();
                        entity.ToWarehouse = warehouseent;
                    }
                    entity.MODE_OF_DISPATCH = item.MODE_OF_DISPATCH;
                    entity.DISPATCH_THRU = item.DISPATCH_THRU;
                    entity.DISPATCH_DATE = item.DISPATCH_DATE;
                    entity.TRANSPOTER_NAME = item.TRANSPOTER_NAME;
                    entity.PERSON_NAME = item.PERSON_NAME;
                    entity.SENDER_BY = item.SENDER_BY;
                    if (item.SENDER_BY != null)
                    {
                        EmployeeEntity empent = new EmployeeEntity();
                        empent.Name = db.Employees.Where(x => x.ID == item.SENDER_BY).Select(x => x.Name).FirstOrDefault();
                        entity.Sender_Employee = empent;
                    }
                    entity.RECEIVED_DATE = item.RECEIVED_DATE;
                    entity.RECEIVED_PERSON = item.RECEIVED_PERSON;
                    entity.INVENTORY_MODE = item.INVENTORY_MODE;
                    entity.WO_ID = item.WO_ID;
                    if (item.WO_ID != null)
                    {
                        MFG_WORK_ORDEREntity WOent = new MFG_WORK_ORDEREntity();
                        WOent.WorkOrderNo = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == item.WO_ID).Select(x => x.WorkOrderNo).FirstOrDefault();
                        entity.WorkOrder = WOent;
                    }
                    entity.CREATED_BY = item.CREATED_BY;
                    entity.CREATED_ON = item.CREATED_ON;
                    entity.MODIFY_BY = item.MODIFY_BY;
                    entity.MODIFY_ON = item.MODIFY_ON;
                    list.Add(entity);

                }
            }

            return list;

        }

    }
}
