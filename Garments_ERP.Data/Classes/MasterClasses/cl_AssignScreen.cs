﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_AssignScreen
    {

        public List<M_ScreeRoleEntity> get()
        {

            List<M_ScreeRoleEntity> list = new List<M_ScreeRoleEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_Screen_Role.ToList();
                foreach (var item in records)
                {
                    M_ScreeRoleEntity entity = new M_ScreeRoleEntity();
                    entity.screenRoleId = item.screenRoleId;
                    entity.ParentScreenId = item.ParentScreenId;
                    M_ScreenEntity Scren1 = new M_ScreenEntity();
                    Scren1.ScreenName = db.M_Screen.Where(x => x.screenId == item.ParentScreenId && x.ParentId == 0).Select(x => x.ScreenName).FirstOrDefault();
                    entity.ParentScrEnt = Scren1;
                    entity.ChildScreenId = item.ChildScreenId;
                    M_ScreenEntity Scren2 = new M_ScreenEntity();
                    Scren2.ScreenName = db.M_Screen.Where(x => x.screenId == item.ChildScreenId && x.ParentId == item.ParentScreenId).Select(x => x.ScreenName).FirstOrDefault();
                    entity.ChildScrEnt = Scren2;
                    entity.RoleId = item.RoleId;
                    M_RoleEntity roleent = new M_RoleEntity();
                    roleent.roleName = db.M_Role.Where(x => x.roleId == item.RoleId).Select(x => x.roleName).FirstOrDefault();
                    entity.RoleEnt = roleent;
                    entity.CreatedOn = item.CreatedOn;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdateOn = item.UpdateOn;
                    entity.UpdateBy = item.UpdateBy;
                    entity.IsActive = item.IsActive;
                    list.Add(entity);
                }
            }
            return list;
        }

        public List<M_ScreenEntity> GetScreenData(long RoleId)
        {
            List<M_ScreenEntity> list = new List<M_ScreenEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_Screen.ToList().Where(x=>x.ParentId==0 && x.IsActive==true && x.IsSub==0).OrderBy(x => x.Sequence);
                foreach (var item in records)
                {
                    M_ScreenEntity entity = new M_ScreenEntity();
                    entity.screenId = item.screenId;
                    entity.ScreenName = item.ScreenName;
                    entity.Discription = item.Discription;
                    entity.URL = item.URL;
                    entity.CreatedOn = item.CreatedOn;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdateOn = item.UpdateOn;
                    entity.UpdateBy = item.UpdateBy;
                    entity.IsActive = item.IsActive;
                    entity.ParentId = item.ParentId;
                    entity.FAIcon = item.FAIcon;
                    entity.IsSub = item.IsSub;
                    //First Sub Screen List
                    List<M_ScreenEntity> ScrEntList = new List<M_ScreenEntity>();
                    var subscrList = db.M_Screen.Where(x => x.ParentId == item.screenId && x.ParentId != 0 && x.IsSub == 1 && x.IsActive == true).ToList().OrderBy(x => x.Sequence);
                    var status = false;
                    if (subscrList != null)
                    {
                        foreach (var data in subscrList)
                        {
                            M_ScreenEntity Scrent = new M_ScreenEntity();
                            Scrent.screenId = data.screenId;
                            Scrent.ScreenName = data.ScreenName;
                            Scrent.Discription = data.Discription;
                            Scrent.URL = data.URL;
                            Scrent.FAIcon = data.FAIcon;
                            Scrent.IsSub = data.IsSub;
                            if (RoleId > 0)
                            {
                                var id = db.M_Screen_Role.Where(x => x.RoleId == RoleId && x.ChildScreenId == data.screenId).Select(x => x.IsActive).FirstOrDefault();
                                Scrent.CHKsatus = id;
                                if (id == true)
                                {
                                    status = true;
                                }
                            }
                            else
                            {
                                Scrent.CHKsatus = false;
                            }

                            //Second Sub Screen List Start
                            List<M_ScreenEntity> ScrSubEntList = new List<M_ScreenEntity>();
                            var subscrList1 = db.M_Screen.Where(x => x.ParentId == data.screenId && x.ParentId != 0 && x.IsSub == 2 && x.IsActive == true).ToList().OrderBy(x => x.Sequence);
                            var status1 = false;
                            if (subscrList1 != null)
                            {
                                foreach (var subdata in subscrList1)
                                {
                                    M_ScreenEntity subScrent = new M_ScreenEntity();
                                    subScrent.screenId = subdata.screenId;
                                    subScrent.ScreenName = subdata.ScreenName;
                                    subScrent.Discription = subdata.Discription;
                                    subScrent.URL = subdata.URL;
                                    subScrent.FAIcon = subdata.FAIcon;
                                    subScrent.IsSub = subdata.IsSub;
                                    if (RoleId > 0)
                                    {
                                        var id = db.M_Screen_Role.Where(x => x.RoleId == RoleId && x.ChildScreenId == subdata.screenId).Select(x => x.IsActive).FirstOrDefault();
                                        subScrent.CHKsatus = id;
                                        if (id == true)
                                        {
                                            status1 = true;
                                        }
                                    }
                                    else
                                    {
                                        subScrent.CHKsatus = false;
                                    }
                                    ScrSubEntList.Add(subScrent);
                                }

                            }

                            Scrent.ScreenList = ScrSubEntList;
                            //Second Sub Screen List END

                            ScrEntList.Add(Scrent);
                        }

                    }
                    entity.CHKsatus = status;
                    entity.ScreenList = ScrEntList;
                    list.Add(entity);
                }
            }
            return list;
        }

        public bool Insert(string MainScr, string SubScr,long roleid)
        {
            bool status = false;
            using (var db = new GarmentERPDBEntities())
            {
                var mainstr = Convert.ToInt64(MainScr);
                var record = db.M_Screen_Role.Where(x => x.RoleId == roleid && x.ParentScreenId == mainstr).ToList();
                if (record.Count > 0)
                {
                    foreach (var data in record)
                    {
                        data.IsActive = false;
                        data.UpdateOn = DateTime.Now;
                        db.SaveChanges();
                    }
                    var SubScreenData = SubScr.Split(',');
                    foreach (var subdata in SubScreenData)
                    {
                        if (subdata != "")
                        {
                            int indof = subdata.IndexOf('^');
                            if (indof > 0)
                            {
                                var SubScrData = subdata.Split('^');
                                long prntId = Convert.ToInt64(SubScrData[0]);
                                
                                var SUbScrdata2 = SubScrData[1].Split('~');
                                foreach (var subd in SUbScrdata2)
                                {
                                    if (subd != "")
                                    {
                                        var childId = Convert.ToInt64(subd);
                                        var recordData = db.M_Screen_Role.Where(x => x.RoleId == roleid && x.ParentScreenId == prntId && x.ChildScreenId == childId).FirstOrDefault();
                                        if (recordData != null)
                                        {
                                            recordData.IsActive = true;
                                            recordData.UpdateOn = DateTime.Now;
                                            db.SaveChanges();
                                            status = true;
                                        }
                                        else
                                        {

                                            M_Screen_Role roleScreen = new M_Screen_Role();
                                            roleScreen.ParentScreenId = prntId;
                                            roleScreen.ChildScreenId = childId;
                                            roleScreen.RoleId = roleid;
                                            roleScreen.IsActive = true;
                                            roleScreen.CreatedOn = DateTime.Now;
                                            db.M_Screen_Role.Add(roleScreen);
                                            db.SaveChanges();
                                            status = true;
                                        }
                                    }
                                }
                                var recordData1 = db.M_Screen_Role.Where(x => x.RoleId == roleid && x.ParentScreenId == mainstr && x.ChildScreenId == prntId).FirstOrDefault();
                                if (recordData1 != null)
                                {
                                    recordData1.IsActive = true;
                                    recordData1.UpdateOn = DateTime.Now;
                                    db.SaveChanges();
                                    status = true;
                                }
                                else
                                {
                                    M_Screen_Role RoleScreen = new M_Screen_Role();
                                    RoleScreen.ParentScreenId = mainstr;
                                    RoleScreen.ChildScreenId = prntId;
                                    RoleScreen.RoleId = roleid;
                                    RoleScreen.IsActive = true;
                                    RoleScreen.CreatedOn = DateTime.Now;
                                    db.M_Screen_Role.Add(RoleScreen);
                                    db.SaveChanges();
                                    status = true;
                                }
                            }
                            else
                            {

                                var childId = Convert.ToInt64(subdata);
                                var recordData = db.M_Screen_Role.Where(x => x.RoleId == roleid && x.ParentScreenId == mainstr && x.ChildScreenId == childId).FirstOrDefault();
                                if (recordData != null)
                                {
                                    recordData.IsActive = true;
                                    recordData.UpdateOn = DateTime.Now;
                                    db.SaveChanges();
                                    status = true;
                                }
                                else
                                {
                                    M_Screen_Role roleScreen = new M_Screen_Role();
                                    roleScreen.ParentScreenId = mainstr;
                                    roleScreen.ChildScreenId = childId;
                                    roleScreen.RoleId = roleid;
                                    roleScreen.IsActive = true;
                                    roleScreen.CreatedOn = DateTime.Now;
                                    db.M_Screen_Role.Add(roleScreen);
                                    db.SaveChanges();
                                    status = true;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var SubScreenData = SubScr.Split(',');
                    foreach (var data in SubScreenData)
                    {
                        if (data != "")
                        {
                            int indof = data.IndexOf('^');

                            if (indof > 0)
                            {
                                var SubScrData = data.Split('^');
                                long prntId = Convert.ToInt64(SubScrData[0]);
                                var SUbScrdata2 = SubScrData[1].Split('~');
                                foreach (var subd in SUbScrdata2)
                                {
                                    if (subd != "")
                                    {
                                        var childId = Convert.ToInt64(subd);
                                        M_Screen_Role RoleScreen = new M_Screen_Role();
                                        RoleScreen.ParentScreenId = prntId;
                                        RoleScreen.ChildScreenId = childId;
                                        RoleScreen.RoleId = roleid;
                                        RoleScreen.IsActive = true;
                                        RoleScreen.CreatedOn = DateTime.Now;
                                        db.M_Screen_Role.Add(RoleScreen);
                                        db.SaveChanges();
                                        status = true;
                                    }
                                }
                            }
                            else
                            {
                                var childSrcId = Convert.ToInt64(data);
                                M_Screen_Role RoleScreen = new M_Screen_Role();
                                RoleScreen.ParentScreenId = mainstr;
                                RoleScreen.ChildScreenId = childSrcId;
                                RoleScreen.RoleId = roleid;
                                RoleScreen.IsActive = true;
                                RoleScreen.CreatedOn = DateTime.Now;
                                db.M_Screen_Role.Add(RoleScreen);
                                db.SaveChanges();
                                status = true;
                            }
                        }
                    }


                }
            }

            return status;
        }

        public bool ActiveDeactive(int id, string Status)
        {
            var isstatus = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_Screen_Role.Find(id);
                record.IsActive = Status == "Deactive" ? false : true;
                db.SaveChanges();
                isstatus = true;
            }
            return isstatus;
        }
    }

}
