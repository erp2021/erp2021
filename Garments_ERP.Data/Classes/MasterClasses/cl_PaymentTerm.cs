﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_PaymentTerm
    {
        public List<M_PaymentTermMasterEntity> get()
        {
            List<M_PaymentTermMasterEntity> list = new List<M_PaymentTermMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_PaymentTermMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_PaymentTermMasterEntity entity = new M_PaymentTermMasterEntity();

                    entity.Id = item.Id;
                    entity.PaymentTerm = item.PaymentTerm;
                    entity.PaymenTermdesc = item.PaymenTermdesc;
                    entity.Createdate = item.Createdate;
                    entity.Createdby = item.Createdby;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    entity.IsActive = item.IsActive;
                    list.Add(entity);
                }

            }
            return list;
        }
        public int Insert(M_PaymentTermMasterEntity entity)
        {
            int id = 0;

            M_PaymentTermMaster record = new M_PaymentTermMaster();
            record.Id = entity.Id;
            record.PaymentTerm = entity.PaymentTerm;
            record.PaymenTermdesc = entity.PaymenTermdesc;
            record.Createdate = DateTime.Now;
            record.Createdby = entity.Createdby;

            //New Code Added by Rahul on 09-01-2020
            record.Company_ID = entity.Company_ID;
            record.BranchId = entity.Branch_ID;

            record.IsActive = true;
            using (var db = new GarmentERPDBEntities())
            {
                db.M_PaymentTermMaster.Add(record);
                db.SaveChanges();
                id = record.Id;
            }


            return id;

        }
        public bool CheckPaymentTermExistance(string paymentterm, int? Id)
        {
            bool result = true;

            using (var db = new GarmentERPDBEntities())
            {
                if (Id == null)
                {
                    var PaymentTerm = db.M_PaymentTermMaster.Where(x => x.PaymentTerm == paymentterm && x.IsActive == true).FirstOrDefault();
                    if (PaymentTerm != null)
                        result = false;
                }
                else
                {
                    var PaymentTerm = db.M_PaymentTermMaster.Where(x => x.PaymentTerm == paymentterm && x.Id != Id && x.IsActive == true).FirstOrDefault();
                    if (PaymentTerm != null)
                        result = false;
                }


            }
            return result;
        }
        public bool Update(M_PaymentTermMasterEntity entity)
        {
            bool isupdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_PaymentTermMaster.Find(entity.Id);
                record.PaymentTerm = entity.PaymentTerm;
                record.PaymenTermdesc = entity.PaymenTermdesc;
                record.Updatedby = entity.Updatedby;
                record.Updateddate = DateTime.Now;


                //New Code Added by Rahul on 09-01-2020
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.Branch_ID;

                db.SaveChanges();
                isupdated = true;
            }
            return isupdated;
        }
        public bool Delete(int id)
        {
            bool isdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_PaymentTermMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                isdelete = true;
            }
            return isdelete;
        }
    }
}
