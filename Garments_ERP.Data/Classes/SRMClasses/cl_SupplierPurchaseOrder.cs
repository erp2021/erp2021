﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.CommonEntities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Garments_ERP.Data.Admin
{
    public class cl_SupplierPurchaseOrder
    {
        cl_role rlobj = new cl_role();
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;

        public string getnextPONo()
        {
            string no;
            no = "SPO-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var rec = db.SRM_PurchaseOrderMaster.Count() > 0 ? db.SRM_PurchaseOrderMaster.Max(x => x.Id) + 1 : 1;
                no = no + "-" + rec.ToString();
            }

            return no;
        }


        //Reject Supplier PO to update Status
        public bool RejectedPO(long id, int Uid)
        {
            bool isdeleted = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.SRM_PurchaseOrderMaster.Find(id);

                    record.IsStatus = 5;
                    record.Updateddate = DateTime.Now;
                    record.Updatedby = Uid;
                    db.SaveChanges();
                    isdeleted = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return isdeleted;
        }

        //List Of PO With Status and User Wise
        public List<SRM_PurchaseOrderMasterEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<SRM_PurchaseOrderMasterEntity> list = new List<SRM_PurchaseOrderMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_PurchaseOrderMaster.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.Craetedby == Userid).OrderByDescending(x=>x.Id).ToList();
                if (records.Count >= 0 && roleid != 4)
                {
                    records = db.SRM_PurchaseOrderMaster.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).OrderByDescending(x => x.Id).ToList();            
                }

                foreach (var item in records)
                {

                    SRM_PurchaseOrderMasterEntity entity = new SRM_PurchaseOrderMasterEntity();
                    entity.Id = item.Id;
                    entity.PO_NO = item.PO_NO;
                    entity.PO_Date = item.PO_Date;
                    // entity.POType = item.POType;
                    entity.QuotationId = item.QuotationId;
                    if (item.SRM_QuotationMaster != null)
                    {
                        SRM_QuotationMasterEntity quotationentity = new SRM_QuotationMasterEntity();
                        quotationentity.Id = item.SRM_QuotationMaster.Id;
                        quotationentity.QuotationNo = item.SRM_QuotationMaster.QuotationNo;
                        quotationentity.QuotationDate = item.SRM_QuotationMaster.QuotationDate;
                        quotationentity.SupplierId = item.SRM_QuotationMaster.SupplierId;
                        entity.SRM_QuotationMaster = quotationentity;
                    }
                    entity.QuotationDate = item.QuotationDate;
                    entity.ContactPersonId = item.ContactPersonId;
                    entity.TaxTotal = item.TaxTotal;
                    entity.ItemTotal = item.ItemTotal;
                    entity.GrandTotal = item.GrandTotal;
                    if (item.ContactPersonId != null)
                    {
                        M_Ledger_BillingDetailsEntity contactdetail = new M_Ledger_BillingDetailsEntity();
                        var condata = db.M_Ledger_BillingDetails.Where(x => x.Id == item.ContactPersonId).SingleOrDefault();
                        if (condata != null)
                        {
                            contactdetail.Id = condata.Id;
                            contactdetail.Billing_Name = condata.Billing_Name;
                        }
                        else
                        {
                            contactdetail.Id = Convert.ToInt32(item.ContactPersonId);
                            contactdetail.Billing_Name = "--";
                        }

                        entity.M_Ledger_BillDEntity = contactdetail;
                    }
                    entity.Comment = item.Comment;
                    entity.SupplierId = item.SupplierId;
                    entity.IsActive = item.IsActive;
                    entity.Createddate = item.Createddate;
                    entity.Craetedby = item.Craetedby;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    entity.DeliveryLoaction = item.DeliveryLoaction;
                    if (item.DeliveryLoaction > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.DeliveryLoaction).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WAREHOUSE = locentity;
                        }
                    }

                    entity.Narration = item.Narration;
                    entity.Approvaldate = item.Approvaldate;
                    entity.Approvalstatus = item.Approvalstatus;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsReadymade = item.IsReadymade;


                    entity.POValidityFrom = item.POValidityFrom;
                    entity.POValidityTo = item.POValidityTo;
                    entity.PaymentTerm = item.PaymentTerm;
                    if (item.M_PaymentTermMaster != null)
                    {
                        M_PaymentTermMasterEntity payentity = new M_PaymentTermMasterEntity();
                        payentity.Id = item.M_PaymentTermMaster.Id;
                        payentity.PaymentTerm = item.M_PaymentTermMaster.PaymentTerm;
                        entity.M_PaymentTermMaster = payentity;
                    }
                    if (item.SupplierId != null)
                    {
                        M_LedgersEntity legderentity = new M_LedgersEntity();
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.SupplierId).SingleOrDefault();
                        if (custdata != null)
                        {
                            legderentity.Ledger_Id = custdata.Ledger_Id;
                            legderentity.Ledger_Name = custdata.Ledger_Name;
                        }
                        else
                        {
                            legderentity.Ledger_Id = Convert.ToInt32(item.SupplierId);
                            legderentity.Ledger_Name = "--";
                        }
                        entity.M_Ledgersentity = legderentity;
                    }
                    list.Add(entity);
                }

                if (roleid != 4)
                {
                    var Userlist = rlobj.GetUserRole().Where(x => x.roleId == 4 || x.roleId == roleid).ToList();
                    list = list.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && Userlist.Any(y => y.UserId == x.Craetedby)).OrderByDescending(x => x.Id).ToList();
                }
            }

            return list;

        }

        ////List Of PO With Status and User and Form Date And To Date Wise
        public List<SRM_PurchaseOrderMasterEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<SRM_PurchaseOrderMasterEntity> list = new List<SRM_PurchaseOrderMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_PurchaseOrderMaster.Where(x => x.IsActive == true && x.PO_Date >= from && x.PO_Date <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.Craetedby == Userid).OrderByDescending(x=>x.Id).ToList();
                if (records.Count >= 0 && roleid != 4)
                {
                    records = db.SRM_PurchaseOrderMaster.Where(x => x.IsActive == true && x.PO_Date >= from && x.PO_Date <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).OrderByDescending(x => x.Id).ToList();
                }
                foreach (var item in records)
                {

                    SRM_PurchaseOrderMasterEntity entity = new SRM_PurchaseOrderMasterEntity();
                    entity.Id = item.Id;
                    entity.PO_NO = item.PO_NO;
                    entity.PO_Date = item.PO_Date;
                    // entity.POType = item.POType;
                    entity.QuotationId = item.QuotationId;
                    if (item.SRM_QuotationMaster != null)
                    {
                        SRM_QuotationMasterEntity quotationentity = new SRM_QuotationMasterEntity();
                        quotationentity.Id = item.SRM_QuotationMaster.Id;
                        quotationentity.QuotationNo = item.SRM_QuotationMaster.QuotationNo;
                        quotationentity.QuotationDate = item.SRM_QuotationMaster.QuotationDate;
                        quotationentity.SupplierId = item.SRM_QuotationMaster.SupplierId;
                        entity.SRM_QuotationMaster = quotationentity;
                    }
                    entity.QuotationDate = item.QuotationDate;
                    entity.ContactPersonId = item.ContactPersonId;
                    entity.TaxTotal = item.TaxTotal;
                    entity.ItemTotal = item.ItemTotal;
                    entity.GrandTotal = item.GrandTotal;
                    if (item.ContactPersonId != null)
                    {
                        M_Ledger_BillingDetailsEntity contactdetail = new M_Ledger_BillingDetailsEntity();
                        var condata = db.M_Ledger_BillingDetails.Where(x => x.Id == item.ContactPersonId).SingleOrDefault();
                        if (condata != null)
                        {
                            contactdetail.Id = condata.Id;
                            contactdetail.Billing_Name = condata.Billing_Name;
                        }
                        else
                        {
                            contactdetail.Id = Convert.ToInt32(item.ContactPersonId);
                            contactdetail.Billing_Name = "--";
                        }

                        entity.M_Ledger_BillDEntity = contactdetail;
                    }
                    entity.Comment = item.Comment;
                    entity.SupplierId = item.SupplierId;
                    entity.IsActive = item.IsActive;
                    entity.Createddate = item.Createddate;
                    entity.Craetedby = item.Craetedby;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    entity.DeliveryLoaction = item.DeliveryLoaction;
                    if (item.DeliveryLoaction>0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.DeliveryLoaction).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WAREHOUSE = locentity;
                        }
                    }

                    entity.Narration = item.Narration;
                    entity.Approvaldate = item.Approvaldate;
                    entity.Approvalstatus = item.Approvalstatus;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;

                    entity.POValidityFrom = item.POValidityFrom;
                    entity.POValidityTo = item.POValidityTo;
                    entity.PaymentTerm = item.PaymentTerm;
                    if (item.M_PaymentTermMaster != null)
                    {
                        M_PaymentTermMasterEntity payentity = new M_PaymentTermMasterEntity();
                        payentity.Id = item.M_PaymentTermMaster.Id;
                        payentity.PaymentTerm = item.M_PaymentTermMaster.PaymentTerm;
                        entity.M_PaymentTermMaster = payentity;
                    }
                    if (item.SupplierId != null)
                    {
                        M_LedgersEntity legderentity = new M_LedgersEntity();
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.SupplierId).SingleOrDefault();
                        if (custdata != null)
                        {
                            legderentity.Ledger_Id = custdata.Ledger_Id;
                            legderentity.Ledger_Name = custdata.Ledger_Name;
                        }
                        else
                        {
                            legderentity.Ledger_Id = Convert.ToInt32(item.SupplierId);
                            legderentity.Ledger_Name = "--";
                        }
                        entity.M_Ledgersentity = legderentity;
                    }
                    list.Add(entity);
                }
                if (roleid != 4)
                {
                    var Userlist = rlobj.GetUserRole().Where(x => x.roleId == 4 || x.roleId == roleid).ToList();
                    list = list.Where(x => x.IsActive == true && x.PO_Date >= from && x.PO_Date <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && Userlist.Any(y => y.UserId == x.Craetedby)).OrderByDescending(x => x.Id).ToList();
                }

            }
            return list;
        }



        public List<SRM_PurchaseOrderMasterEntity> Get(DateTime from, DateTime to)
        {
            List<SRM_PurchaseOrderMasterEntity> list = new List<SRM_PurchaseOrderMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_PurchaseOrderMaster.ToList().Where(x => x.IsActive == true && x.PO_Date >= from && x.PO_Date <= to).ToList();
                foreach (var item in records)
                {

                    SRM_PurchaseOrderMasterEntity entity = new SRM_PurchaseOrderMasterEntity();
                    entity.Id = item.Id;
                    entity.PO_NO = item.PO_NO;
                    entity.PO_Date = item.PO_Date;
                    // entity.POType = item.POType;
                    entity.QuotationId = item.QuotationId;
                    if (item.SRM_QuotationMaster != null)
                    {
                        SRM_QuotationMasterEntity quotationentity = new SRM_QuotationMasterEntity();
                        quotationentity.Id = item.SRM_QuotationMaster.Id;
                        quotationentity.QuotationNo = item.SRM_QuotationMaster.QuotationNo;
                        quotationentity.QuotationDate = item.SRM_QuotationMaster.QuotationDate;
                        quotationentity.SupplierId = item.SRM_QuotationMaster.SupplierId;
                        entity.SRM_QuotationMaster = quotationentity;
                    }
                    entity.QuotationDate = item.QuotationDate;
                    entity.ContactPersonId = item.ContactPersonId;
                    entity.TaxTotal = item.TaxTotal;
                    entity.ItemTotal = item.ItemTotal;
                    entity.GrandTotal = item.GrandTotal;
                    if (item.ContactPersonId != null)
                    {
                        M_Ledger_BillingDetailsEntity contactdetail = new M_Ledger_BillingDetailsEntity();
                        var condata = db.M_Ledger_BillingDetails.Where(x => x.Id == item.ContactPersonId).SingleOrDefault();
                        if (condata != null)
                        {
                            contactdetail.Id = condata.Id;
                            contactdetail.Billing_Name = condata.Billing_Name;
                        }
                        else
                        {
                            contactdetail.Id = Convert.ToInt32(item.ContactPersonId);
                            contactdetail.Billing_Name = "--";
                        }

                        entity.M_Ledger_BillDEntity = contactdetail;
                    }
                    entity.Comment = item.Comment;
                    entity.SupplierId = item.SupplierId;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.IsReadymade = item.IsReadymade;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.Createddate = item.Createddate;
                    entity.Craetedby = item.Craetedby;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    entity.DeliveryLoaction = item.DeliveryLoaction;
                    if (item.DeliveryLoaction>0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.DeliveryLoaction).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WAREHOUSE = locentity;
                        }
                    }
                    entity.POValidityFrom = item.POValidityFrom;
                    entity.POValidityTo = item.POValidityTo;
                    entity.PaymentTerm = item.PaymentTerm;
                    if (item.M_PaymentTermMaster != null)
                    {
                        M_PaymentTermMasterEntity payentity = new M_PaymentTermMasterEntity();
                        payentity.Id = item.M_PaymentTermMaster.Id;
                        payentity.PaymentTerm = item.M_PaymentTermMaster.PaymentTerm;
                        entity.M_PaymentTermMaster = payentity;
                    }
                    if (item.SupplierId != null)
                    {
                        M_LedgersEntity legderentity = new M_LedgersEntity();
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.SupplierId).SingleOrDefault();
                        if (custdata != null)
                        {
                            legderentity.Ledger_Id = custdata.Ledger_Id;
                            legderentity.Ledger_Name = custdata.Ledger_Name;
                        }
                        else
                        {
                            legderentity.Ledger_Id = Convert.ToInt32(item.SupplierId);
                            legderentity.Ledger_Name = "--";
                        }
                        entity.M_Ledgersentity = legderentity;
                    }
                    list.Add(entity);
                }
            }
            return list;
        }
        public List<SRM_PurchaseOrderMasterEntity> Get()
        {
            List<SRM_PurchaseOrderMasterEntity> list = new List<SRM_PurchaseOrderMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_PurchaseOrderMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {

                    SRM_PurchaseOrderMasterEntity entity = new SRM_PurchaseOrderMasterEntity();
                    entity.Id = item.Id;
                    entity.PO_NO = item.PO_NO;
                    entity.PO_Date = item.PO_Date;
                    // entity.POType = item.POType;
                    entity.QuotationId = item.QuotationId;
                    if (item.SRM_QuotationMaster != null)
                    {
                        SRM_QuotationMasterEntity quotationentity = new SRM_QuotationMasterEntity();
                        quotationentity.Id = item.SRM_QuotationMaster.Id;
                        quotationentity.QuotationNo = item.SRM_QuotationMaster.QuotationNo;
                        quotationentity.QuotationDate = item.SRM_QuotationMaster.QuotationDate;
                        quotationentity.SupplierId = item.SRM_QuotationMaster.SupplierId;
                        entity.SRM_QuotationMaster = quotationentity;
                    }
                    entity.QuotationDate = item.QuotationDate;
                    entity.ContactPersonId = item.ContactPersonId;
                    entity.TaxTotal = item.TaxTotal;
                    entity.ItemTotal = item.ItemTotal;
                    entity.GrandTotal = item.GrandTotal;
                    if (item.ContactPersonId != null)
                    {
                        M_Ledger_BillingDetailsEntity contactdetail = new M_Ledger_BillingDetailsEntity();
                        var condata = db.M_Ledger_BillingDetails.Where(x => x.Id == item.ContactPersonId).SingleOrDefault();
                        if (condata != null)
                        {
                            contactdetail.Id = condata.Id;
                            contactdetail.Billing_Name = condata.Billing_Name;
                        }
                        else
                        {
                            contactdetail.Id = Convert.ToInt32(item.ContactPersonId);
                            contactdetail.Billing_Name = "--";
                        }

                        entity.M_Ledger_BillDEntity = contactdetail;
                    }
                    entity.Comment = item.Comment;
                    entity.SupplierId = item.SupplierId;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.IsReadymade = item.IsReadymade;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.Createddate = item.Createddate;
                    entity.Craetedby = item.Craetedby;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    entity.DeliveryLoaction = item.DeliveryLoaction;
                    if (item.DeliveryLoaction > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.DeliveryLoaction).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WAREHOUSE = locentity;
                        }
                    }
                    entity.POValidityFrom = item.POValidityFrom;
                    entity.POValidityTo = item.POValidityTo;
                    entity.PaymentTerm = item.PaymentTerm;
                    if (item.M_PaymentTermMaster != null)
                    {
                        M_PaymentTermMasterEntity payentity = new M_PaymentTermMasterEntity();
                        payentity.Id = item.M_PaymentTermMaster.Id;
                        payentity.PaymentTerm = item.M_PaymentTermMaster.PaymentTerm;
                        entity.M_PaymentTermMaster = payentity;
                    }
                    if (item.SupplierId != null)
                    {
                        M_LedgersEntity legderentity = new M_LedgersEntity();
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.SupplierId).SingleOrDefault();
                        if (custdata != null)
                        {
                            legderentity.Ledger_Id = custdata.Ledger_Id;
                            legderentity.Ledger_Name = custdata.Ledger_Name;
                        }
                        else
                        {
                            legderentity.Ledger_Id = Convert.ToInt32(item.SupplierId);
                            legderentity.Ledger_Name = "--";
                        }
                        entity.M_Ledgersentity = legderentity;
                    }
                    list.Add(entity);
                }
            }
            return list;
        }

        public List<SRM_PurchaseOrderMasterEntity> Getbysupplierid(long suppid)
        {
            List<long> POId = new List<long>();
            List<bool> POIdTF = new List<bool>();

            List<SRM_PurchaseOrderMasterEntity> list = new List<SRM_PurchaseOrderMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var WOd = db.SRM_GRNInward.Where(x => x.IsActive == true).ToList();
                foreach (var Data in WOd)
                {

                    var rec1 = db.SRM_GRNInward.Where(x => x.POid == Data.POid).Select(x => x.ID).ToList();
                    POIdTF.Clear();
                    foreach (var dat2 in rec1)
                    {
                        var recd1 = db.SRM_GRNItem.Where(x => x.ItemInwardId == dat2).ToList();

                        foreach (var dat3 in recd1)
                        {
                            if (dat3.Balance_Qty == 0)
                            {
                                POIdTF.Add(true);
                            }
                            else
                            {
                                POIdTF.Add(false);
                            }
                        }
                    }
                    int chki = POIdTF.Count(ai => ai == false);
                    if (chki == 0)
                    {
                        POId.Add(Convert.ToInt32(Data.POid));
                    }
                }

                var records = db.SRM_PurchaseOrderMaster.ToList().Where(x => x.SupplierId == suppid && x.IsActive == true && !POId.Contains(x.Id)).ToList().OrderByDescending(x => x.Id);
                foreach (var item in records)
                {
                    SRM_PurchaseOrderMasterEntity entity = new SRM_PurchaseOrderMasterEntity();
                    entity.PO_NO = item.PO_NO;
                    entity.Id = item.Id;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    list.Add(entity);
                }

            }
            return list;
        }



        public SRM_PurchaseOrderMasterEntity getbyid(long id)
        {
            SRM_PurchaseOrderMasterEntity entity = new SRM_PurchaseOrderMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.SRM_PurchaseOrderMaster.Find(id);
                if (item != null)
                {
                    entity.Id = item.Id;
                    entity.PO_NO = item.PO_NO;
                    entity.PO_Date = item.PO_Date;
                    // entity.POType = item.POType;
                    entity.TaxTotal = item.TaxTotal;
                    entity.ItemTotal = item.ItemTotal;
                    entity.totalcost = item.totalcost;
                    entity.GrandTotal = item.GrandTotal;
                    entity.QuotationId = item.QuotationId;
                    if (item.SRM_QuotationMaster != null)
                    {
                        SRM_QuotationMasterEntity quotationentity = new SRM_QuotationMasterEntity();
                        quotationentity.Id = item.SRM_QuotationMaster.Id;
                        quotationentity.QuotationNo = item.SRM_QuotationMaster.QuotationNo;
                        quotationentity.QuotationDate = item.SRM_QuotationMaster.QuotationDate;
                        quotationentity.SupplierId = item.SRM_QuotationMaster.SupplierId;
                        entity.SRM_QuotationMaster = quotationentity;
                    }
                    entity.QuotationDate = item.QuotationDate;
                    entity.ContactPersonId = item.ContactPersonId;
                    if (item.ContactPersonId != null)
                    {
                        M_Ledger_BillingDetailsEntity contactdetail = new M_Ledger_BillingDetailsEntity();
                        var condata = db.M_Ledger_BillingDetails.Where(x => x.Id == item.ContactPersonId).SingleOrDefault();
                        if (condata != null)
                        {
                            contactdetail.Id = condata.Id;
                            contactdetail.Billing_Name = condata.Billing_Name;
                            contactdetail.Address = condata.Address;
                        }
                        else
                        {
                            contactdetail.Id = Convert.ToInt32(item.ContactPersonId);
                            contactdetail.Billing_Name = "--";
                            contactdetail.Address = "";
                        }

                        entity.M_Ledger_BillDEntity = contactdetail;
                    }
                    entity.Comment = item.Comment;
                    entity.Narration = item.Narration;
                    entity.Approvaldate = item.Approvaldate;
                    entity.Approvalstatus = item.Approvalstatus;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsReadymade = item.IsReadymade;
                    entity.SupplierId = item.SupplierId;
                    entity.IsActive = item.IsActive;
                    entity.Createddate = item.Createddate;
                    entity.Craetedby = item.Craetedby;
                    entity.Updateddate = item.Updateddate;
                    entity.Updatedby = item.Updatedby;
                    entity.DeliveryLoaction = item.DeliveryLoaction;
                    if (item.DeliveryLoaction > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.DeliveryLoaction).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WAREHOUSE = locentity;
                        }
                    }
                    entity.POValidityFrom = item.POValidityFrom;
                    entity.POValidityTo = item.POValidityTo;
                    entity.PaymentTerm = item.PaymentTerm;
                    if (item.M_PaymentTermMaster != null)
                    {
                        M_PaymentTermMasterEntity payentity = new M_PaymentTermMasterEntity();
                        payentity.Id = item.M_PaymentTermMaster.Id;
                        payentity.PaymentTerm = item.M_PaymentTermMaster.PaymentTerm;
                        entity.M_PaymentTermMaster = payentity;
                    }
                    if (item.SupplierId != null)
                    {
                        M_LedgersEntity legderentity = new M_LedgersEntity();
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.SupplierId).SingleOrDefault();
                        if (custdata != null)
                        {
                            legderentity.Ledger_Id = custdata.Ledger_Id;
                            legderentity.Ledger_Name = custdata.Ledger_Name;
                        }
                        else
                        {
                            legderentity.Ledger_Id = Convert.ToInt32(item.SupplierId);
                            legderentity.Ledger_Name = "--";
                        }
                        entity.M_Ledgersentity = legderentity;
                    }

                    var otherCharge = db.T_POOtherCharge.Where(x => x.TXID == item.Id && x.InventoryModeId == 10008).ToList();
                    if (otherCharge != null)
                    {
                        List<T_POOtherChargeEntity> oclist = new List<T_POOtherChargeEntity>();
                        foreach (var ocdata in otherCharge)
                        {
                            T_POOtherChargeEntity ent = new T_POOtherChargeEntity();
                            ent.Id = ocdata.Id;
                            ent.TXID = ocdata.TXID;
                            ent.OtherChargeId = ocdata.OtherChargeId;
                            if (ocdata.OtherChargeId > 0)
                            {
                                M_POOtherChargesMasterEntity ocmaster = new M_POOtherChargesMasterEntity();
                                string OCname = db.M_POOtherChargesMaster.Where(x => x.id == ocdata.OtherChargeId).Select(x => x.POOtherChargeName).FirstOrDefault();
                                ocmaster.POOtherChargeName = OCname;
                                ent.OCMaster = ocmaster;
                            }

                            ent.OtherChargeValue = ocdata.OtherChargeValue;
                            ent.GST = ocdata.GST;
                            ent.InventoryModeId = ocdata.InventoryModeId;
                            ent.Company_ID = ocdata.Company_ID;
                            ent.BranchId = ocdata.BranchId;
                            ent.CreatedOn = DateTime.Now;
                            ent.CreatedBy = ocdata.CreatedBy;
                            oclist.Add(ent);
                        }
                        entity.POOtherChargeEntity = oclist;
                    }


                    var itemrecords = db.SRM_PurchaseOrderItemDetail.Where(x => x.PO_ID == item.Id).ToList();
                    List<SRM_PurchaseOrderItemDetailEntity> itemlist = new List<SRM_PurchaseOrderItemDetailEntity>();
                    if (itemrecords != null)
                    {
                        foreach (var itemrecord in itemrecords)
                        {
                            SRM_PurchaseOrderItemDetailEntity poitemeentty = new SRM_PurchaseOrderItemDetailEntity();
                            poitemeentty.Id = itemrecord.Id;
                            poitemeentty.PO_ID = itemrecord.PO_ID;
                            poitemeentty.POQty = itemrecord.POQty;
                            poitemeentty.Amount = itemrecord.Amount;
                            poitemeentty.CreatedBy = itemrecord.CreatedBy;
                            poitemeentty.CreatedDate = itemrecord.CreatedDate;
                            poitemeentty.ItemDesc = itemrecord.ItemDesc;
                            poitemeentty.ItemId = itemrecord.ItemId;

                            int IAid = Convert.ToInt32(itemrecord.Item_Attribute_ID);
                            int ISid = 0;

                            if (itemrecord.M_ItemMaster != null)
                            {
                                M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                                itementity.Id = itemrecord.M_ItemMaster.Id;
                                itementity.ItemName = getItemName(itemrecord.M_ItemMaster.Id, IAid, ISid);
                                itementity.OpeningStock = itemrecord.M_ItemMaster.OpeningStock;
                                itementity.HScode = itemrecord.M_ItemMaster.HScode;
                                poitemeentty.M_ItemMaster = itementity;
                            }
                            poitemeentty.Item_Attribute_ID = itemrecord.Item_Attribute_ID > 0 ? itemrecord.Item_Attribute_ID : 0;
                            poitemeentty.Provision = itemrecord.Provision;
                            poitemeentty.Rate = itemrecord.Rate;
                            poitemeentty.TaxId = itemrecord.TaxId;
                            //if (itemrecord.M_TaxMaster != null)
                            //{
                            //    M_TaxMasterEntity taxentity = new M_TaxMasterEntity();
                            //    taxentity.Id = itemrecord.M_TaxMaster.Id;
                            //    taxentity.Taxname = itemrecord.M_TaxMaster.Taxname;
                            //    poitemeentty.M_TaxMaster = taxentity;
                            //}
                            poitemeentty.TaxValue = itemrecord.TaxValue;
                            poitemeentty.StockQty = itemrecord.StockQty;
                            poitemeentty.RateIndicatorId = itemrecord.RateIndicatorId;
                            if (itemrecord.M_RateIndicatorMaster != null)
                            {
                                M_RateIndicatorMasterEntity rateindicatorentity = new M_RateIndicatorMasterEntity();
                                rateindicatorentity.Id = itemrecord.M_RateIndicatorMaster.Id;
                                rateindicatorentity.RateIndicatorName = itemrecord.M_RateIndicatorMaster.RateIndicatorName;
                                poitemeentty.M_RateIndicatorMaster = rateindicatorentity;
                            }
                            poitemeentty.TotalQty = itemrecord.TotalQty;
                            poitemeentty.UnitId = itemrecord.UnitId;
                            if (itemrecord.M_UnitMaster != null)
                            {
                                M_UnitMasterEntity unitentity = new M_UnitMasterEntity();
                                unitentity.Id = itemrecord.M_UnitMaster.Id;
                                unitentity.ItemUnit = itemrecord.M_UnitMaster.ItemUnit;
                                poitemeentty.M_UnitMaster = unitentity;
                            }
                            poitemeentty.UpdatedDate = itemrecord.UpdatedDate;
                            poitemeentty.UpdatedBy = itemrecord.UpdatedBy;
                            poitemeentty.Wastage = itemrecord.Wastage;
                            List<string> rawimgdata = new List<string>();
                            long SPR = (long)db.SRM_QuotationMaster.Where(x => x.Id == item.QuotationId).Select(x => x.PR_Id).FirstOrDefault();
                            long bomid = (long)db.SRM_PurchaseRequestMaster.Where(x => x.Id == SPR).Select(x => x.BOM_Id).FirstOrDefault();
                            poitemeentty.IsReadymade = db.M_BillOfMaterialMaster.Where(x => x.BOM_ID == bomid).Select(x => x.IsReadymate).FirstOrDefault();
                            long bomrawid = (long)db.M_BillOfMaterialDetail.Where(x => x.BOM_ID == bomid && x.ItemId == itemrecord.ItemId && x.Item_Attribute_ID == itemrecord.Item_Attribute_ID).Select(x => x.ID).FirstOrDefault();
                            var rawimg = db.M_BOMRawItemImage.Where(x => x.BOMrawitemdetailid == bomrawid).ToList();
                            if (rawimg != null)
                            {
                                foreach (var img in rawimg)
                                {
                                    if (img.image != "")
                                    {
                                        rawimgdata.Add(img.image);
                                    }
                                }
                            }
                            poitemeentty.rawitemimg_list = rawimgdata;

                            entity.itemdetailentity = poitemeentty;
                            

                            itemlist.Add(poitemeentty);
                        }



                        entity.SRM_PurchaseOrderItemDetail = itemlist;
                    }

                    var po_deliveryDetail = db.MFG_PO_DeliveryDetail.Where(x => x.SPO_ID == id && x.IsActive == true).ToList();
                    List<MFG_PO_DeliveryDetail_Entity> po_deliveryschedules = new List<MFG_PO_DeliveryDetail_Entity>();
                    foreach (var sche_ in po_deliveryDetail)
                    {
                        MFG_PO_DeliveryDetail_Entity po_delentity = new MFG_PO_DeliveryDetail_Entity();
                        po_delentity.PO_DelSchedule_ID = sche_.PO_DelSchedule_ID;
                        po_delentity.SPO_ID = sche_.SPO_ID;
                        po_delentity.Delivery_Date = sche_.Delivery_Date;
                        po_delentity.Delivery_Qty = sche_.Delivery_Qty;
                        po_delentity.Item_ID = sche_.Item_ID;
                        po_delentity.Item_Attribute_Id = sche_.Item_Attribute_Id;
                        M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                        if (sche_.Item_ID != null)
                        {
                            int IAidd = sche_.Item_Attribute_Id==null?0:Convert.ToInt32(sche_.Item_Attribute_Id);
                            var itemrecord = db.M_ItemMaster.Where(x => x.Id == sche_.Item_ID).SingleOrDefault();
                            itementity.Id = itemrecord.Id;
                            itementity.ItemName = getItemName(itemrecord.Id, IAidd, 0);
                        }
                        po_delentity.M_ItemMaster = itementity;
                        po_delentity.Comments = sche_.Comments;
                        po_delentity.Ledger_Id = sche_.Ledger_Id;
                        po_deliveryschedules.Add(po_delentity);
                    }
                    entity.MFG_PO_DeliveryDetail = po_deliveryschedules;

                }

            }
            return entity;
        }


        public string getItemName(long itemid, int attibid, int subcatid)
        {
            string itemname = "";
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 32; //Check Bill Of Material Create Or Not Using Customer PO 
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = Convert.ToString(subcatid);
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "<root><Row><ItemId>" + itemid + "</ItemId><AttributeId>" + attibid + "</AttributeId></Row></root>";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                foreach (DataTable table in dt.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        itemname = Convert.ToString(dr["ItemName"]);
                    }
                }

            }
            return itemname;
        }

        public long Insert(SRM_PurchaseOrderMasterEntity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                SRM_PurchaseOrderMaster record = new SRM_PurchaseOrderMaster();
                record.Id = entity.Id;
                record.PO_Date = entity.PO_Date;
                // record.POType = entity.POType;
                record.QuotationDate = entity.QuotationDate;
                record.QuotationId = entity.QuotationId;
                record.SupplierId = entity.SupplierId;
                record.Comment = entity.Comment;
                record.ContactPersonId = entity.ContactPersonId;
                record.ItemTotal = entity.ItemTotal;
                record.TaxTotal = entity.TaxTotal;
                record.totalcost = entity.totalcost;
                record.GrandTotal = entity.GrandTotal;
                record.Createddate = DateTime.Now;
                record.Craetedby = entity.Craetedby;
                record.DeliveryLoaction = entity.DeliveryLoaction;
                record.PaymentTerm = entity.PaymentTerm;
                record.POValidityFrom = entity.POValidityFrom;
                record.POValidityTo = entity.POValidityTo;
                record.IsActive = entity.IsActive;
                record.Narration = entity.Narration;
                record.Approvaldate = entity.Approvaldate;
                record.Approvalstatus = entity.Approvalstatus;
                record.IsStatus = entity.IsStatus;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.BranchId;
                record.IsReadymade = entity.IsReadymade;
                if (entity.IsReadymade == 0)
                {
                    record.PO_NO = "RM-" + Convert.ToString(getnextPONo());
                }
                else
                {
                    record.PO_NO = "RF-" + Convert.ToString(getnextPONo());
                }
                db.SRM_PurchaseOrderMaster.Add(record);
                db.SaveChanges();
                id = record.Id;
                if (id > 0)
                {
                    if (entity.POOtherChargeEntity != null)
                    {
                        foreach (var data in entity.POOtherChargeEntity)
                        {
                            T_POOtherCharge ent = new T_POOtherCharge();
                            ent.TXID = id;
                            ent.OtherChargeId = data.OtherChargeId;
                            ent.OtherChargeValue = data.OtherChargeValue;
                            ent.GST = data.GST;
                            ent.InventoryModeId = data.InventoryModeId;
                            ent.Company_ID = data.Company_ID;
                            ent.BranchId = data.BranchId;
                            ent.CreatedOn = DateTime.Now;
                            ent.CreatedBy = data.CreatedBy;
                            db.T_POOtherCharge.Add(ent);
                            db.SaveChanges();
                        }
                    }


                   foreach (var item in entity.SRM_PurchaseOrderItemDetail)
                    {
                        SRM_PurchaseOrderItemDetail itemrecord = new SRM_PurchaseOrderItemDetail();
                        itemrecord.Id = item.Id;
                        itemrecord.PO_ID = id;
                        itemrecord.Amount = item.Amount;
                        itemrecord.CreatedBy = item.CreatedBy;
                        itemrecord.IsActive = item.IsActive;
                        itemrecord.ItemDesc = item.ItemDesc;
                        itemrecord.ItemId = item.ItemId;
                        itemrecord.Item_Attribute_ID = item.Item_Attribute_ID;
                        itemrecord.POQty = item.POQty;
                        itemrecord.Provision = item.Provision;
                        itemrecord.Rate = item.Rate;
                        itemrecord.TaxId = item.TaxId;
                        itemrecord.TaxValue = item.TaxValue;
                        itemrecord.StockQty = item.StockQty;
                        itemrecord.RateIndicatorId = item.RateIndicatorId;
                        itemrecord.TotalQty = item.TotalQty;
                        itemrecord.UnitId = item.UnitId;
                        itemrecord.CreatedDate = DateTime.Now;
                        itemrecord.Company_ID = item.Company_ID;
                        itemrecord.BranchId = item.BranchId;
                        itemrecord.Wastage = item.Wastage;
                        itemrecord.IsActive = true;
                        db.SRM_PurchaseOrderItemDetail.Add(itemrecord);
                        db.SaveChanges();
                    }

                    if (entity.MFG_PO_DeliveryDetail != null)
                    {
                        foreach (var sche_ in entity.MFG_PO_DeliveryDetail)
                        {
                            MFG_PO_DeliveryDetail po_delentity = new MFG_PO_DeliveryDetail();
                            po_delentity.PO_DelSchedule_ID = sche_.PO_DelSchedule_ID;
                            po_delentity.Ledger_Id = sche_.Ledger_Id;
                            po_delentity.SPO_ID = id;
                            po_delentity.Delivery_Qty = sche_.Delivery_Qty;
                            po_delentity.Delivery_Date = sche_.Delivery_Date;
                            po_delentity.Comments = sche_.Comments;
                            po_delentity.Item_ID = sche_.Item_ID;
                            po_delentity.Item_Attribute_Id = sche_.Item_Attribute_Id;
                            po_delentity.Created_Date = sche_.Created_Date;
                            po_delentity.IsActive = sche_.IsActive;
                            po_delentity.Created_By = sche_.Created_By;
                            //po_delentity.Company_ID = sche_.Company_ID;
                            //po_delentity.BranchId = sche_.BranchId;
                            db.MFG_PO_DeliveryDetail.Add(po_delentity);
                            db.SaveChanges();
                        }
                    }
                }

                if (id > 0)
                {
                    var data = db.SRM_QuotationMaster.Find(entity.QuotationId);
                    data.IsStatus = 4;
                    data.UpdatedDate = DateTime.Now;
                    data.UpdatedBy = entity.Craetedby;
                    db.SaveChanges();
                }
            }
            return id;
        }

        public bool Update(SRM_PurchaseOrderMasterEntity entity)
        {
            bool isupdatd = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    SRM_PurchaseOrderMaster record = db.SRM_PurchaseOrderMaster.Find(entity.Id);
                    record.Comment = Convert.ToString(entity.Comment);
                    record.ContactPersonId = entity.ContactPersonId;
                    //  record.POType = entity.POType;
                    record.ItemTotal = entity.ItemTotal;
                    record.TaxTotal = entity.TaxTotal;
                    record.totalcost = entity.totalcost;
                    record.GrandTotal = entity.GrandTotal;
                    record.Updateddate = DateTime.Now;
                    record.Updatedby = entity.Updatedby;
                    record.DeliveryLoaction = entity.DeliveryLoaction;
                    record.PaymentTerm = entity.PaymentTerm;
                    record.POValidityFrom = entity.POValidityFrom;
                    record.POValidityTo = entity.POValidityTo;
                    record.IsActive = entity.IsActive;
                    record.IsStatus = entity.IsStatus;
                    record.Approvaldate = entity.Approvaldate;
                    record.Approvalstatus = entity.Approvalstatus;
                    record.Comment = entity.Comment;
                    db.SaveChanges();
                    isupdatd = true;

                    if (entity.POOtherChargeEntity != null)
                    {
                        var singleRec = db.T_POOtherCharge.Where(x => x.TXID == entity.Id && x.InventoryModeId == 10008).ToList();
                        foreach (var rec in singleRec)
                        {
                            var OCrecord = db.T_POOtherCharge.FirstOrDefault(x => x.Id == rec.Id);
                            db.T_POOtherCharge.Remove(OCrecord);
                            db.SaveChanges();
                        }

                        foreach (var data in entity.POOtherChargeEntity)
                        {
                            T_POOtherCharge ent = new T_POOtherCharge();
                            ent.TXID = entity.Id;
                            ent.OtherChargeId = data.OtherChargeId;
                            ent.OtherChargeValue = data.OtherChargeValue;
                            ent.GST = data.GST;
                            ent.InventoryModeId = data.InventoryModeId;
                            ent.Company_ID = data.Company_ID;
                            ent.BranchId = data.BranchId;
                            ent.CreatedOn = DateTime.Now;
                            ent.CreatedBy = data.CreatedBy;
                            db.T_POOtherCharge.Add(ent);
                            db.SaveChanges();
                        }
                    }
                    
                    foreach (var item in entity.SRM_PurchaseOrderItemDetail)
                    {

                        SRM_PurchaseOrderItemDetail itemrecord = db.SRM_PurchaseOrderItemDetail.Find(item.Id);
                        itemrecord.Id = item.Id;
                        itemrecord.PO_ID = entity.Id;
                        itemrecord.Amount = item.Amount;
                        itemrecord.IsActive = item.IsActive;
                        itemrecord.ItemDesc = item.ItemDesc;
                        itemrecord.ItemId = item.ItemId;
                        itemrecord.Item_Attribute_ID = item.Item_Attribute_ID;
                        itemrecord.POQty = item.POQty;
                        itemrecord.Provision = item.Provision;
                        itemrecord.Rate = item.Rate;
                        itemrecord.TaxId = item.TaxId;
                        itemrecord.TaxValue = item.TaxValue;
                        itemrecord.StockQty = item.StockQty;
                        itemrecord.RateIndicatorId = item.RateIndicatorId;
                        itemrecord.TotalQty = item.TotalQty;
                        //itemrecord.UnitId = item.UnitId;
                        itemrecord.UpdatedBy = item.UpdatedBy;
                        itemrecord.UpdatedDate = DateTime.Now;
                        itemrecord.Wastage = item.Wastage;
                        db.SaveChanges();
                    }

                    //Code to insert and update po delivery schedules....
                    if (entity.MFG_PO_DeliveryDetail != null)
                    {
                        foreach (var sche_ in entity.MFG_PO_DeliveryDetail)
                        {
                            if (sche_.PO_DelSchedule_ID > 0)
                            {
                                MFG_PO_DeliveryDetail po_delup = db.MFG_PO_DeliveryDetail.Find(sche_.PO_DelSchedule_ID);
                                po_delup.PO_DelSchedule_ID = sche_.PO_DelSchedule_ID;
                                //po_delup.Ledger_Id = sche_.Ledger_Id;
                                po_delup.SPO_ID = entity.Id;
                                po_delup.Delivery_Qty = sche_.Delivery_Qty;
                                po_delup.Delivery_Date = sche_.Delivery_Date;
                                po_delup.Comments = sche_.Comments;
                                po_delup.Item_ID = sche_.Item_ID;
                                po_delup.Modified_Date = sche_.Modified_Date;
                                po_delup.Modified_By = sche_.Modified_By;
                                db.SaveChanges();
                            }
                            else
                            {
                                MFG_PO_DeliveryDetail po_delentity = new MFG_PO_DeliveryDetail();
                                po_delentity.PO_DelSchedule_ID = sche_.PO_DelSchedule_ID;
                                po_delentity.Ledger_Id = sche_.Ledger_Id;
                                po_delentity.SPO_ID = entity.Id;
                                po_delentity.Delivery_Qty = sche_.Delivery_Qty;
                                po_delentity.Delivery_Date = sche_.Delivery_Date;
                                po_delentity.Comments = sche_.Comments;
                                po_delentity.Item_ID = sche_.Item_ID;
                                po_delentity.Created_Date = DateTime.Now;
                                po_delentity.IsActive = sche_.IsActive;
                                po_delentity.Created_By = sche_.Created_By;
                                db.MFG_PO_DeliveryDetail.Add(po_delentity);
                                db.SaveChanges();
                            }

                        }
                    }

                }
            }
            catch (Exception)
            {

            }

            return isupdatd;
        }

        public bool Delete(long id, int Userid)
        {
            bool deleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var contact = db.SRM_PurchaseOrderMaster.Find(id);
                contact.IsActive = false;
                contact.Updatedby = Userid;
                contact.Updateddate = DateTime.Now;
                db.SaveChanges();
                deleted = true;

                var Quoid = db.SRM_PurchaseOrderMaster.Where(x => x.Id == id).Select(x => x.QuotationId).FirstOrDefault();
                var PRrecord = db.SRM_QuotationMaster.Find(Quoid);
                PRrecord.IsStatus = 3;
                PRrecord.UpdatedBy = Userid;
                PRrecord.UpdatedDate = DateTime.Now;
                db.SaveChanges();
            }
            return deleted;
        }

        public object getbyPONO(string PONO)
        {
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_SupplierPO_report";
                cmd.Parameters.Add("POno", SqlDbType.NVarChar, 100).Value = PONO;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            return dt;
        }

    }
}
