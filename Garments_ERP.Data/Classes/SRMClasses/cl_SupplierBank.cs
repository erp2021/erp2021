﻿using System;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_SupplierBank
    {
        public List<M_SupplierBankDetailsEntity> get()
        {
            List<M_SupplierBankDetailsEntity> list = new List<M_SupplierBankDetailsEntity>();
            using (var db=new GarmentERPDBEntities())
            {
                var records = db.M_SupplierBankDetails.ToList().Where(x=>x.IsActive==true);
                foreach (var item in records)
                {
                    M_SupplierBankDetailsEntity entity = new M_SupplierBankDetailsEntity();
                    entity.Id = item.Id;
                    entity.Bankcode = item.Bankcode;
                    entity.Bankname = item.Bankname;
                    entity.Accountno = item.Accountno;
                    entity.Createdby = item.Createdby;
                    entity.Createddate = item.Createddate;
                    entity.Supplierid = item.Supplierid;
                    if (item.M_SupplierMaster != null)
                    {
                        M_SupplierMasterEntity supplierentity = new M_SupplierMasterEntity();
                        supplierentity.Id = item.M_SupplierMaster.Id;
                        supplierentity.Suppliername = item.M_SupplierMaster.Suppliername;
                        entity.M_SupplierMaster = supplierentity;
                    }
                    entity.Updatedby = item.Updatedby;
                    entity.Updateddate = item.Updateddate;
                    list.Add(entity);
                }
            }
            return list;
        }
        public M_SupplierBankDetailsEntity GetById(long id)
        {
            M_SupplierBankDetailsEntity entity = new M_SupplierBankDetailsEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_SupplierBankDetails.Where(x =>x.Supplierid==id && x.IsActive == true);
                foreach (var item in records)
                {
                   
                    entity.Id = item.Id;
                    entity.Bankcode = item.Bankcode;
                    entity.Bankname = item.Bankname;
                    entity.Accountno = item.Accountno;
                    entity.Createdby = item.Createdby;
                    entity.Createddate = item.Createddate;
                    entity.Supplierid = item.Supplierid;
                    if (item.M_SupplierMaster !=null)
                    {
                        M_SupplierMasterEntity supplierentity = new M_SupplierMasterEntity();
                        supplierentity.Id = item.M_SupplierMaster.Id;
                        supplierentity.Suppliername = item.M_SupplierMaster.Suppliername;
                        entity.M_SupplierMaster = supplierentity;
                    }
                    entity.Updatedby = item.Updatedby;
                    entity.Updateddate = item.Updateddate;                    
                }
            }
            return entity;
        }
        public long Insert(M_SupplierBankDetailsEntity entity)
        {
            long id = 0;
            using (var db=new GarmentERPDBEntities())
            {
                M_SupplierBankDetails item = new M_SupplierBankDetails();
                item.Id = entity.Id;
                item.Bankcode = entity.Bankcode;
                item.Bankname = entity.Bankname;
                item.Accountno = entity.Accountno;
                item.Createdby = entity.Createdby;
                item.Createddate = entity.Createddate;
                item.IsActive = entity.IsActive;
                item.Supplierid = entity.Supplierid;
                item.Updatedby = entity.Updatedby;
                item.Updateddate = entity.Updateddate;
                db.M_SupplierBankDetails.Add(item);
                db.SaveChanges();
                id = item.Id;
            }
            return id;
        }
        public bool Update(M_SupplierBankDetailsEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_SupplierBankDetails.Where(x => x.Id == entity.Id).FirstOrDefault();
                record.Id = entity.Id;
                record.IsActive = entity.IsActive;
                record.Bankname = entity.Bankname;
                record.Bankcode = entity.Bankcode;
                record.Accountno = entity.Accountno;
                record.Supplierid = entity.Supplierid;              
                record.Updateddate = entity.Updateddate;
                try
                {
                    db.SaveChanges();
                    IsUpdated = true;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return IsUpdated;
        }
    }
}
