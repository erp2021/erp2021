﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_SupplierPRItemDetail
    {
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;

        public bool Update(SRM_PurchaseRequestItemDetailEntity entity)
        {
            bool isupdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SRM_PurchaseRequestItemDetail.Find(entity.Id);
                record.RequiredQty = entity.RequiredQty;
                record.IsActive = entity.IsActive;
                record.UpdatedDate = DateTime.Now;
                record.UpdatedBy = entity.UpdatedBy;
                db.SaveChanges();
                isupdated = true;
            }
            return isupdated;
        }
        public List<SRM_PurchaseRequestItemDetailEntity> getbyid(long id)
        {
            List<SRM_PurchaseRequestItemDetailEntity> list = new List<SRM_PurchaseRequestItemDetailEntity>();
            using (var db = new GarmentERPDBEntities())
            {

                var records = db.SRM_PurchaseRequestItemDetail.Where(m => m.PR_ID == id && m.IsActive == true).ToList();
                foreach (var item in records)
                {
                    SRM_PurchaseRequestItemDetailEntity entity = new SRM_PurchaseRequestItemDetailEntity();
                    entity.Id = item.Id;
                    entity.ItemCategoryId = item.ItemCategoryId;
                    

                    if (item.M_ItemSubCategoryMaster != null)
                    {
                        M_ItemSubCategoryMasterEntity itemcatentity = new M_ItemSubCategoryMasterEntity();
                        itemcatentity.Id = item.M_ItemSubCategoryMaster.Id;
                        itemcatentity.Itemsubcategory = item.M_ItemSubCategoryMaster.Itemsubcategory;
                        entity.M_ItemSubCategoryMaster = itemcatentity;
                    }
                    entity.ItemId = item.ItemId;
                    int IAid=Convert.ToInt32(item.Item_Attribute_ID);
                    int ISid =Convert.ToInt32(item.ItemCategoryId);
                    if (item.M_ItemMaster != null)
                    {
                        M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                        itementity.Id = item.M_ItemMaster.Id;
                        itementity.ItemName = getItemName(item.M_ItemMaster.Id, IAid, ISid);
                        itementity.ItemRate = item.M_ItemMaster.ItemRate;
                        itementity.HScode = item.M_ItemMaster.HScode;
                        entity.M_ItemMaster = itementity;
                    }
                    entity.Item_Attribute_ID = item.Item_Attribute_ID > 0 ? item.Item_Attribute_ID : 0;
                    entity.PR_ID = item.PR_ID;
                    entity.ItemDesc = item.ItemDesc;
                    entity.Comment = item.Comment;
                    entity.UnitId = item.UnitId;
                    entity.orderQty = item.orderQty;
                    entity.RequiredQty = item.RequiredQty;
                    if (item.M_UnitMaster != null)
                    {
                        M_UnitMasterEntity unitentity = new M_UnitMasterEntity();
                        unitentity.Id = item.M_UnitMaster.Id;
                        unitentity.ItemUnit = item.M_UnitMaster.ItemUnit;
                        entity.M_UnitMaster = unitentity;
                    }
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    List<string> rawimgdata = new List<string>();
                    long bomid =(long)db.SRM_PurchaseRequestMaster.Where(x => x.Id == item.PR_ID).Select(x => x.BOM_Id).FirstOrDefault();
                    entity.IsReadymade =db.M_BillOfMaterialMaster.Where(x=>x.BOM_ID== bomid).Select(x=>x.IsReadymate).FirstOrDefault();
                    long bomrawid= (long)db.M_BillOfMaterialDetail.Where(x => x.BOM_ID == bomid && x.ItemId== item.ItemId && x.Item_Attribute_ID== item.Item_Attribute_ID).Select(x => x.ID).FirstOrDefault();
                    var rawimg = db.M_BOMRawItemImage.Where(x => x.BOMrawitemdetailid == bomrawid).ToList();
                    if (rawimg != null)
                    {
                        foreach (var img in rawimg)
                        {
                            if (img.image != "")
                            {
                                rawimgdata.Add(img.image);
                            }
                        }
                    }
                    entity.rawitemimg_list = rawimgdata;
                    list.Add(entity);
                }

            }

            return list;
        }

        public string getItemName(long itemid, int attibid,int subcatid)
        {
            string itemname = "";
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 32; //Check Bill Of Material Create Or Not Using Customer PO 
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value =Convert.ToString(subcatid);
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "<root><Row><ItemId>" + itemid + "</ItemId><AttributeId>" + attibid + "</AttributeId></Row></root>";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                foreach (DataTable table in dt.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        itemname = Convert.ToString(dr["ItemName"]);
                    }
                }

            }
            return itemname;
        }

        public List<SRM_PurchaseRequestItemDetailEntity> get()
        {
            List<SRM_PurchaseRequestItemDetailEntity> list = new List<SRM_PurchaseRequestItemDetailEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_PurchaseRequestItemDetail.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    SRM_PurchaseRequestItemDetailEntity entity = new SRM_PurchaseRequestItemDetailEntity();
                    entity.Id = item.Id;
                    entity.ItemCategoryId = item.ItemCategoryId;
                    if (item.M_ItemSubCategoryMaster != null)
                    {
                        M_ItemSubCategoryMasterEntity itemcatentity = new M_ItemSubCategoryMasterEntity();
                        itemcatentity.Id = item.M_ItemSubCategoryMaster.Id;
                        itemcatentity.Itemsubcategory = item.M_ItemSubCategoryMaster.Itemsubcategory;
                        entity.M_ItemSubCategoryMaster = itemcatentity;
                    }
                    entity.ItemId = item.ItemId;
                    if (item.M_ItemMaster != null)
                    {
                        M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                        itementity.Id = item.M_ItemMaster.Id;
                        itementity.ItemName = item.M_ItemMaster.ItemName;
                        itementity.ItemRate = item.M_ItemMaster.ItemRate;
                        entity.M_ItemMaster = itementity;
                    }
                    entity.PR_ID = item.PR_ID;
                    entity.ItemDesc = item.ItemDesc;
                    entity.Comment = item.Comment;
                    entity.UnitId = item.UnitId;
                    if (item.M_UnitMaster != null)
                    {
                        M_UnitMasterEntity unitentity = new M_UnitMasterEntity();
                        unitentity.Id = item.M_UnitMaster.Id;
                        unitentity.ItemUnit = item.M_UnitMaster.ItemUnit;
                        entity.M_UnitMaster = unitentity;
                    }
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    list.Add(entity);
                }
            }
            return list;
        }
        public bool Delete(long id)
        {
            bool isdeleted = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.SRM_PurchaseRequestItemDetail.Find(id);
                    record.IsActive = false;
                    db.SaveChanges();
                    isdeleted = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return isdeleted;
        }
        public long Insert(SRM_PurchaseRequestItemDetailEntity entity)
        {
            long id = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    SRM_PurchaseRequestItemDetail record = new SRM_PurchaseRequestItemDetail();
                    record.ItemId = entity.ItemId;
                    record.Item_Attribute_ID = entity.Item_Attribute_ID;
                    record.ItemDesc = entity.ItemDesc;
                    record.PR_ID = entity.PR_ID;
                    record.RequiredDate = entity.RequiredDate;
                    record.orderQty = entity.orderQty;
                    record.RequiredQty = entity.RequiredQty;
                    record.ItemCategoryId = entity.ItemCategoryId;
                    record.UnitId = entity.UnitId;
                    record.Comment = entity.Comment;
                    record.IsActive = entity.IsActive;
                    record.CreatedBy = entity.CreatedBy;
                    record.CreatedDate =DateTime.Now;
                    record.Company_ID = entity.Company_ID;
                    record.BranchId = entity.BranchId;
                    record.Item_Attribute_ID = entity.Item_Attribute_ID;
                    db.SRM_PurchaseRequestItemDetail.Add(record);
                    db.SaveChanges();
                    id = record.Id;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return id;
        }
    }
}
