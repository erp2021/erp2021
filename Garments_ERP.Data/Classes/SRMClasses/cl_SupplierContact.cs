﻿using Garments_ERP.Entity;
using Garments_ERP.Data.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_SupplierContact
    {
        public List<M_SupplierContactDetailsEntity> get()
        {
            List<M_SupplierContactDetailsEntity> list = new List<M_SupplierContactDetailsEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_SupplierContactDetails.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {
                    M_SupplierContactDetailsEntity entity = new M_SupplierContactDetailsEntity();
                    entity.Id = item.Id;
                    entity.Contactpersonname = item.Contactpersonname;
                    entity.Department = item.Department;
                    M_DepartmentMasterEntity departentity = new M_DepartmentMasterEntity();
                    if (item.M_DepartmentMaster != null)
                    {
                        departentity.Id = item.M_DepartmentMaster.Id;
                        departentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        entity.M_DepartmentMaster = departentity;
                    }
                    entity.Createdby = item.Createdby;
                    entity.Createddate = item.Createddate;
                    entity.Email = item.Email;
                    entity.IsActive = item.IsActive;
                    M_SupplierMasterEntity supplierentity = new M_SupplierMasterEntity();
                    if (item.M_SupplierMaster != null)
                    {
                        supplierentity.Id = item.M_SupplierMaster.Id;
                        supplierentity.Suppliername = item.M_SupplierMaster.Suppliername;
                        supplierentity.Suppliertype = item.M_SupplierMaster.Suppliertype;
                        supplierentity.Phone = item.M_SupplierMaster.Phone;
                        entity.M_SupplierMaster = supplierentity;
                    }
                    entity.Mobileno = item.Mobileno;
                    entity.Remark = item.Remark;
                    entity.Supplierid = item.Supplierid;
                    entity.Telephoneno = item.Telephoneno;
                    entity.Faxno = item.Faxno;
                    entity.Updatedby = item.Updatedby;
                    entity.Updateddate = item.Updateddate;
                    list.Add(entity);
                }
            }
            return list;
        }
        public List<M_SupplierContactDetailsEntity> GetById(long id)
        {
            List<M_SupplierContactDetailsEntity> list = new List<M_SupplierContactDetailsEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_SupplierContactDetails.Where(x => x.Supplierid == id && x.IsActive == true);
                foreach (var item in records)
                {
                    M_SupplierContactDetailsEntity entity = new M_SupplierContactDetailsEntity();
                    entity.Id = item.Id;
                    entity.Contactpersonname = item.Contactpersonname;
                    entity.Department = item.Department;
                    M_DepartmentMasterEntity departentity = new M_DepartmentMasterEntity();
                    if (item.M_DepartmentMaster != null)
                    {
                        departentity.Id = item.M_DepartmentMaster.Id;
                        departentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        entity.M_DepartmentMaster = departentity;
                    }
                    entity.Createdby = item.Createdby;
                    entity.Createddate = item.Createddate;
                    entity.Email = item.Email;
                    entity.IsActive = item.IsActive;
                    M_SupplierMasterEntity supplierentity = new M_SupplierMasterEntity();
                    if (item.M_SupplierMaster != null)
                    {
                        supplierentity.Id = item.M_SupplierMaster.Id;
                        supplierentity.Suppliername = item.M_SupplierMaster.Suppliername;
                        supplierentity.Suppliertype = item.M_SupplierMaster.Suppliertype;
                        supplierentity.Phone = item.M_SupplierMaster.Phone;
                        entity.M_SupplierMaster = supplierentity;
                    }
                    entity.Mobileno = item.Mobileno;
                    entity.Remark = item.Remark;
                    entity.Supplierid = item.Supplierid;
                    entity.Telephoneno = item.Telephoneno;
                    entity.Faxno = item.Faxno;
                    entity.Updatedby = item.Updatedby;
                    entity.Updateddate = item.Updateddate;
                    list.Add(entity);
                }
            }
            return list;
        }
        public long Insert(M_SupplierContactDetailsEntity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                M_SupplierContactDetails item = new M_SupplierContactDetails();
                item.Id = entity.Id;
                item.Contactpersonname = entity.Contactpersonname;
                item.Department = entity.Department;
                item.Createdby = entity.Createdby;
                item.Createddate = entity.Createddate;
                item.Email = entity.Email;
                item.Faxno = entity.Faxno;
                item.IsActive = entity.IsActive;
                item.Mobileno = entity.Mobileno;
                item.Remark = entity.Remark;
                item.Supplierid = entity.Supplierid;
                item.Telephoneno = entity.Telephoneno;
                item.Updatedby = entity.Updatedby;
                item.Updateddate = entity.Updateddate;
                db.M_SupplierContactDetails.Add(item);
                db.SaveChanges();
                id = item.Id;
            }
            return id;
        }
        public bool Update(M_SupplierContactDetailsEntity entity)
        {
            bool IsUpdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.M_SupplierContactDetails.Where(x => x.Id == entity.Id).FirstOrDefault();
                if (record != null)
                {
                    record.Id = entity.Id;
                    record.IsActive = entity.IsActive;
                    record.Contactpersonname = entity.Contactpersonname;
                    record.Department = entity.Department;
                    record.Email = entity.Email;
                    record.Faxno = entity.Faxno;
                    record.Mobileno = entity.Mobileno;
                    record.Remark = entity.Remark;
                    record.Telephoneno = entity.Telephoneno;
                    record.Updateddate = entity.Updateddate;
                    record.Updatedby = entity.Updatedby;
                    record.Supplierid = entity.Supplierid;
                    record.Updateddate = entity.Updateddate;
                    try
                    {
                        db.SaveChanges();
                        IsUpdated = true;
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }

            }
            return IsUpdated;
        }
    }
}
