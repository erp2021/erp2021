﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Garments_ERP.Data.Admin
{
    public class cl_SupplierQuotationItemDetail
    {
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;

        public long Insert(SRM_QuotationItemDetailEntity record)
        {
            long id = 0;

            using (var db = new GarmentERPDBEntities())
            {
                SRM_QuotationItemDetail entity = new SRM_QuotationItemDetail();
                entity.CreatedBy = record.CreatedBy;
                entity.CreatedDate = DateTime.Now;
                entity.DiscountRate = record.DiscountRate;
                entity.DiscountTypeId = record.DiscountTypeId;
                entity.ItemDesc = record.ItemDesc;
                entity.ItemId = record.ItemId;
                entity.Item_Attribute_ID = record.Item_Attribute_ID;
                entity.ItemRate = record.ItemRate;
                entity.QuotationId = record.QuotationId;
                entity.RateIndicatorId = record.RateIndicatorId;
                entity.UnitId = record.UnitId;
                entity.TotalPrice = record.TotalPrice;
                entity.TotalQty = record.TotalQty;
                entity.IsActive = true;
                entity.Company_ID = record.Company_ID;
                entity.BranchId = record.BranchId;
                db.SRM_QuotationItemDetail.Add(entity);
                db.SaveChanges();
                id = entity.Id;

            }
            return id;
        }


        public bool Update(SRM_QuotationItemDetailEntity entity)
        {
            bool isupdated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SRM_QuotationItemDetail.Find(entity.Id);
                record.Id = entity.Id;
                record.DiscountRate = entity.DiscountRate;
                record.DiscountTypeId = entity.DiscountTypeId;
                record.ItemDesc = entity.ItemDesc;
                record.ItemId = entity.ItemId;
                record.Item_Attribute_ID = entity.Item_Attribute_ID;
                record.ItemRate = entity.ItemRate;
                record.QuotationId = entity.QuotationId;
                record.RateIndicatorId = entity.RateIndicatorId;
                //record.UnitId = entity.UnitId;
                record.TotalPrice = entity.TotalPrice;
                record.TotalQty = entity.TotalQty;
                record.UpdatedBy = entity.UpdatedBy;
                record.UpdatedDate =DateTime.Now;
                record.IsActive = true;
                db.SaveChanges();
                isupdated = true;
            }

            return isupdated;
        }
        public List<SRM_QuotationItemDetailEntity> getbyquotationid(long id)
        {
            cl_Tax taxobj = new cl_Tax();


            List<SRM_QuotationItemDetailEntity> entitylist = new List<SRM_QuotationItemDetailEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_QuotationItemDetail.Where(x => x.QuotationId == id && x.IsActive == true).ToList();
                foreach (var item in records)
                {
                    SRM_QuotationItemDetailEntity entity = new SRM_QuotationItemDetailEntity();
                    entity.Id = item.Id;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.DiscountRate = item.DiscountRate;
                    entity.DiscountTypeId = item.DiscountTypeId;
                    // entity.taxlist = taxlist_;
                    if (item.M_DiscountTypeMaster != null)
                    {
                        M_DiscountTypeMasterEntity discountentity = new M_DiscountTypeMasterEntity();
                        discountentity.Id = item.M_DiscountTypeMaster.Id;
                        discountentity.DiscountTypeName = item.M_DiscountTypeMaster.DiscountTypeName;
                        discountentity.ShortName = item.M_DiscountTypeMaster.ShortName;
                        entity.M_DiscountTypeMaster = discountentity;
                    }
                    entity.ItemDesc = item.ItemDesc;
                    entity.ItemId = item.ItemId;
                    
                    entity.UnitId = item.UnitId;
                    if (item.M_UnitMaster != null)
                    {
                        M_UnitMasterEntity unitentity = new M_UnitMasterEntity();
                        unitentity.ItemUnit = item.M_UnitMaster.ItemUnit;
                        unitentity.Id = item.M_UnitMaster.Id;
                        entity.M_UnitMaster = unitentity;
                    }
                    int IAid = Convert.ToInt32(item.Item_Attribute_ID);
                    int ISid = 0;
                    if (item.M_ItemMaster != null)
                    {
                        M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                        itementity.Id = item.M_ItemMaster.Id;
                        itementity.ItemName = getItemName(item.M_ItemMaster.Id, IAid, ISid);;
                        itementity.ItemRate = item.M_ItemMaster.ItemRate;
                        itementity.OpeningStock = item.M_ItemMaster.OpeningStock;
                        itementity.EOQ = item.M_ItemMaster.EOQ;
                        itementity.HScode= item.M_ItemMaster.HScode;
                        //entity.M_UnitMaster = unitentity;
                        entity.M_ItemMaster = itementity;
                        
                    }
                    entity.Item_Attribute_ID = item.Item_Attribute_ID > 0 ? item.Item_Attribute_ID : 0;
                    entity.ItemRate = item.ItemRate;
                    entity.QuotationId = item.QuotationId;
                    if (item.SRM_QuotationMaster != null)
                    {
                        SRM_QuotationMasterEntity quotationentity = new SRM_QuotationMasterEntity();
                        quotationentity.Id = item.SRM_QuotationMaster.Id;
                        quotationentity.PR_No = item.SRM_QuotationMaster.PR_No;
                        quotationentity.QuotationNo = item.SRM_QuotationMaster.QuotationNo;
                        quotationentity.SupplierId = item.SRM_QuotationMaster.SupplierId;
                        quotationentity.GSTTax = item.SRM_QuotationMaster.GSTTax;
                        quotationentity.quotdate = Convert.ToString(string.Format("{0:dd/MM/yyyy}", item.SRM_QuotationMaster.QuotationDate));
                       
                        //DateTime dt= DateTime.ParseExact(item.SRM_QuotationMaster.QuotationDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        //quotationentity.QuotationDate = dt;
                 
                        if (item.SRM_QuotationMaster.SupplierId != null)
                        {
                            M_LedgersEntity legderentity = new M_LedgersEntity();
                            var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.SRM_QuotationMaster.SupplierId).SingleOrDefault();
                            if (custdata != null)
                            {
                                legderentity.Ledger_Id = custdata.Ledger_Id;
                                legderentity.Ledger_Name = custdata.Ledger_Name;
                            }
                            else
                            {
                                legderentity.Ledger_Id = Convert.ToInt32(item.SRM_QuotationMaster.SupplierId);
                                legderentity.Ledger_Name = "--";
                            }
                            quotationentity.M_Ledgersentity = legderentity;
                            entity.SRM_QuotationMaster = quotationentity;
                        }



                    }
                    entity.RateIndicatorId = item.RateIndicatorId;
                    if (item.M_RateIndicatorMaster != null)
                    {
                        M_RateIndicatorMasterEntity rateentity = new M_RateIndicatorMasterEntity();
                        rateentity.Id = item.M_RateIndicatorMaster.Id;
                        rateentity.RateIndicatorName = item.M_RateIndicatorMaster.RateIndicatorName;
                        rateentity.ShortName = item.M_RateIndicatorMaster.ShortName;
                        entity.M_RateIndicatorMaster = rateentity;
                    }
                    entity.TotalPrice = item.TotalPrice;
                    entity.TotalQty = item.TotalQty;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.IsActive = item.IsActive;
                    List<string> rawimgdata = new List<string>();
                    long SPR =(long)db.SRM_QuotationMaster.Where(x => x.Id == item.QuotationId).Select(x => x.PR_Id).FirstOrDefault();
                    long bomid = (long)db.SRM_PurchaseRequestMaster.Where(x => x.Id == SPR).Select(x => x.BOM_Id).FirstOrDefault();
                    entity.IsReadymade = db.M_BillOfMaterialMaster.Where(x => x.BOM_ID == bomid).Select(x => x.IsReadymate).FirstOrDefault();
                    long bomrawid = (long)db.M_BillOfMaterialDetail.Where(x => x.BOM_ID == bomid && x.ItemId == item.ItemId && x.Item_Attribute_ID == item.Item_Attribute_ID).Select(x => x.ID).FirstOrDefault();
                    var rawimg = db.M_BOMRawItemImage.Where(x => x.BOMrawitemdetailid == bomrawid).ToList();
                    if (rawimg != null)
                    {
                        foreach (var img in rawimg)
                        {
                            if (img.image != "")
                            {
                                rawimgdata.Add(img.image);
                            }
                        }
                    }
                    entity.rawitemimg_list = rawimgdata;


                    entitylist.Add(entity);
                }
            }
            return entitylist;
        }

        public string getItemName(long itemid, int attibid, int subcatid)
        {
            string itemname = "";
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 32; //Check Bill Of Material Create Or Not Using Customer PO 
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = Convert.ToString(subcatid);
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "<root><Row><ItemId>" + itemid + "</ItemId><AttributeId>" + attibid + "</AttributeId></Row></root>";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                foreach (DataTable table in dt.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        itemname = Convert.ToString(dr["ItemName"]);
                    }
                }

            }
            return itemname;
        }

        public List<SRM_QuotationItemDetailEntity> getbyquotationid()
        {
            List<SRM_QuotationItemDetailEntity> entitylist = new List<SRM_QuotationItemDetailEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_QuotationItemDetail.Where(x => x.IsActive == true).ToList();
                foreach (var item in records)
                {
                    SRM_QuotationItemDetailEntity entity = new SRM_QuotationItemDetailEntity();
                    entity.Id = item.Id;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.DiscountRate = item.DiscountRate;
                    entity.DiscountTypeId = item.DiscountTypeId;
                    if (item.M_DiscountTypeMaster != null)
                    {
                        M_DiscountTypeMasterEntity discountentity = new M_DiscountTypeMasterEntity();
                        discountentity.Id = item.M_DiscountTypeMaster.Id;
                        discountentity.DiscountTypeName = item.M_DiscountTypeMaster.DiscountTypeName;
                        discountentity.ShortName = item.M_DiscountTypeMaster.ShortName;
                        entity.M_DiscountTypeMaster = discountentity;
                    }
                    entity.ItemDesc = item.ItemDesc;
                    entity.ItemId = item.ItemId;
                    entity.UnitId = item.UnitId;
                    if (item.M_UnitMaster != null)
                    {
                        M_UnitMasterEntity unitentity = new M_UnitMasterEntity();
                        unitentity.ItemUnit = item.M_ItemMaster.M_UnitMaster.ItemUnit;
                        unitentity.Id = item.M_ItemMaster.M_UnitMaster.Id;
                        entity.M_UnitMaster = unitentity;
                    }
                    if (item.M_ItemMaster != null)
                    {
                        M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                        itementity.Id = item.M_ItemMaster.Id;
                        itementity.ItemName = item.M_ItemMaster.ItemName;
                        itementity.ItemRate = item.M_ItemMaster.ItemRate;
                        itementity.OpeningStock = item.M_ItemMaster.OpeningStock;
                        entity.M_ItemMaster = itementity;
                    }
                    entity.ItemRate = item.ItemRate;
                    entity.QuotationId = item.QuotationId;
                    if (item.SRM_QuotationMaster != null)
                    {
                        SRM_QuotationMasterEntity quotationentity = new SRM_QuotationMasterEntity();
                        quotationentity.Id = item.SRM_QuotationMaster.Id;
                        quotationentity.PR_No = item.SRM_QuotationMaster.PR_No;
                        quotationentity.QuotationNo = item.SRM_QuotationMaster.QuotationNo;
                        quotationentity.SupplierId = item.SRM_QuotationMaster.SupplierId;
                        entity.SRM_QuotationMaster = quotationentity;
                    }
                    entity.RateIndicatorId = item.RateIndicatorId;
                    if (item.M_RateIndicatorMaster != null)
                    {
                        M_RateIndicatorMasterEntity rateentity = new M_RateIndicatorMasterEntity();
                        rateentity.Id = item.M_RateIndicatorMaster.Id;
                        rateentity.RateIndicatorName = item.M_RateIndicatorMaster.RateIndicatorName;
                        rateentity.ShortName = item.M_RateIndicatorMaster.ShortName;
                        entity.M_RateIndicatorMaster = rateentity;
                    }
                    entity.TotalPrice = item.TotalPrice;
                    entity.TotalQty = item.TotalQty;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.IsActive = item.IsActive;
                    entitylist.Add(entity);
                }
            }
            return entitylist;
        }

        public bool deleterawitem(long id)
        {
            bool isdeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SRM_QuotationItemDetail.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                isdeleted = true;
            }

            return isdeleted;
        }
    }
}
