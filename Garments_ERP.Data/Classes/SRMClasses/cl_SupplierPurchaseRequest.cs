﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_SupplierPurchaseRequest
    {
        cl_role rlobj = new cl_role();
        public string getnextprno()
        {
            string no;
            no = "PR-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SRM_PurchaseRequestMaster.FirstOrDefault() != null ? db.SRM_PurchaseRequestMaster.Max(x => x.Id) + 1 : 1;
                no = no + "-" + record.ToString();
            }

            return no;
        }

        public long getnextprID()
        {
            long no;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SRM_PurchaseRequestMaster.FirstOrDefault() != null ? db.SRM_PurchaseRequestMaster.Max(x => x.Id) + 1 : 1;
                no =Convert.ToInt64(record.ToString());
            }

            return no;
        }

        ////List Of Quotation With Status and User and Form Date And To Date Wise
        public List<SRM_PurchaseRequestMasterEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<SRM_PurchaseRequestMasterEntity> list = new List<SRM_PurchaseRequestMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_PurchaseRequestMaster.Where(x => x.IsActive == true && x.PR_Date >= from && x.PR_Date <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy == Userid).OrderByDescending(x=>x.Id).ToList();
                if (records.Count >= 0 && roleid != 4)
                {
                    records = db.SRM_PurchaseRequestMaster.Where(x => x.IsActive == true && x.PR_Date >= from && x.PR_Date <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).OrderByDescending(x => x.Id).ToList();
                   
                }

                foreach (var item in records)
                {
                    SRM_PurchaseRequestMasterEntity entity = new SRM_PurchaseRequestMasterEntity();
                    entity.Id = item.Id;
                    entity.AccountHeadId = item.AccountHeadId;
                    if (item.AccountHeadId != null)
                    {
                        EmployeeEntity empentity = new EmployeeEntity();
                        empentity.Name = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.Name).FirstOrDefault();
                        empentity.ID = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.ID).FirstOrDefault();
                        empentity.EmpID = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.EmpID).FirstOrDefault();
                        empentity.UserId = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.UserId).FirstOrDefault();
                        entity.Employee = empentity;
                    }
                    entity.AdditionalInfo = item.AdditionalInfo;
                    entity.ApprovalStatus = item.ApprovalStatus;
                    entity.BOM_Id = item.BOM_Id;
                    entity.BOM_Date = item.BOM_Date;
                    entity.EnquiryId = item.EnquiryId;
                    if (item.EnquiryId != null && item.EnquiryId>0)
                    {
                        var EnqData = db.CRM_EnquiryMaster.Where(x => x.Id == item.EnquiryId).FirstOrDefault();
                        if (EnqData != null)
                        {
                            CRM_EnquiryMasterEntity enqentity = new CRM_EnquiryMasterEntity();
                            enqentity.EnquiryId = EnqData.Id;
                            enqentity.Enquiryno = EnqData.Enquiryno;
                            entity.CRM_EnquiryMaster = enqentity;
                        }
                    }
                    entity.StyleId = item.StyleId;
                    if (item.M_StyleMaster != null)
                    {
                        M_StyleMasterEntity styleentity = new M_StyleMasterEntity();
                        styleentity.Id = item.M_StyleMaster.Id;
                        styleentity.Stylename = item.M_StyleMaster.Stylename;
                        entity.M_StyleMaster = styleentity;
                    }
                    if (item.M_BillOfMaterialMaster != null)
                    {
                        M_BillOfMaterialMasterEntity bomentity = new M_BillOfMaterialMasterEntity();
                        bomentity.BOM_ID = item.M_BillOfMaterialMaster.BOM_ID;
                        bomentity.BOM_No = item.M_BillOfMaterialMaster.BOM_No;
                        entity.M_BillOfMaterialMaster = bomentity;
                    }
                    entity.DepartmentId = item.DepartmentId;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                        deptentity.Id = item.M_DepartmentMaster.Id;
                        deptentity.DepartmentNo = item.M_DepartmentMaster.DepartmentNo;
                        deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        deptentity.Departmentdesc = item.M_DepartmentMaster.Departmentdesc;
                        deptentity.Shortname = item.M_DepartmentMaster.Shortname;
                        entity.M_DepartmentMaster = deptentity;
                    }
                    entity.PR_Date = item.PR_Date;
                    entity.PR_No = item.PR_No;
                    entity.Priority = item.Priority;
                    entity.Comment = item.Comment;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;

                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsReadymade = item.IsReadymade;
                    list.Add(entity);
                }
                if (roleid != 4)
                {
                    var Userlist = rlobj.GetUserRole().Where(x => x.roleId == 4 || x.roleId == roleid).ToList();
                    list = list.Where(x => x.IsActive == true && x.PR_Date >= from && x.PR_Date <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && Userlist.Any(y => y.UserId == x.CreatedBy)).OrderByDescending(x => x.Id).ToList();

                }
            }
            return list;
        }

        //Get Account Head Name With Department Wise
        public List<EmployeeEntity> GetAccountHead(int deptid, int Company_ID, int BranchId)
        {
            List<EmployeeEntity> list = new List<EmployeeEntity>();
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.M_User_Department.Where(x => x.DepartmentId == deptid && x.CompanyId == Company_ID && x.BranchId == BranchId && x.IsActive==true).ToList();
                    foreach(var item in record)
                    {
                        EmployeeEntity ent=new EmployeeEntity();
                        ent.UserId =Convert.ToInt32(item.UserId);
                        ent.Name = db.Employees.Where(x=>x.UserId==item.UserId && x.IsActive==true).Select(x=>x.Name).FirstOrDefault();
                        list.Add(ent);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return list;
        }


        //List Of Quotation With Status and User Wise
        public List<SRM_PurchaseRequestMasterEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<SRM_PurchaseRequestMasterEntity> list = new List<SRM_PurchaseRequestMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_PurchaseRequestMaster.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy == Userid).OrderByDescending(x=>x.Id).ToList();
                if (records.Count >= 0 && roleid != 4)
                {
                    records = db.SRM_PurchaseRequestMaster.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).OrderByDescending(x => x.Id).ToList();
                }

                foreach (var item in records)
                {
                    SRM_PurchaseRequestMasterEntity entity = new SRM_PurchaseRequestMasterEntity();
                    entity.Id = item.Id;
                    entity.AccountHeadId = item.AccountHeadId;
                    if (item.AccountHeadId != null)
                    {
                        EmployeeEntity empentity = new EmployeeEntity();
                        empentity.Name = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x=>x.Name).FirstOrDefault();
                        empentity.ID = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.ID).FirstOrDefault();
                        empentity.EmpID = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.EmpID).FirstOrDefault(); 
                        empentity.UserId = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.UserId).FirstOrDefault(); 
                        entity.Employee = empentity;
                    }
                    entity.AdditionalInfo = item.AdditionalInfo;
                    entity.ApprovalStatus = item.ApprovalStatus;
                    entity.BOM_Id = item.BOM_Id;
                    entity.BOM_Date = item.BOM_Date;
                    entity.EnquiryId = item.EnquiryId;
                    if (item.EnquiryId != null && item.EnquiryId>0)
                    {
                        var EnqData = db.CRM_EnquiryMaster.Where(x => x.Id == item.EnquiryId).FirstOrDefault();
                        if (EnqData != null)
                        {
                            CRM_EnquiryMasterEntity enqentity = new CRM_EnquiryMasterEntity();
                            enqentity.EnquiryId = EnqData.Id;
                            enqentity.Enquiryno = EnqData.Enquiryno;
                            entity.CRM_EnquiryMaster = enqentity;
                        }
                    }
                    entity.StyleId = item.StyleId;
                    if (item.M_StyleMaster != null)
                    {
                        M_StyleMasterEntity styleentity = new M_StyleMasterEntity();
                        styleentity.Id = item.M_StyleMaster.Id;
                        styleentity.Stylename = item.M_StyleMaster.Stylename;
                        entity.M_StyleMaster = styleentity;
                    }
                    if (item.M_BillOfMaterialMaster != null)
                    {
                        M_BillOfMaterialMasterEntity bomentity = new M_BillOfMaterialMasterEntity();
                        bomentity.BOM_ID = item.M_BillOfMaterialMaster.BOM_ID;
                        bomentity.BOM_No = item.M_BillOfMaterialMaster.BOM_No;
                        entity.M_BillOfMaterialMaster = bomentity;
                    }
                    entity.DepartmentId = item.DepartmentId;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                        deptentity.Id = item.M_DepartmentMaster.Id;
                        deptentity.DepartmentNo = item.M_DepartmentMaster.DepartmentNo;
                        deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        deptentity.Departmentdesc = item.M_DepartmentMaster.Departmentdesc;
                        deptentity.Shortname = item.M_DepartmentMaster.Shortname;
                        entity.M_DepartmentMaster = deptentity;
                    }
                    entity.PR_Date = item.PR_Date;
                    entity.PR_No = item.PR_No;
                    entity.Priority = item.Priority;
                    entity.Comment = item.Comment;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsReadymade = item.IsReadymade;

                    list.Add(entity);
                }
                if ( roleid != 4)
                {
                    var Userlist = rlobj.GetUserRole().Where(x => x.roleId == 4 || x.roleId == roleid).ToList();
                    list = list.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && Userlist.Any(y => y.UserId == x.CreatedBy)).OrderByDescending(x => x.Id).ToList();
                }
            }

            return list;

        }


        //Reject PR to update Status
        public bool RejectedPR(long id, int Uid)
        {
            bool isdeleted = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.SRM_PurchaseRequestMaster.Find(id);
                    
                    record.IsStatus = 5;
                    record.UpdatedDate = DateTime.Now;
                    record.UpdatedBy = Uid;
                    db.SaveChanges();
                    isdeleted = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isdeleted;
        }

        public bool Update(SRM_PurchaseRequestMasterEntity entity)
        {
            bool isupdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SRM_PurchaseRequestMaster.Find(entity.Id);
                record.AccountHeadId = entity.AccountHeadId;
                record.AdditionalInfo = entity.AdditionalInfo;
                record.ApprovalStatus = entity.ApprovalStatus;           
                record.DepartmentId = entity.DepartmentId;
                record.PR_Date = entity.PR_Date;            
                record.Priority = entity.Priority;
                record.IsActive = entity.IsActive;
                record.Comment = entity.Comment;
                record.UpdatedDate = DateTime.Now;
                record.UpdatedBy = entity.UpdatedBy;
                record.IsStatus = entity.IsStatus;
                record.ApprovalDate = entity.Approvaldate;
                db.SaveChanges();
                isupdated = true;
            }
            return isupdated;
        }
        public SRM_PurchaseRequestMasterEntity getbyid(long id)
        {
            SRM_PurchaseRequestMasterEntity entity = new SRM_PurchaseRequestMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.SRM_PurchaseRequestMaster.Find(id);
                entity.Id = item.Id;
                entity.AccountHeadId = item.AccountHeadId;
                if (item.AccountHeadId != null)
                {
                    EmployeeEntity empentity = new EmployeeEntity();
                    empentity.Name = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.Name).FirstOrDefault();
                    empentity.ID = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.ID).FirstOrDefault();
                    empentity.EmpID = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.EmpID).FirstOrDefault();
                    empentity.UserId = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.UserId).FirstOrDefault();
                    entity.Employee = empentity;
                }
                entity.AdditionalInfo = item.AdditionalInfo;
                entity.ApprovalStatus = item.ApprovalStatus;
                entity.Approvaldate = item.ApprovalDate;
                entity.BOM_Id = item.BOM_Id;
                entity.BOM_Date = item.BOM_Date;
                entity.EnquiryId = item.EnquiryId;
                if (item.EnquiryId != null && item.EnquiryId>0)
                {
                    var EnqData = db.CRM_EnquiryMaster.Where(x => x.Id == item.EnquiryId).FirstOrDefault();
                    if (EnqData != null)
                    {
                        CRM_EnquiryMasterEntity enqentity = new CRM_EnquiryMasterEntity();
                        enqentity.EnquiryId = EnqData.Id;
                        enqentity.Enquiryno = EnqData.Enquiryno;
                        entity.CRM_EnquiryMaster = enqentity;
                    }
                }
                else
                {
                    entity.POid = item.POid;
                    var qtid = db.CRM_CustomerPOMaster.Where(x => x.Id == item.POid && x.IsActive == true).Select(x => x.QuotID).SingleOrDefault();
                    var Enqno = db.CRM_QuotationMaster.Where(x => x.Id == qtid).Select(x => x.Enquiryrefno).SingleOrDefault();
                    CRM_EnquiryMasterEntity enquiryentity = new CRM_EnquiryMasterEntity();
                    enquiryentity.Enquiryno = Enqno;
                    entity.CRM_EnquiryMaster = enquiryentity;
                }
                
                entity.StyleId = item.StyleId;
                if (item.M_StyleMaster != null)
                {
                    M_StyleMasterEntity styleentity = new M_StyleMasterEntity();
                    styleentity.Id = item.M_StyleMaster.Id;
                    styleentity.Stylename = item.M_StyleMaster.Stylename;
                    entity.M_StyleMaster = styleentity;
                }
                if (item.M_BillOfMaterialMaster != null)
                {
                    M_BillOfMaterialMasterEntity bomentity = new M_BillOfMaterialMasterEntity();
                    bomentity.BOM_ID = item.M_BillOfMaterialMaster.BOM_ID;
                    bomentity.BOM_No = item.M_BillOfMaterialMaster.BOM_No;
                    bomentity.IsReadymate= item.M_BillOfMaterialMaster.IsReadymate;
                    entity.M_BillOfMaterialMaster = bomentity;
                }
                entity.DepartmentId = item.DepartmentId;
                if (item.M_DepartmentMaster != null)
                {
                    M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                    deptentity.Id = item.M_DepartmentMaster.Id;
                    deptentity.DepartmentNo = item.M_DepartmentMaster.DepartmentNo;
                    deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                    deptentity.Departmentdesc = item.M_DepartmentMaster.Departmentdesc;
                    deptentity.Shortname = item.M_DepartmentMaster.Shortname;
                    entity.M_DepartmentMaster = deptentity;
                }
                entity.PR_Date = item.PR_Date;
                entity.PR_No = item.PR_No;
                entity.Priority = item.Priority;
                entity.Comment = item.Comment;
                entity.CreatedDate = item.CreatedDate;
                entity.CreatedBy = item.CreatedBy;
                entity.UpdatedDate = item.UpdatedDate;
                entity.UpdatedBy = item.UpdatedBy;
                entity.IsActive = item.IsActive;
                entity.IsStatus = item.IsStatus;
                entity.Company_ID = item.Company_ID;
                entity.BranchId = item.BranchId;
                entity.IsStatus = item.IsStatus;
                entity.Company_ID = item.Company_ID;
                entity.BranchId = item.BranchId;
                entity.IsReadymade = item.IsReadymade;
            }

            return entity;
        }

        public List<SRM_PurchaseRequestMasterEntity> get(DateTime from,DateTime to)
        {
            List<SRM_PurchaseRequestMasterEntity> list = new List<SRM_PurchaseRequestMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_PurchaseRequestMaster.ToList().Where(x => x.IsActive == true && x.PR_Date>=from && x.PR_Date<=to);
                foreach (var item in records)
                {
                    SRM_PurchaseRequestMasterEntity entity = new SRM_PurchaseRequestMasterEntity();
                    entity.Id = item.Id;
                    entity.AccountHeadId = item.AccountHeadId;
                    if (item.AccountHeadId != null)
                    {
                        EmployeeEntity empentity = new EmployeeEntity();
                        empentity.Name = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.Name).FirstOrDefault();
                        empentity.ID = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.ID).FirstOrDefault();
                        empentity.EmpID = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.EmpID).FirstOrDefault();
                        empentity.UserId = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.UserId).FirstOrDefault();
                        entity.Employee = empentity;
                    }
                    entity.AdditionalInfo = item.AdditionalInfo;
                    entity.ApprovalStatus = item.ApprovalStatus;
                    entity.BOM_Id = item.BOM_Id;
                    entity.BOM_Date = item.BOM_Date;
                    entity.EnquiryId = item.EnquiryId;
                    if (item.EnquiryId != null && item.EnquiryId>0)
                    {
                        var EnqData = db.CRM_EnquiryMaster.Where(x => x.Id == item.EnquiryId).FirstOrDefault();
                        if (EnqData != null)
                        {
                            CRM_EnquiryMasterEntity enqentity = new CRM_EnquiryMasterEntity();
                            enqentity.EnquiryId = EnqData.Id;
                            enqentity.Enquiryno = EnqData.Enquiryno;
                            entity.CRM_EnquiryMaster = enqentity;
                        }
                    }
                    entity.StyleId = item.StyleId;
                    if (item.M_StyleMaster != null)
                    {
                        M_StyleMasterEntity styleentity = new M_StyleMasterEntity();
                        styleentity.Id = item.M_StyleMaster.Id;
                        styleentity.Stylename = item.M_StyleMaster.Stylename;
                        entity.M_StyleMaster = styleentity;
                    }
                    if (item.M_BillOfMaterialMaster != null)
                    {
                        M_BillOfMaterialMasterEntity bomentity = new M_BillOfMaterialMasterEntity();
                        bomentity.BOM_ID = item.M_BillOfMaterialMaster.BOM_ID;
                        bomentity.BOM_No = item.M_BillOfMaterialMaster.BOM_No;
                        entity.M_BillOfMaterialMaster = bomentity;
                    }
                    entity.DepartmentId = item.DepartmentId;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                        deptentity.Id = item.M_DepartmentMaster.Id;
                        deptentity.DepartmentNo = item.M_DepartmentMaster.DepartmentNo;
                        deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        deptentity.Departmentdesc = item.M_DepartmentMaster.Departmentdesc;
                        deptentity.Shortname = item.M_DepartmentMaster.Shortname;
                        entity.M_DepartmentMaster = deptentity;
                    }
                    entity.PR_Date = item.PR_Date;
                    entity.PR_No = item.PR_No;
                    entity.Priority = item.Priority;
                    entity.Comment = item.Comment;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsReadymade = item.IsReadymade;
                    list.Add(entity);
                }
            }
            return list;
        }


        public List<SRM_PurchaseRequestMasterEntity> get()
        {
            List<SRM_PurchaseRequestMasterEntity> list = new List<SRM_PurchaseRequestMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_PurchaseRequestMaster.ToList().Where(x => x.IsActive == true).OrderByDescending(x => x.Id);
                foreach (var item in records)
                {
                    SRM_PurchaseRequestMasterEntity entity = new SRM_PurchaseRequestMasterEntity();
                    entity.Id = item.Id;
                    entity.AccountHeadId = item.AccountHeadId;
                    if (item.AccountHeadId != null)
                    {
                        EmployeeEntity empentity = new EmployeeEntity();
                        empentity.Name = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.Name).FirstOrDefault();
                        empentity.ID = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.ID).FirstOrDefault();
                        empentity.EmpID = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.EmpID).FirstOrDefault();
                        empentity.UserId = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.UserId).FirstOrDefault();
                        entity.Employee = empentity;
                    }
                    entity.AdditionalInfo = item.AdditionalInfo;
                    entity.ApprovalStatus = item.ApprovalStatus;
                    entity.BOM_Id = item.BOM_Id;
                    entity.BOM_Date = item.BOM_Date;
                    entity.EnquiryId = item.EnquiryId;
                    if (item.EnquiryId != null && item.EnquiryId>0)
                    {
                        var EnqData = db.CRM_EnquiryMaster.Where(x => x.Id == item.EnquiryId).FirstOrDefault();
                        if (EnqData != null)
                        {
                            CRM_EnquiryMasterEntity enqentity = new CRM_EnquiryMasterEntity();
                            enqentity.EnquiryId = EnqData.Id;
                            enqentity.Enquiryno = EnqData.Enquiryno;
                            entity.CRM_EnquiryMaster = enqentity;
                        }
                    }
                    entity.StyleId = item.StyleId;
                    if (item.M_StyleMaster != null)
                    {
                        M_StyleMasterEntity styleentity = new M_StyleMasterEntity();
                        styleentity.Id = item.M_StyleMaster.Id;
                        styleentity.Stylename = item.M_StyleMaster.Stylename;
                        entity.M_StyleMaster = styleentity;
                    }
                    if (item.M_BillOfMaterialMaster != null)
                    {
                        M_BillOfMaterialMasterEntity bomentity = new M_BillOfMaterialMasterEntity();
                        bomentity.BOM_ID = item.M_BillOfMaterialMaster.BOM_ID;
                        bomentity.BOM_No = item.M_BillOfMaterialMaster.BOM_No;                       
                        entity.M_BillOfMaterialMaster = bomentity;
                    }
                    entity.DepartmentId = item.DepartmentId;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                        deptentity.Id = item.M_DepartmentMaster.Id;
                        deptentity.DepartmentNo = item.M_DepartmentMaster.DepartmentNo;
                        deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        deptentity.Departmentdesc = item.M_DepartmentMaster.Departmentdesc;
                        deptentity.Shortname = item.M_DepartmentMaster.Shortname;
                        entity.M_DepartmentMaster = deptentity;
                    }
                    entity.PR_Date = item.PR_Date;
                    entity.PR_No = item.PR_No;
                    entity.Priority = item.Priority;
                    entity.Comment = item.Comment;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsReadymade = item.IsReadymade;
                    list.Add(entity);
                }
            }
            return list;
        }
        public bool Delete(long id,int Userid)
        {
            bool isdeleted = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.SRM_PurchaseRequestMaster.Find(id);
                    record.IsActive = false;
                    record.UpdatedBy = Userid;
                    record.UpdatedDate = DateTime.Now;
                    db.SaveChanges();
                    isdeleted = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isdeleted;
        }
        public long Insert(SRM_PurchaseRequestMasterEntity entity)
        {
            long id = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    SRM_PurchaseRequestMaster record = new SRM_PurchaseRequestMaster();
                    record.Id = getnextprID();
                    record.AccountHeadId = entity.AccountHeadId;
                    record.AdditionalInfo = entity.AdditionalInfo;
                    record.ApprovalStatus = entity.ApprovalStatus;
                    record.BOM_Id = entity.BOM_Id;
                    record.BOM_Date = entity.BOM_Date;

                    if (entity.EnquiryId != null)
                    {
                        record.EnquiryId = entity.EnquiryId;
                    }
                    else
                    {
                        record.POid = entity.POid;
                    }
                    record.StyleId = entity.StyleId;
                    record.DepartmentId = entity.DepartmentId;
                    record.PR_Date = entity.PR_Date;
                    record.PR_No =Convert.ToString(getnextprno());
                    record.Priority = entity.Priority;
                    record.IsActive = entity.IsActive;
                    record.Comment = entity.Comment;
                    record.CreatedBy = entity.CreatedBy;
                    record.CreatedDate = DateTime.Now;
                    record.ApprovalDate = entity.Approvaldate;
                    record.IsStatus = entity.IsStatus;
                    record.Company_ID = entity.Company_ID;
                    record.BranchId = entity.BranchId;
                    record.IsReadymade = entity.IsReadymade;
                    if (entity.IsReadymade==0)
                    {
                        record.PR_No = "RM-"  + Convert.ToString(getnextprno());
                    }
                    else
                    {
                        record.PR_No = "RF-" + Convert.ToString(getnextprno());
                    }
                    db.SRM_PurchaseRequestMaster.Add(record);
                    db.SaveChanges();
                    id = record.Id;
                }   
            }
            catch (Exception ex)
            {

            }
            return id;
        }

        public List<SRM_PurchaseRequestMasterEntity> getList(int companyid,int branchid)
        {
            List<SRM_PurchaseRequestMasterEntity> list = new List<SRM_PurchaseRequestMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var PR_Idid = db.SRM_QuotationMaster.Where(x => x.IsActive == true && x.Company_ID==companyid && x.BranchId==branchid).Select(u => u.PR_Id).ToArray();

                var records = db.SRM_PurchaseRequestMaster.ToList().Where(x => x.IsActive == true && !PR_Idid.Contains(x.Id) && x.Company_ID == companyid && x.BranchId == branchid && x.IsStatus==3).OrderByDescending(x => x.Id);
                foreach (var item in records)
                {
                    SRM_PurchaseRequestMasterEntity entity = new SRM_PurchaseRequestMasterEntity();
                    entity.Id = item.Id;
                    entity.AccountHeadId = item.AccountHeadId;
                    if (item.AccountHeadId != null)
                    {
                        EmployeeEntity empentity = new EmployeeEntity();
                        empentity.Name = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.Name).FirstOrDefault();
                        empentity.ID = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.ID).FirstOrDefault();
                        empentity.EmpID = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.EmpID).FirstOrDefault();
                        empentity.UserId = db.Employees.Where(x => x.UserId == item.AccountHeadId).Select(x => x.UserId).FirstOrDefault();
                        entity.Employee = empentity;
                    }
                    entity.AdditionalInfo = item.AdditionalInfo;
                    entity.ApprovalStatus = item.ApprovalStatus;
                    entity.BOM_Id = item.BOM_Id;
                    entity.BOM_Date = item.BOM_Date;
                    entity.EnquiryId = item.EnquiryId;
                    if (item.EnquiryId != null && item.EnquiryId>0)
                    {
                        var EnqData = db.CRM_EnquiryMaster.Where(x => x.Id == item.EnquiryId).FirstOrDefault();
                        if (EnqData != null)
                        {
                            CRM_EnquiryMasterEntity enqentity = new CRM_EnquiryMasterEntity();
                            enqentity.EnquiryId = EnqData.Id;
                            enqentity.Enquiryno = EnqData.Enquiryno;
                            entity.CRM_EnquiryMaster = enqentity;
                        }
                    }
                    entity.StyleId = item.StyleId;
                    if (item.M_StyleMaster != null)
                    {
                        M_StyleMasterEntity styleentity = new M_StyleMasterEntity();
                        styleentity.Id = item.M_StyleMaster.Id;
                        styleentity.Stylename = item.M_StyleMaster.Stylename;
                        entity.M_StyleMaster = styleentity;
                    }
                    if (item.M_BillOfMaterialMaster != null)
                    {
                        M_BillOfMaterialMasterEntity bomentity = new M_BillOfMaterialMasterEntity();
                        bomentity.BOM_ID = item.M_BillOfMaterialMaster.BOM_ID;
                        bomentity.BOM_No = item.M_BillOfMaterialMaster.BOM_No;
                        entity.M_BillOfMaterialMaster = bomentity;
                    }
                    entity.DepartmentId = item.DepartmentId;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                        deptentity.Id = item.M_DepartmentMaster.Id;
                        deptentity.DepartmentNo = item.M_DepartmentMaster.DepartmentNo;
                        deptentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        deptentity.Departmentdesc = item.M_DepartmentMaster.Departmentdesc;
                        deptentity.Shortname = item.M_DepartmentMaster.Shortname;
                        entity.M_DepartmentMaster = deptentity;
                    }
                    entity.PR_Date = item.PR_Date;
                    entity.PR_No = item.PR_No;
                    entity.Priority = item.Priority;
                    entity.Comment = item.Comment;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    list.Add(entity);
                }
            }
            return list;
        }
    }
}
