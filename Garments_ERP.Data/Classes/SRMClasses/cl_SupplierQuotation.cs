﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_SupplierQuotation
    {
        cl_role rlobj = new cl_role();
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;

        public string getnextQuotatioNo()
        {
            string no;
            no = "SQUO-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var rec = db.SRM_QuotationMaster.Count() > 0 ? db.SRM_QuotationMaster.Max(x => x.Id) + 1 : 1;
                no = no + "-" + rec.ToString();
            }
            return no;
        }

        //Reject Quotation to update Status
        public bool RejectedQuo(long id, int Uid)
        {
            bool isdeleted = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.SRM_QuotationMaster.Find(id);
                    
                    record.IsStatus = 5;
                    record.UpdatedDate = DateTime.Now;
                    record.UpdatedBy = Uid;
                    db.SaveChanges();
                    isdeleted = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isdeleted;
        }


        //List Of Quotation With Status and User Wise
        public List<SRM_QuotationMasterEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<SRM_QuotationMasterEntity> list = new List<SRM_QuotationMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_QuotationMaster.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy == Userid).OrderByDescending(x=>x.Id).ToList();
                if (records.Count >= 0 && roleid != 4)
                {
                    records = db.SRM_QuotationMaster.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).OrderByDescending(x => x.Id).ToList();
                }
                foreach (var item in records)
                {
                    SRM_QuotationMasterEntity entity = new SRM_QuotationMasterEntity();
                    entity.Id = item.Id;
                    entity.QuotationNo = item.QuotationNo;
                    entity.Narration = item.Narration;
                    entity.PR_No = item.PR_No;
                    entity.PR_Id = item.PR_Id;
                    if (item.SRM_PurchaseRequestMaster != null)
                    {
                        SRM_PurchaseRequestMasterEntity purchaseentity = new SRM_PurchaseRequestMasterEntity();
                        purchaseentity.Id = item.SRM_PurchaseRequestMaster.Id;
                        entity.PR_No = item.SRM_PurchaseRequestMaster.PR_No;
                        entity.SRM_PurchaseRequestMaster = purchaseentity;
                    }
                    entity.QuotationDate = item.QuotationDate;
                    entity.QuotationNo = item.QuotationNo;
                    entity.SupplierId = item.SupplierId;

                    if (item.SupplierId != null)
                    {
                        M_LedgersEntity legderentity = new M_LedgersEntity();
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.SupplierId).SingleOrDefault();
                        if (custdata != null)
                        {
                            legderentity.Ledger_Id = custdata.Ledger_Id;
                            legderentity.Ledger_Name = custdata.Ledger_Name;
                        }
                        else
                        {
                            legderentity.Ledger_Id = Convert.ToInt32(item.SupplierId);
                            legderentity.Ledger_Name = "--";
                        }
                        entity.M_Ledgersentity = legderentity;
                    }

                    entity.Validity = item.Validity;
                    entity.GSTTax = item.GSTTax;
                    entity.OtherTax = item.OtherTax;
                    entity.TotalCost = item.TotalCost;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.Approvaldate = item.Approvaldate;
                    entity.Approvalstatus = item.Approvalstatus;
                    entity.Comment = item.Comment;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsReadymade = item.IsReadymade;
                    entity.GrandTotal = item.GrandTotal;
                    entity.POStatus = CheckPO(item.Id);
                    list.Add(entity);
                }

                if ( roleid != 4)
                {
                    var Userlist = rlobj.GetUserRole().Where(x => x.roleId == 4 || x.roleId == roleid).ToList();
                    list = list.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && Userlist.Any(y => y.UserId == x.CreatedBy)).OrderByDescending(x => x.Id).ToList();
                }
            }

            return list;

        }


        ////List Of Quotation With Status and User and Form Date And To Date Wise
        public List<SRM_QuotationMasterEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<SRM_QuotationMasterEntity> list = new List<SRM_QuotationMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_QuotationMaster.Where(x => x.IsActive == true && x.QuotationDate >= from && x.QuotationDate <= to &&  x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy == Userid).OrderByDescending(x=>x.Id).ToList();
                if (records.Count >= 0 && roleid != 4)
                {
                    records = db.SRM_QuotationMaster.Where(x => x.IsActive == true && x.QuotationDate >= from && x.QuotationDate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).OrderByDescending(x => x.Id).ToList();
                }
                foreach (var item in records)
                {
                    SRM_QuotationMasterEntity entity = new SRM_QuotationMasterEntity();
                    entity.Id = item.Id;
                    entity.QuotationNo = item.QuotationNo;
                    entity.Narration = item.Narration;
                    entity.PR_No = item.PR_No;
                    entity.PR_Id = item.PR_Id;
                    if (item.SRM_PurchaseRequestMaster != null)
                    {
                        SRM_PurchaseRequestMasterEntity purchaseentity = new SRM_PurchaseRequestMasterEntity();
                        purchaseentity.Id = item.SRM_PurchaseRequestMaster.Id;
                        entity.PR_No = item.SRM_PurchaseRequestMaster.PR_No;
                        entity.SRM_PurchaseRequestMaster = purchaseentity;
                    }
                    entity.QuotationDate = item.QuotationDate;
                    entity.QuotationNo = item.QuotationNo;
                    entity.SupplierId = item.SupplierId;

                    if (item.SupplierId != null)
                    {
                        M_LedgersEntity legderentity = new M_LedgersEntity();
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.SupplierId).SingleOrDefault();
                        if (custdata != null)
                        {
                            legderentity.Ledger_Id = custdata.Ledger_Id;
                            legderentity.Ledger_Name = custdata.Ledger_Name;
                        }
                        else
                        {
                            legderentity.Ledger_Id = Convert.ToInt32(item.SupplierId);
                            legderentity.Ledger_Name = "--";
                        }
                        entity.M_Ledgersentity = legderentity;
                    }

                    entity.Validity = item.Validity;
                    entity.GSTTax = item.GSTTax;
                    entity.OtherTax = item.OtherTax;
                    entity.TotalCost = item.TotalCost;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.Approvaldate = item.Approvaldate;
                    entity.Approvalstatus = item.Approvalstatus;
                    entity.Comment = item.Comment;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsReadymade = item.IsReadymade;
                    entity.GrandTotal = item.GrandTotal;
                    entity.POStatus = CheckPO(item.Id);
                    list.Add(entity);
                }
                if (roleid != 4)
                {
                    var Userlist = rlobj.GetUserRole().Where(x => x.roleId == 4 || x.roleId == roleid).ToList();
                    records = db.SRM_QuotationMaster.Where(x => x.IsActive == true && x.QuotationDate >= from && x.QuotationDate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && Userlist.Any(y => y.UserId == x.CreatedBy)).OrderByDescending(x => x.Id).ToList();
                }

            }
            return list;
        }



        public SRM_QuotationMasterEntity getbyid(long id)
        {
            SRM_QuotationMasterEntity entity = new SRM_QuotationMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.SRM_QuotationMaster.Find(id);
                entity.Id = item.Id;
                entity.QuotationNo = item.QuotationNo;
                entity.Narration = item.Narration;
                entity.PR_No = item.PR_No;
                entity.PR_Id = item.PR_Id;
                if (item.SRM_PurchaseRequestMaster != null)
                {
                    SRM_PurchaseRequestMasterEntity purchaseentity = new SRM_PurchaseRequestMasterEntity();
                    purchaseentity.Id = item.SRM_PurchaseRequestMaster.Id;
                    entity.PR_No = item.SRM_PurchaseRequestMaster.PR_No;
                    entity.SRM_PurchaseRequestMaster = purchaseentity;
                }
                entity.QuotationDate = item.QuotationDate;
                entity.SupplierId = item.SupplierId;
                if (item.SupplierId != null)
                {
                    M_LedgersEntity supplierentity = new M_LedgersEntity();
                    var suppdta = db.M_Ledgers.Where(x => x.Ledger_Id ==entity.SupplierId).SingleOrDefault();
                    if (suppdta != null)
                    {
                        supplierentity.Ledger_Id = suppdta.Ledger_Id;
                        supplierentity.Ledger_Name = suppdta.Ledger_Name;
                        entity.M_Ledgersentity = supplierentity;
                    }

                    M_Ledger_BillingDetailsEntity supplieraddentity = new M_Ledger_BillingDetailsEntity();
                    var suppadddta = db.M_Ledger_BillingDetails.Where(x => x.Ledger_Id == entity.SupplierId).SingleOrDefault();
                    if (suppadddta != null)
                    {
                        supplieraddentity.Contact_No = suppadddta.Contact_No;
                        supplieraddentity.GSTIN = suppadddta.GSTIN;
                        entity.M_Ledger_Billentity = supplieraddentity;
                    }   
                }
                entity.Validity = item.Validity;
                entity.GSTTax = item.GSTTax;
                entity.OtherTax = item.OtherTax;
                entity.TotalCost = item.TotalCost;
                entity.CreatedDate = item.CreatedDate;
                entity.CreatedBy = item.CreatedBy;
                entity.UpdatedDate = item.UpdatedDate;
                entity.UpdatedBy = item.UpdatedBy;
                entity.IsActive = item.IsActive;
                entity.Comment = item.Comment;
                entity.Approvaldate = item.Approvaldate;
                entity.Approvalstatus = item.Approvalstatus;
                entity.Company_ID = item.Company_ID;
                entity.BranchId = item.BranchId;
                entity.IsStatus = item.IsStatus;
                entity.IsReadymade = item.IsReadymade;
                entity.GrandTotal = item.GrandTotal;
                var otherCharge = db.T_POOtherCharge.Where(x => x.TXID == item.Id && x.InventoryModeId == 10010).ToList();
                if (otherCharge != null)
                {
                    List<T_POOtherChargeEntity> oclist = new List<T_POOtherChargeEntity>();
                    foreach (var ocdata in otherCharge)
                    {
                        T_POOtherChargeEntity ent = new T_POOtherChargeEntity();
                        ent.Id = ocdata.Id;
                        ent.TXID = ocdata.TXID;
                        ent.OtherChargeId = ocdata.OtherChargeId;
                        if (ocdata.OtherChargeId > 0)
                        {
                            M_POOtherChargesMasterEntity ocmaster = new M_POOtherChargesMasterEntity();
                            string OCname = db.M_POOtherChargesMaster.Where(x => x.id == ocdata.OtherChargeId).Select(x => x.POOtherChargeName).FirstOrDefault();
                            ocmaster.POOtherChargeName = OCname;
                            ent.OCMaster = ocmaster;
                        }

                        ent.OtherChargeValue = ocdata.OtherChargeValue;
                        ent.GST = ocdata.GST;
                        ent.InventoryModeId = ocdata.InventoryModeId;
                        ent.Company_ID = ocdata.Company_ID;
                        ent.BranchId = ocdata.BranchId;
                        ent.CreatedOn = DateTime.Now;
                        ent.CreatedBy = ocdata.CreatedBy;
                        oclist.Add(ent);
                    }
                    entity.POOtherChargeEntity = oclist;
                }

            }
            return entity;
        }


        public List<SRM_QuotationMasterEntity> get(DateTime from,DateTime to)
        {
            List<SRM_QuotationMasterEntity> list = new List<SRM_QuotationMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_QuotationMaster.ToList().Where(x => x.IsActive == true && x.QuotationDate>=from && x.QuotationDate<=to).ToList();
                foreach (var item in records)
                {
                    SRM_QuotationMasterEntity entity = new SRM_QuotationMasterEntity();
                    entity.Id = item.Id;
                    entity.QuotationNo = item.QuotationNo;
                    entity.Narration = item.Narration;
                    entity.PR_No = item.PR_No;
                    entity.PR_Id = item.PR_Id;
                    if (item.SRM_PurchaseRequestMaster != null)
                    {
                        SRM_PurchaseRequestMasterEntity purchaseentity = new SRM_PurchaseRequestMasterEntity();
                        purchaseentity.Id = item.SRM_PurchaseRequestMaster.Id;
                        entity.PR_No = item.SRM_PurchaseRequestMaster.PR_No;
                        entity.SRM_PurchaseRequestMaster = purchaseentity;
                    }
                    entity.QuotationDate = item.QuotationDate;
                    entity.QuotationNo = item.QuotationNo;
                    entity.SupplierId = item.SupplierId;
                    if (item.SupplierId != null)
                    {
                        M_LedgersEntity legderentity = new M_LedgersEntity();
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id ==item.SupplierId).SingleOrDefault();
                        if (custdata != null)
                        {
                            legderentity.Ledger_Id = custdata.Ledger_Id;
                            legderentity.Ledger_Name = custdata.Ledger_Name;
                        }
                        else
                        {
                            legderentity.Ledger_Id = Convert.ToInt32(item.SupplierId);
                            legderentity.Ledger_Name = "--";
                        }
                        entity.M_Ledgersentity = legderentity;
                    }
                    entity.Validity = item.Validity;
                    entity.GSTTax = item.GSTTax;
                    entity.OtherTax = item.OtherTax;
                    entity.TotalCost = item.TotalCost;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsReadymade = item.IsReadymade;
                    entity.GrandTotal = item.GrandTotal;
                    entity.POStatus = CheckPO(item.Id);
                    list.Add(entity);
                }
            }
            return list;
        }

        public List<SRM_QuotationMasterEntity> get()
        {
            List<SRM_QuotationMasterEntity> list = new List<SRM_QuotationMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SRM_QuotationMaster.ToList().Where(x => x.IsActive == true).OrderByDescending(x=>x.Id);
                foreach (var item in records)
                {
                    SRM_QuotationMasterEntity entity = new SRM_QuotationMasterEntity();
                    entity.Id = item.Id;
                    entity.QuotationNo = item.QuotationNo;
                    entity.Narration = item.Narration;
                    entity.PR_No = item.PR_No;
                    entity.PR_Id = item.PR_Id;
                    if (item.SRM_PurchaseRequestMaster != null)
                    {
                        SRM_PurchaseRequestMasterEntity purchaseentity = new SRM_PurchaseRequestMasterEntity();
                        purchaseentity.Id = item.SRM_PurchaseRequestMaster.Id;
                        entity.PR_No = item.SRM_PurchaseRequestMaster.PR_No;
                        entity.SRM_PurchaseRequestMaster = purchaseentity;
                    }
                    entity.QuotationDate = item.QuotationDate;
                    entity.QuotationNo = item.QuotationNo;
                    entity.SupplierId = item.SupplierId;

                    if (item.SupplierId != null)
                    {
                        M_LedgersEntity legderentity = new M_LedgersEntity();
                        var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == item.SupplierId).SingleOrDefault();
                        if (custdata != null)
                        {
                            legderentity.Ledger_Id = custdata.Ledger_Id;
                            legderentity.Ledger_Name = custdata.Ledger_Name;
                        }
                        else
                        {
                            legderentity.Ledger_Id = Convert.ToInt32(item.SupplierId);
                            legderentity.Ledger_Name = "--";
                        }
                        entity.M_Ledgersentity = legderentity;
                    }

                    entity.Validity = item.Validity;
                    entity.GSTTax = item.GSTTax;
                    entity.OtherTax = item.OtherTax;
                    entity.TotalCost = item.TotalCost;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.Approvaldate = item.Approvaldate;
                    entity.Approvalstatus = item.Approvalstatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.Comment = item.Comment;
                    entity.GrandTotal = item.GrandTotal;
                    entity.IsReadymade = item.IsReadymade;
                    entity.POStatus = CheckPO(item.Id);
                    list.Add(entity);
                }
            }
            return list;
        }


        public bool CheckPO(long Quoid)
        {
            bool status = false;
            using (var db = new GarmentERPDBEntities())
            {
                var Quotationid = db.SRM_PurchaseOrderMaster.Where(x => x.IsActive == true).Select(u => u.QuotationId).ToArray();
                foreach(var data in Quotationid)
                {
                    if(Convert.ToInt64(data)==Quoid)
                    {
                        status = true;
                    }
                }
            }
            return status;
        }

        public long Insert(SRM_QuotationMasterEntity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                SRM_QuotationMaster item = new SRM_QuotationMaster();
                item.Id = entity.Id;
                item.CreatedBy = entity.CreatedBy;
                item.CreatedDate = DateTime.Now;
                item.Narration = entity.Narration;
                item.PR_No = entity.PR_No;
                item.PR_Id = entity.PR_Id;
                item.QuotationDate = entity.QuotationDate;
                item.SupplierId = entity.SupplierId;
                item.Validity = entity.Validity;
                item.GSTTax = entity.GSTTax;
                item.OtherTax = entity.OtherTax;
                item.TotalCost = entity.TotalCost;
                item.GrandTotal = entity.GrandTotal;
                item.IsActive = entity.IsActive;
                item.Approvaldate = entity.Approvaldate;
                item.Approvalstatus = entity.Approvalstatus;
                item.IsStatus = entity.IsStatus;
                item.Comment = entity.Comment;
                item.Company_ID = entity.Company_ID;
                item.BranchId = entity.BranchId;
                item.IsReadymade = entity.IsReadymade;
                if (entity.IsReadymade == 0)
                {
                    item.QuotationNo = "RM-" + Convert.ToString(getnextQuotatioNo());
                }
                else
                {
                    item.QuotationNo = "RF-" + Convert.ToString(getnextQuotatioNo());
                }
                
                db.SRM_QuotationMaster.Add(item);
                db.SaveChanges();
                id = item.Id;
                if (id > 0)
                {
                    if (entity.POOtherChargeEntity != null)
                    {
                        foreach (var data in entity.POOtherChargeEntity)
                        {
                            T_POOtherCharge ent = new T_POOtherCharge();
                            ent.TXID = id;
                            ent.OtherChargeId = data.OtherChargeId;
                            ent.OtherChargeValue = data.OtherChargeValue;
                            ent.GST = data.GST;
                            ent.InventoryModeId = data.InventoryModeId;
                            ent.Company_ID = data.Company_ID;
                            ent.BranchId = data.BranchId;
                            ent.CreatedOn = DateTime.Now;
                            ent.CreatedBy = data.CreatedBy;
                            db.T_POOtherCharge.Add(ent);
                            db.SaveChanges();
                        }
                    }
                }
                if (id > 0)
                {
                    var data = db.SRM_PurchaseRequestMaster.Find(entity.PR_Id);
                    data.IsStatus = 4;
                    data.UpdatedDate = DateTime.Now;
                    data.UpdatedBy = entity.CreatedBy;
                    db.SaveChanges();
                }
            }
            return id;
        }
        public bool Update(SRM_QuotationMasterEntity entity)
        {
            bool isupdated = false;

            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SRM_QuotationMaster.Find(entity.Id);
                record.Id = entity.Id;
                record.Narration = entity.Narration;
                //record.PR_No = entity.PR_No;
                //record.PR_Id = entity.PR_Id;
                //record.QuotationDate = entity.QuotationDate;
                //record.QuotationNo = entity.QuotationNo;
                //record.SupplierId = entity.SupplierId;
                record.UpdatedBy = entity.UpdatedBy;
                record.UpdatedDate =DateTime.Now;
                record.Validity = entity.Validity;
                record.GSTTax = entity.GSTTax;
                record.OtherTax = entity.OtherTax;
                record.TotalCost = entity.TotalCost;
                record.GrandTotal = entity.GrandTotal;
                record.IsActive = entity.IsActive;
                record.Comment = entity.Comment;
                record.IsStatus = entity.IsStatus;
                db.SaveChanges();
                isupdated = true;
                if (entity.POOtherChargeEntity != null)
                {
                    var singleRec = db.T_POOtherCharge.Where(x => x.TXID == entity.Id && x.InventoryModeId == 10010).ToList();
                    foreach (var rec in singleRec)
                    {
                        var OCrecord = db.T_POOtherCharge.FirstOrDefault(x => x.Id == rec.Id);
                        db.T_POOtherCharge.Remove(OCrecord);
                        db.SaveChanges();
                    }

                    foreach (var data in entity.POOtherChargeEntity)
                    {
                        T_POOtherCharge ent = new T_POOtherCharge();
                        ent.TXID = entity.Id;
                        ent.OtherChargeId = data.OtherChargeId;
                        ent.OtherChargeValue = data.OtherChargeValue;
                        ent.GST = data.GST;
                        ent.InventoryModeId = data.InventoryModeId;
                        ent.Company_ID = data.Company_ID;
                        ent.BranchId = data.BranchId;
                        ent.CreatedOn = DateTime.Now;
                        ent.CreatedBy = data.CreatedBy;
                        db.T_POOtherCharge.Add(ent);
                        db.SaveChanges();
                    }
                }
            }
            return isupdated;
        }

        public List<SRM_QuotationMasterEntity> getquotationnobysupplierid(long supplierid)
        {
            List<SRM_QuotationMasterEntity> quotnolist = new List<SRM_QuotationMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var list = db.SRM_QuotationMaster.Where(x => x.SupplierId == supplierid).ToList();
                foreach (var item in list)
                {
                    SRM_QuotationMasterEntity entity = new SRM_QuotationMasterEntity();
                    entity.QuotationNo = item.QuotationNo;
                    entity.Id = item.Id;
                    quotnolist.Add(entity);
                }
            }
            return quotnolist;
        }
        public bool Delete(long id,int Userid)
        {
            bool IsDeleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var contact = db.SRM_QuotationMaster.Where(x => x.Id == id).FirstOrDefault();
                
                try
                {
                    contact.IsActive = false;
                    contact.UpdatedBy = Userid;
                    contact.UpdatedDate = DateTime.Now;
                    db.SaveChanges();
                    IsDeleted = true;
                    
                    var PRid = db.SRM_QuotationMaster.Where(x => x.Id == id).Select(x => x.PR_Id).FirstOrDefault();
                    var PRrecord = db.SRM_PurchaseRequestMaster.Find(PRid);
                    PRrecord.IsStatus = 3;
                    PRrecord.UpdatedBy = Userid;
                    PRrecord.UpdatedDate = DateTime.Now;
                    db.SaveChanges();
                }
                catch (Exception)
                {

                    throw;
                }
                return IsDeleted;
            }
        }

        public object QuoteGet(long id)
        {
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_SupplierQuotation_report";
                cmd.Parameters.Add("Id", SqlDbType.BigInt).Value = id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            return dt;
        }

    }
}
