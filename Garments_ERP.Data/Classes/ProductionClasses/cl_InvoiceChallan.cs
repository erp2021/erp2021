﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_InvoiceChallan
    {
        public List<T_Invoice_ChallanEntity> Get()
        {
            try
            {
                List<T_Invoice_ChallanEntity> list = new List<T_Invoice_ChallanEntity>();
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.T_Invoice_Challan.ToList();
                    if(record!=null && record.Count>0)
                    {
                        foreach (var data in record)
                        {
                            T_Invoice_ChallanEntity entity = new T_Invoice_ChallanEntity();
                            entity.InvoiceId = data.InvoiceId;
                            entity.InvoiceNo = data.InvoiceNo;
                            entity.InvoiceDate = data.InvoiceDate;
                            entity.ChallanNo = data.ChallanNo;
                            entity.ChallanDate = data.ChallanDate;
                            entity.PO_Id = data.PO_Id;
                            if(data.PO_Id>0)
                            {
                                long poid = Convert.ToInt64(data.PO_Id);
                                var podata = db.CRM_CustomerPOMaster.Where(x => x.Id == poid).FirstOrDefault();
                                CRM_CustomerPOMasterentity poent = new CRM_CustomerPOMasterentity();
                                poent.POno = podata.POno;
                                entity.poent = poent;
                            }
                            entity.PO_Date = data.PO_Date;
                            entity.LedgerId = data.LedgerId;
                            if(data.LedgerId>0)
                            {
                                long Ledid = Convert.ToInt64(data.LedgerId);
                                M_LedgersEntity Mleg = new M_LedgersEntity();
                                var Ledger = db.M_Ledgers.Where(x => x.Ledger_Id == Ledid).FirstOrDefault();
                                Mleg.Ledger_Name = Ledger.Ledger_Name;
                                entity.LedgerEnt = Mleg;
                            }

                            entity.DeliveryAdd = data.DeliveryAdd;
                            entity.Total = data.Total;
                            entity.GST = data.GST;
                            entity.GSTAmount = data.GSTAmount;
                            entity.GrossTotal = data.GrossTotal;
                            entity.CreatedBy = data.CreatedBy;
                            entity.CreatedOn = data.CreatedOn;
                            entity.UpdateBy = data.UpdateBy;
                            entity.UpdateOn = data.UpdateOn;
                            entity.PaymentTerm = data.PaymentTerm;
                            if(data.PaymentTerm>0)
                            {
                                long payt = Convert.ToInt64(data.PaymentTerm);
                                var Paydata = db.M_PaymentTermMaster.Where(x => x.Id == payt).FirstOrDefault();
                                M_PaymentTermMasterEntity payent = new M_PaymentTermMasterEntity();
                                payent.PaymentTerm = Paydata.PaymentTerm;
                                entity.PaytermEnt = payent;
                            }
                            entity.DuoOn = data.DuoOn;
                            entity.Company_ID = data.Company_ID;
                            entity.BranchId = data.BranchId;
                            entity.IsStatus = data.IsStatus;
                            long invid = Convert.ToInt64(data.InvoiceId);
                            var ItemData = db.T_InvoiceChallan_ProductDetails.Where(x => x.InvoiceId == invid).FirstOrDefault();
                            int IAid = Convert.ToInt32(ItemData.Item_Attribute_ID);
                            int ISid = 0;
                            long itemid = Convert.ToInt64(ItemData.ItemId);
                            cl_GRNInward grnobj = new cl_GRNInward();
                            string ItemName = grnobj.getItemName(itemid, IAid, ISid);
                            entity.productname = ItemName;
                            entity.PoQty = ItemData.POQty;
                            entity.InvQty = ItemData.Qty;
                            list.Add(entity);
                        }
                    }
                }

                return list;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string getnextInvoiceNo()
        {
            string no;
            no = "INV-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var rec = db.T_Invoice_Challan.Count() > 0 ? db.T_Invoice_Challan.Max(x => x.InvoiceId) + 1 : 1;
                no = no + "-" + rec.ToString();
            }
            return no;
        }

        public long Insert(T_Invoice_ChallanEntity entity)
        {
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    long id = 0;
                    T_Invoice_Challan record = new T_Invoice_Challan();

                    record.InvoiceDate = entity.InvoiceDate;
                    record.ChallanDate = entity.ChallanDate;
                    record.PO_Id = entity.PO_Id;
                    record.PO_Date = entity.PO_Date;
                    record.LedgerId = entity.LedgerId;
                    record.DeliveryAdd = entity.DeliveryAdd;
                    record.Total = entity.Total;
                    record.GST = entity.GST;
                    record.GSTAmount = entity.GSTAmount;
                    record.GrossTotal = entity.GrossTotal;
                    record.CreatedBy = entity.CreatedBy;
                    record.CreatedOn = DateTime.Now;
                    record.PaymentTerm = entity.PaymentTerm;
                    record.DuoOn = entity.InvoiceDate;
                    record.Company_ID = entity.Company_ID;
                    record.BranchId = entity.BranchId;
                    record.InvoiceNo = getnextInvoiceNo();
                    string[] ChallanNo =getnextInvoiceNo().Split('-');
                    record.ChallanNo = "DCH-" + ChallanNo[1] + "-" + ChallanNo[2] + "-" + ChallanNo[3] + "-" + ChallanNo[4];
                    db.T_Invoice_Challan.Add(record);
                    db.SaveChanges();
                    id = record.InvoiceId;
                    if (id > 0)
                    {
                        if (entity.POOtherChargeEntity != null && entity.POOtherChargeEntity.Count>0)
                        {
                            foreach (var data in entity.POOtherChargeEntity)
                            {
                                T_POOtherCharge ent = new T_POOtherCharge();
                                ent.TXID = id;
                                ent.OtherChargeId = data.OtherChargeId;
                                ent.OtherChargeValue = data.OtherChargeValue;
                                ent.GST = data.GST;
                                ent.InventoryModeId = data.InventoryModeId;
                                ent.Company_ID = data.Company_ID;
                                ent.BranchId = data.BranchId;
                                ent.CreatedOn = DateTime.Now;
                                ent.CreatedBy = data.CreatedBy;
                                db.T_POOtherCharge.Add(ent);
                                db.SaveChanges();
                            }
                        }

                        if(entity.TInv_ProdEntity!=null && entity.TInv_ProdEntity.Count>0)
                        {
                            foreach (var data in entity.TInv_ProdEntity)
                            {
                                T_InvoiceChallan_ProductDetails rec = new T_InvoiceChallan_ProductDetails();
                                rec.InvoiceId = id;
                                rec.ItemId = data.ItemId;
                                rec.Item_Attribute_ID = data.Item_Attribute_ID;
                                rec.BaleNo = data.BaleNo;
                                rec.UOM = data.UOM;
                                rec.HSNNO = data.HSNNO;
                                rec.Qty = data.Qty;
                                rec.Rate = data.Rate;
                                rec.Total = data.Total;
                                rec.GST = data.GST;
                                rec.CreatedBy = data.CreatedBy;
                                rec.CreatedOn = DateTime.Now;
                                rec.Company_ID = data.Company_ID;            
                                rec.BranchId = data.BranchId;
                                rec.POQty = data.POQty;
                                rec.BalQty = data.BalQty;
                                rec.Batchid = data.Batchid;
                                db.T_InvoiceChallan_ProductDetails.Add(rec);
                                db.SaveChanges();
                                string[] pivalstr = data.itemPIval.Split('#');
                                foreach(var pidat in pivalstr)
                                {
                                    if(pidat!="")
                                    {
                                        string[] PI = pidat.Split('~');
                                        M_ItemOutwardMaster outitemrecord = new M_ItemOutwardMaster();
                                        outitemrecord.Inward_ID = Convert.ToInt64(PI[0]);
                                        outitemrecord.ItemSubCategoryID = 0;
                                        outitemrecord.Item_ID = data.ItemId;
                                        outitemrecord.Item_Attribute_ID = data.Item_Attribute_ID;
                                        outitemrecord.UOM =Convert.ToInt32(data.UOM);
                                        outitemrecord.ItemQty = Convert.ToDecimal(PI[3]);
                                        outitemrecord.IsActive = true;
                                        outitemrecord.OutwardBy = Convert.ToInt32(data.CreatedBy);
                                        outitemrecord.OutwardDate = DateTime.Now;
                                        outitemrecord.Company_ID = entity.Company_ID;
                                        outitemrecord.BranchId = entity.BranchId;
                                        outitemrecord.IsStatus = entity.IsStatus;
                                        db.M_ItemOutwardMaster.Add(outitemrecord);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                    }


                    return id;

                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<M_Product_InfoEntity> GetBatachLot(long ItemId, int ItemAttrId, long poid)
        {
            try
            {
                List<M_Product_InfoEntity> list = new List<M_Product_InfoEntity>();
                using (var db = new GarmentERPDBEntities())
                {
                   
                    var wodata = db.MFG_WORK_ORDER.Where(x => x.POID == poid && x.IsActive == true && x.Approved == 1 && x.IsStatus == 4).ToList();
                    if (wodata.Count > 0)
                    {
                        foreach (var wo in wodata)
                        {

                            long woid = Convert.ToInt64(wo.WorkOrderID);
                            var fg_transfer = db.FG_TransferMaster.Where(x => x.RefWorkOrderId == woid && x.IsActive == true && (x.IsStatus == 3 || x.IsStatus == 4)).ToList();
                            if(fg_transfer.Count>0)
                            {
                                foreach( var fg in fg_transfer)
                                {
                                    long fgid = Convert.ToInt64(fg.Id);
                                    var FGTransferDetails = db.FG_TransferDetail.Where(x => x.FG_TransferId == fgid && x.IsActive == true && x.IsQC_Done.Trim() == "Yes").ToList();
                                    if(FGTransferDetails.Count>0)
                                    {
                                        foreach(var FGD in FGTransferDetails)
                                        {
                                            M_Product_InfoEntity entity = new M_Product_InfoEntity();
                                            long inwrdid = Convert.ToInt64(FGD.batchlotid);
                                            var productdata = db.M_Product_Info.Where(x => x.ItemInward_ID == inwrdid && x.Dynamic_Controls_Id == 4).FirstOrDefault();
                                            productdata = productdata != null ? productdata : db.M_Product_Info.Where(x => x.ItemInward_ID == inwrdid && x.Dynamic_Controls_Id == 3).FirstOrDefault();
                                            if(productdata!=null)
                                            {
                                                entity.ItemInward_ID = productdata.ItemInward_ID;
                                                entity.Dynamic_Controls_value= productdata.Dynamic_Controls_value;
                                                entity.Dynamic_Controls_Id = productdata.Dynamic_Controls_Id;
                                                list.Add(entity);
                                            }

                                        }
                                    }
                                }
                            }

                        }
                    }

                }

                return list;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string getnextChallanNo()
        {
            string no;
            no = "DCH-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var rec = db.T_Invoice_Challan.Count() > 0 ? db.T_Invoice_Challan.Max(x => x.InvoiceId) + 1 : 1;
                no = no + "-" + rec.ToString();
            }
            return no;
        }

        public List<CRM_CustomerPOMasterentity> getPOListbycustomerid(long custid)
        {
            List<long> POId = new List<long>();
            List<bool> POIdTF = new List<bool>();

            List<CRM_CustomerPOMasterentity> list = new List<CRM_CustomerPOMasterentity>();
            using (var db = new GarmentERPDBEntities())
            {
                var allpolist = db.CRM_CustomerPOMaster.Where(x => x.Approvalstatus == true && x.IsActive == true && x.IsStatus==4 && x.customerid== custid);
                foreach (var item in allpolist)
                {
                    CRM_CustomerPOMasterentity entity = new CRM_CustomerPOMasterentity();
                    entity.POno = item.POno;
                    entity.Id = item.Id;
                    list.Add(entity);
                }

            }
            return list;
        }
    }
}
