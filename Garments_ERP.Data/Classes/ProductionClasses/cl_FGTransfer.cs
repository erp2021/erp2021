﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_FGTransfer
    {
        cl_GRNInward grnobj = new cl_GRNInward();
        public string getnextTFNo()
        {
            string no;
            no = "TF-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var rec = db.FG_TransferMaster.Count() > 0 ? db.FG_TransferMaster.Max(x => x.Id) + 1 : 1;
                no = no + "-" + rec.ToString();
            }

            return no;
        }


        //List Of GRN With Status and User Wise
        public List<FG_TransferMasterEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<FG_TransferMasterEntity> list = new List<FG_TransferMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.FG_TransferMaster.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).ToList();
                //if (records.Count == 0 && roleid != 4)
                //{
                //    string str = "select Id,TransferNo,TransferDate,ProductionPlanId,ProductionPlanDate,RefWorkOrderId,WarehouseId,RackId,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,IsActive,Comment, ";
                //    str = str + " DepartmentId,RefWorkOrderDate,Company_ID,BranchId,IsStatus ";
                //    str = str + " from FG_TransferMaster where IsActive=1  and IsStatus=" + IsStatus + " and Company_ID=" + Company_ID + " and BranchId=" + BranchId + " and CreatedBy in(select UserId from M_User_Role where roleId=4)";
                //    records = db.FG_TransferMaster.SqlQuery(str).ToList();
                //}

                foreach (var item in records)
                {

                    FG_TransferMasterEntity entity = new FG_TransferMasterEntity();
                    entity.Id = item.Id;
                    entity.TransferNo = item.TransferNo;
                    entity.TransferDate = item.TransferDate;
                    entity.Comment = item.Comment;
                    entity.DepartmentId = item.DepartmentId;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity departentity = new M_DepartmentMasterEntity();
                        departentity.Id = item.M_DepartmentMaster.Id;
                        departentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        entity.M_DepartmentMaster = departentity;
                    }
                    entity.ProductionPlanDate = item.ProductionPlanDate;
                    entity.ProductionPlanId = item.ProductionPlanId;
                    entity.RefWorkOrderId = item.RefWorkOrderId;
                    entity.RefWorkOrderDate = item.RefWorkOrderDate;
                    if (item.MFG_WORK_ORDER != null)
                    {
                        MFG_WORK_ORDEREntity workorderentity = new MFG_WORK_ORDEREntity();
                        workorderentity.WorkOrderID = item.MFG_WORK_ORDER.WorkOrderID;
                        workorderentity.WorkOrderNo = item.MFG_WORK_ORDER.WorkOrderNo;
                        workorderentity.Date = item.MFG_WORK_ORDER.Date;
                        entity.MFG_WORK_ORDER = workorderentity;
                    }
                    entity.WarehouseId = item.WarehouseId;
                    if (item.WarehouseId > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseId).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WAREHOUSE = locentity;
                        }
                    }

                    entity.IsActive = item.IsActive;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsStatus = item.IsStatus;
                    entity.ItemId = item.ItemId;
                    entity.Item_Attribute_Id = item.Item_Attribute_Id;
                    int IAid = Convert.ToInt32(item.Item_Attribute_Id);
                    int ISid = 0;
                    long itemid = Convert.ToInt64(item.ItemId);
                    cl_GRNInward grnobj = new cl_GRNInward();
                    string ItemName = grnobj.getItemName(itemid, IAid, ISid);
                    entity.ItemName = ItemName;
                    entity.HSNCode = item.HSNCode;
                    entity.Unit = item.Unit;
                    int uniid =Convert.ToInt32(item.Unit);
                    string ItemUnit = db.M_UnitMaster.Where(x => x.Id == uniid).Select(x => x.ItemUnit).FirstOrDefault();
                    entity.UnitName = ItemUnit;
                    entity.WOQty = item.WOQty;
                    entity.TotBatchQty = item.TotBatchQty;
                    entity.ProducedQty = item.ProducedQty;
                    entity.BalanceQty = item.BalanceQty;
                    var Rackdata = db.M_RackAllocation.Where(x => x.InwardId == item.Id && x.InventoryMode == 7).ToList();
                    if (Rackdata != null && Rackdata.Count>0)
                    { 
                        var str = "";
                        foreach (var data in Rackdata)
                        {
                            string[] batchlotstrId = data.BatchLot.Split(',');
                            string Batchvalstr = "";
                            string BatchList = "";
                            foreach (var IinData in batchlotstrId)
                            {
                                long inid = Convert.ToInt64(IinData);
                                var ProInfo = db.M_Product_Info.Where(x => x.ItemInward_ID == inid && x.Dynamic_Controls_Id == 4).ToList();
                                ProInfo = ProInfo != null && ProInfo.Count>0 ? ProInfo : db.M_Product_Info.Where(x => x.ItemInward_ID == inid && x.Dynamic_Controls_Id == 3).ToList();
                                foreach (var PIdata in ProInfo)
                                {
                                    Batchvalstr = Batchvalstr + PIdata.Dynamic_Controls_value + ",";
                                }
                            }
                            if (Batchvalstr != "")
                            {
                                BatchList = Batchvalstr.Remove(Batchvalstr.Length - 1, 1);
                            }
                            else
                            {
                                BatchList = "";
                            }

                            string[] RackstrId = data.RackId.Split(',');
                            string racknameStr = "";
                            foreach (var rackData in RackstrId)
                            {
                                long racid = Convert.ToInt64(rackData);
                                var rackname = db.M_RackMaster.Where(x => x.Id == racid).Select(x => x.Rack).FirstOrDefault();
                                racknameStr = racknameStr + "," + rackname;
                            }
                            if (racknameStr != "")
                            {
                                racknameStr = racknameStr.Remove(racknameStr.Length - 1, 1);
                            }
                            else
                            {
                                racknameStr = "";
                            }

                            int IAid1 = Convert.ToInt32(data.Item_Attribute_Id);
                            int ISid1 = 0;
                            long itemid1 = Convert.ToInt64(data.ItemId);
                            string ItemName1 = grnobj.getItemName(itemid1, IAid1, ISid1);
                            str = str + Convert.ToString(data.ItemId) + "~" + ItemName1 + "~" + Convert.ToString(data.Item_Attribute_Id) + "~" + data.BatchLot + "~" + BatchList + "~" + Convert.ToString(data.BatchLotQty) + "~" + data.RackId + "~" + racknameStr + "~" + data.RackQty + "#";
                        }

                        if (str != "")
                        {
                            entity.RackAllocation = str;
                        }
                    }
                    else
                    {
                        entity.RackAllocation = "";
                    }
                    list.Add(entity);
                }
            }

            return list;

        }


        ////List Of GRN With Status and User and Form Date And To Date Wise
        public List<FG_TransferMasterEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<FG_TransferMasterEntity> list = new List<FG_TransferMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.FG_TransferMaster.Where(x => x.IsActive == true && x.TransferDate >= from && x.TransferDate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).ToList();
                ////if (records.Count == 0 && roleid != 4)
                ////{
                ////    string str = "select Id,TransferNo,TransferDate,ProductionPlanId,ProductionPlanDate,RefWorkOrderId,WarehouseId,RackId,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,IsActive,Comment, ";
                ////    str = str + " DepartmentId,RefWorkOrderDate,Company_ID,BranchId,IsStatus ";
                ////    str = str + " from FG_TransferMaster where IsActive=1 and TransferDate >= convert(datetime,'" + from + "',103) and TransferDate <=convert(datetime,'" + to + "',103)  and IsStatus=" + IsStatus + " and Company_ID=" + Company_ID + " and BranchId=" + BranchId + " and CreatedBy in(select UserId from M_User_Role where roleId=4)";
                ////    records = db.FG_TransferMaster.SqlQuery(str).ToList();
                ////}

                foreach (var item in records)
                {

                    FG_TransferMasterEntity entity = new FG_TransferMasterEntity();
                    entity.Id = item.Id;
                    entity.TransferNo = item.TransferNo;
                    entity.TransferDate = item.TransferDate;
                    entity.Comment = item.Comment;
                    entity.DepartmentId = item.DepartmentId;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity departentity = new M_DepartmentMasterEntity();
                        departentity.Id = item.M_DepartmentMaster.Id;
                        departentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        entity.M_DepartmentMaster = departentity;
                    }
                    entity.ProductionPlanDate = item.ProductionPlanDate;
                    entity.ProductionPlanId = item.ProductionPlanId;
                    entity.RefWorkOrderId = item.RefWorkOrderId;
                    entity.RefWorkOrderDate = item.RefWorkOrderDate;
                    if (item.MFG_WORK_ORDER != null)
                    {
                        MFG_WORK_ORDEREntity workorderentity = new MFG_WORK_ORDEREntity();
                        workorderentity.WorkOrderID = item.MFG_WORK_ORDER.WorkOrderID;
                        workorderentity.WorkOrderNo = item.MFG_WORK_ORDER.WorkOrderNo;
                        workorderentity.Date = item.MFG_WORK_ORDER.Date;
                        entity.MFG_WORK_ORDER = workorderentity;
                    }
                    entity.WarehouseId = item.WarehouseId;
                    if (item.WarehouseId > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseId).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WAREHOUSE = locentity;
                        }
                    }

                    entity.IsActive = item.IsActive;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsStatus = item.IsStatus;
                    entity.ItemId = item.ItemId;
                    entity.Item_Attribute_Id = item.Item_Attribute_Id;
                    int IAid = Convert.ToInt32(item.Item_Attribute_Id);
                    int ISid = 0;
                    long itemid = Convert.ToInt64(item.ItemId);
                    cl_GRNInward grnobj = new cl_GRNInward();
                    string ItemName = grnobj.getItemName(itemid, IAid, ISid);
                    entity.ItemName = ItemName;
                    entity.HSNCode = item.HSNCode;
                    entity.Unit = item.Unit;
                    int uniid = Convert.ToInt32(item.Unit);
                    string ItemUnit = db.M_UnitMaster.Where(x => x.Id == uniid).Select(x => x.ItemUnit).FirstOrDefault();
                    entity.UnitName = ItemUnit;
                    entity.WOQty = item.WOQty;
                    entity.TotBatchQty = item.TotBatchQty;
                    entity.ProducedQty = item.ProducedQty;
                    entity.BalanceQty = item.BalanceQty;

                    var Rackdata = db.M_RackAllocation.Where(x => x.InwardId == item.Id && x.InventoryMode == 7).ToList();
                    if (Rackdata != null && Rackdata.Count>0)
                    {
                        var str = "";
                        foreach (var data in Rackdata)
                        {
                            string[] batchlotstrId = data.BatchLot.Split(',');
                            string Batchvalstr = "";
                            string BatchList = "";
                            foreach (var IinData in batchlotstrId)
                            {
                                long inid = Convert.ToInt64(IinData);
                                var ProInfo = db.M_Product_Info.Where(x => x.ItemInward_ID == inid && x.Dynamic_Controls_Id == 4).ToList();
                                ProInfo = ProInfo != null && ProInfo.Count>0 ? ProInfo : db.M_Product_Info.Where(x => x.ItemInward_ID == inid && x.Dynamic_Controls_Id == 3).ToList();
                                foreach (var PIdata in ProInfo)
                                {
                                    Batchvalstr = Batchvalstr + PIdata.Dynamic_Controls_value + ",";
                                }
                            }
                            if (Batchvalstr != "")
                            {
                                BatchList = Batchvalstr.Remove(Batchvalstr.Length - 1, 1);
                            }
                            else
                            {
                                BatchList = "";
                            }

                            string[] RackstrId = data.RackId.Split(',');
                            string racknameStr = "";
                            foreach (var rackData in RackstrId)
                            {
                                long racid = Convert.ToInt64(rackData);
                                var rackname = db.M_RackMaster.Where(x => x.Id == racid).Select(x => x.Rack).FirstOrDefault();
                                racknameStr = racknameStr + "," + rackname;
                            }
                            if (racknameStr != "")
                            {
                                racknameStr = racknameStr.Remove(racknameStr.Length - 1, 1);
                            }
                            else
                            {
                                racknameStr = "";
                            }

                            int IAid1 = Convert.ToInt32(data.Item_Attribute_Id);
                            int ISid1 = 0;
                            long itemid1 = Convert.ToInt64(data.ItemId);
                            string ItemName1 =grnobj.getItemName(itemid1, IAid1, ISid1);
                            str = str + Convert.ToString(data.ItemId) + "~" + ItemName1 + "~" + Convert.ToString(data.Item_Attribute_Id) + "~" + data.BatchLot + "~" + BatchList + "~" + Convert.ToString(data.BatchLotQty) + "~" + data.RackId + "~" + racknameStr + "~" + data.RackQty + "#";
                        }

                        if (str != "")
                        {
                            entity.RackAllocation = str;
                        }
                    }
                    else
                    {
                        entity.RackAllocation = "";
                    }
                    list.Add(entity);
                }
            }
            return list;
        }




        public List<FG_TransferMasterEntity> Get(DateTime from,DateTime to)
        {
            List<FG_TransferMasterEntity> list = new List<FG_TransferMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.FG_TransferMaster.ToList().Where(x => x.IsActive == true && x.TransferDate>=from && x.TransferDate<=to).ToList();
                foreach (var item in records)
                {

                    FG_TransferMasterEntity entity = new FG_TransferMasterEntity();
                    entity.Id = item.Id;
                    entity.TransferNo = item.TransferNo;
                    entity.TransferDate = item.TransferDate;
                    entity.Comment = item.Comment;
                    entity.DepartmentId = item.DepartmentId;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity departentity = new M_DepartmentMasterEntity();
                        departentity.Id = item.M_DepartmentMaster.Id;
                        departentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        entity.M_DepartmentMaster = departentity;
                    }
                    entity.ProductionPlanDate = item.ProductionPlanDate;
                    entity.ProductionPlanId = item.ProductionPlanId;
                    entity.RefWorkOrderId = item.RefWorkOrderId;
                    entity.RefWorkOrderDate = item.RefWorkOrderDate;
                    if (item.MFG_WORK_ORDER != null)
                    {
                        MFG_WORK_ORDEREntity workorderentity = new MFG_WORK_ORDEREntity();
                        workorderentity.WorkOrderID = item.MFG_WORK_ORDER.WorkOrderID;
                        workorderentity.WorkOrderNo = item.MFG_WORK_ORDER.WorkOrderNo;
                        workorderentity.Date = item.MFG_WORK_ORDER.Date;
                        entity.MFG_WORK_ORDER = workorderentity;
                    }
                    entity.WarehouseId = item.WarehouseId;
                    if (item.WarehouseId > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseId).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WAREHOUSE = locentity;
                        }
                    }

                    entity.IsActive = item.IsActive;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.ItemId = item.ItemId;
                    entity.Item_Attribute_Id = item.Item_Attribute_Id;
                    int IAid = item.Item_Attribute_Id != null ? Convert.ToInt32(item.Item_Attribute_Id) : 0;
                    int ISid = 0;
                    long itemid = item.ItemId != null ? Convert.ToInt64(item.ItemId) : 0;
                    cl_GRNInward grnobj = new cl_GRNInward();
                    string ItemName = itemid > 0 ? grnobj.getItemName(itemid, IAid, ISid) : "";
                    entity.ItemName = ItemName;
                    entity.HSNCode = item.HSNCode;
                    entity.Unit = item.Unit;
                    int uniid = item.Unit!=null ? Convert.ToInt32(item.Unit) : 0;
                    string ItemUnit = uniid > 0 ? db.M_UnitMaster.Where(x => x.Id == uniid).Select(x => x.ItemUnit).FirstOrDefault() : "";
                    entity.UnitName = ItemUnit;
                    entity.WOQty = item.WOQty;
                    entity.TotBatchQty = item.TotBatchQty;
                    entity.ProducedQty = item.ProducedQty;
                    entity.BalanceQty = item.BalanceQty;

                    var Rackdata = db.M_RackAllocation.Where(x => x.InwardId == item.Id && x.InventoryMode == 7).ToList();
                    if (Rackdata != null && Rackdata.Count>0)
                    {
                        var str = "";
                        foreach (var data in Rackdata)
                        {
                            string[] batchlotstrId = data.BatchLot.Split(',');
                            string Batchvalstr = "";
                            string BatchList = "";
                            foreach (var IinData in batchlotstrId)
                            {
                                long inid = Convert.ToInt64(IinData);
                                var ProInfo = db.M_Product_Info.Where(x => x.ItemInward_ID == inid && x.Dynamic_Controls_Id == 4).ToList();
                                ProInfo = ProInfo != null && ProInfo.Count>0 ? ProInfo : db.M_Product_Info.Where(x => x.ItemInward_ID == inid && x.Dynamic_Controls_Id == 3).ToList();
                                foreach (var PIdata in ProInfo)
                                {
                                    Batchvalstr = Batchvalstr + PIdata.Dynamic_Controls_value + ",";
                                }
                            }
                            if (Batchvalstr != "")
                            {
                                BatchList = Batchvalstr.Remove(Batchvalstr.Length - 1, 1);
                            }
                            else
                            {
                                BatchList = "";
                            }

                            string[] RackstrId = data.RackId.Split(',');
                            string racknameStr = "";
                            foreach (var rackData in RackstrId)
                            {
                                long racid = Convert.ToInt64(rackData);
                                var rackname = db.M_RackMaster.Where(x => x.Id == racid).Select(x => x.Rack).FirstOrDefault();
                                racknameStr = racknameStr + "," + rackname;
                            }
                            if (racknameStr != "")
                            {
                                racknameStr = racknameStr.Remove(racknameStr.Length - 1, 1);
                            }
                            else
                            {
                                racknameStr = "";
                            }

                            int IAid1 = Convert.ToInt32(data.Item_Attribute_Id);
                            int ISid1 = 0;
                            long itemid1 = Convert.ToInt64(data.ItemId);
                            string ItemName1 = grnobj.getItemName(itemid1, IAid1, ISid1);
                            str = str + Convert.ToString(data.ItemId) + "~" + ItemName1 + "~" + Convert.ToString(data.Item_Attribute_Id) + "~" + data.BatchLot + "~" + BatchList + "~" + Convert.ToString(data.BatchLotQty) + "~" + data.RackId + "~" + racknameStr + "~" + data.RackQty + "#";
                        }

                        if (str != "")
                        {
                            entity.RackAllocation = str;
                        }
                    }
                    else
                    {
                        entity.RackAllocation = "";
                    }
                    list.Add(entity);
                }
            }
            return list;
        }
        public List<FG_TransferMasterEntity> Get()
        {
            List<FG_TransferMasterEntity> list = new List<FG_TransferMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.FG_TransferMaster.ToList().Where(x => x.IsActive == true);
                foreach (var item in records)
                {

                    FG_TransferMasterEntity entity = new FG_TransferMasterEntity();
                    entity.Id = item.Id;
                    entity.TransferNo = item.TransferNo;
                    entity.TransferDate = item.TransferDate;
                    entity.Comment = item.Comment;
                    entity.DepartmentId = item.DepartmentId;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity departentity = new M_DepartmentMasterEntity();
                        departentity.Id = item.M_DepartmentMaster.Id;
                        departentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        entity.M_DepartmentMaster = departentity;
                    }
                    entity.ProductionPlanDate = item.ProductionPlanDate;
                    entity.ProductionPlanId = item.ProductionPlanId;
                    entity.RefWorkOrderId = item.RefWorkOrderId;
                    entity.RefWorkOrderDate = item.RefWorkOrderDate;
                    if (item.MFG_WORK_ORDER != null)
                    {
                        MFG_WORK_ORDEREntity workorderentity = new MFG_WORK_ORDEREntity();
                        workorderentity.WorkOrderID = item.MFG_WORK_ORDER.WorkOrderID;
                        workorderentity.WorkOrderNo = item.MFG_WORK_ORDER.WorkOrderNo;
                        workorderentity.Date = item.MFG_WORK_ORDER.Date;
                        entity.MFG_WORK_ORDER = workorderentity;
                    }
                    entity.WarehouseId = item.WarehouseId;
                    if (item.WarehouseId > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseId).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WAREHOUSE = locentity;
                        }
                    }

                    entity.IsActive = item.IsActive;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsStatus = item.IsStatus;
                    list.Add(entity);
                }
            }
            return list;
        }

        public FG_TransferMasterEntity getbyid(long id)
        {
            FG_TransferMasterEntity entity = new FG_TransferMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.FG_TransferMaster.Find(id);
                if (item != null)
                {
                    entity.Id = item.Id;
                    entity.TransferNo = item.TransferNo;
                    entity.TransferDate = item.TransferDate;
                    entity.Comment = item.Comment;
                    entity.DepartmentId = item.DepartmentId;
                    if (item.M_DepartmentMaster != null)
                    {
                        M_DepartmentMasterEntity departentity = new M_DepartmentMasterEntity();
                        departentity.Id = item.M_DepartmentMaster.Id;
                        departentity.Departmentname = item.M_DepartmentMaster.Departmentname;
                        entity.M_DepartmentMaster = departentity;
                    }
                    entity.ProductionPlanDate = item.ProductionPlanDate;
                    entity.ProductionPlanId = item.ProductionPlanId;
                    
                    entity.RefWorkOrderId = item.RefWorkOrderId;
                    entity.RefWorkOrderDate = item.RefWorkOrderDate;
                    if (item.MFG_WORK_ORDER != null)
                    {
                        MFG_WORK_ORDEREntity workorderentity = new MFG_WORK_ORDEREntity();
                        workorderentity.WorkOrderID = item.MFG_WORK_ORDER.WorkOrderID;
                        workorderentity.WorkOrderNo = item.MFG_WORK_ORDER.WorkOrderNo;
                        workorderentity.Date = item.MFG_WORK_ORDER.Date;
                        entity.MFG_WORK_ORDER = workorderentity;
                    }
                    entity.WarehouseId = item.WarehouseId;
                    if (item.WarehouseId > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseId).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WAREHOUSE = locentity;
                        }
                    }

                   
                    entity.IsActive = item.IsActive;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsStatus = item.IsStatus;
                    entity.ItemId = item.ItemId;
                    entity.Item_Attribute_Id = item.Item_Attribute_Id > 0 ? item.Item_Attribute_Id : 0;
                    int IAid = item.Item_Attribute_Id!=null ? Convert.ToInt32(item.Item_Attribute_Id) : 0;
                    int ISid = 0;
                    M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                    if (item.ItemId != null)
                    {
                        itementity.Id =Convert.ToInt64(item.ItemId);
                        
                        long itemid = Convert.ToInt64(item.ItemId);
                        itementity.ItemName = grnobj.getItemName(itemid, IAid, ISid);
                        entity.M_ItemMaster = itementity;
                    }
                    else
                    {
                        itementity.Id = 0;
                        itementity.ItemName = "";
                        entity.M_ItemMaster = itementity;
                    }
                    
                    entity.HSNCode = item.HSNCode;
                    entity.Unit = item.Unit != null ? item.Unit : 0;
                    long unitid = item.Unit != null ? Convert.ToInt64(item.Unit) : 0;
                    if(unitid>0)
                    {
                        entity.UnitName = db.M_UnitMaster.Where(x => x.Id == unitid).Select(x => x.ItemUnit).FirstOrDefault();
                    }
                    else
                    {
                        entity.UnitName = "";
                    }

                    entity.WOQty = item.WOQty != null ? item.WOQty : 0;
                    entity.TotBatchQty = item.TotBatchQty != null ? item.TotBatchQty : 0;
                    entity.ProducedQty = item.ProducedQty != null ? item.ProducedQty : 0;
                    entity.BalanceQty = item.BalanceQty != null ? item.BalanceQty : 0;


                    string Batchvalstr = "";
                    var ItemInId = db.FG_TransferDetail.Where(x => x.FG_TransferId == item.Id).ToList();
                    foreach (var IinData in ItemInId)
                    {
                        var ProInfo = db.M_Product_Info.Where(x => x.ItemInward_ID == IinData.batchlotid && x.Dynamic_Controls_Id == 4).ToList();
                        ProInfo = ProInfo != null && ProInfo.Count>0 ? ProInfo : db.M_Product_Info.Where(x => x.ItemInward_ID == IinData.batchlotid && x.Dynamic_Controls_Id == 3).ToList();
                        foreach (var PIdata in ProInfo)
                        {
                            Batchvalstr = Batchvalstr + PIdata.Dynamic_Controls_value + "~";
                        }
                        if (Batchvalstr != "")
                        {
                            Batchvalstr = Batchvalstr + IinData.batchlotid + "#";
                        }
                    }

                    if (Batchvalstr != "")
                    {
                        entity.BatchList = Batchvalstr.Remove(Batchvalstr.Length - 1, 1);
                    }
                    else
                    {
                        entity.BatchList = "";
                    }

                    var Rackdata = db.M_RackAllocation.Where(x => x.InwardId == item.Id && x.InventoryMode == 7).ToList();
                    if (Rackdata != null && Rackdata.Count>0)
                    {
                        var str = "";
                        foreach (var data in Rackdata)
                        {
                            string[] batchlotstrId = data.BatchLot.Split(',');
                            string Batchvalstr1 = "";
                            string BatchList = "";
                            foreach (var IinData in batchlotstrId)
                            {
                                long inid = Convert.ToInt64(IinData);
                                var ProInfo = db.M_Product_Info.Where(x => x.ItemInward_ID == inid && x.Dynamic_Controls_Id == 4).ToList();
                                ProInfo = ProInfo != null && ProInfo.Count>0 ? ProInfo : db.M_Product_Info.Where(x => x.ItemInward_ID == inid && x.Dynamic_Controls_Id == 3).ToList();
                                foreach (var PIdata in ProInfo)
                                {
                                    Batchvalstr1 = Batchvalstr1 + PIdata.Dynamic_Controls_value + ",";
                                }
                            }
                            if (Batchvalstr1 != "")
                            {
                                BatchList = Batchvalstr1.Remove(Batchvalstr1.Length - 1, 1);
                            }
                            else
                            {
                                BatchList = "";
                            }

                            string[] RackstrId = data.RackId.Split(',');
                            string racknameStr = "";
                            foreach (var rackData in RackstrId)
                            {
                                long racid = Convert.ToInt64(rackData);
                                var rackname = db.M_RackMaster.Where(x => x.Id == racid).Select(x => x.Rack).FirstOrDefault();
                                racknameStr = racknameStr + rackname + ",";
                            }
                            if (racknameStr != "")
                            {
                                racknameStr = racknameStr.Remove(racknameStr.Length - 1, 1);
                            }
                            else
                            {
                                racknameStr = "";
                            }

                            int IAid1 = Convert.ToInt32(data.Item_Attribute_Id);
                            int ISid1 = 0;
                            long itemid = Convert.ToInt64(data.ItemId);
                            string ItemName = grnobj.getItemName(itemid, IAid1, ISid1);
                            str = str + Convert.ToString(data.ItemId) + "~" + ItemName + "~" + Convert.ToString(data.Item_Attribute_Id) + "~" + data.BatchLot + "~" + BatchList + "~" + Convert.ToString(data.BatchLotQty) + "~" + data.RackId + "~" + racknameStr + "~" + data.RackQty + "#";
                        }

                        if (str != "")
                        {
                            entity.RackAllocation = str;
                        }
                    }
                    else
                    {
                        entity.RackAllocation = "";
                    }

                    var itemrecords = db.FG_TransferDetail.Where(x => x.FG_TransferId == item.Id).ToList();
                    List<FG_TransferDetailEntity> itemlist = new List<FG_TransferDetailEntity>();
                    if (itemrecords != null)
                    {
                        foreach (var itemrecord in itemrecords)
                        {
                            FG_TransferDetailEntity TFitemeentty = new FG_TransferDetailEntity();
                            TFitemeentty.Id = itemrecord.Id;
                            TFitemeentty.CreatedBy = itemrecord.CreatedBy;
                            TFitemeentty.CreatedDate = itemrecord.CreatedDate;
                            TFitemeentty.BalanceQty = itemrecord.BalanceQty;
                            TFitemeentty.FG_TransferId = itemrecord.FG_TransferId;
                            if (itemrecord.FG_TransferMaster != null)
                            {
                                FG_TransferMasterEntity TFentity = new FG_TransferMasterEntity();
                                TFentity.Id = itemrecord.FG_TransferMaster.Id;
                                TFentity.TransferNo = itemrecord.FG_TransferMaster.TransferNo;
                                TFentity.TransferDate = itemrecord.FG_TransferMaster.TransferDate;
                                TFitemeentty.FG_TransferMaster = TFentity;
                            }
                            TFitemeentty.IsQC_Done = itemrecord.IsQC_Done;
                            TFitemeentty.UpdatedDate = itemrecord.UpdatedDate;
                            TFitemeentty.UpdatedBy = itemrecord.UpdatedBy;
                            TFitemeentty.IsActive = itemrecord.IsActive;
                            TFitemeentty.ProducedQty = itemrecord.ProducedQty;
                            TFitemeentty.BatchLotQty = itemrecord.BatchLotQty;
                            var ProInfo = db.M_Product_Info.Where(x => x.ItemInward_ID == itemrecord.batchlotid && x.Dynamic_Controls_Id == 4).FirstOrDefault();
                            ProInfo = ProInfo != null ? ProInfo : db.M_Product_Info.Where(x => x.ItemInward_ID == itemrecord.batchlotid && x.Dynamic_Controls_Id == 3).FirstOrDefault();
                            TFitemeentty.BtachLot = ProInfo.Dynamic_Controls_value;


                            itemlist.Add(TFitemeentty);
                        }
                        entity.FG_TransferDetail = itemlist;
                    }
                }
            }
            return entity;
        }



        public long Insert(FG_TransferMasterEntity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                var FGdat = db.FG_TransferMaster.Where(x => x.RefWorkOrderId == entity.RefWorkOrderId).Select(x => x.Id).ToList();
                foreach (var fgid in FGdat)
                {
                    MFG_WORK_ORDER_DETAIL detail1 = new MFG_WORK_ORDER_DETAIL();
                    var FGT = db.FG_TransferDetail.Where(x => x.FG_TransferId == fgid).ToList();
                    foreach (var wd in FGT)
                    {
                        wd.BalanceQty = 0;
                        db.SaveChanges();
                    }
                }

                FG_TransferMaster record = new FG_TransferMaster();
                record.DepartmentId = entity.DepartmentId;
                record.ProductionPlanDate = entity.ProductionPlanDate;
                record.ProductionPlanId = entity.ProductionPlanId;
                record.RefWorkOrderId = entity.RefWorkOrderId;
                record.RefWorkOrderDate = entity.RefWorkOrderDate;
                record.TransferDate = entity.TransferDate;
                record.WarehouseId = entity.WarehouseId;
                record.Comment = entity.Comment;               
                record.CreatedDate = DateTime.Now;
                record.CreatedBy = entity.CreatedBy;
                record.IsActive = entity.IsActive;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.BranchId;
                record.IsStatus = entity.IsStatus;
                record.ItemId = entity.ItemId;
                record.Item_Attribute_Id = entity.Item_Attribute_Id;
                record.HSNCode = entity.HSNCode;
                record.Unit = entity.Unit;
                record.WOQty = entity.WOQty;
                record.ProducedQty = entity.ProducedQty;
                record.BalanceQty = entity.BalanceQty;
                record.TotBatchQty = entity.TotBatchQty;
                record.TransferNo = getnextTFNo();
                db.FG_TransferMaster.Add(record);
                db.SaveChanges();
                id = record.Id;
                if (id > 0)
                {
                    foreach (var item in entity.FG_TransferDetail)
                    {
                        FG_TransferDetail itemrecord = new FG_TransferDetail();
                        itemrecord.BalanceQty = item.BalanceQty;
                        itemrecord.FG_TransferId = id;
                        itemrecord.IsQC_Done = item.IsQC_Done;
                        itemrecord.ProducedQty = item.ProducedQty;
                        itemrecord.CreatedBy = item.CreatedBy;
                        itemrecord.CreatedDate = DateTime.Now;
                        itemrecord.Company_ID = item.Company_ID;
                        itemrecord.BranchId = item.BranchId;
                        itemrecord.IsActive = true;
                        itemrecord.batchlotid = item.batchlotid;
                        itemrecord.BatchLotQty = item.BatchLotQty;
                        db.FG_TransferDetail.Add(itemrecord);
                        db.SaveChanges();
                    }
                }

                if (id > 0)
                {
                    List<bool> WOIdTF = new List<bool>();
                    var rec1 = db.FG_TransferMaster.Where(x => x.RefWorkOrderId == entity.RefWorkOrderId).Select(x => x.Id).ToList();
                    WOIdTF.Clear();
                    foreach (var dat2 in rec1)
                    {
                        var recd1 = db.FG_TransferDetail.Where(x => x.FG_TransferId == dat2).ToList();

                        foreach (var dat3 in recd1)
                        {
                            if (dat3.BalanceQty == 0)
                            {
                                WOIdTF.Add(true);
                            }
                            else
                            {
                                WOIdTF.Add(false);
                            }
                        }
                    }
                    int chki = WOIdTF.Count(ai => ai == false);
                    if (chki == 0)
                    {
                        var data = db.MFG_WORK_ORDER.Find(entity.RefWorkOrderId);
                        data.IsStatus = 4;
                        data.UpdatedDate = DateTime.Now;
                        data.UpdatedBy = entity.CreatedBy;
                        db.SaveChanges();

                        var indata = db.FG_TransferMaster.Where(x => x.RefWorkOrderId == entity.RefWorkOrderId).Select(x => x.Id).ToList();
                        foreach (var did in indata)
                        {
                            var GRNdata = db.FG_TransferMaster.Find(did);
                            GRNdata.IsStatus = 4;
                            GRNdata.UpdatedDate = DateTime.Now;
                            GRNdata.UpdatedBy = entity.CreatedBy;
                            db.SaveChanges();
                        }

                    }
                }

            }
            return id;
        }

        public bool Update(FG_TransferMasterEntity entity)
        {
            bool isupdatd = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    FG_TransferMaster record = db.FG_TransferMaster.Find(entity.Id);                  
                    record.Id = entity.Id;
                    record.DepartmentId = entity.DepartmentId;                
                    record.WarehouseId = entity.WarehouseId;
                    record.Comment = entity.Comment;                  
                    record.UpdatedDate = entity.UpdatedDate;
                    record.UpdatedBy = entity.UpdatedBy;
                    record.IsActive = entity.IsActive;
                    db.SaveChanges();
                    isupdatd = true;
                    foreach (var item in entity.FG_TransferDetail)
                    {

                        FG_TransferDetail itemrecord = db.FG_TransferDetail.Find(item.Id);
                        itemrecord.Id = item.Id;
                        itemrecord.BalanceQty = item.BalanceQty;
                        itemrecord.FG_TransferId = entity.Id;
                        itemrecord.IsQC_Done = item.IsQC_Done;
                        itemrecord.ProducedQty = item.ProducedQty;                 
                        itemrecord.IsActive = item.IsActive;
                        itemrecord.UpdatedBy = item.UpdatedBy;
                        itemrecord.UpdatedDate = DateTime.Now;                     
                        itemrecord.IsActive = true;
                        
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return isupdatd;
        }

        public bool Delete(long id)
        {
            bool deleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.FG_TransferMaster.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                deleted = true;
            }
            return deleted;
        }
    }
}
