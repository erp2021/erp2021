﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Classes
{
    public class cl_StoreWorkOrder
    {
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;
        cl_role rlobj = new cl_role();
        cl_Enquiry enqo = new cl_Enquiry();
        public string getnextSWOno()
        {
            string no;
            no = "SWO-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.MFG_Store_WorkOrder.Count() > 0 ? db.MFG_Store_WorkOrder.Max(x => x.Store_WO_Id) + 1 : 1;

                no = no + "-" + record.ToString();
            }

            return no;
        }

        public string getnextprno()
        {
            string no;
            no = "PR-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.SRM_PurchaseRequestMaster.FirstOrDefault() != null ? db.SRM_PurchaseRequestMaster.Max(x => x.Id) + 1 : 1;
                no = no + "-" + record.ToString();
            }

            return no;
        }
        public DataTable CheckBOM(long StoreItemId, long ItemSizeId, decimal ItemQty,int companyid,int branchid)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection cs = new SqlConnection(con))
                {

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cs;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SP_Store_WO_BOM";
                    cmd.Parameters.Add("StoreItemId", SqlDbType.BigInt).Value = StoreItemId; //Check Bill Of Material Create Or Not Using Customer PO 
                    cmd.Parameters.Add("ItemSizeId", SqlDbType.BigInt).Value = ItemSizeId;
                    cmd.Parameters.Add("ItemQty", SqlDbType.VarChar).Value =Convert.ToString(ItemQty);
                    cmd.Parameters.Add("CompanyId", SqlDbType.BigInt).Value = companyid;
                    cmd.Parameters.Add("BranchId", SqlDbType.BigInt).Value = branchid;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return dt;
        }

        public long AddUpdate(MFG_Store_WorkOrderEntity entity, List<MFG_Store_WO_ItemDetailsEntity> SWO_ItemEntity, List<SRM_PurchaseRequestItemDetailEntity> SWO_BOM_ItemEntity)
        {
            long id = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    if (entity.Store_WO_Id > 0)
                    {
                        var ent = db.MFG_Store_WorkOrder.Find(entity.Store_WO_Id);
                        ent.Store_WO_Date = entity.Store_WO_Date;
                        ent.Store_Name = entity.Store_Name;
                        ent.Store_Warehouse = entity.Store_Warehouse;
                        ent.Customer_Id = entity.Customer_Id;
                        ent.PO_ref_No = entity.PO_ref_No;
                        ent.PO_Id = entity.PO_Id;
                        ent.IsActive = entity.IsActive;
                        ent.Updated_By = entity.Created_By;
                        ent.Updated__Date = entity.Created_Date;
                        ent.Company_ID = entity.Company_ID;
                        ent.BranchId = entity.BranchId;
                        ent.ApproveDate = entity.ApproveDate;
                        ent.Comment = entity.Comment;
                        ent.IsStatus = entity.IsStatus;
                        ent.Approvalstatus = entity.Approvalstatus;
                        db.SaveChanges();
                        id = entity.Store_WO_Id;
                    }
                    else
                    {
                        MFG_Store_WorkOrder ent = new MFG_Store_WorkOrder();
                        ent.Store_WO_Date = entity.Store_WO_Date;
                        ent.Store_Name = entity.Store_Name;
                        ent.Store_Warehouse = entity.Store_Warehouse;
                        ent.Customer_Id = entity.Customer_Id;
                        ent.PO_ref_No = entity.PO_ref_No;
                        ent.PO_Id = entity.PO_Id;
                        ent.IsActive = entity.IsActive;
                        ent.Created_By = entity.Created_By;
                        ent.Created_Date = entity.Created_Date;
                        ent.Company_ID = entity.Company_ID;
                        ent.BranchId = entity.BranchId;
                        ent.ApproveDate = entity.ApproveDate;
                        ent.Comment = entity.Comment;
                        ent.IsStatus = entity.IsStatus;
                        ent.Approvalstatus = entity.Approvalstatus;
                        ent.Store_WO_No = Convert.ToString(getnextSWOno());
                        db.MFG_Store_WorkOrder.Add(ent);
                        db.SaveChanges();
                        id = ent.Store_WO_Id;
                    }
                
                    foreach(var itement in SWO_ItemEntity)
                    {
                        if(itement.SWO_Item_DetailsId>0)
                        {
                            var IE =db.MFG_Store_WO_ItemDetails.Find(itement.SWO_Item_DetailsId);
                            IE.Store_WO_Id = id;
                            IE.BranName = itement.BrandName;
                            IE.ItemCategoryId = itement.ItemCategoryId;
                            IE.ItemId = itement.ItemId;
                            IE.Item_Attribute_Id = itement.Item_Attribute_Id;
                            IE.UnitId = itement.UnitId;
                            IE.Quantity = itement.Quantity;
                            IE.IsActive = true;
                            IE.Updated_By = entity.Created_By;
                            IE.Updated__Date = entity.Created_Date;
                            IE.Company_ID = entity.Company_ID;
                            IE.BranchId = entity.BranchId;
                            IE.ItemCode = itement.ItemCode;
                            db.SaveChanges();
                        }
                        else
                        {
                            MFG_Store_WO_ItemDetails IE = new MFG_Store_WO_ItemDetails();
                            IE.Store_WO_Id = id;
                            IE.BranName = itement.BrandName;
                            IE.ItemCategoryId = itement.ItemCategoryId;
                            IE.ItemId = itement.ItemId;
                            IE.Item_Attribute_Id = itement.Item_Attribute_Id;
                            IE.UnitId = itement.UnitId;
                            IE.Quantity = itement.Quantity;
                            IE.IsActive = true;
                            IE.Created_By = entity.Created_By;
                            IE.Created_Date = entity.Created_Date;
                            IE.Company_ID = entity.Company_ID;
                            IE.BranchId = entity.BranchId;
                            IE.ItemCode = itement.ItemCode;
                            db.MFG_Store_WO_ItemDetails.Add(IE);
                            db.SaveChanges();
                        }
                    }

                    if (SWO_BOM_ItemEntity.Count > 0)
                    {
                        long PRid = 0;
                        long oldBOM = 0;
                        long StyleId = 0;
                        foreach (var boment in SWO_BOM_ItemEntity)
                        {
                            SRM_PurchaseRequestMaster PRentity = new SRM_PurchaseRequestMaster();
                            PRentity.PR_Date = DateTime.Now;
                            PRentity.BOM_Date = DateTime.Now;
                            var priority = "Normal";
                            PRentity.Priority = priority;
                            PRentity.IsActive = true;
                            PRentity.CreatedDate = DateTime.Now;
                            PRentity.ApprovalDate = DateTime.Now;
                            PRentity.CreatedBy =Convert.ToInt32(entity.Created_By);
                            PRentity.Company_ID = entity.Company_ID;
                            PRentity.BranchId = entity.BranchId;
                            PRentity.IsReadymade = 0;
                            PRentity.ApprovalDate = entity.ApproveDate;
                            PRentity.Comment = entity.Comment;
                            PRentity.IsStatus = entity.IsStatus;
                            PRentity.ApprovalStatus = entity.Approvalstatus;
                            PRentity.BOM_Id = boment.BOMID;
                            

                            if (PRid == 0 && oldBOM != boment.BOMID)
                            {
                                oldBOM = Convert.ToInt64(boment.BOMID);
                                PRentity.StyleId = boment.Styleid;
                                PRentity.PR_No = "RM-" + Convert.ToString(getnextprno());
                                db.SRM_PurchaseRequestMaster.Add(PRentity);
                                db.SaveChanges();
                                PRid = PRentity.Id;
                            }
                            if(PRid>0)
                            {
                                SRM_PurchaseRequestItemDetail record = new SRM_PurchaseRequestItemDetail();
                                record.ItemId = boment.ItemId;
                                record.Item_Attribute_ID = boment.Item_Attribute_ID;
                                record.PR_ID = PRid;
                                record.orderQty = boment.orderQty;
                                record.RequiredQty = boment.RequiredQty;
                                record.ItemCategoryId = boment.ItemCategoryId;
                                record.UnitId = boment.UnitId;
                                record.IsActive = true;
                                record.CreatedBy =Convert.ToInt32(entity.Created_By);
                                record.CreatedDate = DateTime.Now;
                                record.Company_ID = entity.Company_ID;
                                record.BranchId = entity.BranchId;
                                db.SRM_PurchaseRequestItemDetail.Add(record);
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return id;
        }

        public List<MFG_Store_WorkOrderEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<MFG_Store_WorkOrderEntity> list = new List<MFG_Store_WorkOrderEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.MFG_Store_WorkOrder.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.Created_By == Userid).OrderByDescending(x => x.Store_WO_Id).ToList();
                if (records.Count >= 0 && roleid != 4)
                {
                    records = db.MFG_Store_WorkOrder.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).OrderByDescending(x => x.Store_WO_Id).ToList();
                }
                foreach (var item in records)
                {
                    MFG_Store_WorkOrderEntity entity = new MFG_Store_WorkOrderEntity();
                    entity.Store_WO_Id = item.Store_WO_Id;
                    entity.Store_WO_No = item.Store_WO_No;
                    entity.Store_WO_Date = item.Store_WO_Date;
                    entity.Store_Name = item.Store_Name;
                    entity.StoreName = db.Stores.Where(x => x.Id == item.Store_Name).Select(x => x.Name).FirstOrDefault();
                    entity.Store_Warehouse = item.Store_Warehouse;
                    entity.StoreWarehouse = db.M_WAREHOUSE.Where(x => x.ID == item.Store_Warehouse).Select(x => x.SHORT_NAME).FirstOrDefault();
                    entity.Customer_Id = item.Customer_Id;
                    if(item.Customer_Id!=null)
                    {
                        entity.CustomerName = db.M_Ledgers.Where(x => x.Ledger_Id == item.Customer_Id).Select(x => x.Ledger_Name).FirstOrDefault();
                    }
                    entity.PO_ref_No = item.PO_ref_No;
                    entity.PO_Id = item.PO_Id;
                    entity.IsActive = item.IsActive;
                    entity.Created_By = item.Created_By;
                    entity.Created_Date = item.Created_Date;
                    entity.Updated_By = item.Updated_By;
                    entity.Updated__Date = item.Updated__Date;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.ApproveDate = item.ApproveDate;
                    entity.Comment = item.Comment;
                    entity.IsStatus = item.IsStatus;
                    entity.Approvalstatus = item.Approvalstatus;
                    list.Add(entity);
                }
                if (roleid != 4)
                {
                    var Userlist = rlobj.GetUserRole().Where(x => x.roleId == 4 || x.roleId == roleid).ToList();
                    list = list.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && Userlist.Any(y => y.UserId == x.Created_By)).OrderByDescending(x => x.Store_WO_Id).ToList();
                }
            }

            return list;

        }

        public List<MFG_Store_WO_ItemDetailsEntity> GetStoreWO_ItemData(long Store_WO_Id)
        {
            List<MFG_Store_WO_ItemDetailsEntity> list = new List<MFG_Store_WO_ItemDetailsEntity>();
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.MFG_Store_WO_ItemDetails.Where(x => x.IsActive == true && x.Store_WO_Id == Store_WO_Id).ToList();
                    foreach(var ent in record)
                    {
                        MFG_Store_WO_ItemDetailsEntity entity = new MFG_Store_WO_ItemDetailsEntity();
                        entity.SWO_Item_DetailsId = ent.SWO_Item_DetailsId;
                        entity.Store_WO_Id = ent.Store_WO_Id;
                        entity.BrandName = ent.BranName;
                        entity.ItemCategoryId = ent.ItemCategoryId;
                        entity.itemcatName = db.M_ItemCategoryMaster.Where(x => x.Id == ent.ItemCategoryId).Select(x => x.Itemcategory).FirstOrDefault();
                        entity.ItemId = ent.ItemId;
                        var itemEnt = db.M_ItemMaster.Where(x => x.Id == ent.ItemId).FirstOrDefault();
                        if (itemEnt != null)
                        {
                            M_ItemMasterEntity iteme = new M_ItemMasterEntity();
                            long itemid = Convert.ToInt64(ent.ItemId);
                            int Attiid = Convert.ToInt32(ent.Item_Attribute_Id);
                            entity.ItemName = enqo.getItemName(itemid, Attiid, 0);
                        }
                        entity.Item_Attribute_Id = ent.Item_Attribute_Id;
                        entity.UnitId = ent.UnitId;
                        entity.UnitName = db.M_UnitMaster.Where(x => x.Id == ent.UnitId).Select(x => x.ItemUnit).FirstOrDefault();
                        entity.Quantity = ent.Quantity;
                        entity.IsActive = ent.IsActive;
                        entity.Created_By = ent.Created_By;
                        entity.Created_Date = ent.Created_Date;
                        entity.Updated_By = ent.Updated_By;
                        entity.Updated__Date = ent.Updated__Date;
                        entity.Company_ID = ent.Company_ID;
                        entity.BranchId = ent.BranchId;
                        entity.ItemCode = ent.ItemCode;
                        list.Add(entity);
                    }
                }
             }
            catch (Exception)
            {

                throw;
            }
            return list;
        }

    }
}
