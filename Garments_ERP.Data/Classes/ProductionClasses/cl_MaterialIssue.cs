﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;
using Garments_ERP.Entity.ProductionEntities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Garments_ERP.Data.Admin
{
    public class cl_MaterialIssue
    {
        public string con = ConfigurationManager.ConnectionStrings["myconn"].ConnectionString;

        //List Of  Material Issue With Status and User Wise
        public List<MFG_MaterialIssueMasterEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<MFG_MaterialIssueMasterEntity> list = new List<MFG_MaterialIssueMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.MFG_MaterialIssueMaster.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).ToList();
                if (records.Count >= 0 && roleid != 4)
                {
                    string str = "select Id,WarehouseID,MaterialIssueNo,EmpID,StyleID,IssueDate,PPNo,PPDate,RefWO,RefWODate,Comment,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,IsActive,DeptId, ";
                    str = str + "Company_ID,BranchId,IsStatus ";
                    str = str + " from MFG_MaterialIssueMaster where IsActive=1  and IsStatus=" + IsStatus + " and Company_ID=" + Company_ID + " and BranchId=" + BranchId + " ";
                    records = db.MFG_MaterialIssueMaster.SqlQuery(str).ToList();
                }
                
                foreach (var item in records)
                {
                    MFG_MaterialIssueMasterEntity entity = new MFG_MaterialIssueMasterEntity();

                    entity.Id = item.Id;
                    entity.MaterialIssueNo = item.MaterialIssueNo;
                    if (item.Employee != null)
                    {
                        EmployeeEntity empentity = new EmployeeEntity();
                        empentity.Name = item.Employee.Name;
                        empentity.ID = item.Employee.ID;
                        empentity.EmpID = item.Employee.EmpID;
                        entity.Employee = empentity;
                    }
                    entity.WarehouseID = item.WarehouseID;
                    if (item.WarehouseID > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseID).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WAREHOUSE = locentity;
                        }
                    }

                    if (item.M_StyleMaster != null)
                    {
                        M_StyleMasterEntity styleentity = new M_StyleMasterEntity();
                        styleentity.Id = item.M_StyleMaster.Id;
                        styleentity.Stylename = item.M_StyleMaster.Stylename;
                        entity.M_StyleMaster = styleentity;
                    }
                    entity.StyleID = item.StyleID;
                    entity.IssueDate = item.IssueDate;
                    entity.PPNo = item.PPNo;
                    var _ppno = db.MFG_PRODUCTION_PLAN.Where(x => x.ID == item.PPNo && x.WorkOrderID == item.RefWO).Select(x => x.PlanNo).SingleOrDefault();
                    MFG_PRODUCTION_PLANEntity ppent = new MFG_PRODUCTION_PLANEntity();
                    ppent.PlanNo = _ppno;
                    entity.Productionplan = ppent;

                    entity.PPDate = item.PPDate;
                    entity.RefWO = item.RefWO;
                    var wono = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == item.RefWO).Select(x => x.WorkOrderNo).SingleOrDefault();
                    MFG_WORK_ORDEREntity woent = new MFG_WORK_ORDEREntity();
                    woent.WorkOrderNo = wono;
                    entity.MFG_WORK_ORDER = woent;

                    entity.RefWODate = item.RefWODate;
                    entity.Comment = item.Comment;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    var outdata = db.M_ItemOutwardMaster.Where(x => x.MI_ID == item.Id).Select(x => x.MI_ID).Distinct().SingleOrDefault();
                    List<M_ItemOutwardMasterEntity> outmastrentlist = new List<M_ItemOutwardMasterEntity>();
                    M_ItemOutwardMasterEntity outmastrent = new M_ItemOutwardMasterEntity();
                    if (outdata != null)
                    {
                        outmastrent.MI_ID = outdata;
                    }
                    else
                    {
                        outmastrent.MI_ID = 0;
                    }
                    outmastrentlist.Add(outmastrent);
                    entity.outwarditemlist = outmastrentlist;

                    list.Add(entity);
                }
            }

            return list;

        }


        ////List Of Material Issue With Status and User and Form Date And To Date Wise
        public List<MFG_MaterialIssueMasterEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<MFG_MaterialIssueMasterEntity> list = new List<MFG_MaterialIssueMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.MFG_MaterialIssueMaster.Where(x => x.IsActive == true && x.IssueDate >= from && x.IssueDate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).ToList();
                if (records.Count >= 0 && roleid != 4)
                {
                    string str = "select Id,WarehouseID,MaterialIssueNo,EmpID,StyleID,IssueDate,PPNo,PPDate,RefWO,RefWODate,Comment,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,IsActive,DeptId, ";
                    str = str + "Company_ID,BranchId,IsStatus ";
                    str = str + " from MFG_MaterialIssueMaster where IsActive=1 and IssueDate >= convert(datetime,'" + from + "',103) and IssueDate <=convert(datetime,'" + to + "',103)  and IsStatus=" + IsStatus + " and Company_ID=" + Company_ID + " and BranchId=" + BranchId + "";
                    records = db.MFG_MaterialIssueMaster.SqlQuery(str).ToList();
                }
                foreach (var item in records)
                {
                    MFG_MaterialIssueMasterEntity entity = new MFG_MaterialIssueMasterEntity();

                    entity.Id = item.Id;
                    entity.MaterialIssueNo = item.MaterialIssueNo;
                    if (item.Employee != null)
                    {
                        EmployeeEntity empentity = new EmployeeEntity();
                        empentity.Name = item.Employee.Name;
                        empentity.ID = item.Employee.ID;
                        empentity.EmpID = item.Employee.EmpID;
                        entity.Employee = empentity;
                    }
                    entity.WarehouseID = item.WarehouseID;
                    if (item.WarehouseID > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseID).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WAREHOUSE = locentity;
                        }
                    }

                    if (item.M_StyleMaster != null)
                    {
                        M_StyleMasterEntity styleentity = new M_StyleMasterEntity();
                        styleentity.Id = item.M_StyleMaster.Id;
                        styleentity.Stylename = item.M_StyleMaster.Stylename;
                        entity.M_StyleMaster = styleentity;
                    }
                    entity.StyleID = item.StyleID;
                    entity.IssueDate = item.IssueDate;
                    entity.PPNo = item.PPNo;
                    var _ppno = db.MFG_PRODUCTION_PLAN.Where(x => x.ID == item.PPNo && x.WorkOrderID == item.RefWO).Select(x => x.PlanNo).SingleOrDefault();
                    MFG_PRODUCTION_PLANEntity ppent = new MFG_PRODUCTION_PLANEntity();
                    ppent.PlanNo = _ppno;
                    entity.Productionplan = ppent;

                    entity.PPDate = item.PPDate;
                    entity.RefWO = item.RefWO;
                    var wono = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == item.RefWO).Select(x => x.WorkOrderNo).SingleOrDefault();
                    MFG_WORK_ORDEREntity woent = new MFG_WORK_ORDEREntity();
                    woent.WorkOrderNo = wono;
                    entity.MFG_WORK_ORDER = woent;

                    entity.RefWODate = item.RefWODate;
                    entity.Comment = item.Comment;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    var outdata = db.M_ItemOutwardMaster.Where(x => x.MI_ID == item.Id).Select(x => x.MI_ID).Distinct().SingleOrDefault();
                    List<M_ItemOutwardMasterEntity> outmastrentlist = new List<M_ItemOutwardMasterEntity>();
                    M_ItemOutwardMasterEntity outmastrent = new M_ItemOutwardMasterEntity();
                    if (outdata != null)
                    {
                        outmastrent.MI_ID = outdata;
                    }
                    else
                    {
                        outmastrent.MI_ID = 0;
                    }
                    outmastrentlist.Add(outmastrent);
                    entity.outwarditemlist = outmastrentlist;

                    list.Add(entity);
                }
            }
            return list;
        }


        public MFG_MaterialIssueItemDetailsEntity getbyitemid(long itemid, long MIid)
        {
            MFG_MaterialIssueItemDetailsEntity entity = new MFG_MaterialIssueItemDetailsEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.MFG_MaterialIssueItemDetails.Where(x => x.ItemID == itemid && x.MaterialIssueid== MIid && x.IsActive == true).FirstOrDefault();
                if (record != null)
                {
                    entity.Id = record.Id;
                    entity.ItemID = record.ItemID;
                    entity.UOM_ID =Convert.ToInt32(record.UOM_ID);
                    M_UnitMasterEntity unitent = new M_UnitMasterEntity();
                    var unitanme = db.M_UnitMaster.Where(x => x.Id == record.UOM_ID).Select(x => x.ItemUnit).SingleOrDefault();
                    unitent.Unitdesc = unitanme;
                    entity.M_UnitMaster = unitent;
                    entity.ItemSubCategoryID = record.ItemSubCategoryID;
                    decimal totoused=0;

                    List<M_ItemOutwardMasterEntity> lsyoutitem = new List<M_ItemOutwardMasterEntity>();
                    var itemdata = db.M_ItemOutwardMaster.Where(x => x.Item_ID == itemid && x.MI_ID == MIid && x.IsActive == true).Select(x=>x.ItemQty).ToList();
                    if (itemdata != null)
                    {
                        foreach (var itemd in itemdata)
                        {
                            totoused = Convert.ToDecimal(totoused) + Convert.ToDecimal(itemd.Value);
                        }
                    }

                    entity.IssueQty = totoused>record.IssueQty?0: Convert.ToDecimal(record.IssueQty)-Convert.ToDecimal(totoused);
                }
                else
                {
                    var recorditem = db.M_ItemMaster.Where(x => x.Id == itemid  && x.IsActive == true).FirstOrDefault();
                    if (recorditem != null)
                    {
                        entity.Id = recorditem.Id;
                        entity.ItemID = recorditem.Id;
                        entity.UOM_ID = Convert.ToInt32(recorditem.Unit);
                        M_UnitMasterEntity unitent = new M_UnitMasterEntity();
                        var unitanme = db.M_UnitMaster.Where(x => x.Id == recorditem.Unit).Select(x => x.ItemUnit).SingleOrDefault();
                        unitent.Unitdesc = unitanme;
                        entity.M_UnitMaster = unitent;
                        entity.ItemSubCategoryID = recorditem.Itemsubcategoryid;
                        decimal totoused = 0;

                        List<M_ItemOutwardMasterEntity> lsyoutitem = new List<M_ItemOutwardMasterEntity>();
                        var itemdata = db.M_ItemOutwardMaster.Where(x => x.Item_ID == itemid && x.MI_ID == MIid && x.IsActive == true).Select(x => x.ItemQty).ToList();
                        if (itemdata != null)
                        {
                            foreach (var itemd in itemdata)
                            {
                                totoused = Convert.ToDecimal(totoused) + Convert.ToDecimal(itemd.Value);
                            }
                        }

                        entity.IssueQty = totoused > recorditem.OpeningStock ? 0 : Convert.ToDecimal(recorditem.OpeningStock) - Convert.ToDecimal(totoused);
                    }
                }
            }
            return entity;
        }

        public MFG_WorkOrderDetailsForProductionPlan GetWorkOrderDetails(long workOrderID, long planID,long MIid)
        {
            MFG_WorkOrderDetailsForProductionPlan entity = new MFG_WorkOrderDetailsForProductionPlan();
            cl_CustomerPO poobj = new cl_CustomerPO();
            cl_ProductionPlan ppobj = new cl_ProductionPlan();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.MFG_WORK_ORDER.Find(workOrderID);
                if (item != null)
                {
                    entity.WorkOrderEntity.WorkOrderID = item.WorkOrderID;
                    entity.WorkOrderEntity.CustomerID = item.CustomerID;
                    entity.WorkOrderEntity.POID = item.POID;
                    entity.WorkOrderEntity.StyleID = item.StyleID;
                    entity.WorkOrderEntity.WorkOrderNo = item.WorkOrderNo;
                    entity.WorkOrderEntity.StyleType = item.StyleType;
                    entity.WorkOrderEntity.DepartmentID = item.DepartmentID;
                    entity.WorkOrderEntity.RevisionNo = item.RevisionNo;
                    entity.WorkOrderEntity.RequiredQty = item.RequiredQty;
                    entity.WorkOrderEntity.UnitID = item.UnitID;
                    entity.WorkOrderEntity.Date = item.Date;
                    entity.WorkOrderEntity.Approved = item.Approved;
                    entity.WorkOrderEntity.ApproveDate = item.ApproveDate;
                    entity.WorkOrderEntity.Comment = item.Comment;
                    entity.WorkOrderEntity.RequiredQty = item.RequiredQty;

                    // var PPRecord = ppobj.Getbyid(planID);
                    // entity.WorkOrderEntity.customerpo = customerPORecord;

                    var customerPORecord = poobj.getbyid(item.POID);
                    entity.WorkOrderEntity.customerpo = customerPORecord;

                    List<MFG_ProductionPlanProcessDetails> Processes = new List<MFG_ProductionPlanProcessDetails>();
                    List<M_BillOfMaterialDetailEntity> Bomdetails = new List<M_BillOfMaterialDetailEntity>();
                    var enquiryDetails = db.CRM_CustomerPOMaster.Include("CRM_QuotationMaster.CRM_EnquiryMaster").Where(x => x.Id == item.POID).FirstOrDefault();
                    if (enquiryDetails != null && enquiryDetails.CRM_QuotationMaster != null)
                    {
                        long enquiryID = enquiryDetails.CRM_QuotationMaster.EnquiryID;
                        var bom = db.M_BillOfMaterialMaster.Where(bm => bm.EnquiryId == enquiryID).FirstOrDefault();
                        bom= bom!=null? bom: db.M_BillOfMaterialMaster.Where(bm => bm.Poid == enquiryDetails.Id).FirstOrDefault();
                        if (bom != null)
                        {
                            int? processCycleID = bom.Prooce_Cycle_id;
                            long? BOM_ID = bom.BOM_ID;
                            


                            List<M_BillOfMaterialDetail> bomrawitemdetails = GetRawMaterialList(bom.BOM_ID, MIid);
                            foreach (var rawitem in bomrawitemdetails)
                            {
                                M_BillOfMaterialDetailEntity bomdetailentity = new M_BillOfMaterialDetailEntity();
                                bomdetailentity.ID = rawitem.ID;
                                bomdetailentity.BOM_ID = rawitem.BOM_ID;
                                bomdetailentity.ItemId = rawitem.ItemId;

                                if (rawitem.ItemId != null)
                                {
                                    var itemdata = db.M_ItemMaster.Where(x => x.Id == rawitem.ItemId).SingleOrDefault();
                                    M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                                    itementity.Id = itemdata.Id;
                                    itementity.ItemName = itemdata.ItemName;
                                    bomdetailentity.M_ItemMaster = itementity;
                                }
                                
                                bomdetailentity.ItemSubCategoryId = rawitem.ItemSubCategoryId;
                                bomdetailentity.UnitId = rawitem.UnitId;
                                if (rawitem.UnitId != null)
                                {
                                    var unitdata = db.M_UnitMaster.Where(x => x.Id == rawitem.UnitId).SingleOrDefault();
                                    M_UnitMasterEntity unitentity = new M_UnitMasterEntity();
                                    unitentity.Id = unitdata.Id;
                                    unitentity.ItemUnit = unitdata.ItemUnit;
                                    bomdetailentity.M_UnitMaster = unitentity;
                                }
                                Bomdetails.Add(bomdetailentity);
                            }
                            List<M_BOM_ProcessCycleEntity> BOM_PC = new List<M_BOM_ProcessCycleEntity>();
                            var processCylceTrnsctions = db.M_BOM_ProcessCycle.Where(x => x.BOM_ID == bom.BOM_ID).ToList();
                            foreach (var proc in processCylceTrnsctions)
                            {
                                MFG_ProductionPlanProcessDetails _proc = new MFG_ProductionPlanProcessDetails();
                                _proc.ProcessID = proc.ProcessId;
                                _proc.Processsequence = proc.Processsequence;
                                _proc.SHORT_NAME =db.M_PROCESS_Master.Where(x=>x.Process_Id == proc.ProcessId).Select(x=>x.SHORT_NAME).FirstOrDefault();
                                var opid = db.M_PROCESS_Master.Where(x => x.Process_Id == proc.ProcessId).Select(x => x.Process_Incharge_ID).SingleOrDefault();
                                _proc.InchargeID = Convert.ToInt64(opid);
                                _proc.Operator = db.Employees.Where(x => x.UserId == opid).Select(x => x.Name).SingleOrDefault();
                                var procwestg = db.M_PROCESS_Master.Where(x => x.Process_Id == proc.ProcessId).Select(x => x.Process_Wastage).FirstOrDefault();
                                _proc.Wastage = (procwestg == null) ? 0 : procwestg;
                                int ISid = 0;
                                int iaid =Convert.ToInt32(proc.Item_Attribute_ID);
                                long itemid = Convert.ToInt64(proc.ItemOutId);
                                _proc.ProcessProduct = getItemName(itemid, iaid, ISid);
                                _proc.ProcessProductid = itemid;
                                _proc.ProcessProductidAttr = iaid;

                                Processes.Add(_proc);
                            }
                        }
                    }
                    entity.BOMDetailList = Bomdetails;
                    entity.ProcessList = Processes;
                }

            }

            return entity;
        }

        public string getItemName(int itemid, int attibid, int subcatid)
        {
            string itemname = "";
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 32; //Check Bill Of Material Create Or Not Using Customer PO 
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = Convert.ToString(subcatid);
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "<root><Row><ItemId>" + itemid + "</ItemId><AttributeId>" + attibid + "</AttributeId></Row></root>";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                foreach (DataTable table in dt.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        itemname = Convert.ToString(dr["ItemName"]);
                    }
                }

            }
            return itemname;
        }

        public List<M_BillOfMaterialDetail> GetRawMaterialList(long BOMId, long MIid)
        {
            List<M_BillOfMaterialDetail> list = new List<M_BillOfMaterialDetail>();
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_BOMAutoPR";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 5;//Get Raw Material Details
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = Convert.ToString(MIid);
                cmd.Parameters.Add("attributexml", SqlDbType.VarChar).Value = "<root><Row><BOMid>"+ BOMId + "</BOMid></Row></root>";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                foreach (DataTable table in dt.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        M_BillOfMaterialDetail ledenty = new M_BillOfMaterialDetail();
                        ledenty.ID = Convert.ToInt64(dr["ID"]);
                        ledenty.BOM_ID = Convert.ToInt64(dr["BOM_ID"]);
                        ledenty.ItemId = Convert.ToInt64(dr["ItemId"]);
                        ledenty.ItemSubCategoryId = Convert.ToInt32(dr["ItemSubCategoryId"]);
                        ledenty.UnitId = Convert.ToInt32(dr["UnitId"]);
                        list.Add(ledenty);
                    }
                }


            }

            return list;
        }

        public string getnextmino()
        {
            string no;
            no = "MI-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.MFG_MaterialIssueMaster.FirstOrDefault() != null ? db.MFG_MaterialIssueMaster.Max(x => x.Id) + 1 : 1;
                no = no + "-" + record.ToString();
            }

            return no;
        }

        public string getnextTno()
        {
            string no;
            no = "T-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.T_ITEM_TRANSFER.FirstOrDefault() != null ? db.T_ITEM_TRANSFER.Max(x => x.TRANSFER_ID) + 1 : 1;
                no = no + "-" + record.ToString();
            }

            return no;
        }

        public List<SP_GetDetailsForMaterialIssueEntity> GetWOProcessMaterialDetails(long woId)
        {
            List<SP_GetDetailsForMaterialIssueEntity> list = new List<SP_GetDetailsForMaterialIssueEntity>();
            using (var db = new GarmentERPDBEntities())
            {

                //var records= db.SP_GetDetailsForMaterialIssue(woId).GroupBy(x=>x.ProcessID).Select(g => new
                //{
                //    ProcessID = g.Key,
                //    ProcessName = g.First().ProcessName,
                //    ItemID = g.First().ItemID,
                //    ItemName = g.First().ItemName,
                //    Itemsubcategory = g.First().Itemsubcategory,
                //    ItemSubCategoryID = g.First().ItemSubCategoryID,
                //    UnitID = g.First().UnitID,
                //    ItemUnit = g.First().ItemUnit,
                //    RequiredQty = g.First().RequiredQty,
                //    IssuedQty = g.First().IssuedQty
                //});  foreach (var item in records.ToList())
                // List<SP_GetDetailsForMaterialIssueEntity> processList = new List<SP_GetDetailsForMaterialIssueEntity>();
                //var records = db.SP_GetDetailsForMaterialIssue(woId).ToList();
                //var processrecords = list.Select(x => x.ProcessName).Distinct().ToList();
                //foreach (var record in processrecords.ToList())
                //{

                //    var distinctProcess = records.Find(x => x.ProcessName == record);
                //    if (distinctProcess !=null)
                //    {
                //        foreach (var item in records)
                //        {
                //            SP_GetDetailsForMaterialIssueEntity entity = new SP_GetDetailsForMaterialIssueEntity();
                //            entity.ProcessName = item.ProcessName;
                //            entity.ProcessID = item.ProcessID;
                //            entity.ItemID = item.ItemID;
                //            entity.ItemName = item.ItemName;
                //            entity.Itemsubcategory = item.Itemsubcategory;
                //            entity.ItemSubCategoryID = item.ItemSubCategoryID;
                //            entity.UnitID = item.UnitID;
                //            entity.ItemUnit = item.ItemUnit;
                //            entity.RequiredQty = item.RequiredQty;
                //            entity.IssuedQty = item.IssuedQty;
                //            list.Add(entity);
                //        }
                //    }                  
                //}

                var records = db.SP_GetDetailsForMaterialIssue(woId).ToList();
                foreach (var item in records)
                {
                    SP_GetDetailsForMaterialIssueEntity entity = new SP_GetDetailsForMaterialIssueEntity();
                    entity.ProcessName = item.ProcessName;
                    entity.ProcessID = item.ProcessID;
                    entity.ItemID = item.ItemID;
                    entity.ItemName = item.ItemName;
                    entity.Itemsubcategory = item.Itemsubcategory;
                    entity.ItemSubCategoryID = item.ItemSubCategoryID;
                    entity.UnitID = item.UnitID;
                    entity.ItemUnit = item.ItemUnit;
                    entity.RequiredQty = item.RequiredQty;
                    entity.BalanceQty = item.BalanceQty;
                    entity.IssuedQty = item.IssuedQty;
                    list.Add(entity);
                }

            }
            return list;
        }
        public List<MFG_MaterialIssueMasterEntity> get()
        {
            List<MFG_MaterialIssueMasterEntity> list = new List<MFG_MaterialIssueMasterEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.MFG_MaterialIssueMaster.ToList().Where(x => x.IsActive == true);

                foreach (var item in records)
                {
                    MFG_MaterialIssueMasterEntity entity = new MFG_MaterialIssueMasterEntity();

                    entity.Id = item.Id;
                    entity.MaterialIssueNo = item.MaterialIssueNo;
                    if (item.Employee != null)
                    {
                        EmployeeEntity empentity = new EmployeeEntity();
                        empentity.Name = item.Employee.Name;
                        empentity.ID = item.Employee.ID;
                        empentity.EmpID = item.Employee.EmpID;
                        entity.Employee = empentity;
                    }
                    entity.WarehouseID = item.WarehouseID;
                    if (item.WarehouseID > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseID).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WAREHOUSE = locentity;
                        }
                    }

                    if (item.M_StyleMaster != null)
                    {
                        M_StyleMasterEntity styleentity = new M_StyleMasterEntity();
                        styleentity.Id = item.M_StyleMaster.Id;
                        styleentity.Stylename = item.M_StyleMaster.Stylename;
                        entity.M_StyleMaster = styleentity;
                    }
                    entity.StyleID = item.StyleID;
                    entity.IssueDate = item.IssueDate;
                    entity.PPNo = item.PPNo;
                    var _ppno = db.MFG_PRODUCTION_PLAN.Where(x => x.ID == item.PPNo && x.WorkOrderID == item.RefWO).Select(x => x.PlanNo).SingleOrDefault();
                    MFG_PRODUCTION_PLANEntity ppent = new MFG_PRODUCTION_PLANEntity();
                    ppent.PlanNo = _ppno;
                    entity.Productionplan = ppent;

                    entity.PPDate = item.PPDate;
                    entity.RefWO = item.RefWO;
                    var wono = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == item.RefWO).Select(x => x.WorkOrderNo).SingleOrDefault();
                    MFG_WORK_ORDEREntity woent = new MFG_WORK_ORDEREntity();
                    woent.WorkOrderNo = wono;
                    entity.MFG_WORK_ORDER = woent;

                    entity.RefWODate = item.RefWODate;
                    entity.Comment = item.Comment;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;

                    var outdata = db.M_ItemOutwardMaster.Where(x => x.MI_ID == item.Id).Select(x => x.MI_ID).Distinct().SingleOrDefault();
                    List<M_ItemOutwardMasterEntity> outmastrentlist = new List<M_ItemOutwardMasterEntity>();
                    M_ItemOutwardMasterEntity outmastrent = new M_ItemOutwardMasterEntity();
                    if (outdata!=null)
                    {
                        outmastrent.MI_ID = outdata;
                    }
                    else
                    {
                        outmastrent.MI_ID = 0;
                    }
                    outmastrentlist.Add(outmastrent);
                    entity.outwarditemlist = outmastrentlist;

                    list.Add(entity);
                }
            }
            return list;
        }
        public long Insert(MFG_MaterialIssueMasterEntity entity)
        {
            long id = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var MIlist = db.MFG_MaterialIssueMaster.Where(x => x.RefWO == entity.RefWO).ToList();
                    if(MIlist!=null)
                    {
                        foreach(var midata in MIlist)
                        {
                            MFG_MaterialIssueItemDetails detail1 = new MFG_MaterialIssueItemDetails();
                            var WOD = db.MFG_MaterialIssueItemDetails.Where(x => x.MaterialIssueid == midata.Id).ToList();
                            foreach (var wd in WOD)
                            {
                                wd.BalanceQty = 0;
                                db.SaveChanges();
                            }
                        }
                    }
                    MFG_MaterialIssueMaster record = new MFG_MaterialIssueMaster();
                    record.WarehouseID = entity.WarehouseID;
                    record.EmpID = entity.EmpID;
                    record.StyleID = entity.StyleID;
                    record.IssueDate = entity.IssueDate;
                    record.PPNo = entity.PPNo;
                    record.PPDate = entity.PPDate;
                    record.RefWO = entity.RefWO;
                    record.RefWODate = entity.RefWODate;
                    record.Comment = entity.Comment;
                    record.DeptId = entity.DeptId;
                    record.CreatedBy = entity.CreatedBy;
                    record.CreatedDate = DateTime.Now;
                    record.IsActive = entity.IsActive;
                    record.IsStatus = entity.IsStatus;
                    record.Company_ID = entity.Company_ID;
                    record.BranchId = entity.BranchId;
                    record.MaterialIssueNo = Convert.ToString(getnextmino());
                    db.MFG_MaterialIssueMaster.Add(record);
                    db.SaveChanges();
                    id = record.Id;
                    if (id > 0)
                    {
                        foreach (var item in entity.issueitemlist)
                        {
                            if (item.IssueQty > 0)
                            {
                                MFG_MaterialIssueItemDetails itemrecord = new MFG_MaterialIssueItemDetails();
                                itemrecord.Id = item.Id;
                                itemrecord.MaterialIssueid = id;
                                itemrecord.ItemSubCategoryID = item.ItemSubCategoryID;
                                itemrecord.ItemID = item.ItemID;
                                itemrecord.Item_Attribute_ID = item.Item_Attribute_ID; ;
                                itemrecord.UOM_ID = item.UOM_ID;
                                itemrecord.ReqQty = item.ReqQty;
                                itemrecord.AvailableQty = item.AvailableQty;
                                itemrecord.IssueQty = item.IssueQty;
                                itemrecord.BalanceQty = item.BalanceQty;
                                itemrecord.IsActive = true;
                                itemrecord.stocktype = item.stocktype;
                                itemrecord.Company_ID = item.Company_ID;
                                itemrecord.BranchId = item.BranchId;
                                db.MFG_MaterialIssueItemDetails.Add(itemrecord);
                                db.SaveChanges();
                            }
                            
                        }
                    }

                    if(id>0)
                    {
                        List<long> WOId = new List<long>();
                        List<bool> WOIdTF = new List<bool>();
                        var rec1 = db.MFG_MaterialIssueMaster.Where(x => x.RefWO == entity.RefWO && x.Company_ID == entity.Company_ID && x.BranchId == entity.BranchId).Select(x => x.Id).ToList();
                        WOIdTF.Clear();
                        foreach (var dat2 in rec1)
                        {
                            var recd1 = db.MFG_MaterialIssueItemDetails.Where(x => x.MaterialIssueid == dat2).ToList();

                            foreach (var dat3 in recd1)
                            {
                                if (dat3.BalanceQty == 0)
                                {
                                    WOIdTF.Add(true);
                                }
                                else
                                {
                                    WOIdTF.Add(false);
                                }
                            }
                        }
                        int chki = WOIdTF.Count(ai => ai == false);
                        if (chki == 0)
                        {
                            foreach (var dat2 in rec1)
                            {
                                var rec = db.MFG_MaterialIssueMaster.Find(dat2);
                                rec.UpdatedBy = entity.CreatedBy;
                                rec.UpdatedDate = DateTime.Now;
                                rec.IsStatus =4;
                                db.SaveChanges();
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return id;
        }
        public MFG_MaterialIssueMasterEntity getById(long id)
        {
            MFG_MaterialIssueMasterEntity entity = new MFG_MaterialIssueMasterEntity();

            using (var db = new GarmentERPDBEntities())
            {
                var item = db.MFG_MaterialIssueMaster.Find(id);
                entity.Id = item.Id;
                entity.MaterialIssueNo = item.MaterialIssueNo;
                if (item.Employee != null)
                {
                    EmployeeEntity empentity = new EmployeeEntity();
                    empentity.Name = item.Employee.Name;
                    empentity.ID = item.Employee.ID;
                    empentity.EmpID = item.Employee.EmpID;
                    entity.Employee = empentity;
                }
                entity.EmpID = item.EmpID;
                
                entity.WarehouseID = item.WarehouseID;
                if (item.WarehouseID > 0)
                {
                    var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseID).FirstOrDefault();
                    if (warehouse != null)
                    {
                        M_WarehouseEntity locentity = new M_WarehouseEntity();
                        locentity.ID = warehouse.ID;
                        locentity.SHORT_NAME = warehouse.SHORT_NAME;
                        entity.M_WAREHOUSE = locentity;
                    }
                }

                if (item.M_StyleMaster != null)
                {
                    M_StyleMasterEntity styleentity = new M_StyleMasterEntity();
                    styleentity.Id = item.M_StyleMaster.Id;
                    styleentity.Stylename = item.M_StyleMaster.Stylename;
                    entity.M_StyleMaster = styleentity;
                }
                entity.WarehouseID = item.WarehouseID;
                M_WarehouseEntity Whent = new M_WarehouseEntity();
                var WHname = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseID).Select(x => x.SHORT_NAME).FirstOrDefault();
                Whent.SHORT_NAME = WHname;
                entity.M_WAREHOUSE = Whent;

                entity.StyleID = item.StyleID;
                entity.IssueDate = item.IssueDate;
                entity.PPNo = item.PPNo;
                var _ppno = db.MFG_PRODUCTION_PLAN.Where(x => x.ID == item.PPNo && x.WorkOrderID == item.RefWO).Select(x => x.PlanNo).SingleOrDefault();
                MFG_PRODUCTION_PLANEntity ppent = new MFG_PRODUCTION_PLANEntity();
                ppent.PlanNo = _ppno;
                entity.Productionplan = ppent;

                entity.PPDate = item.PPDate;
                entity.RefWO = item.RefWO;
                var wono = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == item.RefWO).Select(x => x.WorkOrderNo).SingleOrDefault();
                MFG_WORK_ORDEREntity woent = new MFG_WORK_ORDEREntity();
                woent.WorkOrderNo = wono;
                entity.MFG_WORK_ORDER = woent;
                
                entity.RefWODate = item.RefWODate;
                entity.Comment = item.Comment;
                entity.CreatedDate = item.CreatedDate;
                entity.CreatedBy = item.CreatedBy;
                entity.UpdatedDate = item.UpdatedDate;
                entity.UpdatedBy = item.UpdatedBy;
                entity.IsActive = item.IsActive;
                entity.DeptId = item.DeptId;
                M_DepartmentMasterEntity deptent = new M_DepartmentMasterEntity();
                var deptname = db.M_DepartmentMaster.Where(x => x.Id == item.DeptId).Select(x => x.Departmentname).FirstOrDefault();
                deptent.Departmentname = deptname;
                entity.deptenty = deptent;

                var itemlist = db.MFG_MaterialIssueItemDetails.Where(x => x.MaterialIssueid == entity.Id && x.IsActive == true).ToList();
                List<MFG_MaterialIssueItemDetailsEntity> itemlist1 = new List<MFG_MaterialIssueItemDetailsEntity>();
                foreach (var item1 in itemlist)
                {
                    MFG_MaterialIssueItemDetailsEntity itemrecord = new MFG_MaterialIssueItemDetailsEntity();
                    itemrecord.Id = item1.Id;
                    itemrecord.MaterialIssueid = id;
                    itemrecord.ItemSubCategoryID = item1.ItemSubCategoryID;
                    var itemsub = db.M_ItemSubCategoryMaster.Where(x => x.Id == item1.ItemSubCategoryID).Select(x => x.Itemsubcategory).SingleOrDefault();
                    M_ItemSubCategoryMasterEntity subent = new M_ItemSubCategoryMasterEntity();
                    subent.Itemsubcategory = itemsub;
                    itemrecord.M_ItemSubCategoryMaster = subent;
                    itemrecord.ItemID = item1.ItemID;
                    
                    int IAid = Convert.ToInt32(item1.Item_Attribute_ID);
                    int ISid = Convert.ToInt32(item1.ItemSubCategoryID); 

                    if (item1.ItemID > 0)
                    {
                        M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                        itementity.ItemName = getItemName(item1.ItemID, IAid, ISid);
                        itemrecord.M_ItemMaster = itementity;
                    }
                    itemrecord.Item_Attribute_ID = item1.Item_Attribute_ID > 0 ? item1.Item_Attribute_ID : 0;
                    
                    itemrecord.UOM_ID = (int)item1.UOM_ID;
                    M_UnitMasterEntity unitentity = new M_UnitMasterEntity();
                    if (item1.M_UnitMaster != null)
                    {
                        unitentity.Id = item1.M_UnitMaster.Id;
                        unitentity.ItemUnit = item1.M_UnitMaster.ItemUnit;
                        itemrecord.M_UnitMaster = unitentity;
                    }
                    itemrecord.ReqQty = item1.ReqQty;
                    itemrecord.AvailableQty = item1.AvailableQty;
                    itemrecord.IssueQty = item1.IssueQty;
                    itemrecord.BalanceQty = item1.BalanceQty;
                    itemrecord.IsActive = true;
                    itemrecord.stocktype = item1.stocktype;

                    
                    itemlist1.Add(itemrecord);

                }
                entity.issueitemlist = itemlist1;
            }
            return entity;
        }

       
        public string getItemName(long itemid, int attibid, int subcatid)
        {
            string itemname = "";
            DataSet dt = new DataSet();
            using (SqlConnection cs = new SqlConnection(con))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cs;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Attribute";
                cmd.Parameters.Add("type", SqlDbType.Int).Value = 32; //Check Bill Of Material Create Or Not Using Customer PO 
                cmd.Parameters.Add("attribute", SqlDbType.VarChar).Value = Convert.ToString(subcatid);
                cmd.Parameters.Add("attributexml", SqlDbType.Xml).Value = "<root><Row><ItemId>" + itemid + "</ItemId><AttributeId>" + attibid + "</AttributeId></Row></root>";
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                foreach (DataTable table in dt.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        itemname = Convert.ToString(dr["ItemName"]);
                    }
                }

            }
            return itemname;
        }

        public bool Update(MFG_MaterialIssueMasterEntity entity)
        {
            bool isupdated = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    MFG_MaterialIssueMaster record = db.MFG_MaterialIssueMaster.Find(entity.Id);

                    //record.Id = entity.Id;
                    record.WarehouseID = entity.WarehouseID;
                    //record.MaterialIssueNo = entity.MaterialIssueNo;
                    record.EmpID = entity.EmpID;
                    record.DeptId = entity.DeptId;
                   // record.StyleID = entity.StyleID;
                    record.IssueDate = entity.IssueDate;
                    //record.PPNo = entity.PPNo;
                    record.PPDate = entity.PPDate;
                    //record.RefWO = entity.RefWO;
                  //  record.RefWODate = entity.RefWODate;
                    record.Comment = entity.Comment;
                    record.CreatedBy = entity.CreatedBy;
                    record.CreatedDate = entity.CreatedDate;
                    record.UpdatedDate = DateTime.Now;
                    record.UpdatedBy = entity.UpdatedBy;
                    //  record.IsActive = entity.IsActive;
                    db.SaveChanges();
                    isupdated = true;
                    if (isupdated == true)
                    {
                        foreach (var item in entity.issueitemlist)
                        {
                            if (item.Id != 0)
                            {
                                MFG_MaterialIssueItemDetails itemrecord = db.MFG_MaterialIssueItemDetails.Find(item.Id);
                                // itemrecord.Id = item.Id;
                                //itemrecord.MaterialIssueid = entity.Id;
                                itemrecord.ItemID = item.ItemID;
                                itemrecord.Item_Attribute_ID = item.Item_Attribute_ID;
                                itemrecord.UOM_ID = item.UOM_ID;
                                itemrecord.ReqQty = item.ReqQty;
                                itemrecord.AvailableQty = item.AvailableQty;
                                itemrecord.IssueQty = item.IssueQty;
                                itemrecord.BalanceQty = item.BalanceQty;
                                itemrecord.IsActive = true;
                                db.SaveChanges();
                            }
                            else
                            {
                                //insert code for new item
                                MFG_MaterialIssueItemDetails itemrecord = new MFG_MaterialIssueItemDetails();
                                itemrecord.Id = item.Id;
                                itemrecord.MaterialIssueid = entity.Id;
                                itemrecord.ItemID = item.ItemID;
                                itemrecord.UOM_ID = item.UOM_ID;
                                itemrecord.ReqQty = item.ReqQty;
                                itemrecord.AvailableQty = item.AvailableQty;
                                itemrecord.IssueQty = item.IssueQty;
                                itemrecord.BalanceQty = item.BalanceQty;
                                itemrecord.IsActive = true;
                                db.MFG_MaterialIssueItemDetails.Add(itemrecord);
                                db.SaveChanges();


                            }
                        }
                    }
                    if (isupdated == true)
                    {
                        foreach (var item in entity.styleimglist)
                        {
                            MFG_MaterialIssueStyleImage stylerecord = db.MFG_MaterialIssueStyleImage.Find(item.Id);
                            stylerecord.Id = item.Id;
                            stylerecord.StyleImgName = item.StyleImgName;
                            stylerecord.ItemID = item.ItemID;
                            //stylerecord.MaterialIssueId = entity.Id;
                            stylerecord.IsActive = true;
                            db.SaveChanges();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isupdated;
        }

        //Delete Material Issue
        public bool Delete(long id,int Uid)
        {
            bool isdeleted = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.MFG_MaterialIssueMaster.Find(id);
                    record.IsActive = false;
                    record.UpdatedBy = Uid;
                    record.UpdatedDate = DateTime.Now;
                    db.SaveChanges();
                    isdeleted = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isdeleted;
        }
        public bool deleterawitem(long id)
        {
            bool isdeleted = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.MFG_MaterialIssueItemDetails.Find(id);
                    record.IsActive = false;
                    db.SaveChanges();
                    isdeleted = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isdeleted;
        }

    }
}
