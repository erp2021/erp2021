﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Data.Classes.Helper;
using System.Globalization;

namespace Garments_ERP.Data.Admin
{
    public class cl_ProductionPlan
    {
        cl_GRNInward grnobj = new cl_GRNInward();

        //List Of  Work Order With Status and User Wise
        public List<MFG_PRODUCTION_PLANEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<MFG_PRODUCTION_PLANEntity> list = new List<MFG_PRODUCTION_PLANEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.MFG_PRODUCTION_PLAN.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy == Userid).ToList();
                if (records.Count == 0 && roleid != 4)
                {
                    string str = "select ID,PlanNo,PlanDate,CustomerID,WorkOrderID,POID,StyleID,SampleReferenceNo,StartDate,ProcessCycleID,Target_Qty,UnitID,TargetDate,WorkCenterID,DepartmentID, ";
                    str = str + "ToolID,OperatorID,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,ApprovedStatus,Comment,PerDayProduction,Company_ID,BranchId,IsStatus,ApproveDate,IsActive  ";
                    str = str + " from MFG_PRODUCTION_PLAN where IsActive=1  and IsStatus=" + IsStatus + " and Company_ID=" + Company_ID + " and BranchId=" + BranchId + " and CreatedBy in(select UserId from M_User_Role where roleId=4)";
                    records = db.MFG_PRODUCTION_PLAN.SqlQuery(str).ToList();
                }
                foreach (var item in records)
                {
                    MFG_PRODUCTION_PLANEntity entity = new MFG_PRODUCTION_PLANEntity();
                    entity.ID = item.ID;
                    entity.PlanNo = item.PlanNo;
                    entity.PlanDate = item.PlanDate;
                    entity.CustomerID = item.CustomerID;
                    entity.WorkOrderID = item.WorkOrderID;
                    entity.WorkOrderNo = item.MFG_WORK_ORDER.WorkOrderNo;
                    entity.POID = item.POID;
                    entity.StyleID = item.StyleID;
                    entity.StyleName = item.M_StyleMaster.Stylename;
                    entity.SampleReferenceNo = item.SampleReferenceNo;
                    entity.StartDate = item.StartDate;
                    entity.PlanDateString = Convert.ToDateTime(item.PlanDate).ToString(Helper.DATE_FORMAT_FOR_DISPLAY);
                    entity.StartDateString = Convert.ToDateTime(item.StartDate).ToString(Helper.DATE_FORMAT_FOR_DISPLAY);
                    entity.TargetDateString = Convert.ToDateTime(item.TargetDate).ToString(Helper.DATE_FORMAT_FOR_DISPLAY);
                    entity.Target_Qty = item.Target_Qty;
                    entity.TargetDate = item.TargetDate;
                    entity.WorkCenterID = item.WorkCenterID;
                    entity.Approved = (item.ApprovedStatus == null || item.ApprovedStatus == 0) ? 0 : 1;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.Comment = item.Comment;
                    entity.PerDayProduction = item.PerDayProduction;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsStatus = item.IsStatus;
                    entity.IsActive = item.IsActive == true ? true : false;
                    entity.ApprovedDate = item.ApproveDate;
                    list.Add(entity);
                }
            }

            return list;

        }


        //Reject PP to update Status
        public bool RejectedPP(long id, int Uid)
        {
            bool isdeleted = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.MFG_PRODUCTION_PLAN.Find(id);
                    record.IsStatus = 5;
                    record.UpdatedDate = DateTime.Now;
                    record.UpdatedBy = Convert.ToInt64(Uid);
                    db.SaveChanges();
                    isdeleted = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isdeleted;
        }
        

        public string getnextPNno()
        {
            string no;
            no = "PP-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.MFG_PRODUCTION_PLAN.Count() > 0 ? db.MFG_PRODUCTION_PLAN.Max(x => x.ID) + 1 : 1;

                //  var rec = db.CRM_QuotationMaster.Count() > 0 ? db.CRM_QuotationMaster.Max(x => x.Id) + 1 : 0;
                no = no + "-" + record.ToString();
            }

            return no;
        }

        //Delete Production Plan
        public bool Delete(long id, int Uid)
        {

            bool deleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.MFG_PRODUCTION_PLAN.Find(id);
                record.IsActive = false;
                record.UpdatedBy = Convert.ToInt64(Uid);
                record.UpdatedDate = DateTime.Now;
                db.SaveChanges();
                deleted = true;
            }
            return deleted;
        }


        ////List Of Work Order With Status and User and Form Date And To Date Wise
        public List<MFG_PRODUCTION_PLANEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<MFG_PRODUCTION_PLANEntity> list = new List<MFG_PRODUCTION_PLANEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.MFG_PRODUCTION_PLAN.Where(x => x.IsActive == true && x.PlanDate >= from && x.PlanDate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy == Userid).ToList();
                if (records.Count == 0 && roleid != 4)
                {
                    string str = "select ID,PlanNo,PlanDate,CustomerID,WorkOrderID,POID,StyleID,SampleReferenceNo,StartDate,ProcessCycleID,Target_Qty,UnitID,TargetDate,WorkCenterID,DepartmentID, ";
                    str = str + "ToolID,OperatorID,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,ApprovedStatus,Comment,PerDayProduction,Company_ID,BranchId,IsStatus,ApproveDate,IsActive  ";
                    str = str + " from MFG_PRODUCTION_PLAN where IsActive=1 and PlanDate >= convert(datetime,'" + from + "',103) and PlanDate <=convert(datetime,'" + to + "',103)  and IsStatus=" + IsStatus + " and Company_ID=" + Company_ID + " and BranchId=" + BranchId + " and CreatedBy in(select UserId from M_User_Role where roleId=4)";
                    records = db.MFG_PRODUCTION_PLAN.SqlQuery(str).ToList();
                }

                foreach (var item in records)
                {
                    MFG_PRODUCTION_PLANEntity entity = new MFG_PRODUCTION_PLANEntity();
                    entity.ID = item.ID;
                    entity.PlanNo = item.PlanNo;
                    entity.PlanDate = item.PlanDate;
                    entity.CustomerID = item.CustomerID;
                    entity.WorkOrderID = item.WorkOrderID;
                    entity.WorkOrderNo = item.MFG_WORK_ORDER.WorkOrderNo;
                    entity.POID = item.POID;
                    entity.StyleID = item.StyleID;
                    entity.StyleName = item.M_StyleMaster.Stylename;
                    entity.SampleReferenceNo = item.SampleReferenceNo;
                    entity.StartDate = item.StartDate;
                    entity.PlanDateString = Convert.ToDateTime(item.PlanDate).ToString(Helper.DATE_FORMAT_FOR_DISPLAY);
                    entity.StartDateString = Convert.ToDateTime(item.StartDate).ToString(Helper.DATE_FORMAT_FOR_DISPLAY);
                    entity.TargetDateString = Convert.ToDateTime(item.TargetDate).ToString(Helper.DATE_FORMAT_FOR_DISPLAY);
                    entity.Target_Qty = item.Target_Qty;
                    entity.TargetDate = item.TargetDate;
                    entity.WorkCenterID = item.WorkCenterID;
                    entity.Approved = (item.ApprovedStatus == null || item.ApprovedStatus == 0) ? 0 : 1;
                    entity.CreatedDate = item.CreatedDate;
                    entity.CreatedBy = item.CreatedBy;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.Comment = item.Comment;
                    entity.PerDayProduction = item.PerDayProduction;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsStatus = item.IsStatus;
                    entity.IsActive = item.IsActive == true ? true : false;
                    entity.ApprovedDate = item.ApproveDate;
                    list.Add(entity);
                }
            }
            return list;
        }


        public M_BillOfMaterialDetailEntity GetPerDayQty(int WOID, int ItemId,int iAttr)
        {
            M_BillOfMaterialDetailEntity list = new M_BillOfMaterialDetailEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var poid = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == WOID).Select(x => x.POID).FirstOrDefault();
                var Enqid = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == WOID).Select(x => x.EnquiryID).FirstOrDefault();
                var BOMId = db.M_BillOfMaterialMaster.Where(x => x.Poid == poid && x.IsActive==true).Select(x => x.BOM_ID).FirstOrDefault();
                BOMId= BOMId>0?BOMId : db.M_BillOfMaterialMaster.Where(x => x.EnquiryId == Enqid && x.IsActive == true).Select(x => x.BOM_ID).FirstOrDefault();
                var bomdata = db.M_BillOfMaterialDetail.Where(x => x.BOM_ID == BOMId && x.ItemId == ItemId && x.Item_Attribute_ID==iAttr && x.IsActive == true).Select(x => x.PerQty).FirstOrDefault();
                list.PerQty = bomdata;
            }

                return list;
        }

        public List<MFG_PRODUCTION_PLANEntity> getWODetials(int WOID)
        {
            List<MFG_PRODUCTION_PLANEntity> list = new List<MFG_PRODUCTION_PLANEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.MFG_PRODUCTION_PLAN.Include("MFG_WORK_ORDER").Include("M_StyleMaster").Where(x=>x.WorkOrderID==WOID).ToList();

                foreach (var item in records)
                {
                    MFG_PRODUCTION_PLANEntity entity = new MFG_PRODUCTION_PLANEntity();
                    entity.ID = item.ID;
                    entity.PlanNo = item.PlanNo;
                    entity.WorkOrderID = item.WorkOrderID;
                    entity.WorkOrderNo = item.MFG_WORK_ORDER.WorkOrderNo;
                    entity.POID = item.POID;
                    MFG_WORK_ORDEREntity woent = new MFG_WORK_ORDEREntity();
                    woent.wodate = Convert.ToString(item.MFG_WORK_ORDER.Date);
                    entity.MFG_WORK_ORDER = woent;

                    entity.MFG_WORK_ORDER = woent;
                    entity.StyleID = item.StyleID;
                    entity.StyleName = item.M_StyleMaster.Stylename;
                    entity.PlanDateString = Convert.ToDateTime(item.PlanDate).ToString();
                    entity.StartDateString = Convert.ToDateTime(item.StartDate).ToString();
                    entity.TargetDateString = Convert.ToDateTime(item.TargetDate).ToString();
                    entity.Target_Qty = item.Target_Qty;
                    entity.Approved = (item.ApprovedStatus == null || item.ApprovedStatus == 0) ? 0 : 1;
                    entity.CreatedDate = item.CreatedDate;
                    list.Add(entity);
                }

            }

            return list;
        }


        public List<MFG_PRODUCTION_PLANEntity> Get()
        {
            List<MFG_PRODUCTION_PLANEntity> list = new List<MFG_PRODUCTION_PLANEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.MFG_PRODUCTION_PLAN.Include("MFG_WORK_ORDER").Include("M_StyleMaster").ToList();

                foreach (var item in records)
                {
                    MFG_PRODUCTION_PLANEntity entity = new MFG_PRODUCTION_PLANEntity();
                    entity.ID = item.ID;
                    entity.PlanNo = item.PlanNo;
                    entity.WorkOrderID = item.WorkOrderID;
                    entity.WorkOrderNo = item.MFG_WORK_ORDER.WorkOrderNo;
                    entity.StyleID = item.StyleID;
                    entity.StyleName = item.M_StyleMaster.Stylename;
                    entity.PlanDateString = Convert.ToDateTime(item.PlanDate).ToString(Helper.DATE_FORMAT_FOR_DISPLAY);
                    entity.StartDateString = Convert.ToDateTime(item.StartDate).ToString(Helper.DATE_FORMAT_FOR_DISPLAY);
                    entity.TargetDateString = Convert.ToDateTime(item.TargetDate).ToString(Helper.DATE_FORMAT_FOR_DISPLAY);
                    entity.Target_Qty = item.Target_Qty;
                    entity.Approved = (item.ApprovedStatus == null || item.ApprovedStatus == 0) ? 0 : 1;
                    entity.CreatedDate = item.CreatedDate;
                    list.Add(entity);
                }

            }

            return list;
        }

        public long Insert(MFG_PRODUCTION_PLANEntity entity)
        {
            long PlanID = 0;
            using (var db = new GarmentERPDBEntities())
            {
                MFG_PRODUCTION_PLAN record = new MFG_PRODUCTION_PLAN();
                record.POID = entity.POID;
                record.CustomerID = entity.CustomerID;
                record.WorkOrderID = entity.WorkOrderID;
                record.StyleID = entity.StyleID;
                record.PlanDate = entity.PlanDate;
                record.SampleReferenceNo = entity.SampleReferenceNo;
                record.StartDate = entity.StartDate;
                record.TargetDate = entity.TargetDate;
                record.WorkCenterID = entity.WorkCenterID;
                record.ToolID = entity.ToolID;
                record.OperatorID = entity.OperatorID;
                record.CreatedBy = entity.CreatedBy;
                record.CreatedDate = entity.CreatedDate;
                record.Comment = entity.Comment;
                record.ApprovedStatus = entity.Approved;
                record.ApproveDate = entity.ApprovedDate;
                record.IsStatus = entity.IsStatus;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.BranchId;
                record.PlanNo = GetPlanNo();
                record.Target_Qty = entity.Target_Qty;
                record.PerDayProduction = entity.PerDayProduction;
                record.IsActive = entity.IsActive;
                //record.DepartmentID = entity.DepartmentID;
                // record.ProcessCycleID = entity.ProcessCycleID;
                //  record.UnitID = entity.UnitID;
                db.MFG_PRODUCTION_PLAN.Add(record);
                db.SaveChanges();
                PlanID = record.ID;
                long styleid= entity.StyleID; 
                if (PlanID > 0)
                {
                    foreach (var item in entity.detaillist)
                    {
                        MFG_PRODUCTION_PLAN_DETAILS detailentity = new MFG_PRODUCTION_PLAN_DETAILS();
                        try
                        {
                            detailentity.PlanID = PlanID;
                            detailentity.StyleID = styleid;
                            detailentity.Target_Qty = item.Target_Qty;
                            detailentity.Actual_Qty = item.Actual_Qty;
                            detailentity.Attribute_ID = item.Attribute_ID;
                            detailentity.Attribute_Value_ID = item.Attribute_Value_ID;
                            detailentity.rate = item.rate;
                            detailentity.SizeID = item.SizeID;
                            detailentity.Company_ID = item.Company_ID;
                            detailentity.BranchId = item.BranchId;
                            //detailentity.UnitID = item.UnitID;
                            //detailentity.MachineID = item.MachineID;
                            db.MFG_PRODUCTION_PLAN_DETAILS.Add(detailentity);
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            throw;
                        }
                        try
                        {
                            MFG_ALLOCATION allocationentity = new MFG_ALLOCATION();
                            allocationentity.StyleID = item.StyleID;
                            allocationentity.SizeID = item.SizeID;
                            allocationentity.PlanID = PlanID;
                            allocationentity.Approved = entity.Approved;
                            allocationentity.Company_ID = entity.Company_ID;
                            allocationentity.BranchId = entity.BranchId;
                            allocationentity.WorkOrderID = entity.WorkOrderID;
                           
                        }
                        catch (Exception ex)
                        {
                            throw;
                        }

                    }

                    var workOrder = db.MFG_WORK_ORDER.Where(wo => wo.WorkOrderID == entity.WorkOrderID).FirstOrDefault();
                    if (workOrder != null)
                    {
                        workOrder.PlanID = PlanID;
                        db.SaveChanges();
                    }

                }
            }

            return PlanID;
        }

        static string GetPlanNo()
        {
            DateTime dt = DateTime.Now;
            Random random = new Random();
            int randoNo = random.Next(1, 1000);

            return "PL-" + dt.Day + "" + dt.Month + "" + dt.Year + "" + randoNo.ToString("000000");
        }

        public bool Update(MFG_PRODUCTION_PLANEntity entity)
        {
            bool updated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.MFG_PRODUCTION_PLAN.Find(entity.ID);
                //record.PlanNo = entity.PlanNo;
                record.POID = entity.POID;
                record.WorkOrderID = entity.WorkOrderID;
                record.DepartmentID = entity.DepartmentID;
                record.StyleID = entity.StyleID;
                record.PlanDate = entity.PlanDate;
                record.SampleReferenceNo = entity.SampleReferenceNo;
                record.StartDate = entity.StartDate;
                record.ProcessCycleID = entity.ProcessCycleID;
                record.Target_Qty = entity.Target_Qty;
                record.UnitID = entity.UnitID;
                record.TargetDate = entity.TargetDate;
                record.WorkCenterID = entity.WorkCenterID;
                record.Comment = entity.Comment;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.BranchId;
                record.ApprovedStatus = entity.Approved;
                record.ApproveDate = entity.ApprovedDate;
                record.UpdatedBy = entity.UpdatedBy;
                record.UpdatedDate = DateTime.Now;
                record.IsStatus = entity.IsStatus;
                db.SaveChanges();
                updated = true;
            }

            return updated;
        }


        public MFG_PRODUCTION_PLANEntity Getbyid(long id)
        {
            MFG_PRODUCTION_PLANEntity entity = new MFG_PRODUCTION_PLANEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.MFG_PRODUCTION_PLAN.Find(id);
                if (record != null)
                {
                    entity.ID = record.ID;
                    entity.PlanNo = record.PlanNo;
                    entity.POID = record.POID;
                    entity.WorkOrderID = record.WorkOrderID;
                    entity.CustomerID = record.CustomerID;
                    MFG_WORK_ORDEREntity woentity = new MFG_WORK_ORDEREntity();
                    if (record.MFG_WORK_ORDER != null)
                    {
                        woentity.WorkOrderID = record.MFG_WORK_ORDER.WorkOrderID;
                        woentity.WorkOrderNo = record.MFG_WORK_ORDER.WorkOrderNo;
                        woentity.Date = record.MFG_WORK_ORDER.Date;
                        entity.MFG_WORK_ORDER = woentity;
                    }
                    entity.MFG_WORK_ORDER.Date = record.MFG_WORK_ORDER.Date;
                    entity.DepartmentID = record.DepartmentID;
                    M_DepartmentMasterEntity deptentity = new M_DepartmentMasterEntity();
                    if (record.M_DepartmentMaster != null)
                    {
                        deptentity.Id = record.M_DepartmentMaster.Id;
                        deptentity.Departmentname = record.M_DepartmentMaster.Departmentname;
                        entity.M_DepartmentMaster = deptentity;
                    }
                    entity.StyleID = record.StyleID;
                    M_StyleMasterEntity mstyleentity = new M_StyleMasterEntity();
                    if (record.M_StyleMaster != null)
                    {
                        mstyleentity.Id = record.M_StyleMaster.Id;
                        mstyleentity.Stylename = record.M_StyleMaster.Stylename;
                        entity.M_StyleMaster = mstyleentity;
                    }
                    entity.PlanDate = record.PlanDate;
                    
                    entity.PlanDateString = Convert.ToDateTime(record.PlanDate).ToString(Helper.DATE_FORMAT_FOR_EDIT);
                    entity.StartDateString = Convert.ToDateTime(record.StartDate).ToString(Helper.DATE_FORMAT_FOR_EDIT);
                    entity.TargetDateString = Convert.ToDateTime(record.TargetDate).ToString(Helper.DATE_FORMAT_FOR_EDIT);

                    entity.SampleReferenceNo = record.SampleReferenceNo;
                    entity.StartDate = record.StartDate;
                    entity.ProcessCycleID = record.ProcessCycleID;
                    entity.Target_Qty = record.Target_Qty;
                    entity.UnitID = record.UnitID;
                    entity.TargetDate = record.TargetDate;
                    entity.WorkCenterID = record.WorkCenterID;
                    M_WorkCenterMasterEntity WOCent = new M_WorkCenterMasterEntity();
                    var WCnme = db.M_WorkCenterMaster.Where(x => x.Id == record.WorkCenterID).Select(x => x.WorkCenterName).FirstOrDefault();
                    WOCent.WorkCenterName = WCnme;
                    entity.WCentity = WOCent;

                    entity.ToolID = record.ToolID;
                    entity.OperatorID = record.OperatorID;
                    entity.Comment = record.Comment;
                    entity.Approved = record.ApprovedStatus;
                    entity.ApprovedDate = record.ApproveDate;
                    entity.IsStatus = record.IsStatus;
                    entity.Company_ID = record.Company_ID;
                    entity.BranchId = record.BranchId;

                    var list = db.MFG_PRODUCTION_PLAN_DETAILS.Where(x => x.PlanID == id).ToList();
                    List<MFG_PRODUCTION_PLAN_DETAILSEntity> detaillist = new List<MFG_PRODUCTION_PLAN_DETAILSEntity>();
                    foreach (var item in list)
                    {
                        MFG_PRODUCTION_PLAN_DETAILSEntity detailentity = new MFG_PRODUCTION_PLAN_DETAILSEntity();

                        detailentity.ID = item.ID;
                        detailentity.PlanID = item.PlanID;

                        detailentity.StyleID = item.StyleID;
                        M_StyleMasterEntity styleentity = new M_StyleMasterEntity();
                        if (item.M_StyleMaster != null)
                        {
                            styleentity.Id = item.M_StyleMaster.Id;
                            styleentity.Stylename = item.M_StyleMaster.Stylename;
                            detailentity.M_StyleMaster = styleentity;
                        }
                        detailentity.Attribute_ID = item.Attribute_ID;
                        detailentity.Attribute_Value_ID = item.Attribute_Value_ID;
                        //M_ItemSizeMasterEntity itemsize = new M_ItemSizeMasterEntity();
                        //if (item.M_ItemSizeMaster != null)
                        //{
                        //    itemsize.Id = item.M_ItemSizeMaster.Id;
                        //    itemsize.Sizetype = item.M_ItemSizeMaster.Sizetype;

                        //    detailentity.M_ItemSizeMaster = itemsize;
                        //}
                        detailentity.MachineID = item.MachineID;
                        MachineEntity machine = new MachineEntity();
                        if (item.MachineMaster != null)
                        {
                            machine.Id = item.MachineMaster.Id;
                            machine.Machinename = item.MachineMaster.Machinename;
                            detailentity.MachineMaster = machine;
                        }
                        //detailentity.TargetQuantity = item.TargetQuantity;
                        //detailentity.ActualQuantity = item.ActualQuantity;
                        detailentity.UnitID = item.UnitID;
                        M_UnitMasterEntity unit = new M_UnitMasterEntity();
                        if (item.M_UnitMaster != null)
                        {
                            unit.Id = item.M_UnitMaster.Id;
                            unit.ItemUnit = item.M_UnitMaster.ItemUnit;
                            detailentity.M_UnitMaster = unit;
                        }

                        detaillist.Add(detailentity);
                    }
                    entity.detaillist = detaillist;
                }
            }
            return entity;
        }

        public MFG_WorkOrderDetailsForProductionPlan GetWorkOrderDetails(long workOrderID, long planID)
        {
            MFG_WorkOrderDetailsForProductionPlan entity = new MFG_WorkOrderDetailsForProductionPlan();
            cl_CustomerPO poobj = new cl_CustomerPO();

            using (var db = new GarmentERPDBEntities())
            {
                var item = db.MFG_WORK_ORDER.Find(workOrderID);
                if (item != null)
                {
                    entity.WorkOrderEntity.WorkOrderID = item.WorkOrderID;
                    entity.WorkOrderEntity.CustomerID = item.CustomerID;
                    entity.WorkOrderEntity.POID = item.POID;
                    entity.WorkOrderEntity.StyleID = item.StyleID;
                    entity.WorkOrderEntity.WorkOrderNo = item.WorkOrderNo;
                    entity.WorkOrderEntity.StyleType = item.StyleType;
                    entity.WorkOrderEntity.DepartmentID = item.DepartmentID;
                    entity.WorkOrderEntity.RevisionNo = item.RevisionNo;
                    entity.WorkOrderEntity.RequiredQty = item.RequiredQty;
                    entity.WorkOrderEntity.UnitID = item.UnitID;
                    entity.WorkOrderEntity.Date = item.Date;
                    entity.WorkOrderEntity.Approved = item.Approved;
                    entity.WorkOrderEntity.ApproveDate = item.ApproveDate;
                    entity.WorkOrderEntity.Comment = item.Comment;
                    entity.WorkOrderEntity.RequiredQty = item.RequiredQty;

                    var customerPORecord = poobj.getbyid(item.POID);
                    entity.WorkOrderEntity.customerpo = customerPORecord;

                    var poStyleDetails = db.CRM_CustomerPOStyleDetail.Where(x => x.Poid == item.POID).FirstOrDefault();
                    var detailrecordlist = db.MFG_WORK_ORDER_DETAIL.Where(x => x.WorkOrderID == item.WorkOrderID).ToList();

                    List<MFG_ProductionPlanProcessDetails> Processes = new List<MFG_ProductionPlanProcessDetails>();
                    var enquiryDetails = db.CRM_CustomerPOMaster.Include("CRM_QuotationMaster.CRM_EnquiryMaster").Where(x => x.Id == item.POID).FirstOrDefault();
                    if (enquiryDetails != null && enquiryDetails.CRM_QuotationMaster != null)
                    {
                        long enquiryID = enquiryDetails.CRM_QuotationMaster.EnquiryID;
                        var bom = db.M_BillOfMaterialMaster.Where(bm => bm.EnquiryId == enquiryID).FirstOrDefault();
                        if(bom==null)
                        {
                            bom = db.M_BillOfMaterialMaster.Where(bm => bm.Poid == item.POID).FirstOrDefault();
                        }

                        if (bom != null)
                        {
                            int? processCycleID = bom.Prooce_Cycle_id;                           
                            List<ProcessCycleTransaction> processCylceTrnsctions = db.ProcessCycleTransactions.Where(pcyle => pcyle.Processcycleid == processCycleID).ToList<ProcessCycleTransaction>();
                            foreach (var proc in processCylceTrnsctions)
                            {
                                MFG_ProductionPlanProcessDetails _proc = new MFG_ProductionPlanProcessDetails();
                                _proc.ProcessID = proc.Processid;
                                _proc.Processsequence = proc.Processsequence;
                                _proc.SHORT_NAME = proc.M_PROCESS_Master.SHORT_NAME;
                                var opid = db.M_PROCESS_Master.Where(x => x.Process_Id == proc.Processid).Select(x => x.Process_Incharge_ID).SingleOrDefault();
                                _proc.Operator = db.Employees.Where(x=>x.UserId==opid).Select(x=>x.Name).SingleOrDefault();
                                _proc.Wastage = (proc.M_PROCESS_Master.Process_Wastage == null) ? 0 : proc.M_PROCESS_Master.Process_Wastage;
                                _proc.Machine = proc.M_PROCESS_Master.M_MachineTypeMaster.Machine_TypeName;
                                var catid = db.M_ProcessInCategoryDetails.Where(x => x.ProCycTran_ID == proc.Id).Select(x => x.ItemcategoryIn).FirstOrDefault();
                                _proc.ProcessProductIN = db.M_ItemCategoryMaster.Where(x => x.Id == catid).Select(x => x.Itemcategory).FirstOrDefault();
                                _proc.ProcessProduct = proc.M_PROCESS_Master.Process_Produce_Output;
                                Processes.Add(_proc);
                            }
                        }
                    }
                    entity.ProcessList = Processes;

                    ////List<MFG_ProductionPlanStyleDetails> styleDetails = new List<MFG_ProductionPlanStyleDetails>();
                    ////if (detailrecordlist != null && detailrecordlist.Count > 0)
                    ////{

                    ////    List<MFG_PRODUCTION_PLAN_DETAILS> prodPlanDetails = null;
                    ////    prodPlanDetails = db.MFG_PRODUCTION_PLAN_DETAILS.Where(pdtl => pdtl.PlanID == planID).ToList();

                    ////    foreach (var size in detailrecordlist)
                    ////    {
                    ////        MFG_ProductionPlanStyleDetails detail = new MFG_ProductionPlanStyleDetails();
                    ////        detail.WorkOrderID = size.ID;
                    ////        detail.WO_Qty = size.Quantity;
                    ////        detail.Target_Qty = size.ProductionQty;
                    ////        detail.StyleID = item.StyleID;
                    ////        detail.SizeID = size.SizeID;
                    ////        detail.SizeType = size.M_ItemSizeMaster.Sizetype;
                    ////        detail.UnitID = item.UnitID;
                    ////        if (poStyleDetails != null)
                    ////        {
                    ////            if (poStyleDetails.M_StyleMaster != null)
                    ////            {
                    ////                detail.StyleName = poStyleDetails.M_StyleMaster.Stylename;
                    ////                detail.StyleNo = poStyleDetails.M_StyleMaster.StyleNo;
                    ////            }

                    ////            if (poStyleDetails.M_SegmentTypeMaster != null)
                    ////                detail.SegmentName = poStyleDetails.M_SegmentTypeMaster.Segmenttype;

                    ////            if (poStyleDetails.M_ItemMaster != null)
                    ////                detail.ItemName = poStyleDetails.M_ItemMaster.ItemName;

                    ////        }

                    ////        if (prodPlanDetails != null && prodPlanDetails.Count > 0)
                    ////        {
                    ////            var prodPlanDetail = prodPlanDetails.Where(pp => pp.StyleID == item.StyleID && pp.SizeID == size.SizeID).FirstOrDefault();

                    ////            if (prodPlanDetail != null)
                    ////                detail.Actual_Qty = (prodPlanDetail.Actual_Qty != null) ? prodPlanDetail.Actual_Qty : 0;

                    ////        }

                    ////        styleDetails.Add(detail);
                    ////    }
                    ////}

                    ////entity.StyleList = styleDetails;
                }

            }

            return entity;
        }

        public MfgDailyProductionPlanEntity GetMonthlyPlanDetails(long monthlyPlanID)
        {
            MfgDailyProductionPlanEntity dailyproductionPlanObject = new MfgDailyProductionPlanEntity();


            using (var db = new GarmentERPDBEntities())
            {
                var monthlyProductionPlan = db.MFG_PRODUCTION_PLAN.Where(pln => pln.ID == monthlyPlanID).FirstOrDefault();

                var dailyProductionPlanDetails = db.MFG_PRODUCTION_DAILY_PLAN.GroupBy(pln => pln.MonthlyPlanID).Select(PTotal => new { MonthlyPlanID = PTotal.Key,TotalQuantityFinished = PTotal.Sum(c=>c.Daily_Target_Qty ) });


                if (monthlyProductionPlan != null)
                {
                    dailyproductionPlanObject.MonthlyPlanID = monthlyProductionPlan.ID;
                    dailyproductionPlanObject.MonthlyPlanQuantity = monthlyProductionPlan.Target_Qty;
                    dailyproductionPlanObject.MonthlyFromDate = Convert.ToDateTime(monthlyProductionPlan.StartDate).ToString(Helper.DATE_FORMAT_FOR_DISPLAY);
                    dailyproductionPlanObject.MonthlyToDate = Convert.ToDateTime(monthlyProductionPlan.TargetDate).ToString(Helper.DATE_FORMAT_FOR_DISPLAY);  ;

                    dailyproductionPlanObject.MonthlyFromDateToValidate = Convert.ToDateTime(monthlyProductionPlan.StartDate).ToString("MM/dd/yyyy");
                    dailyproductionPlanObject.MonthlyToDateToValidate = Convert.ToDateTime(monthlyProductionPlan.TargetDate).ToString("MM/dd/yyyy"); ;

                    long Woid = Convert.ToInt64(monthlyProductionPlan.WorkOrderID);
                    var WoData = db.MFG_WORK_ORDER_DETAIL.Where(x => x.WorkOrderID == Woid).FirstOrDefault();
                    if(WoData!=null)
                    {
                        int IAid = Convert.ToInt32(WoData.SizeID);
                        int ISid = 0;
                        long itemid = 0;
                        if (WoData.ItemId != null)
                        {
                            dailyproductionPlanObject.Itemid = WoData.ItemId;
                            itemid = Convert.ToInt64(WoData.ItemId);
                        }
                        else
                        {
                            var poid = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == Woid).Select(x => x.POID).FirstOrDefault();
                            var postyle = db.CRM_CustomerPOStyleDetail.Where(x => x.Poid == poid).FirstOrDefault();
                            itemid = Convert.ToInt64(postyle.Itemid);
                        }
                        dailyproductionPlanObject.ItemName = grnobj.getItemName(itemid, IAid, ISid);
                        dailyproductionPlanObject.Itemid = WoData.ItemId;
                    }

                    
                    dailyproductionPlanObject.StyleID = monthlyProductionPlan.StyleID;
                    dailyproductionPlanObject.DailyPlanned_Qty = monthlyProductionPlan.PerDayProduction;
                    if (monthlyProductionPlan.M_StyleMaster != null)
                    {
                        dailyproductionPlanObject.StyleNo = (monthlyProductionPlan.M_StyleMaster.StyleNo != null)?monthlyProductionPlan.M_StyleMaster.StyleNo:string.Empty;
                        dailyproductionPlanObject.StyleName = (monthlyProductionPlan.M_StyleMaster.Stylename != null)? monthlyProductionPlan.M_StyleMaster.Stylename:string.Empty;
                        dailyproductionPlanObject.StyleDescription = (monthlyProductionPlan.M_StyleMaster.Styledescription!= null)? monthlyProductionPlan.M_StyleMaster.Styledescription:string.Empty;
                    }
                    else
                    {
                        dailyproductionPlanObject.StyleNo =string.Empty;
                        dailyproductionPlanObject.StyleName = string.Empty;
                       dailyproductionPlanObject.StyleDescription = string.Empty;
                    }

                    dailyproductionPlanObject.MachineID = 0;
                    dailyproductionPlanObject.UnitID = monthlyProductionPlan.UnitID;

                    if (dailyProductionPlanDetails != null)
                    {
                        var monthlyPlannedQty = dailyProductionPlanDetails.Where(c => c.MonthlyPlanID == monthlyPlanID).FirstOrDefault();
                        if (monthlyPlannedQty != null)
                        {
                            dailyproductionPlanObject.MonthlyFinishedQuantity = monthlyPlannedQty.TotalQuantityFinished;
                            dailyproductionPlanObject.MonthlyPendingQuantity = dailyproductionPlanObject.MonthlyPlanQuantity - monthlyPlannedQty.TotalQuantityFinished;
                        }
                        else
                        {
                            dailyproductionPlanObject.MonthlyFinishedQuantity = 0;
                            dailyproductionPlanObject.MonthlyPendingQuantity = dailyproductionPlanObject.MonthlyPlanQuantity;
                        }
                    }

                    List<DailyProductionStyleSize> StyleSizeDetails = new List<DailyProductionStyleSize>();

                    var monthlyProductionPlanDetails = db.MFG_PRODUCTION_PLAN_DETAILS.Where(dtls => dtls.PlanID == monthlyPlanID).ToList();
                    if (monthlyProductionPlanDetails != null && monthlyProductionPlanDetails.Count > 0)
                    {
                        foreach (var styleSize in monthlyProductionPlanDetails)
                        {
                            DailyProductionStyleSize StyleSizeDtlObject = new DailyProductionStyleSize();

                            if (styleSize.SizeID != null)
                            StyleSizeDtlObject.SizeID = Convert.ToInt64(styleSize.SizeID);

                            //StyleSizeDtlObject.SizeType = styleSize.M_ItemSizeMaster.Sizetype;
                            StyleSizeDtlObject.PlannedQuantity = styleSize.Actual_Qty;
                            StyleSizeDtlObject.ProducedQuantity = 0;
                            StyleSizeDetails.Add(StyleSizeDtlObject);
                        }
                    }
                    dailyproductionPlanObject.StyleSizeDetails = StyleSizeDetails;
                }
            }


            return dailyproductionPlanObject;
        }

        /*
        public bool UpdateProductionPlanDetail(MFG_PRODUCTION_PLAN_DETAILSEntity plandetailentity)
        {
            bool isupdated = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.MFG_PRODUCTION_PLAN_DETAILS.Find(plandetailentity.ID);
                record.MachineID = plandetailentity.MachineID;
                record.SizeID = plandetailentity.SizeID;
                record.StyleID = plandetailentity.StyleID;
                //record.TargetQuantity = plandetailentity.TargetQuantity;
                record.UnitID = plandetailentity.UnitID;
                //record.ActualQuantity = plandetailentity.ActualQuantity;

                db.SaveChanges();
                isupdated = true;

            }

            return isupdated;
        }

        public bool DeleteProductionPlanDetail(long id)
        {
            bool deleted = false;
            using (var db = new GarmentERPDBEntities())
            {

            }
            return deleted;
        }

        public bool Delete(long id)
        {
            bool deleted = false;

            using (var db = new GarmentERPDBEntities())
            {

            }

            return deleted;
        }

        */
    }
}
