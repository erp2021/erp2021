﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;
using Garments_ERP.Entity.ProductionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Classes.ProductionClasses
{
    public class cl_FinishedGoodCreate
    {
        public int Insert(M_ItemMasterEntity entity)
        {

            int insertid = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    M_ItemMaster record = new M_ItemMaster();
                    // record.Finished_Goods_ID = entity.Finished_Goods_ID;
                    record.Packingtypeid = entity.Packingtypeid;
                    record.ItemName = entity.ItemName;
                    record.HScode = entity.HScode;
                    record.Taxrate = entity.Taxrate;
                    record.ItemCode = entity.ItemCode;
                    //record.Itemsubcategoryid = entity.Itemsubcategoryid;
                    record.Createddate = entity.Createddate;
                    record.CreatedBy = entity.CreatedBy;
                    record.Company_ID = entity.Company_ID;
                    record.BranchId = entity.Branch_ID;
                    record.Is_Set = true;
                    record.IsActive = true;
                    db.M_ItemMaster.Add(record);
                    db.SaveChanges();
                    insertid = Convert.ToInt32(record.Id);
                    if (insertid > 0)
                    {

                        // ProcessCycleTransactionEntity tranentity = new ProcessCycleTransactionEntity();
                        foreach (var item in entity.finishitemlist)
                        {
                            //item.IsActive = true;
                            M_Finished_Goods_Details tranrecord = new M_Finished_Goods_Details();

                            tranrecord.Finished_Item_ID = insertid;
                            //  tranrecord.ItemcategoryIn = item.ItemcategoryIn;
                            tranrecord.Item_ID = item.Item_ID;
                            tranrecord.Item_Attr_Id = item.Item_Attr_Id;
                            tranrecord.itmUOMid = item.itmUOMid;
                            tranrecord.itmUOM_val = item.itmUOM_val;
                            tranrecord.Item_inward_id = item.Item_inward_id;
                            tranrecord.Item_sub_catg_id = item.Item_sub_catg_id;
                            tranrecord.Batch_qty = item.Batch_qty;
                            tranrecord.IsuueQty = item.IsuueQty;
                            //tranrecord.Set_Qty = item.Item_ID;
                            //New Code Added by Rahul on 09-01-2020
                            tranrecord.Createdby = item.Createdby;
                            tranrecord.Createddate = item.Createddate;
                            tranrecord.Company_ID = item.companyid;
                            tranrecord.BranchId = item.branchid;

                            tranrecord.IsActive = true;



                            db.M_Finished_Goods_Details.Add(tranrecord);
                            db.SaveChanges();
                            var tid = tranrecord.Finished_Goods_Product_ID;

                        }

                    }
                    if (insertid > 0)
                    {

                        // ProcessCycleTransactionEntity tranentity = new ProcessCycleTransactionEntity();
                        foreach (var item in entity.inwarditemlist)
                        {
                            //item.IsActive = true;
                            M_ItemInwardMaster inwardrecord = new M_ItemInwardMaster();
                            inwardrecord.Item_ID = Convert.ToInt32(item.Item_ID);
                            inwardrecord.Item_Attribute_ID = item.Item_Attribute_ID;
                            inwardrecord.UOM = item.UOM;
                            inwardrecord.ItemInward_ID = Convert.ToInt32(item.ItemInward_ID);
                            inwardrecord.ItemQty = item.ItemQty;
                            inwardrecord.BalanceQty = item.BalanceQty;
                            inwardrecord.InventoryModeId = 7;
                            //tranrecord.Set_Qty = item.Item_ID;
                            //New Code Added by Rahul on 09-01-2020
                            inwardrecord.CreatedBy = item.CreatedBy;
                            inwardrecord.CreatedDate = item.CreatedDate;
                            inwardrecord.Company_ID = item.Company_ID;
                            inwardrecord.BranchId = item.BranchId;
                            inwardrecord.IsActive = true;


                            db.M_ItemInwardMaster.Add(inwardrecord);
                            db.SaveChanges();
                            var tid1 = inwardrecord.ID;

                        }

                    }
                    if (insertid > 0)
                    {

                        // ProcessCycleTransactionEntity tranentity = new ProcessCycleTransactionEntity();
                        foreach (var item in entity.productinfolist)
                        {
                            //item.IsActive = true;
                            M_Product_Info inwardrecord = new M_Product_Info();
                            inwardrecord.ItemInward_ID = Convert.ToInt32(item.ItemInward_ID);
                            inwardrecord.Dynamic_Controls_Id = item.Dynamic_Controls_Id;
                            inwardrecord.Dynamic_Controls_value = item.Dynamic_Controls_value;
                            inwardrecord.ItemInward_ID = Convert.ToInt32(item.ItemInward_ID);
                            inwardrecord.Created_Date = item.Created_Date;
                            inwardrecord.Created_By = item.Created_By;
                            inwardrecord.Company_ID = item.Company_ID;
                            inwardrecord.BranchId = item.BranchId;

                            db.M_Product_Info.Add(inwardrecord);
                            db.SaveChanges();
                            var tid2 = inwardrecord.Product_Info_Id;

                        }

                    }
                    if (insertid > 0)
                    {

                        // ProcessCycleTransactionEntity tranentity = new ProcessCycleTransactionEntity();
                        foreach (var item in entity.outwardlist)
                        {
                            //item.IsActive = true;
                            M_ItemOutwardMaster outwardrecord = new M_ItemOutwardMaster();
                            outwardrecord.Item_ID = Convert.ToInt32(item.Item_ID);
                            outwardrecord.Item_Attribute_ID = item.Item_Attribute_ID;
                            outwardrecord.ItemSubCategoryID = item.ItemSubCategoryID;
                            outwardrecord.Inward_ID = Convert.ToInt32(item.Inward_ID);
                            outwardrecord.UpdatedBy = item.UpdatedBy;
                            outwardrecord.UpdatedDate = item.UpdatedDate;
                            outwardrecord.Company_ID = item.Company_ID;
                            outwardrecord.BranchId = item.BranchId;
                            outwardrecord.MI_ID = insertid;
                            db.M_ItemOutwardMaster.Add(outwardrecord);
                            db.SaveChanges();
                            var tid2 = outwardrecord.ID;

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return insertid;
        }

        public List<M_ItemMasterEntity> get()
        {
            List<M_ItemMasterEntity> list = new List<M_ItemMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ItemMaster.ToList().Where(x => x.IsActive == true && x.Is_Set == true);
                foreach (var item in records)
                {
                    M_ItemMasterEntity entity = new M_ItemMasterEntity();
                    entity.Id = item.Id;
                    entity.ItemCode = item.ItemCode;
                    entity.ItemName = item.ItemName;
                    entity.HSchapter = item.HSchapter;
                    entity.HScode = item.HScode;
                    entity.Specification = item.Specification;
                    entity.Unit = item.Unit;
                    if (item.M_UnitMaster != null)
                    {
                        M_UnitMasterEntity unitentity = new M_UnitMasterEntity();
                        unitentity.Id = item.M_UnitMaster.Id;
                        unitentity.ItemUnit = item.M_UnitMaster.ItemUnit;
                        entity.M_UnitMaster = unitentity;
                    }
                    entity.CurrencyId = item.CurrencyId;
                    entity.ItemRate = item.ItemRate;
                    entity.Taxrate = item.Taxrate;
                    entity.OpeningStock = item.OpeningStock;
                    entity.Length = item.Length;
                    entity.Width = item.Width;
                    entity.Height = item.Height;
                    entity.ItemCategoryid = item.ItemCategoryid;
                    if (item.M_ItemCategoryMaster != null)
                    {
                        M_ItemCategoryMasterEntity categoryentity = new M_ItemCategoryMasterEntity();
                        categoryentity.Id = item.M_ItemCategoryMaster.Id;
                        categoryentity.Itemcategory = item.M_ItemCategoryMaster.Itemcategory;
                        entity.M_ItemCategoryMaster = categoryentity;
                    }
                    entity.Itemcolorid = item.Itemcolorid;
                    if (item.M_ItemColorMaster != null)
                    {
                        M_ItemColorEntity colorentity = new M_ItemColorEntity();
                        colorentity.Id = item.M_ItemColorMaster.Id;
                        colorentity.ItemColor = item.M_ItemColorMaster.ItemColor;
                        colorentity.ItemColordesc = item.M_ItemColorMaster.ItemColordesc;
                        entity.M_ItemColorMaster = colorentity;
                    }
                    entity.Companyname = item.Companyname;
                    entity.Itemsubcategoryid = item.Itemsubcategoryid;
                    if (item.M_ItemSubCategoryMaster != null)
                    {
                        M_ItemSubCategoryMasterEntity subcateentity = new M_ItemSubCategoryMasterEntity();
                        subcateentity.Id = item.M_ItemSubCategoryMaster.Id;
                        subcateentity.Itemsubcategory = item.M_ItemSubCategoryMaster.Itemsubcategory;
                        subcateentity.itemcategoryid = item.M_ItemSubCategoryMaster.itemcategoryid;
                        entity.M_ItemSubCategoryMaster = subcateentity;
                    }
                    entity.Brand = item.Brand;
                    entity.Shortname = item.Shortname;
                    entity.Batchperserial = item.Batchperserial;
                    entity.Warehouseid = item.Warehouseid;
                    entity.Rackid = item.Rackid;
                    if (item.M_RackMaster != null)
                    {
                        M_RackMasterEntity rackentity = new M_RackMasterEntity();
                        rackentity.Id = item.M_RackMaster.Id;
                        rackentity.Rack = item.M_RackMaster.Rack;
                        rackentity.Rackdesc = item.M_RackMaster.Rackdesc;
                        entity.M_RackMaster = rackentity;
                    }
                    entity.Shapeid = item.Shapeid;
                    entity.EOQ = item.EOQ;
                    entity.QCrequired = item.QCrequired;
                    entity.Packingtypeid = item.Packingtypeid;
                    entity.ItemImage = item.ItemImage;
                    entity.Description = item.Description;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Createddate = item.Createddate;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    list.Add(entity);
                }
            }
            return list;
        }

        public M_ItemMasterEntity GetById(long id)
        {
            M_ItemMasterEntity entity = new M_ItemMasterEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_ItemMaster.Where(x => x.Id == id && x.IsActive == true);
                foreach (var item in records)
                {
                    entity.Id = item.Id;
                    entity.ItemCode = item.ItemCode;
                    entity.ItemName = item.ItemName;
                    entity.HScode = item.HScode;
                    entity.HSchapter = item.HSchapter;
                    entity.Specification = item.Specification;
                    entity.Unit = item.Unit;
                    if (item.M_UnitMaster != null)
                    {
                        M_UnitMasterEntity unitentity = new M_UnitMasterEntity();
                        unitentity.Id = item.M_UnitMaster.Id;
                        unitentity.ItemUnit = item.M_UnitMaster.ItemUnit;
                        entity.M_UnitMaster = unitentity;
                    }
                    entity.CurrencyId = item.CurrencyId;
                    entity.ItemRate = item.ItemRate;
                    entity.Taxrate = item.Taxrate;
                    entity.OpeningStock = item.OpeningStock;
                    entity.Length = item.Length;
                    entity.Width = item.Width;
                    entity.Height = item.Height;
                    entity.Itemgroupid = item.Itemgroupid;
                    entity.ItemStyleid = item.ItemStyleid;
                    M_StyleMasterEntity style = new M_StyleMasterEntity();
                    var stylename = db.M_StyleMaster.Where(x => x.Id == item.ItemStyleid).Select(x => x.Stylename).FirstOrDefault();
                    style.Stylename = stylename;
                    entity.StyleName = style;
                    entity.ItemStyleNo = item.ItemStyleNo;
                    entity.ItemStyledescription = item.ItemStyledescription;
                    entity.ItemCategoryid = item.ItemCategoryid;

                    if (item.M_ItemCategoryMaster != null)
                    {
                        M_ItemCategoryMasterEntity categoryentity = new M_ItemCategoryMasterEntity();
                        categoryentity.Id = item.M_ItemCategoryMaster.Id;
                        categoryentity.Itemcategory = item.M_ItemCategoryMaster.Itemcategory;
                        entity.M_ItemCategoryMaster = categoryentity;
                    }
                    entity.Itemcolorid = item.Itemcolorid;
                    if (item.M_ItemColorMaster != null)
                    {
                        M_ItemColorEntity colorentity = new M_ItemColorEntity();
                        colorentity.Id = item.M_ItemColorMaster.Id;
                        colorentity.ItemColor = item.M_ItemColorMaster.ItemColor;
                        colorentity.ItemColordesc = item.M_ItemColorMaster.ItemColordesc;
                        entity.M_ItemColorMaster = colorentity;
                    }
                    entity.Companyname = item.Companyname;
                    entity.Itemsubcategoryid = item.Itemsubcategoryid;
                    if (item.M_ItemSubCategoryMaster != null)
                    {
                        M_ItemSubCategoryMasterEntity subcateentity = new M_ItemSubCategoryMasterEntity();
                        subcateentity.Id = item.M_ItemSubCategoryMaster.Id;
                        subcateentity.Itemsubcategory = item.M_ItemSubCategoryMaster.Itemsubcategory;
                        subcateentity.itemcategoryid = item.M_ItemSubCategoryMaster.itemcategoryid;
                        entity.M_ItemSubCategoryMaster = subcateentity;
                    }
                    entity.Brand = item.Brand;
                    entity.Shortname = item.Shortname;
                    entity.Batchperserial = item.Batchperserial;
                    entity.Warehouseid = item.Warehouseid;

                    entity.Rackid = item.Rackid;
                    if (item.M_RackMaster != null)
                    {
                        M_RackMasterEntity rackentity = new M_RackMasterEntity();
                        rackentity.Id = item.M_RackMaster.Id;
                        rackentity.Rack = item.M_RackMaster.Rack;
                        rackentity.Rackdesc = item.M_RackMaster.Rackdesc;
                        entity.M_RackMaster = rackentity;
                    }
                    entity.Shapeid = item.Shapeid;
                    entity.ShadeId = item.ShadeId;
                    entity.EOQ = item.EOQ;
                    entity.QCrequired = item.QCrequired;
                    entity.Packingtypeid = item.Packingtypeid;
                    entity.ItemImage = item.ItemImage;
                    entity.Description = item.Description;
                    entity.CreatedBy = item.CreatedBy;
                    entity.Createddate = item.Createddate;
                    entity.Updateddate = item.Updateddate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.IsActive = item.IsActive;
                    List<M_FinishedGoodsEntity> imgentitylist = new List<M_FinishedGoodsEntity>();
                    var itemimages = db.M_Finished_Goods_Details.Where(x => x.Finished_Item_ID == id).ToList();

                    if (itemimages != null)
                    {
                        string Itemname = "";
                        foreach (var img in itemimages)
                        {
                            var itemname = db.M_ItemMaster.Where(x => x.Id == img.Item_ID).ToList();
                            foreach (var name in itemname)
                            {
                                Itemname = name.ItemName;
                            }
                            //item.IsActive = true;
                            M_FinishedGoodsEntity tranrecord = new M_FinishedGoodsEntity();
                            tranrecord.ItemName = Itemname;
                            tranrecord.Finished_Item_ID = img.Finished_Item_ID;
                            //  tranrecord.ItemcategoryIn = item.ItemcategoryIn;
                            tranrecord.Item_ID = img.Item_ID;
                            tranrecord.Item_Attr_Id = Convert.ToInt32(img.Item_Attr_Id);
                            tranrecord.itmUOMid = Convert.ToInt32(img.itmUOMid);
                            tranrecord.itmUOM_val = img.itmUOM_val;
                            tranrecord.Item_inward_id = Convert.ToInt32(img.Item_inward_id);
                            tranrecord.Item_sub_catg_id = Convert.ToInt32(img.Item_sub_catg_id);
                            tranrecord.Batch_qty = Convert.ToInt32(img.Batch_qty);
                            tranrecord.IsuueQty = Convert.ToInt32(img.IsuueQty);
                            imgentitylist.Add(tranrecord);
                        }
                        entity.M_FG = imgentitylist;
                    }

                }
            }
            return entity;
        }
    }
}
