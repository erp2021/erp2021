﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Classes.ProductionClasses
{
    public class cl_RawMaterialIssue
    {
        public string getnextmino()
        {
            string no;
            no = "MI-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.MFG_MaterialIssueMaster.FirstOrDefault() != null ? db.MFG_MaterialIssueMaster.Max(x => x.Id) + 1 : 1;
                no = no + "-" + record.ToString();
            }

            return no;
        }
        public long Insert1(MFG_MaterialIssueMasterEntity entity)
        {

            long id = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    MFG_MaterialIssueMaster record = new MFG_MaterialIssueMaster();

                    record.Id = entity.Id;
                    record.WarehouseID = entity.WarehouseID;
                    record.MaterialIssueNo = Convert.ToString(getnextmino());
                    record.EmpID = entity.EmpID;
                    record.StyleID = entity.StyleID;
                    record.IssueDate = entity.IssueDate;                 
                    record.RefWO = entity.RefWO;
                    record.RefWODate = entity.RefWODate;
                   // record.Comment = entity.Comment;
                    record.DeptId = entity.DeptId;
                    record.CreatedBy = entity.CreatedBy;
                    record.CreatedDate = DateTime.Now;
                    record.UpdatedDate = DateTime.Now;
                    //record.UpdatedBy = entity.UpdatedBy;
                    record.IsActive = entity.IsActive;
                    record.PPNo = db.MFG_PRODUCTION_PLAN.Where(x => x.WorkCenterID == entity.RefWO && x.ApprovedStatus == 1).Select(x => x.ID).FirstOrDefault();
                    record.PPDate = db.MFG_PRODUCTION_PLAN.Where(x => x.WorkCenterID == entity.RefWO && x.ApprovedStatus == 1).Select(x => x.PlanDate).FirstOrDefault();
                    db.MFG_MaterialIssueMaster.Add(record);
                    db.SaveChanges();
                    id = record.Id;
                    //if (id > 0)
                    //{
                    //    foreach (var item in entity.issueitemlist)
                    //    {
                    //        MFG_MaterialIssueItemDetails itemrecord = new MFG_MaterialIssueItemDetails();
                    //        itemrecord.Id = item.Id;
                    //        itemrecord.MaterialIssueid = id;
                    //        itemrecord.Process_ID = item.Process_ID;
                    //        itemrecord.ItemSubCategoryID = item.ItemSubCategoryID;
                    //        itemrecord.ItemID = item.ItemID;
                    //        itemrecord.UOM_ID = item.UOM_ID;
                    //        itemrecord.ReqQty = item.ReqQty;
                    //        itemrecord.AvailableQty = item.AvailableQty;
                    //        itemrecord.IssueQty = item.IssueQty;
                    //        itemrecord.BalanceQty = item.BalanceQty;
                    //        itemrecord.IsActive = true;
                    //        db.MFG_MaterialIssueItemDetails.Add(itemrecord);
                    //        db.SaveChanges();
                    //    }
                        //    foreach (var outitem in entity.issueitemlist)
                        //    {
                        //        M_ItemOutwardMaster outitemrecord= new M_ItemOutwardMaster();
                        //        outitemrecord.ID = outitem.Id;
                        //        outitemrecord.MI_ID = id;
                        //        outitemrecord.Process_ID = outitem.Process_ID;
                        //        outitemrecord.ItemSubCategoryID = outitem.ItemSubCategoryID;
                        //        outitemrecord.Item_ID = outitem.ItemID;
                        //        outitemrecord.UOM = outitem.UOM_ID;
                        //        outitemrecord.ItemQty = outitem.IssueQty;
                        //        outitemrecord.IsActive = true;                          
                        //        outitemrecord.OutwardBy = outitem.loggedouser;
                        //        outitemrecord.OutwardDate = DateTime.Now;
                        //        outitemrecord.UpdatedBy = outitem.loggedouser;
                        //        outitemrecord.UpdatedDate = DateTime.Now;
                        //        db.M_ItemOutwardMaster.Add(outitemrecord);
                        //        db.SaveChanges();
                        //    }
                         //}                

                    }
            }
            catch (Exception ex)
            {
                throw;
            }
            return id;
        }

       
        public long Insert(M_ItemOutwardMaster entity)
        {
            long id = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    M_ItemOutwardMaster outitemrecord = new M_ItemOutwardMaster();
                    outitemrecord.ID = entity.ID;
                    outitemrecord.Inward_ID = entity.Inward_ID;
                    outitemrecord.MI_ID = entity.MI_ID;
                    outitemrecord.Process_ID = entity.Process_ID;
                    outitemrecord.ItemSubCategoryID =entity.ItemSubCategoryID;
                    outitemrecord.Item_ID = entity.Item_ID;
                    outitemrecord.Item_Attribute_ID = entity.Item_Attribute_ID;
                    outitemrecord.UOM = entity.UOM;
                    outitemrecord.ItemQty = entity.ItemQty;
                    outitemrecord.IsActive = true;
                    outitemrecord.OutwardBy = entity.OutwardBy;
                    outitemrecord.OutwardDate = DateTime.Now;
                    outitemrecord.Item_Outid = entity.Item_Outid;
                    outitemrecord.Company_ID = entity.Company_ID;
                    outitemrecord.BranchId = entity.BranchId;
                    outitemrecord.IsStatus = entity.IsStatus;
                    db.M_ItemOutwardMaster.Add(outitemrecord);
                    db.SaveChanges();
                    id = outitemrecord.ID;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return id;
        }
            
    }
}
