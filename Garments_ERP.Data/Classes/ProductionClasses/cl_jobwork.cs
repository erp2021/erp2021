﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_jobwork
    {
        public string getnextJWNo()
        {
            string no;
            no = "JW-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var rec = db.M_Jobwork.Count() > 0 ? db.M_Jobwork.Max(x => x.jobworkid) + 1 : 1;
                no = no + "-" + rec.ToString();
            }

            return no;
        }

        //Get Of Job Work With Id Wise
        public M_JobworkEntity GetById(long id)
        {
            M_JobworkEntity entity = new M_JobworkEntity();
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.M_Jobwork.Find(id);

                if(item != null)
                {   
                    entity.jobworkid = item.jobworkid;
                    entity.jobworkno = item.jobworkno;
                    entity.processInchargeId = item.processInchargeId;
                    if (item.processInchargeId > 0)
                    {
                        EmployeeEntity emp = new EmployeeEntity();
                        emp.Name = db.Employees.Where(x => x.UserId == item.processInchargeId).Select(x => x.Name).FirstOrDefault();
                        entity.EmployeeEntity = emp;
                    }
                    entity.woid = item.woid;
                    if (item.woid > 0)
                    {
                        MFG_WORK_ORDEREntity WO = new MFG_WORK_ORDEREntity();
                        WO.WorkOrderNo = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == item.woid).Select(x => x.WorkOrderNo).FirstOrDefault();
                        entity.WOEntity = WO;
                    }

                    entity.transferId = item.transferId;
                    if (item.transferId != "")
                    {
                        T_ITEM_TRANSFEREntity Tent = new T_ITEM_TRANSFEREntity();
                        int indx = Convert.ToString(item.transferId).IndexOf(',');
                        var TNo = "";
                        long tid = 0;
                        if (indx > 0)
                        {
                            string[] Tid = Convert.ToString(item.transferId).Split(',');

                            for (int i = 0; i < Tid.Length; i++)
                            {
                                tid = Convert.ToInt64(Tid[i]);
                                if (i == 0)
                                {
                                    TNo = db.T_ITEM_TRANSFER.Where(x => x.TRANSFER_ID == tid).Select(x => x.STOCK_TRANSFER_NO).FirstOrDefault();
                                }
                                else
                                {
                                    TNo = TNo + "," + db.T_ITEM_TRANSFER.Where(x => x.TRANSFER_ID == tid).Select(x => x.STOCK_TRANSFER_NO).FirstOrDefault();
                                }
                            }
                        }
                        else
                        {
                            tid = Convert.ToInt64(item.transferId);
                            TNo = db.T_ITEM_TRANSFER.Where(x => x.TRANSFER_ID == tid).Select(x => x.STOCK_TRANSFER_NO).FirstOrDefault();
                        }
                        Tent.STOCK_TRANSFER_NO = TNo;
                        entity.TransferEntity = Tent;
                    }

                    entity.JobStatus = item.JobStatus;
                    entity.startjobdate = item.startjobdate;
                    entity.startjobtime = item.startjobtime;

                    entity.endjobdate = item.endjobdate;
                    entity.endjobtime = item.endjobtime;

                    entity.MachineId = item.MachineId;
                    if (item.MachineId != null)
                    {
                        MachineEntity machEntity = new MachineEntity();
                        machEntity.Machinename = db.MachineMasters.Where(x => x.Id == item.MachineId).Select(x => x.Machinename).FirstOrDefault();
                        entity.machineEntity = machEntity;
                    }

                    entity.ProcessId = item.ProcessId;
                    if (item.ProcessId != null)
                    {
                        M_ProcessMasterEntity ProcEntity = new M_ProcessMasterEntity();
                        decimal Pid = Convert.ToDecimal(item.ProcessId);
                        ProcEntity.SHORT_NAME = db.M_PROCESS_Master.Where(x => x.Process_Id == Pid).Select(x => x.SHORT_NAME).FirstOrDefault();
                        entity.ProcessEntity = ProcEntity;
                    }

                    entity.OutItemName = item.OutItemName;
                    entity.outAttributeId = item.outAttributeId;
                    if (item.OutItemName != null)
                    {
                        M_ItemMasterEntity itemEntity = new M_ItemMasterEntity();
                        cl_GRNInward grnobj = new cl_GRNInward();
                        int IAid = Convert.ToInt32(item.outAttributeId);
                        int ISid = 0;
                        long OutItemid = Convert.ToInt64(item.OutItemName);
                        itemEntity.ItemName = grnobj.getItemName(OutItemid, IAid, ISid);
                        entity.ItemEntity = itemEntity;
                    }

                    entity.OutQty = item.OutQty;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.ModifyBy = item.ModifyBy;
                    entity.ModifyDate = item.ModifyDate;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsStatus = item.IsStatus;
                    entity.AcceptQty = item.AcceptQty;
                    entity.QC = item.QC;
                    List<M_Jobwork_MaterialEntity> JBMEnt = new List<M_Jobwork_MaterialEntity>();
                    var JBMRecord = db.M_Jobwork_Material.Where(x => x.jobworkid == item.jobworkid).ToList();
                    foreach(var da in JBMRecord)
                    {
                        M_Jobwork_MaterialEntity JBM = new M_Jobwork_MaterialEntity();
                        JBM.ItemId = da.ItemId;
                        if (da.ItemId != null)
                        {
                            M_ItemMasterEntity itemEntity = new M_ItemMasterEntity();
                            cl_GRNInward grnobj = new cl_GRNInward();
                            int IAid = Convert.ToInt32(da.ItemAttributeId);
                            int ISid = 0;
                            long OutItemid = Convert.ToInt64(da.ItemId);
                            itemEntity.ItemName = grnobj.getItemName(OutItemid, IAid, ISid);
                            JBM.ItemEntity = itemEntity;
                        }

                        JBM.unitid = da.unitid;
                        if (da.unitid != null)
                        {
                            M_UnitMasterEntity unitEntity = new M_UnitMasterEntity();
                            unitEntity.ItemUnit = db.M_UnitMaster.Where(x => x.Id == da.unitid).Select(x => x.ItemUnit).FirstOrDefault();
                            JBM.UnitEntity = unitEntity;
                        }

                        JBM.BatchId = da.BatchId;
                        if (da.BatchId != null)
                        {
                            M_Product_InfoEntity BTEntity = new M_Product_InfoEntity();
                            BTEntity.Dynamic_Controls_value = db.M_Product_Info.Where(x => x.ItemInward_ID == da.BatchId && x.Dynamic_Controls_Id==4).Select(x => x.Dynamic_Controls_value).FirstOrDefault();
                            JBM.BatchEntity = BTEntity;

                            M_Product_InfoEntity BTEntity1 = new M_Product_InfoEntity();
                            BTEntity1.Dynamic_Controls_value = db.M_Product_Info.Where(x => x.ItemInward_ID == da.BatchId && x.Dynamic_Controls_Id == 1).Select(x => x.Dynamic_Controls_value).FirstOrDefault();
                            JBM.BatchEntity1 = BTEntity1;
                        }
                        JBM.BatchQty = da.BatchQty;
                        JBM.ConsumedQty = da.ConsumedQty;
                        JBM.BalanceQty = da.BalanceQty;
                        JBM.BatchId = da.ItemAttributeId;
                        JBM.j_materialId = da.j_materialId;
                        JBM.jobworkid = da.jobworkid;
                        JBMEnt.Add(JBM);
                    }
                    entity.JBmaterial = JBMEnt;


                }
            }

            return entity;

        }

        //List Of Job Work With Status and User Wise
        public List<M_JobworkEntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<M_JobworkEntity> list = new List<M_JobworkEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                string Uid = Convert.ToString(Userid);
                var records = db.M_Jobwork.Where(x =>x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy== Uid).ToList();
               
                foreach (var item in records)
                {

                    M_JobworkEntity entity = new M_JobworkEntity();
                    entity.jobworkid = item.jobworkid;
                    entity.jobworkno = item.jobworkno;
                    entity.processInchargeId = item.processInchargeId;
                    if(item.processInchargeId>0)
                    {
                        EmployeeEntity emp = new EmployeeEntity();
                        emp.Name = db.Employees.Where(x => x.UserId == item.processInchargeId).Select(x => x.Name).FirstOrDefault();
                        entity.EmployeeEntity = emp;
                    }
                    entity.woid = item.woid;
                    if (item.woid > 0)
                    {
                        MFG_WORK_ORDEREntity WO = new MFG_WORK_ORDEREntity();
                        WO.WorkOrderNo = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == item.woid).Select(x => x.WorkOrderNo).FirstOrDefault();
                        entity.WOEntity = WO;
                    }

                    entity.transferId = item.transferId;
                    if(item.transferId!="")
                    {
                        T_ITEM_TRANSFEREntity Tent = new T_ITEM_TRANSFEREntity();
                        int indx = Convert.ToString(item.transferId).IndexOf(',');
                        var TNo = "";
                        long tid = 0;
                        if (indx>0)
                        {
                            string[] Tid = Convert.ToString(item.transferId).Split(',');
                            
                            for (int i=0;i< Tid.Length;i++)
                            {
                                tid = Convert.ToInt64(Tid[i]);
                                if (i == 0)
                                {
                                    TNo = db.T_ITEM_TRANSFER.Where(x => x.TRANSFER_ID == tid).Select(x => x.STOCK_TRANSFER_NO).FirstOrDefault();
                                }
                                else
                                {
                                    TNo = TNo+","+ db.T_ITEM_TRANSFER.Where(x => x.TRANSFER_ID == tid).Select(x => x.STOCK_TRANSFER_NO).FirstOrDefault();
                                }
                            }
                        }
                        else
                        {
                            tid = Convert.ToInt64(item.transferId);
                            TNo = db.T_ITEM_TRANSFER.Where(x => x.TRANSFER_ID == tid).Select(x => x.STOCK_TRANSFER_NO).FirstOrDefault();
                        }
                        Tent.STOCK_TRANSFER_NO = TNo;
                        entity.TransferEntity = Tent;
                    }
                    
                    entity.JobStatus = item.JobStatus;
                    entity.startjobdate = item.startjobdate;
                    entity.startjobtime = item.startjobtime;
                   
                    entity.endjobdate = item.endjobdate;
                    entity.endjobtime = item.endjobtime;

                    entity.MachineId = item.MachineId;
                    if (item.MachineId != null)
                    {
                        MachineEntity machEntity = new MachineEntity();
                        machEntity.Machinename = db.MachineMasters.Where(x => x.Id == item.MachineId).Select(x => x.Machinename).FirstOrDefault();
                        entity.machineEntity = machEntity;
                    }

                    entity.ProcessId = item.ProcessId;
                    if (item.ProcessId != null)
                    {
                        M_ProcessMasterEntity ProcEntity = new M_ProcessMasterEntity();
                        decimal Pid = Convert.ToDecimal(item.ProcessId);
                        ProcEntity.SHORT_NAME = db.M_PROCESS_Master.Where(x => x.Process_Id == Pid).Select(x => x.SHORT_NAME).FirstOrDefault();
                        entity.ProcessEntity = ProcEntity;
                    }

                    entity.OutItemName = item.OutItemName;
                    entity.outAttributeId = item.outAttributeId;
                    if (item.OutItemName != null)
                    {
                        M_ItemMasterEntity itemEntity = new M_ItemMasterEntity();
                        cl_GRNInward grnobj = new cl_GRNInward();
                        int IAid = Convert.ToInt32(item.outAttributeId);
                        int ISid = 0;
                        long OutItemid= Convert.ToInt64(item.OutItemName);
                        itemEntity.ItemName = grnobj.getItemName(OutItemid, IAid, ISid);
                        entity.ItemEntity = itemEntity;
                    }

                    entity.OutQty = item.OutQty;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.ModifyBy = item.ModifyBy;
                    entity.ModifyDate = item.ModifyDate;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsStatus = item.IsStatus;
                    entity.AcceptQty = item.AcceptQty;
                    entity.QC = item.QC;
                    list.Add(entity);
                }
            }

            return list;

        }


        //List Of Job Work With Status and User Wise Date Wise
        public List<M_JobworkEntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<M_JobworkEntity> list = new List<M_JobworkEntity>();

            using (var db = new GarmentERPDBEntities())
            {
                string Uid = Convert.ToString(Userid);
                var records = db.M_Jobwork.Where(x => x.startjobdate >= from && x.startjobdate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy == Uid).ToList();
                if (IsStatus == 3)
                {
                     records = db.M_Jobwork.Where(x => x.startjobdate >= from && x.startjobdate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy == Uid).ToList();
                }
                else
                {
                     records = db.M_Jobwork.Where(x => x.endjobdate >= from && x.endjobdate <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId && x.CreatedBy == Uid).ToList();
                }
                foreach (var item in records)
                {

                    M_JobworkEntity entity = new M_JobworkEntity();
                    entity.jobworkid = item.jobworkid;
                    entity.jobworkno = item.jobworkno;
                    entity.processInchargeId = item.processInchargeId;
                    if (item.processInchargeId > 0)
                    {
                        EmployeeEntity emp = new EmployeeEntity();
                        emp.Name = db.Employees.Where(x => x.UserId == item.processInchargeId).Select(x => x.Name).FirstOrDefault();
                        entity.EmployeeEntity = emp;
                    }
                    entity.woid = item.woid;
                    if (item.woid > 0)
                    {
                        MFG_WORK_ORDEREntity WO = new MFG_WORK_ORDEREntity();
                        WO.WorkOrderNo = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == item.woid).Select(x => x.WorkOrderNo).FirstOrDefault();
                        entity.WOEntity = WO;
                    }

                    entity.transferId = item.transferId;
                    if (item.transferId != "")
                    {
                        T_ITEM_TRANSFEREntity Tent = new T_ITEM_TRANSFEREntity();
                        int indx = Convert.ToString(item.transferId).IndexOf(',');
                        var TNo = "";
                        long tid = 0;
                        if (indx > 0)
                        {
                            string[] Tid = Convert.ToString(item.transferId).Split(',');

                            for (int i = 0; i < Tid.Length; i++)
                            {
                                tid = Convert.ToInt64(Tid[i]);
                                if (i == 0)
                                {
                                    TNo = db.T_ITEM_TRANSFER.Where(x => x.TRANSFER_ID == tid).Select(x => x.STOCK_TRANSFER_NO).FirstOrDefault();
                                }
                                else
                                {
                                    TNo = TNo + "," + db.T_ITEM_TRANSFER.Where(x => x.TRANSFER_ID == tid).Select(x => x.STOCK_TRANSFER_NO).FirstOrDefault();
                                }
                            }
                        }
                        else
                        {
                            tid = Convert.ToInt64(item.transferId);
                            TNo = db.T_ITEM_TRANSFER.Where(x => x.TRANSFER_ID == tid).Select(x => x.STOCK_TRANSFER_NO).FirstOrDefault();
                        }
                        Tent.STOCK_TRANSFER_NO = TNo;
                        entity.TransferEntity = Tent;
                    }

                    entity.JobStatus = item.JobStatus;
                    entity.startjobdate = item.startjobdate;
                    entity.startjobtime = item.startjobtime;

                    entity.endjobdate = item.endjobdate;
                    entity.endjobtime = item.endjobtime;

                    entity.MachineId = item.MachineId;
                    if (item.MachineId != null)
                    {
                        MachineEntity machEntity = new MachineEntity();
                        machEntity.Machinename = db.MachineMasters.Where(x => x.Id == item.MachineId).Select(x => x.Machinename).FirstOrDefault();
                        entity.machineEntity = machEntity;
                    }

                    entity.ProcessId = item.ProcessId;
                    if (item.ProcessId != null)
                    {
                        M_ProcessMasterEntity ProcEntity = new M_ProcessMasterEntity();
                        decimal Pid = Convert.ToDecimal(item.ProcessId);
                        ProcEntity.SHORT_NAME = db.M_PROCESS_Master.Where(x => x.Process_Id == Pid).Select(x => x.SHORT_NAME).FirstOrDefault();
                        entity.ProcessEntity = ProcEntity;
                    }

                    entity.OutItemName = item.OutItemName;
                    entity.outAttributeId = item.outAttributeId;
                    if (item.OutItemName != null)
                    {
                        M_ItemMasterEntity itemEntity = new M_ItemMasterEntity();
                        cl_GRNInward grnobj = new cl_GRNInward();
                        int IAid = Convert.ToInt32(item.outAttributeId);
                        int ISid = 0;
                        long OutItemid = Convert.ToInt64(item.OutItemName);
                        itemEntity.ItemName = grnobj.getItemName(OutItemid, IAid, ISid);
                        entity.ItemEntity = itemEntity;
                    }

                    entity.OutQty = item.OutQty;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.ModifyBy = item.ModifyBy;
                    entity.ModifyDate = item.ModifyDate;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsStatus = item.IsStatus;
                    entity.AcceptQty = item.AcceptQty;
                    entity.QC = item.QC;
                    list.Add(entity);
                }
            }

            return list;
        }

        //Get Process List Against WO and User
        public List<M_ProcessMasterEntity> GetProcessList(long workorderID, int userid)
        {
            try
            {
                List<M_ProcessMasterEntity> list = new List<M_ProcessMasterEntity>();

                using (var db = new GarmentERPDBEntities())
                {
                    
                    cl_Workorder obj = new cl_Workorder();
                    var processdata = obj.GetWOProcessDetailsForIncharge(userid).Where(x=>x.WorkOrderID== workorderID).ToList();
                    foreach(var data in processdata)
                    {
                        M_ProcessMasterEntity entity = new M_ProcessMasterEntity();
                        entity.Process_Id =Convert.ToDecimal(data.ProcessID);
                        entity.SHORT_NAME = data.ProcessName;
                        list.Add(entity);
                    }
                    

                }
                return list;
            }
            catch (Exception)
            {

                throw;
            }
        }

        //Get Material Transfer No and User
        public List<T_ITEM_TRANSFEREntity> GetTransferNo(long workorderID, int userid)
        {
            
            try
            {
                List<T_ITEM_TRANSFEREntity> list = new List<T_ITEM_TRANSFEREntity>();
                using (var db = new GarmentERPDBEntities())
                {
                    var transferno = db.T_ITEM_TRANSFER.Where(x => x.WO_ID == workorderID && x.RECEIVED_PERSON == userid && x.IsStatus==4).ToList();
                    foreach (var data in transferno)
                    {
                        T_ITEM_TRANSFEREntity entity = new T_ITEM_TRANSFEREntity();
                        entity.TRANSFER_ID = data.TRANSFER_ID;
                        entity.INVENTORY_ID = data.INVENTORY_ID;
                        entity.STOCK_TRANSFER_NO = data.STOCK_TRANSFER_NO;
                        entity.STOCK_TRANSFER_DATE = data.STOCK_TRANSFER_DATE;
                        entity.WAREHOUSE_FROM = data.WAREHOUSE_FROM;
                        entity.WAREHOUSE_TO = data.WAREHOUSE_TO;
                        entity.RECEIVED_PERSON = data.RECEIVED_PERSON;
                        entity.IsStatus = data.IsStatus;
                        entity.Company_ID = data.Company_ID;
                        entity.BranchId = data.BranchId;

                        list.Add(entity);
                    }

                }

                return list;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //Get Machine List Against Process
        public List<MachineEntity> GetMachine(long ProcessId)
        {
            try
            {
                List<MachineEntity> list= new List<MachineEntity>();
                using (var db = new GarmentERPDBEntities())
                {
                    var machinedata = db.M_PROCESS_Master.Where(x => x.Process_Id == ProcessId).ToList();
                    foreach(var data in machinedata)
                    {
                        var test = db.MachineMasters.Where(x => x.Machinetypeid ==data.Process_Machine_Type_Id && x.IsActive == true).FirstOrDefault();
                        MachineEntity entity = new MachineEntity();
                        entity.Id = test.Id;
                        entity.Machinename = test.Machinename;
                        list.Add(entity);
                    }
                }

                return list;
            }
            catch (Exception)
            {

                throw;
            }
        }


        //Get Out Item Details
        public M_BOM_ProcessCycleEntity GetOutItem(int ProcessId, long workorderID)
        {
            try
            {
                M_BOM_ProcessCycleEntity entity = new M_BOM_ProcessCycleEntity();
                using (var db = new GarmentERPDBEntities())
                {
                    var enqid = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == workorderID).Select(x => x.EnquiryID).FirstOrDefault();
                    var poid = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == workorderID).Select(x => x.POID).FirstOrDefault();
                    var BOM = db.M_BillOfMaterialMaster.Where(x => x.Poid == poid).Select(x => x.BOM_ID).FirstOrDefault();
                    BOM= BOM>0? BOM: db.M_BillOfMaterialMaster.Where(x => x.EnquiryId == enqid).Select(x => x.BOM_ID).FirstOrDefault();

                    var data = db.M_BOM_ProcessCycle.Where(x => x.BOM_ID == BOM && x.ProcessId==ProcessId).SingleOrDefault();
                    entity.BOM_ID = data.BOM_ID;
                    entity.ProcessId = data.ProcessId;
                    entity.ItemOutId = data.ItemOutId;
                    entity.Item_Attribute_ID = data.Item_Attribute_ID;
                    var procwestg = db.M_PROCESS_Master.Where(x => x.Process_Id == ProcessId).Select(x => x.Process_Wastage).FirstOrDefault();
                    entity.westage = (procwestg == null) ? 0 : procwestg;
                    int IAid = Convert.ToInt32(data.Item_Attribute_ID);
                    int ISid = 0;
                    long itemoutid = Convert.ToInt64(data.ItemOutId);
                    cl_GRNInward onj = new cl_GRNInward();
                    M_ItemMasterEntity ent = new M_ItemMasterEntity();
                    ent.ItemName= onj.getItemName(itemoutid,IAid, ISid);
                    entity.ItemMasterentity = ent;

                }

                return entity;
            }
            catch (Exception)
            {

                throw;
            }
        }


        //Get Transfer Material Details 
        public List<T_ITEM_TRANSFER_DETAILEntity> GetMaterialTransfer(string TransferNo)
        {
            try
            {
                List<T_ITEM_TRANSFER_DETAILEntity> TransferEnt = new List<T_ITEM_TRANSFER_DETAILEntity>();
                using (var db = new GarmentERPDBEntities())
                {
                    string[] Tno = TransferNo.Split(',');
                    for (int i = 0; i < Tno.Length; i++)
                    {
                        if(Tno[i] != "")
                        {
                            long noid = Convert.ToInt64(Tno[i]);
                            var itemdata = db.T_ITEM_TRANSFER_DETAIL.Where(x =>x.TRANSFER_ID== noid).ToList();
                            foreach (var dt in itemdata)
                            {
                                T_ITEM_TRANSFER_DETAILEntity ent = new T_ITEM_TRANSFER_DETAILEntity();
                                ent.ID = dt.ID;
                                ent.TRANSFER_ID = dt.TRANSFER_ID;
                                ent.ITEM_ID = dt.ITEM_ID;

                                M_ItemMasterEntity itement = new M_ItemMasterEntity();
                                int ISid = (int)db.M_ItemMaster.Where(x => x.Id == dt.ITEM_ID).Select(x => x.Itemsubcategoryid).FirstOrDefault();
                                cl_BillOfMaterialDetail bomobj = new cl_BillOfMaterialDetail();
                                itement.ItemName = bomobj.getItemName((long)dt.ITEM_ID, (int)dt.ITEM_ATTRIBUTE_ID, ISid);

                                ent.itement = itement;

                                ent.TRANSFER_QTY = dt.TRANSFER_QTY;
                                ent.WAREHOUSE_STORAGE_TO = dt.WAREHOUSE_STORAGE_TO;
                                ent.BATCH_NO_ID = dt.BATCH_NO_ID;

                                M_Product_InfoEntity batch = new M_Product_InfoEntity();
                                batch.Dynamic_Controls_value = db.M_Product_Info.Where(x => x.Dynamic_Controls_Id == 4 && x.ItemInward_ID == dt.BATCH_NO_ID).Select(x => x.Dynamic_Controls_value).FirstOrDefault();
                                ent.BatchInfo = batch;

                                M_Product_InfoEntity batch1 = new M_Product_InfoEntity();
                                batch1.Dynamic_Controls_value = db.M_Product_Info.Where(x => x.Dynamic_Controls_Id == 1 && x.ItemInward_ID == dt.BATCH_NO_ID).Select(x => x.Dynamic_Controls_value).FirstOrDefault();
                                ent.BatchInfo1 = batch1;


                                ent.UNIT_ID = dt.UNIT_ID;
                                M_UnitMasterEntity unit = new M_UnitMasterEntity();
                                unit.ItemUnit = db.M_UnitMaster.Where(x => x.Id == dt.UNIT_ID).Select(x => x.ItemUnit).FirstOrDefault();
                                ent.unit = unit;

                                ent.InspectedQty = dt.InspectedQty != null ? dt.InspectedQty : 0;
                                ent.RejectedQty = dt.RejectedQty != null ? dt.RejectedQty : 0;
                                ent.ITEM_ATTRIBUTE_ID = dt.ITEM_ATTRIBUTE_ID;
                                ent.QC = dt.QC;
                                ent.CREATED_BY = dt.CREATED_BY;
                                ent.CREATED_ON = dt.CREATED_ON;
                                ent.MODIFY_BY = dt.MODIFY_BY;
                                ent.MODIFY_ON = dt.MODIFY_ON;
                                ent.Company_ID = dt.Company_ID;
                                ent.BranchId = dt.BranchId;
                                TransferEnt.Add(ent);
                            }
                        }
                    }
                    
                }

                return TransferEnt;
            }
            catch (Exception)
            {

                throw;
            }
        }


        //Insert Job Work
        public long Insert(M_JobworkEntity entity)
        {
            try
            {
                long id = 0;
                using (var db = new GarmentERPDBEntities())
                {
                    M_Jobwork data = new M_Jobwork();
                    
                    data.processInchargeId = entity.processInchargeId;
                    data.woid = entity.woid;
                    data.transferId = entity.transferId;
                    data.Senderby = entity.Senderby;
                    data.JobStatus = entity.JobStatus;
                    data.startjobdate = entity.startjobdate;
                    data.startjobtime = entity.startjobtime;
                    data.endjobdate = entity.endjobdate;
                    data.endjobtime = entity.endjobtime;
                    data.MachineId = entity.MachineId;
                    data.ProcessId = entity.ProcessId;
                    data.OutItemName = entity.OutItemName;
                    data.outAttributeId = entity.outAttributeId;
                    data.OutQty = entity.OutQty;
                    data.CreatedBy = entity.CreatedBy;
                    data.CreatedDate = DateTime.Now;
                    data.Company_ID = entity.Company_ID;
                    data.BranchId = entity.BranchId;
                    data.IsStatus = entity.IsStatus;
                    data.jobworkno = getnextJWNo();
                    db.M_Jobwork.Add(data);
                    db.SaveChanges();
                    id = data.jobworkid;
                    if(id>0)
                    {
                        foreach (var item in entity.JBmaterial)
                        {
                            if(item.ConsumedQty>0)
                            {
                                M_Jobwork_Material itemrecord = new M_Jobwork_Material();
                                itemrecord.jobworkid = id;
                                itemrecord.ItemId = item.ItemId;
                                itemrecord.unitid = item.unitid;
                                itemrecord.BatchId = item.BatchId;
                                itemrecord.BatchQty = item.BatchQty;
                                itemrecord.ConsumedQty = item.ConsumedQty;
                                itemrecord.BalanceQty = item.BalanceQty;
                                itemrecord.CreatedBy = item.CreatedBy;
                                itemrecord.CreatedDate = DateTime.Now;
                                itemrecord.Company_ID = item.Company_ID;
                                itemrecord.BranchId = item.BranchId;
                                itemrecord.ItemAttributeId = item.ItemAttributeId;
                                db.M_Jobwork_Material.Add(itemrecord);
                                db.SaveChanges();
                                long id2 = itemrecord.j_materialId;

                                M_ItemOutwardMaster outitemrecord = new M_ItemOutwardMaster();
                                outitemrecord.Inward_ID = id2;
                                outitemrecord.MI_ID = db.MFG_MaterialIssueMaster.Where(x => x.RefWO == entity.woid).Select(x => x.Id).FirstOrDefault(); 
                                outitemrecord.Process_ID = entity.ProcessId;
                                outitemrecord.ItemSubCategoryID = 0;
                                outitemrecord.Item_ID = item.ItemId;
                                outitemrecord.Item_Attribute_ID = item.ItemAttributeId;
                                outitemrecord.UOM =(int)item.unitid;
                                outitemrecord.ItemQty = item.ConsumedQty;
                                outitemrecord.IsActive = true;
                                outitemrecord.OutwardBy =Convert.ToInt32(item.CreatedBy);
                                outitemrecord.OutwardDate = DateTime.Now;
                                outitemrecord.Item_Outid =(int) entity.OutItemName;
                                outitemrecord.Item_Attribute_ID =(int)entity.outAttributeId;
                                outitemrecord.ItemSubCategoryID = db.M_ItemMaster.Where(x => x.Id == entity.OutItemName).Select(x => x.Itemsubcategoryid).FirstOrDefault();
                                outitemrecord.Company_ID = entity.Company_ID;
                                outitemrecord.BranchId = entity.BranchId;
                                outitemrecord.IsStatus = entity.IsStatus;
                                db.M_ItemOutwardMaster.Add(outitemrecord);
                                db.SaveChanges();
                                long id3 = outitemrecord.ID;


                            }
                        }
                    }

                    

                }
                return id;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
