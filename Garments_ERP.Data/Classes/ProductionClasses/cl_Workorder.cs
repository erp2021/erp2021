﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.ProductionEntities;

namespace Garments_ERP.Data.Admin
{
    public class cl_Workorder
    {
        public List<M_ProductInfoEntity> GetBatchData(long id, long ItemId, int ItemAttr)
        {
            try
            {
                List<M_ProductInfoEntity> list = new List<M_ProductInfoEntity>();
                using (var db = new GarmentERPDBEntities())
                {
                    var jobdata = db.M_Jobwork.Where(x => x.woid == id && x.OutItemName == ItemId && x.outAttributeId == ItemAttr).ToList();
                    if(jobdata!=null)
                    {
                        foreach(var jobr in jobdata)
                        {
                            long jobid = Convert.ToInt64(jobr.jobworkid);
                            var record = db.M_ItemInwardMaster.Where(x => x.Item_ID == ItemId && x.Item_Attribute_ID == ItemAttr && x.Jobwork_ID == jobid).ToList();
                            foreach (var item in record)
                            {

                                M_ProductInfoEntity RAEntity = new M_ProductInfoEntity();

                                var productdata = db.M_Product_Info.Where(x => x.ItemInward_ID == item.ID).ToList();
                                RAEntity.inwardId = item.ID;
                                string expdat = "", lotno = "", batchno = "", serialno = "";
                                foreach (var PId in productdata)
                                {
                                    //Expiry Date
                                    if (PId.Dynamic_Controls_Id == 1)
                                    {
                                        expdat = PId.Dynamic_Controls_value;
                                    }
                                    else if (PId.Dynamic_Controls_Id == 3)//Lot No
                                    {
                                        lotno = PId.Dynamic_Controls_value;
                                    }
                                    else if (PId.Dynamic_Controls_Id == 4)
                                    {
                                        batchno = PId.Dynamic_Controls_value;
                                    }
                                    else if (PId.Dynamic_Controls_Id == 6)//Serial No
                                    {
                                        serialno = PId.Dynamic_Controls_value;
                                    }
                                }
                                RAEntity.ExpiryDate = expdat == "" ? "" : expdat;
                                RAEntity.LotNo = lotno == "" ? "" : lotno;
                                RAEntity.BatchNo = batchno == "" ? "" : batchno;
                                RAEntity.SerialNo = serialno == "" ? "" : serialno;

                                RAEntity.TotalQty = item.ItemQty;
                                var itemout = db.M_ItemOutwardMaster.Where(x => x.Inward_ID == item.ID).ToList();
                                decimal totoutQty = 0;
                                foreach (var IO in itemout)
                                {
                                    totoutQty = Convert.ToDecimal(totoutQty) + Convert.ToDecimal(IO.ItemQty);
                                }
                                RAEntity.ConsumedQty = totoutQty;
                                decimal BalQty = 0;
                                if (item.ItemQty > totoutQty)
                                {
                                    BalQty = Convert.ToDecimal(item.ItemQty) - Convert.ToDecimal(totoutQty);
                                }
                                else
                                {
                                    BalQty = 0;
                                }
                                RAEntity.BalancedQty = BalQty;
                                if (BalQty > 0)
                                {
                                    list.Add(RAEntity);
                                }
                            }
                        }
                        
                    }
                }
                return list;

            }
            catch (Exception)
            {

                throw;
            }
        }


        public M_ItemMaster_AttributeMaserEntity GetItemData(long id)
        {
            try
            {
                M_ItemMaster_AttributeMaserEntity entity =new M_ItemMaster_AttributeMaserEntity();
                using (var db = new GarmentERPDBEntities())
                {
                    var WoEntity = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == id && x.IsActive == true).FirstOrDefault();
                    long poid = Convert.ToInt64(WoEntity.POID);
                    decimal WOQty = Convert.ToDecimal(WoEntity.RequiredQty);
                    var POstyle = db.CRM_CustomerPOStyleDetail.Where(x => x.Poid == poid).FirstOrDefault();
                    var sizerecords = db.CRM_CustomerPOStyleSizeDetail.Where(x => x.POstyleid == POstyle.Id & x.IsActive == true).FirstOrDefault();
                    int IAid = Convert.ToInt32(sizerecords.Sizeid);
                    int ISid = 0;
                    long itemid = Convert.ToInt64(POstyle.Itemid);
                    cl_GRNInward grnobj = new cl_GRNInward();
                    string ItemName = grnobj.getItemName(itemid, IAid, ISid);
                    entity.ItemId = itemid;
                    entity.Item_Attribute_ID = IAid;
                    entity.ItemName = ItemName;
                    entity.Quantity = WOQty;
                    long jobid = db.M_Jobwork.Where(x => x.woid == id && x.OutItemName == itemid && x.outAttributeId == IAid).Select(x => x.jobworkid).FirstOrDefault();
                    var HSNCode = db.M_ItemInwardMaster.Where(x => x.Jobwork_ID == jobid && x.Item_ID == itemid && x.Item_Attribute_ID == IAid).Select(x => x.HSNCode).FirstOrDefault();
                    entity.HSNCode = HSNCode != null ? HSNCode : db.M_ItemMaster.Where(x => x.Id == itemid).Select(x => x.HScode).FirstOrDefault();
                    var unitid=db.M_ItemMaster.Where(x => x.Id == itemid).Select(x => x.Unit).FirstOrDefault();
                    if(unitid!=null)
                    {
                        entity.UnitId = Convert.ToInt64(unitid);
                        entity.UnitName = db.M_UnitMaster.Where(x => x.Id == unitid).Select(x => x.ItemUnit).FirstOrDefault();
                    }
                    else
                    {
                        entity.UnitId =0;
                        entity.UnitName = "";
                    }
                }
                return entity;
            }
            catch (Exception)
            {

                throw;
            }
        }

        //Reject WO to update Status
        public bool RejectedWO(long id, int Uid)
        {
            bool isdeleted = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    var record = db.MFG_WORK_ORDER.Find(id);
                    record.IsStatus = 5;
                    record.UpdatedDate = DateTime.Now;
                    record.UpdatedBy =Convert.ToInt64(Uid);
                    db.SaveChanges();
                    isdeleted = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isdeleted;
        }

        //List Of  Work Order With Status and User Wise
        public List<MFG_WORK_ORDEREntity> GetStatusData(int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<MFG_WORK_ORDEREntity> list = new List<MFG_WORK_ORDEREntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.MFG_WORK_ORDER.Where(x => x.IsActive == true && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).ToList();
                //if (records.Count >= 0 && roleid != 4)
                //{
                //    string str = "select WorkOrderID,CustomerID,POID,StyleID,EnquiryID,ProcessCycleID,WorkOrderNo,PlanID,StyleType,DepartmentID,RevisionNo,RequiredQty,UnitID,Date,Approved,ApproveDate, ";
                //    str = str + "Comment,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,IsActive,IsMIdone,Company_ID,BranchId,IsStatus ";
                //    str = str + " from MFG_WORK_ORDER where IsActive=1  and IsStatus=" + IsStatus + " and Company_ID=" + Company_ID + " and BranchId=" + BranchId + " and CreatedBy in(select UserId from M_User_Role where roleId=4 or roleId=" + roleid + ")";
                //    records = db.MFG_WORK_ORDER.SqlQuery(str).ToList();
                //}
                foreach (var item in records)
                {
                    MFG_WORK_ORDEREntity entity = new MFG_WORK_ORDEREntity();
                    entity.WorkOrderID = item.WorkOrderID;
                    entity.CustomerID = item.CustomerID;
                    M_LedgersEntity Ledent = new M_LedgersEntity();
                    Ledent.Ledger_Name = db.M_Ledgers.Where(x => x.Ledger_Id == item.CustomerID && x.Is_Active == true).Select(x => x.Ledger_Name).FirstOrDefault();
                    entity.LedgerEntity = Ledent;

                    entity.POID = item.POID;
                    entity.StyleID = item.StyleID;
                    M_StyleMasterEntity styleent = new M_StyleMasterEntity();
                    styleent.Stylename = db.M_StyleMaster.Where(x => x.Id == item.StyleID && x.IsActive == true).Select(x => x.Stylename).FirstOrDefault();
                    entity.M_Styleentity = styleent;

                    entity.EnquiryID = item.EnquiryID;
                    entity.ProcessCycleID = item.ProcessCycleID;
                    entity.WorkOrderNo = item.WorkOrderNo;
                    entity.StyleType = item.StyleType;

                    entity.DepartmentID = item.DepartmentID;
                    entity.RevisionNo = item.RevisionNo;
                    entity.RequiredQty = item.RequiredQty;
                    entity.UnitID = item.UnitID;
                    entity.Date = item.Date;
                    entity.Approved = item.Approved;
                    entity.ApproveDate = item.ApproveDate;
                    entity.IsMIdone = item.IsMIdone;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;

                    entity.PlanID = (item.PlanID != null) ? (long)item.PlanID : -1;

                    entity.Comment = item.Comment;

                    CRM_CustomerPOMasterentity CuspO = new CRM_CustomerPOMasterentity();
                    if (item.CRM_CustomerPOMaster != null)
                    {
                        CuspO.POno = item.CRM_CustomerPOMaster.POno;
                        entity.customerpo = CuspO;
                    }
                    list.Add(entity);
                }
            }

            return list;

        }


        ////List Of Work Order With Status and User and Form Date And To Date Wise
        public List<MFG_WORK_ORDEREntity> get(DateTime from, DateTime to, int IsStatus, int Company_ID, int BranchId, int Userid, int roleid)
        {
            List<MFG_WORK_ORDEREntity> list = new List<MFG_WORK_ORDEREntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.MFG_WORK_ORDER.Where(x => x.IsActive == true && x.Date >= from && x.Date <= to && x.IsStatus == IsStatus && x.Company_ID == Company_ID && x.BranchId == BranchId).ToList();
                //if (records.Count >= 0 && roleid != 4)
                //{
                //    string str = "select WorkOrderID,CustomerID,POID,StyleID,EnquiryID,ProcessCycleID,WorkOrderNo,PlanID,StyleType,DepartmentID,RevisionNo,RequiredQty,UnitID,Date,Approved,ApproveDate, ";
                //    str = str + "Comment,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,IsActive,IsMIdone,Company_ID,BranchId,IsStatus ";
                //    str = str + " from MFG_WORK_ORDER where IsActive=1 and Date >= convert(datetime,'" + from + "',103) and Date <=convert(datetime,'" + to + "',103)  and IsStatus=" + IsStatus + " and Company_ID=" + Company_ID + " and BranchId=" + BranchId + " and CreatedBy in(select UserId from M_User_Role where roleId=4 or roleId=" + roleid + ")";
                //    records = db.MFG_WORK_ORDER.SqlQuery(str).ToList();
                //}
                foreach (var item in records)
                {
                    MFG_WORK_ORDEREntity entity = new MFG_WORK_ORDEREntity();
                    entity.WorkOrderID = item.WorkOrderID;
                    entity.CustomerID = item.CustomerID;
                    entity.POID = item.POID;
                    entity.StyleID = item.StyleID;
                    entity.EnquiryID = item.EnquiryID;
                    entity.ProcessCycleID = item.ProcessCycleID;
                    entity.WorkOrderNo = item.WorkOrderNo;
                    entity.StyleType = item.StyleType;
                    entity.DepartmentID = item.DepartmentID;
                    entity.RevisionNo = item.RevisionNo;
                    entity.RequiredQty = item.RequiredQty;
                    entity.UnitID = item.UnitID;
                    entity.Date = item.Date;
                    entity.Approved = item.Approved;
                    entity.ApproveDate = item.ApproveDate;
                    entity.IsMIdone = item.IsMIdone;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;

                    entity.PlanID = (item.PlanID != null) ? (long)item.PlanID : -1;

                    entity.Comment = item.Comment;

                    CRM_CustomerPOMasterentity CuspO = new CRM_CustomerPOMasterentity();
                    if (item.CRM_CustomerPOMaster != null)
                    {
                        CuspO.POno = item.CRM_CustomerPOMaster.POno;
                        entity.customerpo = CuspO;
                    }
                    list.Add(entity);
                }
            }
            return list;
        }



        public string getnextWOno()
        {
            string no;
            no = "WO-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.MFG_WORK_ORDER.Count() > 0 ? db.MFG_WORK_ORDER.Max(x => x.WorkOrderID) + 1 : 1;

                //  var rec = db.CRM_QuotationMaster.Count() > 0 ? db.CRM_QuotationMaster.Max(x => x.Id) + 1 : 0;
                no = no + "-" + record.ToString();
            }

            return no;
        }

        public List<MFG_WORK_ORDEREntity> Get(DateTime from, DateTime to)
        {
            List<MFG_WORK_ORDEREntity> list = new List<MFG_WORK_ORDEREntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.MFG_WORK_ORDER.ToList().Where(x => x.IsActive == true && x.Date >= from && x.Date <= to).ToList();
                foreach (var item in records)
                {
                    MFG_WORK_ORDEREntity entity = new MFG_WORK_ORDEREntity();
                    entity.WorkOrderID = item.WorkOrderID;
                    entity.CustomerID = item.CustomerID;
                    entity.POID = item.POID;
                    entity.StyleID = item.StyleID;
                    entity.ProcessCycleID = item.ProcessCycleID;
                    entity.EnquiryID = item.EnquiryID;
                    entity.WorkOrderNo = item.WorkOrderNo;
                    entity.StyleType = item.StyleType;
                    entity.DepartmentID = item.DepartmentID;
                    entity.RevisionNo = item.RevisionNo;
                    entity.RequiredQty = item.RequiredQty;
                    entity.UnitID = item.UnitID;
                    entity.Date = item.Date;
                    entity.Approved = item.Approved;
                    entity.ApproveDate = item.ApproveDate;
                    entity.Comment = item.Comment;
                    entity.IsMIdone = item.IsMIdone;
                    CRM_CustomerPOMasterentity CuspO = new CRM_CustomerPOMasterentity();
                    if (item.CRM_CustomerPOMaster != null)
                    {
                        CuspO.POno = item.CRM_CustomerPOMaster.POno;
                        entity.customerpo = CuspO;
                    }
                    list.Add(entity);
                }
            }
            return list;
        }

        public List<MFG_WORK_ORDEREntity> Get()
        {
            List<MFG_WORK_ORDEREntity> list = new List<MFG_WORK_ORDEREntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var records = db.MFG_WORK_ORDER.ToList().Where(x => x.IsActive == true).OrderByDescending(x => x.WorkOrderID);
                foreach (var item in records)
                {
                    MFG_WORK_ORDEREntity entity = new MFG_WORK_ORDEREntity();
                    entity.WorkOrderID = item.WorkOrderID;
                    entity.CustomerID = item.CustomerID;
                    entity.POID = item.POID;
                    entity.StyleID = item.StyleID;
                    entity.EnquiryID = item.EnquiryID;
                    entity.ProcessCycleID = item.ProcessCycleID;
                    entity.WorkOrderNo = item.WorkOrderNo;
                    entity.StyleType = item.StyleType;
                    entity.DepartmentID = item.DepartmentID;
                    entity.RevisionNo = item.RevisionNo;
                    entity.RequiredQty = item.RequiredQty;
                    entity.UnitID = item.UnitID;
                    entity.Date = item.Date;
                    entity.Approved = item.Approved;
                    entity.ApproveDate = item.ApproveDate;
                    entity.IsMIdone = item.IsMIdone;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsStatus = item.IsStatus;
                    entity.PlanID = (item.PlanID != null) ? (long)item.PlanID : -1;

                    entity.Comment = item.Comment;

                    CRM_CustomerPOMasterentity CuspO = new CRM_CustomerPOMasterentity();
                    if (item.CRM_CustomerPOMaster != null)
                    {
                        CuspO.POno = item.CRM_CustomerPOMaster.POno;
                        entity.customerpo = CuspO;
                    }
                    list.Add(entity);
                }
            }
            return list;
        }

        public List<MFG_WORK_ORDEREntity> GetWOMI(int comid,int branchid)
        {
            List<long> WOId = new List<long>();
            List<bool> WOIdTF = new List<bool>();

            List<MFG_WORK_ORDEREntity> list = new List<MFG_WORK_ORDEREntity>();

            using (var db = new GarmentERPDBEntities())
            {
                var MId = db.MFG_MaterialIssueMaster.Where(x => x.IsActive == true && x.Company_ID==comid && x.BranchId==branchid).ToList();
                foreach (var Data in MId)
                {

                    var rec1 = db.MFG_MaterialIssueMaster.Where(x => x.RefWO == Data.RefWO && x.Company_ID == comid && x.BranchId == branchid).Select(x => x.Id).ToList();
                    WOIdTF.Clear();
                    foreach (var dat2 in rec1)
                    {
                        var recd1 = db.MFG_MaterialIssueItemDetails.Where(x => x.MaterialIssueid == dat2).ToList();

                        foreach (var dat3 in recd1)
                        {
                            if (dat3.BalanceQty == 0)
                            {
                                WOIdTF.Add(true);
                            }
                            else
                            {
                                WOIdTF.Add(false);
                            }
                        }
                    }
                    int chki = WOIdTF.Count(ai => ai == false);
                    if (chki == 0)
                    {
                        WOId.Add(Convert.ToInt32(Data.RefWO));
                    }
                }


                var records = db.MFG_WORK_ORDER.ToList().Where(x => x.IsActive == true && x.Company_ID == comid && x.BranchId == branchid && !WOId.Contains(x.WorkOrderID)).OrderByDescending(x => x.WorkOrderID);
                foreach (var item in records)
                {
                    MFG_WORK_ORDEREntity entity = new MFG_WORK_ORDEREntity();
                    entity.WorkOrderID = item.WorkOrderID;
                    entity.CustomerID = item.CustomerID;
                    entity.POID = item.POID;
                    entity.StyleID = item.StyleID;
                    entity.EnquiryID = item.EnquiryID;
                    entity.ProcessCycleID = item.ProcessCycleID;
                    entity.WorkOrderNo = item.WorkOrderNo;
                    entity.StyleType = item.StyleType;
                    entity.DepartmentID = item.DepartmentID;
                    entity.RevisionNo = item.RevisionNo;
                    entity.RequiredQty = item.RequiredQty;
                    entity.UnitID = item.UnitID;
                    entity.Date = item.Date;
                    entity.Approved = item.Approved;
                    entity.ApproveDate = item.ApproveDate;
                    entity.IsMIdone = item.IsMIdone;
                    entity.PlanID = (item.PlanID != null) ? (long)item.PlanID : -1;

                    entity.Comment = item.Comment;

                    CRM_CustomerPOMasterentity CuspO = new CRM_CustomerPOMasterentity();
                    if (item.CRM_CustomerPOMaster != null)
                    {
                        CuspO.POno = item.CRM_CustomerPOMaster.POno;
                        entity.customerpo = CuspO;
                    }
                    list.Add(entity);
                }
            }
            return list;
        }

        public MFG_WORK_ORDEREntity GetbyId(long id)
        {
            MFG_WORK_ORDEREntity entity = new MFG_WORK_ORDEREntity();
            using (var db = new GarmentERPDBEntities())
            {
                cl_CustomerPO poobj = new cl_CustomerPO();
                var item = db.MFG_WORK_ORDER.Find(id);
                if (item != null)
                {
                    entity.WorkOrderID = item.WorkOrderID;
                    entity.CustomerID = item.CustomerID;
                    var custname = db.M_Ledgers.Where(x => x.Ledger_Id == item.CustomerID).Select(x => x.Ledger_Name).FirstOrDefault();
                    M_LedgersEntity ledent = new M_LedgersEntity();
                    ledent.Ledger_Name = custname;
                    entity.LedgerEntity = ledent;
                    entity.POID = item.POID;
                    entity.StyleID = item.StyleID;
                    M_StyleMasterEntity styleentity = new M_StyleMasterEntity();
                    if (item.M_StyleMaster != null)
                    {
                        styleentity.Id = item.M_StyleMaster.Id;
                        styleentity.Stylename = item.M_StyleMaster.Stylename;
                        entity.M_Styleentity = styleentity;
                    }
                    entity.EnquiryID = item.EnquiryID;
                    entity.ProcessCycleID = item.ProcessCycleID;
                    entity.WorkOrderNo = item.WorkOrderNo;
                    entity.StyleType = item.StyleType;
                    entity.DepartmentID = item.DepartmentID;                   
                    entity.RevisionNo = item.RevisionNo;
                    entity.RequiredQty = item.RequiredQty;
                    entity.UnitID = item.UnitID;
                    entity.Date = item.Date;
                    entity.Approved = item.Approved;
                    entity.ApproveDate = item.ApproveDate;
                    entity.Comment = item.Comment;
                    entity.IsMIdone = item.IsMIdone;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    var customerporecord = poobj.getbyid(item.POID);
                    entity.customerpo = customerporecord;

                    var detailrecordlist = db.MFG_WORK_ORDER_DETAIL.Where(x => x.WorkOrderID == item.WorkOrderID).ToList();
                    List<MFG_WORK_ORDER_DETAILEntity> detaillist = new List<MFG_WORK_ORDER_DETAILEntity>();
                    decimal total_ = 0;
                    foreach (var size in detailrecordlist)
                    {
                        MFG_WORK_ORDER_DETAILEntity detail = new MFG_WORK_ORDER_DETAILEntity();
                        detail.ID = size.ID;
                        detail.Quantity = size.Quantity;
                        total_ = total_ + (decimal)detail.Quantity;
                        detail.RefStyleID = size.RefStyleID;
                        detail.UnitID = size.UnitID;
                        detail.SizeID = size.SizeID;
                        detail.BalanceQty = size.BalanceQty;
                        detail.ProductionQty = size.ProductionQty;
                        detaillist.Add(detail);
                    }
                    entity.RequiredQty = total_;
                    entity.workorderdetaillist = detaillist;
                }

            }

            return entity;
        }

        public List<MFG_WorkOrderProcessDetailsEntity> GetbyWOId(int id)
        {
            using (var db = new GarmentERPDBEntities())
            {
                var recordlist = db.MFG_WorkOrderProcessDetails.Where(x => x.WO_ID == id && x.IsActive == true).ToList();
                List<MFG_WorkOrderProcessDetailsEntity> woprocessdetaillist = new List<MFG_WorkOrderProcessDetailsEntity>();
                foreach (var record in recordlist)
                {
                    MFG_WorkOrderProcessDetailsEntity processrecord = new MFG_WorkOrderProcessDetailsEntity();
                    processrecord.WOProcessID = record.WOProcessID;
                    processrecord.WO_ID = record.WO_ID;
                    MFG_WORK_ORDEREntity woentity = new MFG_WORK_ORDEREntity();
                    if (record.MFG_WORK_ORDER != null)
                    {
                        woentity.WorkOrderID = record.MFG_WORK_ORDER.WorkOrderID;
                        woentity.WorkOrderNo = record.MFG_WORK_ORDER.WorkOrderNo;
                        processrecord.MFG_WORK_ORDER = woentity;
                    }
                    processrecord.ProcessID = record.ProcessID;
                    M_ProcessMasterEntity processentity = new M_ProcessMasterEntity();
                    if (record.M_PROCESS_Master != null)
                    {
                        processentity.Process_Id = record.M_PROCESS_Master.Process_Id;
                        processentity.Process_Incharge_ID = record.M_PROCESS_Master.Process_Incharge_ID;
                        processentity.SHORT_NAME = record.M_PROCESS_Master.SHORT_NAME;
                        processrecord.M_PROCESS_Master = processentity;
                    }
                    processrecord.ProcessInchargeID = record.ProcessInchargeID;
                    EmployeeEntity empentity = new EmployeeEntity();
                    if (record.ProcessInchargeID != null)
                    {
                        empentity.UserId =db.Employees.Where(x=>x.UserId== record.ProcessInchargeID).Select(x=>x.UserId).FirstOrDefault();
                        empentity.Name = db.Employees.Where(x => x.UserId == record.ProcessInchargeID).Select(x => x.Name).FirstOrDefault();
                        processrecord.Employee = empentity;
                    }
                    woprocessdetaillist.Add(processrecord);

                }
                return woprocessdetaillist;
            }
        }

        public List<SP_GetWO_ProcessMaterialDetailsEntity> GetWOProcessDetails()
        {
            List<SP_GetWO_ProcessMaterialDetailsEntity> list = new List<SP_GetWO_ProcessMaterialDetailsEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SP_GetWO_ProcessMaterialDetails().ToList().Where(x => x.IsActive == true && x.IssuedQty < x.RequiredQty);
                foreach (var item in records)
                {
                    SP_GetWO_ProcessMaterialDetailsEntity entity = new SP_GetWO_ProcessMaterialDetailsEntity();
                    entity.WOProcessMaterial_ID = item.WOProcessMaterial_ID;
                    entity.WOProcessID = item.WOProcessID;
                    entity.Itemsubcategory = item.Itemsubcategory;
                    entity.ItemName = item.ItemName;
                    entity.ItemUnit = item.ItemUnit;
                    entity.RequiredQty = item.RequiredQty;
                    entity.IssuedQty = item.IssuedQty;
                    entity.ItemCheckedby = item.ItemCheckedby;
                    entity.ItemCheckedDate = item.ItemCheckedDate;
                    entity.WOStartedBY = item.WOStartedBY;
                    entity.WOStartDate = item.WOStartDate;
                    entity.WOEndedBy = item.WOEndedBy;
                    entity.WOEndDate = item.WOEndDate;
                    list.Add(entity);
                }
            }
            return list;
        }

        public List<SP_GetProc_DetailsForProc_InchargeEntity> GetWOProcessDetailsForIncharge(int inId)
        {
            List<SP_GetProc_DetailsForProc_InchargeEntity> list = new List<SP_GetProc_DetailsForProc_InchargeEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SP_GetProc_DetailsForProc_Incharge(inId).ToList();
                foreach (var item in records)
                {
                    SP_GetProc_DetailsForProc_InchargeEntity entity = new SP_GetProc_DetailsForProc_InchargeEntity();
                    entity.ProcessID = item.ProcessID;
                    entity.ProcessName = item.ProcessName;
                    entity.WorkOrderNo = item.WorkOrderNo;
                    entity.WorkOrderID = item.WorkOrderID;
                    list.Add(entity);
                }
            }
            return list;
        }

        public long InsertWOProcesses(MFG_WorkOrderProcessDetailsEntity entity)
        {
            long id = 0;
            using (var db = new GarmentERPDBEntities())
            {
                MFG_WorkOrderProcessDetails record = new MFG_WorkOrderProcessDetails();
                record.WOProcessID = entity.WOProcessID;
                record.WO_ID = entity.WO_ID;
                record.ProcessID = entity.ProcessID;
                record.ProcessInchargeID = entity.ProcessInchargeID;
                record.CreatedDate = DateTime.Now;
                record.CreatedBy = entity.CreatedBy;
                record.IsActive = true;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.BranchId;
                db.MFG_WorkOrderProcessDetails.Add(record);
                db.SaveChanges();
                id = record.WOProcessID;
            }
            return id;
        }
        public bool InsertWOProcessMaterial(long id)
        {
            bool isInserted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SP_GetWO_ProcessMaterial(id).ToList();
                foreach (var item in records)
                {
                    MFG_WorkOrderProcessMaterials record = new MFG_WorkOrderProcessMaterials();
                    record.WOProcessID = item.WOProcessID;
                    record.ItemSubCategoryID = Convert.ToInt32(item.ItemSubCategory);
                    record.ItemID = item.Itemid;
                    record.UnitID = item.UnitId;
                    record.RequiredQty = item.ProcessRequiredQty;
                    record.IssuedQty = item.IssuedQty;
                    record.IsActive = true;
                    db.MFG_WorkOrderProcessMaterials.Add(record);
                    db.SaveChanges();
                    isInserted = true;
                }
            }
            return isInserted;
        }
        public long Insert(MFG_WORK_ORDEREntity entity)
        {
            long id = 0;

            using (var db = new GarmentERPDBEntities())
            {
                var woiddata = db.MFG_WORK_ORDER.Where(x => x.POID == entity.POID).Select(x => x.WorkOrderID).ToList();
                if (woiddata.Count > 0)
                {
                    foreach (var woid in woiddata)
                    {
                        MFG_WORK_ORDER_DETAIL detail1 = new MFG_WORK_ORDER_DETAIL();
                        var WOD = db.MFG_WORK_ORDER_DETAIL.Where(x => x.WorkOrderID == woid).ToList();
                        foreach (var wd in WOD)
                        {
                            wd.BalanceQty = 0;
                            db.SaveChanges();
                        }
                    }
                }

                MFG_WORK_ORDER record = new MFG_WORK_ORDER();
                record.WorkOrderID = entity.WorkOrderID;
                record.CustomerID = entity.CustomerID;
                record.POID = entity.POID;
                record.StyleID = entity.StyleID;
                record.EnquiryID = entity.EnquiryID;
                record.ProcessCycleID = entity.ProcessCycleID;
                record.StyleType =Convert.ToString(entity.StyleType);
                record.DepartmentID = entity.DepartmentID;
                record.RevisionNo = entity.RevisionNo;
                record.RequiredQty = entity.RequiredQty;
                record.UnitID = entity.UnitID;
                record.Date = entity.Date;
                record.Approved = entity.Approved;
                record.ApproveDate = entity.ApproveDate;
                record.Comment = entity.Comment;
                record.CreatedDate =DateTime.Now;
                record.CreatedBy = entity.CreatedBy;
                record.IsActive = true;
                record.IsMIdone = false;
                record.IsStatus = entity.IsStatus;
                record.Company_ID = entity.Company_ID;
                record.BranchId = entity.BranchId;
                record.WorkOrderNo = Convert.ToString(getnextWOno());
                db.MFG_WORK_ORDER.Add(record);
                db.SaveChanges();
                id = record.WorkOrderID;
                if (id > 0)
                {
                    foreach (var item in entity.workorderdetaillist)
                    {
                        MFG_WORK_ORDER_DETAIL detail = new MFG_WORK_ORDER_DETAIL();
                        detail.WorkOrderID = id;
                        detail.ItemId = item.ItemId;
                        detail.Quantity = item.Quantity;
                        detail.RefStyleID = item.RefStyleID;
                        detail.UnitID = item.UnitID;
                        detail.SizeID = item.SizeID;
                        detail.BalanceQty = item.BalanceQty;
                        detail.ProductionQty = item.ProductionQty;
                        detail.Attribute_ID = item.Attribute_ID;
                        detail.Attribute_Value_ID  = item.Attribute_Value_ID;
                        detail.rate = item.rate;
                        detail.Company_ID = item.Company_ID;
                        detail.BranchId = item.BranchId;
                        detail.IsActive = true;
                        db.MFG_WORK_ORDER_DETAIL.Add(detail);
                        db.SaveChanges();
                    }
                }

                if (id > 0)
                {
                    List<bool> POIdTF = new List<bool>();
                    var rec1 = db.MFG_WORK_ORDER.Where(x => x.POID == entity.POID).Select(x => x.WorkOrderID).ToList();
                    POIdTF.Clear();
                    foreach (var dat2 in rec1)
                    {
                        var recd1 = db.MFG_WORK_ORDER_DETAIL.Where(x => x.WorkOrderID == dat2).ToList();

                        foreach (var dat3 in recd1)
                        {
                            if (dat3.BalanceQty == 0)
                            {
                                POIdTF.Add(true);
                            }
                            else
                            {
                                POIdTF.Add(false);
                            }
                        }
                    }
                    int chki = POIdTF.Count(ai => ai == false);
                    if (chki == 0)
                    {
                        var data = db.CRM_CustomerPOMaster.Find(entity.POID);
                        data.IsStatus = 4;
                        data.Updateddate = DateTime.Now;
                        data.Updatedby =(int)entity.CreatedBy;
                        db.SaveChanges();
                    }
                }
            }

            return id;

        }
        public bool MIDone(long id)
        {

            bool done = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.MFG_WORK_ORDER.Find(id);
                record.IsMIdone = true;
                db.SaveChanges();
                done = true;
            }
            return done;
        }
        public bool Delete(long id,int Uid)
        {

            bool deleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.MFG_WORK_ORDER.Find(id);
                record.IsActive = false;
                record.UpdatedBy = Uid;
                record.UpdatedDate = DateTime.Now;
                db.SaveChanges();
                deleted = true;

                var poid = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == id).Select(x => x.POID).FirstOrDefault();
                List<bool> POIdTF = new List<bool>();
                var rec1 = db.MFG_WORK_ORDER.Where(x => x.POID == poid).Select(x => x.WorkOrderID).ToList();
                POIdTF.Clear();
                foreach (var dat2 in rec1)
                {
                    var recd1 = db.MFG_WORK_ORDER_DETAIL.Where(x => x.WorkOrderID == dat2).ToList();

                    foreach (var dat3 in recd1)
                    {
                        if (dat3.BalanceQty == 0)
                        {
                            POIdTF.Add(true);
                        }
                        else
                        {
                            POIdTF.Add(false);
                        }
                    }
                }
                int chki = POIdTF.Count(ai => ai == false);
                if (chki == 0)
                {
                    var data = db.CRM_CustomerPOMaster.Find(poid);
                    data.IsStatus = 3;
                    data.Updateddate = DateTime.Now;
                    data.Updatedby = Uid;
                    db.SaveChanges();

                    var indata = db.MFG_WORK_ORDER.Where(x => x.POID == poid).Select(x => x.WorkOrderID).ToList();
                    foreach (var did in indata)
                    {
                        var GRNdata = db.MFG_WORK_ORDER.Find(did);
                        GRNdata.IsStatus = 3;
                        GRNdata.UpdatedDate = DateTime.Now;
                        GRNdata.UpdatedBy = Uid;
                        db.SaveChanges();
                    }

                }

            }
            return deleted;
        }
        public bool Update(MFG_WORK_ORDEREntity entity)
        {
            bool updated = false;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    MFG_WORK_ORDER record = db.MFG_WORK_ORDER.Find(entity.WorkOrderID);

                    record.RevisionNo = entity.RevisionNo;
                    record.RequiredQty = entity.RequiredQty;
                    // record.UnitID = entity.UnitID;
                    record.UpdatedDate = entity.UpdatedDate;
                    record.Approved = entity.Approved;
                    record.ApproveDate = entity.ApproveDate;
                    record.Comment = entity.Comment;
                    record.IsStatus = entity.IsStatus;
                    db.SaveChanges();
                    updated = true;
                    if (updated == true)
                    {
                        
                        foreach (var item in entity.workorderdetaillist)
                        {
                            long id= db.MFG_WORK_ORDER_DETAIL.Where(x => x.WorkOrderID == entity.WorkOrderID && x.SizeID==item.SizeID && x.Attribute_ID==item.Attribute_ID && x.Attribute_Value_ID==item.Attribute_Value_ID).Select(x=>x.ID).FirstOrDefault();
                            MFG_WORK_ORDER_DETAIL detail = db.MFG_WORK_ORDER_DETAIL.Find(id);

                            detail.BalanceQty = item.BalanceQty;
                            detail.ProductionQty = item.ProductionQty;
                            detail.Quantity = item.Quantity;
                            db.SaveChanges();
                        }
                    }

                }
            }
            catch(Exception)
            {
                throw;
            }
            return updated;
        }
        public List<MFG_WORK_ORDEREntity> GetbyCustomerId(long id)
        {
            List<MFG_WORK_ORDEREntity> workOrderList = new List<MFG_WORK_ORDEREntity>();
            using (var db = new GarmentERPDBEntities())
            {

                var exceptionList = db.MFG_PRODUCTION_PLAN.Select(e => e.WorkOrderID).ToList();

                //var query = db.MyEntity
                //                      .Select(e => e.Name)
                //                      .Except(exceptionList);

                var woList = db.MFG_WORK_ORDER.Where(c => c.CustomerID == id).Where(e => !exceptionList.Contains(e.WorkOrderID)).OrderByDescending(x=>x.WorkOrderID).ToList();
                if (woList != null)
                {
                    foreach (var item in woList)
                    {
                        MFG_WORK_ORDEREntity entity = new MFG_WORK_ORDEREntity();
                        entity.WorkOrderID = item.WorkOrderID;
                        entity.WorkOrderNo = item.WorkOrderNo;
                        entity.CustomerID = item.CustomerID;
                        entity.POID = item.POID;
                        entity.StyleID = item.StyleID;
                        entity.EnquiryID = item.EnquiryID;
                        entity.ProcessCycleID = item.ProcessCycleID;
                        entity.WorkOrderNo = item.WorkOrderNo;
                        entity.StyleType = item.StyleType;
                        entity.DepartmentID = item.DepartmentID;
                        entity.RevisionNo = item.RevisionNo;
                        entity.RequiredQty = item.RequiredQty;
                        entity.UnitID = item.UnitID;
                        entity.Date = item.Date;
                        entity.Approved = item.Approved;
                        entity.ApproveDate = item.ApproveDate;
                        entity.Comment = item.Comment;

                        workOrderList.Add(entity);
                    }
                }
            }
            return workOrderList;
        }
        public List<MFG_WORK_ORDEREntity> GetCompletedWorkOrderbyCustomerId(long id)
        {
            List<MFG_WORK_ORDEREntity> workOrderList = new List<MFG_WORK_ORDEREntity>();
            using (var db = new GarmentERPDBEntities())
            {

                var exceptionList = db.MFG_PRODUCTION_PLAN.Select(e => e.WorkOrderID).ToList();

                var woList = db.MFG_WORK_ORDER.Where(c => c.CustomerID == id).Where(e => exceptionList.Contains(e.WorkOrderID)).ToList();
                if (woList != null)
                {
                    foreach (var item in woList)
                    {
                        MFG_WORK_ORDEREntity entity = new MFG_WORK_ORDEREntity();
                        entity.WorkOrderID = item.WorkOrderID;
                        entity.WorkOrderNo = item.WorkOrderNo;
                        entity.CustomerID = item.CustomerID;
                        entity.POID = item.POID;
                        entity.StyleID = item.StyleID;
                        entity.EnquiryID = item.EnquiryID;
                        entity.ProcessCycleID = item.ProcessCycleID;
                        entity.WorkOrderNo = item.WorkOrderNo;
                        entity.StyleType = item.StyleType;
                        entity.DepartmentID = item.DepartmentID;
                        entity.RevisionNo = item.RevisionNo;
                        entity.RequiredQty = item.RequiredQty;
                        entity.UnitID = item.UnitID;
                        entity.Date = item.Date;
                        entity.Approved = item.Approved;
                        entity.ApproveDate = item.ApproveDate;
                        entity.Comment = item.Comment;
                        entity.IsStatus = item.IsStatus;
                        entity.Company_ID = item.Company_ID;
                        entity.BranchId = item.BranchId;

                        workOrderList.Add(entity);
                    }
                }
            }
            return workOrderList;
        }
        public List<KeyValuePair<long, string>> GetMonthlyPlansByWorkOrderID(long workOrderID)
        {
            List<KeyValuePair<long, string>> prodPlanList = new List<KeyValuePair<long, string>>();
            using (var db = new GarmentERPDBEntities())
            {
                var lstPlans = db.MFG_PRODUCTION_PLAN.Where(pp => pp.WorkOrderID == workOrderID && pp.IsActive==true && pp.IsStatus==3).ToList();

                foreach (var item in lstPlans)
                {
                    prodPlanList.Add(new KeyValuePair<long, string>(item.ID, item.PlanNo));
                }
            }

            return prodPlanList;
        }
        public string GetWorkOrdersQuantity(long WorkOrderID)
        {
            string qty = string.Empty;

            using (var db = new GarmentERPDBEntities())
            {
                var wrkOrder = db.MFG_WORK_ORDER.Where(e => e.WorkOrderID == WorkOrderID).FirstOrDefault();
                if (wrkOrder != null)
                    qty = (wrkOrder.RequiredQty != null) ? wrkOrder.RequiredQty.Value.ToString() : string.Empty;
            }
            return qty;
        }

        public List<MFG_WORK_ORDEREntity> getWOSEBal(long id)
        {
            List<MFG_WORK_ORDEREntity> entitydata = new List<MFG_WORK_ORDEREntity>();
            using (var db = new GarmentERPDBEntities())
            {
                cl_CustomerPO poobj = new cl_CustomerPO();
                var itemdata = db.MFG_WORK_ORDER.Where(x => x.POID == id && x.IsActive == true).ToList();

                if (itemdata != null)
                {
                    foreach (var item in itemdata)
                    {
                        MFG_WORK_ORDEREntity entity = new MFG_WORK_ORDEREntity();
                        var detailrecordlist = db.MFG_WORK_ORDER_DETAIL.Where(x => x.WorkOrderID == item.WorkOrderID).ToList();
                        List<MFG_WORK_ORDER_DETAILEntity> detaillist = new List<MFG_WORK_ORDER_DETAILEntity>();
                        decimal total_ = 0;
                        foreach (var size in detailrecordlist)
                        {
                            MFG_WORK_ORDER_DETAILEntity detail = new MFG_WORK_ORDER_DETAILEntity();
                            detail.ID = size.ID;
                            detail.Quantity = size.Quantity;
                            total_ = total_ + (decimal)detail.Quantity;
                            detail.RefStyleID = size.RefStyleID;
                            detail.UnitID = size.UnitID;
                            detail.SizeID = size.SizeID;
                            detail.BalanceQty = size.BalanceQty;
                            detail.ProductionQty = size.ProductionQty;
                            detail.WorkOrderID = size.WorkOrderID;
                            detail.Attribute_ID = size.Attribute_ID;
                            detail.Attribute_Value_ID = size.Attribute_Value_ID;
                            ////M_ItemSizeMasterEntity sizentity = new M_ItemSizeMasterEntity();
                            ////if (size.M_ItemSizeMaster != null)
                            ////{
                            ////    sizentity.Id = size.M_ItemSizeMaster.Id;
                            ////    sizentity.Sizetype = size.M_ItemSizeMaster.Sizetype;
                            ////    detail.M_ItemSizeMasterentity = sizentity;
                            ////}

                            detaillist.Add(detail);
                        }
                        entity.RequiredQty = total_;
                        entity.workorderdetaillist = detaillist;
                        entitydata.Add(entity);
                    }
                }

            }

            return entitydata;
        }

        public List<CRM_CustomerPOMasterentity> getPOListbycustomerid(long custid)
        {
            List<long> POId = new List<long>();
            List<bool> POIdTF = new List<bool>();
            
            List<CRM_CustomerPOMasterentity> list = new List<CRM_CustomerPOMasterentity>();
            using (var db = new GarmentERPDBEntities())
            {
                var WOd = db.MFG_WORK_ORDER.Where(x => x.IsActive == true && x.Approved == 1).ToList();
                foreach (var Data in WOd)
                {

                    var rec1 = db.MFG_WORK_ORDER.Where(x => x.POID == Data.POID).Select(x => x.WorkOrderID).ToList();
                    POIdTF.Clear();
                    foreach (var dat2 in rec1)
                    {
                        var recd1 = db.MFG_WORK_ORDER_DETAIL.Where(x => x.WorkOrderID ==dat2).ToList();
                        
                        foreach (var dat3 in recd1)
                        {
                            if(dat3.BalanceQty==0)
                            {
                                POIdTF.Add(true);
                            }
                            else
                            {
                                POIdTF.Add(false);
                            }
                        }
                    }
                    int chki = POIdTF.Count(ai => ai == false);
                    if (chki==0)
                    {
                        POId.Add(Data.POID);
                    }
                }
                cl_CustomerPO CPO_obj = new cl_CustomerPO();
                var records = db.CRM_CustomerPOMaster.ToList().Where(x => x.customerid == custid && x.IsActive == true && !POId.Contains(x.Id)).ToList().OrderByDescending(x => x.Id);
                foreach (var item in records)
                {
                    CRM_CustomerPOMasterentity entity = new CRM_CustomerPOMasterentity();
                    entity.POno = item.POno;
                    entity.Id = item.Id;
                    entity.IsStatus = item.IsStatus;
                    entity.Company_ID = item.Company_ID;
                    entity.BranchId = item.BranchId;
                    entity.IsReadmade = item.IsReadymade;
                    list.Add(entity);
                }

            }
            return list;
        }

    }
}
