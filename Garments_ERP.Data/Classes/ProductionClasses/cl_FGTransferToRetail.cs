﻿using Garments_ERP.Data.Admin;
using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;
using Garments_ERP.Entity.ProductionEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Classes.ProductionClasses
{
    public class cl_FGTransferToRetail
    {
        public List<MFG_WORK_ORDEREntity> Get_WorkOrders(int companyid, int branchid, string inputStr, long CustID)
        {
            List<MFG_WORK_ORDEREntity> list = new List<MFG_WORK_ORDEREntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.sp_get_workorders_autoextender(companyid, branchid, CustID, inputStr).ToList();
                if (records.Count > 0)
                {
                    foreach (var item in records)
                    {
                        MFG_WORK_ORDEREntity entity = new MFG_WORK_ORDEREntity();
                        entity.WorkOrderID = item.WorkOrderID;
                        entity.WorkOrderNo = item.WorkOrderNo;
                        entity.RequiredQty = item.RequiredQty;
                        entity.Itemid = item.Itemid;
                        entity.ItemName = item.ItemName;
                        list.Add(entity);
                    }
                }
            }
            return list;
        }
        public List<FG_TransferMasterEntity> Get_FGTransfers(int companyid, int branchid, string inputStr, long woID)
        {
            List<FG_TransferMasterEntity> list = new List<FG_TransferMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.sp_get_finishtransfers_autoextender(companyid, branchid, woID, inputStr).ToList();
                if (records.Count > 0)
                {
                    foreach (var item in records)
                    {
                        FG_TransferMasterEntity entity = new FG_TransferMasterEntity();
                        entity.Id = item.Id;
                        entity.TransferNo = item.TransferNo;
                        entity.WarehouseId = item.WarehouseId;
                        entity.WarehouseName = item.WarehouseName;
                        list.Add(entity);
                    }
                }
            }
            return list;
        }
        public List<FG_TransferDetailEntity> Get_FGTransferDetails(int companyid, int branchid, long fg_transferid)
        {
            List<FG_TransferDetailEntity> list = new List<FG_TransferDetailEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.sp_get_finishtransfer_details(companyid, branchid, fg_transferid).ToList();
                if (records.Count > 0)
                {
                    foreach (var item in records)
                    {
                        FG_TransferDetailEntity entity = new FG_TransferDetailEntity();
                        entity.Id = item.Id;
                        entity.ProducedQty = item.FG_TransferredQty;
                        list.Add(entity);
                    }
                }
            }
            return list;
        }

        public Int64 SaveFG_Retail(FG_Transfer_To_RetailEntity FG_TREntity)
        {
            long insertedId = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    FG_Transfer_To_Retail item = new FG_Transfer_To_Retail();
                    item.Customer_ID = FG_TREntity.Customer_ID;
                    item.WorkOrderID = FG_TREntity.WorkOrderID;
                    item.Item_ID = FG_TREntity.Item_ID;
                    item.FG_Transfer_ID = FG_TREntity.FG_Transfer_ID;
                    item.WarehouseId = FG_TREntity.WarehouseId;
                    item.RackId = FG_TREntity.RackId;
                    item.Branch_ID = FG_TREntity.Branch_ID;
                    item.Company_ID = FG_TREntity.Company_ID;
                    item.IsActive = true;
                    item.POid = FG_TREntity.POid;
                    item.GRNID = FG_TREntity.GRNID;
                    item.Item_Attribute_Id = FG_TREntity.Item_Attribute_Id;
                    item.HSNCode = FG_TREntity.HSNCode;
                    item.Unit_Id = FG_TREntity.Unit_Id;
                    item.PO_Qty = FG_TREntity.PO_Qty;
                    db.FG_Transfer_To_Retail.Add(item);
                    db.SaveChanges();
                    insertedId = item.FG_TR_ID;

                    foreach (var item_ in FG_TREntity.FG_Transfer_To_Retail_Details)
                    {
                        FG_Transfer_To_Retail_Details itemRecord = new FG_Transfer_To_Retail_Details();
                        itemRecord.FG_TR_ID = insertedId;
                        itemRecord.TransferToRetail_Qty = item_.TransferToRetail_Qty;
                        itemRecord.Balance_Qty = item_.Balance_Qty;
                        itemRecord.InwardId = item_.InwardId;
                        itemRecord.InwardQty = item_.InwardQty;
                        itemRecord.Is_Active = true;
                        db.FG_Transfer_To_Retail_Details.Add(itemRecord);
                        db.SaveChanges();
                        long tranId_ = itemRecord.FG_TR_Detail_ID;



                        M_ItemOutwardMaster outitem = new M_ItemOutwardMaster();
                        outitem.Inward_ID= item_.InwardId;
                        var subcatid = db.M_ItemMaster.Where(x => x.Id == FG_TREntity.Item_ID).Select(x => x.Itemsubcategoryid).FirstOrDefault();
                        outitem.ItemSubCategoryID = Convert.ToInt32(subcatid);
                        outitem.Item_ID = FG_TREntity.Item_ID;
                        outitem.UOM = FG_TREntity.Unit_Id;
                        outitem.ItemQty = item_.TransferToRetail_Qty;
                        outitem.OutwardBy = FG_TREntity.CreatedBy;
                        outitem.OutwardDate = DateTime.Now;
                        outitem.IsActive = true;
                        outitem.Item_Attribute_ID = FG_TREntity.Item_Attribute_Id;
                        outitem.Company_ID = FG_TREntity.Company_ID;
                        outitem.BranchId = FG_TREntity.Branch_ID;
                        db.M_ItemOutwardMaster.Add(outitem);
                        db.SaveChanges();

                        
                        M_ItemInwardMaster inward_ = new M_ItemInwardMaster();
                        inward_.Item_ID = FG_TREntity.Item_ID;
                        inward_.ItemQty = item_.TransferToRetail_Qty;
                        inward_.BalanceQty = item_.TransferToRetail_Qty;
                        inward_.InventoryModeId = 8;
                        inward_.Transaction_ID = tranId_;
                        inward_.IsActive = true;
                        inward_.Company_ID = FG_TREntity.Company_ID;
                        inward_.BranchId = FG_TREntity.Branch_ID;
                        inward_.CreatedBy = FG_TREntity.CreatedBy;
                        inward_.CreatedDate = FG_TREntity.CreatedDate;
                        db.M_ItemInwardMaster.Add(inward_);
                        db.SaveChanges();
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
            return insertedId;
        }

        public List<FG_Transfer_To_RetailEntity> Get(int company_id, int branch_id)
        {
            List<FG_Transfer_To_RetailEntity> list = new List<FG_Transfer_To_RetailEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.FG_Transfer_To_Retail.ToList().Where(x => x.IsActive == true && x.Company_ID == company_id && x.Branch_ID == branch_id);
                foreach (var item in records)
                {
                    FG_Transfer_To_RetailEntity entity = new FG_Transfer_To_RetailEntity();
                    entity.FG_TR_ID = item.FG_TR_ID;
                    entity.Customer_ID = item.Customer_ID;
                    var custname = db.M_Ledgers.Where(x => x.Ledger_Id == item.Customer_ID).Select(x => x.Ledger_Name).FirstOrDefault();
                    entity.Customer_Name = custname;
                    entity.Item_ID = item.Item_ID;
                    int IAid = Convert.ToInt32(item.Item_Attribute_Id);
                    int ISid = 0;
                    M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                    if (item.M_ItemMaster != null)
                    {
                        itementity.Id = item.M_ItemMaster.Id;
                        cl_GRNInward grnobj = new cl_GRNInward();
                        itementity.ItemName = grnobj.getItemName(item.M_ItemMaster.Id, IAid, ISid);
                        entity.M_ItemMaster = itementity;
                    }
                    entity.Item_Attribute_Id = item.Item_Attribute_Id > 0 ? item.Item_Attribute_Id : 0;
                    entity.RackId = item.RackId;
                    entity.WorkOrderID = item.WorkOrderID;
                    if(item.WorkOrderID!=null)
                    {
                        MFG_WORK_ORDEREntity woent = new MFG_WORK_ORDEREntity();
                        woent.WorkOrderNo = db.MFG_WORK_ORDER.Where(x => x.WorkOrderID == item.WorkOrderID).Select(x => x.WorkOrderNo).FirstOrDefault();
                        entity.MFG_WORK_ORDER = woent;
                    }

                    entity.WarehouseId = item.WarehouseId;
                    if (item.WarehouseId > 0)
                    {
                        var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseId).FirstOrDefault();
                        if (warehouse != null)
                        {
                            M_WarehouseEntity locentity = new M_WarehouseEntity();
                            locentity.ID = warehouse.ID;
                            locentity.SHORT_NAME = warehouse.SHORT_NAME;
                            entity.M_WAREHOUSE = locentity;
                        }
                    }

                    entity.FG_Transfer_ID = item.FG_Transfer_ID;
                    if (item.FG_Transfer_ID != null)
                    {
                        FG_TransferMasterEntity FGent = new FG_TransferMasterEntity();
                        FGent.TransferNo = db.FG_TransferMaster.Where(x => x.Id == item.FG_Transfer_ID).Select(x => x.TransferNo).FirstOrDefault();
                        entity.FG_TransferMaster = FGent;
                    }

                    entity.IsActive = item.IsActive;
                    entity.CreatedBy = item.CreatedBy;
                    entity.CreatedDate = item.CreatedDate;
                    entity.UpdatedDate = item.UpdatedDate;
                    entity.UpdatedBy = item.UpdatedBy;
                    entity.POid = item.POid;
                    if (item.POid != null)
                    {
                        CRM_CustomerPOMasterentity POEnt = new CRM_CustomerPOMasterentity();
                        POEnt.POno = db.CRM_CustomerPOMaster.Where(x => x.Id == item.POid).Select(x => x.POno).FirstOrDefault();
                        entity.POEntity = POEnt;
                    }
                    entity.GRNID = item.GRNID;
                    if (item.GRNID != null)
                    {
                        SRM_GRNInwardEntity GRNEnt = new SRM_GRNInwardEntity();
                        GRNEnt.GRNNo = db.SRM_GRNInward.Where(x => x.ID == item.GRNID).Select(x => x.GRNNo).FirstOrDefault();
                        entity.GRNEntity = GRNEnt;
                    }
                    entity.HSNCode = item.HSNCode;
                    entity.PO_Qty = item.PO_Qty;
                    decimal qty = 0;
                    var TranferItemData = db.FG_Transfer_To_Retail_Details.Where(x => x.FG_TR_ID == item.FG_TR_ID).ToList();
                    foreach(var data in TranferItemData)
                    {
                        qty = qty +Convert.ToDecimal(data.TransferToRetail_Qty);
                    }
                    entity.TransferQty = qty;

                    list.Add(entity);
                }
            }
            return list;
        }

        public FG_Transfer_To_RetailEntity getbyid(long id)
        {
            FG_Transfer_To_RetailEntity entity = new FG_Transfer_To_RetailEntity();
            entity.FG_Transfer_To_Retail_Details = null;
            using (var db = new GarmentERPDBEntities())
            {
                var item = db.FG_Transfer_To_Retail.Find(id);
                entity.FG_TR_ID = item.FG_TR_ID;
                entity.Customer_ID = item.Customer_ID;
                var custname = db.M_Ledgers.Where(x => x.Ledger_Id == item.Customer_ID).Select(x => x.Ledger_Name).FirstOrDefault();
                entity.Customer_Name = custname;
                entity.Item_ID = item.Item_ID;
                if (item.Item_ID != null)
                {
                    M_ItemMasterEntity itementity = new M_ItemMasterEntity();
                    itementity.Id = item.M_ItemMaster.Id;
                    itementity.ItemName = item.M_ItemMaster.ItemName;
                    entity.M_ItemMaster = itementity;
                }
                entity.RackId = item.RackId;
                entity.WorkOrderID = item.WorkOrderID;
                entity.WarehouseId = item.WarehouseId;
                if (item.WarehouseId > 0)
                {
                    var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseId).FirstOrDefault();
                    if (warehouse != null)
                    {
                        M_WarehouseEntity locentity = new M_WarehouseEntity();
                        locentity.ID = warehouse.ID;
                        locentity.SHORT_NAME = warehouse.SHORT_NAME;
                        entity.M_WAREHOUSE = locentity;
                    }
                }
                entity.FG_Transfer_ID = item.FG_Transfer_ID;
                entity.IsActive = item.IsActive;
                entity.CreatedBy = item.CreatedBy;
                entity.CreatedDate = item.CreatedDate;
                entity.UpdatedDate = item.UpdatedDate;
                entity.UpdatedBy = item.UpdatedBy;

                List<FG_Transfer_To_Retail_DetailsEntity> itemlist = new List<FG_Transfer_To_Retail_DetailsEntity>();
            //    var records_ = db.sp_get_FGTransfer_to_retail_details(item.FG_TR_ID).ToList();
            //    if (records_.Count > 0)
            //    {
            //        foreach (var item_ in records_)
            //        {
            //            FG_Transfer_To_Retail_DetailsEntity detail_ = new FG_Transfer_To_Retail_DetailsEntity();
            //            detail_.FG_TR_Detail_ID = item_.FG_TR_Detail_ID;
            //            detail_.FG_TR_ID = item_.FG_TR_ID;
            //            detail_.Attribute_ID = item_.Attribute_ID;
            //            detail_.Attribute_Name = item_.AttributName;
            //            detail_.Attribute_Value_Name = item_.AttributeValue;
            //            detail_.Attribute_Value_ID = item_.Attribute_Value_ID;
            //            detail_.FG_Transferred_Qty = item_.FG_Transferred_Qty;
            //            detail_.TransferToRetail_Qty = item_.TransferToRetail_Qty;
            //            detail_.WorkOrderQty = item_.WorkOrderQty;
            //            detail_.Balance_Qty = item_.Balance_Qty;
            //            itemlist.Add(detail_);
            //        }
            //    }
            //    entity.FG_Transfer_To_Retail_Details = itemlist;
            }

            return entity;
        }


        public bool Delete(long id)
        {
            bool deleted = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.FG_Transfer_To_Retail.Find(id);
                record.IsActive = false;
                db.SaveChanges();
                deleted = true;
            }
            return deleted;
        }


        public List<SRM_GRNInwardEntity> GetGRNList(long Bomid)
        {
            try
            {
                List<SRM_GRNInwardEntity> List = new List<SRM_GRNInwardEntity>();
                using (var db = new GarmentERPDBEntities())
                {
                    long SPR = (long)db.SRM_PurchaseRequestMaster.Where(x => x.BOM_Id == Bomid).Select(x => x.Id).FirstOrDefault();
                    long SQUO= (long)db.SRM_QuotationMaster.Where(x => x.PR_Id == SPR).Select(x => x.Id).FirstOrDefault();
                    long SPO= (long)db.SRM_PurchaseOrderMaster.Where(x => x.QuotationId == SQUO).Select(x => x.Id).FirstOrDefault();
                    
                    var records = db.SRM_GRNInward.ToList().Where(x => x.IsActive == true && x.POid== SPO);
                    foreach (var item in records)
                    {
                        SRM_GRNInwardEntity entity = new SRM_GRNInwardEntity();
                        entity.ID = item.ID;
                        entity.WarehouseId = item.WarehouseId;
                        if (item.WarehouseId > 0)
                        {
                            var warehouse = db.M_WAREHOUSE.Where(x => x.ID == item.WarehouseId).FirstOrDefault();
                            if (warehouse != null)
                            {
                                M_WarehouseEntity locentity = new M_WarehouseEntity();
                                locentity.ID = warehouse.ID;
                                locentity.SHORT_NAME = warehouse.SHORT_NAME;
                                entity.M_WarehouseMaster = locentity;
                            }
                        }

                        entity.SupplierId = item.SupplierId;
                        if (entity.SupplierId != null)
                        {
                            M_LedgersEntity legderentity = new M_LedgersEntity();
                            var custdata = db.M_Ledgers.Where(x => x.Ledger_Id == entity.SupplierId).FirstOrDefault();
                            if (custdata != null)
                            {
                                legderentity.Ledger_Id = custdata.Ledger_Id;
                                legderentity.Ledger_Name = custdata.Ledger_Name;
                            }
                            else
                            {
                                legderentity.Ledger_Id = Convert.ToInt32(entity.SupplierId);
                                legderentity.Ledger_Name = "--";
                            }
                            entity.M_Ledgersentity = legderentity;
                        }
                        entity.GRNNo = item.GRNNo;
                        entity.InwardDate = item.InwardDate;
                        entity.ChallanNO = item.ChallanNO;
                        entity.ChallanDate = item.ChallanDate;
                        entity.InvoiceNo = item.InvoiceNo;
                        entity.InvoiceDate = item.InvoiceDate;
                        entity.DepartmentId = item.DepartmentId;
                        entity.InwardBy = item.InwardBy;
                        entity.InvoiceCreated = item.InvoiceCreated;
                        entity.CreatedBy = item.CreatedBy;
                        entity.CreatedDate = item.CreatedDate;
                        entity.UpdatedBy = item.UpdatedBy;
                        entity.UpdatedDate = item.UpdatedDate;
                        entity.ProcessingStatusId = item.ProcessingStatusId;
                        entity.InvoiceAmount = item.InvoiceAmount;
                        entity.EwayBillNo = item.EwayBillNo;
                        entity.TruckNo = item.TruckNo;
                        entity.Transporter = item.Transporter;
                        entity.IsActive = item.IsActive;
                        entity.POno = db.SRM_PurchaseOrderMaster.Where(x => x.Id == item.POid).Select(x => x.PO_NO).FirstOrDefault();
                        List.Add(entity);
                    }
                }
                return List;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
