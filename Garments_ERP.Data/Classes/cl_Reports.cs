﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.ReportsSPEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Garments_ERP.Data.Admin
{
    public class cl_Reports
    {
        public List<SP_WarehouseStockDetailsEntity> GetDateWiseWarehouseStock(long itemcategory)
        {
            List<SP_WarehouseStockDetailsEntity> list = new List<SP_WarehouseStockDetailsEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SP_GetWarehouseStockDetails().ToList().Where(x => x.Id== itemcategory);
                foreach (var item in records)
                {
                    SP_WarehouseStockDetailsEntity entity = new SP_WarehouseStockDetailsEntity();
                    entity.SHORT_NAME = item.SHORT_NAME;                  
                    entity.ItemName = item.ItemName;
                    entity.ItemId = item.ItemId;
                    entity.Itemcategory = item.Itemcategory;             
                    entity.Accepted_Qty = item.Accepted_Qty;
                    entity.ItemUnit = item.ItemUnit;
                    list.Add(entity);                  
                }
            }

            return list;

        }
        public List<SP_WarehouseStockDetailsEntity> GetWarehouseStock()
        {
            List<SP_WarehouseStockDetailsEntity> list = new List<SP_WarehouseStockDetailsEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SP_GetWarehouseStockDetails().ToList();
                foreach (var item in records)
                {
                    SP_WarehouseStockDetailsEntity entity = new SP_WarehouseStockDetailsEntity();
                    entity.SHORT_NAME = item.SHORT_NAME;
                    entity.ItemName = item.ItemName;
                    entity.ItemId = item.ItemId;
                    entity.Itemcategory = item.Itemcategory;
                    entity.Accepted_Qty = item.Accepted_Qty;
                    entity.ItemUnit = item.ItemUnit;
                    list.Add(entity);
                }
            }
            return list;

        }
        public List<SP_GetItemInwardDetailsEntity> GetItemInwardDetail(long itemname)
        {
            List<SP_GetItemInwardDetailsEntity> list = new List<SP_GetItemInwardDetailsEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SP_GetItemInwardDetails().ToList().Where(x => x.ItemId == itemname);
                foreach (var item in records)
                {
                    SP_GetItemInwardDetailsEntity entity = new SP_GetItemInwardDetailsEntity();
                    entity.SHORT_NAME = item.SHORT_NAME;
                    entity.ItemName = item.ItemName;
                    entity.GRN_Qty = item.GRN_Qty;
                    entity.Accepted_Qty = item.Accepted_Qty;
                    entity.GRNNo = item.GRNNo;
                    entity.InwardDate = item.InwardDate;
                    list.Add(entity);
                }
            }
            return list;
        }
        public List<SP_GetItemInwardDetailsEntity> GetDateWiseItemInwardDetail(DateTime from,DateTime to)
        {
            List<SP_GetItemInwardDetailsEntity> list = new List<SP_GetItemInwardDetailsEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SP_GetItemInwardDetails().Where(x => x.InwardDate >= from && x.InwardDate <= to).ToList();
                foreach (var item in records)
                {
                    SP_GetItemInwardDetailsEntity entity = new SP_GetItemInwardDetailsEntity();
                    entity.SHORT_NAME = item.SHORT_NAME;
                    entity.ItemName = item.ItemName;
                    entity.GRN_Qty = item.GRN_Qty;
                    entity.Accepted_Qty = item.Accepted_Qty;
                    entity.GRNNo = item.GRNNo;
                    entity.InwardDate = item.InwardDate;
                    list.Add(entity);
                }
            }
            return list;
        }
        public List<SP_GetInventoryDetailsEntity> GetInventoryDetails()
        {
            List<SP_GetInventoryDetailsEntity> list = new List<SP_GetInventoryDetailsEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.SP_GetInventoryDetails().ToList();
                foreach (var item in records)
                {
                    SP_GetInventoryDetailsEntity entity = new SP_GetInventoryDetailsEntity();
                    entity.InwardFrom = item.InwardFrom;
                    entity.ItemName = item.ItemName;                  
                    entity.ItemQty = item.ItemQty;
                    entity.BalanceQty = item.BalanceQty;
                    entity.ItemUnit = item.ItemUnit;
                    list.Add(entity);
                }
            }
            return list;

        }

    }
}
