﻿using Garments_ERP.Data.Data;
using Garments_ERP.Entity;
using Garments_ERP.Entity.Accounts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Garments_ERP.Data.Classes.Accounts
{
    public class cl_CounterSales
    {
        public List<Acc_Voucher_Customer_Details_Entity> Get_NameContact(int companyid, int branchid, string inputStr)
        {
            List<Acc_Voucher_Customer_Details_Entity> list = new List<Acc_Voucher_Customer_Details_Entity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.sp_entity_mobile_get(companyid, branchid, inputStr).ToList();
                if (true)
                {
                    foreach (var item in records)
                    {
                        Acc_Voucher_Customer_Details_Entity entity = new Acc_Voucher_Customer_Details_Entity();
                        entity.Customer_ID = item.Customer_ID;
                        entity.Customer_Mob_No = item.Customer_Mob_No;
                        entity.NameContact = item.NameContact;
                        entity.CustomerName = item.CustomerName;
                        list.Add(entity);
                    }
                }
            }
            return list;
        }

        public List<M_ItemMasterEntity> Get_ItemsWithHSN(int companyid, int branchid, string inputStr)
        {
            List<M_ItemMasterEntity> list = new List<M_ItemMasterEntity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.sp_get_items_autoextender(companyid, branchid, inputStr).ToList();
                if (true)
                {
                    foreach (var item in records)
                    {
                        M_ItemMasterEntity entity = new M_ItemMasterEntity();
                        entity.Id = item.Item_ID;
                        entity.Item_HSN = item.Item_HSN;
                        entity.ItemName = item.ItemName;
                        entity.Taxrate = item.Taxrate;
                        entity.HScode = item.HScode;
                        entity.OpeningStock = item.OpeningStock;
                        list.Add(entity);
                    }
                }
            }
            return list;
        }

        public string getnextcountersalesno()
        {
            string no;
            no = "CS-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.Acc_Voucher.Count() > 0 ? db.Acc_Voucher.Max(x => x.Voucher_ID) + 1 : 1;
                no = no + "-" + record.ToString();
            }

            return no;
        }
        public Int32 SaveCounterSales(Acc_Voucher_Entity voucher_, List<Acc_Voucher_Items_Detail_Entity> voucheritems_, Acc_Voucher_Customer_Details_Entity vouchercustomer_)
        {
            int voucherId = 0;
            try
            {
                using (var db = new GarmentERPDBEntities())
                {
                    Acc_Voucher item = new Acc_Voucher();
                    item.Voucher_ID = voucher_.Voucher_ID;
                    item.Voucher_No = voucher_.Voucher_No;
                    item.Voucher_Amount = voucher_.Voucher_Amount;
                    item.Is_Deleted = false;
                    item.Is_Cancelled = false;
                    item.Voucher_Type_ID = voucher_.Voucher_Type_ID;
                    item.Narration = voucher_.Narration;
                    item.Approved_or_Rejected_By = voucher_.Approved_or_Rejected_By;
                    item.Approved_or_Rejected_Date = voucher_.Approved_or_Rejected_Date;
                    item.Branch_Id = voucher_.Branch_Id;
                    if (voucher_.Voucher_ID > 0)
                    {
                        var record = db.Acc_Voucher.Where(x => x.Voucher_ID == voucher_.Voucher_ID).FirstOrDefault();
                        record.Voucher_Amount = voucher_.Voucher_Amount;
                        record.Narration = voucher_.Narration;
                        record.Modified_By = voucher_.Created_By;
                        record.Modified_Date = DateTime.Now;
                        //db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        //db.Configuration.ValidateOnSaveEnabled = true;
                    }
                    else
                    {
                        item.Voucher_Date = voucher_.Voucher_Date;
                        item.Created_By = voucher_.Created_By;
                        item.Created_Date = DateTime.Now;
                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.Acc_Voucher.Add(item);
                        db.SaveChanges();
                        db.Configuration.ValidateOnSaveEnabled = true;
                    }
                    voucherId = item.Voucher_ID;

                 
                    foreach (var item_ in voucheritems_)
                    {
                        Acc_Voucher_Items_Detail itemRecord = new Acc_Voucher_Items_Detail();
                        itemRecord.Voucher_ID = voucherId;
                        itemRecord.Voucher_Item_Detail_ID = item_.Voucher_Item_Detail_ID;
                        itemRecord.Item_ID = item_.Item_ID;
                        itemRecord.Item_Description = item_.Item_Description;
                        itemRecord.Item_UOM_ID = item_.Item_UOM_ID;
                        itemRecord.Barcode = item_.Barcode;
                        itemRecord.Hsncode = item_.Hsncode;
                        itemRecord.Sales_Qty = item_.Sales_Qty;
                        itemRecord.Rate = item_.Rate;
                        itemRecord.Scheme_ID = item_.Scheme_ID;
                        itemRecord.Discount_Type_ID = item_.Discount_Type_ID;
                        itemRecord.Discount_Rate = item_.Discount_Rate;
                        itemRecord.Taxable_Amount = item_.Taxable_Amount;
                        itemRecord.GST_Rate = item_.GST_Rate;
                        itemRecord.GST_Amount = item_.GST_Amount;
                        itemRecord.Total_Amount = item_.Total_Amount;
                        if (item_.Voucher_Item_Detail_ID > 0)
                        {
                            var it_record = db.Acc_Voucher_Items_Detail.Where(x => x.Voucher_Item_Detail_ID == item_.Voucher_Item_Detail_ID).FirstOrDefault();
                            it_record.Item_ID = item_.Item_ID;
                            it_record.Item_Description = item_.Item_Description;
                            it_record.Item_UOM_ID = item_.Item_UOM_ID;
                            it_record.Barcode = item_.Barcode;
                            it_record.Hsncode = item_.Hsncode;
                            it_record.Sales_Qty = item_.Sales_Qty;
                            it_record.Rate = item_.Rate;
                            it_record.Scheme_ID = item_.Scheme_ID;
                            it_record.Discount_Type_ID = item_.Discount_Type_ID;
                            it_record.Discount_Rate = item_.Discount_Rate;
                            it_record.Taxable_Amount = item_.Taxable_Amount;
                            it_record.GST_Rate = item_.GST_Rate;
                            it_record.GST_Amount = item_.GST_Amount;
                            it_record.Total_Amount = item_.Total_Amount;
                            it_record.Modified_By = item_.Created_By;
                            it_record.Modified_Date = DateTime.Now;
                            db.SaveChanges();
                        }
                        else
                        {
                            itemRecord.Created_By = item_.Created_By;
                            itemRecord.Created_Date = DateTime.Now;
                            itemRecord.Is_Deleted = false;
                            db.Acc_Voucher_Items_Detail.Add(itemRecord);
                            db.SaveChanges();
                        }
                    }

                    Acc_Voucher_Customer_Details vCust_ = new Acc_Voucher_Customer_Details();
                    vCust_.Voucher_ID = voucherId;
                    vCust_.Voucher_Customer_Detail_ID = vouchercustomer_.Voucher_Customer_Detail_ID;
                    vCust_.Customer_Mob_No = vouchercustomer_.Customer_Mob_No;
                    vCust_.Customer_ID = vouchercustomer_.Customer_ID;
                    vCust_.Consignee_ID = vouchercustomer_.Consignee_ID;
                    vCust_.Consignee_Mob_No = vouchercustomer_.Consignee_Mob_No;
                    vCust_.Employee_ID = vouchercustomer_.Employee_ID;
                    vCust_.Payment_Settlement_ID = vouchercustomer_.Payment_Settlement_ID;
                    vCust_.Payment_Mode_ID = vouchercustomer_.Payment_Mode_ID;
                    vCust_.Cash_Amount = vouchercustomer_.Cash_Amount;
                    vCust_.Cheque_No = vouchercustomer_.Cheque_No;
                    vCust_.Cheque_Date = vouchercustomer_.Cheque_Date;
                    vCust_.Cheque_Amount = vouchercustomer_.Cheque_Amount;
                    vCust_.Bank_Name = vouchercustomer_.Bank_Name;
                    vCust_.Credit_Card_No = vouchercustomer_.Credit_Card_No;
                    vCust_.Credit_Card_Amount = vouchercustomer_.Credit_Card_Amount;
                    vCust_.Net_Banking_Amount = vouchercustomer_.Net_Banking_Amount;
                    vCust_.Net_Banking_Ref_No = vouchercustomer_.Net_Banking_Ref_No;
                    vCust_.Total_Amount = vouchercustomer_.Total_Amount;
                    vCust_.Remarks = vouchercustomer_.Remarks;
                    if (vouchercustomer_.Voucher_Customer_Detail_ID > 0)
                    {
                        var cut_record = db.Acc_Voucher_Customer_Details.Where(x => x.Voucher_Customer_Detail_ID == vouchercustomer_.Voucher_Customer_Detail_ID).FirstOrDefault();
                        cut_record.Customer_Mob_No = vouchercustomer_.Customer_Mob_No;
                        cut_record.Customer_ID = vouchercustomer_.Customer_ID;
                        cut_record.Consignee_ID = vouchercustomer_.Consignee_ID;
                        cut_record.Consignee_Mob_No = vouchercustomer_.Consignee_Mob_No;
                        cut_record.Employee_ID = vouchercustomer_.Employee_ID;
                        cut_record.Payment_Settlement_ID = vouchercustomer_.Payment_Settlement_ID;
                        cut_record.Payment_Mode_ID = vouchercustomer_.Payment_Mode_ID;
                        cut_record.Cash_Amount = vouchercustomer_.Cash_Amount;
                        cut_record.Cheque_No = vouchercustomer_.Cheque_No;
                        cut_record.Cheque_Date = vouchercustomer_.Cheque_Date;
                        cut_record.Cheque_Amount = vouchercustomer_.Cheque_Amount;
                        cut_record.Bank_Name = vouchercustomer_.Bank_Name;
                        cut_record.Credit_Card_No = vouchercustomer_.Credit_Card_No;
                        cut_record.Credit_Card_Amount = vouchercustomer_.Credit_Card_Amount;
                        cut_record.Net_Banking_Amount = vouchercustomer_.Net_Banking_Amount;
                        cut_record.Net_Banking_Ref_No = vouchercustomer_.Net_Banking_Ref_No;
                        cut_record.Total_Amount = vouchercustomer_.Total_Amount;
                        cut_record.Remarks = vouchercustomer_.Remarks;
                        cut_record.Modified_By = vouchercustomer_.Created_By;
                        cut_record.Modified_Date = DateTime.Now;
                        db.SaveChanges();
                    }
                    else
                    {
                        vCust_.Created_By = vouchercustomer_.Created_By;
                        vCust_.Created_Date = DateTime.Now;
                        db.Acc_Voucher_Customer_Details.Add(vCust_);
                        db.SaveChanges();
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
            return voucherId;
        }
        public List<Acc_Voucher_Entity> Get_AllCountersales(int branch_id)
        {
            List<Acc_Voucher_Entity> sales = new List<Acc_Voucher_Entity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.Acc_Voucher.ToList().Where(x => x.Is_Deleted == false && x.Branch_Id == branch_id);
                foreach (var item in records)
                {
                    Acc_Voucher_Entity entity = new Acc_Voucher_Entity();
                    entity.Voucher_ID = item.Voucher_ID;
                    entity.Voucher_No = item.Voucher_No;
                    entity.Created_By = item.Created_By;
                    M_UserEntity uentity = new M_UserEntity();
                    if (item.Created_By != null)
                    {
                        var users = db.M_UserMaster.Where(x => x.Id == item.Created_By).SingleOrDefault();
                        if (users != null)
                        {
                            uentity.Id = users.Id;
                            uentity.Username = users.Username;
                            entity.Userentity = uentity;
                        }
                    }
                    entity.Voucher_Type_ID = item.Voucher_Type_ID;
                    entity.Voucher_Date = item.Voucher_Date;
                    entity.Voucher_Amount = item.Voucher_Amount;
                    Acc_Voucher_Type_Entity ventity = new Acc_Voucher_Type_Entity();
                    if (item.Voucher_Type_ID != null)
                    {
                        var types = db.Acc_Voucher_Type.Where(x => x.Voucher_Type_ID == item.Voucher_Type_ID).SingleOrDefault();
                        if (types != null)
                        {
                            ventity.Voucher_Type_ID = types.Voucher_Type_ID;
                            ventity.Voucher_Type = types.Voucher_Type;
                            entity.Acc_Voucher_Type = ventity;
                        }
                    }
                    sales.Add(entity);
                }
            }
            return sales;

        }
        public bool Delete(int id)
        {
            bool isdelete = false;
            using (var db = new GarmentERPDBEntities())
            {
                var record = db.Acc_Voucher.Find(id);
                record.Is_Deleted = true;
                db.SaveChanges();
                isdelete = true;
            }
            return isdelete;
        }
        public Sales_Entity Get_Countersales_ById(int voucher_id, int branch_id)
        {
            Sales_Entity salesRecords = new Sales_Entity();
            salesRecords.Voucher = null;
            salesRecords.VoucherItems = null;
            salesRecords.Voucher_Customer = null;

            Acc_Voucher_Entity Vr_entity = new Acc_Voucher_Entity();
            Acc_Voucher_Customer_Details_Entity Cr_entity = new Acc_Voucher_Customer_Details_Entity();
            List<Acc_Voucher_Items_Detail_Entity> It_entList = new List<Acc_Voucher_Items_Detail_Entity>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.Acc_Voucher.Where(x => x.Is_Deleted == false && x.Branch_Id == branch_id && x.Voucher_ID == voucher_id);
                foreach (var item in records)
                {
                    Vr_entity.Voucher_ID = item.Voucher_ID;
                    Vr_entity.Voucher_No = item.Voucher_No;
                    Vr_entity.Created_By = item.Created_By;
                    Vr_entity.Voucher_Type_ID = item.Voucher_Type_ID;
                    if (item.Voucher_Date != null)
                    {
                        Vr_entity.Voucher_Date = Convert.ToDateTime(item.Voucher_Date).Date;
                        string stdate = Convert.ToDateTime(item.Voucher_Date).ToString("MM/dd/yyyy");
                        Vr_entity.vc_date = stdate;
                        //entity.Start_Date = item.Start_Date;
                    }
                    //Vr_entity.Voucher_Date = item.Voucher_Date;
                    //if (item.Voucher_Date != null)
                    //{
                    //    string date_ = item.Voucher_Date.ToString("DD/MM/YYYY");
                    //    Vr_entity.Voucher_Date =Convert.ToDateTime(date_);
                    //}
                    Vr_entity.Voucher_Amount = item.Voucher_Amount;
                    salesRecords.Voucher = Vr_entity;
                }
                var cr_records = db.Acc_Voucher_Customer_Details.Where(x => x.Voucher_ID == voucher_id);
                foreach (var item_ in cr_records)
                {
                    Cr_entity.Voucher_ID = item_.Voucher_ID;
                    Cr_entity.Voucher_Customer_Detail_ID = item_.Voucher_Customer_Detail_ID;
                    Cr_entity.Created_By = item_.Created_By;
                    Cr_entity.Customer_Mob_No = item_.Customer_Mob_No;
                    Cr_entity.Customer_ID = item_.Customer_ID;
                    if (item_.Customer_ID != null)
                    {
                        M_LedgersEntity lentity = new M_LedgersEntity();
                        var customers = db.M_Ledgers.Where(x => x.Ledger_Id == item_.Customer_ID).SingleOrDefault();
                        if (customers != null)
                        {
                            lentity.Ledger_Id = customers.Ledger_Id;
                            lentity.Ledger_Name = customers.Ledger_Name;
                        }
                        Cr_entity.Customer_Name = lentity.Ledger_Name;

                    }
                    Cr_entity.Consignee_ID = item_.Consignee_ID;
                    if (item_.Consignee_ID != null)
                    {
                        M_LedgersEntity centity = new M_LedgersEntity();
                        var consingnee = db.M_Ledgers.Where(x => x.Ledger_Id == item_.Consignee_ID).SingleOrDefault();
                        if (consingnee != null)
                        {
                            centity.Ledger_Id = consingnee.Ledger_Id;
                            centity.Ledger_Name = consingnee.Ledger_Name;
                        }
                        Cr_entity.Consignee_Name = centity.Ledger_Name;

                    }
                    Cr_entity.Consignee_Mob_No = item_.Consignee_Mob_No;
                    Cr_entity.Employee_ID = item_.Employee_ID;
                    Cr_entity.Payment_Settlement_ID = item_.Payment_Settlement_ID;
                    Cr_entity.Payment_Mode_ID = item_.Payment_Mode_ID;
                    Cr_entity.Cash_Amount = item_.Cash_Amount;
                    Cr_entity.Cheque_No = item_.Cheque_No;
                    Cr_entity.Cheque_Date = item_.Cheque_Date;
                    Cr_entity.Cheque_Amount = item_.Cheque_Amount;
                    Cr_entity.Bank_Name = item_.Bank_Name;
                    Cr_entity.Credit_Card_No = item_.Credit_Card_No;
                    Cr_entity.Credit_Card_Amount = item_.Credit_Card_Amount;
                    Cr_entity.Net_Banking_Amount = item_.Net_Banking_Amount;
                    Cr_entity.Net_Banking_Ref_No = item_.Net_Banking_Ref_No;
                    Cr_entity.Total_Amount = item_.Total_Amount;
                    Cr_entity.Remarks = item_.Remarks;
                    salesRecords.Voucher_Customer = Cr_entity;
                }
                var it_records = db.Acc_Voucher_Items_Detail.Where(x => x.Voucher_ID == voucher_id && x.Is_Deleted == false);
                foreach (var _item in it_records)
                {
                    Acc_Voucher_Items_Detail_Entity it_entity = new Acc_Voucher_Items_Detail_Entity();
                    it_entity.Voucher_ID = _item.Voucher_ID;
                    it_entity.Voucher_Item_Detail_ID = _item.Voucher_Item_Detail_ID;
                    it_entity.Barcode = _item.Barcode;
                    it_entity.Hsncode = _item.Hsncode;
                    it_entity.Item_ID = _item.Item_ID;
                    it_entity.Item_Description = _item.Item_Description;
                    it_entity.Item_UOM_ID = _item.Item_UOM_ID;
                    it_entity.Sales_Qty = _item.Sales_Qty;
                    it_entity.Rate = _item.Rate;
                    it_entity.Scheme_ID = _item.Scheme_ID;
                    it_entity.Discount_Type_ID = _item.Discount_Type_ID;
                    it_entity.Discount_Rate = _item.Discount_Rate;
                    it_entity.Taxable_Amount = _item.Taxable_Amount;
                    it_entity.GST_Rate = _item.GST_Rate;
                    it_entity.GST_Amount = _item.GST_Amount;
                    it_entity.Total_Amount = _item.Total_Amount;
                    It_entList.Add(it_entity);
                }
                salesRecords.VoucherItems = It_entList;
            }
            return salesRecords;

        }
    }
}
