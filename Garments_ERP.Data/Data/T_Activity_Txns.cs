//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Garments_ERP.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_Activity_Txns
    {
        public int Id { get; set; }
        public Nullable<int> Activity_ID { get; set; }
        public string Activity_Log { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<int> Entity_Type { get; set; }
        public Nullable<long> Entity_Reference_Id { get; set; }
        public Nullable<int> Department_Id { get; set; }
        public Nullable<int> Role_ID { get; set; }
        public Nullable<int> User_ID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> IsStatus { get; set; }
    }
}
