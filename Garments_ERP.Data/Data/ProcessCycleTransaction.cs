//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Garments_ERP.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProcessCycleTransaction
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProcessCycleTransaction()
        {
            this.M_ProcessInCategoryDetails = new HashSet<M_ProcessInCategoryDetails>();
        }
    
        public long Id { get; set; }
        public Nullable<int> Processcycleid { get; set; }
        public Nullable<decimal> Processid { get; set; }
        public Nullable<int> Processsequence { get; set; }
        public Nullable<long> ItemcategoryIn { get; set; }
        public Nullable<long> ItemcategoryOut { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string comment { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
    
        public virtual M_ItemCategoryMaster M_ItemCategoryMaster { get; set; }
        public virtual M_ItemCategoryMaster M_ItemCategoryMaster1 { get; set; }
        public virtual M_PROCESS_Master M_PROCESS_Master { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<M_ProcessInCategoryDetails> M_ProcessInCategoryDetails { get; set; }
    }
}
