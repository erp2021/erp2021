//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Garments_ERP.Data.Data
{
    using System;
    
    public partial class SP_Quotation_report_Result
    {
        public string QuotationNo { get; set; }
        public string QuoteDate { get; set; }
        public string Tentivedate { get; set; }
        public string ItemName { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> Totalqty { get; set; }
        public Nullable<decimal> SalePrice { get; set; }
        public Nullable<decimal> Tax { get; set; }
        public Nullable<decimal> Subtotal { get; set; }
        public Nullable<decimal> Total { get; set; }
        public string Ledger_Name { get; set; }
        public string Address { get; set; }
        public string Contact_No { get; set; }
        public string Email { get; set; }
        public string Company_Name { get; set; }
        public string Head_Office_Add { get; set; }
        public string EmailID { get; set; }
        public string Mobile_No { get; set; }
    }
}
