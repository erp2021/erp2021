//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Garments_ERP.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class M_ItemCategoryMaster
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public M_ItemCategoryMaster()
        {
            this.CRM_EnquiryRawitemDetail = new HashSet<CRM_EnquiryRawitemDetail>();
            this.CRM_QuotationRawItemDetail = new HashSet<CRM_QuotationRawItemDetail>();
            this.M_ItemMaster = new HashSet<M_ItemMaster>();
            this.M_ItemMaster1 = new HashSet<M_ItemMaster>();
            this.M_ItemSubCategoryMaster = new HashSet<M_ItemSubCategoryMaster>();
            this.M_PROCESS_Master = new HashSet<M_PROCESS_Master>();
            this.ProcessCycleTransactions = new HashSet<ProcessCycleTransaction>();
            this.ProcessCycleTransactions1 = new HashSet<ProcessCycleTransaction>();
        }
    
        public long Id { get; set; }
        public string Itemcategory { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<int> Itemgroupid { get; set; }
        public string Itemcategorydesc { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CRM_EnquiryRawitemDetail> CRM_EnquiryRawitemDetail { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CRM_QuotationRawItemDetail> CRM_QuotationRawItemDetail { get; set; }
        public virtual M_ItemGroupMaster M_ItemGroupMaster { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<M_ItemMaster> M_ItemMaster { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<M_ItemMaster> M_ItemMaster1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<M_ItemSubCategoryMaster> M_ItemSubCategoryMaster { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<M_PROCESS_Master> M_PROCESS_Master { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProcessCycleTransaction> ProcessCycleTransactions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProcessCycleTransaction> ProcessCycleTransactions1 { get; set; }
    }
}
