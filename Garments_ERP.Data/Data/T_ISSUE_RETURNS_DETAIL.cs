//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Garments_ERP.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_ISSUE_RETURNS_DETAIL
    {
        public decimal ID { get; set; }
        public Nullable<decimal> RETURNS_ID { get; set; }
        public Nullable<decimal> ISSUED_QTY { get; set; }
        public Nullable<decimal> RETURNED_QTY { get; set; }
        public string UNIT { get; set; }
        public string ITEM_STATUS { get; set; }
        public Nullable<decimal> ITEM_ID { get; set; }
        public Nullable<decimal> ISSUE_ID { get; set; }
        public Nullable<decimal> INVENTORY_ID { get; set; }
        public Nullable<System.DateTime> RETURN_DATE { get; set; }
        public Nullable<decimal> BEFORE_WEIGHT { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> ITEM_ATTRIBUTE_ID { get; set; }
        public string HSN_CODE { get; set; }
        public Nullable<decimal> PO_TRANSFER_QTY { get; set; }
        public Nullable<long> BATCH_LOT_ID { get; set; }
        public Nullable<decimal> BALANCED_QTY { get; set; }
    }
}
