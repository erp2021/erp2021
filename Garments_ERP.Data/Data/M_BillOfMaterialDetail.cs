//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Garments_ERP.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class M_BillOfMaterialDetail
    {
        public long ID { get; set; }
        public long BOM_ID { get; set; }
        public Nullable<int> ItemSubCategoryId { get; set; }
        public Nullable<long> ItemId { get; set; }
        public string ItemDescription { get; set; }
        public Nullable<long> SupplierId { get; set; }
        public Nullable<int> UnitId { get; set; }
        public Nullable<decimal> RequiredQty { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Comment { get; set; }
        public Nullable<decimal> PerQty { get; set; }
        public Nullable<int> Item_Attribute_ID { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
    
        public virtual M_BillOfMaterialMaster M_BillOfMaterialMaster { get; set; }
        public virtual M_ItemMaster M_ItemMaster { get; set; }
        public virtual M_ItemSubCategoryMaster M_ItemSubCategoryMaster { get; set; }
        public virtual M_UnitMaster M_UnitMaster { get; set; }
    }
}
