//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Garments_ERP.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class T_ISSUE_ITEM_DETAILS
    {
        public decimal ID { get; set; }
        public Nullable<decimal> ISSUE_ITEM_ID { get; set; }
        public Nullable<decimal> INVENTORY_ITEM_DETAIL_ID { get; set; }
        public Nullable<decimal> ISSUED_QTY { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
    }
}
