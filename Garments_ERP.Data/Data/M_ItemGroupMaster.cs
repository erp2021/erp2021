//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Garments_ERP.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class M_ItemGroupMaster
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public M_ItemGroupMaster()
        {
            this.M_ItemCategoryMaster = new HashSet<M_ItemCategoryMaster>();
        }
    
        public int Id { get; set; }
        public string Itemgroup { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Itemgroupdesc { get; set; }
        public Nullable<System.DateTime> Createddate { get; set; }
        public Nullable<int> Createdby { get; set; }
        public Nullable<System.DateTime> Updateddate { get; set; }
        public Nullable<int> Updatedby { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<M_ItemCategoryMaster> M_ItemCategoryMaster { get; set; }
    }
}
