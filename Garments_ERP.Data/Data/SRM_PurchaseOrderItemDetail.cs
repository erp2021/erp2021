//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Garments_ERP.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class SRM_PurchaseOrderItemDetail
    {
        public long Id { get; set; }
        public Nullable<long> PO_ID { get; set; }
        public Nullable<long> ItemId { get; set; }
        public string ItemDesc { get; set; }
        public Nullable<int> UnitId { get; set; }
        public Nullable<decimal> TotalQty { get; set; }
        public Nullable<decimal> Provision { get; set; }
        public Nullable<decimal> Wastage { get; set; }
        public Nullable<decimal> StockQty { get; set; }
        public Nullable<decimal> POQty { get; set; }
        public Nullable<decimal> Rate { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> RateIndicatorId { get; set; }
        public Nullable<int> TaxId { get; set; }
        public Nullable<decimal> TaxValue { get; set; }
        public Nullable<int> Item_Attribute_ID { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
    
        public virtual M_ItemMaster M_ItemMaster { get; set; }
        public virtual M_RateIndicatorMaster M_RateIndicatorMaster { get; set; }
        public virtual M_UnitMaster M_UnitMaster { get; set; }
        public virtual SRM_PurchaseOrderMaster SRM_PurchaseOrderMaster { get; set; }
    }
}
