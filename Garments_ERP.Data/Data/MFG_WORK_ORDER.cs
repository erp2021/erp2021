//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Garments_ERP.Data.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class MFG_WORK_ORDER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MFG_WORK_ORDER()
        {
            this.FG_TransferMaster = new HashSet<FG_TransferMaster>();
            this.MFG_ALLOCATION = new HashSet<MFG_ALLOCATION>();
            this.MFG_MaterialIssueMaster = new HashSet<MFG_MaterialIssueMaster>();
            this.MFG_PRODUCTION_PLAN = new HashSet<MFG_PRODUCTION_PLAN>();
            this.MFG_WorkOrderProcessDetails = new HashSet<MFG_WorkOrderProcessDetails>();
            this.MFG_WORK_ORDER_DETAIL = new HashSet<MFG_WORK_ORDER_DETAIL>();
        }
    
        public long WorkOrderID { get; set; }
        public long CustomerID { get; set; }
        public long POID { get; set; }
        public long StyleID { get; set; }
        public Nullable<long> EnquiryID { get; set; }
        public Nullable<int> ProcessCycleID { get; set; }
        public string WorkOrderNo { get; set; }
        public Nullable<long> PlanID { get; set; }
        public string StyleType { get; set; }
        public Nullable<long> DepartmentID { get; set; }
        public Nullable<decimal> RevisionNo { get; set; }
        public Nullable<decimal> RequiredQty { get; set; }
        public Nullable<int> UnitID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> Approved { get; set; }
        public Nullable<System.DateTime> ApproveDate { get; set; }
        public string Comment { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsMIdone { get; set; }
        public Nullable<int> Company_ID { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> IsStatus { get; set; }
    
        public virtual CRM_CustomerPOMaster CRM_CustomerPOMaster { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FG_TransferMaster> FG_TransferMaster { get; set; }
        public virtual M_ProcessCycleMaster M_ProcessCycleMaster { get; set; }
        public virtual M_StyleMaster M_StyleMaster { get; set; }
        public virtual M_UnitMaster M_UnitMaster { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MFG_ALLOCATION> MFG_ALLOCATION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MFG_MaterialIssueMaster> MFG_MaterialIssueMaster { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MFG_PRODUCTION_PLAN> MFG_PRODUCTION_PLAN { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MFG_WorkOrderProcessDetails> MFG_WorkOrderProcessDetails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MFG_WORK_ORDER_DETAIL> MFG_WORK_ORDER_DETAIL { get; set; }
    }
}
