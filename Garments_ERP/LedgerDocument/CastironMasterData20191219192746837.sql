USE [BALCODBV1]
GO
SET IDENTITY_INSERT [dbo].[tbl_Cast_Iron_Analysis_Master] ON 

INSERT [dbo].[tbl_Cast_Iron_Analysis_Master] ([Id], [Parameter_Name], [Specification], [DepartmentId], [DataGroup], [IsDeleted], [CreatedOn], [UpdatedOn], [CreatedBy], [UpdatedBy]) 
VALUES (1, N'C', N'3.3 - 3.7 %', NULL, NULL, 0, CAST(N'2019-12-19T00:00:00.000' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tbl_Cast_Iron_Analysis_Master] ([Id], [Parameter_Name], [Specification], [DepartmentId], [DataGroup], [IsDeleted], [CreatedOn], [UpdatedOn], [CreatedBy], [UpdatedBy]) 
VALUES (2, N'Mn', N'0.6 - 0.9 %', NULL, NULL, 0, CAST(N'2019-12-19T00:00:00.000' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tbl_Cast_Iron_Analysis_Master] ([Id], [Parameter_Name], [Specification], [DepartmentId], [DataGroup], [IsDeleted], [CreatedOn], [UpdatedOn], [CreatedBy], [UpdatedBy]) 
VALUES (3, N'P', N'0.5% max.', NULL, NULL, 0, CAST(N'2019-12-19T00:00:00.000' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tbl_Cast_Iron_Analysis_Master] ([Id], [Parameter_Name], [Specification], [DepartmentId], [DataGroup], [IsDeleted], [CreatedOn], [UpdatedOn], [CreatedBy], [UpdatedBy]) 
VALUES (4, N'S', N'0.2 % max', NULL, NULL, 0, CAST(N'2019-12-19T00:00:00.000' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tbl_Cast_Iron_Analysis_Master] ([Id], [Parameter_Name], [Specification], [DepartmentId], [DataGroup], [IsDeleted], [CreatedOn], [UpdatedOn], [CreatedBy], [UpdatedBy]) 
VALUES (5, N'Si', N'2.7 - 3.2 %', NULL, NULL, 0, CAST(N'2019-12-19T00:00:00.000' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tbl_Cast_Iron_Analysis_Master] ([Id], [Parameter_Name], [Specification], [DepartmentId], [DataGroup], [IsDeleted], [CreatedOn], [UpdatedOn], [CreatedBy], [UpdatedBy]) 
VALUES (6, N'C.E', N'4.3 - 4.7', NULL, NULL, 0, CAST(N'2019-12-19T00:00:00.000' AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Cast_Iron_Analysis_Master] OFF
