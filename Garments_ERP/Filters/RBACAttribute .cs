﻿using Garments_ERP.Data.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Garments_ERP.Filters
{
    public class RBACAttribute : AuthorizeAttribute  
    {


        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            /*Create permission string based on the requested controller 
              name and action name in the format 'controllername-action'*/
            var controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var actionname = filterContext.ActionDescriptor.ActionName;

            string requiredPermission = String.Format("{0}-{1}",
                   filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                   filterContext.ActionDescriptor.ActionName);

            if (System.Web.HttpContext.Current.Session["LoggedUser"] == null)
            {
                filterContext.Result = new RedirectResult("~/Login/Index");
               
            }
            else
            {
                var roleId = Helper.Helper.CurrentLoggedUser.RoleID;
                long RId = Convert.ToInt64(roleId);
                cl_AssignScreen obj = new cl_AssignScreen();
                var status = false;
                if (controller != "DashBoard" && controller!= "ProductionPlan")
                {
                    var isrecord = obj.GetScreenData(RId).Where(x => x.CHKsatus == true).ToList();
                    if (isrecord == null)
                    {
                        status = false;
                    }
                    else
                    {
                        foreach (var data in isrecord)
                        {
                            foreach (var scrdt in data.ScreenList)
                            {
                                int len = scrdt.ScreenList.Count();
                                if (len > 0)
                                {
                                    foreach (var listdata in scrdt.ScreenList)
                                    {
                                        if (listdata.URL == controller)
                                        {
                                            status = true;
                                            System.Web.HttpContext.Current.Session["ScreenId"] = listdata.screenId;
                                        }
                                    }
                                }
                                else
                                {
                                    if (scrdt.URL == controller)
                                    {
                                        status = true;
                                        System.Web.HttpContext.Current.Session["ScreenId"] = scrdt.screenId;
                                    }
                                }
                            }
                        }
                    }

                    if (status == false)
                    {
                        filterContext.Result = new RedirectToRouteResult(
                                                             new RouteValueDictionary {
                                                { "action", "Index" },
                                                { "controller", "Unauthorised" } });

                    }
                }
            }
            /*Create an instance of our custom user authorisation object passing requesting 
              user's 'Windows Username' into constructor*/
         //   RBACUser requestingUser = new RBACUser(filterContext.RequestContext.HttpContext.User.Identity.Name);

            //Check if the requesting user has the permission to run the controller's action
          //  if (!requestingUser.HasPermission(requiredPermission) & !requestingUser.IsSystemAdmin)
          //  {
                /*User doesn't have the required permission and is not a SysAdmin, return our 
                  custom '401 Unauthorized' access error. Since we are setting 
                  filterContext.Result to contain an ActionResult page, the controller's 
                  action will not be run.

                  The custom '401 Unauthorized' access error will be returned to the 
                  browser in response to the initial request.*/
                
         //   }
            /*If the user has the permission to run the controller's action, then 
              filterContext.Result will be uninitialized and executing the controller's 
              action is dependant on whether filterContext.Result is uninitialized.*/
        }


    }
}