﻿
$(document).ready(function () {
    $('.select2').select2();
    $('.select2').width("100%");

});


$('#saveWarehouse').on('click', function () {
    var WAREHOUSE_TYPE_ID = $("#WAREHOUSE_TYPE_ID").val();
    var WAREHOUSE_ROLE = $('#WAREHOUSE_ROLE').val();
    var SHORT_NAME = $('#SHORT_NAME').val();
    var OWNER_ENTITY_ID = $('#OWNER_ENTITY_ID').val();
    var OWNER_DEPT_ID = $('#OWNER_DEPT_ID').val();
  
    var isvalid = true;
    //validations of Rack details 
    if (WAREHOUSE_TYPE_ID == "" || WAREHOUSE_TYPE_ID == 0) {
        swal("Please select warehouse type");
        isvalid = false;
    }
    //else if (WAREHOUSE_ROLE == "" || WAREHOUSE_ROLE == 0) {
    //    swal("Please select warehouse role");
    //    isvalid = false;
    //}
    else if (SHORT_NAME == "") {
        swal("Please enter warehouse name");
        isvalid = false;
    }
    else if (OWNER_ENTITY_ID == "" || OWNER_ENTITY_ID == 0) {
        swal("Please select owner of warehouse");
        isvalid = false;
    }
    else if (OWNER_DEPT_ID == "" || OWNER_DEPT_ID == 0) {
        swal("Please select owner department");
        isvalid = false;
    }
    return isvalid;
});