﻿function Editstylesizedetail(id) {
    //alert("in editstylesize" + id);
    var number = id.match(/\d+/);
    var total = 0;
    var sizeper_ele = $('#sizeper' + number).val();
    var sizepcs_ele = $('#sizepcs' + number).val();
    var reqty = $('#reqqty').val();
    if (reqty == "") {
        // $('#qtymsg').text();
        swal("enter require qty.");



    }
    else {
        $('#qtymsg').text("");
        if (sizepcs_ele != "") {
            var sizeper = (parseFloat(sizepcs_ele) * 100) / parseFloat(reqty);
            $('#sizeper' + number).val(parseFloat(sizeper).toFixed(2));

            $('#pertd' + number).attr("sizedata", parseFloat(sizeper).toFixed(2));
            $('#pcstd' + number).attr("sizedata", parseFloat(sizepcs_ele).toFixed(2));
            // $('#sizeper' + number).attr('sizedata') = parseFloat(sizeper).toFixed(2);
        }
        else {

            var sizepcs = (parseFloat(sizeper_ele) * parseFloat(reqty) / 100);
            $('#sizepcs' + number).val(parseFloat(sizepcs).toFixed(2));

            $('#pcstd' + number).attr("sizedata", parseFloat(sizepcs).toFixed(2));
            $('#pertd' + number).attr("sizedata", parseFloat(sizeper_ele).toFixed(2));
            //  $('#sizepcs' + number).attr('sizedata') = parseFloat(sizepcs).toFixed(2);
        }
    }
}


function editsizeper(id) {
    //alert("in editstylesize" + id);
    var number = id.match(/\d+/);
    var total = 0;
    var sizeper_ele = $('#sizeper' + number).val();
    var sizepcs_ele = $('#sizepcs' + number).val();
    var reqty = $('#reqqty').val();
    if (reqty == "") {
        // $('#qtymsg').text();
        swal("enter require qty.");

    }

    else {
        $('#qtymsg').text("");
        if (sizeper_ele != "") {
            if (parseFloat(sizeper_ele) <= 100) {
                var sizepcs = (parseFloat(sizeper_ele) * parseFloat(reqty) / 100);
                $('#sizepcs' + number).val(parseFloat(sizepcs).toFixed(2));

                $('#pcstd' + number).attr("sizedata", parseFloat(sizepcs).toFixed(2));
                $('#pertd' + number).attr("sizedata", parseFloat(sizeper_ele).toFixed(2));
            }
            else {
                swal("enter valid percentage");
            }
        }
        // $('#sizepcs' + number).attr('sizedata') = parseFloat(sizepcs).toFixed(2);
        //}
    }
}

function editsizepcs(id) {
    var number = id.match(/\d+/);
    var total = 0;
    var sizeper_ele = $('#sizeper' + number).val();
    var sizepcs_ele = $('#sizepcs' + number).val();
    var reqty = $('#reqqty').val();
    if (reqty == "") {
        // $('#qtymsg').text();
        swal("enter require qty.");

    }
    else {
        // $('#qtymsg').text("");
        if (sizepcs_ele != "") {

            if (parseFloat(sizepcs_ele) <= parseFloat(reqty)) {
                var sizeper = (parseFloat(sizepcs_ele) * 100) / parseFloat(reqty);
                $('#sizeper' + number).val(parseFloat(sizeper).toFixed(2));

                $('#pertd' + number).attr("sizedata", parseFloat(sizeper).toFixed(2));
                $('#pcstd' + number).attr("sizedata", parseFloat(sizepcs_ele).toFixed(2));
            }
            else {
                swal("enter valid quantity");
            }
        }

    }

}



function validrawitem() {
    var valid = true;

    var itemcategoryid = $("#itemcategoryid").val();
    var itemsubcategoryid = $("#itemsubcategoryid").val();


    $('#rawitemtable tr').each(function () {

        if ($(this).find('.validitemcatrow').val() == itemcategoryid && $(this).find('.validitemsubrow').val() == itemsubcategoryid) {
            valid = false;
        }
    });

    return valid;
}


