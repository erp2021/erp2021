﻿
//get autocomplete for CustomerWithMobileNo

//Code added by ramesh on 10-Jan2020
$("#NameContact").autocomplete({
    source: function (request, response) {

        //var inputs = request.term;
        var inp = $("#NameContact").val();
        $.ajax({
            url: '/Sales/Get_NameContact/',
            //data: JSON.stringify(inp),
            data: "{'MyJson':'" + inp + "'}",
            //data: "{ 'MyJson': '" + JSON.stringify(inp) + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                debugger;
                //data = JSON.parse(data);
                response($.map(data, function (item) {
                    //return { label: item.NameContact, value: item.NameContact, dataid=item.Customer_ID };
                    return { label: item.NameContact, value: item.NameContact, dataid: item.Customer_ID };
                }));

            },
            error: function (response) {
                alert(response.responseText);
            },
            failure: function (response) {
                alert(response.responseText);
            }
        });
    },
    select: function (e, i) {
        var str = i.item.value;
        var res = str.split("-");
        var custMob = res[0];
        var custName = res[1];
        $("#Customer_Mob_No").val(custMob);
        $("#CustomerName").val(custName);
        $("#Customer_ID").val(i.item.dataid);
    },
    minLength: 1
});

$("#ConsigneeMob").autocomplete({
    source: function (request, response) {

        //var inputs = request.term;
        var inp = $("#ConsigneeMob").val();
        $.ajax({
            url: '/Sales/Get_NameContact/',
            //data: JSON.stringify(inp),
            data: "{'MyJson':'" + inp + "'}",
            //data: "{ 'MyJson': '" + JSON.stringify(inp) + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                debugger;
                //data = JSON.parse(data);
                response($.map(data, function (item) {
                    //return { label: item.NameContact, value: item.NameContact, dataid=item.Customer_ID };
                    return { label: item.NameContact, value: item.NameContact, dataid: item.Customer_ID };
                }));

            },
            error: function (response) {
                alert(response.responseText);
            },
            failure: function (response) {
                alert(response.responseText);
            }
        });
    },
    select: function (e, i) {
        var str = i.item.value;
        var res = str.split("-");
        var custMob = res[0];
        var custName = res[1];
        $("#Consignee_Mob_No").val(custMob);
        $("#ConsigneeName").val(custName);
        $("#Consignee_ID").val(i.item.dataid);
    },
    minLength: 1
});

$("#ScanBarcode").autocomplete({

    source: function (request, response) {
        var itemArray = [];
        //var inputs = request.term;
        var inp = $("#ScanBarcode").val();
        $.ajax({
            url: '/Sales/Get_ItemsWithHSN/',
            //data: JSON.stringify(inp),
            data: "{'MyJson':'" + inp + "'}",
            //data: "{ 'MyJson': '" + JSON.stringify(inp) + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                debugger;
                //data = JSON.parse(data);
                response($.map(data, function (item) {
                    var itemDetails_ = {
                        'Item_ID': item.Id,
                        'ItemName': item.ItemName,
                        'Taxrate': item.Taxrate,
                        'HScode': item.HScode,
                        'OpeningStock': item.OpeningStock
                    };
                    itemArray.push(itemDetails_);
                    //return { label: item.NameContact, value: item.NameContact, dataid=item.Customer_ID };
                    return { label: item.Item_HSN, value: item.Item_HSN, dataid: item.Id, data: itemArray };

                }));

            },
            error: function (response) {
                alert(response.responseText);
            },
            failure: function (response) {
                alert(response.responseText);
            }
        });
    },
    select: function (e, i) {
        var _items = i.item.data.filter(function (value, index, array) {
            return value.Item_ID === i.item.dataid;
        });
        $.each(_items, function (key, rval) {
            $('#Item_ID').attr('data-item-name', rval.ItemName);
            $('#Item_ID').attr('data-item-taxrate', rval.Taxrate);
            $('#Item_ID').attr('data-item-hscode', rval.HScode);
            $('#Item_ID').attr('data-item-avlqty', rval.OpeningStock);
        });
        $("#Item_ID").val(i.item.dataid);
    },
    minLength: 1
});

//Code to add sales items into grid
function addItemRow() {

    var itemID = $("#Item_ID").val();
    var itemName = $('#Item_ID').attr('data-item-name');
    if (itemName == "" || itemName == undefined) {
        itemName = $("#ScanBarcode").val();
    }
    var itemTaxrate = $('#Item_ID').attr('data-item-taxrate');
    if (itemTaxrate == undefined) {
        itemTaxrate = '';
    }
    var itemHsCode = $('#Item_ID').attr('data-item-hscode');
    if (itemHsCode == undefined) {
        itemHsCode = '';
    }
    var itemAvlqty = $('#Item_ID').attr('data-item-avlqty');
    var rowCnt = 0;
    var $rows = $('#SalesBillItemsList').find('tbody tr');
    if ($rows.length > 0) {
        $.each($rows, function (key, row) {
            var item_id = $(row).find('[name=txtItem_ID]').val();
            if (itemID == item_id) {
                swal("warning", "Item already exists in grid", "warning");
            }
        });
    }
   
    if (itemName != "" || itemName != undefined) {
        var strLen = itemName.length;
        if (strLen >= 3) {
            var $rows_ = $('#SalesBillItemsList').find('tbody tr');
            if ($rows_.length > 0) {
                rowCnt = $rows_.length;
                rowCnt = rowCnt + 1;
            }
            else {
                rowCnt = rowCnt + 1;
            }
            var tblrow = '<tr id="rowid' + rowCnt + '">' +
                '<td class="action text-center" style="width: 20px"><input type="hidden" id="Voucher_Item_Detail_ID' + rowCnt + '" class="form-control" name="Voucher_Item_Detail_ID" value="0"/><a id="' + rowCnt + '" class="fa fa-trash" onclick="removeRow(' + rowCnt + ');" aria-hidden="true"></a></td>' +
                '<td class="Image"><img src="" class="" style="border:1px"/></td>' +
                '<td class="Barcode"><input id="txtBarcode' + rowCnt + '" type="text" name="txtBarcode" class="form-control" autocomplete="off" style="width: 100px;"></td>' +
                '<td class="HSNCode"><input id="txtHsnCode' + rowCnt + '" value="' + itemHsCode + '" type="text" name="txtHsnCode" class="form-control" autocomplete="off" style="width: 100px;"></td>' +
                '<td class="Item"> <input type="hidden" id="txtItem_ID' + rowCnt + '" class="form-control" name="txtItem_ID" value="' + itemID + '"/><input id="txtItem' + rowCnt + '" value="' + itemName + '" type="text" name="txtItem" class="form-control ui-autocomplete-input" autocomplete="off" style="width:200px;"></td>' +
                '<td class="UnitCode"><input type="hidden" id="txtItemUOM' + rowCnt + '" class="form-control" name="txtItemUOM" value="' + itemID + '"/><input id="txtUnitCode' + rowCnt + '" type="text" name="txtUnitCode" readonly="true" class="form-control ui-autocomplete-input" autocomplete="off" style="width: 100px;"></td>' +
                '<td class="qty"><input id="txtSalesQty' + rowCnt + '" type="text" name="txtSalesQty" data-avl-qty="' + itemAvlqty + '" onchange="calLineTotal(' + rowCnt + ')" class="form-control text-right" autocomplete="off" style="width: 100px;" onkeypress="return isNumberKey(event,this.id)" value="0" placeholder="Qty"></td>' +
                '<td class="rate"><input id="txtRate' + rowCnt + '" type="text" name="txtRate" onchange="calLineTotal(' + rowCnt + ')" class="form-control text-right" onkeypress="return isNumberKey(event,this.id)" autocomplete="off" style="width: 100px;" placeholder="Rate"></td>' +
                '<td class="scheme"><select id="ddlScheme' + rowCnt + '" name="ddlScheme" class="form-control select2" style="width:100px;"><option value="0">--Select--</option></select></td>' +
                '<td class="discount"><select id="ddlDiscount' + rowCnt + '" name="ddlDiscount" class="form-control select2" style="width: 100px;"></select></td>' +
                '<td class="discRate"><input id="txtDiscountRate' + rowCnt + '" type="text" name="txtDiscountRate" onchange="calLineTotal(' + rowCnt + ')" value="0" class="form-control text-right" onkeypress="return isNumberKey(event,this.id)" style="width: 100px;"></td>' +
                '<td class="taxableVal"><input id="txtTaxableval' + rowCnt + '" type="text" name="txtTaxableval" value="0" class="form-control text-right" readonly="true" style="width: 100px;"></td>' +
                '<td class="GSTrate"><input id="txtGSTRate' + rowCnt + '" type="text" name="txtGSTRate" onchange="calLineTotal(' + rowCnt + ')" value="' + itemTaxrate + '" class="form-control text-right" style="width: 100px;"></td>' +
                '<td class="gstAmt"><input id="txtGSTAmount' + rowCnt + '" type="text" name="txtGSTAmount" value="0" class="form-control text-right" readonly="true" style="width: 100px;"></td>' +
                '<td class="linetotal"><input id="txttotal' + rowCnt + '" name="txttotal" type="text text-right" value="0" class="form-control text-right rowtotal_" readonly="true" style="width: 100px;"></td>' +
                '</tr>';
            if (rowCnt == 1) {
                $('#SalesBillItemsList').append(tblrow);
            } else {
                $('#SalesBillItemsList > tbody > tr:first').before(tblrow);
            }
            getDiscountType(rowCnt);
            $("#txtBarcode" + rowCnt).focus();
            //calNewOutstanding(rowCnt);   

        }
    }

    $("#ScanBarcode").val('');
    $("#Item_ID").val('');
    $('#Item_ID').attr('data-item-name', '');
    $('#Item_ID').attr('data-item-taxrate', 0);
    $('#Item_ID').attr('data-item-hscode', '');
    $('#Item_ID').attr('data-item-avlqty', '');
}

//populateUOM
function getDiscountType(id) {
    var ddluom = $("#ddlDiscount" + id);
    var startrow = 0;
    var startrowvalue = "--Select--";
    $.ajax({
        type: "POST",
        url: '/Sales/Get_DiscountType/',
        data: "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (dataset) {
            $("#ddlDiscount" + id).empty();
            $("#ddlDiscount" + id).append('<option value="' + startrow + '">' + startrowvalue + '</option>');
            $.each(dataset, function (i, item) {

                $("#ddlDiscount" + id).append('<option value="' + item.Id + '">' + item.DiscountTypeName + '</option>');
            });
            $("#ddlDiscount" + id).select2();
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

//calculate line total
function calLineTotal(id) {
    /// txtSalesQty   txtRate  txtDiscountRate   txtTaxableval   txtGSTRate   txtGSTAmount txttotal
    var qty = $("#txtSalesQty" + id).val();
    //var pcs = $("#txtpcs" + id).val();
    var disc = $("#txtDiscountRate" + id).val();
    if (disc == "" || disc == undefined) {
        disc = 0;
    }
    var rate = $("#txtRate" + id).val();
    var gstRate = $("#txtGSTRate" + id).val();
    if (gstRate == "" || gstRate == undefined) {
        gstRate = 0;
    }
    //var taxableTotal = $("#txtTaxableval" + id).val();
    var GSTAmount = $("#txtGSTAmount" + id).val();
    var itemTotal = $("#txttotal" + id).val();
    var hndSalesTotal_ = $("#TotalAmount").val();
    //var subtotal_ = $("#txtsubtotal").val();

    if (itemTotal > 0) {
        hndSalesTotal_ = hndSalesTotal_ - itemTotal;
    }
    if (qty != "" && qty > 0) {
        if (rate > 0) {

            var line_total = parseFloat(qty) * (parseFloat(rate));// - parseFloat(disc));
            var discount_ = parseFloat(line_total) * parseFloat(disc) / 100;
            var linetotal = line_total - discount_;
            //var linetotal = parseFloat(pcs) * (parseFloat(rate) - parseFloat(disc));                        
            $("#txtTaxableval" + id).val(parseFloat(linetotal).toFixed(2));
            var valueWithTax = parseFloat(linetotal) * parseFloat(gstRate) / 100;
            $("#txtGSTAmount" + id).val(parseFloat(valueWithTax).toFixed(2));
            var item_total = parseFloat(linetotal) + parseFloat(valueWithTax);
            $("#txttotal" + id).val(parseFloat(item_total).toFixed(2));
            var grandtotal_ = parseFloat(item_total) + parseFloat(hndSalesTotal_);
            $("#TotalAmount").val(grandtotal_.toFixed(2));
            $("#TotalBillAmount").html(grandtotal_.toFixed(2));

        }
        else {
            // swal("Enter rate of particulars.");
            $("#txtRate" + id).focus();

        }
    }
    else {
        //swal("Enter PCS of particulars.");
        //$("#txtpcs" + id).focus();
    }

}

//function to remove row
function removeRow(id) {
    calAmount(id);
    var rowid = "rowid" + id;
    $("#SalesBillItemsList").find("#" + rowid).remove();
}

function calAmount(id) {
    var itemTotal = $("#txttotal" + id).val();
    var hndSalesTotal_ = $("#TotalAmount").val();
    if (itemTotal > 0) {
        hndSalesTotal_ = hndSalesTotal_ - itemTotal;
        $("#TotalAmount").val(hndSalesTotal_.toFixed(2));
        $("#TotalBillAmount").html(hndSalesTotal_.toFixed(2));
    }
}

//accept number including dot only.
function isNumberKey(evt, id) {
    try {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) {
            var txt = document.getElementById(id).value;
            if (!(txt.indexOf(".") > -1)) {

                return true;
            }
        }
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    } catch (w) {
        alert(w);
    }
}

//function to adjust amount as per mode of payment
function adjustAmount() {
    var payMode = $("#Payment_Mode_ID").children(":selected").text();
    var payModeId = $("#Payment_Mode_ID").val();
    var voucherAmount = $("#TotalAmount").val();
    $("#Cash_Amount").val('');
    $("#Cheque_No").val('');
    $("#Cheque_Date").val('');
    $("#Bank_Name").val('');
    $("#Cheque_Amount").val('');
    $("#Credit_Card_No").val('');
    $("#Credit_Card_Amount").val('');
    $("#Net_Banking_Ref_No").val('');
    $("#Net_Banking_Amount").val('');
    $("#Total_Amount").val(voucherAmount);
    $("#Received_Amount").val('');
    $("#CashtobeReturns").val(0.00);

    $("#Cash_Amount").attr('disabled', true);
    $("#Cheque_No").attr('disabled', true);
    $("#Cheque_Date").attr('disabled', true);
    $("#Bank_Name").attr('disabled', true);
    $("#Cheque_Amount").attr('disabled', true);
    $("#Credit_Card_No").attr('disabled', true);
    $("#Credit_Card_Amount").attr('disabled', true);
    $("#Net_Banking_Ref_No").attr('disabled', true);
    $("#Net_Banking_Amount").attr('disabled', true);

    if (voucherAmount > 0) {
        payMode = (payMode).toLowerCase();
        if (payMode == "cash") {
            $("#Cash_Amount").attr('disabled', false);
            $("#Cash_Amount").val(voucherAmount);
        }
        if (payMode == "cheque") {
            var date = $("#Voucher_Date").val();
            $("#Cheque_No").attr('disabled', false);
            $("#Cheque_Date").attr('disabled', false);
            $("#Bank_Name").attr('disabled', false);
            $("#Cheque_Amount").attr('disabled', false);
            $("#Cheque_Date").val(date);
            $("#Cheque_Amount").val(voucherAmount);
        }
        if (payMode == "card") {
            $("#Credit_Card_No").attr('disabled', false);
            $("#Credit_Card_Amount").attr('disabled', false);
            $("#Credit_Card_Amount").val(voucherAmount);
        }
        if (payMode == "net banking") {
            $("#Net_Banking_Ref_No").attr('disabled', false);
            $("#Net_Banking_Amount").attr('disabled', false);
            $("#Net_Banking_Amount").val(voucherAmount);
        }
    }
}

//fucntion to calculate cash to be returns
function calCashtobereturn() {
    var rec_Cash = $("#Received_Amount").val();
    var tot_cash = $("#Total_Amount").val();
    var ret_Cash = 0;
    $("#CashtobeReturns").val(0);
    if (rec_Cash > 0 && rec_Cash > tot_cash) {
        ret_Cash = parseFloat(rec_Cash) - parseFloat(tot_cash);
        ret_Cash = parseFloat(ret_Cash).toFixed(2);
        $("#CashtobeReturns").val(ret_Cash);
    }
}

//validate data
function ValidateData() {
    var msg = "Mandetory Information : \n";
    var blnFlag = true;


    var $rows_ = $('#SalesBillItemsList').find('tbody tr');

    if ($rows_.length == 0) {
        msg = msg + "* Please select at least one item for sale. \n";
        blnFlag = false;
    }
    if ($("#NameContact").val() == "") {
        msg = msg + "* Please enter customer mobile no. \n";
        blnFlag = false;
    }
    if ($("#CustomerName").val() == "") {
        msg = msg + "* Please select customer mobile no from list.\n";
        blnFlag = false;
    }
    if ($('#Employee_ID').val() == "0") {
        msg = msg + "* Please select salesman.\n";
        blnFlag = false;
    }

    if ($("#Payment_Mode_ID").val() == "") {
        msg = msg + "* Please select mode of payment. \n";
        blnFlag = false;
    }

    if ($("#TotalAmount").val() == "0") {
        msg = msg + "* Sales amount should be greater than zero. \n";
        blnFlag = false;
    }

    if (msg != "" && blnFlag == false) {
        swal("warning", msg, "warning");
        return false;
    }
    return true;
}

//code to save sales
$("#btnSave").click(function () {

    if (ValidateData() == true) {
        var isPageValidate = true;
        var voucherData = {};
        var branch_id = $("#Branch_Id").val();
        var user_ID = $("#Created_By").val();
        var voucherAmount = $("#TotalAmount").val();
        var voucherTypeId = 2;
        var Voucher_ID = $("#Voucher_ID").val();
        if (Voucher_ID == undefined) {
            Voucher_ID = 0;
        }
        voucherData.Voucher_ID = Voucher_ID;
        voucherData.Voucher_No = null;
        voucherData.Voucher_Type_ID = voucherTypeId;
        voucherData.Voucher_Date = $("#Voucher_Date").val();
        //voucherData.User_ID = User_Id;
        voucherData.Voucher_Amount = voucherAmount;
        voucherData.Voucher_Status = "P";
        voucherData.Approved_or_Rejected_By = 0;
        voucherData.Branch_Id = branch_id;
        voucherData.Created_By = user_ID;
        voucherData.Mode = "Add";

        var voucherCustomerData = {};
        var vCustDetail_Id = $("#Voucher_Customer_Detail_ID").val();
        if (vCustDetail_Id == "" || vCustDetail_Id == undefined) {

        }
        voucherCustomerData.Voucher_Customer_Detail_ID = vCustDetail_Id;
        voucherCustomerData.Customer_Mob_No = $("#Customer_Mob_No").val();
        voucherCustomerData.Customer_ID = $("#Customer_ID").val();
        voucherCustomerData.Consignee_Mob_No = $("#Consignee_Mob_No").val();
        voucherCustomerData.Consignee_ID = $("#Consignee_ID").val();
        voucherCustomerData.Employee_ID = $("#Employee_ID").val();
        voucherCustomerData.Payment_Settlement_ID = $("#Payment_Settlement_ID").val();
        voucherCustomerData.Payment_Mode_ID = $("#Payment_Mode_ID").val();
        voucherCustomerData.Cash_Amount = $("#Cash_Amount").val();
        voucherCustomerData.Cheque_No = $("#Cheque_No").val();
        voucherCustomerData.Cheque_Date = $("#Cheque_Date").val();
        voucherCustomerData.Bank_Name = $("#Bank_Name").val();
        voucherCustomerData.Cheque_Amount = $("#Cheque_Amount").val();
        voucherCustomerData.Credit_Card_No = $("#Credit_Card_No").val();
        voucherCustomerData.Credit_Card_Amount = $("#Credit_Card_Amount").val();
        voucherCustomerData.Net_Banking_Ref_No = $("#Net_Banking_Ref_No").val();
        voucherCustomerData.Net_Banking_Amount = $("#Net_Banking_Amount").val();
        voucherCustomerData.Total_Amount = $("#Total_Amount").val();
        voucherCustomerData.Received_Amount = $("#Received_Amount").val();
        voucherCustomerData.Remarks = $("#Remarks").val();
        voucherCustomerData.Created_By = user_ID;


        var inventoryData = [];
        var $rows = $('#SalesBillItemsList').find('tbody tr');
        if ($rows.length > 0) {
            $.each($rows, function (key, row) {

                //txtitem txtitemdesc txtqty txtpcs txtuom txtdisc txtrate txttotal
                //var img = $(row).find('[name=itemImg]').attr('data-ledger-id');
                var vitd_id = $(row).find('[name=Voucher_Item_Detail_ID]').val();
                if (vitd_id == "" || vitd_id == undefined) {
                    vitd_id = 0;
                }
                var barcode = $(row).find('[name=txtBarcode]').val();
                var hscode = $(row).find('[name=txtHsnCode]').val();
                var item_id = $(row).find('[name=txtItem_ID]').val();
                var item_uom = $(row).find('[name=txtItemUOM]').val();
                var item = $(row).find('[name=txtItem]').val();
                var qty_ = $(row).find('[name=txtSalesQty]').val();
                var rate_ = $(row).find('[name=txtRate]').val();
                var scheme_ = $(row).find('[name=ddlScheme]').val();
                var disc_ = $(row).find('[name=ddlDiscount]').val();
                var discRate_ = $(row).find('[name=txtDiscountRate]').val();
                var taxableAmt = $(row).find('[name=txtTaxableval]').val();
                var gstRate_ = $(row).find('[name=txtGSTRate]').val();
                var gstAmt_ = $(row).find('[name=txtGSTAmount]').val();
                var itemTotal = $(row).find('[name=txttotal]').val();

                if (qty_ != "" && rate_ != "") {

                    var itemDetails_ = {
                        'Voucher_Item_Detail_ID': vitd_id,
                        'Barcode': barcode,
                        'Hsncode': hscode,
                        'Item_ID': item_id,
                        'Item_Description': item,
                        'Item_UOM_ID': item_uom,
                        'Sales_Qty': qty_,
                        'Rate': rate_,
                        'Scheme_ID': scheme_,
                        'Discount_Type_ID': disc_,
                        'Discount_Rate': discRate_,
                        'Taxable_Amount': taxableAmt,
                        'GST_Rate': gstRate_,
                        'GST_Amount': gstAmt_,
                        'Total_Amount': itemTotal,
                        'Created_By': user_ID
                    };
                    inventoryData.push(itemDetails_);

                }
            });
        }


        if (isPageValidate === true) {

            var test = JSON.stringify({ Voucher: voucherData, VoucherItems: inventoryData, Voucher_Customer: voucherCustomerData });
            $.ajax({
                url: '/Sales/SaveCounterSales/',
                //data: JSON.stringify(inp),
                data: test,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (output) {
                    if (output > 0) {

                        //swal("success", "Voucher saved successfully.", "success");

                        swal({
                            title: 'success',
                            text: 'Voucher saved successfully.',
                            showConfirmButton: true,
                            showCancelButton: false,
                            closeOnConfirm: true,
                            closeOnCancel: true,
                            confirmButtonText: 'OK',
                            confirmButtonColor: '#8CD4F5',
                            cancelButtonText: 'Cancel'
                        },
                            function (isConfirm) {

                                if (isConfirm) {

                                    var url = '/Sales/Index/';
                                    window.location.href = url;

                                } else {
                                    swal("Cancelled", ")", "error");
                                }
                            });
                    }
                },
                //error: function (response) {
                //    alert(response.responseText);
                //},
                //failure: function (response) {
                //    alert(response.responseText);
                //}
            });
        }
    }
});

//code to view counter sale
function load_salesData() {
    var branch_id = $("#Branch_Id").val();
    var Voucher_ID = $("#Voucher_ID").val();
    var param = { 'v_Id': Voucher_ID, 'branchid': branch_id };
    if (branch_id != "" && Voucher_ID != "") {
        $.ajax({
            type: "POST",
            url: '/Sales/Get_Countersales_ById/',
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (dataset) {

                var voucherDetails = dataset.Voucher;
                var voucherItemDetails = dataset.VoucherItems;
                var voucherCustomerDetails = dataset.Voucher_Customer;
                var str_date = dMyyyy(voucherDetails.vc_date);
                $("#Voucher_Date").val(str_date);
                $("#TotalAmount").val(voucherDetails.Voucher_Amount);
                $("#TotalBillAmount").html(voucherDetails.Voucher_Amount);
                $.each(voucherItemDetails, function (i, item) {
                    var $rows_ = $('#SalesBillItemsList').find('tbody tr');
                    var rowCnt = 0;
                    if ($rows_.length > 0) {
                        rowCnt = $rows_.length;
                        rowCnt = rowCnt + 1;
                    }
                    else {
                        rowCnt = rowCnt + 1;
                    }
                    var tblrow = '<tr id="rowid' + rowCnt + '">' +
                        '<td class="action text-center" style="width: 20px"><input type="hidden" id="Voucher_Item_Detail_ID' + rowCnt + '" class="form-control" name="Voucher_Item_Detail_ID" value="' + item.Voucher_Item_Detail_ID + '"/><a id="' + rowCnt + '" class="fa fa-trash" onclick="removeRow(' + rowCnt + ');" aria-hidden="true"></a></td>' +
                        '<td class="Image"><img src="" class="" style="border:1px"/></td>' +
                        '<td class="Barcode"><input id="txtBarcode' + rowCnt + '" type="text" value="' + item.Barcode + '" name="txtBarcode" class="form-control" autocomplete="off" style="width: 100px;"></td>' +
                        '<td class="HSNCode"><input id="txtHsnCode' + rowCnt + '" value="' + item.Hsncode + '" type="text" name="txtHsnCode" class="form-control" autocomplete="off" style="width: 100px;"></td>' +
                        '<td class="Item"> <input type="hidden" id="txtItem_ID' + rowCnt + '" class="form-control" name="txtItem_ID" value="' + item.Item_ID + '"/><input id="txtItem' + rowCnt + '" value="' + item.Item_Description + '" type="text" name="txtItem" class="form-control ui-autocomplete-input" autocomplete="off" style="width:200px;"></td>' +
                        '<td class="UnitCode"><input type="hidden" id="txtItemUOM' + rowCnt + '" class="form-control" name="txtItemUOM" value="' + item.Item_UOM_ID + '"/><input id="txtUnitCode' + rowCnt + '" type="text" name="txtUnitCode" readonly="true" class="form-control ui-autocomplete-input" autocomplete="off" style="width: 100px;"></td>' +
                        '<td class="qty"><input id="txtSalesQty' + rowCnt + '" type="text" name="txtSalesQty" value="' + item.Sales_Qty + '" onchange="calLineTotal(' + rowCnt + ')" class="form-control text-right" autocomplete="off" style="width: 100px;" onkeypress="return isNumberKey(event,this.id)" value="0" placeholder="Qty"></td>' +
                        '<td class="rate"><input id="txtRate' + rowCnt + '" type="text" name="txtRate" value="' + item.Rate + '" onchange="calLineTotal(' + rowCnt + ')" class="form-control text-right" onkeypress="return isNumberKey(event,this.id)" autocomplete="off" style="width: 100px;" placeholder="Rate"></td>' +
                        '<td class="scheme"><select id="ddlScheme' + rowCnt + '" name="ddlScheme" class="form-control select2" style="width:100px;"><option value="0">--Select--</option></select></td>' +
                        '<td class="discount"><select id="ddlDiscount' + rowCnt + '" name="ddlDiscount" class="form-control select2" style="width: 100px;"></select></td>' +
                        '<td class="discRate"><input id="txtDiscountRate' + rowCnt + '" type="text" name="txtDiscountRate" value="' + item.Discount_Rate + '" onchange="calLineTotal(' + rowCnt + ')" value="0" class="form-control text-right" onkeypress="return isNumberKey(event,this.id)" style="width: 100px;"></td>' +
                        '<td class="taxableVal"><input id="txtTaxableval' + rowCnt + '" type="text" name="txtTaxableval" value="' + item.Taxable_Amount + '" class="form-control text-right" readonly="true" style="width: 100px;"></td>' +
                        '<td class="GSTrate"><input id="txtGSTRate' + rowCnt + '" type="text" name="txtGSTRate" onchange="calLineTotal(' + rowCnt + ')" value="' + item.GST_Rate + '" class="form-control text-right" style="width: 100px;"></td>' +
                        '<td class="gstAmt"><input id="txtGSTAmount' + rowCnt + '" type="text" name="txtGSTAmount" value="' + item.GST_Amount + '" class="form-control text-right" readonly="true" style="width: 100px;"></td>' +
                        '<td class="linetotal"><input id="txttotal' + rowCnt + '" name="txttotal" type="text text-right" value="' + item.Total_Amount + '" class="form-control text-right rowtotal_" readonly="true" style="width: 100px;"></td>' +
                        '</tr>';
                    if (rowCnt == 1) {
                        $('#SalesBillItemsList').append(tblrow);
                    } else {
                        $('#SalesBillItemsList > tbody > tr:first').before(tblrow);
                    }
                    getDiscountType(rowCnt);
                    //$("#ddlDiscount" + rowCnt).val(item.Discount_Type_ID);
                    FillDiscountBasedonId($("#ddlDiscount" + rowCnt), item.Discount_Type_ID);
                    //$("#ddlDiscount" + rowCnt).val(item.Discount_Type_ID).trigger('change');
                    //$("#ddlDiscount" + rowCnt).select2().trigger('change');
                    //$("#ddlDiscount" + rowCnt).val();
                });

                //$.each(voucherCustomerDetails, function (i, customerinfo) {
                $("#Voucher_Customer_Detail_ID").val(voucherCustomerDetails.Voucher_Customer_Detail_ID);
                $("#NameContact").val(voucherCustomerDetails.Customer_Mob_No);
                $("#Customer_Mob_No").val(voucherCustomerDetails.Customer_Mob_No);
                $("#Customer_ID").val(voucherCustomerDetails.Customer_ID);
                $("#CustomerName").val(voucherCustomerDetails.Customer_Name);
                $("#ConsigneeMob").val(voucherCustomerDetails.Consignee_Mob_No);
                $("#ConsigneeName").val(voucherCustomerDetails.Consignee_Name);
                $("#Consignee_Mob_No").val(voucherCustomerDetails.Consignee_Mob_No);
                $("#Consignee_ID").val(voucherCustomerDetails.Consignee_ID);
                // Set selected 
                $('#Employee_ID').val(voucherCustomerDetails.Employee_ID);
                $('#Employee_ID').select2().trigger('change');
                $("#Payment_Settlement_ID").val(voucherCustomerDetails.Payment_Settlement_ID);
                $('#Payment_Settlement_ID').select2().trigger('change');
                $("#Payment_Mode_ID").val(voucherCustomerDetails.Payment_Mode_ID);
                $('#Payment_Mode_ID').select2().trigger('change');
                $("#Cash_Amount").val(voucherCustomerDetails.Cash_Amount);
                $("#Cheque_No").val(voucherCustomerDetails.Cheque_No);
                var Ch_date = dMyyyy(voucherCustomerDetails.Cheque_Date);
                $("#Cheque_Date").val(Ch_date);
                $("#Bank_Name").val(voucherCustomerDetails.Bank_Name);
                $("#Cheque_Amount").val(voucherCustomerDetails.Cheque_Amount);
                $("#Credit_Card_No").val(voucherCustomerDetails.Credit_Card_No);
                $("#Credit_Card_Amount").val(voucherCustomerDetails.Credit_Card_Amount);
                $("#Net_Banking_Ref_No").val(voucherCustomerDetails.Net_Banking_Ref_No);
                $("#Net_Banking_Amount").val(voucherCustomerDetails.Net_Banking_Amount);
                $("#Total_Amount").val(voucherCustomerDetails.Total_Amount);
                $("#Remarks").val(voucherCustomerDetails.Remarks);
                //});
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}

function dMyyyy(date_) {
    var date_n = new Date(date_);
    //var date_n = Date.parse(date_);
    const currentDate = date_n.getDate();
    //const formattedCurrentDate = ("0" + currentDate).slice(-2);
    const currentMonth = date_n.getMonth();
    //const formattedCurrentMonth = ("0" + (currentMonth + 1)).slice(-2);
    const currentYear = date_n.getFullYear();
    const monthList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    var date_Default = currentDate + '-' + monthList[currentMonth] + '-' + currentYear;
    return date_Default;
}

function FillDiscountBasedonId($selectList, Selectval) {

    $.ajax({
        type: "POST",
        url: '/Sales/Get_DiscountType/',
        data: "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (dataset) {
            if (dataset != undefined) {
                $selectList.select2();
                if (Selectval != undefined) {
                    $selectList.val(Selectval);
                    $selectList.select2().trigger('change');
                    //$selectList.val(Selectval).trigger('change');
                }
            }

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}
