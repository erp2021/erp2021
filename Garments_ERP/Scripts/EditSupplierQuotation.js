﻿


function calQty(id) {
    var number = id.match(/\d+/);
    var total = 0;
    var totalcost = 0;
    var totalprice = 0;
    var Totalqty = 0;
    Totalqty = $("#Totalqty" + number).val();
    var itemrate = $("#itemtd" + number).val();
    totalprice = $("#totalprice" + number).val();
    if (itemrate != "") {
        total = Totalqty * itemrate;
    }
    $("#totalprice" + number).val(total);

    $(".totalprice").each(function () {
        if ($(this).val() != "") {
            totalcost = totalcost + parseInt($(this).val());
        }
    });
    $("#total").val(totalcost);
    $("#addedprice").val(totalcost);
    calTotalCost();
}
function calTotalCost() {
    var totalcost = 0;
    var gsttaxamount = 0;
    var grandtotal = 0;
    var taxv = 0;
    $(".totalprice").each(function () {
        if ($(this).val() != "") {
            totalcost = totalcost + parseInt($(this).val());
        }
    });

    var totalprice = totalcost;
   // var totalprice = $("#addedprice").val();
    var othertaxamount = 0;
    var GSTtax = $("#GSTCharge").val();
    taxv = (parseInt(totalprice) * parseInt(GSTtax)) / 100;
    $("#tax").val('' + taxv);
    $("#Htax").val('' + taxv);
    var Othertax = $("#OtherTax").val();
    if (GSTtax != "" || Othertax != "") {
        gsttaxamount = totalprice * GSTtax / 100;
        othertaxamount = totalprice * Othertax / 100;
        taxv = parseFloat(gsttaxamount) + parseFloat(othertaxamount);
        $("#tax").val('' + taxv);
        $("#Htax").val('' + taxv);
        totalcost = parseFloat(totalprice) + parseFloat(gsttaxamount) + parseFloat(othertaxamount);
        $("#total").val(totalcost);
        $("#Grandtotal").val(totalcost);
    }
    else {
        var totalwithouttax = $("#addedprice").val();
        $("#total").val(totalwithouttax);
        $("#Grandtotal").val(totalwithouttax);
    }
    GetGrandTot();
}

function GetGrandTot() {
    var $dataRows = $("#OtherChargeDetails tr:not('.rowtitle')");
    var len = $dataRows.length;
    var OC_tot1 = 0;
    var potot = $("#total").val();
    if (len > 0) {
        $dataRows.each(function (i) {
            $(this).find('.ocdata').each(function (i) {
                if (i == 4) {
                    var Octot = ($(this).val());
                    Octot = Octot == "" ? 0 : Octot;
                    OC_tot1 = parseFloat(OC_tot1) + parseFloat(Octot);
                }
            });
        });

        var Grandtot = parseFloat(potot) + parseFloat(OC_tot1);
        $("#Grandtotal").val(Grandtot);
    }
    else {
        $("#Grandtotal").val(potot);
    }
}


$('#SaveQuotation').on('click', function () {
    var SupplierId = $("#SupplierId").val();
    var PR_No = $('#PR_No').val();
    var Gst = $("#GSTCharge").val();

    var str = "";
    var count = 0;
    var rowcount = 0;
    var isFirstItem = 0;
    var $dataRows = $("#rawitemtable tr:not('.titlerow')");
    var rowlenght = $dataRows.length;
    var isvalid = true;

    if (SupplierId == "" || SupplierId == 0) {
        swal("Please select supplier");
        isvalid = false;
    }
    else if (PR_No == "" || PR_No == 0) {
        swal("Please select purchase request number");
        isvalid = false;
    }
    else if (Gst == "select") {
        swal("Please select GST");
        isvalid = false;
    }
    else if (rowlenght > 0) {
        var stylerowcount = 0;
        var Stcount = 0;
        $dataRows.each(function () {
            debugger;
            count = 0;
            stylerowcount++;
            var Aleng = $(this).children('td').length;
            $(this).find('.rawitem').each(function (i) {
                debugger;
                var itemlist = ($(this).val());  //here get td value
                count++;

                if (itemlist != undefined && itemlist != "") {

                    if (count == Aleng) {
                        str += itemlist;
                    }
                    else {
                        str += itemlist + "~";
                    }
                }
                else {
                    Stcount = 1;
                }

            });

            if (stylerowcount == rowlenght) {
                rowlenght = rowlenght;
            }
            else {
                str += "#";
            }
            $('#QuotationItemList').val(str);

        });

        var $OC_dataRows = $("#OtherChargeDetails tr:not('.rowtitle')");
        var OClenght = $OC_dataRows.length;
        var Ocrowcount = 0;
        var str1 = "";
        if (OClenght > 0) {
            $OC_dataRows.each(function (i) {
                count = 0;
                Ocrowcount++;
                var Aleng = $(this).children('td').length;
                $(this).find('.ocdata').each(function (i) {
                    var itemlist = ($(this).val());
                    count++;
                    itemlist = itemlist == "select" || itemlist == "" ? 0 : itemlist;
                    if (itemlist != undefined) {

                        if (count == Aleng) {
                            str1 += itemlist;
                        }
                        else {
                            str1 += itemlist + "~";
                        }
                    }
                });

                if (Ocrowcount == OClenght) {
                }
                else {
                    str1 += "#";
                }
            });
            $("#otherchargestr").val(str1);
        }
        else {
            $("#otherchargestr").val("");
        }

        if (Stcount == 1) {
            swalMsg("Item Details Required.");
            isvalid = false;
        }
    }

    return isvalid;
});

