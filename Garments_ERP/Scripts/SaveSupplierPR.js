﻿
function GetAccountHead() {
    debugger;
    var startrow = 0;
    var startrowvalue = "---Select----";
    var deptid = $("#DepartmentId").val();
    if (deptid != "") {
        $.ajax({
            type: "POST",
            url: '/SupplierPurchase/GetAccountHead/',
            data: "{'deptid':" + deptid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                $("#AccountHeadId").empty();
                $("#AccountHeadId").append('<option value="' + startrow + '">' + startrowvalue + '</option>');
                $.each(result, function (i, bussiness) {
                    $("#AccountHeadId").append('<option value="' + bussiness.UserId + '">' + bussiness.Name + '</option>');
                });
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}




$('#savePR').on('click', function () {
    var PR_Date = $("#PR_Date").val();
    var DepartmentId = $('#DepartmentId').val();
    var AccountHeadId = $("#AccountHeadId").val();
    var BOM_Id = $("#BOM_Id").val();
    var str = "";
    var count = 0;
    var rowcount = 0;
   
    var $dataRows = $("#rawitemtable tr:not('.titlerow')");
    var rowlenght = $dataRows.length;
    var isvalid = true;
   
    if (PR_Date == "") {
        swalMsg("Please Select PR Date");
        isvalid = false;
    }
    else if (DepartmentId == "" || DepartmentId == 0) {
        swalMsg("Please Select Department");
        isvalid = false;
    }
    else if (AccountHeadId == "" || AccountHeadId == 0) {
        swalMsg("Please Select Account Head");
        isvalid = false;
    }
    else if (BOM_Id == "" || BOM_Id == 0) {
        swalMsg("Please Select BOM");
        isvalid = false;
    }
    else if (rowlenght > 0) {
        var stylerowcount = 0;
        var Stcount = 0;
        $dataRows.each(function () {
            debugger;
            count = 0;
            stylerowcount++;
            var Aleng = $(this).children('td').length;
            $(this).find('.rawitem').each(function (i) {
                debugger;
                var itemlist = ($(this).val());  //here get td value
                count++;

                if (itemlist != undefined && itemlist!="") {

                    if (count == Aleng) {
                        str += itemlist;
                    }
                    else {
                        str += itemlist + "~";
                    }
                }
                else {
                    Stcount = 1;
                }

            });

            if (stylerowcount == rowlenght) {
                rowlenght = rowlenght;
            }
            else {
                str += "#";
            }
            $('#PRItemList').val(str);

        });
        debugger;
        if (Stcount==1) {
            swalMsg("Item Details Required.");
            isvalid = false;
        }
    }
    return isvalid;
});
// function addrow () {
//    var $dataRows = $("#rawitemtable tr:not('.titlerow')");
//    contactsdiv = $($dataRows[0]).clone();
//    $('#rawitemtable tbody').append(contactsdiv);
//    $dataRows = $("#rawitemtable tr:not('.titlerow')");
//    length1 = $dataRows.length;
//    $select1 = $('select[name="unitid"]', $($dataRows[length1 - 1])).clone();
//    //$select1.select2();
//    $('select[name="unitid"]', $($dataRows[length1 - 1])).parent().replaceWith($select1);
//    $('select[name="unitid"]', $($dataRows[length1 - 1])).select2();
//}

function swalMsg(errormsg) {
    swal({
        title: 'Error',
        text: errormsg,
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false,
        closeOnCancel: false

    });
}
