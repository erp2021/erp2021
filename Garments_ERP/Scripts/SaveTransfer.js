﻿
function isQCcheck(id) {
    var number = id.match(/\d+/);
    if ($('#qcdone' + number).prop('checked')) {
        $('#hideqcdone' + number).val("Yes");
    } else {
        $('#hideqcdone' + number).val("No");
    }
}

function calQty(id) {
    var number = id.match(/\d+/);
    var Balanceqty = 0;
    var workorderqty = $("#workorderqty" + number).val();
    var producedqty = $("#producedqty" + number).val();
    if (workorderqty != "" && producedqty != "") {
        Balanceqty = parseFloat(workorderqty) - parseFloat(producedqty);
    }
    else {
        Balanceqty = 0;
    }
    $("#balanceqty" + number).val(Balanceqty);
}

$('#saveTransfer').on('click', function () {
    debugger;
    var DepartmentId = $('#DepartmentId').val();
    var WarehouseId = $("#WarehouseId").val();
    var RackId = $("#RackId").val();
    var RefWorkOrderId = $('#RefWorkOrderId').val();
    var str = "";
    var count = 0;
    var rowcount = 0;
    var isFirstItem = 0;
    var $dataRows = $("#ItemBatchtab tr:not('.title')");
    var rowlenght = $dataRows.length;
    var isvalid = true;
    var stylerowcount = 0;

    if (DepartmentId == "" || DepartmentId == 0) {
        swal("Please Select Department");
        isvalid = false;
    }
    else if (WarehouseId == "" || WarehouseId == 0) {
        swal("Please Select Warehouse");
        isvalid = false;
    }
    else if (RackId == "" || RackId == 0) {
        swal("Please Select Rack");
        isvalid = false;
    }
    else if (RefWorkOrderId == "" || RefWorkOrderId == 0) {
        swal("Please Select Work Order Number");
        isvalid = false;
    }
    else if (rowlenght > 0) {
        count = 0;
        var totprod = 0;
        $dataRows.each(function () {
            debugger;
            count = 0;
            var produce = $("#producedqty" + stylerowcount).val() == "" ? 0 : $("#producedqty" + stylerowcount).val();
            totprod = parseFloat(totprod) + parseFloat(produce);
            stylerowcount++;
            var Aleng = $(this).children('td').length;
            $(this).find('.stylerow').each(function (i) {
                debugger;
                var itemlist = ($(this).val());  //here get td value
                count++;
                if (itemlist != undefined) {

                    if (count == Aleng) {
                        str += itemlist;
                    }
                    else {
                        str += itemlist + "~";
                    }
                }

            });

            if (stylerowcount == rowlenght) {
                rowlenght = rowlenght;
            }
            else {
                str += "#";
            }

        });
        debugger;
        $("#totProducedQty").val(parseFloat(totprod).toFixed(2));
        var balqty = 0;
        var totbatchQty = $("#totBatchQty").val();
        if (parseFloat(totprod) > parseFloat(totbatchQty)) {
            balqty = 0;
        }
        else {
            balqty = parseFloat(totbatchQty)- parseFloat(totprod); 
        }
        $("#totBalancedQty").val(balqty);


        if (count > 0) {
            $('#Bomliststr').val(str);
        }
        else {
            swal("Item Details Required.");
            isvalid = false;
        }
    }

    return isvalid;
});

$('#editTransfer').on('click', function () {
    var DepartmentId = $('#DepartmentId').val();
    var WarehouseId = $("#WarehouseId").val();
    var RackId = $("#RackId").val();

    var str = "";
    var count = 0;
    var rowcount = 0;
    var isFirstItem = 0;
    var $dataRows = $("#itemsizetable tr:not('.titlerow')");
    var rowlenght = $dataRows.length;
    var stylerowcount = 0;
    var isvalid = true;

    if (DepartmentId == "" || DepartmentId == 0) {
        showAlert("Please select department");
        isvalid = false;
    }
    else if (WarehouseId == "" || WarehouseId == 0) {
        showAlert("Please select warehouse");
        isvalid = false;
    }
    else if (RackId == "" || RackId == 0) {
        showAlert("Please select rack");
        isvalid = false;
    }
    else if (rowlenght > 0) {
        count = 0;
        $dataRows.each(function () {
            count = 0;
            stylerowcount++;
            var Aleng = $(this).children('td').length;
            var prodQty = 0;

            $(this).find('.producedqty').each(function (i) {
                prodQty = ($(this).val()) == "" ? -1 : ($(this).val());  //here get td value
            });

            
            if (prodQty == -1) {
                showAlert("Item details required.");
                isvalid = false;
            }
            else {
                $(this).find('.stylerow').each(function (i) {
                    var itemlist = ($(this).val());  //here get td value
                    count++;

                    if (itemlist != undefined) {

                        if (count == Aleng) {
                            str += itemlist;
                        }
                        else {
                            str += itemlist + "~";
                        }
                    }

                });
            }
            if (stylerowcount == rowlenght) {

                rowlenght = rowlenght;
            }
            else {
                str += "#";
            }

        });
        if (count > 0) {
            $('#sizeliststr').val(str);
        }
        else {
            showAlert("Item details required.");
            isvalid = false;
        }
    }

    function showAlert(msg) {
        swal({
            title: "Error",
            text: msg,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
    }


    return isvalid;
});