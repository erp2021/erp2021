﻿function GetList() {
    var typeid = $("#typeid").val();
    var BSOid = $("#BSOid").val();
    $("#POid").empty();
    if (typeid == "customer") {
        $.ajax({
            type: "POST",
            url: '/InvoiceChallan/getPOListbycustomerid/',
            data: "{'id':" + BSOid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                $("#POid").append('<option value="0">Select</option>');
                $.each(result, function (i, bussiness) {
                    $("#POid").append('<option value="' + bussiness.Id + '">' + bussiness.POno + '</option>');
                });
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    else if (typeid == "supplier") {
        $.ajax({
            type: "POST",
            url: '/SupplierGRN/getPOlist/',
            data: "{'id':" + BSOid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (items) {
                $("#POid").append('<option value="0">Select</option>');
                $.each(items, function (i, item) {
                    $("#POid").append('<option value="' + item.Id + '">' + item.PO_NO + '</option>');
                });
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    else if (typeid == "operator") {
        $.ajax({
            type: "POST",
            url: '/ReturnOrder/getOperatorPOlist/',
            data: "{'id':" + BSOid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                $("#POid").append('<option value="0">Select</option>');
                $.each(result, function (i, bussiness) {
                    $("#POid").append('<option value="' + bussiness.Id + '">' + bussiness.POno + '</option>');
                });
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}

function ClearData() {
    $("#BSOnameId").val("");
    $("#BSOid").val("");
    $("#POid").empty();
    $("#WOid").empty();
    $("#invoicenoid").empty();
    $("#MInoid").empty();
    $("#MTnoid").empty();
}

function GetPoData() {
    debugger;
    var typeid = $("#typeid").val();    
    var poid = $("#POid").val();
    if (typeid == "customer") {

        if (parseInt(poid) > 0) {
            $("#invoicenoid").empty();
            $.ajax({
                type: "POST",
                url: '/ReturnOrder/GetInvoiceList/',
                data: "{'id':" + poid + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    debugger;
                    if (result != null) {
                        $("#podate").val(setDateformat(result[0]));

                        $("#invoicenoid").append('<option value="0">Select</option>');
                        $.each(result[1], function (i, bussiness) {
                            $("#invoicenoid").append('<option value="' + bussiness.InvoiceId + '">' + bussiness.InvoiceNo + '</option>');
                        });
                    }
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    }
    else if (typeid == "supplier") {
        if (parseInt(poid) > 0) {
            $.ajax({
                type: "POST",
                url: '/ReturnOrder/GetPOData/',
                data: "{'id':" + poid + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    debugger;
                    if (result != null) {
                        $("#podate").val(setDateformat(result[0]));
                        setItemData(result[1]);
                    }
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    }
    else if (typeid == "operator") {
        if (parseInt(poid) > 0) {
            $.ajax({
                type: "POST",
                url: '/ReturnOrder/GetOperatorPOData/',
                data: "{'id':" + poid + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    debugger;
                    if (result != null) {
                        $("#podate").val(setDateformat(result[0]));

                        $("#WOid").append('<option value="0">Select</option>');
                        $.each(result[1], function (i, bussiness) {
                            $("#WOid").append('<option value="' + bussiness.WorkOrderID + '">' + bussiness.WorkOrderNo + '</option>');
                        });
                    }
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    }
}

function getWOData() {
    var woid = $("#WOid").val();
    if (parseInt(woid) > 0) {
        $.ajax({
            type: "POST",
            url: '/ReturnOrder/GetWOData/',
            data: "{'id':" + woid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                debugger;
                if (result != null) {
                    $("#wodate").val(setDateformat(result[0]));

                    $("#MInoid").append('<option value="0">Select</option>');
                    $.each(result[1], function (i, bussiness) {
                        $("#MInoid").append('<option value="' + bussiness.Id + '">' + bussiness.MaterialIssueNo + '</option>');
                    });
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}

function GetransferData() {
    var MIid = $("#MInoid").val();
    var optid = $("#BSOid").val();
    if (parseInt(MIid) > 0 && optid != null && optid != "" && parseFloat(optid)>0) {
        $.ajax({
            type: "POST",
            url: '/ReturnOrder/GetTransferData/',
            data: "{'id':" + MIid + ",'oprtid':" + optid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                debugger;
                if (result != null) {
                    $("#MTnoid").append('<option value="0">Select</option>');
                    $.each(result, function (i, bussiness) {
                        $("#MTnoid").append('<option value="' + bussiness.TRANSFER_ID + '">' + bussiness.STOCK_TRANSFER_NO + '</option>');
                    });
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}

function GetTransferItem() {
    var Tid = $("#MTnoid").val();
    if (parseInt(Tid) > 0) {
        $.ajax({
            type: "POST",
            url: '/ReturnOrder/GetTransferItem/',
            data: "{'id':" + Tid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                debugger;
                if (result != null) {
                    $("#transferdate").val(setDateformat(result[0]));
                    setItemData(result[1]);
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}

function setDateformat(Dt) {

    var dt1 = Dt.substring(6, Dt.length);
    dt1 = dt1.substring(0, dt1.length - 2);
    var today = new Date(parseInt(dt1));
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    // var today2 = today.setMonth(mm + 1);
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    today = mm + '/' + dd + '/' + yyyy;

    return today;
}

function GetInvoiceData() {
    var invcid = $("#invoicenoid").val();

    if (invcid > 0) {
        $.ajax({
            type: "POST",
            url: '/ReturnOrder/GetInvoiceItem/',
            data: "{'id':" + invcid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                debugger;
                if (result != null) {
                    $("#invoicedate").val(setDateformat(result[0]));
                    setItemData(result[1]);
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}

function setItemData(list) {
    $("#returnTable > tbody").empty();
    if (list != null) {

        $.each(list, function (i, data) {
            debugger;
            var batchstr = "";
            var red = "";
            var issueqty = 0;
            if (data.BatchList != null && data.BatchList.length > 0) {
                red = "readonly";
                $.each(data.BatchList, function (k, bt) {
                    batchstr = batchstr + "<option value='" + bt.ItemInward_ID + "'>" + bt.Dynamic_Controls_value + "</option>";
                });
                issueqty = data.ISSUED_QTY;
            }

            var str = "<tr><td>" + data.ItemMaster.ItemName + "<input type='hidden' value='" + data.ITEM_ID + "' class='rowdata' id='itemid" + i + "' /><input type='hidden' value='" + data.ITEM_ATTRIBUTE_ID + "'  class='rowdata' id='itemAttrid" + i + "' /></td>";
            str = str + "<td>" + data.HSN_CODE + "<input type='hidden' value='" + data.HSN_CODE + "' class='rowdata' /></td>";
            str = str + "<td>" + data.unitent.ItemUnit + "<input type='hidden' value='" + data.UNIT + "' class='rowdata' /></td>";
            str = str + "<td>" + data.PO_TRANSFER_QTY + "<input type='hidden' value='" + data.PO_TRANSFER_QTY + "' id='poqtyid" + i + "'  class='rowdata' /></td>";
            str = str + "<td><input type='text' class='form-control rowdata' value='" + issueqty + "' id='issueqtyid" + i + "' " + red +"  /></td>";
            str = str + "<td><select class='form-control select2 rowdata'  multiple='multiple'  id='batchid" + i + "' onchange='GetInwardQty(this.id)'><option value='0'>Select</option>" + batchstr + "</select></td>";
            str = str + "<td><input type='text' class='form-control rowdata' id='returnqtyid" + i + "' value='0' onkeypress='return isNumber(event)' " + red +"onchange='balchange(this.id)' /></td>";
            str = str + "<td><input type='text' class='form-control rowdata' id='balqtyid" + i + "' value='0' onkeypress='return isNumber(event)' readonly /></td></tr>";
            $("#returnTable > tbody").append(str);

            $("#batchid" + i).select2();
            $("#batchid" + i).next('span').attr("style", "width:230px;");
        });

    }
}


function GetInwardQty(id) {
    debugger;
    var no = id.match(/\d+/);
    no = no[0];
    var ItemId = $("#itemid" + no).val();
    var itemAttr = $("#itemAttrid" + no).val();
    var inwardid = $("#batchid" + no + " option:selected").toArray().map(item => item.value).join();
    var Josndata = { 'ItemId': ItemId, 'ItemAttr': itemAttr, 'inwardid': inwardid };
    var MyJson = JSON.stringify(Josndata);
    $.ajax({
        type: "POST",
        url: '/BillOfMaterial/SaveAutoPR/',
        data: "{'Model':'" + MyJson + "','type':9}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            result = JSON.parse(result);
            $.each(result.Table, function (key, BT) {
                $("#returnqtyid" + no).val(BT.TotalQty);
                var issueqty = $("#issueqtyid" + no).val();
                var bal = parseFloat(issueqty) - parseFloat(BT.TotalQty);
                $("#balqtyid" + no).val(bal);
            });
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function balchange(id) {
    var no = id.match(/\d+/);
    no = no[0];
    var issueqty = $("#issueqtyid" + no).val();
    var returnqty = $("#returnqtyid" + no).val();
    var bal = parseFloat(issueqty) - parseFloat(returnqty);
    $("#balqtyid" + no).val(bal);
}

$("#saveReturnStock").on('click', function () {
    var str = "";
    var $dataRows = $("#returnTable >tbody > tr");
    var rowlenght = $dataRows.length;
    var stylerowcount = 0;
    var rcount = 0;
    var status = true;
    $dataRows.each(function () {
        debugger;
        count = 0;
        stylerowcount++;
        var Aleng = $(this).children('td').length;
        $(this).find('.rowdata').each(function (i) {
            debugger;
            var itemlist = ($(this).val());  //here get td value
            if (i == 2 || i == 6) {
                itemlist = itemlist == "" || itemlist == null ? "hsn" : itemlist;
            }
            if (itemlist == "" || itemlist == null) {
                rcount = 1;
            }
            count++;

            if (itemlist != undefined) {

                if (count == 9) {
                    str += itemlist;
                }
                else {
                    str += itemlist + "~";
                }
            }

        });

        if (stylerowcount == rowlenght) {

        }
        else {
            str += "#";
        }

    });
    
    $('#returnItemstr').val(str);

    if (rcount == 1) {
        swal({
            title: 'Error',
            text: "Please Fill Item Details",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        status = false;
    }
    return status;
});