﻿
$('#saveEmployee').on('click', function () {
    debugger;
    var Name = $("#Name").val();
    var dob = $("#dob").val();
    var maritalstatus = $("#maritalstatus").val();
    var Nationality = $("#Nationality").val();
    var MotherTongue = $("#MotherTongue").val();
    var EmployeeType = $("#EmployeeType").val();
    var address1 = $("#address1").val();
    var countryid = $("#countryid").val();
    var addressstateid = $("#addressstateid").val();
    var addresscityid = $("#addresscityid").val();
    var pincode = $("#pincode").val();
    var MobileNo = $("#MobileNo").val();
    var PhoneNo = $("#PhoneNo").val();
    var EmployeeRole = $("#EmployeeRole").val();
    var EmployeeDept = $("#EmployeeDept").val();
    
    var isvalid = true;

    if (Name == "") {
        msgshow("Please Enter Employee Name");
        isvalid = false;
    }
    else if (dob == "") {
        msgshow("Please Select DOB");
        isvalid = false;
    }
    else if (maritalstatus == "" || maritalstatus=="0") {
        msgshow("Please Select Marital Status");
        isvalid = false;
    }
    else if (Nationality == "") {
        msgshow("Please Enter Nationality");
        isvalid = false;
    }
    else if (MotherTongue == "" || MotherTongue=="0") {
        msgshow("Please Select Mother Tongue");
        isvalid = false;
    }
    else if (EmployeeType == "" || EmployeeType == "0") {
        msgshow("Please Select Employee Type");
        isvalid = false;
    }
    else if (address1 == "") {
        msgshow("Please Enter Address");
        isvalid = false;
    }
    else if (countryid == "" || countryid == "0") {
        msgshow("Please Select Country");
        isvalid = false;
    }
    else if (addressstateid == "" || addressstateid == "0") {
        msgshow("Please Select State");
        isvalid = false;
    }
    else if (addresscityid == "" || addresscityid == "0") {
        msgshow("Please Select City");
        isvalid = false;
    }
    else if (pincode == "" ) {
        msgshow("Please Enter Pincode");
        isvalid = false;
    }
    else if (MobileNo == "") {
        msgshow("Please Enter Mobile Number");
        isvalid = false;
    }
    else if (PhoneNo == "") {
        msgshow("Please Enter Emergency Phone No.");
        isvalid = false;
    }
    else if (EmployeeRole == "") {
        msgshow("Please Select Employee Role.");
        isvalid = false;
    }
    else if (EmployeeDept == "") {
        msgshow("Please Select Employee Department");
        isvalid = false;
    }

    return isvalid;
});

function msgshow(alrtmsg) {
    swal({
        title: 'Error',
        text: alrtmsg,
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false,
        closeOnCancel: false

    });
}