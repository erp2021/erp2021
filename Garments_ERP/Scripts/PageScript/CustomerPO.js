﻿var MyItemArra = [];
var $ItemArra = [];
var MyRawItemArray = [];
var RawItemArray = [];
var ErrorMessage = "";
var OtherChare = [];

$(document).ready(function () {

    var roleid = $("#roleid").val();
    if (parseInt(roleid) != 4) {
        $(".approval").show();
        $(".approvalComment").show();
    }
    else {
        $(".approval").hide();
        $(".approvalComment").hide();
    }

    $('.select2').select2();
    $('.select2').width("100%");
    $("#bs_datepicker_component_container > .ui-datepicker-inline").attr("style", "Display:none;");

    debugger;
    var pathsdat = window.location.pathname;
    pathsdat = pathsdat.split('/');
    if (pathsdat.length == 2) {
        pathsdat[2] = "Index";
    }

    if (pathsdat[2] == "Edit") {
        GetPOStyleData();
        getAddress();
        othercharge1();
    }
    else if (pathsdat[2] == "Index") {
        $("#bs_datepicker_component_container > .ui-datepicker-inline").attr('style', 'display:none;');
        $("#bs_datepicker_component_container1 > .ui-datepicker-inline").attr('style', 'display:none;');

        GetData(2, 'Pending');

        var param = { 'Type': 3, 'attribute': '' };
        $.ajax({
            type: "POST",
            url: '/CustomerEnquiry/StatusCount/',
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                result = JSON.parse(result);
                $.each(result.Table, function (i, data) {
                    $("#Pendingcount").text(data.pending);
                    $("#InProgresscount").text(data.inprogress);
                    $("#Completedcount").text(data.completed);
                    $("#Rejectedcount").text(data.Rejected);
                });
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    else {

        

        $("#approvaldate").datepicker({
            //setDate: currentDate,
            dateFormat: 'mm/dd/yy'
            // maxDate: 0
        });
        $("#POdate").datepicker({
            //setDate: currentDate,
            dateFormat: 'mm/dd/yy'
            // maxDate: 0
        });
        $("#Tentivedate").datepicker({
            //setDate: currentDate,
            dateFormat: 'mm/dd/yy'
            //  maxDate: 0
        });
        $("#Delivery_Schedule_Date").datepicker({
            //setDate: currentDate,
            dateFormat: 'mm/dd/yy',
            minDate: 0
        });

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var today = mm + '/' + dd + '/' + yyyy;
        document.getElementById("Quotdate").value = today;
        document.getElementById("Tentivedate").value = today;
        document.getElementById("POdate").value = today;
    }

});

$('#RefreshPage').click(function () {
    window.location.href = "/CustomerPO/Index/";
});

function setswal(msg) {
    if (ErrorMessage.length > 0) {
        ErrorMessage += "\n" + msg;
    } else { ErrorMessage = msg; }
}

function calculatetotal() {
    debugger;
    var subtotal = $('#subtotal').val();
    var tax = $('#tax').val();
    var total = parseFloat(subtotal) * parseFloat(tax) / 100;
    $('#lblGSTamount').text(total);
    var totalamt = parseFloat(subtotal) + total;
    $('#total').val(totalamt);
    $('#Grandtotal').val(totalamt);
}

function getCustomerdetails() {
    var startrow = 0;
    var startrowvalue = "---Select----";
    var custid = $("#customerid").attr("data-id");
    $("#Quotrefno").empty();
    $.ajax({
        type: "POST",
        url: '/Quotation/getquotnobycustomerid/',
        data: "{'id':" + custid + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {

            $("#Quotrefno").append('<option value="' + startrow + '">' + startrowvalue + '</option>');
            $.each(result, function (i, bussiness) {
                $("#Quotrefno").append('<option value="' + bussiness.Id + '">' + bussiness.QuotationNo + '</option>');
            });

        },
        failure: function (response) {
            alert(response.d);
        }
    });

    getAddress();

}

function getAddress() {
    var startrow = 0;
    var startrowvalue = "---Select----";
    var custid = $("#customerid").attr("data-id");
    $.ajax({
        type: "POST",
        url: '/CustomerEnquiry/getaddress/',
        data: "{'custid':" + custid + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {

            result = JSON.parse(result);
            $('#Addressid').empty();
            $("#Addressid").append('<option value="' + startrow + '">' + startrowvalue + '</option>');
            $.each(result, function (i, item) {

                $("#Addressid").append('<option value="' + item.Id + '">' + item.Address + '</option>');
            });
            $("#Addressid").val($("#Addressid option:eq(1)").val());

            getinfo();

        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function getinfo() {
    var custaddid = $("#Addressid").val();
    if (custaddid != "") {
        $.ajax({
            type: "POST",
            url: '/quotation/getcustomerdetails/',
            data: "{'id':" + custaddid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                result = JSON.parse(result);
                $.each(result, function (i, item) {
                    $('#email').val(item.Email);
                    $("#phone").val(item.Contact_No);
                });
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }

}

function GetDateFormateChage(DateString) {
    DateString = DateString.substr(0, DateString.lastIndexOf("T"));
    var splitString = DateString.split('-');
    DateString = splitString[1] + "/" + splitString[2] + "/" + splitString[0];
    return DateString
}

function getquotdetails() {
    var quotid = $('#Quotrefno').val();
    var quot = $("#Quotrefno option:selected").text();
    if (quotid != 0 && quotid != "" && quotid != null) {
        $.ajax({
            type: "POST",
            url: '/Quotation/getquotationdetails/',
            data: "{'id':" + quotid + "}",
            contentType: "application/json;",
            dataType: "json",
            success: function (result) {
                debugger;
                result = JSON.parse(result);

                result.Quotationdate = result.Quotationdate != null ? GetDateFormateChage(result.Quotationdate) : "";
                result.Approvaldate = result.Approvaldate != null ? GetDateFormateChage(result.Approvaldate) : "";
                result.Createddate = result.Createddate != null ? GetDateFormateChage(result.Createddate) : "";
                result.Tentivedate = result.Tentivedate != null ? GetDateFormateChage(result.Tentivedate) : "";
                var Isreadymade = result.IsReadmade != null && result.IsReadmade > 0 ? result.IsReadmade : 0;
                $("#hidIsReadyMate").val(Isreadymade);
                if (result.POOtherChargeEntity != null) {
                    if (result.POOtherChargeEntity.length > 0) {
                        var j = 0;
                        $("#OtherChargeDetails").show();
                        $("#OC_GrandTotal_id").show();
                        $('#OtherChargeDetails > tbody').empty();
                        $.each(result.POOtherChargeEntity, function (k, ocdata) {
                            debugger;
                            var otherchargeid = "otherchargeid" + j;
                            var OC_Amount = "OC_Amount" + j;
                            var OC_GSTid = "OC_GSTid" + j;
                            var hidgst = "hidgst" + j;
                            var OC_GSTAmt = "OC_GSTAmt" + j;
                            var OC_total = "OC_total" + j;
                            var ocTid = "ocTid" + j;
                            var GST = ocdata.GST == "" || ocdata.GST == null ? 0 : parseFloat(ocdata.GST);
                            var OC_amount = parseFloat(ocdata.OtherChargeValue);
                            var gstamount = parseFloat(OC_amount) * parseFloat(GST) / 100;
                            var tot = parseFloat(gstamount) + parseFloat(OC_amount);

                            var row = '<tr><td>' + ocdata.OCMaster.POOtherChargeName + '<input type="hidden" class="ocdata" id="' + otherchargeid + '" name="' + otherchargeid + '" value="' + ocdata.OtherChargeId + '" /></td>' +
                                '<td>' + OC_amount + '<input type="hidden" class="form-control ocdata" value="' + ocdata.OtherChargeValue + '" id="' + OC_Amount + '" name="' + OC_Amount + '" onchange="OC_Cal(this.id)" onkeypress="return isNumber(event)" style="display:none;" readonly /></td>' +
                                '<td><select class="form-control select2 OC_GST ocdata" style="width: 115px;" id="OC_GSTid' + j + '" name="OC_GSTid' + j + '" disabled="disabled"></select></td>' +
                                '<td>' + gstamount + '<input type="hidden" value="' + gstamount + '" class="form-control ocdata" id="' + OC_GSTAmt + '" name="' + OC_GSTAmt + '" onkeypress="return isNumber(event)" readonly /></td>' +
                                '<td>' + tot + '<input type="hidden" value="' + tot + '" class="form-control ocdata" id="' + OC_total + '" name="' + OC_total + '" onkeypress="return isNumber(event)" readonly /></td>' +
                                '<td><a id="btnOCDelete" class="deleteOC btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></a><input type="hidden" class="ocdata" id="' + ocTid + '" name="' + ocTid + '" value="' + ocdata.Id + '" /></td></tr>';
                                 $('#OtherChargeDetails > tbody').append(row);
                            j++;
                            var jsonaOC = { "Id": ocTid0, "TXID": 0, "OtherChargeId": otherchargeid, "OtherChargeValue": OC_Amount, "GST": GST };
                            OtherChare.push(jsonaOC);
                            debugger;
                            //Get Tax List
                            $.ajax({
                                type: "POST",
                                url: '/Quotation/getGSTTaxlist/',
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (gstsdata) {
                                    debugger;
                                    var result_ = JSON.parse(gstsdata);
                                    var len = OtherChare.length;
                                    for (var m = 0; m < len; m++) {
                                        $('#OC_GSTid' + m).empty();
                                        $("#OC_GSTid" + m).append('<option value="select">Select</option>');
                                        $.each(result_, function (k, item) {
                                            $("#OC_GSTid" + m).append('<option value="' + item.Tax_Rate + '">' + item.Category_Name + '</option>');
                                        });
                                        $("#OC_GSTid" + m).val(OtherChare[m].GST);
                                    }
                                },
                                failure: function (response) {
                                    alert(response.d);
                                }
                            });    
                           
                        });
                    }
                    else {
                        $("#OtherChargeDetails").hide();
                    }
                }
                $("#issampling").val(result.Sampling);
                $("#Tentivedate").val(result.Tentivedate);

                $("#Quotdate").val(result.Quotationdate);

              
                $('#isselectenquiry').val("1");


                $('#costprice').val(result.Costprice);
                $('#saleprice').val(result.SalePrice);

              
                $('#Total').val(result.Total);
                $('#Htotal').val(result.Total);
                $('#GrandTotal').val(result.GrandTotal);


                $('#isselectenquiry').val("0");
                var len = 0;
                if (result.StyleEnt != null) {
                    $.each(result.StyleEnt, function (e, dat) {
                        var Rate = dat.rate;
                        var sizeid = dat.Sizeid;
                        var attributeval = dat.AttrValEnt != null ? dat.AttrValEnt.Attribute_Value : "";
                        var SegTId = dat.Segmenttypeid;
                        var SegTName = dat.SegTypeEnt != null ? dat.SegTypeEnt.Segmenttype : "";
                        var itemid = dat.Itemid;
                        var itemName = dat.ItemEnt != null ? dat.ItemEnt.ItemName : "";
                        var styleid = dat.Styleid;
                        var styleName = dat.styleEnt != null ? dat.styleEnt.Stylename : "";
                        var styleDesc = dat.Styledesc;
                        var styleNo = dat.Styleno;
                        var Brand = dat.Brand;
                        var AttrSizeId = dat.Sizeid;
                        var attributeid = dat.Attribute_ID;
                        var AttributeValId = dat.Attribute_Value_ID;
                        var $MdataRow = $("#MultiItemTab > tbody > tr");
                        var file1 = document.querySelector('#styleimg');
                        var path = $("#Attrimages" + len).val();
                        var ReqQty = dat.Reqqty;
                        var Mlen = $MdataRow.length;
                        var str = '<tr data="' + Mlen + '">';
                        str = str + '<td>' + SegTName + '<input type="hidden" id="M_SegTypeID_' + Mlen + '" name="M_SegTypeID" value="' + SegTId + '" /></td>';
                        str = str + '<td>' + itemName + '<input type="hidden" id="M_ItemID_' + Mlen + '" name="M_ItemID" value="' + itemid + '" /></td>';
                        str = str + '<td>' + styleName + '<input type="hidden" id="M_styleID_' + Mlen + '" name="M_styleID" value="' + styleid + '" /></td>';
                        str = str + '<td>' + styleDesc + '<input type="hidden" id="M_styleDesc_' + Mlen + '" name="M_styleDesc" value="' + styleDesc + '" /></td>';
                        str = str + '<td>' + styleNo + '<input type="hidden" id="M_styleNo_' + Mlen + '" name="M_styleNo" value="' + styleNo + '" /></td>';
                        str = str + '<td>' + Brand + '<input type="hidden" id="M_Brand_' + Mlen + '" name="M_Brand" value="' + Brand + '" /></td>';
                        str = str + '<td>' + ReqQty + '<input type="hidden" id="M_ReqQty_' + Mlen + '" name="M_ReqQty" value="' + ReqQty + '" /></td>';
                        str = str + '<td>' + dat.ItemUnit + '<input type="hidden" id="M_ItemUnit_' + Mlen + '" name="M_ItemUnit" value="' + dat.ItemUnit + '" /></td>';
                        str = str + '<td><input type="text" class="form-control" id="M_Rate_' + Mlen + '" name="M_Rate" value="' + dat.rate + '"  readonly /></td>';
                        str = str + '<td><input type="text" class="form-control" value="' + dat.SellRate + '"  id="M_SellRate_' + Mlen + '" name="M_SellRate" onkeypress="return isNumber(event);" onkeyup="GetTotQty(this.id)" /></td>';
                        str = str + '<td><select class="form-control select2" style="width: 115px;" id="M_GSTCharge_' + Mlen + '" name="M_GSTCharge" onchange="GetTotQty(this.id)"></select></td>';
                        str = str + '<td><input type="text" class="form-control" id="M_Tax_' + Mlen + '" name="M_Tax" value="0"  readonly /></td>';
                        str = str + '<td><input type="text" class="form-control" value="' + dat.Subtotal + '"   id="M_total_' + Mlen + '" name="M_total" onkeypress="return isNumber(event);" readonly /></td>';
                        if (file1.files.length > 0) {
                            str = str + '<td><input type="file" class="form-control" name="Style_Img" id="Style_Img_' + Mlen + '" style="display:none;" multiple>';
                            str = str + '<input type="button" class="btn btn-info mr-3" value="Preview Image" onclick=ImageUpload(this.id,"preview","Style","Style_Img_' + Mlen + '") /></td>';
                        }
                        else {
                            str = str + '<td><input type="file" class="form-control" name="Style_Img" id="Style_Img_' + Mlen + '" style="display:none;" multiple>';
                            str = str + '<input type="button" class="btn btn-info mr-3" value="Upload Image" onclick=ImageUpload(this.id,"upload","Style","Style_Img_' + Mlen + '") /></td>';
                        }
                        str = str + '<td><input type="button" class="btn btn-info" value="View Image"  onclick=ImagePreview("Attr_Images' + Mlen + '") /><input type="hidden" id="Attr_Images' + Mlen + '" name="Attr_Images' + Mlen + '" value="' + path + '" class="form-control" /></td>';
                        str = str + '<td><button type="button" id="M_delete" class="deleteitemArr btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></button></td></tr>';
                        $("#MultiItemTab > tbody").append(str);
                        debugger;
                        MyRawItemArray = [];
                        var myListJosn = {
                            "Id": 0, "Poid": 0, "Segmenttypeid": SegTId, "Styleid": styleid, "Itemid": itemid, "ItemName": itemName, "Styledesc": styleDesc,
                            "Styleno": styleNo, "Brand": Brand, "Reqqty": ReqQty, "Item_Attribute_ID": AttrSizeId, "Sizeid": sizeid, "rate": Rate, "StyleImgCount": file1.files.length,
                            "Attribute_ID": attributeid, "Attribute_Value_ID": AttributeValId, "Totalqty": ReqQty, "ItemUnit": dat.ItemUnit, "SellRate": dat.SellRate, "GST": dat.GST, "Subtotal": dat.Subtotal,
                            "MyRawItem": MyRawItemArray
                        };

                        var $myListJosn2 = {
                            "Id": 0, "Poid": 0, "Segmenttypeid": SegTId, "Styleid": styleid, "Itemid": itemid, "ItemName": itemName, "Styledesc": styleDesc,
                            "Styleno": styleNo, "Brand": Brand, "Reqqty": ReqQty, "Item_Attribute_ID": AttrSizeId, "Sizeid": sizeid, "rate": Rate, "StyleImgCount": file1.files.length,
                            "Attribute_ID": attributeid, "Attribute_Value_ID": AttributeValId, "Totalqty": ReqQty, "ItemUnit": dat.ItemUnit, "SellRate": dat.SellRate, "GST": dat.GST, "Subtotal": dat.Subtotal
                        };

                        var file2 = document.querySelector('#Style_Img_' + Mlen);
                        file2.files = file1.files;
                        var emptyFile = document.createElement('input');
                        emptyFile.type = 'file';
                        file1.files = emptyFile.files;

                        MyItemArra.push(myListJosn);
                        $ItemArra.push($myListJosn2);
                        GetRawItemData();
                        GetQuoStyleRawItemData(dat.Id, Mlen);

                        //Get Tax List
                        $.ajax({
                            type: "POST",
                            url: '/quotation/getGSTTaxlist/',
                            data: '{}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (gstsdata) {
                                var result_ = JSON.parse(gstsdata);
                                $('#M_GSTCharge_' + Mlen).empty();
                                $('#M_GSTCharge_' + Mlen).append('<option value="select">Select</option>');
                                $.each(result_, function (i, item) {
                                    $('#M_GSTCharge_' + Mlen).append('<option value="' + item.Tax_Rate + '">' + item.Category_Name + '</option>');
                                });
                                $('#M_GSTCharge_' + Mlen).val(dat.GST);
                            },
                            failure: function (response) {
                                alert(response.d);
                            }
                        });
                    });
                }

            },
            failure: function (response) {
                alert(response.d);
            }
        });

    }

}


function getItemdetail() {
    $("#AttributeTable").empty();
    var itemid = $("#Itemid").attr("data-ID");
    if (itemid != null && itemid != "" && itemid != "0" && itemid != undefined) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '/Item/getAttributeValue/',
            data: "{'MyJson':'" + itemid + "','type':4}",
            contentType: "application/json;",
            success: function (Result) {
                debugger;
                Result = JSON.parse(Result);
                var tabstr = "";
                var len1 = 1;
                tabstr = tabstr + "<tr  class='attrbtab1'>";
                $.each(Result.Table, function (a, data) {
                    debugger
                    $("#AttributeId").val(data.Attribute_ID);

                    $.each(data.AttributName.split(","), function (b, edata) {

                        tabstr = tabstr + "<th>" + edata + "</th>";
                    });
                });
                tabstr = tabstr + "<th>Required Quantity</th><th>Cost Price Per Qty</th><th><p class='required'>Sell Price Per Qty</p></th><th><p class='required'>GST Charge(%)</p></th><th>Tax</th><th>Sub Total</th><th>Image</th>";
                tabstr = tabstr + "</tr>"


                $.each(Result.Table1, function (i, subcate) {
                    debugger;
                    len1 = len1 + i;
                    var attributename = subcate.AttributeValue;
                    var index1 = attributename.indexOf(',');
                    if (index1 == -1) {
                        attributename = attributename + ",";
                    }
                    attributename = attributename.split(',');
                    tabstr = tabstr + "<tr class='attrtabval'>";
                    var ck = 0;
                    $.each(subcate.Attribute_Value_ID.split("~"), function (k, e) {
                        if (e != "") {
                            tabstr = tabstr + "<td><input type='hidden' value='" + e + "' class='hid2'  />"
                            if (attributename[ck] != "") {
                                tabstr = tabstr + "<input type='hidden' class='hidval2' value='" + attributename[ck] + "' /> " + attributename[ck] + "";
                            }
                            tabstr = tabstr + "</td>";
                            ck = ck + 1;
                        }
                    });


                    var path = "";
                    var n = 0;
                    if (subcate.AttributeImage != "" && subcate.AttributeImage != null) {
                        $.each(subcate.AttributeImage.split(','), function (c, d) {
                            if (n == 0) {
                                path = "@itemimgpath_/" + d;
                            }
                            else {
                                path = path + ',' + "@itemimgpath_/" + d;
                            }
                            n = n + 1;
                        });
                    }
                    $("#ItemUnit").val(subcate.ItemUnit);

                    tabstr = tabstr + "<td><input type='text' class='form-control IAReqQty' value='' id='IAReqQty" + len1 + "' onkeypress='return isNumber(event)' onkeyup='SetTotQty()' /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control IARate' value='" + subcate.rate + "'  readonly /><input type='hidden' class='form-control IAsizeid' value='" + subcate.Item_Attribute_ID + "'  readonly /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control IASellRate' value=''  id='IASellRate" + len1 + "' onkeypress='return isNumber(event);' onkeyup='A_GetTotQty(this.id)' /></td>";
                    tabstr = tabstr + "<td><select class='form-control select2 IAGSTCharge' style='width:115px;' id='GSTCharge" + len1 + "' name='GSTCharge" + len1 + "' onchange='A_GetTotQty(this.id)'></select></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control IATax' value='' dataGST='" + subcate.Taxrate + "' id='IATax" + len1 + "' onkeypress='return isNumber(event);' readonly /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control IAtotal' value='' id='IAtotal" + len1 + "' onkeypress='return isNumber(event);' readonly /></td>";
                    tabstr = tabstr + "<td dataval='" + subcate.ImgCount + "'><input type='hidden' class='IAImgcount' id='IAImg_count" + len1 + "' value='" + subcate.ImgCount + "' />";
                    tabstr = tabstr + "<input type='button' class='btn btn-info' value='View Image' id='viewimage" + len1 + "'  onclick=GetImageUpload(this.id,'preview','Item') />";
                    tabstr = tabstr + "<input type='file' style='display:none;' id='ItemAttrimages" + len1 + "' name='ItemAttrimages_id' class='form-control' multiple /> <input type='hidden' id='Attrimages" + len1 + "' name='Attrimages" + len1 + "' value='" + path + "' class='form-control' /></td>";
                    tabstr = tabstr + "</tr>"

                });
                tabstr = "<tbody>" + tabstr + "</tbody>"
                $("#AttributeTable").append(tabstr);

                var tdlen = document.getElementById('AttributeTable').rows[0].cells.length;
                if (parseInt(tdlen) > 10) {
                    var wid = parseInt(tdlen) * 140;
                    $("#AttributeTable").attr('style', 'width:' + wid + 'px');
                    $("#AttributeTable").parent('div').attr('style', 'overflow-x:scroll;');
                }

                //Get Tax List
                $.ajax({
                    type: "POST",
                    url: '/quotation/getGSTTaxlist',
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (gstsdata) {

                        var result_ = JSON.parse(gstsdata);
                        $('.IAGSTCharge').empty();
                        $(".IAGSTCharge").append('<option value="select">Select</option>');
                        $.each(result_, function (i, item) {
                            $(".IAGSTCharge").append('<option value="' + item.Tax_Rate + '">' + item.Category_Name + '</option>');
                        });

                        var i = 1;
                        $("#AttributeTable tr.attrtabval").children('td').find(".IATax").each(function () {
                            var GST = this.getAttribute('dataGST');
                            $("#GSTCharge" + i).val(GST);
                            i++;
                        });

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });

            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}

function GetStyleData() {
    var styleid = $('#Styleid').attr("data-ID");

    if (styleid != null && styleid != "" && styleid != "0" && styleid != undefined) {
        $.ajax({
            type: "POST",
            url: '/CustomerEnquiry/GetStyleData/',
            data: "{'styleid':" + styleid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                result = JSON.parse(result);
                $("#Styledesc").val(result.Styledescription == null ? "" : result.Styledescription);
                $("#Styleno").val(result.StyleNo == null ? "" : result.StyleNo);

            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}

function SetTotQty() {
    var TotQty = 0;
    $("#AttributeTable tr.attrtabval").children('td').find(".IAReqQty").each(function () {
        var Qty = this.value == "" ? 0 : parseFloat(this.value);
        TotQty = TotQty + parseFloat(Qty);
    });
    $("#Reqqty").val(TotQty);
}

function A_GetTotQty(id) {
    debugger;
    var no = id.match(/\d+/);
    no = no != null ? no[0] : "";
    var RQty = $("#IAReqQty" + no).val();
    RQty = RQty != "" ? RQty : 0;
    var SellRate = $("#IASellRate" + no).val();
    SellRate = SellRate != "" ? SellRate : 0;
    var GST = $("#GSTCharge" + no).val();
    GST = GST != null && GST != "" && GST != "select" ? GST : 0;
    var tot = parseFloat(RQty) * parseFloat(SellRate);
    var taxv = (parseFloat(tot) * parseFloat(GST)) / 100;
    $("#IATax" + no).val(taxv);
    tot = parseFloat(tot) + parseFloat(taxv);
    $("#IAtotal" + no).val(parseFloat(tot).toFixed(2));
}

function ListVadation() {
    var msg = "Mandetory Information : \n";
    var blnFlag = true;

    if ($("#Segmenttypeid").val() == "" || $("#Segmenttypeid").val() == undefined || $("#Segmenttypeid").val() == "Select") {
        msg = msg + "* Please Select Segment Type. \n";
        blnFlag = false;
    }
    if ($("#Itemid").val() == "" || $("#Itemid").attr("data-ID") == "0" || $("#Itemid").attr("data-ID") == "") {
        msg = msg + "* Please Select Item. \n";
        blnFlag = false;
    }
    if ($("#Styleid").val() == "" || $("#Styleid").attr("data-ID") == "0" || $("#Styleid").attr("data-ID") == "") {
        msg = msg + "* Please Select Style. \n";
        blnFlag = false;
    }
    if ($("#Reqqty").val() == "") {
        msg = msg + "* Please Enter Item Req. Quantity. \n";
        blnFlag = false;
    }
    if (msg != "" && blnFlag == false) {
        swal("warning", msg, "error");
        return false;
    }

    return true;
}

function GetTotQty(id) {
    debugger;
    var no = id.match(/\d+/);
    no = no != null ? no[0] : "";
    var RQty = $("#M_ReqQty_" + no).val();
    RQty = RQty != "" ? RQty : 0;
    var SellRate = $("#M_SellRate_" + no).val();
    SellRate = SellRate != "" ? SellRate : 0;
    var GST = $("#M_GSTCharge_" + no).val();
    GST = GST != "" && GST != "select" ? GST : 0;
    var tot = parseFloat(RQty) * parseFloat(SellRate);
    var taxv = (parseFloat(tot) * parseFloat(GST)) / 100;
    $("#M_Tax_" + no).val(taxv);
    tot = parseFloat(tot) + parseFloat(taxv);
    $("#M_total_" + no).val(parseFloat(tot).toFixed(2));
    MyItemArra[no].SellRate = SellRate;
    MyItemArra[no].GST = GST;
    MyItemArra[no].Subtotal = tot;

    $ItemArra[no].SellRate = SellRate;
    $ItemArra[no].GST = GST;
    $ItemArra[no].Subtotal = tot;


    QuationCal();
}

function QuationCal() {
    debugger;
    var totRQ = 0;
    var salP = 0;
    var subtot = 0;
    var st = 0;
    var $dataRow = $("#MultiItemTab > tbody > tr");
    var rowlen = $dataRow.length;
    if (rowlen > 0) {
        var len = 0;
        for (var i = 0; i < rowlen; i++) {
            var tot = $("#M_total_" + i).val();
            tot = tot == "" ? 0 : tot;
            subtot = parseFloat(subtot) + parseFloat(tot);
        }
    }
    subtot = parseFloat(subtot).toFixed(2);
    $("#Total").val(subtot);
    $("#Htotal").val(subtot);
    $("#GrandTotal").val(subtot);
    GetGrandTot();
}

function GetGrandTot() {
    var $dataRows = $("#OtherChargeDetails tr:not('.rowtitle')");
    var len = $dataRows.length;
    var OC_tot1 = 0;
    var potot = $("#Total").val();
    if (len > 0) {
        $dataRows.each(function (i) {
            $(this).find('.ocdata').each(function (i) {
                if (i == 4) {
                    var Octot = ($(this).val());
                    Octot = Octot == "" ? 0 : Octot;
                    OC_tot1 = parseFloat(OC_tot1) + parseFloat(Octot);
                }
            });
        });

        var Grandtot = parseFloat(potot) + parseFloat(OC_tot1);
        $("#GrandTotal").val(Grandtot);
    }
    else {
        $("#GrandTotal").val(potot);
    }
}


function AddtoList() {
    if (ListVadation() == true) {
        var $dataRow = $("#AttributeTable > tbody > tr:not('.attrbtab1')");
        var rowlen = $dataRow.length;
        if (rowlen > 0) {
            var len = 0;
            $dataRow.each(function (i) {
                len = len + 1;
                //get row of sizetable
                var attributeval = "";
                var ReqQty = "";
                var Rate = "";
                var sizeid = "";
                var Sellrate = "";
                var GST = "";
                var taxval = "";
                var tot = "";
                var imgcount = "";
                $(this).children('td').each(function (i) {
                    debugger;
                    $(this).find(".hid2").each(function () {
                        if (i == 0) {
                            attributeval = this.value;
                        }
                        else {
                            attributeval = attributeval + "," + this.value;
                        }
                    });

                    $(this).find(".IAReqQty").each(function () {
                        ReqQty = this.value;
                    });

                    $(this).find(".IARate").each(function () {
                        Rate = this.value;
                    });

                    $(this).find(".IAsizeid").each(function () {
                        sizeid = this.value;
                    });
                    $(this).find(".IASellRate").each(function () {
                        Sellrate = this.value;
                    });
                    $(this).find(".IAGSTCharge ").each(function () {
                        GST = this.value;
                    });
                    $(this).find(".IATax").each(function () {
                        taxval = this.value;
                    });
                    $(this).find(".IAtotal").each(function () {
                        tot = this.value;
                    });
                    $(this).find(".IAImgcount").each(function () {
                        imgcount = this.value;
                    });

                });
                var SegTId = $("#Segmenttypeid").val();
                var SegTName = $("#Segmenttypeid option:selected").text();
                var itemid = $("#Itemid").attr("data-ID");
                var itemName = $("#Itemid").val();
                var styleid = $("#Styleid").attr("data-ID");
                var styleName = $("#Styleid").val();
                var styleDesc = $("#Styledesc").val();
                var styleNo = $("#Styleno").val();
                var Brand = $("#Brand").val();
                var AttrSizeId = $("#AttrSizeId").val();
                var attributeid = $("#AttributeId").val();
                var $MdataRow = $("#MultiItemTab > tbody > tr");
                var file1 = document.querySelector('#styleimg');
                var path = $("#Attrimages" + len).val();
                var ReqQty = $("#Reqqty").val();
                var ItemUnit = $("#ItemUnit").val();

                var Mlen = $MdataRow.length;
                var str = '<tr data="' + Mlen + '">';
                str = str + '<td>' + SegTName + '<input type="hidden" id="M_SegTypeID_' + Mlen + '" name="M_SegTypeID" value="' + SegTId + '" /></td>';
                str = str + '<td>' + itemName + '<input type="hidden" id="M_ItemID_' + Mlen + '" name="M_ItemID" value="' + itemid + '" /></td>';
                str = str + '<td>' + styleName + '<input type="hidden" id="M_styleID_' + Mlen + '" name="M_styleID" value="' + styleid + '" /></td>';
                str = str + '<td>' + styleDesc + '<input type="hidden" id="M_styleDesc_' + Mlen + '" name="M_styleDesc" value="' + styleDesc + '" /></td>';
                str = str + '<td>' + styleNo + '<input type="hidden" id="M_styleNo_' + Mlen + '" name="M_styleNo" value="' + styleNo + '" /></td>';
                str = str + '<td>' + Brand + '<input type="hidden" id="M_Brand_' + Mlen + '" name="M_Brand" value="' + Brand + '" /></td>';
                str = str + '<td>' + ReqQty + '<input type="hidden" id="M_ReqQty_' + Mlen + '" name="M_ReqQty" value="' + ReqQty + '" /></td>';
                str = str + '<td>' + ItemUnit + '<input type="hidden" id="M_ItemUnit_' + Mlen + '" name="M_ItemUnit" value="' + ItemUnit + '" /></td>';
                str = str + '<td><input type="text" class="form-control" id="M_Rate_' + Mlen + '" name="M_Rate" value="' + Rate + '"  readonly /></td>';
                str = str + '<td><input type="text" class="form-control" value="' + Sellrate + '"  id="M_SellRate_' + Mlen + '" name="M_SellRate" onkeypress="return isNumber(event);" onkeyup="GetTotQty(this.id)" /></td>';
                str = str + '<td><select class="form-control select2" style="width: 115px;" id="M_GSTCharge_' + Mlen + '" name="M_GSTCharge" onchange="GetTotQty(this.id)"></select></td>';
                str = str + '<td><input type="text" class="form-control" id="M_Tax_' + Mlen + '" name="M_Tax" value="' + taxval + '"  readonly /></td>';
                str = str + '<td><input type="text" class="form-control" value="' + tot + '"   id="M_total_' + Mlen + '" name="M_total" onkeypress="return isNumber(event);" readonly /></td>';
                if (file1.files.length > 0) {
                    str = str + '<td><input type="file" class="form-control" name="Style_Img" id="Style_Img_' + Mlen + '" style="display:none;" multiple>';
                    str = str + '<input type="button" class="btn btn-info mr-3" value="Preview Image" onclick=ImageUpload(this.id,"preview","Style","Style_Img_' + Mlen + '") /></td>';
                }
                else {
                    str = str + '<td><input type="file" class="form-control" name="Style_Img" id="Style_Img_' + Mlen + '" style="display:none;" multiple>';
                    str = str + '<input type="button" class="btn btn-info mr-3" value="Upload Image" onclick=ImageUpload(this.id,"upload","Style","Style_Img_' + Mlen + '") /></td>';
                }
                str = str + '<td><input type="button" class="btn btn-info" value="View Image"  onclick=ImagePreview("Attr_Images' + Mlen + '") /><input type="hidden" id="Attr_Images' + Mlen + '" name="Attr_Images' + Mlen + '" value="' + path + '" class="form-control" /></td>';
                str = str + '<td><button type="button" id="M_delete" class="deleteitemArr btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></button></td></tr>';
                $("#MultiItemTab > tbody").append(str);
                debugger;
                MyRawItemArray = [];
                var myListJosn = {
                    "Id": 0, "Quotationid": 0, "Segmenttypeid": SegTId, "Styleid": styleid, "Itemid": itemid, "ItemName": itemName, "Styledesc": styleDesc,
                    "Styleno": styleNo, "Brand": Brand, "Reqqty": ReqQty, "Item_Attribute_ID": AttrSizeId, "Sizeid": sizeid, "rate": Rate, "StyleImgCount": file1.files.length,
                    "Attribute_ID": attributeid, "Attribute_Value_ID": attributeval, "Totalqty": ReqQty, "ItemUnit": ItemUnit, "SellRate": Sellrate, "GST": GST, "Subtotal": tot,
                    "MyRawItem": MyRawItemArray
                };

                var $myListJosn2 = {
                    "Id": 0, "Quotationid": 0, "Segmenttypeid": SegTId, "Styleid": styleid, "Itemid": itemid, "ItemName": itemName, "Styledesc": styleDesc,
                    "Styleno": styleNo, "Brand": Brand, "Reqqty": ReqQty, "Item_Attribute_ID": AttrSizeId, "Sizeid": sizeid, "rate": Rate, "StyleImgCount": file1.files.length,
                    "Attribute_ID": attributeid, "Attribute_Value_ID": attributeval, "Totalqty": ReqQty, "ItemUnit": ItemUnit, "SellRate": Sellrate, "GST": GST, "Subtotal": tot
                };

                var file2 = document.querySelector('#Style_Img_' + Mlen);
                file2.files = file1.files;
                var emptyFile = document.createElement('input');
                emptyFile.type = 'file';
                file1.files = emptyFile.files;

                MyItemArra.push(myListJosn);
                $ItemArra.push($myListJosn2);

                //Get Tax List
                $.ajax({
                    type: "POST",
                    url: '/quotation/getGSTTaxlist',
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (gstsdata) {

                        var result_ = JSON.parse(gstsdata);
                        $('#M_GSTCharge_' + Mlen).empty();
                        $('#M_GSTCharge_' + Mlen).append('<option value="select">Select</option>');
                        $.each(result_, function (i, item) {
                            $('#M_GSTCharge_' + Mlen).append('<option value="' + item.Tax_Rate + '">' + item.Category_Name + '</option>');
                        });
                        $('#M_GSTCharge_' + Mlen).val(GST);

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            });

            $("#Segmenttypeid").val("");
            $("#Segmenttypeid").select2().trigger('change');
            $("#Itemid").attr("data-ID", "0");
            $("#Itemid").val("");
            $("#Styleid").attr("data-ID", "0");
            $("#Styleid").val("");
            $("#Styledesc").val("");
            $("#Styleno").val("");
            $("#Brand").val("");
            $("#Reqqty").val("");
            $("#ItemUnit").val("");
            $("#AttributeTable > tbody").empty();
            QuationCal();
        }
    }
}

$(document).on("click", ".deleteitemArr", function () {
    var id = $(this).closest("tr").attr('data');
    MyItemArra.splice(id, 1);
    // alert(id);
    $(this).closest("tr").remove(); // closest used to remove the respective 'tr' in which I have my controls
    //  $('#hiderawitemtable tr').find('#' + id).remove();
    $('table#MultiItemTab > tbody > tr#' + id).remove();
    
});

$("#savepo").on("click", function () {

    debugger;
    ErrorMessage = "";
    var isvalid = true;
    if (MyItemArra.length == 0) {
        ErrorMessage += "Please Check Style Details \n";
        isvalid = false;
    }
    var custid = $('#customerid').attr("data-ID");
    var addressid = $('#Addressid').val();

    if (custid == "") {
        ErrorMessage += "Please Select Customer \n";
        isvalid = false;
    }

    if (addressid == "0" || addressid == "") {
        ErrorMessage += "Please Select Customer Address. \n";
        isvalid = false;
    }

    var Quotrefno = $("#Quotrefno").val();
    if (Quotrefno == "0") {
        ErrorMessage += "Please Select Enquiry No. \n";
        isvalid = false;
    }
    if (ErrorMessage.length > 0) {
        swal({
            title: 'Error',
            text: ErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        isvalid = false;
    }
    else {
        SavePO();
    }
    return isvalid;
});


function SavePO() {
    debugger;
    var Id = $("#Id").val();
    var POno = $("#POno").val();
    var POdate = $("#POdate").val();
    var customerid = $("#customerid").attr("data-ID");
    var Addressid = $("#Addressid").val();
    var Quotrefno = $("#Quotrefno option:selected").text();
    var QuotId = $("#Quotrefno").val();
    var Sampling = false
    var Quotdate = $("#Quotdate").val();
    var issampling = $("#issampling").val();
    if (issampling=="true") {
        Sampling = true;
    }
    
    var Tentivedate = $("#Tentivedate").val();
    var Remark = $("#Remark").val();
    var Approvalstatus = $("#Approvalstatus").val();
    if (parseInt(Approvalstatus) == 1) {
        Approvalstatus = true;
    }
    else if (parseInt(Approvalstatus) == 0) {
        Approvalstatus = false;
    }
    var Comment = $("#comment").val();
    var Total = $("#Total").val();
    var hidIsReadyMate = $("#hidIsReadyMate").val();
    var GrandTotal = $("#GrandTotal").val();

    OtherChare = [];
    var $OC_dataRows = $("#OtherChargeDetails > tbody > tr");
    var OClenght = $OC_dataRows.length;
    for (var k = 0; k < OClenght; k++) {
        var otherchargeid = $("#otherchargeid" + k).val();
        var OC_Amount = $("#OC_Amount" + k).val();
        var OC_GSTid = $("#OC_GSTid" + k).val();
        var OC_GSTAmt = $("#OC_GSTAmt" + k).val();
        var OC_total = $("#OC_total" + k).val();
        var ocTid = $("#ocTid").val();

        var jsonaOC = { "Id": 0, "TXID": 0, "OtherChargeId": otherchargeid, "OtherChargeValue": OC_Amount, "GST": OC_GSTid };
        OtherChare.push(jsonaOC);
    }

    var $data = {
        "Id": Id, "QuotationID": QuotId, "Quotrefno": Quotrefno, "Quotdate": Quotdate, "customerid": customerid, "PODate": POdate, "POno": POno,
        "Remark": Remark, "Approvalstatus": Approvalstatus, "Sampling": Sampling,
        "Comments": Comment, "Costprice": 0, "Saleprice": 0, "Subtotal": 0, 'Tax': 0, 'Tentivedate': Tentivedate, "Total": Total, "Addressid": Addressid,
        'IsReadmade': hidIsReadyMate, "GrandTotal": GrandTotal, "StyleEnt": $ItemArra, "POOtherChargeEntity": OtherChare
    }


    debugger;
    var formData = new FormData();
    var totalFiles = MyItemArra.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.querySelector('#Style_Img_' + i);
        for (var k = 0; k < file.files.length; k++) {
            formData.append("Style_Img", file.files[k]);
        }
    }

    formData.append("data", JSON.stringify($data));


    debugger;
    $.ajax({
        url: '/CustomerPO/AddUpdate/',
        type: 'POST',
        dataType: 'json',
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            debugger;
            if (data != undefined) {
                swal({
                    title: 'Success',
                    text: 'PO Submit Successfully.',
                    type: 'success',
                    confirmButtonText: "OK"
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            window.location.href = '/CustomerPO/Index';
                        }
                    });
            }
        }
    });
}



///////// PO Index Start

function deleteCustPO(id) {
    var _id = id;
    var param = { 'id': _id };

    swal({
        title: 'Confirmation',
        text: 'Are You Sure Delete Purchase Order?',
        showConfirmButton: true,
        showCancelButton: true,
        closeOnConfirm: true,
        closeOnCancel: true,
        confirmButtonText: 'Confirm',
        confirmButtonColor: '#8CD4F5',
        cancelButtonText: 'Cancel'
    },
        function (isConfirm) {

            if (isConfirm) {

                $.ajax({
                    type: "POST",
                    url: '/CustomerPO/Delete/',
                    data: JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {

                        if (result == true) {
                            //alert("true");
                            window.location.reload();
                        }

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });

            } else {
                swal("Cancelled", ")", "error");
            }
        });


}


function CancleCPO(id) {
    var cpoid = id;
    var param = { 'id': cpoid };

    swal({
        title: 'Confirmation',
        text: 'Are You Sure Rejected This Purchase Order?',
        showConfirmButton: true,
        showCancelButton: true,
        closeOnConfirm: true,
        closeOnCancel: true,
        confirmButtonText: 'Confirm',
        confirmButtonColor: '#8CD4F5',
        cancelButtonText: 'Cancel'
    },
        function (isConfirm) {

            if (isConfirm) {

                $.ajax({
                    type: "POST",
                    url: '/CustomerPO/RejectedCPO/',
                    data: JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {

                        if (result == true) {
                            swal({
                                title: 'Confirmation',
                                text: 'Record Rejected Successfully!',
                                showConfirmButton: true,
                                closeOnConfirm: true,
                                closeOnCancel: true,
                                confirmButtonText: 'OK',
                                confirmButtonColor: '#8CD4F5',
                            },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        window.location.reload();
                                    }
                                }
                            )

                        }

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });

            } else {
                swal("Cancelled", ")", "error");
            }
        });


}


function GetData(Status, Name) {
    $("#Pending").empty();
    $("#Completed").empty();
    $("#InProgress").empty();
    $("#Rejected").empty();

    $("#" + Name).html(loadingHTML());
    $("#" + Name).load("/CustomerPO/GetStatusData/", { 'IsStatus': Status });

}

function setDateformat(Dt) {

    var dt1 = Dt.substring(6, Dt.length);
    dt1 = dt1.substring(0, dt1.length - 2);
    var today = new Date(parseInt(dt1));
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    // var today2 = today.setMonth(mm + 1);
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    today = mm + '/' + dd + '/' + yyyy;

    return today;
}

function loadingHTML() {
    return $("#divLoadingContent").html();
}

function FilterDataShow() {
    debugger;
    var formDate = $("#fromdate").val();
    var toDate = $("#todate").val();
    var Name = $("#Tablist > .active").attr('id');
    var Status = $("#" + Name).attr("datavalue");

    $("#Pending").empty();
    $("#Completed").empty();
    $("#InProgress").empty();
    $("#Rejected").empty();

    $("#" + Name).html(loadingHTML());
    $("#" + Name).load("/CustomerPO/GetFilterStatusData/", { 'fromdate': formDate, 'todate': toDate, 'IsStatus': Status });
}

function GetPOProduct(POId) {
    if (POId != "") {
        if (parseInt(POId) > 0) {
            $("#POProductmodel").modal("show");
            $.ajax({
                type: "POST",
                url: '/CustomerPO/GetPOStyleData/',
                data: "{'POId':" + POId + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    debugger;
                    result = JSON.parse(result);
                    var len = 0;
                    if (result != null) {
                        $("#EnqProductTab > tbody").empty();
                        $.each(result, function (e, dat) {
                            var Rate = dat.rate;
                            var sizeid = dat.Sizeid;
                            var attributeval = dat.AttrValEnt != null ? dat.AttrValEnt.Attribute_Value : "";
                            var SegTId = dat.Segmenttypeid;
                            var SegTName = dat.SegTypeEnt != null ? dat.SegTypeEnt.Segmenttype : "";
                            var itemid = dat.Itemid;
                            var itemName = dat.ItemEnt != null ? dat.ItemEnt.ItemName : "";
                            var styleid = dat.Styleid;
                            var styleName = dat.styleEnt != null ? dat.styleEnt.Stylename : "";
                            var styleDesc = dat.Styledesc;
                            var styleNo = dat.Styleno;
                            var Brand = dat.Brand;
                            var AttrSizeId = dat.Sizeid;
                            var attributeid = dat.Attribute_ID;
                            var $MdataRow = $("#MultiItemTab > tbody > tr");
                            var file1 = document.querySelector('#styleimg');
                            var path = $("#Attrimages" + len).val();
                            var ReqQty = dat.Reqqty;
                            var Mlen = $MdataRow.length;
                            var str = '<tr data="' + Mlen + '">';
                            str = str + '<td>' + SegTName + '<input type="hidden" id="M_SegTypeID_' + Mlen + '" name="M_SegTypeID" value="' + SegTId + '" /></td>';
                            str = str + '<td>' + itemName + '<input type="hidden" id="M_ItemID_' + Mlen + '" name="M_ItemID" value="' + itemid + '" /></td>';
                            str = str + '<td>' + styleName + '<input type="hidden" id="M_styleID_' + Mlen + '" name="M_styleID" value="' + styleid + '" /></td>';
                            str = str + '<td>' + styleDesc + '<input type="hidden" id="M_styleDesc_' + Mlen + '" name="M_styleDesc" value="' + styleDesc + '" /></td>';
                            str = str + '<td>' + styleNo + '<input type="hidden" id="M_styleNo_' + Mlen + '" name="M_styleNo" value="' + styleNo + '" /></td>';
                            str = str + '<td>' + Brand + '<input type="hidden" id="M_Brand_' + Mlen + '" name="M_Brand" value="' + Brand + '" /></td>';
                            str = str + '<td>' + ReqQty + '<input type="hidden" id="M_ReqQty_' + Mlen + '" name="M_ReqQty" value="' + ReqQty + '" /></td>';
                            str = str + '<td>' + dat.ItemUnit + '<input type="hidden" id="M_ItemUnit_' + Mlen + '" name="M_ItemUnit" value="' + dat.ItemUnit + '" /></td></tr>';
                            $("#EnqProductTab > tbody").append(str);

                        });
                    }
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    }
}

//////// PO Index End

////PO Edit Start/////////////
function othercharge1() {
    var $dataRows = $("#OtherChargeDetails tr:not('.rowtitle')");
    var sizerowlenght = $dataRows.length;

    if (sizerowlenght > 0) {

        $("#OtherChargeDetails").show();
        $("#OC_GrandTotal_id").show();
        //Get Tax List
        $.ajax({
            type: "POST",
            url: '/quotation/getGSTTaxlist/',
            data: '{}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (gstsdata) {
                var result_ = JSON.parse(gstsdata);
                $('.OC_GST').empty();
                $(".OC_GST").append('<option value="select">Select</option>');
                $.each(result_, function (i, item) {
                    $(".OC_GST").append('<option value="' + item.Tax_Rate + '">' + item.Category_Name + '</option>');
                });
                $dataRows.each(function (i) {
                    var gstval = $("#hidgst" + i).val();
                    gstval = parseInt(gstval);
                    $("#OC_GSTid" + i).attr("style", "width:115px;");
                    $("#OC_GSTid" + i).val(gstval);
                    $("#OC_GSTid" + i).select2().trigger('change');
                });
            },
            failure: function (response) {
                alert(response.d);
            }
        });


    }
    else {
        $("#OtherChargeDetails").hide();
        $("#OC_GrandTotal_id").hide();
    }

}

function GetPOStyleData() {
    var Id = $("#Id").val();
    if (Id != "") {
        if (parseInt(Id) > 0) {
            $.ajax({
                type: "POST",
                url: '/CustomerPO/GetPOStyleData/',
                data: "{'POId':" + Id + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    debugger;
                    result = JSON.parse(result);
                    var len = 0;
                    if (result != null) {
                        $.each(result, function (e, dat) {
                            var Rate = dat.rate;
                            var sizeid = dat.Sizeid;
                            var attributeval = dat.AttrValEnt != null ? dat.AttrValEnt.Attribute_Value : "";
                            var SegTId = dat.Segmenttypeid;
                            var SegTName = dat.SegTypeEnt != null ? dat.SegTypeEnt.Segmenttype : "";
                            var itemid = dat.Itemid;
                            var itemName = dat.ItemEnt != null ? dat.ItemEnt.ItemName : "";
                            var styleid = dat.Styleid;
                            var styleName = dat.styleEnt != null ? dat.styleEnt.Stylename : "";
                            var styleDesc = dat.Styledesc;
                            var styleNo = dat.Styleno;
                            var Brand = dat.Brand;
                            var AttrSizeId = dat.Sizeid;
                            var attributeid = dat.Attribute_ID;
                            var AttributeValId = dat.Attribute_Value_ID;
                            var $MdataRow = $("#MultiItemTab > tbody > tr");
                            var file1 = document.querySelector('#styleimg');
                            var path = $("#Attrimages" + len).val();
                            var ReqQty = dat.Reqqty;
                            var Mlen = $MdataRow.length;
                            var str = '<tr data="' + Mlen + '">';
                            str = str + '<td>' + SegTName + '<input type="hidden" id="M_SegTypeID_' + Mlen + '" name="M_SegTypeID" value="' + SegTId + '" /></td>';
                            str = str + '<td>' + itemName + '<input type="hidden" id="M_ItemID_' + Mlen + '" name="M_ItemID" value="' + itemid + '" /></td>';
                            str = str + '<td>' + styleName + '<input type="hidden" id="M_styleID_' + Mlen + '" name="M_styleID" value="' + styleid + '" /></td>';
                            str = str + '<td>' + styleDesc + '<input type="hidden" id="M_styleDesc_' + Mlen + '" name="M_styleDesc" value="' + styleDesc + '" /></td>';
                            str = str + '<td>' + styleNo + '<input type="hidden" id="M_styleNo_' + Mlen + '" name="M_styleNo" value="' + styleNo + '" /></td>';
                            str = str + '<td>' + Brand + '<input type="hidden" id="M_Brand_' + Mlen + '" name="M_Brand" value="' + Brand + '" /></td>';
                            str = str + '<td>' + ReqQty + '<input type="hidden" id="M_ReqQty_' + Mlen + '" name="M_ReqQty" value="' + ReqQty + '" /></td>';
                            str = str + '<td>' + dat.ItemUnit + '<input type="hidden" id="M_ItemUnit_' + Mlen + '" name="M_ItemUnit" value="' + dat.ItemUnit + '" /></td>';
                            str = str + '<td><input type="text" class="form-control" id="M_Rate_' + Mlen + '" name="M_Rate" value="' + dat.rate + '"  readonly /></td>';
                            str = str + '<td><input type="text" class="form-control" value="' + dat.SellRate + '"  id="M_SellRate_' + Mlen + '" name="M_SellRate" onkeypress="return isNumber(event);" onkeyup="GetTotQty(this.id)" /></td>';
                            str = str + '<td><select class="form-control select2" style="width: 115px;" id="M_GSTCharge_' + Mlen + '" name="M_GSTCharge" onchange="GetTotQty(this.id)"></select></td>';
                            str = str + '<td><input type="text" class="form-control" id="M_Tax_' + Mlen + '" name="M_Tax" value="0"  readonly /></td>';
                            str = str + '<td><input type="text" class="form-control" value="' + dat.Subtotal + '"   id="M_total_' + Mlen + '" name="M_total" onkeypress="return isNumber(event);" readonly /></td>';
                            if (file1.files.length > 0) {
                                str = str + '<td><input type="file" class="form-control" name="Style_Img" id="Style_Img_' + Mlen + '" style="display:none;" multiple>';
                                str = str + '<input type="button" class="btn btn-info mr-3" value="Preview Image" onclick=ImageUpload(this.id,"preview","Style","Style_Img_' + Mlen + '") /></td>';
                            }
                            else {
                                str = str + '<td><input type="file" class="form-control" name="Style_Img" id="Style_Img_' + Mlen + '" style="display:none;" multiple>';
                                str = str + '<input type="button" class="btn btn-info mr-3" value="Upload Image" onclick=ImageUpload(this.id,"upload","Style","Style_Img_' + Mlen + '") /></td>';
                            }
                            str = str + '<td><input type="button" class="btn btn-info" value="View Image"  onclick=ImagePreview("Attr_Images' + Mlen + '") /><input type="hidden" id="Attr_Images' + Mlen + '" name="Attr_Images' + Mlen + '" value="' + path + '" class="form-control" /></td>';
                            str = str + '<td><button type="button" id="M_delete" class="deleteitemArr btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></button></td></tr>';
                            $("#MultiItemTab > tbody").append(str);
                            debugger;
                            MyRawItemArray = [];
                            var myListJosn = {
                                "Id": dat.Id, "Poid": dat.Poid, "Segmenttypeid": SegTId, "Styleid": styleid, "Itemid": itemid, "ItemName": itemName, "Styledesc": styleDesc,
                                "Styleno": styleNo, "Brand": Brand, "Reqqty": ReqQty, "Item_Attribute_ID": AttrSizeId, "Sizeid": sizeid, "rate": Rate, "StyleImgCount": file1.files.length,
                                "Attribute_ID": attributeid, "Attribute_Value_ID": AttributeValId, "Totalqty": ReqQty, "ItemUnit": dat.ItemUnit, "SellRate": dat.SellRate, "GST": dat.GST, "Subtotal": dat.Subtotal,
                                "MyRawItem": MyRawItemArray
                            };

                            var $myListJosn2 = {
                                "Id": dat.Id, "Poid": dat.Poid, "Segmenttypeid": SegTId, "Styleid": styleid, "Itemid": itemid, "ItemName": itemName, "Styledesc": styleDesc,
                                "Styleno": styleNo, "Brand": Brand, "Reqqty": ReqQty, "Item_Attribute_ID": AttrSizeId, "Sizeid": sizeid, "rate": Rate, "StyleImgCount": file1.files.length,
                                "Attribute_ID": attributeid, "Attribute_Value_ID": AttributeValId, "Totalqty": ReqQty, "ItemUnit": dat.ItemUnit, "SellRate": dat.SellRate, "GST": dat.GST, "Subtotal": dat.Subtotal
                            };

                            var file2 = document.querySelector('#Style_Img_' + Mlen);
                            file2.files = file1.files;
                            var emptyFile = document.createElement('input');
                            emptyFile.type = 'file';
                            file1.files = emptyFile.files;

                            MyItemArra.push(myListJosn);
                            $ItemArra.push($myListJosn2);

                            //Get Tax List
                            $.ajax({
                                type: "POST",
                                url: '/quotation/getGSTTaxlist/',
                                data: '{}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (gstsdata) {
                                    var result_ = JSON.parse(gstsdata);
                                    $('#M_GSTCharge_' + Mlen).empty();
                                    $('#M_GSTCharge_' + Mlen).append('<option value="select">Select</option>');
                                    $.each(result_, function (i, item) {
                                        $('#M_GSTCharge_' + Mlen).append('<option value="' + item.Tax_Rate + '">' + item.Category_Name + '</option>');
                                    });
                                    $('#M_GSTCharge_' + Mlen).val(dat.GST);
                                },
                                failure: function (response) {
                                    alert(response.d);
                                }
                            });
                        });
                    }
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    }
}
////PO Edit End/////////////