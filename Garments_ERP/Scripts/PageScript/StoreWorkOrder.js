﻿
var SWO_ItemArray = [];
var SWO_BOMArrya = [];
$(document).ready(function () {
    var pathsdat = window.location.pathname;
    pathsdat = pathsdat.split('/');
    if (pathsdat.length == 2) {
        pathsdat[2] = "Index";
    }
    if (pathsdat[2] == "Edit") {
        
    }
    else if (pathsdat[2] == "Index") {
        $("#bs_datepicker_component_container > .ui-datepicker-inline").attr('style', 'display:none;');
        $("#bs_datepicker_component_container1 > .ui-datepicker-inline").attr('style', 'display:none;');

        GetData(2, 'Pending');

        var param = { 'Type': 15, 'attribute': '' };
        $.ajax({
            type: "POST",
            url: '/CustomerEnquiry/StatusCount/',
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                result = JSON.parse(result);
                $.each(result.Table, function (i, data) {
                    $("#Pendingcount").text(data.pending);
                    $("#InProgresscount").text(data.inprogress);
                    $("#Completedcount").text(data.completed);
                    $("#Rejectedcount").text(data.Rejected);
                });
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    else {
        $("#bs_datepicker_component_container > .ui-datepicker-inline").attr("style", "Display:none;");
        $('.select2').select2();
        $('.select2').width("100%");

        var tempdate = new Date();
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        // var today2 = today.setMonth(mm + 1);
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        today = mm + '/' + dd + '/' + yyyy;

        var actualDate = new Date();
        var nextMonth = new Date(actualDate.getFullYear(), actualDate.getMonth() + 1, actualDate.getDate());
        dd = nextMonth.getDate();
        mm = nextMonth.getMonth() + 1;
        yyyy = nextMonth.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var nexttentivedate = mm + '/' + dd + '/' + yyyy;

        $("#SWO_Date").datepicker({
            //setDate: currentDate,
            format: 'mm/dd/yy',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            onClose: function (dateText, inst) {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            },
            maxDate: 0

        });
        $("#SWO_Date").val(today);

    }

    var roleid = $("#roleid").val();
    if (parseInt(roleid) != 4) {
        $(".approval").show();
    }
    else {
        $(".approval").hide();
    }
});

function Validation() {

    var status = false;
    var msg = "";
    var BrandName = $("#BrandName").val();
    if (BrandName == "" || BrandName == undefined || BrandName == null) {
        msg = "Plese Enter Brand Name. \n";
        status = true;
    }
    var itemcatName = $("#SWO_itemcatNameid").val();
    var itemcatNameid = $("#SWO_itemcatNameid").attr("data-id");
    if (itemcatName == "" || itemcatNameid == "" || itemcatName == null || itemcatNameid == null || itemcatName == undefined || itemcatNameid == undefined) {
        msg = "Plese Select Item Category. \n";
        status = true;
    }
    var StoreItem = $("#StoreItemid").val();
    var StoreItemId = $("#StoreItemid").attr("data-id");
    if (StoreItem == "" || StoreItemId == "" || StoreItem == null || StoreItemId == null || StoreItem == undefined || StoreItemId == undefined) {
        msg = "Plese Select Item Store Name. \n";
        status = true;
    }
    var ItemQty = $("#ItemQty").val();
    if (ItemQty == "" || ItemQty == undefined || ItemQty == null) {
        msg = "Plese Enter Item Quantity. \n";
        status = true;
    }
    if (msg != "") {
        swal("warning", msg, "error");
        window.onkeydown = null;
        window.onfocus = null;
    }
    return status;
}

function AddItemRow() {
    if (Validation() == false) {    

            var BrandName = $("#BrandName").val();
            var itemcatName = $("#SWO_itemcatNameid").val();
            var itemcatNameid = $("#SWO_itemcatNameid").attr("data-id");
            var StoreItem = $("#StoreItemid").val();
            var StoreItemId = $("#StoreItemid").attr("data-id");
            var ItemCode = $("#ItemCode").val();
            var ItemSizeId = $("#StoreItemid").attr("data-aid");
            var ItemUnit = $("#ItemUnit").val();
            var ItemUnitId = $("#ItemUnit").attr("data-id");
            var ItemQty = $("#ItemQty").val();

            var ItemJson = {
                "SWO_Item_DetailsId": 0, "Store_WO_Id": 0, "BrandName": BrandName, "ItemCategoryId": itemcatNameid, "itemcatName": itemcatName,
                "ItemId": StoreItemId, "ItemName": StoreItem, "ItemCode": ItemCode, "Item_Attribute_Id": ItemSizeId,
                "UnitId": ItemUnitId, "UnitName": ItemUnit, "Quantity": ItemQty
            };
            SWO_ItemArray.push(ItemJson);
            ItemRowData();
            clearAll();
        
        
    }
}

function CheckBOM() {
    var StoreItemId = $("#StoreItemid").attr("data-id");
    var ItemSizeId = $("#StoreItemid").attr("data-aid");
    var ItemQty = $("#ItemQty").val();
    if (StoreItemId != null || StoreItemId != undefined || ItemSizeId != undefined || ItemSizeId != null) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '/StoreWorkOrder/CheckBOM/',
            data: "{'StoreItemId':" + StoreItemId + ",'ItemSizeId':" + ItemSizeId + ",'ItemQty':" + ItemQty+"}",
            contentType: "application/json;",
            success: function (Result) {
                debugger;
                Result = JSON.parse(Result);
                if (Result.length > 0) {
                    $.each(Result, function (i, dat) {
                        debugger;
                        var BOMJson = {
                            "SWO_ItemId": StoreItemId, "BOM_ID": dat.BOM_ID, "ItemSubCategoryId": dat.ItemSubCategoryId, "Itemsubcategory": dat.Itemsubcategory, "ItemId": dat.ItemId, "ItemName": dat.ItemName,
                            "Item_Attribute_ID": dat.Item_Attribute_ID, "SupplierId": dat.SupplierId, "SupplierName": dat.SupplierName, "UnitId": dat.UnitId, "ItemUnit": dat.ItemUnit,
                            "RequiredQty": dat.RequiredQty, "PerQty": dat.PerQty, "AvailableInStock": dat.AvailableInStock, "Styleid": dat.Styleid
                        }
                        SWO_BOMArrya.push(BOMJson);

                    });
                    AddItemRow();
                }
                else {
                    swal("warning", "Bill Of Material Not Available, Contact Your BOM Section", "error");
                    window.onkeydown = null;
                    window.onfocus = null;
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}


function clearAll() {
    $("#BrandName").val("");
    $("#SWO_itemcatNameid").val("");
    $("#SWO_itemcatNameid").attr("data-id","");
    $("#StoreItemid").val("");
    $("#StoreItemid").attr("data-id","");
    $("#ItemCode").val("");
    $("#StoreItemid").attr("data-aid","");
    $("#ItemUnit").val("");
    $("#ItemUnit").attr("data-id","");
    $("#ItemQty").val("");
}



function ItemRowData() {
    var len = SWO_ItemArray.length;
    if (len > 0) {
        $("#StoreItemTab > tbody").empty();
        $.each(SWO_ItemArray, function (i, dat) {
            var str = '<tr data="' + i + '"><td><div style="width:195px;">' + dat.BrandName + '</div></td><td><div style="width:195px;">' + dat.itemcatName + '</div></td><td><div style="width:195px;">' + dat.ItemName + '</div></td>';
            str = str + '<td><div style="width:195px;">' + dat.ItemCode + '</div></td><td><div style="width:195px;">' + dat.UnitName + '</div></td><td><div style="width:195px;">' + dat.Quantity + '</div></td>';
            str = str + '<td><button type="button" id="BOMData" class="btn btn-info btn-sm inner" style="padding:2px 7px 5px 7px;border-radius: 4px;" onclick="ShowBOM(' + i + ')">BOM</button> <i class="fa fa-trash deleteitemArr"></i></td></tr>';
            $("#StoreItemTab > tbody").append(str);
        });
    }
}


$(document).on("click", ".deleteitemArr", function () {
    debugger;
    var id = $(this).closest("tr").attr('data');
    var ItemId = SWO_ItemArray[id].ItemId;
    var indizes = SWO_BOMArrya.reduce((r, v, i) => r.concat(v.SWO_ItemId === ItemId ? i : []), []);
    for (var k = 0; k <indizes.length; k++) {
        SWO_BOMArrya.splice(indizes[k], 1);
    }
    SWO_ItemArray.splice(id, 1);
    // alert(id);
    $(this).closest("tr").remove(); // closest used to remove the respective 'tr' in which I have my controls
    //  $('#hiderawitemtable tr').find('#' + id).remove();
    $('table#StoreItemTab > tbody > tr#' + id).remove();

});

function ShowBOM(id) {
    $("#ItemBOMmodel").modal('show');
    $("#BOMTab > tbody").empty();
    var ItemId = SWO_ItemArray[id].ItemId;
    var indizes = SWO_BOMArrya.reduce((r, v, i) => r.concat(v.SWO_ItemId === ItemId ? i : []), []);
    for (var k = 0; k < indizes.length; k++) {
        var str = '<tr data="' + indizes[k] + '"><td>' + SWO_BOMArrya[indizes[k]].Itemsubcategory + '</td><td>' + SWO_BOMArrya[indizes[k]].ItemName + '</td><td>' + SWO_BOMArrya[indizes[k]].SupplierName + '</td>';
        str = str + '<td>' + SWO_BOMArrya[indizes[k]].PerQty + '</td><td>' + SWO_BOMArrya[indizes[k]].RequiredQty + '</td><td>' + SWO_BOMArrya[indizes[k]].AvailableInStock + '</td>';
        str = str + '<td>' + SWO_BOMArrya[indizes[k]].ItemUnit+'</td></tr>';
        $("#BOMTab > tbody").append(str);   
    }

}

function hidepop() {
    $('.modal').modal('hide');
}


$("#saveswo").on("click", function () {
    debugger;
    ErrorMessage = "";
    var isvalid = true;

    if (SWO_ItemArray.length == 0) {
        ErrorMessage += "Please Enter Item Details. \n";
        isvalid = false;
    }
    var SWO_Date = $("#SWO_Date").val();
    if (SWO_Date == "") {
        ErrorMessage += "Please Select Store Work Order Date. \n";
        isvalid = false;
    }
    var StoreName = $("#StoreNameId").val();
    var StoreNameId = $("#StoreNameId").attr("data-ID");
    if (StoreName == "" || StoreNameId == "") {
        ErrorMessage += "Please Select Store Name. \n";
        isvalid = false;
    }
    var StoreWarehouse = $("#StoreWarehouseId").val();
    var StoreWarehouseId = $("#StoreWarehouseId").attr("data-ID");
    if (StoreWarehouse == "" || StoreWarehouseId == "") {
        ErrorMessage += "Please Select Store Warehouse Name. \n";
        isvalid = false;
    }


    if (ErrorMessage.length > 0) {
        swal({
            title: 'Error',
            text: ErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        isvalid = false;
    }
    else {
        Save_Store_WO();
    }

    return isvalid;

});

function Save_Store_WO() {
    var Store_WO_Id = $("#Store_WO_Id").val();
    var StoreWO_No = $("#StoreWO_No").val();
    var SWO_Date = $("#SWO_Date").val();
    var StoreNameId = $("#StoreNameId").attr("data-ID");
    var StoreWarehouseId = $("#StoreWarehouseId").attr("data-ID");
    var customerid = $("#customerid").attr("data-ID");
    var porefid = $("#porefno").val();
    var porefno = $("#porefno option:selected").text();
    var Approvalstatus = $("#Approvalstatus").val();
    if (parseInt(Approvalstatus) == 1) {
        Approvalstatus = true;
    }
    else if (parseInt(Approvalstatus) == 0) {
        Approvalstatus = false;
    }
    var Comment = $("#Comment").val();

    var $MyJson = {
        "Store_WO_Id": Store_WO_Id, "Store_WO_No": StoreWO_No, "Store_WO_Date": SWO_Date, "Store_Name": StoreNameId, "Store_Warehouse": StoreWarehouseId, "Customer_Id": customerid,
        "PO_ref_No": porefno, "PO_Id": porefid, "Approvalstatus": Approvalstatus, "Comment": Comment
    }
    var formData = new FormData();
    formData.append("data", JSON.stringify($MyJson));
    formData.append("ItemData", JSON.stringify(SWO_ItemArray));
    formData.append("ItemBOM", JSON.stringify(SWO_BOMArrya));

    debugger;
    $.ajax({
        url: '/StoreWorkOrder/AddUpdate/',
        type: 'POST',
        dataType: 'json',
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            debugger;
            if (data != undefined) {
                swal({
                    title: 'Success',
                    text: 'Store Work Order Submit Successfully.',
                    type: 'success',
                    confirmButtonText: "OK"
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            window.location.href = '/StoreWorkOrder/Index';
                        }
                    });
            }
        }
    });
}




///Index Page Code Start
function GetData(Status, Name) {

    $("#Pending").empty();
    $("#Completed").empty();
    $("#InProgress").empty();
    $("#Rejected").empty();

    $("#" + Name).html(loadingHTML());
    $("#" + Name).load("/StoreWorkOrder/GetStatusData", { 'IsStatus': Status });

}

function loadingHTML() {
    return $("#divLoadingContent").html();
}

function GetSWOProduct(Store_WO_Id) {
    if (Store_WO_Id != "") {
        if (parseInt(Store_WO_Id) > 0) {
            $("#SWOProductmodel").modal("show");
            $.ajax({
                type: "POST",
                url: '/StoreWorkOrder/GetStoreWO_ItemData/',
                data: "{'Store_WO_Id':" + Store_WO_Id + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    debugger;
                    result = JSON.parse(result);
                    var len = 0;
                    if (result != null) {
                        $("#SWOProductTab > tbody").empty();
                        $.each(result, function (e, dat) {
                            var str = '<tr data="' + e + '"><td>' + dat.BrandName + '</td><td>' + dat.itemcatName + '</td><td>' + dat.ItemName + '</td>';
                            str = str + '<td>' + dat.ItemCode + '</td><td>' + dat.UnitName + '</td><td>' + dat.Quantity + '</td>';
                            str = str + '</tr>';
                            $("#SWOProductTab > tbody").append(str);

                        });
                    }
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    }
}
///Index Page Code End
