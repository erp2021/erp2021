﻿
//validate data
function ValidateData() {
    var msg = "Mandetory Information : \n";
    var blnFlag = true;


    if ($("#segmenttypeid").val() == "0") {
        msg = msg + "* Please select segment type. \n";
        blnFlag = false;
    }
    if ($("#styleid").val() == "") {
        msg = msg + "* Please select style from list.\n";
        blnFlag = false;
    }
    if ($('#itemid').val() == "") {
        msg = msg + "* Please select item from list.\n";
        blnFlag = false;
    }

    if ($("#Scheme_Name").val() == "") {
        msg = msg + "* Please enter scheme name. \n";
        blnFlag = false;
    }

    if ($("#Discount_Percentage").val() == "") {
        msg = msg + "* Please enter discount percentage. \n";
        blnFlag = false;
    }
    if ($("#Start_Date").val() == "") {
        msg = msg + "* Please enter/select start date. \n";
        blnFlag = false;
    }
    if ($("#End_Date").val() == "") {
        msg = msg + "* Please enter/select end date. \n";
        blnFlag = false;
    }
    if ($("#IsSchemeExist").val() == "true") {
        msg = msg + "* Scheme already exists for selected item in selected period. \n";
        blnFlag = false;
    }
    if (msg != "" && blnFlag == false) {
        swal("warning", msg, "warning");
        return false;
    }
    return true;
}

//code to save sales
$("#btnSave").click(function () {

    if (ValidateData() == true) {

        var isPageValidate = true;
        var schemData = {};
        var branch_id = $("#Branch_Id").val();
        var company_id = $("#Company_Id").val();
        var user_ID = $("#Created_By").val();
        var Scheme_ID = $("#Scheme_ID").val();
        if (Scheme_ID == undefined) {
            Scheme_ID = 0;
        }
        schemData.Scheme_ID = Scheme_ID;
        schemData.Branch_Id = branch_id;
        schemData.Company_Id = company_id;
        schemData.Created_By = user_ID;
        schemData.Segment_Type_ID = $("#segmenttypeid").val();
        schemData.Style_ID = $("#styleid").val();
        schemData.Item_ID = $("#itemid").val();
        schemData.Scheme_Name = $("#Scheme_Name").val();
        schemData.Discount_Percentage = $("#Discount_Percentage").val();
        var st_date = $("#Start_Date").val();
        st_date = dMyyyy(st_date);
        var ed_date = $("#End_Date").val();
        ed_date = dMyyyy(ed_date);
        schemData.Start_Date = st_date;
        schemData.End_Date = ed_date;
        if (isPageValidate === true) {

            var test = JSON.stringify(schemData);
            $.ajax({
                url: '/Scheme/SaveScheme/',
                data: test,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (output) {
                    if (output > 0) {

                        swal({
                            title: 'success',
                            text: 'Scheme saved successfully.',
                            showConfirmButton: true,
                            showCancelButton: false,
                            closeOnConfirm: true,
                            closeOnCancel: true,
                            confirmButtonText: 'OK',
                            confirmButtonColor: '#8CD4F5',
                            cancelButtonText: 'Cancel'
                        },
                            function (isConfirm) {

                                if (isConfirm) {

                                    var url = '/Scheme/Index/';
                                    window.location.href = url;

                                } else {
                                    swal("Cancelled", ")", "error");
                                }
                            });
                        //swal("success", "Scheme created successfully.", "success");
                    }
                },
            });
        }
    }
});

//code to view counter sale
function load_salesData() {
    var Scheme_ID = $("#Scheme_ID").val();
    var param = { 'id': Scheme_ID };
    if (Scheme_ID != "") {
        $.ajax({
            type: "POST",
            url: '/Scheme/Get_Scheme_ById/',
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (dataset) {

                var schemeDetail = dataset;
                $('#segmenttypeid').val(schemeDetail.Segment_Type_ID);
                $('#segmenttypeid').select2().trigger('change');

                $("#stylenameid").val(schemeDetail.M_StyleMaster.Stylename);
                $("#styleid").val(schemeDetail.Style_ID);
                $("#itemnameid").val(schemeDetail.M_ItemMaster.ItemName);
                $("#itemid").val(schemeDetail.Item_ID);

                var str_date = dMyyyy(schemeDetail.st_date);
                $("#Start_Date").val(str_date);

                $("#Scheme_Name").val(schemeDetail.Scheme_Name);
                $("#Discount_Percentage").val(schemeDetail.Discount_Percentage);

                if (schemeDetail.en_date != null) {
                    var end_date = dMyyyy(schemeDetail.en_date);
                    $("#End_Date").val(end_date);
                }


                //});
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}

function dMyyyy(date_) {
    var date_n = new Date(date_);
    //var date_n = Date.parse(date_);
    const currentDate = date_n.getDate();
    //const formattedCurrentDate = ("0" + currentDate).slice(-2);
    const currentMonth = date_n.getMonth();
    //const formattedCurrentMonth = ("0" + (currentMonth + 1)).slice(-2);
    const currentYear = date_n.getFullYear();
    const monthList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    var date_Default = currentDate + '-' + monthList[currentMonth] + '-' + currentYear;
    return date_Default;
}

function checkExists() {

    var itemid = $("#itemid").val();
    var st_date = $("#Start_Date").val();
    var ed_date = $("#End_Date").val();
    if (itemid != "" && st_date != "" && ed_date != "") {
        var branch_id = $("#Branch_Id").val();
        var company_id = $("#Company_Id").val();
        var scheme_id = $("#Scheme_ID").val();
        if (scheme_id == undefined) {
            scheme_id = 0;
        }
        var param = { 'compid': company_id, 'branchid': branch_id, 'itemid': itemid, 'st_date': st_date, 'ed_date': ed_date, 'scheme_id': scheme_id };
        $.ajax({
            type: "POST",
            url: '/Scheme/checkSchemewExists/',
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                if (result == true) {
                    $("#IsSchemeExist").val(result);
                    swal("warning", "Scheme already exists for selected item in selected period.", "warning")
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}