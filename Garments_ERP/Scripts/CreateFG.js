﻿
$(function () {

    $("#saveFinishGoods").on("click", function () {

        var isvalid = true;
        var ItemName = $("#fgitemname").val();
        var itemcode = $("#fgitemcode").val();
        var hsncode = $("#hsncode").val();


        if (ItemName == "") {
            swal("Select Item Name");
            isvalid = false;
        }

        else if (itemcode == "") {
            swal("Select item code");
            isvalid = false;
        }

        else {
            var str = "";
            var rawitemstr = "";
            var $dataRows = $("#BatchInfoTable tr:not('.titlerow')");
            var rowlenght = $dataRows.length;
            var stylerowcount = 0;
            //var stylerowCount = $('#maintable2 tr').length;
            $dataRows.each(function () {
                count = 0;
                stylerowcount++;
                $(this).find('.itemrow').each(function (i) {
                    var itemlist = ($(this).val());  //here get td value
                    count++;

                    if (itemlist != undefined) {

                        if (count == 6) {
                            str += itemlist;
                        }
                        else {
                            str += itemlist + "~";
                        }
                    }

                });

                if (stylerowcount == rowlenght) {

                }
                else {
                    str += "#";
                }

            });
            isvalid = true;

            $('#finishitemdetails').val(str);
        }
        return isvalid;
    });

    $("#fgitemnameid").autocomplete({
        source: function (request, response) {

            var myjson = [];
            var companyid = $("#companyid").val();
            var branchid = $("#branchid").val();
            var mydata = { 'Name': request.term, 'CompanyId': companyid, 'BranchId': branchid };
            myjson.push(mydata);
            var itemsubcatid = null;
            $.ajax({
                url: '/CustomerEnquiry/SearchList/',
                data: "{ 'MyJson': '" + JSON.stringify(myjson) + "','type':18,'id':'" + itemsubcatid + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    $(".ui-helper-hidden-accessible").attr("style", "display:none;");
                    debugger;
                    data = JSON.parse(data);

                    if (data.hasOwnProperty('Table')) {

                        response($.map(data.Table, function (item) {
                            return { label: item.ItemName, value: item.ItemName, dataid: item.Id, dataIA: item.Item_Attribute_ID, dataisc: item.Itemsubcategoryid };

                        }));

                    }

                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        },
        select: function (e, i) {
            $("#InItem").val(i.item.dataid);
            $("#hiddenitemcategoryid").val(i.item.dataisc);
            $("#rawIAid").val(i.item.dataIA);
            GetBatchList();
        },
        minLength: 1
    });


    $(document).on("click", ".addprocess", function () {

        ErrorMessage = "";

        var isvalid = true;

        //var perqty=$('#perqty').val();
        var setqty = $('#setqty').val();

        //var qtyrequired = perqty * setqty;

        var ItemID = $('#avlitemid').val();
        var ItemNAme = $('#avlitem').val();
        var gstrate = $('#gstrate').val();
        var itemcode = $('#itemcode').val();
        var itemhsn = $('#itemhsn').val();
        var openingqty = $('#Outstanding').val();


        var ReqQty = $('#FGqty').val();
        var i = $('#processtable tr').length;
        // var selecteditemcategoryin = itemcatin.options[itemcatinid.selectedIndex].text;
        var strInCategories = "";
        if (parseInt(ReqQty) > parseInt(openingqty)) {
            swal("required qty is more then available qty");
            $('#FGqty').val("");
            isvalid = false;
        }
        if (ItemID == "" && ItemID == undefined && ItemID == 0) {
            swal("select item");
            isvalid = false;
        }

        if (openingqty == 0) {
            swal("opening qty not available");
            isvalid = false;
        }

        isvalid = calLineTotal();
        if (isvalid == true) {


            if (ErrorMessage.length > 0) {
                swal({
                    title: 'Oops',
                    text: ErrorMessage
                });
                isvalid = false;
            }

            else if (openingqty > 0 && openingqty != "" && ReqQty != "") {
                div = '<tr class="titlerow bg-lightgrey" id="' + (i + 1) + '" value="' + (i + 1) + '" data="' + (i + 1) + '">' +
                    '<td style="display:none;"><input type="hidden" id="itemid' + i + '" value="' + ItemID + '" class="form-control processrow validprocessrow"><p class="">' + ItemID + '</p></td>' +
                    '<td style="padding-right:10px;"><input  type="hidden" value="' + ItemNAme + '" class="form-control processrow validprocessrow"><p class="">' + ItemNAme + '</p></td>' +
                    '<td style="padding-right:10px;"><input type="hidden" value="' + itemcode + '" class="form-control processrow validsequence"><p class="">' + itemcode + '</p></td>' +
                    '<td style="padding-right:10px;"><input type="hidden"  value="' + openingqty + '" class="form-control processrow validsequence"><p class="">' + openingqty + '</p></td>' +
                    '<td style="padding-right:10px;"><input type="hidden" value="' + gstrate + '" class="form-control processrow validsequence"><p class="">' + gstrate + '</p></td>' +
                    '<td style="padding-right:10px;"><input type="hidden" value="' + itemhsn + '" class="form-control processrow validsequence"><p class="">' + itemhsn + '</p></td>' +
                    '<td style="padding-right:10px;"><input type="hidden" id="ReqQty' + i + '" value="' + ReqQty + '" class="form-control processrow" name="hdnReqty"><p class="">' + ReqQty + '</p></td>' +
                    //'<td style="padding-right:10px;"><input type="hidden" value="' + itemcatoutid + '" class="form-control processrow"><p class="">' + selecteditemcategoryout + '</p></td>' +
                    //'<td style="padding-right:10px;" id="imgraw' + (i + 1) + '"><input type="hidden" value="' + comment + '" class="form-control processrow">' + comment + ' </td>' +

                    '<td><button type="button" id="btnDelete" class="deleteprocessitem btn btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></button></td>' +
                    '</tr>';
                $('#processtable').append(div);

                $('#avlitem').val("");
                $('#avlitemid').val("");
                $('#Outstanding').val("");
                $('#FGqty').val("");
                $('#gstrate').val("");
                $('#itemcode').val("");
                $('#itemhsn').val("");
                CheckTableRow();
            }
            else {
                swal("set quantity should be");
                $('#FGqty').val("");
            }
        }
        return isvalid;
    });

    $(document).on("click", ".deleteprocessitem", function () {
        // var id = $(this).closest("tr").attr('data');
        // alert(id);
        $(this).closest("tr").remove(); // closest used to remove the respective 'tr' in which I have my controls
        // $('#hidestyletable tr').find('#' + id).remove();
        //  $('table#hidestyletable tr#' + id).remove();
        CheckTableRow();
    });



    $(document).on("click", "#saveprocesscycle", function () {
        var isvalid = true;
        var processname = $('#Packingtypeid').val();
        var SetQty = $('#perqty').val();
        var $dataRows = $("#processtable tr");
        //var $dataRows = $("#processtable tr:not('.titlerow')");
        var rowlenght = $dataRows.length;

        if (rowlenght <= 0) {
            swal("please add Items");
            isvalid = false;
        }
        else if (processname == "") {
            swal("please add Package Type");
            isvalid = false;
        }
        else {
            var str = "";
            var stylerowcount = 0;
            var TableData = new Array();
            var count = 0;

            $dataRows.each(function () {
                count = 0;
                stylerowcount++;
                $(this).find('.processrow').each(function (i) {
                    var itemlist = ($(this).val());  //here get td value
                    count++;
                    if (itemlist != undefined) {

                        if (count == 7) {
                            str += itemlist;
                        }
                        else {
                            str += itemlist + "~";
                        }
                    }

                    //str += itemlist;
                    //str += itemlist + "~";         



                });
                if (stylerowcount == rowlenght) {

                }
                else {
                    str += "#";
                }


            });

            $('#processlist').val(str);
        }
        return isvalid;
    });


});
function CheckTableRow() {

    var rowCount = $('#processtable tr').length;

    if (rowCount > 1) {
        //$("#perqty").prop('disabled', true);
        $("#setqty").prop('disabled', true);
    }
    else {
        //$("#perqty").prop('disabled', false);
        $("#setqty").prop('disabled', false);
    }
}



function calLineTotal(no) {

    var isvalid = true;

    var IssueQty = $("#IssueQty" + no).val();
    var grandtoatl = 0;
    var $tableRows = $("#BatchInfoTable tbody tr");

    for (var n = 1; n <= $tableRows.length; n++) {
        var itemid_ = $("#itemid" + n).val();
        var ReqQty_ = $("#ReqQty" + n).val();
        if (parseInt(itemid) == parseInt(itemid_)) {
            if (ReqQty_ == undefined) {
                ReqQty_ = 0;
            }

            grandtoatl = grandtoatl + parseInt(ReqQty_);

        }


    }
    grandtoatl = grandtoatl + parseInt(perqty);
    if (parseInt(grandtoatl) > parseInt(avlQty)) {
        swal("qty not available");
        isvalid = false;

    }

    return isvalid;
}
function GetBatchList() {
    debugger;
    var ItemId = $("#InItem").val();
    var itemAttr = $("#rawIAid").val();
    var companyid = 0;     //$("#companyid").val();
    var branchid = 0;//$("#branchid").val();
    var Josndata = { 'ItemId': ItemId, 'ItemAttr': itemAttr, 'companyid': companyid, 'branchid': branchid };
    var MyJson = JSON.stringify(Josndata);

    $.ajax({
        type: "POST",
        url: '/BillOfMaterial/SaveAutoPR/',
        data: "{'Model':'" + MyJson + "','type':8}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            debugger;
            result = JSON.parse(result);
            $("#Batchnoid").empty();
            $.each(result.Table, function (key, BT) {
                $("#Batchnoid").append('<option value="' + BT.ItemInward_ID + '">' + BT.Dynamic_Controls_value + '</option>');
            });
            $("#Batchnoid").multiselect('reload');
            $("#Batchnoid").multiselect({
                columns: 1,

                placeholder: 'Select Batch No',
                search: true,
                searchOptions: {
                    'default': 'Search Batch No'
                },
                selectAll: true
            });

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function GetInwardQty() {
    debugger;
    var no = $("#Rowid").val();
    var ItemId = $("#InItem").val();
    var itemAttr = $("#rawIAid").val();
    var inwardid = $('#Batchnoid option:selected').toArray().map(item => item.value).join();

    var Josndata = { 'ItemId': ItemId, 'ItemAttr': itemAttr, 'inwardid': inwardid };
    var MyJson = JSON.stringify(Josndata);
    $.ajax({
        type: "POST",
        url: '/BillOfMaterial/SaveAutoPR/',
        data: "{'Model':'" + MyJson + "','type':9}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            debugger;
            result = JSON.parse(result);
            $.each(result.Table, function (key, BT) {
                $("#batchTotQty").val(BT.TotalQty);
            });
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function SetBatchNo() {
    var ErrorMessage = "";
    var isvalid = true;
    var inwardid = $('#Batchnoid option:selected').toArray().map(item => item.value).join();
    var BatchQty = $("#batchTotQty").val();
    if (inwardid == "") {
        ErrorMessage = "Please Select Batch No!";
        isvalid = false;
    }
    if (BatchQty == "0") {
        ErrorMessage = "Your Stock is Less!";
        isvalid = false;
    }
    var inwardvalue = $('#Batchnoid option:selected').toArray().map(item => item.text).join();
    var $dataRows = $("#BatchInfoTable tr:not('.titlerow')");
    $dataRows.each(function () {
        $(this).find('.Batchnor').each(function (i) {
            var batchno = ($(this).val());
            $.each(inwardvalue.split(','), function (i, dt) {
                if (batchno == dt) {
                    ErrorMessage = batchno + " This Batch No. All Ready in Row!";
                    isvalid = false;
                }
            });
        });
    });


    if (ErrorMessage != "") {
        swal({
            title: 'Error',
            text: ErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        isvalid = false;
    } else {

        var tabstr = "";
        $("#BatchInfoTable > thead").html("");
        tabstr = tabstr + "<tr  class='titlerow'>";
        tabstr = tabstr + "<th style='width:16.5em;'>Item Name</th><th style='width:16.5em;'>Batch No</th><th style='width:16.5em;'>Batch Qty</th><th style='width:16.5em;'>Issue Qty</th>";
        tabstr = tabstr + "<th style='width:16.5em;'>Balance Qty</th><th style='width:6em;'>Action</th>";
        tabstr = tabstr + "</tr>";
        $("#BatchInfoTable > thead").append(tabstr);

        var no = $("#Rowid").val();
        var ItemId = $("#InItem").val();
        var Itemname = $("#fgitemnameid").val();
        var itemAttr = $("#rawIAid").val();
        var isubcat = $("#hiddenitemcategoryid").val();
        //var itemQty = $("#ItemTotQty").val();


        var Josndata = { 'ItemId': ItemId, 'ItemAttr': itemAttr, 'inwardid': inwardid };
        var MyJson = JSON.stringify(Josndata);
        $.ajax({
            type: "POST",
            url: '/BillOfMaterial/SaveAutoPR/',
            data: "{'Model':'" + MyJson + "','type':10}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                debugger;
                result = JSON.parse(result);
                $("#BatchInfoTable").attr('style', 'width: 100%;display:block;');
                var $dataRows = $("#BatchInfoTable tr:not('.titlerow')");
                var len = $dataRows.length;
                $.each(result.Table, function (key, BT) {
                    len = len + 1;
                    tabstr = "";
                    tabstr = tabstr + "<tr>";
                    tabstr = tabstr + "<td><input type='text' class='form-control itemrow itemd' value='" + Itemname + "' readonly style='width:16.5em;' /></td>";
                    tabstr = tabstr + "<td style='display:none;'><input type='hidden' class='form-control itemrow itemd' id='itmid" + len + "' value='" + ItemId + "' readonly /></td>";
                    tabstr = tabstr + "<td style='display:none;'><input type='hidden' class='form-control itemrow itemd' id='itmAttrid" + len + "' value='" + itemAttr + "' readonly /></td>";
                    tabstr = tabstr + "<td style='display:none;'><input type='hidden' class='form-control itemrow itemd' id='itmsubcatid" + len + "' value='" + isubcat + "' readonly /></td>";
                    tabstr = tabstr + "<td style='display:none;'><input type='hidden' class='form-control itemrow itemd' id='itmUOMid" + len + "' value='" + BT.UOM + "' readonly /></td>";
                    tabstr = tabstr + "<td style='display:none;'><input type='hidden' class='form-control itemrow itemd' id='itmUOMval" + len + "' value='" + BT.Unitdesc + "' readonly /></td>";
                    //tabstr = tabstr + "<td><input type='text' class='form-control itemrow totQty itemd' value='" + itemQty + "' id='ItemQty" + len + "' readonly /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control itemrow itemd' value='" + BT.Dynamic_Controls_value + "' readonly /></td>";
                    tabstr = tabstr + "<td style='display:none;'><input type='hidden' class='form-control itemrow Batchnor itemd' value='" + BT.ItemInward_ID + "' readonly /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control itemrow ItemQty itemd'  id='BTItemQty" + len + "' value='" + BT.ItemQty + "' readonly /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control itemrow IssueQty itemd' id='IssueQty" + len + "' name='IssueQty" + len + "' value='0' onkeypress='return isNumber(event)' onchange=GetTotalCal('" + len + "')  /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control itemrow itemd' value='0' id='BalQty" + len + "' Name='BalQty" + len + "' readonly /></td>";
                    tabstr = tabstr + "<td style='width:5em;'><i class='fa fa-trash pluxtrash' onclick='removetr()'></i></td>";
                    tabstr = tabstr + "</tr>";
                    $("#BatchInfoTable > tbody").append(tabstr);
                    //SetTotalQty();
                });

                $("#InItem").val('');
                $("#fgitemnameid").val('');
                $("#rawIAid").val('');
                $("#hiddenitemcategoryid").val('');
                //$("#ItemTotQty").val('');
                $("#batchTotQty").val('');
                GetBatchList();

            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}

function GetTotalCal(id) {
    debugger;
    var no = id;
    var itemQty = $("#BTItemQty" + no).val();
    var IssueQty = $("#IssueQty" + no).val();

    if (parseFloat(IssueQty) > parseFloat(itemQty)) {
        msgshow("You Have Required Only " + itemQty + " Quantity!");
        $("#IssueQty" + no).val("");
    }
    else {
        var bal = 0;
        var batchQty = $("#BTItemQty" + no).val();

        if (parseFloat(IssueQty) > parseFloat(batchQty)) {
            msgshow("You Have Required Only " + itemQty + " Quantity!");
            $("#IssueQty" + no).val("");
        }
        else {
            bal = parseFloat(batchQty) - parseFloat(IssueQty);
            $("#BalQty" + no).val(bal);
        }
    }

}
function msgshow(alrtmsg) {
    swal({
        title: 'Error',
        text: alrtmsg,
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false,
        closeOnCancel: false

    });
}

function removetr() {

    $(this).closest('tr').remove();

}



