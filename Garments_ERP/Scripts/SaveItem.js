﻿
$('#addDocument').on('click', function () {
    var rawimgfiles = $("#ItemImg").val();
    if (rawimgfiles != "") {
        var i = $('#itemdoctable tr').length;
        var rowCount = 0;
        var clone = $("#ItemImg").clone();
        clone.attr('id', 'ItemImg' + (i + 1));
        clone.attr('name', 'rawitemimg');
        clone.css('display', 'none');
        var imgclone = $('.imageThumb', $("#imgpreviewraw")).clone();

        rawdiv = '<tr class="data-contact-person odd bg-info" id="' + (i + 1) + '" value="' + (i + 1) + '" data="' + (i + 1) + '">' +
               '<td style="padding-right:10px;" id="imgraw' + (i + 1) + '"></td>' +
              '<td><button type="button" id="btnDelete" class="deleterawitem btn btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></button></td>' +
              '</tr>';
        $('#itemdoctable').append(rawdiv);
        $('#imgraw' + (i + 1)).html(clone);
        $('#imgraw' + (i + 1)).append(imgclone);
        $('#imgpreviewraw').empty();
        $("#ItemImg").replaceWith($("#ItemImg").val('').clone(true));
    }
});


function getFileExtension(filename) {
    return filename.split('.').pop();
}

function adddocument() {
    debugger;
    var j = $('#documettable tr').length;
    var clone = $("#Leddoc").clone();
    clone.attr('id', 'Leddoc' + (j + 1));
    clone.attr('name', 'LedDocu');
    clone.css('display', 'none');

    $("#Leddoc").val("");
    var imgname = $("#documentList").val();

    var dcumentType = getFileExtension(imgname);
    if (imgname != "" && imgname != null) {
        if (CheckImage(imgname)) {
            var n = imgname.indexOf("~^");
            var imagelist = "<tr><td class='addressrow' rawdata='" + dcumentType + "' id='Leddoc" + (j + 1) + "'>" + dcumentType + "</td><td class='addressrow' rawdata='" + imgname + "' id='Leddoc" + (j + 1) + "'>" + imgname + "</td><td class='addressrow' rawdata='1'  style='display:none'></td><td><i class='fa fa-trash delete'></i></td></tr>";
            $("#documettable").append(imagelist);
            $('#Leddoc' + (j + 1)).append(clone);
        }
        else {

            swal({
                title: 'Error',
                text: 'This Document Allready Add in Row! ',
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                closeOnConfirm: false,
                closeOnCancel: false

            });
        }
    }
    else {
        swal({
            title: 'Error',
            text: 'Please Select Document',
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
    }
}


function CheckImage(imgname) {
    var $dataRows3 = $("#documettable tr:not('.titlerow')");
    var imageName = "";
    $dataRows3.each(function (i) {
        $(this).find('td').each(function (j) {
            if (j == 1) {
                imageName = $(this).attr('rawdata');
            }
        });
    });

    if (imgname == imageName) {
        return false;
    }
    else {
        return true;
    }
}


$(document).on("click", ".deleterawitem", function () {
    var id = $(this).closest("tr").attr('data');
    $(this).closest("tr").remove(); // closest used to remove the respective 'tr' in which I have my controls
});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function GetUPpp(id, name) {
    debugger;
    $("#imgpreview").html('');

    var no = id.match(/\d+/);
    $("#noid").val(no);
    $("#nameid").val(name);
    var pathsdat = window.location.pathname;
    pathsdat = pathsdat.split('/');
    if (pathsdat[2] == "Edit") {
        $('#UploadPreviewModal').modal('toggle');
        if (name == 'preview') {
            $("#imgpreview1").html("");

            var strd1 = '<div id="background"></div>';
            $("#imgpreview1").append(strd1);
            var file1 = document.querySelector('#ItemAttrimages' + no[0]);
            var file2 = document.querySelector('#ItemAttrimages');
            file2.files = file1.files;

            var filesLength = file2.files.length;
            var wid = parseFloat(filesLength) * 80;
            var Rid = $("#imgpreview1").find("input:radio").attr("id");
            var m = 1;
            if (Rid == undefined) {
                m = 0;
            }

            var path = $("#Attrimages" + no).val();
            if (path != "" && path != undefined && filesLength==0) {

                $.each(path.split(','), function (k, d) {
                    var imgstr = "<input type='radio' name='input_thumb' id='id" + k + "' style='z-index:-1;' /><label for='id" + k + "' style='margin-right:9px;height:55px;width:55px;position: relative;overflow: hidden;line-height: 0;padding: 0;'> <img src='" + d + "' style='height:100%;width:100%' /> </label><img src='" + d + "' class='DefaultBigStyle1'>";
                    $("#imgpreview1").append(imgstr);
                    $('#id0').trigger('click');
                });
            }
            for (var i = 0; i < filesLength; i++) {
                var f = file2.files[i];
                var fileReader = new FileReader();
                var k = 0;
                fileReader.onload = (function (e) {
                    debugger;
                    k = m + k;
                    var file = e.target;
                    var imgstr = "<input type='radio' name='input_thumb' id='id" + k + "' style='z-index:-1;' /><label for='id" + k + "' style='margin-right:9px;height:55px;width:55px;position: relative;overflow: hidden;line-height: 0;padding: 0;'> <img src='" + e.target.result + "' style='height:100%;width:100%' /> </label><img src='" + e.target.result + "' class='DefaultBigStyle1'>";
                    $("#imgpreview1").append(imgstr);
                    $('#id0').trigger('click');
                    k++;
                });

                fileReader.readAsDataURL(f);
            }
           
        }
        else {
            $("#uploadid").show();
        }
    }
    else {
        $('#UploadPreviewModal').modal('toggle');
        if (name == 'preview') {
            debugger;
            $("#imgpreview1").html("");
             file1 = document.querySelector('#ItemAttrimages' + no[0]);
             file2 = document.querySelector('#ItemAttrimages');
            file2.files = file1.files;
            
              filesLength = file2.files.length;
             wid = parseFloat(filesLength) * 80;

            var strd = '<div id="background"></div>';
            $("#imgpreview1").append(strd);

            for (var i = 0; i < filesLength; i++) {
                var f = file2.files[i];
                 fileReader = new FileReader();
                var k = 0;
                fileReader.onload = (function (e) {
                    debugger;
                    var file = e.target;
                    var imgstr = "<input type='radio' name='input_thumb' id='id" + k + "' style='z-index:-1;' /><label for='id" + k + "' style='margin-right:9px;height:55px;width:55px;position: relative;overflow: hidden;line-height: 0;padding: 0;'> <img src='" + e.target.result + "' style='height:100%;width:100%' /> </label><img src='" + e.target.result + "' class='DefaultBigStyle1'>";
                    $("#imgpreview1").append(imgstr);
                    $('#id0').trigger('click');
                    k++;
                });

                fileReader.readAsDataURL(f);
            }

        }
        else {
            $("#uploadid").show();
        }
    }

}




$("#ItemAttrimages").on("change", function (e) {
    debugger;
    $("#imgpreview1").html('');
    var strd = '<div id="background"></div>';
    $("#imgpreview1").append(strd);
    var files = e.target.files,
        filesLength = files.length;
    var wid = parseFloat(filesLength) * 80;

    for (var i = 0; i < filesLength; i++) {
        var f = files[i];
        var fileReader = new FileReader();
        var k = 0;
        fileReader.onload = (function (e) {
            var file = e.target;
            var imgstr = "<input type='radio' name='input_thumb' id='id" + k + "' style='z-index:-1;' /><label for='id" + k + "' style='margin-right:9px;height:55px;width:55px;position: relative;overflow: hidden;line-height: 0;padding: 0;'> <img src='" + e.target.result + "' style='height:100%;width:100%' /></label><img src='" + e.target.result + "' class='DefaultBigStyle1'>";
            $("#imgpreview1").append(imgstr);
            $('#id0').trigger('click'); 
            k++;
        });
        
        fileReader.readAsDataURL(f);
    }
   

});

function preview(id) {
    var imgsrc = $(id).attr('src');
    $("#previewid").attr('src', imgsrc);
}

$(document).ready(function ($) {
    $('.pagi-container>img').toggleClass('DefaultBigStyle1');
    $('.pagi-container label:nth-child(0)>img').trigger('click'); // This is to set initial Photo in the Frame.
});


function pophide() {
    $('.modal').modal('hide');
}

function Savehide_A() {
    debugger;
    var pathsdat = window.location.pathname;
    pathsdat = pathsdat.split('/');
    var file1 = document.querySelector('#ItemAttrimages');
    var emptyFile = document.createElement('input');
    emptyFile.type = 'file';

    var no = $("#noid").val();
    var name = $("#nameid").val();
    if (parseInt(no) > 0) {
        var file2 = document.querySelector('#ItemAttrimages' + no);
        var filesLength = file1.files.length;
        if (parseInt(filesLength) > 0) {
            file2.files = file1.files;
            $("#imgpreview1").html("");
            $("#viewimage" + no).parent().attr('dataval', filesLength);
            $("#viewimage" + no).attr('style', 'display:block;');
            $("#uploadimage" + no).attr('style', 'display:none;');
        }
        $('.modal').modal('hide');
        file1.files = emptyFile.files;

    }
    else {
        $('.modal').modal('hide');
    }
    
}



function OnOffBI() {
    var qty = $("#itemstock1").val();
    var name = $("#ItemName").val();
    if (parseFloat(qty) > 0 && name!="") {
        $("#btnSetProductInfo").removeAttr("disabled");
    }
    else {
        $("#btnSetProductInfo").attr("disabled", "disabled");
        
    }
}


function GetPIpp(id) {
    debugger;
    $("#Dynamic_ID option:selected").prop("selected", false);

    $("#ProductInfoTable > tbody").html('');
    $("#ProductInfoTable > thead").html('');
    $("#TotalCount").html('');
    $("#ProductInfoTable1").html('');

    var no = id.match(/\d+/);
    no = no == null ? "" : no;
    var pathsdat = window.location.pathname;
    pathsdat = pathsdat.split('/');
    if (pathsdat[2] == "Edit") {
        var PItithead = $("#producttype" + no).val();
        if (no != "")
        {
            if (PItithead == "" || PItithead == null) {

                swal({
                    title: 'Error',
                    text: "No Product Info Fount!",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false,
                    closeOnCancel: false

                });
                $('.modal').modal('hide');

            }
            else {
                $('#ProdutInfoModal').modal('toggle');
            }
        }
        else {
            $('#ProdutInfoModal').modal('toggle');
        }
    }
    else {
        $('#ProdutInfoModal').modal('toggle');
    }
    
    var MyPIstr = $("#productinfo" + no).val();
    var name = $("#ItemName").val();
    $("#proditemname").val(name);
    $("#proditemname").attr('dataval', no);
    var acptqty = $("#itemstock1").val();
    $("#proAcptQty").val(acptqty);

    if (MyPIstr != "") {
        var protype = $("#producttype" + no).val();
        if (protype != "") {
            $.each(protype.split(","), function (i, e) {
                $("#Dynamic_ID option[value='" + e + "']").prop("selected", true);
            });
            $("#Dynamic_ID").select2().trigger('change');
            var pid = document.getElementById('Dynamic_ID');
            GetProductvalue(pid);
        }
        $(".attrbtab1").removeAttr('style');
        $(".attrbtab2").attr('style', 'display:none;');

        $.each(MyPIstr.split('<'), function (i, data) {

            var lenchk = data.split('>');
            lenchk = lenchk.length;
            var tabstr = "";
            tabstr = tabstr + "<tr class='attrtabval'>";
            $.each(data.split('>'), function (i, data) {
                if (data != "") {
                    if (i == (lenchk - 1)) {
                        tabstr = tabstr + "<td style='width:8em;' dataval='" + data + "'>" + data + "<input type='hidden' class='itemqtycl' value='" + data + "' /> </td>";

                        var path = window.location.pathname;
                        path = path.split('/');
                        if (path[2] == "Edit") {
                            tabstr = tabstr + "<td style='width:6.5em;'><i class='fa fa-trash pluxtrashdis' disabled></i></td>";
                        }
                        else {
                            tabstr = tabstr + "<td style='width:6.5em;'><i class='fa fa-trash pluxtrash' onclick='removetr()'></i></td>";
                        }
                    }
                    else {
                        tabstr = tabstr + "<td dataval='" + data + "' style='width:14em;'><input type='hidden' value='" + data + "' class='hid2'  /> <input type='hidden' class='hidval2' value='" + data + "'  />" + data + "</td>";
                    }
                }

            });
            tabstr = tabstr + "</tr>";
            $("#ProductInfoTable > tbody").append(tabstr);


        });
        GetTotalQty();
        $("#ProductInfoTable").attr('style', 'width:100%;display:block;');
        var path = window.location.pathname;
        path = path.split('/');
        if (path[2] == "Edit") {
            $("#ProductInfoTable1").attr('style', 'display:none;');
            $(".bodyscol").attr('style', 'max-height:300px');
        }
    }
    else {
        $("#ProductInfoTable").attr('style', 'width:100%;display:none;');
    }
}


function GetProductvalue(aid) {
    debugger;
    var AttribId = $(aid).val();
    if (AttribId != "" && AttribId != null) {
        var Data = $('#Dynamic_ID option:selected').toArray().map(item => item.text).join();
        var index = Data.indexOf(',');
        if (index == -1) {
            Data = Data + ",";
        }
        Data = Data.split(',');
        var tabstr = "";
        $("#ProductInfoTable > thead").html("");
        tabstr = tabstr + "<tr  class='attrbtab1' style='display:none;'>";
        for (i = 0; i < Data.length; i++) {
            if (Data[i] != "") {
                tabstr = tabstr + "<th style='width:14em;'>" + Data[i] + "</th>";
            }
        }
        tabstr = tabstr + "<th style='width:8em;'>Quantity</th><th style='width:6.5em;'>Action</th>";
        tabstr = tabstr + "</tr>";
        $("#ProductInfoTable > thead").append(tabstr);


        var tabstr1 = "";
        $("#ProductInfoTable1").html("");
        tabstr1 = tabstr1 + "<tr  class='attrbtab2'>";
        for (i = 0; i < Data.length; i++) {
            if (Data[i] != "") {
                tabstr1 = tabstr1 + "<th style='width:14em;'>" + Data[i] + "</th>";
            }
        }
        tabstr1 = tabstr1 + "<th  style='width:8em;'>Quantity</th><th style='width:6.5em;'>Action</th>";
        tabstr1 = tabstr1 + "</tr>";
        tabstr1 = tabstr1 + "<tr  class='attrbtab_val2'>";
        var index2 = AttribId.indexOf(',');
        if (index2 == -1) {
            AttribId = AttribId + ",";
        }
        AttribId = AttribId.split(',');

        for (i = 0; i < AttribId.length; i++) {
            var option = "";
            if (AttribId[i] != "") {
                if (Data[i] == "Expiry") {
                    tabstr1 = tabstr1 + "<td style='width:14em;'><div class='input-group date hasDatepicker' id='bs_datepicker_component_container4' style='border:1px solid #ccc;border-radius:5px;'><input type='text' class='form-control  pull-right MyExpire hidval'  placeholder='Ex:30/07/2016' value='' id='" + Data[i].split(" ").join("") + "1' name='" + Data[i].split(" ").join("") + "1' onclick='changedateStyle()' /><span class='input-group-addon'><i class='material-icons'>date_range</i></span></div></td>";
                }
                else if (Data[i] == "Lot No" || Data[i] == "Batch") {
                    var PIname = $("#proditemname").val();
                    var iname = "";
                    $.each(PIname.split(' '), function (i, data) {
                        if (data != "" && data != "-") {
                            if (data[0] != "-") {
                                iname = iname + data[0];
                            }
                        }
                    });
                    var radno = Math.random().toFixed(3).split(".").pop();
                    radno = parseInt(radno);
                    radno = radno.length == 2 ? radno + "1" : radno;
                    var no = Data[i].split(" ").join("")[0] + "-ML-PR-" + iname + "-" + radno + "-" + Getreturn();
                    tabstr1 = tabstr1 + "<td style='width:14em;'><input type='text' class='form-control hidval' readonly value='" + no + "' id='" + Data[i].split(" ").join("") + "1' name='" + Data[i].split(" ").join("") + "1' /></td>";
                }
                else {

                    tabstr1 = tabstr1 + "<td style='width:14em;'><input type='text' class='form-control hidval' id='" + Data[i].split(" ").join("") + "1' name='" + Data[i].split(" ").join("") + "1' /></td>";
                }


            }
        }
        tabstr1 = tabstr1 + "<td  style='width:8em;'><input type='text' id='itemQty1'  class='form-control' onkeypress='return isNumber(event)' /></td><td style='width:6.5em;'><i class='fa fa-plus pluxad' onclick='SetProductValue()'></i></td>";
        tabstr1 = tabstr1 + "</tr>";
        $("#ProductInfoTable1").append(tabstr1);

        var cnt = 0;
        $(".MyExpire").datepicker({
            dateFormat: 'mm/dd/yy',
            minDate: 0
        });
    }
    else {
        debugger;
        $("#ProductInfoTable1").html("");
        $("#ProductInfoTable > thead").html("");
        $("#ProductInfoTable > tboday").html("");
        $("#Dynamic_ID  option:selected").val("");
    }
}

function changedateStyle() {

    $("#ui-datepicker-div").css("z-index", "10000");
}

function Getreturn() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = + mm + dd + yyyy;
    return today;
}

function SetProductValue() {
    debugger;
    var comm = {};
    var savedata = {};
    itemQty = $("#itemQty1").val();
    var isvalid = true;
    var ErrorMessage = "";
    //validations of item master

    $("#ProductInfoTable1 tr.attrbtab_val2").children('td').find(".hidval").each(function () {
        var chkatrval = this.value;
        if (chkatrval == "" || chkatrval == "0") {
            ErrorMessage = "Please Enter Details!";
            isvalid = false;
        }
    });


    if (itemQty == "") {
        ErrorMessage = "Please Enter Item Quantity";
        isvalid = false;
    }
    if (ErrorMessage != "") {
        swal({
            title: 'Error',
            text: ErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        isvalid = false;
    }
    else {

        var Data = $('#Dynamic_ID option:selected').toArray().map(item => item.text).join();
        var index = Data.indexOf(',');
        if (index == -1) {
            Data = Data + ",";
        }
        Data = Data.split(',');
        debugger
        var count = 0;
        var temprray = "";
        var atrval = "";
        var item1 = {};
        $("#ProductInfoTable1 tr.attrbtab_val2").children('td').find(".hidval").each(function () {
            atrval = atrval + "," + this.value;
        });

        $("#ProductInfoTable").find(".attrtabval").each(function () {
            temprray = "";
            $(this).children('td').find(".hidval2").each(function () {
                temprray = temprray + "," + this.value;
            });
            if (atrval == temprray) {
                count++;
            }
        });



        if (count == 0) {
            debugger;
            $("#ProductInfoTable").attr('style', 'width: 100%;display:block;');
            var atrid;
            $("#ProductInfoTable1 tr.attrbtab_val2").children('td').find(".hidval").each(function () {
                atrid = atrid + "," + this.value;
            });
            atrid = atrid.split(',');
            atrid.splice(0, 1);
            atrval = atrval.split(',');
            atrval.splice(0, 1);


            $(".attrbtab1").removeAttr('style');
            $(".attrbtab2").attr('style', 'display:none;');
            var tabstr = "";
            var len1 = $("#ProductInfoTable tr").length;
            tabstr = tabstr + "<tr class='attrtabval'>";
            for (i = 0; i < Data.length; i++) {
                if (Data[i] != "" && atrval[i] != "" && (atrid[i] != "" || atrid[i] !== "undefined")) {
                    tabstr = tabstr + "<td dataval='" + atrid[i] + "' style='width:14em;'><input type='hidden' value='" + atrid[i] + "' class='hid2'  /> <input type='hidden' class='hidval2' value='" + atrval[i] + "'  />" + atrval[i] + "</td>";
                }
            }
            tabstr = tabstr + "<td style='width:8em;' dataval='" + itemQty + "'>" + itemQty + "<input type='hidden' class='itemqtycl' value='" + itemQty + "' /> </td><td style='width:6.5em;'><i class='fa fa-trash pluxtrash' onclick='removetr1()'></i></td>";
            tabstr = tabstr + "</tr>";
            $("#ProductInfoTable > tbody").append(tabstr);

            GetTotalQty();
        }
        else {
            swal({
                title: 'Error',
                text: "This Combination All Ready Add In Rows",
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                closeOnConfirm: false,
                closeOnCancel: false

            });
        }

        $("#itemQty1").val("");
        $("#ProductInfoTable1 tr.attrbtab_val2").children('td').find(".hidval").each(function () {
            var idname = this.getAttribute("id");

            var PIname = $("#proditemname").val();
            var iname = "";
            $.each(PIname.split(' '), function (i, data) {
                if (data != "" && data != "-") {
                    if (data[0] != "-") {
                        iname = iname + data[0];
                    }
                }
            });
            var myval = "";
            var myvalueno = "";
            if (idname == "LotNo1" || idname == "Batch1") {

                myval = this.value.split('-');
                myvalueno = myval[4];
                myvalueno = parseInt(myvalueno) + parseInt(1);
                this.value = myval[0] + "-" + myval[1] + "-" + myval[2] + "-" + iname + "-" + myvalueno + "-" + myval[5];
            }
            else {
                this.value = "";
            }


        });

    }
}

function GetTotalQty() {
    debugger;
    var tabstr = "";
    var ItemQty = 0;
    var Data = $('#Dynamic_ID option:selected').toArray().map(item => item.text).join();
    var index = Data.indexOf(',');
    if (index == -1) {
        Data = Data + ",";
    }
    Data = Data.split(',');
    $("#ProductInfoTable tr.attrtabval").children('td').find(".itemqtycl").each(function () {
        ItemQty = parseFloat(ItemQty) + parseFloat(this.value);
    });



    $("#TotalCount").html('');
    var len1 = $("#ProductInfoTable1 tr > td").length;
    tabstr = tabstr + "<tr class='attrtabval'>";
    for (i = 1; i <= Data.length; i++) {
        if (i == Data.length) {
            tabstr = tabstr + "<td style='width:14em;'><b>Total Quantity</b></td>";
        }
        else {
            tabstr = tabstr + "<td style='width:14em;'></td>";
        }

    }
    tabstr = tabstr + "<td style='width:8em;'  dataval='" + ItemQty + "'>" + ItemQty + "<input type='hidden' class='totqtycl' value='" + ItemQty + "' /></td><td style='width:5em;'></td>";
    tabstr = tabstr + "</tr>";
    $("#TotalCount").append(tabstr);
}

$(document).on('click', 'i.pluxtrash', function () {

    $(this).closest('tr').remove();
    GetTotalQty();
    return false;
});

function removetr1() {

    $(this).closest('tr').remove();
    GetTotalQty();
    return false;

}

$('#SaveProdInfo').on('click', function () {
    debugger;
    var TotalQty = "";
    var AcptQty = $("#proAcptQty").val();
    $("#TotalCount tr").children('td').find('.totqtycl').each(function (i) {

        TotalQty = this.value;  //here get td value
    });

    if (parseFloat(TotalQty) == parseFloat(AcptQty)) {
        var Astr = "";
        var $AdataRows = $("#ProductInfoTable tr:not('.attrbtab1')");
        var Arowlenght = $AdataRows.length;
        var Astylerowcount = 0;
        //var stylerowCount = $('#maintable2 tr').length;
        $AdataRows.each(function () {
            debugger;
            Acount = 0;
            Astylerowcount++;
            var Aleng = $(this).children('td').length;
            $(this).children('td').each(function (i) {
                debugger;
                var Aitemlist = ($(this).attr('dataval'));  //here get td value
                Acount++;

                if (Aitemlist != undefined) {

                    if (Acount == (Aleng - 1)) {
                        Astr += Aitemlist;
                    }
                    else {
                        Astr += Aitemlist + ">";
                    }
                }

            });

            if (Astylerowcount == Arowlenght) {
            }
            else {
                Astr += "<";
            }

        });
        var no = $("#proditemname").attr('dataval');
        $("#productinfo" + no).val(Astr);
        var Data = $('#Dynamic_ID option:selected').toArray().map(item => item.value).join();
        $("#producttype" + no).val(Data);
        $('.modal').modal('hide');
    }
    else {
        swal({
            title: 'Error',
            text: "Opening Quantity and Product Total Qunatity Does Not Match!",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
    }
});

