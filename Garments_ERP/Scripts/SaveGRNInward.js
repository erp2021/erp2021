﻿
function calBalQty(id) {
    debugger;
    var idval = id.match(/\d+/);
    var acceptqty = $("#acceptqty" + idval).val("");
    //var balanceqty = $("#balanceqty" + idval).val("");
    var poqty = $("#poqty" + idval).val();
    poqty = parseFloat(poqty).toFixed(2);
    var totProd = $("#totProd" + idval).val();
    totProd = parseFloat(totProd).toFixed(2);
    var chalanqty = $("#chalanqty" + idval).val();
    chalanqty = parseFloat(chalanqty).toFixed(2);
    var balanceqty = 0;
    if (parseFloat(chalanqty) <= parseFloat(poqty)) {
        $("#grnqty" + idval).val(chalanqty);
        var totq = parseFloat(chalanqty) + parseFloat(totProd);
        totq = parseFloat(totq).toFixed(2);
        if (parseFloat(poqty) > parseFloat(totq)) {
            balanceqty = parseFloat(poqty) - parseFloat(totq);
        }
        else {
            balanceqty = 0;
        }
    }
    else {
        msgshow("Please enter challan Qty. less than PO Qty.");

        $("#grnqty" + idval).val("");
        $("#acceptqty" + idval).val("");
        $("#chalanqty").val("");
        $("#chalanqty").focus();
    }
    balanceqty = parseFloat(balanceqty).toFixed(2);
    $("#balanceqty" + idval).val(balanceqty);

    if (parseFloat(chalanqty) + parseFloat(totProd) > parseFloat(poqty)) {
        var remi = parseFloat(poqty) - parseFloat(totProd);
        remi = parseFloat(remi).toFixed(2);
        msgshow("Only " + remi + " Is Remaining from PO Qty ");
        $("#balanceqty" + idval).val(balanceqty);
        $("#chalanqty" + idval).val("");
        $('#chalanqty' + idval).focus();
    }
}
function calAccptQty(id) {
    debugger;
    
    var idval = id.match(/\d+/);
    var poqty = $("#poqty" + idval).val();
    poqty = parseFloat(poqty).toFixed(2);
    var chalanqty = $("#chalanqty" + idval).val();
    chalanqty = parseFloat(chalanqty).toFixed(2);
    var totProd = $("#totProd" + idval).val();
    totProd = parseFloat(totProd).toFixed(2);
    if (parseFloat(chalanqty) <= parseFloat(poqty)) {
        $("#grnqty" + idval).val(chalanqty);
        var totq = parseFloat(chalanqty) + parseFloat(totProd);
        totq = parseFloat(totq).toFixed(2);
        if (parseFloat(poqty) > parseFloat(totq)) {
            balanceqty = parseFloat(poqty) - parseFloat(totq);
        }
        else {
            balanceqty = 0;
        }
        
    }
    balanceqty = parseFloat(balanceqty).toFixed(2);

    $("#balanceqty" + idval).val(balanceqty);
    var acceptqty = $("#acceptqty" + idval).val();
    acceptqty = parseFloat(acceptqty).toFixed(2);
    var balanceqty = $("#balanceqty" + idval).val();
    var bal = 0;
    if (parseFloat(acceptqty) > parseFloat(chalanqty)) {
        msgshow("Please enter accept Qty. less than or equal to challan Qty.");
        $("#acceptqty" + idval).val("");
        $("#btnSetProductInfo" + idval).attr('disabled', 'disabled');
    }
    else {
        $("#btnSetProductInfo" + idval).removeAttr('disabled');
    }
    if (parseFloat(acceptqty) < parseFloat(chalanqty)) {
        bal = parseFloat(chalanqty) - parseFloat(acceptqty);
        var totbal = parseFloat(balanceqty) + parseFloat(bal);
        totbal = parseFloat(totbal).toFixed(2);
        $("#balanceqty" + idval).val(totbal);
    }
}

function msgshow(alrtmsg) {
    swal({
        title: 'Error',
        text: alrtmsg,
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false,
        closeOnCancel: false

    });
}


$('#saveGRNInward').on('click', function () {
    debugger;
    var warehouseid = $("#warehouseid").val();
    var supplierid = $('#supplierid').val();
    var grnno = $("#grnno").val();
    var inwardDate = $('#inwardDate').val();
    var challanno = $("#Challano").val();
    var challanDate = $('#challanDate').val();
    var invoiceno = $('#invoiceno').val();
    var invoiceDate = $('#invoiceDate').val();
    var departmentid = $('#departmentid').val();
    var InvoiceAmount = $('#InvoiceAmount').val();
    var pono = $("#pono").val();
    var isvalid = true;
    var stcount = 0;

    if (warehouseid == "" || warehouseid == 0) {
        msgshow("Please Select Warehouse");
        isvalid = false;
    }
    else if (supplierid == "" || supplierid == 0) {
        msgshow("Please Select Supplier");
        isvalid = false;
    }
    else if (grnno == "") {
        msgshow("Please Enter GRN No");
        isvalid = false;
    }
    else if (challanno == "") {
        msgshow("Please Enter Challan No");
        isvalid = false;
    }
    else if (invoiceno == "") {
        msgshow("Please Enter Invoice No");
        isvalid = false;
    }
    else if (InvoiceAmount == "" || InvoiceAmount=="0") {
        msgshow("Please Enter Invoice amount.");
        isvalid = false;
    }
    else if (pono == "" || pono == "0") {
        msgshow("Please Select PO No.");
        isvalid = false;
    }
    else if (departmentid == "" || departmentid == "0") {
        msgshow("Please Select Department.");
        isvalid = false;
    }
    else {
        isvalid = true;
        var rawsizerowcount = 0;
        var $dataRows = $("#maintable2 tr:not('.titlerow')");
        var sizerowlenght = $dataRows.length;
        var sizerowstr = "";
        $dataRows.each(function () {
            count = 0;
            rawsizerowcount++;
            $(this).find('.itemdata').each(function (i) {
                var itemlist = ($(this).val());
                if (count == 7) {
                    stcount = itemlist == "" || itemlist == "0" ? 1 : 0;
                }

                count++;

                    if (count == 13) {
                        sizerowstr +=itemlist;
                    }
                    else {
                        sizerowstr += itemlist + "~";
                    }
              
            });

            if (rawsizerowcount == sizerowlenght) {

            }
            else {
                sizerowstr += "#";
            }

        });
        $('#grnitemstr').val(sizerowstr);


        var rawsizerowcount1 = 0;
        var $dataRows1 = $("#maintable2 tr:not('.titlerow')");
        var sizerowlenght1 = $dataRows1.length;
        var sizerowstr1 = "";
        $dataRows1.each(function () {
            count = 0;
            rawsizerowcount1++;
            $(this).find('.itemPIdata').each(function (i) {
                var itemlist1 = ($(this).val());
                count++;

                if (count == 2) {
                    sizerowstr1 +=itemlist1;
                }
                else {
                    sizerowstr1 += itemlist1 + "^";
                }

            });

            if (rawsizerowcount1 == sizerowlenght1) {

            }
            else {
                sizerowstr1 += "|";
            }

        });
        $('#grnitemPIstr').val(sizerowstr1);
        //alert(sizerowstr);
    }

    if (stcount == 1) {
        msgshow("Please Enter Product Details.");
        isvalid = false;
    }

    return isvalid;
});


function GetPIpp(id) {
    debugger;
    $("#Dynamic_ID option:selected").prop("selected", false);

    $("#ProductInfoTable > tbody").html('');
    $("#ProductInfoTable > thead").html('');
    $("#TotalCount").html('');
    $("#ProductInfoTable1").html('');
    
    var no = id.match(/\d+/);
    var pathsdat = window.location.pathname;
    pathsdat = pathsdat.split('/');
    if (pathsdat[2] == "Edit") {
        var PItithead = $("#producttype" + no).val();
        if (PItithead == "" || PItithead == null) {

            swal({
                title: 'Error',
                text: "No Product Info Fount!",
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                closeOnConfirm: false,
                closeOnCancel: false

            });
            $('.modal').modal('hide');

        }
        else {
            $('#ProdutInfoModal').modal('toggle');
        }
    }
    else {
        $('#ProdutInfoModal').modal('toggle');
    }



    
    var MyPIstr = $("#productinfo" + no).val();
    var name = $("#itemname" + no).val();
    $("#proditemname").val(name);
    $("#proditemname").attr('dataval', no);
    var acptqty = $("#acceptqty" + no).val();
    $("#proAcptQty").val(acptqty);

    if (MyPIstr != "") {
        var protype = $("#producttype" + no).val();
        if (protype != "") {
            $.each(protype.split(","), function (i, e) {
                $("#Dynamic_ID option[value='" + e + "']").prop("selected", true);
            });
            $("#Dynamic_ID").select2().trigger('change');
            var pid = document.getElementById('Dynamic_ID');
            GetProductvalue(pid);
        }
        $(".attrbtab1").removeAttr('style');
        $(".attrbtab2").attr('style', 'display:none;');

        $.each(MyPIstr.split('#'), function (i, data) {

            var lenchk = data.split('~');
            lenchk = lenchk.length;
            var tabstr = "";
            tabstr = tabstr + "<tr class='attrtabval'>";
            $.each(data.split('~'), function (i, data) {

                if (i == (lenchk - 1)) {
                    tabstr = tabstr + "<td dataval='" + data + "'>" + data + "<input type='hidden' class='itemqtycl' value='" + data + "' /> </td>";
                    
                    var path = window.location.pathname;
                    path = path.split('/');
                    if (path[2] == "Edit") {
                        tabstr = tabstr + "<td><i class='fa fa-trash pluxtrashdis' disabled></i></td>";
                    }
                    else {
                        tabstr = tabstr + "<td><i class='fa fa-trash pluxtrash' onclick='removetr()'></i></td>";
                    }
                }
                else {
                    tabstr = tabstr + "<td dataval='" + data + "'><input type='hidden' value='" + data + "' class='hid2'  /> <input type='hidden' class='hidval2' value='" + data + "'  />" + data + "</td>";
                }

            });
            tabstr = tabstr + "</tr>";
            $("#ProductInfoTable > tbody").append(tabstr);


        });
        GetTotalQty();
        $("#ProductInfoTable").attr('style', 'width:100%;display:inline-table;');
        var path = window.location.pathname;
        path = path.split('/');
        if (path[2] == "Edit") {
            $("#ProductInfoTable1").attr('style', 'display:none;');
            $(".bodyscol").attr('style', 'max-height:300px');
        }
    }
    else {
        $("#ProductInfoTable").attr('style', 'width:100%;display:none;');
    }
}


function GetProductvalue(aid) {
    debugger;
    var AttribId = $(aid).val();
    if (AttribId != "" && AttribId != null) {
        var Data = $('#Dynamic_ID option:selected').toArray().map(item => item.text).join();
        var index = Data.indexOf(',');
        if (index == -1) {
            Data = Data + ",";
        }
        Data = Data.split(',');
        var tabstr = "";
        $("#ProductInfoTable > thead").html("");
        tabstr = tabstr + "<tr  class='attrbtab1' style='display:none;'>";
        for (i = 0; i < Data.length; i++) {
            if (Data[i] != "") {
                tabstr = tabstr + "<th>" + Data[i] + "</th>";
            }
        }
        tabstr = tabstr + "<th>Quantity</th><th style='text-align:right;'>Action</th>";
        tabstr = tabstr + "</tr>";
        $("#ProductInfoTable > thead").append(tabstr);


        var tabstr1 = "";
        $("#ProductInfoTable1").html("");
        tabstr1 = tabstr1 + "<tr  class='attrbtab2'>";
        for (i = 0; i < Data.length; i++) {
            if (Data[i] != "") {
                tabstr1 = tabstr1 + "<th>" + Data[i] + "</th>";
            }
        }
        tabstr1 = tabstr1 + "<th>Quantity</th><th style='text-align: right;'>Action</th>";
        tabstr1 = tabstr1 + "</tr>";
        tabstr1 = tabstr1 + "<tr  class='attrbtab_val2'>";
        var index2 = AttribId.indexOf(',');
        if (index2 == -1) {
            AttribId = AttribId + ",";
        }
        AttribId = AttribId.split(',');

        for (i = 0; i < AttribId.length; i++) {
            var option = "";
            if (AttribId[i] != "") {
                if (Data[i] == "Expiry") {
                    tabstr1 = tabstr1 + "<td><div class='input-group date hasDatepicker' id='bs_datepicker_component_container4' style='border:1px solid #ccc;border-radius:5px;'><input type='text' class='form-control  pull-right MyExpire hidval'  placeholder='Ex:30/07/2016' value='' id='" + Data[i].split(" ").join("") + "1' name='" + Data[i].split(" ").join("") + "1' onclick='changedateStyle()' /><span class='input-group-addon'><i class='material-icons'>date_range</i></span></div></td>";
                }
                else if (Data[i] == "Lot No") {
                    var PIname = $("#proditemname").val();
                    var iname = "";
                    $.each(PIname.split(' '), function (i, data) {
                        if (data != "" && data != "-") {
                            if (data[0] != "-") {
                                iname = iname + data[0];
                            }
                        }
                    });
                    var radno = Math.random().toFixed(3).split(".").pop();
                    radno = parseInt(radno);
                    radno = radno.length == 2 ? radno + "1" : radno;
                    var no = Data[i].split(" ").join("")[0] + "-ML-PR-" + iname+"-" + radno+"-"+Getreturn();
                    tabstr1 = tabstr1 + "<td><input type='text' class='form-control hidval' readonly value='" + no +"' id='" + Data[i].split(" ").join("") + "1' name='" + Data[i].split(" ").join("") + "1' /></td>";
                }
                else{
                  
                    tabstr1 = tabstr1 + "<td><input type='text' class='form-control hidval' id='" + Data[i].split(" ").join("") + "1' name='" + Data[i].split(" ").join("") + "1' /></td>";
                }

                
            }
        }
        tabstr1 = tabstr1 + "<td><input type='text' id='itemQty1'  class='form-control' onkeypress='return isNumber(event)' /></td><td><i class='fa fa-plus pluxad' onclick='SetProductValue()'></i></td>";
        tabstr1 = tabstr1 + "</tr>";
        $("#ProductInfoTable1").append(tabstr1);

        var cnt = 0;
        $(".MyExpire").datepicker({
            dateFormat: 'mm/dd/yy',
        minDate: 0
    });
    }
    else {
        debugger;
        $("#ProductInfoTable1").html("");
        $("#ProductInfoTable > thead").html("");
        $("#ProductInfoTable > tbody").html("");
        $("#Dynamic_ID  option:selected").val("");
    }
}

function changedateStyle() {
   
    $("#ui-datepicker-div").css("z-index", "10000");
}

function Getreturn() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = + mm + dd +yyyy;
    return today;
}

function SetProductValue() {
    debugger;
    var comm = {};
    var savedata = {};
    itemQty = $("#itemQty1").val();
    var isvalid = true;
    var ErrorMessage = "";
    //validations of item master

    $("#ProductInfoTable1 tr.attrbtab_val2").children('td').find(".hidval").each(function () {
        var chkatrval = this.value;
        if (chkatrval == "" || chkatrval == "0") {
            ErrorMessage = "Please Enter Details!";
            isvalid = false;
        }
    });


    if (itemQty == "") {
        ErrorMessage = "Please Enter Item Quantity";
        isvalid = false;
    }
    if (ErrorMessage != "") {
        swal({
            title: 'Error',
            text: ErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        isvalid = false;
    }
    else {
       
        var Data = $('#Dynamic_ID option:selected').toArray().map(item => item.text).join();
        var index = Data.indexOf(',');
        if (index == -1) {
            Data = Data + ",";
        }
        Data = Data.split(',');
        debugger
        var count = 0;
        var temprray = "";
        var atrval = "";
        var item1 = {};
        $("#ProductInfoTable1 tr.attrbtab_val2").children('td').find(".hidval").each(function () {
            atrval = atrval + "," + this.value;
        });

        $("#ProductInfoTable").find(".attrtabval").each(function () {
            temprray = "";
            $(this).children('td').find(".hidval2").each(function () {
                temprray = temprray + "," + this.value;
            });
            if (atrval == temprray) {
                count++;
            }
        });



        if (count == 0) {
            debugger;
            $("#ProductInfoTable").attr('style', 'width: 100%;display:inline-table;');
            var atrid;
            $("#ProductInfoTable1 tr.attrbtab_val2").children('td').find(".hidval").each(function () {
                atrid = atrid + "," + this.value;
            });
            atrid = atrid.split(',');
            atrid.splice(0, 1);
            atrval = atrval.split(',');
            atrval.splice(0, 1);


            $(".attrbtab1").removeAttr('style');
            $(".attrbtab2").attr('style', 'display:none;');
            var tabstr = "";
            var len1 = $("#ProductInfoTable tr").length;
            tabstr = tabstr + "<tr class='attrtabval'>";
            for (i = 0; i < Data.length; i++) {
                if (Data[i] != "" && atrval[i] != "" && (atrid[i] != "" || atrid[i] !== "undefined")) {
                    tabstr = tabstr + "<td dataval='" + atrid[i] + "'><input type='hidden' value='" + atrid[i] + "' class='hid2'  /> <input type='hidden' class='hidval2' value='" + atrval[i] + "'  />" + atrval[i] + "</td>";
                }
            }
            tabstr = tabstr + "<td dataval='" + itemQty + "'>" + itemQty + "<input type='hidden' class='itemqtycl' value='" + itemQty+"' /> </td><td><i class='fa fa-trash pluxtrash' onclick='removetr()'></i></td>";
            tabstr = tabstr + "</tr>";
            $("#ProductInfoTable > tbody").append(tabstr);

            GetTotalQty();
        }
        else {
            swal({
                title: 'Error',
                text: "This Combination All Ready Add In Rows",
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                closeOnConfirm: false,
                closeOnCancel: false

            });
        }

        $("#itemQty1").val("");
        $("#ProductInfoTable1 tr.attrbtab_val2").children('td').find(".hidval").each(function () {
            var idname = this.getAttribute("id");
           
            var PIname = $("#proditemname").val();
            var iname = "";
            $.each(PIname.split(' '), function (i, data) {
                if (data != "" && data != "-") {
                    if (data[0] != "-") {
                        iname = iname + data[0];
                    }
                }
            });
            var myval = "";
            var myvalueno = "";
            if (idname == "LotNo1") {
                myval = this.value.split('-');
                myvalueno = myval[4];
                myvalueno = parseInt(myvalueno) + parseInt(1);
                this.value = myval[0] + "-" + myval[1] + "-" + myval[2] + "-" + iname+"-" + myvalueno + "-" + myval[5];
            }
            else {
                this.value = "";
            }
            //if (idname == "Batch1") {
            //    myval = this.value.split('-');
            //    myvalueno = myval[3].substr(myval[3].length - 3);
            //    myvalueno = parseInt(myvalueno) + parseInt(1);
            //    this.value = myval[0] + "-" + myval[1] + "-" + myval[2] + "-" + iname + myvalueno + "-" + myval[4];
            //}
           
        });

    }
}

function GetTotalQty() {
    debugger;
    var tabstr = "";
    var ItemQty = 0;
    var Data = $('#Dynamic_ID option:selected').toArray().map(item => item.text).join();
    var index = Data.indexOf(',');
    if (index == -1) {
        Data = Data + ",";
    }
    Data = Data.split(',');
    $("#ProductInfoTable tr.attrtabval").children('td').find(".itemqtycl").each(function () {
        ItemQty = parseFloat(ItemQty) + parseFloat(this.value);
    });



    $("#TotalCount").html('');
    var len1 = $("#ProductInfoTable1 tr > td").length;
    tabstr = tabstr + "<tr class='attrtabval'>";
    for (i = 1; i <= Data.length; i++) {
        if (i == Data.length) {
            tabstr = tabstr + "<td><b>Total Quantity</b></td>";
        }
        else {
            tabstr = tabstr + "<td></td>";
        }

    }
    tabstr = tabstr + "<td dataval='" + ItemQty + "'>" + ItemQty + "<input type='hidden' class='totqtycl' value='" + ItemQty +"' /></td><td></td>";
    tabstr = tabstr + "</tr>";
    $("#TotalCount").append(tabstr);
}

$(document).on('click', 'i.pluxtrash', function () {
   
    $(this).closest('tr').remove();
    GetTotalQty();
    return false;
});

function removetr() {
   
    $(this).closest('tr').remove();
    GetTotalQty();
    return false;

}

$('#SaveProdInfo').on('click', function () {
   
    var TotalQty = "";
    var AcptQty = $("#proAcptQty").val();
    $("#TotalCount tr").children('td').find('.totqtycl').each(function (i) {
        
        TotalQty =this.value;  //here get td value
    });

    if (parseFloat(TotalQty) == parseFloat(AcptQty)) {
        var Astr = "";
        var $AdataRows = $("#ProductInfoTable tr:not('.attrbtab1')");
        var Arowlenght = $AdataRows.length;
        var Astylerowcount = 0;
        //var stylerowCount = $('#maintable2 tr').length;
        $AdataRows.each(function () {
            debugger;
            Acount = 0;
            Astylerowcount++
            var Aleng = $(this).children('td').length;
            $(this).children('td').each(function (i) {
                debugger;
                var Aitemlist = ($(this).attr('dataval'));  //here get td value
                Acount++;

                if (Aitemlist != undefined) {

                    if (Acount == (Aleng - 1)) {
                        Astr += Aitemlist;
                    }
                    else {
                        Astr += Aitemlist + "~";
                    }
                }

            });

            if (Astylerowcount == Arowlenght) {
            }
            else {
                Astr += "#";
            }

        });
        var no = $("#proditemname").attr('dataval');
        
        $("#productinfo" + no).val(Astr);
        var Data = $('#Dynamic_ID option:selected').toArray().map(item => item.value).join();
        $("#producttype" + no).val(Data);
        $('.modal').modal('hide');
    }
    else {
        swal({
            title: 'Error',
            text: "Accept Quantity and Product Total Qunatity Does Not Match!",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
    }
});

function pophide() {

    $('.modal').modal('hide');

}





