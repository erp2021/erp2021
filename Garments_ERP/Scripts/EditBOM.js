﻿function setswal(msg) {
    if (ErrorMessage.length > 0) {
        ErrorMessage += "\n" + msg;
    } else { ErrorMessage = msg; }
}
var ErrorMessage = "";



function calQty(id) {
    var number = id.match(/\d+/);
    var total = 0;
    $(".sizetd" + number).each(function () {

        if ($(this).val() != "") {
            total = total + parseFloat($(this).val());
        }
    });
    //alert(total);
    $("#Acceptedqty" + number).val(total);
}


$('#SaveBOM').on('click', function () {

    var IsReadymate = $("#hidIsReadyMate").val();
    if (parseInt(IsReadymate) == 0) {
        debugger;
        ErrorMessage = "";
        var str = "";
        var count = 0;
        var rowcount = 0;
        var isvalid = true;
        var $dataRows1 = $("#rawitemtable tr:not('.rowtitle')");
        var rowlenght1 = $dataRows1.length;
        if (rowlenght1 > 0) {
            $dataRows1.each(function () {
                count = 0;
                rowcount++;
                $(this).find('.rawitemlist').each(function (i) {
                    debugger;
                    var contactlist = ($(this).val());
                    count++;
                    if (i == 3) {
                        if (contactlist == "") {
                            contactlist = "--";
                        }
                    }
                    if (contactlist != undefined && contactlist != "") {
                        if (i == 3 && contactlist == "--") {
                            contactlist = " ";
                        }

                        if (count == 10) {
                            str += contactlist;
                        }
                        else {
                            str += contactlist + "~";
                        }
                    }
                    else {
                        setswal("Please Fill Raw Item Details");
                        isvalid = false;
                    }

                });

                if (rowcount == rowlenght1) {
                    rowlenght1 = rowlenght1;
                }
                else {
                    str += "#";
                }
            });
            $('#BOMitemList').val(str);
        }
        else {
            setswal("Please Fill Raw Item Details");
            isvalid = false;
        }
        
        SetProcessCycle();
    }
    addstyle();
    var str2 = "";
    var rawitemstr = "";
    var $dataRows = $("#hidestyletable tr:not('.titlerow')");
    var rowlenght = $dataRows.length;
    var stylerowcount = 0;
    //var stylerowCount = $('#maintable2 tr').length;
    $dataRows.each(function () {
       
        count = 0;
        stylerowcount++;
        var Aleng = $(this).children('td').length;
        $(this).find('.stylerow').each(function (i) {
           
            var itemlist = ($(this).val());  //here get td value
            count++;

            if (itemlist != undefined) {

                if (count == Aleng) {
                    str2 += itemlist;
                }
                else {
                    str2 += itemlist + "~";
                }
            }

        });

        if (stylerowcount == rowlenght) {
            rowlenght = rowlenght;
        }
        else {
            str2 += "#";
        }

    });


    $('#BOMSizeList').val(str2);

    if (ErrorMessage.length > 0) {
        swal({
            title: 'Error',
            text: ErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        isvalid = false;
    }
    return isvalid;

});



//List Of Process Cycle 
function GetProcessCycle() {


    var procscycleid = $("#Prooce_Cycle_id").val();
    var Josndata = { 'ProcessCycle': procscycleid };
    var MyJson = JSON.stringify(Josndata);

    $.ajax({
        type: "POST",
        url: '/BillOfMaterial/SaveAutoPR/',
        data: "{'Model':'" + MyJson + "','type':7}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            debugger;
            $("#rawitemOuttable > tbody").html('');
            var prow = ' <tr>';
            var i = 0;
            var allPRows = '';
            result = JSON.parse(result);
            $.each(result.Table, function (key, processDetail) {
                i++;
                var newPRow = ' <tr>';

                newPRow = newPRow + '<td>' + processDetail.SHORT_NAME + '<input id="procName' + (i + 1) + '" type="hidden" value="' + processDetail.SHORT_NAME + '" /><input id="procid' + (i + 1) + '" type="hidden" value="' + processDetail.ProcessID + '" class="rawitemlist" /></td>';
                newPRow = newPRow + '<td>' + processDetail.Processsequence + '<input id="procSeq' + (i + 1) + '" type="hidden" value="' + processDetail.Processsequence + '"  class="rawitemlist" /></td>';
                newPRow = newPRow + '<td><input type="hidden" id="outitemcategoryid' + (i + 1) + '" name="outitemcategoryid" value="" /><input type="hidden" id="OldItemname' + (i + 1) + '" name="OldItemname" value="" /> <input type="text" class="form-control text-box single-line ui-autocomplete-input valid" data-val="true" value="" autocomplete="off" id="rawoutitemnameid' + (i + 1) + '" name="rawoutitemnameid' + (i + 1) + '"  onkeydown="GetOutrawitemlist(' + (i + 1) + ')" style="width:75%"; onchange="Checkitem(this.id)" /><input type="hidden" id="OutItem' + (i + 1) + '" name="OutItem" class="form-control rawitemlist" />';
                newPRow = newPRow + '<div id="OTmsg' + (i + 1) + '" style="color:red; display:none"> This Item is not availble! Do you want to create Item? <a href="#" id="Y' + (i + 1) + '" onclick=Showhidepopup(this.id,"Yes")>Yes</a>/<a href="#" id="N' + (i + 1) + '" onclick=Showhidepopup(this.id,"No")>No</a></div></td>';
                newPRow = newPRow + '<td style="display:none;"><input type="hidden" id="rawIAoutid' + (i + 1) + '"   class="rawitemlist" /> </td>';
                newPRow = newPRow + '<td><input type="text" class="form-control rawitemlist" id="Perqty' + (i + 1) + '" name="Perqty' + (i + 1) + '" style="width:75%"; onkeypress="return isNumber(event)" onkeyup=GetoutReqQty("' + (i + 1) + '") /></td>';
                newPRow = newPRow + '<td><input type="text" readonly class="form-control rawitemlist" id="Requiredqty' + (i + 1) + '" name="Requiredqty' + (i + 1) + '" style="width:75%"; /></td>';


                newPRow = newPRow + '</tr>';

                allPRows = allPRows + newPRow;
            });

            $("#rawitemOuttable > tbody").html(allPRows);

        },
        failure: function (response) {
            alert(response.d);
        }
    });

}
function GetoutReqQty(no) {
    var perqty = $("#Perqty" + no).val() == "" ? 0 : $("#Perqty" + no).val();
    var Totqty = $("#reqqty").val();
    var TotReqQty = perqty * Totqty;

    $("#Requiredqty" + no).val(TotReqQty);

}
function pophide() {

    $('.modal').modal('hide');

}
function Showhidepopup(id, name) {
    var no = id.match(/\d+/)[0];
    if (name == "No") {
        $("#rawoutitemnameid" + no).val("");
        $("#OutItem" + no).val("");
        $("#OTmsg" + no).attr('style', 'color:red; display:none');
    }
    else {
        $("#ItemInfo").modal('toggle');
        $("#rowno").val(no);
        var itemname = $("#rawoutitemnameid" + no).val();
        $("#ItemName").val(itemname);
    }
}
function Checkitem(id) {
    var no = id.match(/\d+/)[0];
    var olditemname = $("#rawoutitemnameid" + no).val();
    var OdItem = $("#OldItemname" + no).val();
    var outitemid = $("#OutItem" + no).val();
    if (OdItem == "") {
        if (outitemid == "" || outitemid == "0") {
            $("#OTmsg" + no).attr('style', 'color:red; display:block');
        }
        else {
            $("#OTmsg" + no).attr('style', 'color:red;display:none');

            $("#OldItemname" + no).val(olditemname);
        }
    }
    else {
        if (OdItem == olditemname) {
            $("#OTmsg" + no).attr('style', 'color:red; display:none');
        }
        else {
            $("#OTmsg" + no).attr('style', 'color:red; display:block');
        }
        $("#OutItem" + no).val('');
    }
}


function SetProcessCycle() {

    var str2 = "";
    var rawitemstr = "";
    var $dataRows = $("#rawitemOuttable tr:not('.titlerow')");
    var rowlenght = $dataRows.length;
    var stylerowcount = 0;
    //var stylerowCount = $('#maintable2 tr').length;
    $dataRows.each(function () {
        debugger;
        count = 0;
        stylerowcount++;
        var Aleng = $(this).children('td').length;
        $(this).find('.rawitemlist').each(function (i) {
            debugger;
            var itemlist = ($(this).val());  //here get td value
            count++;

            if (itemlist != undefined) {

                if (count == Aleng) {
                    str2 += itemlist;
                }
                else {
                    str2 += itemlist + "~";
                }
            }

        });

        if (stylerowcount == rowlenght) {
            rowlenght = rowlenght;
        }
        else {
            str2 += "#";
        }

    });

    $("#bomprocessCyclelist").val(str2);

}

function addstyle() {

    var segmenttype = document.getElementById("segmenttypeid");
    var selectedsegment = segmenttype.options[segmenttype.selectedIndex].text;
    var segmenttypeid = $("#segmenttypeid").val();

    //var stylename = document.getElementById("styleid");
    //var selectedstyle = stylename.options[stylename.selectedIndex].text;
    //var styleid = $("#styleid").val();

    var selectedstyle = $("#stylenameid").val();
    var styleid = $("#styleid").val();

    //var item = document.getElementById("itemid");
    //var selectedItem = item.options[item.selectedIndex].text;
    //var itemid = $("#itemid").val();

    var selectedItem = $("#itemnameid_SF").val();
    var itemid = $("#itemid").val();


    var styleno = $("#styleno").val();
    var styledesc = $("#styledesc").val();
    var brand = $("#brand").val();

    var styleimg = $("#styleimg").val();
    var reqqty = $("#reqqty").val();

    var rowCount = 0;

    var i = $('#hidestyletable tr').length;

    if (segmenttypeid == "") {
        // $('#segmentmsg').text("select segment type");
        //setswal("Select Segment Type");
        segmenttypeid = 0;
    }

    if (styleid == "") {
        // $('#stylemsg').text("select style");
        setswal("Select Style");
    }
    if (itemid == "") {
        //$('#itemmsg').text("select item");
        setswal("Select Item");
        // alert(itemid);
    }
    if (reqqty == "") {
        //$('#qtymsg').text("enter require quantity");
        setswal("Enter Require Quantity");
    }

    if (ErrorMessage.length > 0) {
        swal({
            title: 'Error',
            text: ErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        isvalid = false;
    }
    else {


        $('#segmentmsg').text("");
        $('#stylemsg').text("");
        $('#itemmsg').text("");
        $('#stylecolormsg').text("");
        $('#styleshademsg').text("");
        $('#qtymsg').text("");
        //segment  item style brand stydesc color shade image reqqty sizeratio
        var lengthofsize = $('#AttributeTable tr').length;
        var j = 0;
        // for (j = 0; j < lengthofsize; j++) {
        var rawsizerowcount = 0;
        var $dataRows3 = $("#AttributeTable tr:not('.attrbtab1')");
        var sizerowlenght = $dataRows3.length;
        var sizerowstr = "";

        $dataRows3.each(function () {
            debugger;
            var sizearray = [];
            //get row of sizetable
            var attributeval = "";
            var EnqId = "";
            var ReqQty = "";
            var Rate = "";
            var sizeid = "";
            var Sellrate = "";
            var GST = "";
            var taxval = "";
            var tot = "";
            var imgcount = "";
            $(this).children('td').each(function (i) {
                debugger;
                $(this).find(".hid2").each(function () {
                    if (i == 0) {
                        attributeval = this.value;
                    }
                    else {
                        attributeval = attributeval + "," + this.value;
                    }
                });
                $(this).find(".EnqIdcs").each(function () {
                    EnqId = this.value;
                });

                $(this).find(".IAReqQty").each(function () {
                    ReqQty = this.value;
                });

                $(this).find(".IARate").each(function () {
                    Rate = this.value;
                });

                $(this).find(".IAsizeid").each(function () {
                    sizeid = this.value;
                });
                $(this).find(".IASellRate").each(function () {
                    Sellrate = this.value;
                });
                $(this).find(".IAGSTCharge ").each(function () {
                    GST = this.value;
                });
                $(this).find(".IATax").each(function () {
                    taxval = this.value;
                });
                $(this).find(".IAtotal").each(function () {
                    tot = this.value;
                });
                $(this).find(".IAImgcount").each(function () {
                    imgcount = this.value;
                });


            });
            var attributeid = $("#AttributeId").val();
            hidestylediv = '<tr class="data-contact-person odd" id="' + (i + 1) + '" value="' + (i + 1) + '" data="' + (i + 1) + '">' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + EnqId + '" class="form-control stylerow"></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + segmenttypeid + '" class="form-control stylerow"></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + styleid + '" class="form-control stylerow"></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + itemid + '" class="form-control stylerow"></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + styledesc + '" class="form-control stylerow"></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + brand + '" class="form-control stylerow"></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + styleimg + '" class="form-control stylerow"></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + attributeid + '" class="form-control stylerow"></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + attributeval + '" class="form-control stylerow"></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + ReqQty + '" class="form-control stylerow"></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + Rate + '" class="form-control stylerow"></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + sizeid + '" class="form-control stylerow"></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + Sellrate + '" class="form-control stylerow"></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + GST + '" class="form-control stylerow"></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + taxval + '" class="form-control stylerow"></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + tot + '" class="form-control stylerow"></td>' +
                '<td style="padding-right:10px;"><input type="hidden" value="' + imgcount + '" class="form-control stylerow"></td>' +
                '</tr>';
            $('#hidestyletable').append(hidestylediv);

        });
    }
}

$('#AddRawItem').on('click', function () {
    debugger;
    ErrorMessage = [];
    var subcatname = $("#itemsubcategoryidNameid").val();
    var subcatid = $("#itemsubcategoryid").val();
    var itemname = $("#rawitemnameid").val();
    var itemtid = $("#itemrawid").val();
    var IAid = $("#itemrawAid").val();
    var supname = $("#supplier").val();
    var perQty = $("#perqtyid").val();
    var totQty = $("#totqtyid").val();
    var availinstock = $("#availinstockid").val();
    var unitid = $("#_unitid").val();
    var unitname = $("#_unitid option:selected").text();


    var isvalid = true;
    if (subcatid == "") {
        setswal("Please Select Item Sub Category");
        isvalid = false;
    }
    else if (itemtid == "") {
        setswal("Please Select Item Category");
        isvalid = false;
    }
    else if (perQty == "" && perQty == "0") {
        setswal("Please Enter Per Quantity");
        isvalid = false;
    }
    if (ErrorMessage.length > 0) {
        swal({
            title: 'Error',
            text: ErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        isvalid = false;
    }
    else {

        var $dataRows1 = $("#rawitemtable tr:not('.rowtitle')");
        var file1 = document.querySelector('#rawimg');
        var fcount = file1.files.length;
        var r = $dataRows1.length + 1;
        var datatr = '<tr class="data-contact-person odd" id="' + r + '" value="' + r + '" data="' + r + '">' +
            '<td style="padding-right:10px;" class="rawsizerow"><input type="hidden" id="hiddenitemcategoryid' + r + '" value="' + subcatid + '" class="form-control rawitemlist"><p>' + subcatname + '</p></td>' +
            '<td style="padding-right:10px;" class="rawsizerow"><input type="text" class="form-control text-box single-line ui-autocomplete-input valid" data-val="true" autocomplete="off" id="rawitemnameid' + r + '" name="rawitemnameid' + r + '"  onkeydown=Getrawitemlist("' + r + '")  onchange=setItemUnit("' + r + '") value="' + itemname + '" /><input type="hidden" id="rawitemid' + r + '" name="rawitemid' + r + '" value="' + itemtid + '" class="form-control rawitemlist"><input type="hidden" id="rawIAid' + r + '" name="rawIAid' + r + '" value="' + IAid + '" class="form-control rawitemlist"></td>' +
            '<td style="padding-right:10px;" class="rawsizerow"><input type="hidden" value="' + supname + '" class="form-control rawitemlist"><p>' + supname + '</p></td>' +
            '<td style="padding-right:10px;" class="rawsizerow"><input type="text" id="Perqty_' + r + '" class="form-control rawitemlist" name="Perqty_' + r + '" value="' + perQty + '" onkeypress="return isNumber(event)" onkeyup=GetTotReqQty("' + r + '")></td>' +
            '<td style="padding-right:10px;" class="rawsizerow"><input type="text" id="sizecount' + r + '" value="0" name="sizecount" style="display:none" /><input type="text" id="Acceptedqty' + r + '" class="form-control rawitemlist" name="Acceptedqty" value="' + totQty + '"  readonly></td>' +
            '<td style="padding-right:10px;" class="rawsizerow"><input type="text" id="AvailInStock_' + r + '" class="form-control rawitemlist" name="AvailInStock" value="' + availinstock + '"  readonly></td>' +
            '<td style="padding-right:10px;" class="rawsizerow"><input type="hidden" id="hiddenunitid" value="' + unitid + '" class="form-control rawitemlist"><input type="hidden" value="0" class="rawitemlist" />' + unitname + '</td>' +
            '<td style="padding-right:10px;" class="rawitemrow" id="imgraw' + r + '">';
        if (parseInt(fcount) > 0) {
            datatr = datatr + '<input type="button" class="btn btn-info" value="View Image" id="Rviewimage' + r + '"  onclick=GetImageUpload(this.id,"preview","RawItem") /><input type="button" class="btn btn-info" id="Ruploadimage' + r + '" value="Upload Image" style="display: none;" onclick=GetImageUpload(this.id,"upload","RawItem") />';
        }
        else {
            datatr = datatr + '<input type="button" class="btn btn-info" value="View Image" id="Rviewimage' + r + '" style="display: none;"  onclick=GetImageUpload(this.id,"preview","RawItem") /><input type="button" class="btn btn-info" id="Ruploadimage' + r + '" value="Upload Image"  onclick=GetImageUpload(this.id,"upload","RawItem") />';
        }
        datatr = datatr + '<input type="hidden" value="' + fcount + '" class="form-control rawitemlist" id="rawitemcount' + r + '"><input type="hidden" value="-" class="form-control rawitemlist">';
        datatr = datatr + '<input type="file" style="display:none;" id="rawitemimg' + r + '" name="rawitemimg" class="form-control" multiple /></td>';
        datatr = datatr + '<td style="padding-right:10px;" class="rawsizerow"><i class="fa fa-trash pluxtrash" onclick="removetr()"></i></tr>';
        $("#rawitemtable").append(datatr);

        if (parseInt(fcount) > 0) {
            var file2 = document.querySelector('#rawitemimg' + r);
            file2.files = file1.files;
            var emptyFile = document.createElement('input');
            emptyFile.type = 'file';
            file1.files = emptyFile.files;
        }

        $("#itemsubcategoryidNameid").val("");
        $("#itemsubcategoryid").val("");
        $("#rawitemnameid").val("");
        $("#itemrawid").val("");
        $("#supplier").val("");
        $("#supplierNameid").val("");
        $("#perqtyid").val("");
        $("#totqtyid").val("");
        $("#availinstockid").val("");
        $("#_unitid").val("");


    }
    return isvalid;
});


$(document).on('click', 'i.pluxtrash', function () {

    $(this).closest('tr').remove();

    return false;
});

//To add new out Item
function removetr() {

    $(this).closest('tr').remove();
    return false;
}
$('#SaveItemInfo').on('click', function () {
    debugger;
    var Astr = "";
    var Arawitemstr = "";
    var $AdataRows = $("#AttributeTable2 tr:not('.attrbtab1')");
    var Arowlenght = $AdataRows.length;
    var Astylerowcount = 0;
    //var stylerowCount = $('#maintable2 tr').length;
    $AdataRows.each(function () {
        debugger;
        Acount = 0;
        Astylerowcount++
        var Aleng = $(this).children('td').length;
        $(this).children('td').each(function (i) {
            debugger;
            var Aitemlist = ($(this).attr('dataval'));  //here get td value
            Acount++;

            if (Aitemlist != undefined) {

                if (Acount == (Aleng - 1)) {
                    Astr += Aitemlist;
                }
                else {
                    Astr += Aitemlist + "~";
                }
            }

        });

        if (Astylerowcount == Arowlenght) {


        }
        else {
            Astr += "#";
        }

    });

    $("#AttributList1").val(Astr);

    ErrorMessage = "";
    var str = "";
    var count = 0;
    var rowcount = 0;
    var isFirstItem = 0;
    var $dataRows = $("#itemdoctable tr:not('.titlerow')");
    var rowlenght = $dataRows.length;
    var Itemgrouplist = $('#Itemgrouplist').val();
    var Unitlist = $('#Unitlist').val();
    var Attribute_ID = $('#Attribute_ID').val();
    var ItemCategoryid = $('#itemcategoryid').val();
    var Itemsubcategoryid = $('#itemsubcategory').val();
    var ItemName = $("#ItemName").val();
    var OpeningStock = $('#OpeningStock').val();
    var isvalid = true;
    //validations of item master
    if (ItemCategoryid == "" || ItemCategoryid == 0) {
        setswal("Please Select Item Category");
        isvalid = false;
    }
    else if (Itemgrouplist == "" || Itemgrouplist == 0) {
        setswal("Please Select Itemgroup");
        isvalid = false;
    }
    else if (Unitlist == "" || Unitlist == 0) {
        setswal("Please Select Unit");
        isvalid = false;
    }
    else if (Attribute_ID == "" || Attribute_ID == 0) {
        setswal("Please Select Attribute_ID");
        isvalid = false;
    }
    else if (Itemsubcategoryid == "" || Itemsubcategoryid == 0) {
        setswal("Please Select Item Sub-Category");
        isvalid = false;
    }
    else if (ItemName == "") {
        setswal("Please Enter Item Name");
        isvalid = false;
    }
    else if (OpeningStock == "") {
        setswal("Please Enter Item Opening Stock");
        isvalid = false;
    }



    if (ErrorMessage.length > 0) {
        swal({
            title: 'Error',
            text: ErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        isvalid = false;
    }

    else {
        debugger;
        var $form = $("#prodform");
        var formData = $form.serializeArray();
        var companyid = $("#companyid").val();
        var company = { 'CompanyId': companyid };
        formData.push(company);
        var branchid = $("#branchid").val();
        var branch = { 'BranchId': branchid };
        formData.push(branch);
        var UserId = $("#userid").val();
        var user = { 'UserId': UserId };
        formData.push(user);

        var MyJson = JSON.stringify(formData);
        $.ajax({
            type: "POST",
            url: '/BillOfMaterial/SaveAutoPR/',
            data: "{'Model':'" + MyJson + "','type':12}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                debugger;
                result = JSON.parse(result);
                $.each(result.Table, function (i, item) {
                    if (item.save == 200) {
                        swal({
                            title: 'Success',
                            text: 'Item Save Successfully',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "OK",
                            closeOnConfirm: true

                        },
                            function (isConfirm) {
                                if (isConfirm) {
                                    var rowno = $("#rowno").val();
                                    $('#OutItem' + rowno).val(item.itemid);
                                    $('#rawIAoutid' + rowno).val(item.Item_Attribute_ID);
                                    $('#rawoutitemnameid' + rowno).val(item.itemname);
                                    $('.modal').modal('hide');
                                    $("#OTmsg" + rowno).attr('style', 'color:red; display:none');
                                }
                            }
                        );




                    }
                    else {
                        swal({
                            title: 'Error',
                            text: item.ErrorMessage,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: false,
                            closeOnCancel: false

                        });
                    }

                });
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    return isvalid;

});
function SetTotalStock(Qty) {
    var itemQty = $("#OpeningStock").val() == "" ? 0 : $("#OpeningStock").val();
    var totstock = 0;
    $("#AttributeTable tr.attrtabval").children('td').find(".itemqtycl").each(function () {
        totstock = parseFloat(totstock) + parseFloat(this.value);
    });
    $("#OpeningStock").val(totstock);

}
function GetAttributevalue(aid) {
    debugger;
    var AttribId = $(aid).val();

    if (AttribId != "" && AttribId != null) {
        var Data = $('#Attribute_ID option:selected').toArray().map(item => item.text).join();
        var index = Data.indexOf(',')
        if (index == -1) {
            Data = Data + ",";
        }
        Data = Data.split(',');
        var tabstr = "";
        $("#AttributeTable2").html("");
        tabstr = tabstr + "<tr  class='attrbtab1' style='display:none;'>";
        for (i = 0; i < Data.length; i++) {
            if (Data[i] != "") {
                tabstr = tabstr + "<th style='width:10em;'>" + Data[i] + "</th>";
            }
        }
        tabstr = tabstr + "<th style='width:10em;'>Opening Stock</th> <th  style='width:10em;'>Rates</th><th style='width:10em;'>Re-Order Level</th><th style='width:5em;'>Action</th>";
        tabstr = tabstr + "</tr>"
        $("#AttributeTable2").append(tabstr);


        var tabstr1 = "";
        $("#AttributeTable1").html("");
        tabstr1 = tabstr1 + "<tr  class='attrbtab2'>";
        for (i = 0; i < Data.length; i++) {
            if (Data[i] != "") {
                tabstr1 = tabstr1 + "<th style='width:10em;'>" + Data[i] + "</th>";
            }
        }
        tabstr1 = tabstr1 + "<th  style='width:10em;'>Opening Stock</th> <th  style='width:10em;'>Rates</th><th  style='width:10em;'>Re-Order Level</th><th style='width:5em;'>Action</th>";
        tabstr1 = tabstr1 + "</tr>"
        tabstr1 = tabstr1 + "<tr  class='attrbtab_val2'>";
        var index2 = AttribId.indexOf(',');
        if (index2 == -1) {
            AttribId = AttribId + ",";
        }
        AttribId = AttribId.split(',');

        for (i = 0; i < AttribId.length; i++) {
            var option = "";
            if (AttribId[i] != "") {
                tabstr1 = tabstr1 + "<td style='width:10em;'><input type='hidden' id='Hid" + Data[i].split(" ").join("") + "1' class='hidid' /><input type='hidden' id='Hv" + Data[i].split(" ").join("") + "1' class='hidval' /><input type='text' datavalue='' class='form-control text-box single-line ui-autocomplete-input valid' data-val='true' value='' autocomplete='off' id='" + Data[i].split(" ").join("") + "1' Name='" + Data[i].split(" ").join("") + "1' onkeypress=GetAttrList(this,'" + AttribId[i] + "') /></td>";
            }
        }
        tabstr1 = tabstr1 + "<td  style='width:10em;'><input type='text' id='itemstock1'  class='form-control' onkeypress='return isNumber(event)' /></td> <td  style='width:10em;'><input type='text' id='rates1' class='form-control'  onkeypress='return isNumber(event)' /></td><td  style='width:10em;'><input type='text' id='ROL1'  class='form-control'  onkeypress='return isNumber(event)' /></td><td style='width:5em;'><i class='fa fa-plus pluxad' onclick='SetAttributeValue()'></i></td>";
        tabstr1 = tabstr1 + "</tr>"
        $("#AttributeTable1").append(tabstr1);
        var cnt = 0;

    }
    else {
        debugger;
        $("#AttributeTable1").html("");
        $("#AttributeTable2").html("");
        $("#Attribute_ID  option:selected").val("");
    }
}
function SetAttributeValue() {
    debugger;
    var comm = {};
    var savedata = {};

    itemstock = $("#itemstock1").val();
    rates = $("#rates1").val();
    ROL = $("#ROL1").val();
    var isvalid = true;
    var ErrorMessage = "";
    //validations of item master

    $("#AttributeTable1 tr.attrbtab_val2").children('td').find(".hidval").each(function () {
        var chkatrval = this.value;
        if (chkatrval == "" || chkatrval == "0") {
            ErrorMessage = "Please Select Column Attribute Value!";
            isvalid = false;
        }
    });


    if (itemstock == "") {
        ErrorMessage = "Please Enter Item Stock";
        isvalid = false;
    }
    else if (rates == "") {
        ErrorMessage = "Please Enter Rate Of Item";
        isvalid = false;
    }
    else if (ROL == "") {
        ErrorMessage = "Please Enter Re-Order Level";
        isvalid = false;
    }
    if (ErrorMessage != "") {
        swal({
            title: 'Error',
            text: ErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        isvalid = false;
    }
    else {

        var Data = $('#Attribute_ID option:selected').toArray().map(item => item.text).join();
        var index = Data.indexOf(',')
        if (index == -1) {
            Data = Data + ",";
        }
        Data = Data.split(',');
        debugger
        var count = 0;
        var temprray = "";
        var atrval = "";
        var item1 = {}
        $("#AttributeTable1 tr.attrbtab_val2").children('td').find(".hidval").each(function () {
            atrval = atrval + "," + this.value;
        });

        $("#AttributeTable2").find(".attrtabval").each(function () {
            temprray = "";
            $(this).children('td').find(".hidval2").each(function () {
                temprray = temprray + "," + this.value;
            });
            if (atrval == temprray) {
                count++;
            }
        });



        if (count == 0) {
            debugger;
            var atrid
            $("#AttributeTable1 tr.attrbtab_val2").children('td').find(".hidid").each(function () {
                atrid = atrid + "," + this.value;
            });
            atrid = atrid.split(',');
            atrid.splice(0, 1);
            atrval = atrval.split(',');
            atrval.splice(0, 1);


            $(".attrbtab1").removeAttr('style');
            $(".attrbtab2").attr('style', 'display:none;');
            var tabstr = "";
            var len1 = $("#AttributeTable2 tr").length;
            tabstr = tabstr + "<tr class='attrtabval'>";
            for (i = 0; i < Data.length; i++) {
                if (Data[i] != "" && atrval[i] != "" && (atrid[i] != "" || atrid[i] !== "undefined")) {
                    tabstr = tabstr + "<td dataval='" + atrid[i] + "' style='width:10em;'><input type='hidden' value='" + atrid[i] + "' class='hid2'  /> <input type='hidden' class='hidval2' value='" + atrval[i] + "'  />" + atrval[i] + "</td>";
                }
            }
            tabstr = tabstr + "<td style='width:10em;' dataval='" + itemstock + "'>" + itemstock + "<input type='hidden' class='itemqtycl' value='" + itemstock + "' /> </td> <td style='width:10em;' dataval='" + rates + "'>" + rates + "</td><td style='width:10em;'  dataval='" + ROL + "'>" + ROL + "</td><td style='width:5em;'><i class='fa fa-trash pluxtrash' onclick='removetr()'></i></td>";
            tabstr = tabstr + "</tr>";
            $("#AttributeTable2").append(tabstr);
            SetTotalStock(itemstock);
        }
        else {
            swal({
                title: 'Error',
                text: "This Combination All Ready Add In Rows",
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                closeOnConfirm: false,
                closeOnCancel: false

            });
        }

        $("#itemstock1").val("");
        $("#rates1").val("");
        $("#ROL1").val("");

    }
}

function GetStyleData() {
    var styleid = $('#styleid').val();
    if (styleid != 0) {
        $.ajax({
            type: "POST",
            url: '/CustomerEnquiry/GetStyleData/',
            data: "{'styleid':" + styleid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                result = JSON.parse(result);
                $("#styledesc").val(result.Styledescription == null ? "" : result.Styledescription);
                $("#styleno").val(result.StyleNo == null ? "" : result.StyleNo);

            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}
// Create By Shubham QuationCal() 18-07-2019 Start
function GetTotQty(id) {
    debugger;
    var no = id.match(/\d+/);
    no = no != null ? no[0] : "";
    var RQty = $("#IAReqQty" + no).val();
    RQty = RQty != "" ? RQty : 0;
    var SellRate = $("#IASellRate" + no).val();
    SellRate = SellRate != "" ? SellRate : 0;
    var GST = $("#GSTCharge" + no).val();
    GST = GST != "" && GST != "select" && GST != null ? GST : 0;
    var tot = parseFloat(RQty) * parseFloat(SellRate);
    var taxv = (parseFloat(tot) * parseFloat(GST)) / 100;
    $("#IATax" + no).val(taxv);
    tot = parseFloat(tot) + parseFloat(taxv);
    $("#IAtotal" + no).val(parseFloat(tot).toFixed(2));
    var TotQty = 0;
    $("#AttributeTable tr.attrtabval").children('td').find(".IAReqQty").each(function () {
        var Qty = this.value == "" ? 0 : parseFloat(this.value);
        TotQty = TotQty + parseFloat(Qty);
    });
    $("#reqqty").val(TotQty);
    QuationCal();
}

function QuationCal() {
    var totRQ = 0;
    var salP = 0;
    var subtot = 0;
    var st = 0;
    totRQ = $("#reqqty").val() == "" ? 0 : $("#reqqty").val();
    var totSell = 0;
    $("#AttributeTable tr.attrtabval").children('td').find(".IAtotal").each(function () {
        var Qty = this.value == "" ? 0 : parseFloat(this.value);
        totSell = totSell + parseFloat(Qty);
    });
    $("#total").val('' + totSell);
    $("#Htotal").val('' + totSell);
}


$("#copyBOM").on('click', function () {
    $('#copybomModal').modal('toggle');
    $("#FGItemNameId").val("");
    $("#FGItemId").val("");
    $("#FGrawIAid").val("");
    $("#copystyleid").val("");
    $("#copystyleid").attr("dataid", "");
    $("#copystyleDescid").val("");
    $("#copybomid").empty();
    $("#copyrawitemtable > tbody").html("");
});

function GetBOMList() {
    var itemid = $('#FGItemId').val();
    if (itemid != "" && parseInt(itemid) > 0) {
        $.ajax({
            type: "POST",
            url: '/BillOfMaterial/GetBOMList/',
            data: "{'itemid':" + itemid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                debugger;
                if (parseInt(result[0].Id) > 0) {
                    $("#copystyleid").val(result[0].Stylename);
                    $("#copystyleid").attr('dataId', result[0].Id);
                    $("#copystyleDescid").val(result[0].Styledescription);

                    if (result.length == 2) {
                        var startrow = 0;
                        var startrowvalue = "Select BOM";
                        $("#copybomid").empty();
                        $("#copybomid").append('<option  value="' + startrow + '">' + startrowvalue + '</option>');
                        $.each(result[1], function (i, data) {
                            $("#copybomid").append('<option value="' + data.BOM_ID + '">' + data.BOM_No + '</option>');
                        });
                    }
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}


function GetBOMMaterial() {

    var bomid = $('#copybomid').val();
    $("#copyrawitemtable > tbody").html("");
    if (bomid != 0) {
        $.ajax({
            type: "POST",
            url: '/BillOfMaterial/getBOMDetails/',
            data: "{'bomid':" + bomid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                result = JSON.parse(result);
                $.each(result[0], function (i, data) {
                    $("#copytotQtyid").val(data.Totalqty);
                });

                if (result.length == 2) {
                    var imgpath = "";
                    var r = $("#copyrawitemtable > tbody > tr").length;
                    $.each(result[1], function (i, rawitem) {
                        var selecteditemcate = rawitem.M_ItemSubCategoryMaster.Itemsubcategory;
                        r = r + 1;
                        var row = "row" + r;
                        var itemid = "itemid" + r;
                        var unitid = "unitid" + r;
                        var hdnunitid = "hdnunitid" + r;
                        var itemimg = "itemimg" + r;

                        var imgrawpath = "";
                        var rawimgpath_ = $("#rawimgpath_id").val();
                        $.each(rawitem.rawitemimg_list, function (e, dta) {
                            if (e == 0) {
                                imgpath = rawimgpath_ + "/" + dta;
                                imgrawpath = dta;
                            }
                            else {
                                imgpath = imgpath + "," + rawimgpath_ + "/" + dta;
                                imgrawpath = imgrawpath + "," + dta;
                            }
                        });

                        rawdiv = '<tr class="data-contact-person odd" id="' + row + '"><td style="padding-right:10px;"><input type="checkbox" name="selectAllId' + r + '" id="selectAllId' + r + '" class="filled-in chk-col-green selectAllId rawitemlist" dataval="0" onclick="CheckBOM(this)" /><label for="selectAllId' + r + '"></label></td>' +
                            '<td style="padding-right:10px;"><input type="hidden" value="' + rawitem.ItemSubCategoryId + '" class="form-control rawitemlist"><input type="hidden" value="' + selecteditemcate + '" class="form-control rawitemlist">' + selecteditemcate + '</td>' +
                            '<td style="padding-right:10px;"><input type="text" class="form-control rawitemlist" id="rawitemnameid' + r + '" name="rawitemnameid" value="' + rawitem.M_ItemMaster.ItemName + '" style="display:none;" readonly /><input type="hidden" id="rawitemid' + r + '" name="rawitemid" class="rawitemlist" value="' + rawitem.ItemId + '" />' +
                            '<input type="hidden" id="rawIAid' + r + '" name="rawIAid" class="rawitemlist" value="' + rawitem.Item_Attribute_ID + '" />' + rawitem.M_ItemMaster.ItemName + '</td > ' +
                            '<td style="padding-right:10px;"><input type="hidden" value="' + rawitem.SupplierId + '" class="form-control rawitemlist" /><input type="hidden" value="' + rawitem.LedgerEntity.Ledger_Name + '" class="form-control rawitemlist" />' + rawitem.LedgerEntity.Ledger_Name + '</td>' +
                            '<td style="padding-right:10px;"><input type="hidden" value="' + rawitem.PerQty + '" class="form-control rawitemlist" />' + rawitem.PerQty + '</td>' +
                            '<td style="padding-right:10px;"><input type="hidden" value="" class="form-control rawitemlist" id="itemAvailStoockId' + r + '" name="itemAvailStoockId' + r + '" /><p id="PitemAvailStoockId' + r + '"></p></td>' +
                            '<td class="col-md-1"><input type="text" class="form-control rawitemlist" id="' + unitid + '" value="' + rawitem.M_UnitMaster.ItemUnit + '" readonly  style="display:none;"><input type="hidden" class="form-control rawitemlist" id="' + hdnunitid + '" name="unitid" value="' + rawitem.UnitId + '" />' + rawitem.M_UnitMaster.ItemUnit + '</td>' +
                            '<td style="padding-right:10px;" class="rawitemrow" id="imgraw' + (i + 1) + '">';
                        if (rawitem.rawitemimg_list != null) {
                            if (rawitem.rawitemimg_list.length > 0) {
                                rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="View Image"  id="viewimage' + r + '" onclick=GetImageUpload(this.id,"preview","RawItem") />';
                                rawdiv = rawdiv + '<input type="button" class="btn btn-info" style="display:none;" id="Ruploadimage' + r + '" value="Upload Image" />';
                            }
                            else {
                                rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="View Image" style="display:none;" id="viewimage' + r + '" onclick=GetImageUpload(this.id,"preview","RawItem") />';
                                rawdiv = rawdiv + '<input type="button" class="btn btn-info" id="Ruploadimage' + r + '" value="Upload Image"/>';
                            }
                        }
                        else {
                            rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="View Image" style="display:none;" id="viewimage' + r + '" onclick=GetImageUpload(this.id,"preview","RawItem") />';
                            rawdiv = rawdiv + '<input type="button" class="btn btn-info" id="Ruploadimage' + r + '" value="Upload Image"/>';
                        }
                        rawdiv = rawdiv + '<input type="file" style="display:none;" id="rawitemimg' + r + '" name="rawitemimg" class="form-control" multiple /><input type="hidden" value="' + imgrawpath + '" class="form-control rawitemlist">';
                        rawdiv = rawdiv + '<input type="hidden" id="rawitemimg_Id' + r + '" name="rawitemimg_Id' + r + '" value="' + imgpath + '" class="form-control" /></td></tr>';

                        $('#copyrawitemtable > tbody').append(rawdiv);
                        GetAvailStock(rawitem.ItemId, rawitem.Item_Attribute_ID, r);

                    });
                }
            },
            failure: function (response) {
                alert(response.d);
            }

        });
    }

}

function GetAvailStock(itemid, IAid, no) {
    if (itemid != "") {
        $.ajax({
            type: "POST",
            url: '/BillOfMaterial/getUnit/',
            data: "{'itemid':" + itemid + ",'IAid':'" + IAid + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                result = JSON.parse(result);
                $.each(result.Table, function (i, item) {
                    $("#itemAvailStoockId" + no).val(item.AvailQty);
                    $("#PitemAvailStoockId" + no).text(item.AvailQty);
                });
                if (result.Table.length == 0) {
                    $("#itemAvailStoockId" + no).val(0);
                    $("#PitemAvailStoockId" + no).text(0);
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}



function CheckAllBOM(id) {
    var idname = $(id).attr('id');
    $("." + idname).prop('checked', $(id).prop('checked'));
    var $dataRows1 = $("#copyrawitemtable tr:not('.rowtitle')");
    var rowlenght1 = $dataRows1.length;
    if (rowlenght1 > 0) {
        $dataRows1.each(function () {
            $(this).find('input:checkbox').each(function (i) {
                if ($(this).prop("checked") == true) {
                    $(this).attr("dataval", "1");
                }
                else {
                    $(this).attr("dataval", "0");
                }
            });
        });
    }
}

function CheckBOM(id) {
    if ($(id).prop("checked") == true) {
        $(id).attr("dataval", "1");
    }
    else {
        $(id).attr("dataval", "0");
    }
}

$("#CopyMaterial").on('click', function () {
    var str = "";
    var count = 0;
    var rowcount = 0;
    var ccount = 0;
    var $dataRows1 = $("#copyrawitemtable tr:not('.rowtitle')");
    var rowlenght1 = $dataRows1.length;
    if (rowlenght1 > 0) {
        $dataRows1.each(function () {
            count = 0;
            rowcount++;
            $(this).find('.rawitemlist').each(function (i) {
                debugger;
                var contactlist = i == 0 ? ($(this).attr("dataval")) : ($(this).val());
                count++;
                if (i == 7) {
                    if (contactlist == "") {
                        contactlist = "--";
                    }
                }
                if (contactlist != undefined && contactlist != "") {
                    if (i == 7 && contactlist == "--") {
                        contactlist = " ";
                    }

                    if (count == 13) {
                        str += contactlist;
                    }
                    else {
                        str += contactlist + "~";
                    }
                }
                else {
                    setswal("Please Fill Raw Item Details");
                    isvalid = false;
                }

            });

            if (rowcount == rowlenght1) {
                rowlenght1 = rowlenght1;
            }
            else {
                str += "#";
            }
        });

        SetCopyBOm(str);
    }
});

function SetCopyBOm(str) {
    debugger;
    if (str != "") {
        var $dataRows1 = $("#rawitemtable tr:not('.rowtitle')");
        var imgpath = "";
        var len1 = $dataRows1.length;
        $.each(str.split('#'), function (i, data) {
            debugger;

            if (data != "") {
                var rawdata = data.split('~');

                if (parseInt(rawdata[0]) > 0) {
                    len1 = len1 + i;
                    var imgrawpath = "";
                    var rawimgpath_ = $("#rawimgpath_id").val();

                    $.each(rawdata[12].split(','), function (e, dta) {
                        if (e == 0) {
                            imgpath = rawimgpath_ + "/" + dta;
                        }
                        else {
                            imgpath = imgpath + "," + rawimgpath_ + "/" + dta;
                        }
                    });

                    var TotReqQty = $("#reqqty").val();
                    var TotitemQty = parseFloat(rawdata[8]) * parseFloat(TotReqQty);
                    var imgpath_data = "";
                    rawdiv = '<tr class="data-contact-person odd" id="' + len1 + '" value="' + len1 + '" data="' + len1 + '" >' +
                        '<td style="padding-right:10px;" class="rawsizerow validitemsubrow"><input type="hidden" value="' + rawdata[1] + '" id="hiddenitemcategoryid' + len1 + '" class="form-control rawitemlist"><p class="">' + rawdata[2] + '</p></td>' +
                        '<td style="padding-right:10px;" class="rawsizerow"><input type="text" class="form-control text-box single-line ui-autocomplete-input valid" data-val="true" autocomplete="off" id="rawitemnameid' + len1 + '" name="rawitemnameid' + len1 + '"  onkeydown=Getrawitemlist("' + len1 + '")  onchange=setItemUnit("' + len1 + '")   value="' + rawdata[3] + '"/>' +
                        '<input type="hidden" id="rawitemid' + len1 + '" name="rawitemid' + len1 + '" class="rawitemlist" value="' + rawdata[4] + '" /><input type="hidden" id="rawIAid' + len1 + '" name="rawIAid' + len1 + '" class="rawitemlist" value="' + rawdata[5] + '" /></td>' +
                        '<td style="padding-right:10px;" class="rawsizerow"><input type="hidden" value="' + rawdata[7] + '" class="form-control rawitemlist"><p class="">' + rawdata[7] + '</p></td>' +
                        '<td style="padding-right:10px;" class="rawsizerow"><input type="text" style="width:100px;" id="Perqty_' + len1 + '" class="form-control rawitemlist" name="Perqty_' + len1 + '" value="' + rawdata[8] + '" onkeypress="return isNumber(event)" onkeyup=GetTotReqQty("' + len1 + '")></td>' +
                        '<td style="padding-right:10px;" class="rawsizerow"><input type="text" id="sizecount' + len1 + '" value="0" name="sizecount" style="display:none" /><input type="text" id="Acceptedqty' + len1 + '" class="form-control rawitemlist" name="Acceptedqty" value="' + parseFloat(TotitemQty).toFixed(2)+'" readonly></td>' +
                        '<td style="padding-right:10px;" class="rawsizerow"><input type="text" id="AvailInStock_' + len1 + '" class="form-control rawitemlist" name="AvailInStock" value="' + rawdata[9] + '" readonly></td>' +
                        '<td style="padding-right:10px;" class="rawsizerow"><input type="hidden" id="hiddenunitid" value="' + rawdata[11] + '" class="form-control rawitemlist"><input type="hidden" value="0" class="rawitemlist" />' + rawdata[10] + '</td>' +
                        '<td style="padding-right:10px;" class="rawitemrow" id="imgraw' + (i + 1) + '">';
                    if (rawdata[12] != "" && rawdata[12] != null) {
                        var imgstr = rawdata[12].split(',');
                        imgpath_data = rawdata[12];
                        if (imgstr.length > 0) {
                            rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="View Image"  id="viewimage' + len1 + '" onclick=GetImageUpload(this.id,"preview","RawItem") />';
                            rawdiv = rawdiv + '<input type="button" class="btn btn-info" style="display:none;" id="Ruploadimage' + len1 + '" value="Upload Image" onclick=GetImageUpload(this.id,"upload","RawItem") />';
                        }
                        else {
                            rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="View Image" style="display:none;" id="viewimage' + len1 + '" onclick=GetImageUpload(this.id,"preview","RawItem") />';
                            rawdiv = rawdiv + '<input type="button" class="btn btn-info" id="Ruploadimage' + len1 + '" value="Upload Image" onclick=GetImageUpload(this.id,"upload","RawItem") />';
                        }
                    }
                    else {
                        imgpath_data = "-";
                        rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="View Image" style="display:none;" id="viewimage' + len1 + '" onclick=GetImageUpload(this.id,"preview","RawItem") />';
                        rawdiv = rawdiv + '<input type="button" class="btn btn-info" id="Ruploadimage' + len1 + '" value="Upload Image" onclick=GetImageUpload(this.id,"upload","RawItem") />';
                    }
                    rawdiv = rawdiv + '<input type="file" style="display:none;" id="rawitemimg' + len1 + '" name="rawitemimg" class="form-control" multiple />';
                    rawdiv = rawdiv + '<input type="hidden" value="0" class="form-control rawitemlist" id="rawitemcount' + len1 + '"><input type="hidden" value="' + imgpath_data + '" class="form-control rawitemlist">';
                    rawdiv = rawdiv + '<input type="hidden" id="rawitemimg_Id' + len1 + '" name="rawitemimg_Id' + len1 + '" value="' + imgpath + '" class="form-control" /></td>';
                    rawdiv = rawdiv + '<td><i class="fa fa-trash pluxtrash" onclick="removetr()"></i></td>' +
                        '</tr>';

                    $('#rawitemtable').append(rawdiv);
                }

                $('#copybomModal').modal('hide');
            }
        });
    }
}

