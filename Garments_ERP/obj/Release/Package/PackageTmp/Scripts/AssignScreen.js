﻿$(document).ready(function () {
    $('.select2').select2();
    $('.select2').width("100%");

    $.ajax({
        type: "POST",
        url: "/AssignScreen/RoleList/",
        data: "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (subcates) {
            $("#Aroleid").empty();
            $("#Aroleid").append('<option values="0" text="Select">Select</option>');
            $.each(subcates, function (i, data) {
                $("#Aroleid").append('<option value="' + data.roleId + '">' + data.roleName + '</option>');
            });
        },
        failure: function (response) {
            alert(response.d);
        }
    });
    GetScreenData();
});

function GetScreenData() {
    debugger;
    var Roleid = $("#Aroleid").val();
    Roleid = Roleid == "" || Roleid == null   ? 0 : Roleid;
    $.ajax({
        type: "POST",
        url: "/AssignScreen/GetScreenData/",
        data: "{'RoleId':" + Roleid + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (subcates) {
            $("#ScreenDataId").html("");
            $.each(subcates, function (i, subcate) {
                var scrName = subcate.ScreenName;
                var sub = "";
                var str = "";
                str = str + '<div class="col-md-3 col-sm-6 col-xs-6"><input type="hidden" id="screenvalid' + i + '" name="screenvalid' + i + '" dataSub="" /><div class="SRmain"><div class="SRmainHead">';
                str = str + '<div class="icheck-primary d-inline"><input type="checkbox" name="MainScreen' + i + '" id="MainScreen' + i + '" dataMain="' + subcate.screenId + '"  onclick="CheckAll(this)" /><label for="MainScreen' + i + '">' + scrName + '</label></div></div>';
                str = str + '<div class="SRmainbody"><ul id="Chkulid' + i + '">';
                $.each(subcate.ScreenList, function (j, data) {
                    debugger;
                    var SubscrName = data.ScreenName;
                    var status = data.CHKsatus == true ? "checked" : "";
                    sub = data.screenId + "," + sub;
                    var onclik = data.ScreenList.length > 0 ? 'onclick="CheckAllSub(this)"' : 'onclick="IsCheck(this)"';
                    str = str + '<li><div class="icheck-primary d-inline"><input type="checkbox" name="SubScreen' + i + '_' + j + '" datavalue="' + data.screenId + '" datasub="' + data.IsSub + '" id="SubScreen' + i + '_' + j + '" class="MainScreen' + i + '"  dataMain="' + subcate.screenId + '"  ' + onclik + '    ' + status + '  /><label for="SubScreen' + i + '_' + j + '">' + SubscrName + '</label></div>';

                    if (data.ScreenList.length > 0) {
                        str = str + '<ul class="padmd2" id="Chkulid' + i + '_' + j + '">';
                        $.each(data.ScreenList, function (k, subdata) {
                            var SubscrName2 = subdata.ScreenName;
                            var status = subdata.CHKsatus == true ? "checked" : "";
                            sub = subdata.screenId + "," + sub;
                            str = str + '<li><div class="icheck-primary d-inline"><input type="checkbox" name="SubScreen' + i + '_' + j + '_' + k + '" datavalue="' + subdata.screenId + '" datasub="' + subdata.IsSub + '"   id="SubScreen' + i + '_' + j + '_' + k + '" class="SubScreen' + i + '_' + j + '" onclick="IsCheckSub(this)" ' + status + '  /><label for="SubScreen' + i + '_' + j + '_' + k + '">' + SubscrName2 + '</label></div></li>';

                        });
                        str = str + '</li></ul>';
                    }
                    else {
                        str = str + '</li>';
                    }

                });
                str = str + '</ul></div></div></div>';
                $("#ScreenDataId").append(str);
                SetChekValues(i);
            });
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function SetChekValues(no) {
    debugger;
    var lilen = $('#Chkulid' + no + ' li > div > input[type=checkbox]').length;
    if (parseInt(lilen) > 0) {
        checkboxes = $('#Chkulid' + no + ' li > div > input[type=checkbox]');
        var count = 0;
        var str = "";
        checkboxes.each(function (e) {
            debugger;
            if ($(this).prop("checked") == true) {
                var val = $(this).attr('datavalue');
                var subid = $(this).attr('id');
                var subpareUl = $(this).parent('div').parent('li').children('ul').attr('id');
                if (subpareUl != undefined) {
                    //$("." + subid).prop('checked', true);
                    checkboxes1 = $('#' + subpareUl + ' li input[type=checkbox]');
                    var str2 = "";
                    checkboxes1.each(function (e) {
                        if ($(this).prop("checked") == true) {
                            var val1 = $(this).attr('datavalue');
                            str2 = val1 + "~" + str2;
                        }
                    });
                    val = val + '^' + str2;

                }
                var issub = $(this).attr('datasub');
                if (parseInt(issub) > 1) {
                    str = str;
                }
                else {
                    str = val + "," + str;
                }

                count = count + 1;
            } else {
                count = 0;
            }
        });
    }

    if (parseInt(lilen) == 0) {
        $('#MainScreen' + no).prop('checked', false);
    }
    else {
        if (parseInt(count) == parseInt(lilen)) {
            $('#MainScreen' + no).prop('checked', true);
        }
        else {

            $('#MainScreen' + no).prop('checked', false);
        }
    }
    var scrid = $("#MainScreen" + no).attr('dataMain');
    $('#screenvalid' + no).attr('dataSub', str);
    if (str == "") {
        $('#screenvalid' + no).val("");
    }
    else {
        $('#screenvalid' + no).val(scrid);
    }
}


function CheckAll(id) {

    var idname = $(id).attr('id');
    var scrid = $(id).attr('dataMain');
    $("." + idname).prop('checked', $(id).prop('checked'));
    var hidid = $(id).parent('div').parent('div').parent('div').parent('div').find('input:hidden').attr('id');

    if ($(id).prop("checked") == true) {
        var ulid = $("." + idname).parent('div').parent('li').parent('ul').attr('id');
        var str = "";
        if (ulid != undefined) {
            checkboxes = $('#' + ulid + ' li input[type=checkbox]');
            checkboxes.each(function (e) {

                if ($(this).prop("checked") == true) {
                    var val = $(this).attr('datavalue');
                    var subid = $(this).attr('id');
                    var subpareUl = $(this).parent('div').parent('li').children('ul').attr('id');;
                    if (subpareUl != undefined) {
                        $("." + subid).prop('checked', $(id).prop('checked'));
                        checkboxes1 = $('#' + subpareUl + ' li input[type=checkbox]');
                        var str2 = "";
                        checkboxes1.each(function (e) {
                            if ($(this).prop("checked") == true) {
                                var val1 = $(this).attr('datavalue');
                                str2 = val1 + "~" + str2;
                            }
                        });
                        val = val + '^' + str2;

                    }
                    var issub = $(this).attr('datasub');
                    if (parseInt(issub) > 1) {
                        str = str;
                    }
                    else {
                        str = val + "," + str;
                    }


                }
            });

            $("#" + hidid).attr('dataSub', str);
            $("#" + hidid).val(scrid);
        }
        else {
            $("#" + hidid).attr('dataSub', "0");
            $("#" + hidid).val(scrid);
        }
    }
    else {
        var ulid = $("." + idname).parent('div').parent('li').parent('ul').attr('id');
        var str = "";
        if (ulid != undefined) {
            checkboxes = $('#' + ulid + ' li input[type=checkbox]');
            checkboxes.each(function (e) {

                if ($(this).prop("checked") == false) {
                    var subid = $(this).attr('id');
                    var subpareUl = $(this).parent('div').parent('li').children('ul').attr('id');;
                    if (subpareUl != undefined) {
                        $("." + subid).prop('checked', $(id).prop('checked'));

                    }
                }
            });

            $("#" + hidid).attr('dataSub', str);
            $("#" + hidid).val(scrid);
        }
        else {
            $("#" + hidid).attr('dataSub', "0");
            $("#" + hidid).val(scrid);
        }

        $("#" + hidid).attr('dataSub', '');
        $("#" + hidid).val("");
    }
}

function IsCheck(Id) {

    var ulid = $(Id).parent('div').parent('li').parent('ul').attr('id');
    var lilen = $('#' + ulid + ' li input[type=checkbox]').length;
    var count = 0;
    checkboxes = $('#' + ulid + ' li input[type=checkbox]');
    var str = "";
    checkboxes.each(function (e) {

        if ($(this).prop("checked") == true) {
            var val = $(this).attr('datavalue');
            var subid = $(this).attr('id');
            var subpareUl = $(this).parent('div').parent('li').children('ul').attr('id');;
            if (subpareUl != undefined) {
                $("." + subid).prop('checked', true);
                checkboxes1 = $('#' + subpareUl + ' li input[type=checkbox]');
                var str2 = "";
                checkboxes1.each(function (e) {
                    if ($(this).prop("checked") == true) {
                        var val1 = $(this).attr('datavalue');
                        str2 = val1 + "~" + str2;
                    }
                });
                val = val + '^' + str2;

            }
            var issub = $(this).attr('datasub');
            if (parseInt(issub) > 1) {
                str = str;
            }
            else {
                str = val + "," + str;
            }
            count = count + 1;

        } else {
            count = 0;
        }
    });
    var mainchked = $("#" + ulid).parent('div').prev().find('input:checkbox').attr('id');
    var hidid = $("#" + mainchked).parent('div').parent('div').parent('div').parent('div').find('input:hidden').attr('id');
    var scrid = $("#" + mainchked).attr('dataMain');

    $('#' + hidid).attr('dataSub', str);
    if (str == "") {
        $('#' + hidid).val("");
    }
    else {
        $('#' + hidid).val(scrid);
    }

    if (parseInt(count) == parseInt(lilen)) {
        $('#' + mainchked).prop('checked', true);
    }
    else {

        $('#' + mainchked).prop('checked', false);
    }

}

function IsCheckSub(Id) {

    var ulid = $(Id).parent('div').parent('li').parent('ul').attr('id');
    var lilen = $('#' + ulid + ' li input[type=checkbox]').length;
    var count = 0;
    checkboxes = $('#' + ulid + ' li input[type=checkbox]');
    var str = "";
    checkboxes.each(function (e) {

        if ($(this).prop("checked") == true) {
            count = count + 1;
        }
    });

    var subchked = $("#" + ulid).parent('li').children('div').find('input:checkbox').attr('id');
    if (count > 0) {
        $('#' + subchked).prop('checked', true);
    }
    else {
        $('#' + subchked).prop('checked', false);

    }

    var maincount = 0;
    var ulid = $("#" + subchked).parent('div').parent('li').parent('ul').attr('id');
    var lilen = $('#' + ulid + ' li input[type=checkbox]').length;
    checkboxes = $('#' + ulid + ' li input[type=checkbox]');
    var str = "";
    checkboxes.each(function (e) {

        if ($(this).prop("checked") == true) {
            maincount = maincount + 1;
            var val = $(this).attr('datavalue');
            var subid = $(this).attr('id');
            var subpareUl = $(this).parent('div').parent('li').children('ul').attr('id');;
            if (subpareUl != undefined) {
                // $("." + subid).prop('checked', true);
                checkboxes1 = $('#' + subpareUl + ' li input[type=checkbox]');
                var str2 = "";
                checkboxes1.each(function (e) {

                    if ($(this).prop("checked") == true) {
                        var val1 = $(this).attr('datavalue');
                        str2 = val1 + "~" + str2;
                    }
                });
                val = val + '^' + str2;

            }
            var issub = $(this).attr('datasub');
            if (parseInt(issub) > 1) {
                str = str;
            }
            else {
                str = val + "," + str;
            }
        }
    });

    var mainchked = $("#" + ulid).parent('div').prev().find('input:checkbox').attr('id');
    var hidid = $("#" + mainchked).parent('div').parent('div').parent('div').parent('div').find('input:hidden').attr('id');
    var scrid = $("#" + mainchked).attr('dataMain');

    var Machklen = $('#' + ulid + ' li input[type=checkbox]').length;

    if (maincount == Machklen) {
        $('#' + mainchked).prop('checked', true);
    }
    else {
        $('#' + mainchked).prop('checked', false);

    }

    $('#' + hidid).attr('dataSub', str);
    if (str == "") {
        $('#' + hidid).val("");
    }
    else {
        $('#' + hidid).val(scrid);
    }
}

function CheckAllSub(id) {

    var idname = $(id).attr('id');
    var scrid = $(id).attr('dataMain');
    $("." + idname).prop('checked', $(id).prop('checked'));
    var hidid = $(id).parent('div').parent('li').parent('ul').parent('div').parent('div').parent('div').find('input:hidden').attr('id');

    var ulid = $(id).parent('div').parent('li').parent('ul').attr('id');
    var lilen = $('#' + ulid + ' li input[type=checkbox]').length;
    var count = 0;
    var str = "";
    if (ulid != undefined) {
        checkboxes = $('#' + ulid + ' li input[type=checkbox]');
        checkboxes.each(function (e) {

            if ($(this).prop("checked") == true) {
                var val = $(this).attr('datavalue');
                var subid = $(this).attr('id');
                var subpareUl = $(this).parent('div').parent('li').children('ul').attr('id');;
                if (subpareUl != undefined) {
                    $("." + subid).prop('checked', $(id).prop('checked'));
                    checkboxes1 = $('#' + subpareUl + ' li input[type=checkbox]');
                    var str2 = "";
                    checkboxes1.each(function (e) {
                        if ($(this).prop("checked") == true) {
                            var val1 = $(this).attr('datavalue');
                            str2 = val1 + "~" + str2;
                        }
                    });
                    val = val + '^' + str2;

                }
                var issub = $(this).attr('datasub');
                if (parseInt(issub) > 1) {
                    str = str;
                }
                else {
                    str = val + "," + str;
                }
                count = count + 1;

            }
            else {
                count = 0;
            }
        });

        var mainchked = $("#" + ulid).parent('div').prev().find('input:checkbox').attr('id');
        var hidid = $("#" + mainchked).parent('div').parent('div').parent('div').parent('div').find('input:hidden').attr('id');
        var scrid = $("#" + mainchked).attr('dataMain');

        $('#' + hidid).attr('dataSub', str);
        if (str == "") {
            $('#' + hidid).val("");
        }
        else {
            $('#' + hidid).val(scrid);
        }

        if (parseInt(count) == parseInt(lilen)) {
            $('#' + mainchked).prop('checked', true);
        }
        else {

            $('#' + mainchked).prop('checked', false);
        }
        $("#" + hidid).attr('dataSub', str);
        $("#" + hidid).val(scrid);
    }
    else {
        $("#" + hidid).attr('dataSub', "0");
        $("#" + hidid).val(scrid);
    }
}

$('#SaveAssingScreen').on("click", function () {
    var isvalid = true;
    var Aroleid = $('#Aroleid').val();
    HiddenData = $('#ScreenDataId > div > input[type=hidden]');
    var count = 0;
    var str = "";
    HiddenData.each(function (e) {
        var main = $(this).val();
        var valSub = $(this).attr('dataSub');
        if (main != "") {
            str = str + main + '~' + valSub + "#";
            count = 1;
        }
    });

    $("#AssignScreendataId").val(str);
    if (Aroleid == "" || Aroleid =="Select Role") {
        ErrorMsg("Please Select Role!");
        isvalid = false;
    }else if (count==0) {
        ErrorMsg("Please Select Screens!");
        isvalid = false;
    }

    return isvalid;
});

function ErrorMsg(msg) {
    swal({
        title: "Error",
        text: msg,
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false,
        closeOnCancel: false

    });
}
    