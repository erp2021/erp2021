﻿//function to set GST No
function setGSTno() {
    var panno = $('#PANNo').val();
    var statecode = $('#statecodehidden').val();
    if (panno != "") {               
        var gstno = statecode + panno;
        $('#GSTNo').val(gstno);
    }
    else {     
        $('#GSTNo').val(statecode);
    }

}

$('#saveBroker').on("click", function () {
    var isvalid = true;
    var BrokerName = $('#BrokerName').val();

    if (BrokerName == "") {
        MSGshow("Error", "Enter Broker Name", "error");
        isvalid = false;
    }
    var Phone = $('#Phone').val();

    if (Phone == "") {
        MSGshow("Error", "Enter Phone No.", "error");
        isvalid = false;
    }
    
    return isvalid;
});

function MSGshow(_title, alrtmsg, _type) {

    swal({
        title: _title,
        text: alrtmsg,
        type: _type,
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false,
        closeOnCancel: false

    });
}

