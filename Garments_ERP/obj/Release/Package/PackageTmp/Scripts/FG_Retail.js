﻿
var fg_tr_id = "";
$(document).ready(function () {
    $('.select2').select2();
    $('.select2').width("100%");
    var urlVars = getUrlVars();
    fg_tr_id = urlVars.fg_tr_id;
    if (fg_tr_id != "" || fg_tr_id != undefined) {
        retriveDetails();
    }
});
// code to autocomplete work orders and append fields which are dependent on the work orders
//$("#WorkOrderNo").autocomplete({
//    source: function (request, response) {
//        var itemArray = [];
//        //var inputs = request.term;
//        var inp = $("#WorkOrderNo").val();
//        var CustID = $("#customerid").val();
//        $.ajax({
//            url: '/FG_Retail/Get_WorkOrders/',
//            data: "{'userInp':'" + inp + "','CustID':'" + CustID + "'}",
//            dataType: "json",
//            type: "POST",
//            contentType: "application/json; charset=utf-8",
//            success: function (data) {
//                $(".ui-helper-hidden-accessible").attr("style", "display:none;");

//                response($.map(data, function (item) {
//                    var itemDetails_ = {
//                        'WorkOrderID': item.WorkOrderID,
//                        'RequiredQty': item.RequiredQty,
//                        'Itemid': item.Itemid,
//                        'ItemName': item.ItemName,
//                        'WorkOrderNo': item.WorkOrderNo
//                    };
//                    itemArray.push(itemDetails_);
//                    //return { label: item.NameContact, value: item.NameContact, dataid=item.Customer_ID };
//                    return { label: item.WorkOrderNo, value: item.WorkOrderNo, dataid: item.WorkOrderID, data: itemArray };

//                }));

//            },
//            error: function (response) {
//                swal("warning", "Something went wrong..", "warning");
//            },
//            failure: function (response) {
//                alert(response.responseText);
//            }
//        });
//    },
//    select: function (e, i) {
//        var _items = i.item.data.filter(function (value, index, array) {
//            return value.WorkOrderID === i.item.dataid;
//        });
//        $.each(_items, function (key, rval) {
//            $('#ItemID').val(rval.Itemid);
//            $('#Work_Order_Total_Qty').val(rval.RequiredQty);
//            $('#ItemName').val(rval.ItemName);
//            $('#WorkOrderID').val(rval.WorkOrderID);
//        });
//        $("#WorkOrderID").val(i.item.dataid);
//    },
//    minLength: 1
//});

// code to autocomplete Finish Good transfer no's and append fields which are dependent on it
//$("#TransferNo").autocomplete({
//    source: function (request, response) {
//        var itemArray = [];
//        //var inputs = request.term;
//        var inp = $("#TransferNo").val();
//        var woID = $("#WorkOrderID").val();
//        $.ajax({
//            url: '/FG_Retail/Get_FGTransfers/',
//            data: "{'userInp':'" + inp + "','woID':'" + woID + "'}",
//            dataType: "json",
//            type: "POST",
//            contentType: "application/json; charset=utf-8",
//            success: function (data) {
//                $(".ui-helper-hidden-accessible").attr("style", "display:none;");

//                response($.map(data, function (item) {
//                    var itemDetails_ = {
//                        'Id': item.Id,
//                        'TransferNo': item.TransferNo,
//                        'WarehouseId': item.WarehouseId,
//                        'WarehouseName': item.WarehouseName,
//                        'RackId': item.RackId,
//                        'RackName': item.RackName
//                    };
//                    itemArray.push(itemDetails_);
//                    //return { label: item.NameContact, value: item.NameContact, dataid=item.Customer_ID };
//                    return { label: item.TransferNo, value: item.TransferNo, dataid: item.Id, data: itemArray };

//                }));

//            },
//            error: function (response) {
//                swal("warning", "Something went wrong..", "warning");
//            },
//            failure: function (response) {
//                alert(response.responseText);
//            }
//        });
//    },
//    select: function (e, i) {
//        var _items = i.item.data.filter(function (value, index, array) {
//            return value.Id === i.item.dataid;
//        });
//        $.each(_items, function (key, rval) {
//            $('#WarehouseName').val(rval.WarehouseName);
//            $('#WarehouseId').val(rval.WarehouseId);
//            $('#RackName').val(rval.RackName);
//            $('#RackId').val(rval.RackId);
//        });
//        $("#FG_Transfer_ID").val(i.item.dataid);
//        createTable();
//    },
//    minLength: 1
//});

// function to Append FG detail table
function createTable() {
    var fg_transferid = $("#FG_Transfer_ID").val();
    if (fg_transferid > 0) {
        $("#itemQtyDetails").empty();
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '/FG_Retail/Get_FGTransferDetails/',
            data: "{'fg_transferid':'" + fg_transferid + "'}",
            contentType: "application/json;",
            success: function (Resultset) {

                var header = $("<thead></thead>");
                var dyAttributeHead = $("<tr></tr>");
                var attribute = [];

                $.each(Resultset, function (i, result) {
                    $("#AttributeId").val(result.Attribute_ID);
                    $.each(result.Attribute_Name.split(","), function (b, edata) {
                        attribute.push(edata);
                    });
                });
                var uniqueAttr = attribute.filter(function (itm, i, a) {
                    return i == attribute.indexOf(itm);
                });
                //alert(attribute);
                $.each(uniqueAttr, function (b, edata) {
                    var tf_ = $("<th style='width:10em;'>" + edata + "</th>");
                    dyAttributeHead.append(tf_);
                });
                var staticTH = "<th style='width: 10em;'>Work Order Qty</th><th style='width: 10em;'>FG Tranferred Quantity</th><th style='width: 10em;'>Tranfer To Retail Qty</th><th style='width: 10em;'>Balance Quantity</th>";
                dyAttributeHead.append(staticTH);
                header.append(dyAttributeHead);
                $("#itemQtyDetails").append(header);

                var tabstr = "";
                $.each(Resultset, function (i, result) {
                    var attributename = result.Attribute_Value;
                    var index1 = attributename.indexOf(',');
                    if (index1 == -1) {
                        attributename = attributename + ",";
                    }
                    attributename = attributename.split(',');
                    var pr = 0;
                    tabstr = tabstr + "<tr class='attrtabval'><td style='display:none;'><input type='hidden' name='Id' class='EnqIdcs' value='" + result.Id + "' /><input type='hidden' name='FG_TR_Detail_ID' id='FG_TR_Detail_ID" + i + "' value='0' /><input type='hidden' name='Attribute_Value_ID' id='Attribute_Value_ID" + i + "' value='" + result.Attribute_Value_ID + "' /> </td>";
                    $.each(result.Attribute_Value_ID.split(","), function (k, e) {

                        if (e != "") {
                            tabstr = tabstr + "<td style='width:10em;'><input type='hidden' value='" + e + "' class='hid2'  />"
                            if (attributename[pr] != "") {
                                tabstr = tabstr + "<input type='hidden' class='hidval2' value='" + attributename[pr] + "' /> " + attributename[pr] + "";
                            }
                            tabstr = tabstr + "</td>";
                            pr = pr + 1;
                        }
                    });
                    tabstr = tabstr + "<td style='width:10em;text-align:right;'><input type='text' id='WorkOrderQty" + i + "' name='WorkOrderQty' class='form-control' value='" + result.WorkOrderQty + "' readonly /></td>";
                    tabstr = tabstr + "<td style='width:10em;text-align:right;'><input readonly type='text' name='TransferredQty' value='" + result.ProducedQty + "' id='TransferredQty" + i + "' class='form-control' readonly /></td>";
                    tabstr = tabstr + "<td style='width:10em;text-align:right;'><input type='text' name='TransferToRetailQty' value='0' id='TransferToRetailQty" + i + "' onchange='calBalanceQty(" + i + ")' onkeypress='return isNumberKey(event, this.id)' class='form-control'/></td>";
                    tabstr = tabstr + "<td style='width:10em;'><input type='text' name='BalanceQty' id='BalanceQty" + i + "' class='form-control' value='0'  readonly /></td>";
                    //tabstr = tabstr + "<td style='width:10em;'><input type='text' class='form-control IAtotProd' id='IAtotProd" + i + "' value='0'  readonly /></td>";
                    tabstr = tabstr + "</tr>"
                });
                tabstr = "<tbody>" + tabstr + "</tbody>";
                $("#itemQtyDetails").append(tabstr);
            },
            failure: function (response) {
                alert(response.d);
            }
        });

    }
}

//validate data
function ValidateData() {
    var msg = "Mandetory Information : \n";
    var blnFlag = true;

    var $rows_ = $('#itemQtyDetails').find('tbody tr');

    if ($("#customerid").val() == "") {
        msg = msg + "* Please select customer from list. \n";
        blnFlag = false;
    }
    //if ($("#WorkOrderID").val() == "") {
    //    msg = msg + "* Please select work order no from list.\n";
    //    blnFlag = false;
    //}
    //if ($('#FG_Transfer_ID').val() == "") {
    //    msg = msg + "* Please select FG Transfer No from list.\n";
    //    blnFlag = false;
    //}
    //if ($rows_.length == 0) {
    //    msg = msg + "* No more than one row in Product Qty details table. \n";
    //    blnFlag = false;
    //}

    if (msg != "" && blnFlag == false) {
        swal("warning", msg, "warning");
        return false;
    }
    return true;
}

//code to save sales
$("#btnSave").click(function () {

    if (ValidateData() == true) {
        var isPageValidate = true;

        var branch_id = $("#Branch_ID").val();
        var user_ID = $("#CreatedBy").val();
        var companyid = $("#Company_ID").val();
        var FG_TR_ID = 0; //$("#FG_TR_ID").val();
        if (FG_TR_ID == undefined) {
            FG_TR_ID = 0;
        }
        var customerid = $("#customerid").val();
        var WorkOrderID = $("#WorkOrderID").val();
        var ItemID = $("#ItemId").val();
        var FG_Transfer_ID =$("#FG_Transfer_ID").val();
        var WarehouseId =$("#WarehouseId").val();
        var RackId = $("#RackId").val();
        var POid = $("#porefno").val();
        var GRNID = $("#GRNno").val();
        var Item_Attribute_Id = $("#ItemAttributeId").val();
        var HSNCode = $("#HSNCodeid").val();
        var Unit_Id = $("#unitid").val();
        var PO_Qty = $("#POQty").val();


        var FG_TR_Details = [];
        var $rows = $('#itemQtyDetails').find('tbody tr');
        if ($rows.length > 0) {
            var len = 0;
            $.each($rows, function (key, row) {
                len = len + 1;
                //AttributeId, Attribute_Value_ID, WorkOrderQty, TransferredQty, TransferToRetailQty, BalanceQty
                var fg_dt_id = $('#FG_TR_Detail_ID' + len).val();
                var transfertoretailqty = $('#IssueQty' + len).val();
                var balanceqty = $('#BalQty' + len).val();
                var Inwardid = $('#InwardId' + len).val();
                var InwardQty = $('#BTItemQty' + len).val();
                
                if (Inwardid != "" && transfertoretailqty != "") {
                    var itemDetails_ = {
                        'FG_TR_Detail_ID': fg_dt_id,
                        'TransferToRetail_Qty': transfertoretailqty,
                        'Balance_Qty': balanceqty,
                        'InwardId': Inwardid,
                        'InwardQty': InwardQty
                    };
                    FG_TR_Details.push(itemDetails_);
                }
            });

        }
        var FG_RetailData = {
            'FG_TR_ID': FG_TR_ID,
            'Customer_ID': customerid,
            'WorkOrderID': WorkOrderID,
            'Item_ID': ItemID,
            'FG_Transfer_ID': FG_Transfer_ID,
            'WarehouseId': WarehouseId,
            'RackId': RackId,
            'Branch_ID': branch_id,
            'Company_ID': companyid,
            'CreatedBy': user_ID,
            'POid': POid,
            'GRNID': GRNID,
            'Item_Attribute_Id': Item_Attribute_Id,
            'HSNCode': HSNCode,
            'Unit_Id': Unit_Id,
            'PO_Qty': PO_Qty,
            'FG_Transfer_To_Retail_Details': FG_TR_Details
        };

        if (isPageValidate === true) {

            var test = JSON.stringify({ FG_TREntity: FG_RetailData });
            $.ajax({
                url: '/FG_Retail/SaveFG_Retail/',
                //data: JSON.stringify(inp),
                data: test,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (output) {
                    if (output > 0) {

                        //swal("success", "Voucher saved successfully.", "success");

                        swal({
                            title: 'success',
                            text: 'Material Transferred for retail successfully.',
                            showConfirmButton: true,
                            showCancelButton: false,
                            closeOnConfirm: true,
                            closeOnCancel: true,
                            confirmButtonText: 'OK',
                            confirmButtonColor: '#8CD4F5',
                            cancelButtonText: 'Cancel'
                        },
                            function (isConfirm) {

                                if (isConfirm) {

                                    var url = '/FG_Retail/Index/';
                                    window.location.href = url;

                                } else {
                                    swal("Cancelled", ")", "error");
                                }
                            });
                    }
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    }
});

//accept number including dot only.
function isNumberKey(evt, id) {
    try {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) {
            var txt = document.getElementById(id).value;
            if (!(txt.indexOf(".") > -1)) {

                return true;
            }
        }
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    } catch (w) {
        alert(w);
    }
}

function calBalanceQty(id) {
    //var number = id.match(/\d+/);
    $("#BalanceQty" + id).val('');
    var balanceqty = 0;
    var tfQty = $("#TransferredQty" + id).val();
    var tf_rQty = $("#TransferToRetailQty" + id).val();
    if (tfQty > 0) {
        if (tf_rQty > 0) {
            if (tf_rQty <= tfQty) {
                balanceqty = parseFloat(tfQty) - parseFloat(tf_rQty);
            }
            else {
                $("#TransferToRetailQty" + id).val('');
                swal("warning", "Quantity should not be greater than Transferred Qty.", "warning")
            }
        }
    }
    $("#BalanceQty" + id).val(balanceqty);
}

function retriveDetails() {

    if (fg_tr_id > 0) {
        $("#itemQtyDetails").empty();
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '/FG_Retail/Get_FG_TRDetails/',
            data: "{'fg_trid':'" + fg_tr_id + "'}",
            contentType: "application/json;",
            success: function (entityset) {

                $("#FG_TR_ID").val(entityset.FG_TR_ID);
                $("#custnameid").val(entityset.Customer_Name);
                $("#customerid").val(entityset.Customer_ID);
                $("#WorkOrderID").val(entityset.WorkOrderID);
                $("#WorkOrderNo").val(entityset.MFG_WORK_ORDER.WorkOrderNo);
                $("#ItemID").val(entityset.Item_ID);
                $("#ItemName").val(entityset.M_ItemMaster.ItemName);
                $("#Work_Order_Total_Qty").val(entityset.Work_Order_Total_Qty);
                $("#FG_Transfer_ID").val(entityset.FG_Transfer_ID);
                $("#TransferNo").val(entityset.FG_TransferMaster.TransferNo);
                $("#WarehouseId").val(entityset.WarehouseId);
                $("#WarehouseName").val(entityset.M_WAREHOUSE.SHORT_NAME);
                $("#RackId").val(entityset.RackId);
                $("#RackName").val(entityset.M_RackMaster.Rack);

                var Resultset = entityset.FG_Transfer_To_Retail_Details;
                var header = $("<thead></thead>");
                var dyAttributeHead = $("<tr></tr>");
                var attribute = [];

                $.each(Resultset, function (i, result) {
                    $("#AttributeId").val(result.Attribute_ID);
                    $.each(result.Attribute_Name.split(","), function (b, edata) {
                        attribute.push(edata);
                    });
                });
                var uniqueAttr = attribute.filter(function (itm, i, a) {
                    return i == attribute.indexOf(itm);
                });
                //alert(attribute);
                $.each(uniqueAttr, function (b, edata) {
                    var tf_ = $("<th style='width:10em;'>" + edata + "</th>");
                    dyAttributeHead.append(tf_);
                });
                var staticTH = "<th style='width: 10em;'>Work Order Qty</th><th style='width: 10em;'>FG Tranferred Quantity</th><th style='width: 10em;'>Tranfer To Retail Qty</th><th style='width: 10em;'>Balance Quantity</th>";
                dyAttributeHead.append(staticTH);
                header.append(dyAttributeHead);
                $("#itemQtyDetails").append(header);

                var tabstr = "";
                $.each(Resultset, function (i, result) {
                    var attributename = result.Attribute_Value_Name;
                    var index1 = attributename.indexOf(',');
                    if (index1 == -1) {
                        attributename = attributename + ",";
                    }
                    attributename = attributename.split(',');
                    var pr = 0;
                    tabstr = tabstr + "<tr class='attrtabval'><td style='display:none;'><input type='hidden' name='Id' class='EnqIdcs' value='" + result.Id + "' /><input type='hidden' name='FG_TR_Detail_ID' id='FG_TR_Detail_ID" + i + "' value='" + result.FG_TR_Detail_ID + "' /><input type='hidden' name='Attribute_Value_ID' id='Attribute_Value_ID" + i + "' value='" + result.Attribute_Value_ID + "' /> </td>";
                    $.each(result.Attribute_Value_ID.split(","), function (k, e) {

                        if (e != "") {
                            tabstr = tabstr + "<td style='width:10em;'><input type='hidden' value='" + e + "' class='hid2'  />"
                            if (attributename[pr] != "") {
                                tabstr = tabstr + "<input type='hidden' class='hidval2' value='" + attributename[pr] + "' /> " + attributename[pr] + "";
                            }
                            tabstr = tabstr + "</td>";
                            pr = pr + 1;
                        }
                    });
                    tabstr = tabstr + "<td style='width:10em;text-align:right;'><input type='text' id='WorkOrderQty" + i + "' name='WorkOrderQty' class='form-control' value='" + result.WorkOrderQty + "' readonly /></td>";
                    tabstr = tabstr + "<td style='width:10em;text-align:right;'><input readonly type='text' name='TransferredQty' value='" + result.FG_Transferred_Qty + "' id='TransferredQty" + i + "' class='form-control' readonly /></td>";
                    tabstr = tabstr + "<td style='width:10em;text-align:right;'><input type='text' name='TransferToRetailQty' value='" + result.TransferToRetail_Qty + "' id='TransferToRetailQty" + i + "' onchange='calBalanceQty(" + i + ")' onkeypress='return isNumberKey(event, this.id)' class='form-control'/></td>";
                    tabstr = tabstr + "<td style='width:10em;'><input type='text' name='BalanceQty' id='BalanceQty" + i + "' class='form-control' value='" + result.Balance_Qty + "'  readonly /></td>";
                    //tabstr = tabstr + "<td style='width:10em;'><input type='text' class='form-control IAtotProd' id='IAtotProd" + i + "' value='0'  readonly /></td>";
                    tabstr = tabstr + "</tr>"
                });
                tabstr = "<tbody>" + tabstr + "</tbody>";
                $("#itemQtyDetails").append(tabstr);
            },
            failure: function (response) {
                alert(response.d);
            }
        });

    }
}

function getUrlVars() {
    var vars = [], hash;
    //var url = ReplaceAll(window.location.href, '#', '');
    var url = window.location.href;
    var hashes = url.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function ReplaceAll(string, stringToReplace, replaceWith) {
    string = string.split(stringToReplace).join(replaceWith);
    return string;
}



function getCustomerdetails() {
    debugger;
    var startrow = 0;
    var startrowvalue = "---Select----";
    var custid = $("#customerid").val();
    $("#porefno").empty();
    if (custid != "" && parseInt(custid) > 0) {
        $.ajax({
            type: "POST",
            url: '/WorkOrder/getPOListbycustomerid/',
            data: "{'id':" + custid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                $("#porefno").append('<option value="' + startrow + '">' + startrowvalue + '</option>');
                $.each(result, function (i, bussiness) {
                    $("#porefno").append('<option value="' + bussiness.Id + '">' + bussiness.POno + '</option>');
                });
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    //getAddress();
    //getinfo();

}



function getPOdetails(id) {
    var poid = $('#porefno').val();
    var ponumber = $("#porefno option:selected").text();
    if (parseInt(poid) > 0) {
        $.ajax({
            type: "POST",
            url: '/FG_Retail/getPOdetailsbyid/',
            data: "{'id':" + poid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                if (parseInt(result[0]) == 0) {
                    //WO List Bind
                    $("#WorkOrderNo").empty();
                    $("#WorkOrderNo").append("<option value='0'>Select</option>");
                    $.each(result[1], function (i, wodata) {
                        $("#WorkOrderNo").append("<option value='" + wodata.WorkOrderID + "'>" + wodata.WorkOrderNo +"</option>");
                    });
                }
                else {
                    //GRN List Bind
                    $("#GRNno").empty();
                    $("#GRNno").append("<option value='0'>Select</option>");
                    $.each(result[1], function (i, grndata) {
                        debugger;
                        $("#GRNno").append("<option value='" + grndata.ID + "'>" + grndata.GRNNo + "</option>");
                    });
                }
                $("#ScheduleId").hide();
                $("#POQty").val(result[2].styledetailentity.Totalqty);
                if (result[2].MFG_PO_DeliveryDetail != null) {
                    if (result[2].MFG_PO_DeliveryDetail.length > 0) {
                        $("#ScheduleId").show();
                        $("#DeliveryScheduleDetails > tbody").empty();
                        var rowCnt = 0;
                        $.each(result[2].MFG_PO_DeliveryDetail, function (i, item) {
                            rowCnt = rowCnt + 1;
                            var rowid = "rowid" + rowCnt;
                            var item_name = "txtDeliveryItem" + rowCnt;
                            var item_id = "Item_ID" + rowCnt;
                            var scheDate = "txtDelSchDateE" + rowCnt;
                            var delQty = "txtDelQtyE" + rowCnt;
                            var delivery_Comments = "txtDelivery_CommentsE" + rowCnt;
                            var PO_DelSchedule_ID = "PO_DelSchedule_IDE" + rowCnt;
                            var AttribId = "AttribId" + rowCnt;

                            var str = '<tr id="' + rowid + '">' +
                                '<td class="DeliveryItem">' + item.M_ItemMaster.ItemName + '</td>' +
                                '<td class="DeliveryDate">' + setDateformat(item.Delivery_Date) + '</td>' +
                                '<td class="DeliverQty">' + item.Delivery_Qty + '</td>' +
                                '<td class="DeliverComment">' + item.Comments + '</td>' +
                                '<td style="display:none;"><input type="hidden" id="' + PO_DelSchedule_ID + '" class="form-control delrow" name="PO_DelSchedule_ID" value="' + item.PO_DelSchedule_ID + '" />' +
                                '<input type="hidden" id="' + item_id + '" class="form-control delrow" name="Item_ID" value="' + item.Item_ID + '" />' +
                                '<a id="' + rowCnt + '" class="fa fa-fw fa-remove" onclick="removeRow(@rowCnt);"></a></td><td style="display:none;"></td>' +
                                '<td style="display:none;"><input type="hidden" id="' + AttribId + '" class="form-control delrow" name="AttribId" value="' + item.Item_Attribute_Id + '" /></td></tr>';
                            $("#DeliveryScheduleDetails > tbody").append(str);
                        });
                    }

                    
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}

function setDateformat(Dt) {

    var dt1 = Dt.substring(6, Dt.length);
    dt1 = dt1.substring(0, dt1.length - 2);
    var Date1 = new Date(parseInt(dt1));
    return (Date1.getMonth() + 1) + "/" + Date1.getDate() + "/" + Date1.getFullYear();
}

//GRN Data
function GetGRNData() {
    var GRNid = $("#GRNno").val();
    if (parseInt(GRNid) > 0) {
        $.ajax({
            type: "POST",
            url: '/FG_Retail/GetGRNData/',
            data: "{'id':" + GRNid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                if (result != null) {
                    $("#WarehouseName").val(result.M_WarehouseMaster.SHORT_NAME);
                    $("#WarehouseId").val(result.WarehouseId);

                    $.each(result.SRM_GRNItemlist, function (k, itdata) {
                        $("#ItemnameId").val(itdata.M_ItemMaster.ItemName);
                        $("#ItemId").val(itdata.ItemId);
                        $("#ItemAttributeId").val(itdata.Item_Attribute_ID);
                        $("#HSNCodeid").val(itdata.HSNCode);
                        $("#unitNameid").val(itdata.M_UnitMaster.ItemUnit);
                        $("#unitid").val(itdata.UnitId);
                        var MyPIstr = itdata.BatchList;
                        if (MyPIstr != "") {
                            $("#Batchnoid").empty();
                            $("#Batchnoid").append("<option value='0'>Select</option>");
                            $.each(MyPIstr.split('#'), function (i, data) {
                                var btachdata = data.split('~');
                                var Blen = btachdata.length;
                                if (Blen == 3) {
                                    $("#Batchnoid").append("<option value='" + btachdata[2] + "' text='" + btachdata[1]+"'>" + btachdata[1] + "</option>");
                                }
                                else {
                                    $("#Batchnoid").append("<option value='" + btachdata[1] + "' text='" + btachdata[0] +"'>" + btachdata[0] + "</option>");
                                }
                            });
                        }
                    });
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    else {
        $("#ItemnameId").val("");
        $("#ItemId").val("");
        $("#ItemAttributeId").val("");
        $("#HSNCodeid").val("");
        $("#unitNameid").val("");
        $("#unitid").val("");
    }
}


//Get FG Transfer No
function GetFGTransfer() {
    var Woid = $("#WorkOrderNo").val();
    if (parseInt(Woid) > 0) {
        $.ajax({
            type: "POST",
            url: '/FG_Retail/GetFGTransfer/',
            data: "{'id':" + Woid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                $("#TransferNo").empty();
                $("#TransferNo").append("<option value='0'>Select</option>");
                if (result != null) {
                    $.each(result, function (i, fgdata) {
                        $("#TransferNo").append("<option value='" + fgdata.Id + "'>" + fgdata.TransferNo + "</option>");
                    });
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}


function SetTransferData() {
    debugger;
    var ErrorMessage = "";
    var inwardid = $('#Batchnoid option:selected').toArray().map(item => item.value).join();
    if (inwardid == "") {
        ErrorMessage = "Please Select Batch No!";
        isvalid = false;
    }

    var inwardvalue = $('#Batchnoid option:selected').toArray().map(item => item.text).join();
    var $dataRows = $("#itemQtyDetails tr:not('.titlerow')");
    $dataRows.each(function () {
        $(this).find('.Batchnor').each(function (i) {
            var batchno = ($(this).val());
            $.each(inwardvalue.split(','), function (i, dt) {
                if (batchno == dt) {
                    ErrorMessage = batchno + " This Batch No. All Ready in Row!";
                    isvalid = false;
                }
            });
        });
    });
    if (ErrorMessage != "") {
        swal({
            title: 'Error',
            text: ErrorMessage,
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        isvalid = false;
    } else {
        var tabstr = "";
        var ItemId = $("#ItemId").val();
        var itemAttr = $("#ItemAttributeId").val();


        var Josndata = { 'ItemId': ItemId, 'ItemAttr': itemAttr, 'inwardid': inwardid };
        var MyJson = JSON.stringify(Josndata);
        $.ajax({
            type: "POST",
            url: '/BillOfMaterial/SaveAutoPR/',
            data: "{'Model':'" + MyJson + "','type':10}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                debugger;
                result = JSON.parse(result);
                var $dataRows = $("#itemQtyDetails tr:not('.titlerow')");
                var len = $dataRows.length;
                $.each(result.Table, function (key, BT) {
                    len = len + 1;
                    tabstr = "";
                    tabstr = tabstr + "<tr><td style='display:none;'><input type='hidden' id='FG_TR_Detail_ID" + len+"' value='0' readonly /></td>";
                    tabstr = tabstr + "<td>" + BT.Dynamic_Controls_value+"<input type='hidden' value='" + BT.Dynamic_Controls_value + "' readonly /></td>";
                    tabstr = tabstr + "<td style='display:none;'><input type='hidden' id='InwardId" + len +"' value='" + BT.ItemInward_ID + "' readonly /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control' id='BTItemQty" + len + "' value='" + BT.ItemQty + "' readonly /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control' id='IssueQty" + len + "' name='IssueQty" + len + "' value='0' onkeypress='return isNumber(event)' onchange=GetTotalCal('" + len + "')  /></td>";
                    tabstr = tabstr + "<td><input type='text' class='form-control' value='0' id='BalQty" + len + "' Name='BalQty" + len + "' readonly /></td>";
                    tabstr = tabstr + "<td style='width:5em;'><i class='fa fa-trash pluxtrash' onclick='removetr()'></i></td>";
                    tabstr = tabstr + "</tr>";
                    $("#itemQtyDetails > tbody").append(tabstr);
                });

            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}

function GetTotalCal(id) {
    var no = id;
    var bal = 0;
    var batchQty = $("#BTItemQty" + no).val();
    var IssueQty = $("#IssueQty" + no).val();

    if (parseFloat(IssueQty) > parseFloat(batchQty)) {
        msgshow("You Have Required Only " + itemQty + " Quantity!");
        $("#IssueQty" + no).val("");
    }
    else {
        bal = parseFloat(batchQty) - parseFloat(IssueQty);
        $("#BalQty" + no).val(bal);
    }
}

$(document).on('click', 'i.pluxtrash', function () {

    $(this).closest('tr').remove();

    return false;
});

function removetr() {

    $(this).closest('tr').remove();

    return false;

}
