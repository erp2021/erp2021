﻿

function setGSTno() {
    var regtype = $('#suppliertypeid').val();
    if (regtype == "2") {
        $('#gstno').show();
        //var gst = $('#supplierGSTNo').val();
        var cstno = $('#supplierCSTNo').val();
        //var gstno = gst + cstno;
        $('#supplierGSTNo').val(cstno);
    }


}

function setGSTtype() {
    var regtype = $('#suppliertypeid').val();
    if (regtype == "2") {
        $('#gstno').show();
    }
    else {
        $('#gstno').hide();
        $('#supplierGSTNo').val("");
    }
}



$('.select2').select2();
$('.select2').width("100%");

$('#saveSupplier').on('click', function () {
    var suppliername = $("#suppliername").val();
    var suppliertypeid = $('#suppliertypeid').val();
    var supplierphone = $("#supplierphone").val();
    var supplierstateid = $('#supplierstateid').val();
    var suppliercityid = $("#suppliercityid").val();
    var supplierGSTNo = $('#supplierGSTNo').val();
    var supplierTINNo = $('#supplierTINNo').val();
    var suppliermail = $("#supplieremail").val();
    
    var str = "";
    var count = 0;
    var rowcount = 0;
    var isFirstItem = 0;
    var $dataRows = $("#contactstable tr:not('.titlerow')");
    var rowlenght = $dataRows.length;
    var isvalid = true;
    //validations of supplier details 
    if (suppliername == "") {
        swal("Please enter supplier name");
        isvalid = false;
    }
    else if (suppliertypeid == "" || suppliertypeid == 0) {
        swal("Please select supplier type");
        isvalid = false;
    }
    else if (supplierphone == "") {
        swal("Please enter supplier phone no");
        isvalid = false;
    }
    else if (supplierstateid == "" || supplierstateid == 0) {
        swal("Please select supplier state");
        isvalid = false;
    }
    else if (suppliercityid == "" || suppliercityid == 0) {
        swal("Please select supplier city");
        isvalid = false;
    }
        //else if (supplierGSTNo == "") {
        //    swal("Please enter supplier GST No");
        //    isvalid = false;
        //}
    else if (supplierTINNo == "") {
        swal("Please enter supplier TIN No");
        isvalid = false;
    }
        //else if (suppliermail != "") {
        //    if (filter.test(suppliermail)) {
        //        swal("Please enter valid email");
        //        isvalid = false;
        //    }
        //}
    else if (rowlenght > 0) {
        //swal("Please add at least one contact person.");
        //return false;
        $dataRows.each(function () {
            count = 0;
            rowcount++;
            $(this).find('.contactrow').each(function (i) {
                var contactlist = ($(this).val());
                count++;

                if (contactlist != undefined) {

                    if (count == 8) {
                        str += contactlist;
                    }
                    else {
                        str += contactlist + "~";
                    }
                }

            });

            if (rowcount == rowlenght) {
            }
            else {
                str += "#";
            }

        });
        //alert(str);
        $('#ContactList').val(str);
    }

    return isvalid;
});

$(document).on("click", "#addcontacts", function () {

    var contactperson = $("#contactperson").val();
    var mobileno = $("#contactpersonmobile").val();
    var telphone = $("#contactpersontelephone").val();
    var email = $("#contactpersonemail").val();
    var faxno = $("#contactpersonfaxno").val();

    // var contactpersondepartmentid = document.getElementById("contactpersondepartmentid");
    var selecteddept = $("#contactpersondepartmentid option:selected").text(); //contactpersondepartmentid.options[contactpersondepartmentid.selectedIndex].text;
    var deptid = $("#contactpersondepartmentid").val();
    var remark = $("#contactpersonremark").val();

    var rowCount = $('.data-contact-person').length + 1;

    if (contactperson == "") {
        swal("Enter contact person name");
    }
    else if (mobileno == "") {
        swal("Enter contact person mobile no");
    }
    else {
        contactsdiv = '<tr class="data-contact-person odd bg-info">' +
                         '<td style="padding-right:10px;"> <input type="hidden" value="0" class="form-control contactrow"><input type="hidden" value="' + contactperson + '" class="form-control contactrow"><input type="text" name="Item' + rowCount + '" id="Itemid' + rowCount + '" value="' + contactperson + '" class="form-control" disabled style="width:150px;"/></td>' +
              '<td style="padding-right:10px;"><input type="hidden" value="' + mobileno + '" class="form-control contactrow"><input type="text" value="' + mobileno + '" class="form-control" disabled style="width:100px;"/></td>' +
              '<td style="padding-right:10px;"><input type="hidden" value="' + telphone + '" class="form-control contactrow"><input type="text" value="' + telphone + '" class="form-control" disabled style="width:100px;"/></td>' +
               '<td style="padding-right:10px;"><input type="hidden" value="' + email + '" class="form-control contactrow"><input type="text" value="' + email + '" class="form-control" disabled style="width:150px;"/></td>' +
              '<td style="padding-right:10px;"><input type="hidden" value="' + faxno + '" class="form-control contactrow"><input type="text" value="' + faxno + '" class="form-control" disabled style="width:100px;"/></td>' +
              '<td style="padding-right:10px;"><input type="hidden" value="' + deptid + '" class="form-control contactrow"><input type="text" value="' + selecteddept + '" class="form-control" disabled style="width:100px;"/></td>' +
              '<td style="padding-right:10px;"><input type="hidden" value="' + remark + '" class="form-control contactrow"><input type="text" value="' + remark + '" class="form-control" disabled style="width:150px;"/></td>' +
              //'<td style="padding-right:10px;"><p class=""></p></td>' +
              '<td style="padding-right:10px;"><button type="button" id="btnDelete" class="deleteContact btn btn btn-danger btn-xs">Remove</button></td>' +
              '</tr>';
        $('#contactstable').append(contactsdiv);

        $("#contactperson").val("");
        $("#contactpersonmobile").val("");
        $("#contactpersontelephone").val("");
        $("#contactpersonemail").val("");
        $("#contactpersonfaxno").val("");
        $("#contactpersonremark").val("");
    }
});

$(document).on("click", ".deleteContact", function () {
    $(this).closest("tr").remove();// closest used to remove the respective 'tr' in which I have my controls   
});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}