﻿
function SetWHValue(id, value) {
    $("#DeliveryLoaction").val(id);
    $("#warehousenameid").val(value);
    $("#WHlist").hide();
}


function getTaxvalue(id) {
    debugger;
    var number = id.match(/\d+/);
    var taxvalueid = $('#tax' + number).val();
    //var taxvaluename = $("#tax option:selected").text();
    var totRQ = $("#poqty" + number).val() == "" ? 0 : $("#poqty" + number).val();
    var salP = $("#ItemRatetd" + number).val() == "" ? 0 : $("#ItemRatetd" + number).val();
    var subtot = parseFloat(totRQ) * parseFloat(salP);
    var taxv = (parseFloat(subtot) * parseFloat(taxvalueid)) / 100;
    $("#itemtd" + number).val('' + taxv);
    calculatepoqty(id);
}

function calculatepoqty(id) {
    debugger;
    var number = id.match(/\d+/);
    var watstgeper = $('#wastageper' + number).val();
    var qty = $('#Totalqty' + number).val();
    var stockqty = $('#stockqty' + number).val();
    if (watstgeper == "")
        watstgeper = "0";

    var availqty = 0;
    if (parseFloat(stockqty) > parseFloat(qty)) {
        availqty = 0;
    }
    else {
        availqty =parseFloat(qty) - parseFloat(stockqty);
    }
    var addqty = parseFloat(availqty) * watstgeper / 100;
    $('#poqty' + number).val((availqty + addqty).toFixed(2));
    caltotalamt(id);
}


function caltotalamt(id) {
    debugger;
    var number = id.match(/\d+/);
    var poqty = $("#poqty" + number).val();
    var itemrate = $("#ItemRatetd" + number).val();
    var taxvalue = $("#tax" + number).val();
    if (poqty == "")
        poqty = "0";

    if (taxvalue == "" || taxvalue =='select')
        taxvalue = "0";

    
    var totalamount = parseFloat(itemrate) * parseFloat(poqty);
    $("#totalprice" + number).val(totalamount.toFixed(2));
    calculatetax();
}


function calculatetax() {
    debugger;
    var $dataRows = $("#rawitemtable tr:not('.titlerow')");
    var rowlenght = $dataRows.length;
    var rowcount = 0;
    var itemtotalamount = parseFloat($("#ItemTotal").val());

    var total = 0;
    var totaltaxval = 0;
    var tamt = 0;
    $dataRows.each(function () {
        debugger;
        rowcount++;

        var Totalqty = $("#poqty" + rowcount).val() == "" ? 0 : $("#poqty" + rowcount).val();
        var itemrate = $("#ItemRatetd" + rowcount).val();
        if (Totalqty == "")
            Totalqty = "0";

        var totalamount = parseFloat(Totalqty) * parseFloat(itemrate);
        tamt = tamt + parseFloat(totalamount);


        var taxvalue = $("#tax" + rowcount).val();
        if (taxvalue == "" || taxvalue == 'select')
            taxvalue = "0";

        total = parseFloat(Totalqty) * parseFloat(itemrate) * parseFloat(taxvalue) / 100;
        totaltaxval = parseFloat(totaltaxval) + parseFloat(total);


        $('.totalamt').each(function () {
            var totalval = $(this).val();
            if (totalval == "")
                totalval = "0";

            total = total + parseFloat(totalval);


        });
    });
    $('#ItemTotal').val(tamt.toFixed(2));

    var grandtotal = parseFloat(tamt) + parseFloat(totaltaxval);

    $('#TaxAmount').val(parseFloat(totaltaxval).toFixed(2));
    $('#total').val(parseFloat(grandtotal).toFixed(2));
    $("#Grandtotal").val(parseFloat(grandtotal).toFixed(2));
}

function caltax(id) {
    var number = id.match(/\d+/);
    var total = 0;
    var totaltaxval = 0;
    var Totalqty = $("#Totalqty" + number).val();
    var itemrate = $("#ItemRatetd" + number).val();
    var taxvalue = $("#tax" + number).val();

    var hidevalue = parseFloat($("#hideTaxAmount").val());

    var hideitemtotalamount = parseFloat($("#hideItemTotal").val());
    var hidegrandtotal = parseFloat($("#GrandTotal").val());
    var grandtotal = 0;

    if (taxvalue != "") {
        total = parseFloat(Totalqty) * parseFloat(itemrate) * parseFloat(taxvalue) / 100;
    }
    else {
        taxvalue = "0";
        total = parseFloat(Totalqty) * parseFloat(itemrate) * parseFloat(taxvalue) / 100;
    }

    if (hidevalue != "" || hidevalue > 0) {
        totaltaxval = parseFloat(total) + parseFloat(hidevalue);
    }
    else {
        totaltaxval = total;
    }


    if (hideitemtotalamount == "") {
        grandtotal = parseFloat(hidegrandtotal) + parseFloat(hideitemtotalamount) + parseFloat(hidevalue);
    }
    else {
        grandtotal = parseFloat(hideitemtotalamount) + parseFloat(hidevalue);
    }


    $("#TaxAmount").val(totaltaxval);
    $("#hideTaxAmount").val(totaltaxval);
    $("#GrandTotal").val(grandtotal);
    $("#total").val(grandtotal);
    $("#Grandtotal").val(grandtotal);
}



$('#SavePO').on('click', function () {
    var QuotationId = $("#QuotationId").val();
    var contactperson = $("#contactperson").val();
    var pofromdate = $("#POValidityFrom").val();
    var potodate = $("#POValidityTo").val();
    var deliveryloc = $("#DeliveryLoaction").val();
    var paymentterm = $("#PaymentTerm").val();

    var str = "";
    var count = 0;
    var rowcount = 0;
    var isFirstItem = 0;
    var $dataRows = $("#rawitemtable tr:not('.titlerow')");
    var rowlenght = $dataRows.length;
    var isvalid = true;

    if (pofromdate == "") {
        swalmsg("Please select from date");
        isvalid = false;
    }
    else if (potodate == "") {
        swalmsg("Please select PO valid Upto date");
        isvalid = false;
    }
    else if (QuotationId == "" || QuotationId == 0) {
        swalmsg("Please select quotation");
        isvalid = false;
    }
    else if (contactperson == "" || contactperson == 0) {
        swalmsg("Please select contact person");
        isvalid = false;
    }
    else if (deliveryloc == "" || deliveryloc == 0) {
        swalmsg("Please select delivery location");
        isvalid = false;
    }
    else if (paymentterm == "" || paymentterm == 0) {
        swalmsg("Please select payment term");
        isvalid = false;
    }
    else if (rowlenght > 0) {
        var stylerowcount = 0;
        var Stcount = 0;
        $dataRows.each(function () {
            debugger;
            count = 0;
            stylerowcount++;
            var Aleng = $(this).children('td').length;
            $(this).find('.rawitem').each(function (i) {
                debugger;
                var itemlist = ($(this).val());  //here get td value
                count++;

                if (itemlist != undefined && itemlist != "") {

                    if (count == Aleng) {
                        str += itemlist;
                    }
                    else {
                        str += itemlist + "~";
                    }
                }
                else {
                    Stcount = 1;
                }

            });

            if (stylerowcount == rowlenght) {
                rowlenght = rowlenght;
            }
            else {
                str += "#";
            }
            $('#POItemList').val(str);

        });
        debugger;
        if (Stcount == 1) {
            swalMsg("Item Details Required.");
            isvalid = false;
        }

        var $OC_dataRows = $("#OtherChargeDetails tr:not('.rowtitle')");
        var OClenght = $OC_dataRows.length;
        var Ocrowcount = 0;
        var str1 = "";
        if (OClenght > 0) {
            $OC_dataRows.each(function (i) {
                count = 0;
                Ocrowcount++;
                var Aleng = $(this).children('td').length;
                $(this).find('.ocdata').each(function (i) {
                    var itemlist = ($(this).val());
                    count++;
                    itemlist = itemlist == "select" || itemlist == "" ? 0 : itemlist;
                    if (itemlist != undefined) {

                        if (count == Aleng) {
                            str1 += itemlist;
                        }
                        else {
                            str1 += itemlist + "~";
                        }
                    }
                });

                if (Ocrowcount == OClenght) {
                }
                else {
                    str1 += "#";
                }
            });
            $("#otherchargestr").val(str1);
        }
        else {
            $("#otherchargestr").val("");
        }
    }

    createPODeliveryStr();
    return isvalid;
});
//function visiblectrl(id) {
//    var number = id.match(/\d+/);
//    var taxvalue = $("#itemtd" + number).val();
//    taxvalue.prop("disabled", disabled);  onchange="visiblectrl(this.id)"
//}

$('#EditPO').on('click', function () {
    var contactperson = $("#ContactPersonId").val();


    var str = "";
    var count = 0;
    var rowcount = 0;
    var isFirstItem = 0;
    var $dataRows = $("#rawitemtable tr:not('.titlerow')");
    var rowlenght = $dataRows.length;
    var isvalid = true;
    
    if (contactperson == "" || contactperson == 0) {
        swalmsg("Please select contact person");
        isvalid = false;
    }
    else if (rowlenght > 0) {
        var stylerowcount = 0;
        var Stcount = 0;
        $dataRows.each(function () {
            debugger;
            count = 0;
            stylerowcount++;
            var Aleng = $(this).children('td').length;
            $(this).find('.rawitem').each(function (i) {
                debugger;
                var itemlist = ($(this).val());  //here get td value
                count++;

                if (itemlist != undefined && itemlist != "") {

                    if (count == Aleng) {
                        str += itemlist;
                    }
                    else {
                        str += itemlist + "~";
                    }
                }
                else {
                    Stcount = 1;
                }

            });

            if (stylerowcount == rowlenght) {
                rowlenght = rowlenght;
            }
            else {
                str += "#";
            }
            $('#POItemList').val(str);

        });
        debugger;
        if (Stcount == 1) {
            swalMsg("Item Details Required.");
            isvalid = false;
        }

        var $OC_dataRows = $("#OtherChargeDetails tr:not('.rowtitle')");
        var OClenght = $OC_dataRows.length;
        var Ocrowcount = 0;
        var str1 = "";
        if (OClenght > 0) {
            $OC_dataRows.each(function (i) {
                count = 0;
                Ocrowcount++;
                var Aleng = $(this).children('td').length;
                $(this).find('.ocdata').each(function (i) {
                    var itemlist = ($(this).val());
                    count++;
                    itemlist = itemlist == "select" || itemlist == "" ? 0 : itemlist;
                    if (itemlist != undefined) {

                        if (count == Aleng) {
                            str1 += itemlist;
                        }
                        else {
                            str1 += itemlist + "~";
                        }
                    }
                });

                if (Ocrowcount == OClenght) {
                }
                else {
                    str1 += "#";
                }
            });
            $("#otherchargestr").val(str1);
        }
        else {
            $("#otherchargestr").val("");
        }
    }

    createPODeliveryStr();
    return isvalid;
});

function swalmsg(msg) {
    swal({
        title: 'Error',
        text: msg,
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false,
        closeOnCancel: false

    });
}




