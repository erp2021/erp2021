﻿function ShowOtherCharge() {
    debugger;
        var OC_id = $("#otherchrge option:selected").val();
        var OC_name=$("#otherchrge option:selected").text();
    $("#OtherChargeDetails").show();
    $("#OC_GrandTotal_id").show();
        var $dataRows = $("#OtherChargeDetails tr:not('.rowtitle')");
        var sizerowlenght = $dataRows.length;
        var str = "";
    var i = 0;
     var status = true;
        if (sizerowlenght > 0) {
            $dataRows.each(function (i) {
                count = 0;
                var Aleng = $(this).children('td').length;
                $(this).find('.ocdata').each(function (i) {
                    debugger;
                    var ocval = ($(this).val());  //here get td value
                    count++;
                    if (i == 0) {
                        if (OC_id == ocval) {
                            status = false;
                        }
                    }
                });
            });
        }
        else {
            status = true;
            
        }

    if (status == true && OC_name !="Select") {
         i = i + sizerowlenght;
         str = "";
         str = "<tr><td>" + OC_name + "<input type='hidden' class='ocdata' id='otherchargeid" + i + "' name='otherchargeid" + i + "' value='" + OC_id + "' /></td>";
         str = str + "<td><input type='text' class='form-control ocdata' id='OC_Amount" + i + "' name='OC_Amount" + i + "' onchange='OC_Cal(this.id)' onkeypress='return isNumber(event)'/></td>";
         str = str + "<td><select class='form-control select2 OC_GST ocdata' style='width:115px;' id='OC_GSTid" + i + "' onchange='OC_Cal(this.id)' name='OC_GSTid" + i + "'></select></td>";
         str = str + "<td><input type='text' class='form-control ocdata' id='OC_GSTAmt" + i + "' name='OC_GSTAmt" + i + "' onkeypress='return isNumber(event)' readonly /></td>";
         str = str + "<td><input type='text' class='form-control ocdata' id='OC_total" + i + "' name='OC_total" + i + "' onkeypress='return isNumber(event)' readonly /></td>";
         str = str + "<td><input type='hidden' class='ocdata' id='ocTid" + i + "' name='ocTid" + i + "' value='0' /><a id='btnOCDelete' class='deleteOC btn-danger btn-xs'><i class='fa fa-fw fa-trash'></i></a></td></tr>";
         $("#OtherChargeDetails > tbody").append(str);

         //Get Tax List
         $.ajax({
             type: "POST",
             url: '/quotation/getGSTTaxlist/',
             data: '{}',
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function (gstsdata) {
                 var result_ = JSON.parse(gstsdata);
                 $('#OC_GSTid'+i).empty();
                 $("#OC_GSTid"+i).append('<option value="select">Select</option>');
                 $.each(result_, function (k, item) {
                     $("#OC_GSTid"+i).append('<option value="' + item.Tax_Rate + '">' + item.Category_Name + '</option>');
                 });
                 
             },
             failure: function (response) {
                 alert(response.d);
             }
         });    
     }
    else {
        if (OC_name != "Select") {
            mesgshow("Error", "This Other Charge Name All Ready Exits in Row", "error");
        }
     }
} 


function mesgshow(tit,msg,type) {
    swal({
        title: tit,
        text: msg,
        type: type,
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false,
        closeOnCancel: false

    });
}

$(document).on('click', 'a.deleteOC', function () {

    $(this).closest('tr').remove();
    var $dataRows = $("#OtherChargeDetails tr:not('.rowtitle')");
    var sizerowlenght = $dataRows.length;
    var potot = 0;
    if (sizerowlenght == 0) {
        $("#OtherChargeDetails").hide();
        $("#OC_GrandTotal_id").hide();
        potot = $("#total").val();
        $("#Grandtotal").val(potot);
    }
    else {
        var OC_tot1 = 0;
        $dataRows.each(function (i) {

            $(this).find('.ocdata').each(function (i) {
                if (i ==4) {
                    var Octot = ($(this).val());
                    Octot = Octot == "" ? 0 : Octot;
                    OC_tot1 = parseFloat(OC_tot1) + parseFloat(Octot);
                }
            });
        });
        potot = $("#total").val();
        var Grandtot = parseFloat(potot) + parseFloat(OC_tot1);
        $("#Grandtotal").val(Grandtot.toFixed(2));
    }
    return false;
});


function OC_Cal(id) {
    debugger;
    var no = id.match(/\d+/);
    var OC_amount = $("#OC_Amount" + no).val();
    var OC_GST = $("#OC_GSTid" + no).val();
    var GST = OC_GST == "" || OC_GST == "select" ? 0 : OC_GST;
    var gstamount = parseFloat(OC_amount) * parseFloat(GST) / 100;
    $("#OC_GSTAmt" + no).val(gstamount.toFixed(2));
    var tot = parseFloat(gstamount) + parseFloat(OC_amount);
    $("#OC_total" + no).val(tot.toFixed(2));
    var $dataRows = $("#OtherChargeDetails tr:not('.rowtitle')");
    var len = $dataRows.length;
    var OC_tot1 = 0;
    if (len > 0) {
        $dataRows.each(function (i) {
            $(this).find('.ocdata').each(function (i) {
                if (i == 4) {
                    var Octot = ($(this).val());
                    Octot = Octot == "" ? 0 : Octot;
                    OC_tot1 = parseFloat(OC_tot1) + parseFloat(Octot);
                }
            });
        });
        var potot = $("#total").val();
        var Grandtot = parseFloat(potot) + parseFloat(OC_tot1);
        $("#Grandtotal").val(Grandtot);
    }
}

