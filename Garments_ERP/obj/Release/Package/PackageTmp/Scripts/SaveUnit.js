﻿


$('#saveUnit').on('click', function () {
    var ItemUnit = $("#ItemUnit").val();
    var DepartmentId = $('#DepartmentId').val();
    var isvalid = true;
    //validations of Rack details 
    if (ItemUnit == "") {
        swal("Please enter unit name");
        isvalid = false;
    }
    else if (DepartmentId == "" || DepartmentId == 0) {
        swal("Please select department");
        isvalid = false;
    }

    return isvalid;
});

$(document).ready(function () {
    $('.select2').select2();
    $('.select2').width("100%");
});