﻿function Editstylesizedetail(id) {
    //alert("in editstylesize" + id);
    var number = id.match(/\d+/);
    var total = 0;
    var sizeper_ele = $('#sizeper' + number).val();
    var sizepcs_ele = $('#sizepcs' + number).val();
    var reqty = $('#reqqty').val();
    if (reqty == "") {
        // $('#qtymsg').text();
        swal("enter require qty.");



    }
    else {
        $('#qtymsg').text("");
        if (sizepcs_ele != "") {
            var sizeper = (parseFloat(sizepcs_ele) * 100) / parseFloat(reqty);
            $('#sizeper' + number).val(parseFloat(sizeper).toFixed(2));

            $('#pertd' + number).attr("sizedata", parseFloat(sizeper).toFixed(2));
            $('#pcstd' + number).attr("sizedata", parseFloat(sizepcs_ele).toFixed(2));
            // $('#sizeper' + number).attr('sizedata') = parseFloat(sizeper).toFixed(2);
        }
        else {

            var sizepcs = (parseFloat(sizeper_ele) * parseFloat(reqty) / 100);
            $('#sizepcs' + number).val(parseFloat(sizepcs).toFixed(2));

            $('#pcstd' + number).attr("sizedata", parseFloat(sizepcs).toFixed(2));
            $('#pertd' + number).attr("sizedata", parseFloat(sizeper_ele).toFixed(2));
            //  $('#sizepcs' + number).attr('sizedata') = parseFloat(sizepcs).toFixed(2);
        }
    }
}


function editsizeper(id) {
    //alert("in editstylesize" + id);
    var number = id.match(/\d+/);
    var total = 0;
    var sizeper_ele = $('#sizeper' + number).val();
    var sizepcs_ele = $('#sizepcs' + number).val();
    var reqty = $('#reqqty').val();
    if (reqty == "") {
        // $('#qtymsg').text();
        swal("enter require qty.");

    }

    else {
        $('#qtymsg').text("");
        if (sizeper_ele != "") {
            if (parseFloat(sizeper_ele) <= 100) {
                var sizepcs = (parseFloat(sizeper_ele) * parseFloat(reqty) / 100);
                $('#sizepcs' + number).val(parseFloat(sizepcs).toFixed(2));

                $('#pcstd' + number).attr("sizedata", parseFloat(sizepcs).toFixed(2));
                $('#pertd' + number).attr("sizedata", parseFloat(sizeper_ele).toFixed(2));
            }
            else {
                swal("enter valid percentage");
            }
        }
        // $('#sizepcs' + number).attr('sizedata') = parseFloat(sizepcs).toFixed(2);
        //}
    }
}

function editsizepcs(id) {
    var number = id.match(/\d+/);
    var total = 0;
    var sizeper_ele = $('#sizeper' + number).val();
    var sizepcs_ele = $('#sizepcs' + number).val();
    var reqty = $('#reqqty').val();
    if (reqty == "") {
        // $('#qtymsg').text();
        swal("enter require qty.");

    }
    else {
        // $('#qtymsg').text("");
        if (sizepcs_ele != "") {

            if (parseFloat(sizepcs_ele) <= parseFloat(reqty)) {
                var sizeper = (parseFloat(sizepcs_ele) * 100) / parseFloat(reqty);
                $('#sizeper' + number).val(parseFloat(sizeper).toFixed(2));

                $('#pertd' + number).attr("sizedata", parseFloat(sizeper).toFixed(2));
                $('#pcstd' + number).attr("sizedata", parseFloat(sizepcs_ele).toFixed(2));
            }
            else {
                swal("enter valid quantity");
            }
        }

    }

}





$(document).on("click", ".deletestyle", function () {
    var id = $(this).closest("tr").attr('data');
    // alert(id);
    $(this).closest("tr").remove(); // closest used to remove the respective 'tr' in which I have my controls
    // $('#hidestyletable tr').find('#' + id).remove();
    $('table#hidestyletable tr#' + id).remove();


});
$(document).on("click", ".deleterawitem", function () {
    var id = $(this).closest("tr").attr('data');
    // alert(id);
    $(this).closest("tr").remove(); // closest used to remove the respective 'tr' in which I have my controls
    //  $('#hiderawitemtable tr').find('#' + id).remove();
    $('table#hiderawitemtable tr#' + id).remove();

});

function validrawitem() {
    var valid = true;

    var itemcategoryid = $("#itemcategoryid").val();
    var itemsubcategoryid = $("#itemsubcategoryid").val();


    $('#rawitemtable tr').each(function () {

        if ($(this).find('.validitemcatrow').val() == itemcategoryid && $(this).find('.validitemsubrow').val() == itemsubcategoryid) {
            valid = false;
        }
    });

    return valid;
}

$(document).on("click", ".addrawitem", function () {
    debugger;
    var selecteditemcategory = $("#itemcatNameid").val();
    var itemcategoryid = $("#itemcategoryid").val();

    var itemsubcategoryid = $("#itemsubcategory").val();
    var selecteditemsubcategory = $("#itemsubcategoryNameid").val();

   // var rawitem = document.getElementById("rawitemid");
  //  var selectedrawitem = rawitem.options[rawitem.selectedIndex].text;
  //  var rawitemid = $("#rawitemid").val();

    var itemdesc = $('#itemdesc').val();

    var supplier = $("#supplier").val();

    var i = $('#rawitemtable tr').length;

    var rowCount = 0;

    if (itemcategoryid == "0") {
        swal({
            title: 'Error',
            text: "Please Select Item Category.",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
    }
    else if (itemsubcategoryid == "0") {
        swal({
            title: 'Error',
            text: "Please Select Item Subcategory.",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
    }
    else if (validrawitem() == false) {
        swal({
            title: 'Error',
            text: "selected item category or item sub category already added.",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
    }
  //  else if (rawitemid == "0") {
   //     swal("select rawitem");
        // alert(rawitemid);
   // }
    else {

        var file1 = document.querySelector('#rawimg');
        var fcount = file1.files.length;
        var len1 = $("#rawitemtable tr").length;
        len1 = len1 + 1;
        //itemcate subcategory item supploer
        //rawdiv = '<tr class="data-contact-person odd">' +
        //      '<td style="padding-right:10px;"><input type="hidden" value="' + itemcategoryid + '" class="form-control "><input type="text" name="Item' + rowCount + '" id="Itemid' + rowCount + '" value="' + selecteditemcategory + '" class="form-control" disabled style="width:100px;"/></td>' +
        //      '<td style="padding-right:10px;"><input type="hidden" value="' + itemsubcategoryid + '" class="form-control "><p class="">' + selecteditemsubcategory + '</p></td>' +
        //      '<td style="padding-right:10px;"><input type="hidden" value="' + rawitemid + '" class="form-control "><p class="">' + selectedrawitem + '</p></td>' +
        //       '<td style="padding-right:10px;"><p class="">' + supplier + '</p></td>' +
        //      '<td style="padding-right:10px;"></td>' +
        //      '<td><button type="button" id="btnDelete" class="deleterawitem btn btn btn-danger btn-xs">Remove</button></td>' +
        //      '</tr>';


        rawdiv = '<tr class="data-contact-person odd" id="' + (i + 1) + '" value="' + (i + 1) + '" data="' + (i + 1) + '" >' +
            '<td rawdata="0" class="rawitemrow" style="display:none;"><input type="hidden" value="0" class="form-control"></td>' +
            '<td style="padding-right:10px;" class="rawitemrow " rawdata="' + itemcategoryid + '"><input type="hidden" value="' + itemcategoryid + '" class="form-control validitemcatrow"><input type="text" name="Item' + rowCount + '" id="Itemid' + rowCount + '" value="' + selecteditemcategory + '" class="form-control" disabled style="width:100px;display:none;"/>' + selecteditemcategory+'</td>' +
            '<td style="padding-right:10px;" class="rawitemrow " rawdata="' + itemsubcategoryid + '"><input type="hidden" value="' + itemsubcategoryid + '" class="form-control validitemsubrow"><p class="">' + selecteditemsubcategory + '</p></td>' +
            '<td style="padding-right:10px;" class="rawitemrow" rawdata="' + itemdesc + '"><input type="hidden" value="' + itemdesc + '" class="form-control "><p class="">' + itemdesc + '</p></td>' +
            '<td style="padding-right:10px;" class="rawitemrow" rawdata="' + supplier + '"><p class="">' + supplier + '</p></td><td style="padding-right:10px;" class="rawitemrow" id="imgraw' + (i + 1) + '">';
        if (parseInt(fcount) > 0) {
            rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="View Image" id="Rviewimage' + len1 + '"  onclick=GetImageUpload(this.id,"preview","RawItem") /><input type="button" class="btn btn-info" id="Ruploadimage' + len1 + '" value="Upload Image" style="display: none;" onclick=GetImageUpload(this.id,"upload","RawItem") />';
        }
        else {
            rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="View Image" id="Rviewimage' + len1 + '" style="display: none;"  onclick=GetImageUpload(this.id,"preview","RawItem") /><input type="button" class="btn btn-info" id="Ruploadimage' + len1 + '" value="Upload Image"  onclick=GetImageUpload(this.id,"upload","RawItem") />';
        }
        rawdiv = rawdiv + '<input type="file" style="display:none;" id="rawitemimg' + len1 + '" name="rawitemimg" class="form-control" multiple /></td>';
        rawdiv = rawdiv + '<td><button type="button" id="btnDelete" class="deleterawitem btn btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></button></td>' +
              '</tr>';

        $('#rawitemtable').append(rawdiv);

        if (parseInt(fcount) > 0) {
            var file2 = document.querySelector('#rawitemimg' + len1);
            file2.files = file1.files;
            var emptyFile = document.createElement('input');
            emptyFile.type = 'file';
            file1.files = emptyFile.files;
        }


        hiderawdiv = '<tr class="data-contact-person odd" id="' + (i + 1) + '" value="' + (i + 1) + '" data="' + (i + 1) + '">' +
            '<td style="padding-right:10px;"><input type="hidden" value="0" class="form-control rawitemrow"></td>' +
           '<td style="padding-right:10px;"><input type="hidden" value="' + itemcategoryid + '" class="form-control rawitemrow"></td>' +
           '<td style="padding-right:10px;"><input type="hidden" value="' + itemsubcategoryid + '" class="form-control rawitemrow"></td>' +
            '<td style="padding-right:10px;"><input type="hidden" value="' + itemdesc + '" class="form-control rawitemrow"></td>' +
            '<td style="padding-right:10px;"><input type="hidden" value="' + supplier + '" class="form-control rawitemrow"></td>' +
            '<td style="padding-right:10px;"><input type="hidden" value="' + fcount + '" class="form-control rawitemrow" id="rawitemcount' + len1 + '"></td>' +
             '<td style="padding-right:10px;"><button type="button" id="btnDelete" class="deleterawitem btn btn btn-danger btn-xs" style="display:none;">Remove</button></td>' +
         '</tr>';

        $('#hiderawitemtable').append(hiderawdiv);
        
        $('#itemcatNameid').val("");
        $('#itemsubcategoryNameid').val("");
        $('#itemcategoryid').val("0");
        $('#itemcategoryid').trigger("change");
        $('#itemsubcategoryid').val("0");
        $('#itemsubcategoryid').trigger("change");
        $("#supplier").val("");
        $('#itemdesc').val("");
    }
});


// Create By Shubham QuationCal() 18-07-2019 Start
function GetTotQty(id) {
    debugger;
    var no = id.match(/\d+/);
    no = no != null ? no[0] : "";
    var RQty = $("#IAReqQty" + no).val();
    RQty = RQty != "" ? RQty : 0;
    var SellRate = $("#IASellRate" + no).val();
    SellRate = SellRate != "" ? SellRate : 0;
    var GST = $("#GSTCharge" + no).val();
    GST = GST != "" && GST!="select" ? GST : 0;
    var tot = parseFloat(RQty) * parseFloat(SellRate);
    var taxv = (parseFloat(tot) * parseFloat(GST)) / 100;
    $("#IATax" + no).val(taxv);
    tot = parseFloat(tot) + parseFloat(taxv);
    $("#IAtotal" + no).val(parseFloat(tot).toFixed(2));
    var TotQty = 0;
    $("#AttributeTable tr.attrtabval").children('td').find(".IAReqQty").each(function () {
        var Qty = this.value == "" ? 0 : parseFloat(this.value);
            TotQty = TotQty + parseFloat(Qty);
    });
    $("#reqqty").val(TotQty);
    QuationCal();
    }

function QuationCal() {
    var totRQ = 0;
    var salP = 0;
    var subtot = 0;
    var st = 0;
    totRQ = $("#reqqty").val() == "" ? 0 : $("#reqqty").val();
    var totSell = 0;
    $("#AttributeTable tr.attrtabval").children('td').find(".IAtotal").each(function () {
        var Qty = this.value == "" ? 0 : parseFloat(this.value);
        totSell = totSell + parseFloat(Qty);
    });
    $("#total").val('' + totSell);
    $("#Htotal").val('' + totSell);
    $("#Grandtotal").val('' + totSell);
    GetGrandTot(); 
}


function GetGrandTot() {
    var $dataRows = $("#OtherChargeDetails tr:not('.rowtitle')");
    var len = $dataRows.length;
    var OC_tot1 = 0;
    var potot = $("#total").val();
    if (len > 0) {
        $dataRows.each(function (i) {
            $(this).find('.ocdata').each(function (i) {
                if (i == 4) {
                    var Octot = ($(this).val());
                    Octot = Octot == "" ? 0 : Octot;
                    OC_tot1 = parseFloat(OC_tot1) + parseFloat(Octot);
                }
            });
        });

        var Grandtot = parseFloat(potot) + parseFloat(OC_tot1);
        $("#Grandtotal").val(Grandtot);
    }
    else {
        $("#Grandtotal").val(potot);
    }
}

// Create By Shubham QuationCal() 18-07-2019 END