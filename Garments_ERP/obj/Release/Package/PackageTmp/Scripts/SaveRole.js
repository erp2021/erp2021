﻿$('#SaveRole').on("click", function () {
    var isvalid = true;
    var RoleName = $('#RoleName').val();
    if (RoleName == "") {
        ErrorMsg("Please Enter Role Name!");
        isvalid = false;
    }
    return isvalid;
});

function ErrorMsg(msg) {
    swal({
        title: "Error",
        text: msg,
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false,
        closeOnCancel: false

    });
}