﻿


//function to set GST No
function setGSTno() {
    var regtype = $('#customertypeid').val();
    if (regtype == "1") {
        $('#gstno').show();
        var statecode = $('#statecodehidden').val();
        //var gst = $('#customergstno').val();
        var panno = $('#customerCSTNo').val();
        var gstno = statecode + panno;
        $('#customergstno').val(gstno);
    }
    else {
        $('#gstno').hide();
        $('#customergstno').val("");
    }

}

function checkGSTNo() {
    var gstno = $('#customergstno').val();
    var gstinformat = new RegExp('^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$');
    if (gstinformat.test(gstno)) {

    }
    else {
        swal({
            title: "GST No validation",
            text: "Click on OK to enter valid GST No otherwise Reset.",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Reset",
            cancelButtonClass: "btn-danger",
            confirmButtonText: "OK",
            closeOnConfirm: false
        }, function (isConfirm) {
            if (isConfirm) {
                swal.close();
            }
            else {
                $("#customergstno").val("");
            }
        })
    }
}

//function setGSTtype() {
//    var regtype = $('#customertypeid').val();
//    if (regtype == "1") {
//        $('#gstno').show();
//    }
//    else {
//        $('#gstno').hide();
//        $('#customergstno').val("");
//    }
//}


$('#saveCustomer').on('click', function () {
    var customername = $("#customername").val();
    var customertypeid = $('#customertypeid').val();
    var customerphone = $("#custpmerphone").val();
    var customerstateid = $('#customerstateid').val();
    var customercityid = $("#customercityid").val();
    var customerGSTNo = $('#customergstno').val();
    var customerTINNo = $('#cutomerTINno').val();
    var customermail = $("#customeremail").val();

    var str = "";
    var addressstr = "";
    var count = 0;
    var rowcount = 0;
    var addrowcount = 0;
    var isFirstItem = 0;
    var $dataRows = $("#contactstable tr:not('.titlerow')");
    var rowlenght = $dataRows.length;

    var $dataRows_address = $("#contactaddresstable tr:not('.titlerow')");
    var addressrowlenght = $dataRows_address.length;

    var isvalid = true;
    //validations of customer details 
    if (customername == "") {
        swal("Please enter customer name");
        isvalid = false;
    }
    else if (customerTINNo == "") {
        swal("Please enter customer TIN No");
        isvalid = false;
    }
    else if (customertypeid == "" || customertypeid == 0) {
        swal("Please select customer type");
        isvalid = false;
    }
    else if (customerphone == "") {
        swal("Please enter customer phone no");
        isvalid = false;
    }
    //  else if (customerstateid == "" || customerstateid == 0) {
    //      swal("Please select customer state");
    //      isvalid = false;
    //  }
    //  else if (customercityid == "" || customercityid == 0) {
    //      swal("Please select customer city");
    //      isvalid = false;
    //}
    else if (parseInt(addressrowlenght) <= 0) {
        swal("Please add address details.");
        isvalid = false;
    }
    else if (rowlenght > 0 && parseInt(addressrowlenght) > 0) {

        $dataRows.each(function () {
            count = 0;
            rowcount++;
            $(this).find('.contactrow').each(function (i) {
                var contactlist = ($(this).val());
                count++;

                if (contactlist != undefined) {

                    if (count == 8) {
                        str += contactlist;
                    }
                    else {
                        str += contactlist + "~";
                    }
                }

            });

            if (rowcount == rowlenght) {
            }
            else {
                str += "#";
            }

        });
        // alert(str);
        $('#ContactList').val(str);

        $dataRows_address.each(function () {
            count = 0;
            addrowcount++;
            $(this).find('.addressrow').each(function (i) {
                debugger;
                var addresslist = ($(this).attr("rawdata"));
                count++;

                if (addresslist != undefined) {

                    if (count == 5) {
                        addressstr += addresslist;
                    }
                    else {
                        addressstr += addresslist + "~";
                    }
                }

            });

            if (addrowcount == addressrowlenght) {
            }
            else {
                addressstr += "#";
            }

        });

        //  alert(addressstr);
        $('#AddressList').val(addressstr);
    }



    return isvalid;
});



$(document).on("click", "#addcontacts", function () {

    var contactperson = $("#contactperson").val();
    var mobileno = $("#contactpersonmobile").val();
    var telphone = $("#contactpersontelephone").val();
    var email = $("#contactpersonemail").val();
    var faxno = $("#contactpersonfaxno").val();
    var selecteddept = $("#contactpersondepartmentid option:selected").text();
    var deptid = $("#contactpersondepartmentid").val();
    var remark = $("#contactpersonremark").val();

    var rowCount = $('.data-contact-person').length + 1;

    if (contactperson == "") {
        swal("Enter contact person name");
    }
    else if (mobileno == "") {
        swal("Enter contact person mobile no");
    }
    else {
        contactsdiv = '<tr class="data-contact-person odd bg-info">' +
            '<td style="padding-right:10px;"><input type="hidden" value="0" class="form-control contactrow"><input type="hidden" value="' + contactperson + '" class="form-control contactrow"><input type="text" name="Item' + rowCount + '" id="Itemid' + rowCount + '" value="' + contactperson + '" class="form-control" disabled style="width:150px;"/></td>' +
            '<td style="padding-right:10px;"><input type="hidden" value="' + mobileno + '" class="form-control contactrow"><input type="text" value="' + mobileno + '" class="form-control" disabled style="width:100px;"/></td>' +
            '<td style="padding-right:10px;"><input type="hidden" value="' + telphone + '" class="form-control contactrow"><input type="text" value="' + telphone + '" class="form-control" disabled style="width:100px;"/></td>' +
            '<td style="padding-right:10px;"><input type="hidden" value="' + email + '" class="form-control contactrow"><input type="text" value="' + email + '" class="form-control" disabled style="width:150px;"/></td>' +
            '<td style="padding-right:10px;"><input type="hidden" value="' + faxno + '" class="form-control contactrow"><input type="text" value="' + faxno + '" class="form-control" disabled style="width:100px;"/></td>' +
            '<td style="padding-right:10px;"><input type="hidden" value="' + deptid + '" class="form-control contactrow"><input type="text" value="' + selecteddept + '" class="form-control" disabled style="width:100px;"/></td>' +
            '<td style="padding-right:10px;"><input type="hidden" value="' + remark + '" class="form-control contactrow"><input type="text" value="' + remark + '" class="form-control" disabled style="width:150px;"/></td>' +
            //'<td style="padding-right:10px;"><p class=""></p></td>' +
            '<td style="padding-right:10px;"><button type="button" id="btnDelete" class="deleteContact btn btn btn-danger btn-xs">Remove</button></td>' +
            '</tr>';
        $('#contactstable').append(contactsdiv);

        $("#contactperson").val("");
        $("#contactpersonmobile").val("");
        $("#contactpersontelephone").val("");
        $("#contactpersonemail").val("");
        $("#contactpersonfaxno").val("");
        $("#contactpersonremark").val("");
    }
});




$(document).on("click", ".deleteContact", function () {
    $(this).closest("tr").remove();// closest used to remove the respective 'tr' in which I have my controls   
});
function addaddressdetail() {
    var rowCount = $("#RowNo").val() == "" ? 0 : $("#RowNo").val();
    var cityid = $('#addresscityid').val();
    var citytext = $("#addresscityid option:selected").text();

    var stateid = $('#addressstateid').val();
    var statetext = $("#addressstateid option:selected").text();

    var countryid = $('#countryid').val();
    var countrytext = $("#countryid option:selected").text();
    var addressstr = $('#address').val();
    var gstno = $('#customergstno').val();
    var gstinformat = new RegExp('^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$');
    var gstno1 = gstno.toUpperCase();

    var unR = $("#customertypeid").val();

    if (addressstr == "") {
        swal("Please enter address")
    }
    else if (countryid == 0 || countryid == "") {
        swal("Please select country");
    }
    else if (stateid == 0 && stateid == "") {
        swal("Please select state")
    }
    else if (cityid == 0 && cityid == "") {
        swal("Please select city")
    }
    else if (unR != 2) {
        if (!gstinformat.test(gstno1)) {
            swal({
                title: "GST No validation",
                text: "Click on OK to enter valid GST No otherwise Reset.",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Reset",
                cancelButtonClass: "btn-danger",
                confirmButtonText: "OK",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (isConfirm) {
                    swal.close();
                }
                else {
                    $("#customergstno").val("");
                }
            })
        }
    }
    else {

        var div = '<tr data=' + rowCount + '><td style="padding-right:10px;" rawdata="' + addressstr + '" class="addressrow"><p>' + addressstr + '</p></td>' +
            '<td style="padding-right:10px;" class="addressrow" rawdata="' + stateid + '"><p>' + statetext + '</p></td>' +
            '<td style="padding-right:10px;" class="addressrow" rawdata="' + cityid + '"><p>' + citytext + '</p></td>' +
            '<td style="padding-right:10px;" class="addressrow" rawdata="' + countryid + '"><p>' + countrytext + '</p></td>' +
            '<td style="padding-right:10px;" class="addressrow" rawdata="' + gstno1 + '"><p>' + gstno1 + '</p></td>' +
            '<td style="padding-right:10px;" class="addressrow CheckRowData" rawdata="0"><input type="checkbox" id="chkDefauldAddId_' + rowCount + '" class="checkDefaultAdd"></td>' +
            '<td><button type="button" id="btnDelete" class="deleterawitem btn btn btn-danger btn-xs">Remove</button></td></tr>'

        $('#contactaddresstable').append(div);

        if (parseInt(rowCount) == 0) {
            debugger;
            var active = $(".contactaddresstablebody").find("tr").attr('data');
            $('.checkDefaultAdd').prop('checked', false);
            $('#chkDefauldAddId_' + active).prop('checked', true);
            var checked = $("#chkDefauldAddId_" + active).is(":checked");
            if (checked == true) {
                $(".CheckRowData").attr('rawdata', '1');
            }
        }



        $('#address').val("");
        $("#customergstno").val("");
        $('#addresscityid').empty();
        $('#addresscityid').val("0");
        $('#addresscityid').trigger("change");
        $('#countryid').val("");
        $('#countryid').trigger("change");
        rowCount = parseInt(rowCount) + 1;
        $("#RowNo").val(rowCount);
    }
}

$(document).on("click", ".deleterawitem", function () {
    var id = $(this).closest("tr").attr('data');
    alert(id);
    $(this).closest("tr").remove(); // closest used to remove the respective 'tr' in which I have my controls

    // $('table#hiderawitemtable tr#' + id).remove();

});

$(document).on("click", ".checkDefaultAdd", function () {
    debugger;
    var active = $(this).closest("tr").attr('data');
    $('.checkDefaultAdd').prop('checked', false);
    $('#chkDefauldAddId_' + active).prop('checked', true);
    var checked = $("#chkDefauldAddId_" + active).is(":checked");
    if (checked == true) {
        $(".CheckRowData").attr('rawdata', '0');
        $(this).closest("tr td").attr('rawdata', '1');
    }

});


