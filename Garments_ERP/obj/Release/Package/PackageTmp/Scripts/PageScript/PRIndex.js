﻿$(function () {
    $("#bs_datepicker_component_container > .ui-datepicker-inline").attr('style', 'display:none;');
    $("#bs_datepicker_component_container1 > .ui-datepicker-inline").attr('style', 'display:none;');

    GetData(2, 'Pending');

    var param = { 'Type': 1, 'attribute': '' };
    $.ajax({
        type: "POST",
        url: '/SupplierPR/StatusCount/',
        data: JSON.stringify(param),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {

            result = JSON.parse(result);
            $.each(result.Table, function (i, data) {
                $("#Pendingcount").text(data.pending);
                $("#InProgresscount").text(data.inprogress);
                $("#Completedcount").text(data.completed);
                $("#Rejectedcount").text(data.Rejected);
            });
        },
        failure: function (response) {
            alert(response.d);
        }
    });
});


$('#bs_datepicker_component_container').datepicker({
    autoclose: true,
    container: '#bs_datepicker_component_container'
});

$('#bs_datepicker_component_container1').datepicker({
    autoclose: true,
    container: '#bs_datepicker_component_container1'
});


function deleterecord(id) {
    var deletionid = id;
    var param = { 'Id': deletionid };

    swal({
        title: 'Confirmation',
        text: 'Are you sure to delete supplier purchase request?',
        showConfirmButton: true,
        showCancelButton: true,
        closeOnConfirm: true,
        closeOnCancel: true,
        confirmButtonText: 'Confirm',
        confirmButtonColor: '#8CD4F5',
        cancelButtonText: 'Cancel'
    },
        function (isConfirm) {

            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: '/SupplierPR/DeletePR/',
                    data: JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result == true) {
                            window.location.reload();
                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });

            } else {
                swal("Cancelled", "Something Wrong!", "error");
            }
        });
}

function Canclequo(id) {
    var quoid = id;
    var param = { 'id': quoid };

    swal({
        title: 'Confirmation',
        text: 'Are You Sure Rejected This Purchase Request?',
        showConfirmButton: true,
        showCancelButton: true,
        closeOnConfirm: true,
        closeOnCancel: true,
        confirmButtonText: 'Confirm',
        confirmButtonColor: '#8CD4F5',
        cancelButtonText: 'Cancel'
    },
        function (isConfirm) {

            if (isConfirm) {

                $.ajax({
                    type: "POST",
                    url: '/SupplierPR/RejectedPR/',
                    data: JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {

                        if (result == true) {
                            swal({
                                title: 'Confirmation',
                                text: 'Record Rejected Successfully!',
                                showConfirmButton: true,
                                closeOnConfirm: true,
                                closeOnCancel: true,
                                confirmButtonText: 'OK',
                                confirmButtonColor: '#8CD4F5',
                            },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        window.location.reload();
                                    }
                                }
                            )

                        }

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });

            } else {
                swal("Cancelled", "Something Wrong", "error");
            }
        });
}


function GetData(Status, Name) {

    $("#Pending").empty();
    $("#Completed").empty();
    $("#InProgress").empty();
    $("#Rejected").empty();

    $("#" + Name).html(loadingHTML());
    $("#" + Name).load("/SupplierPR/GetStatusData/", { 'IsStatus': Status });

}


function loadingHTML() {
    return $("#divLoadingContent").html();
}


function setDateformat(Dt) {

    var dt1 = Dt.substring(6, Dt.length);
    dt1 = dt1.substring(0, dt1.length - 2);
    var today = new Date(parseInt(dt1));
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    // var today2 = today.setMonth(mm + 1);
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    today = mm + '/' + dd + '/' + yyyy;

    return today;
}

function FilterDataShow() {
    debugger;
    var formDate = $("#fromdate").val();
    var toDate = $("#todate").val();
    var Name = $("#Tablist > .active").attr('id');
    var Status = $("#" + Name).attr("datavalue");

    $("#Pending").empty();
    $("#Completed").empty();
    $("#InProgress").empty();
    $("#Rejected").empty();

    $("#" + Name).html(loadingHTML());
    $("#" + Name).load("/SupplierPR/GetFilterStatusData/", { 'fromdate': formDate, 'todate': toDate, 'IsStatus': Status });
}