﻿var ItemArray = [];
var ItemJson = {};
$(document).ready(function () {
    $('.select2').select2();
    $('.select2').width("100%");
    $('.select2').addClass("FW-N");
    var roleid = $("#roleid").val();
    if (parseInt(roleid) != 4) {
        $(".approval").show();
        $(".approvalComment").show();
    }
    else {
        $(".approval").hide();
        $(".approvalComment").hide();
    }


    $("#PR_Date").datepicker({
        dateFormat: 'mm/dd/yy',
        maxDate: 0
    });

    $("#approvaldate").datepicker({
        dateFormat: 'mm/dd/yy',
        maxDate: 0
    });

    $('input[name="requireddate"]').datepicker({
        dateFormat: 'mm/dd/yy',
        minDate: 0
    });

    $("#bs_datepicker_component_container > .ui-datepicker-inline").attr("style", "Display:none;");



    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var today = mm + '/' + dd + '/' + yyyy;
    document.getElementById("PR_Date").value = today;
    $('input[name="requireddate"]').val(today);
    $("#approvaldate").val(today);
});


function GetDateFormateChage(DateString) {
    if (DateString != null) {
        DateString = DateString.substr(0, DateString.lastIndexOf("T"));
        var splitString = DateString.split('-');
        DateString = splitString[1] + "/" + splitString[2] + "/" + splitString[0];
        return DateString;
    }
    else {
        return null;
    }
}


function GetAccountHead() {
    debugger;
    var startrow = 0;
    var startrowvalue = "---Select----";
    var deptid = $("#DepartmentId").val();
    if (deptid != "") {
        $.ajax({ 
            type: "POST",
            url: '/SupplierPR/GetAccountHead/',
            data: "{'deptid':" + deptid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                $("#AccountHeadId").empty();
                $("#AccountHeadId").append('<option value="' + startrow + '">' + startrowvalue + '</option>');
                $.each(result, function (i, bussiness) {
                    $("#AccountHeadId").append('<option value="' + bussiness.UserId + '">' + bussiness.Name + '</option>');
                });
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}




$('#savePR').on('click', function () {

    var PR_Date = $("#PR_Date").val();
    var DepartmentnameId = $("#DepartmentnameId").val();
    var DepartmentId = DepartmentnameId == "" ? 0 : $('#DepartmentId').val();
    var AccountHeadId = $("#AccountHeadId").val();
    var str = "";
    var count = 0;
    var rowcount = 0;
    var rowlenght = ItemArray.length;
    var isvalid = true;

    if (PR_Date == "") {
        swalMsg("Please Select PR Date");
        isvalid = false;
    }
    else if (DepartmentId == "" || DepartmentId == 0) {
        swalMsg("Please Select Department");
        isvalid = false;
    }
    else if (AccountHeadId == "" || AccountHeadId == 0) {
        swalMsg("Please Select Account Head");
        isvalid = false;
    }
    else if (rowlenght > 0) {
        $.each(ItemArray, function (i, data) {
            str += data.ItemSubCatId + '~' + data.ItemId + '~' + data.ItemAttrib + '~' + data.HSNCode + '~' + data.Unitid + '~' + data.ReqQty + '~' + data.FileCount + '#';
        });
        $('#PRItemList').val(str);
    }
    if (str == "") {
        swalMsg("Item Details Required.");
        isvalid = false;
    }
    return isvalid;
});

function swalMsg(errormsg) {
    swal({
        title: 'Error',
        text: errormsg,
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false,
        closeOnCancel: false

    });
}

function setItemUnit1() {
    debugger;
    var itemName = $("#rawitemnameid").val();
    var itemId = itemName == "" ? 0 : $("#itemrawid").val();
    var itemAttrId = itemName == "" ? 0 : $("#itemrawAid").val();
    if (itemName != "") {
        var JonObj = { "itemid": itemId, "IAid": itemAttrId};
        var jsonstr = JSON.stringify(JonObj);
        var type = 1;
        $.ajax({
            type: "POST",
            url: '/SupplierPR/InventoryPR/',
            data: "{'Jobject':'" + jsonstr + "','type':" + type + ",'Attribute':''}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                debugger;
                result = JSON.parse(result);
                debugger;
                $.each(result.root, function (i, item) {
                    debugger;
                    $('#unitid').val(item.UnitId);
                    $('#unitid').select2().trigger('change');
                    $("#availinstockid").val(item.AvailQty);
                    $("#availstockid").text(item.AvailQty);
                    $("#hsncodeid").val(item.HSNCode);
                    $("#hsncode").text(item.HSNCode);
                });
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    else {
        $('#unitid').val(0).trigger('change');
    }
}


function AddItem() {
    var itemsubname = $("#itemsubcategoryidNameid").val();
    var itemsubid = itemsubname == "" ? 0 : $("#itemsubcategoryid").val();

    var itemName = $("#rawitemnameid").val();
    var itemId = itemName == "" ? 0 : $("#itemrawid").val();
    var itemAttrId = itemName == "" ? 0 : $("#itemrawAid").val();

    var HSNCode = $("#hsncodeid").val();
    var Unitid = $("#unitid").val();
    var UnitName = $("#unitid option:selected").text();

    var availstock = $("#availinstockid").val();

    var ReqQty = $("#Reqqtyid").val();

    if (itemsubid == "" || parseInt(itemsubid) == 0) {
        swalMsg("Please Select Item Sub Category!");
    }
    else if (itemId == "" || parseInt(itemId) == 0) {
        swalMsg("Please Select Item Name!");
    }
    else if (Unitid == "") {
        swalMsg("Please Select Item Unit!");
    }
    else if (parseFloat(ReqQty) == 0) {
        swalMsg("Please Enter Req. Qty!");
    }
    else {
        debugger;
        
        var file1 = document.querySelector('#rawimg');
        var fcount = file1.files.length;
        var arraylen = ItemArray.length;
        if (arraylen > 0) {
            var status = false;
            $.each(ItemArray, function (i, data) {
                if (data.ItemId == itemId) {
                    swalMsg("This " + data.ItemName + " already Available in Row!");
                    status = true;
                }
            })
            if (status == false) {
                ItemJson = { 'ItemSubCatName': itemsubname, 'ItemSubCatId': itemsubid, 'ItemName': itemName, 'ItemId': itemId, 'ItemAttrib': itemAttrId, 'HSNCode': HSNCode, 'UnitName': UnitName, 'Unitid': Unitid, 'ReqQty': ReqQty, 'availstock': availstock, 'Files': file1.files, 'FileCount': fcount };
                ItemArray.push(ItemJson);
            }
        }
        else {
            ItemJson = { 'ItemSubCatName': itemsubname, 'ItemSubCatId': itemsubid, 'ItemName': itemName, 'ItemId': itemId, 'ItemAttrib': itemAttrId, 'HSNCode': HSNCode, 'UnitName': UnitName, 'Unitid': Unitid, 'ReqQty': ReqQty, 'availstock': availstock, 'Files': file1.files, 'FileCount': fcount};
            ItemArray.push(ItemJson);
        }
        setValue(ItemArray);
    }
}

function setValue(ItemArray) {
    $("#rawitemtable > tbody").empty();
    $.each(ItemArray, function (i, data) {
        var str = "<tr><td>" + data.ItemSubCatName + "</td><td>" + data.ItemName + "</td><td>" + data.HSNCode + "</td><td>" + data.UnitName + "</td><td>" + data.ReqQty + "</td><td>" + data.availstock + "</td><td>";
        str = str + "<input type='file' class='form-control' name='rawitemimg' id='rawitemimg" + i + "' style='display:none;'><button type='button' class='btn btn-info' style='margin-top:0em;' id='Rviewimage" + i + "' onclick=GetImageUpload(this.id,'preview','RawItem')><i class='fa fa-eye'></i></button></td>";
        str = str + "<td><button type='button' class='btn btn-danger' style='margin-top:0em;padding: 1px 4px 1px 5px;' onclick='removetr(" + i + ")'><i class='fa fa-close'></i></button></td></tr>";
        $("#rawitemtable > tbody").append(str);

        var file1 = document.querySelector('#rawitemimg' + i);
        file1.files = data.Files;

    });
    clearItem();
}
function clearItem() {
    $("#itemsubcategoryidNameid").val("");
    $("#itemsubcategoryid").val("");

    $("#rawitemnameid").val("");
    $("#itemrawid").val("");
    $("#itemrawAid").val("");

    $("#hsncodeid").val("");
    $("#unitid").val("Select Unit");
    $("#unitid").select2().trigger('change');
    $("#availinstockid").val("");
    $("#availstockid").text("");
    $("#Reqqtyid").val("");
    var file1 = document.querySelector('#rawimg');
    var emptyFile = document.createElement('input');
    emptyFile.type = 'file';
    file1.files = emptyFile.files;
}

function removetr(index) {
    ItemArray.splice(index, 1);
    setValue(ItemArray);
}