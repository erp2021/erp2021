﻿


//function to set GST No
function setGSTno() {
        var statecode = $('#statecodehidden').val();
        //var gst = $('#customergstno').val();
    var panno = $('#LedgerCSTNo').val();
        var gstno = statecode + panno;
        $('#customergstno').val(gstno);
   
}


function checkGSTNo() {
    var gstno = $('#customergstno').val();
    var gstinformat = new RegExp('^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$');
    if (gstinformat.test(gstno)) {

    }
    else {
        swal({
            title: "GST No validation",
            text: "Click on OK to enter valid GST No otherwise Reset.",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Reset",
            cancelButtonClass: "btn-danger",
            confirmButtonText: "OK",
            closeOnConfirm: false
        }, function (isConfirm) {
            if (isConfirm) {
                swal.close();
            }
            else {
                $("#customergstno").val("");
            }
        })
    }
}

//function setGSTtype() {
//    var regtype = $('#customertypeid').val();
//    if (regtype == "1") {
//        $('#gstno').show();
//    }
//    else {
//        $('#gstno').hide();
//        $('#customergstno').val("");
//    }
//}


$('#saveLedger').on('click', function () {

    

    var Ledgername = $("#Ledger_Name").val();
    var customertypeid = $("#customertypeid").val();
    var deprt = $("#contactpersondepartmentid").val();
    var panno = $("#PANno").val();

    var str = "";
    var addressstr = "";
    var combrnchtr = "";
    var count = 0;
    var rowcount = 0;
    var addrowcount = 0;
    var CBrowcount = 0;
    var isFirstItem = 0;

    var $dataRows_address = $("#billdetailstable tr:not('.titlerow')");
    var addressrowlenght = $dataRows_address.length;

    var $dataRows_combranch = $("#companyMapTab tr:not('.titlerow')");
    var combrnchrowlenght = $dataRows_combranch.length;
    debugger;
    if (parseInt(combrnchrowlenght) > 0) {
        $dataRows_combranch.each(function () {
            debugger;
            count = 0;
            CBrowcount++;
            $(this).find('.combranchval').each(function (i) {
                debugger;
                var CBlist = ($(this).attr("rawdata"));
                count++;

                if (CBlist != undefined) {

                    if (count == 3) {
                        combrnchtr += CBlist;
                    }
                    else {
                        combrnchtr += CBlist + "~";
                    }
                }

            });

            if (CBrowcount == combrnchrowlenght) {
            }
            else {
                combrnchtr += "#";
            }

        });

        //  alert(addressstr);
        $('#combranchList').val(combrnchtr);
    }



    var isvalid = true;
    //validations of customer details 
    if (Ledgername == "") {
        Getvalidate("Please Enter Ledger Name", "error", "Error");
        isvalid = false;
    }
    else if (customertypeid == "" || customertypeid == 0) {
        Getvalidate("Please Select Ledger Type", "error", "Error");
        isvalid = false;
    }
    else if (deprt == "" || deprt =="---Select Department---") {
        Getvalidate("Please Select Department", "error", "Error");
        isvalid = false;
    }
    else if (panno == "") {
        Getvalidate("Please Enter PAN No", "error", "Error");
        isvalid = false;
    }
    else if (parseInt(addressrowlenght) > 0) {

        $dataRows_address.each(function () {
            count = 0;
            addrowcount++;
            $(this).find('.addressrow').each(function (i) {
                debugger;
                var addresslist = ($(this).attr("rawdata"));
                count++;

                if (addresslist != undefined) {

                    if (count == 18) {
                        addressstr += addresslist;
                    }
                    else {
                        addressstr += addresslist + "~";
                    }
                }

            });

            if (addrowcount == addressrowlenght) {
            }
            else {
                addressstr += "#";
            }

        });

        //  alert(addressstr);
        $('#AddressList').val(addressstr);

    }
    



    return isvalid;
});



$(document).on("click", "#addcontacts", function () {
    var Billingid = $("#billid").val() == "" ? 0 : $("#billid").val();
    var BillingName = $("#Billingperson").val();
    var Unit = $("#BillingUnit").val();
    var mobno = $("#contactpersonmobile").val();
    var telpno = $("#contactpersontelephone").val();
    var email = $("#contactpersonemail").val();
    var fax = $("#contactpersonfaxno").val();
    var gstno = $("#customergstno").val();
    var add = $("#address").val();
    var countid = $("#countryid").val();
    var countname = $("#countryid option:selected").text();
    var stateid = $("#addressstateid").val();
    var statename = $("#addressstateid option:selected").text();
    var cityid = $("#addresscityid").val();
    var cityname = $("#addresscityid option:selected").text();
    var custbank = $("#customerbank").val();
    var accno = $("#customeraccountno").val();
    var bankcode = $("#customerbankcode").val();
    var sizid = "", sizname = "";

    //Rahul ,03-01-2020(Added branch and company in dynamic table grid)
    var Company = $("#companyname").val();
    var Branch = $("#branchname").val();
    if ($("#sezid").prop("checked") == true) {
        sizid = 1;
        sizname = 'Yes';
    }
    else {
        sizid = 0;
        sizname = 'No';
    }
    //sizid = 1;
    
    var rowCount = $('.data-contact-person').length + 1;

    if (BillingName == "") {
        Getvalidate("Enter Billing Name", 'error', 'Error');
    }
    else if (mobno == "") {
        Getvalidate("Enter Mobile No", 'error', 'Error');
    }
    else if (add == "") {
        Getvalidate("Enter Valid Address", 'error', 'Error');
    }
    else {
        contactsdiv = '<tr class="data-contact-person odd bg-info">' +
            '<td class="addressrow" rawdata="' + Billingid + '" style="display:none;">0</td>'
            + '<td class="addressrow" rawdata="' + BillingName + '">' + BillingName + '</td>'
            + '<td class="addressrow" rawdata="' + Unit + '">' + Unit + '</td>'
            + '<td class="addressrow" rawdata="' + mobno + '">' + mobno + '</td>'
            + '<td class="addressrow" rawdata="' + telpno + '">' + telpno + '</td>'
            + '<td class="addressrow" rawdata="' + email + '">' + email + '</td>'
            + '<td class="addressrow" rawdata="' + fax + '">' + fax + '</td>'
            + '<td class="addressrow" rawdata="' + add + '">' + add + '</td>'
            + '<td class="addressrow" rawdata="' + cityid + '">' + cityname + '</td>'
            + '<td class="addressrow" rawdata="' + stateid + '">' + statename + '</td>'
            + '<td class="addressrow" rawdata="' + countid + '">' + countname + '</td>'
            + '<td class="addressrow" rawdata="' + gstno + '">' + gstno + '</td>'
            + '<td class="addressrow" rawdata="' + sizid + '">' + sizid + '</td>'
            + '<td class="addressrow" rawdata="' + custbank + '">' + custbank + '</td>'
            + '<td class="addressrow" rawdata="' + accno + '">' + accno + '</td>'
            + '<td  class="addressrow" rawdata="' + bankcode + '">' + bankcode + '</td>'
             + '<td  class="addressrow" rawdata="' + Company + '">' + Company + '</td>'
              + '<td  class="addressrow" rawdata="' + Branch + '">' + Branch + '</td>'
            + '<td  class="addressrow" rawdata="1" style="display:none;"></td>'
            + '<td><i class="fa fa-edit edit"></i><i class="fa fa-trash delete"></i></td>'
            + '</tr>';

        $('#billdetailstable').append(contactsdiv);
        clearAll();

    }
});


$(document).on("click", ".edit", function () {
    var Billarray = [];
    $(this).closest("tr").children('td').each(function (i) {
        var Aitemlist = ($(this).attr('rawdata'));
        Billarray.push(Aitemlist);
    });
    debugger;
    $("#billid").val(Billarray[0]);
    $("#Billingperson").val(Billarray[1]);
    $("#BillingUnit").val(Billarray[2]);
    $("#contactpersonmobile").val(Billarray[3]);
    $("#contactpersontelephone").val(Billarray[4]);
    $("#contactpersonemail").val(Billarray[5]);
    $("#contactpersonfaxno").val(Billarray[6]);
    $("#address").val(Billarray[7]);
    $("#countryid").val(Billarray[10]);
    GetState_bind(Billarray[9], Billarray[8]);
    
    $("#customergstno").val(Billarray[11]);
    $("#customerbank").val(Billarray[13]);
    $("#customeraccountno").val(Billarray[14]);
    $("#customerbankcode").val(Billarray[15]);
    $("#companyname").val(Billarray[16]);
    $("#branchname").val(Billarray[17]);
    if (Billarray[12] == "1") {
        $("#sezid").prop("checked", true);
    }
    else {
        $("#sezid").prop("checked", false);
    }
    
    $(this).closest("tr").remove();
});

function GetState_bind(st,ct) {
    debugger;
    $("#addressstateid").empty();
    var countryid = $("#countryid").val();
    if (countryid != "0" && countryid != "") {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/Customer/Getstate/',
            dataType: "json",
            data: "{'countryid':" + countryid + "}",
            success: function (states) {
                debugger;
                $.each(states, function (i, item) {
                    $("#addressstateid").append('<option value="' + item.ID + '">' + item.Name + '</option>');

                });
                $("#addressstateid").val(st);
                GetCity_bind(ct);
            },
            error: function (result) {
                alert("Error" + result.status);
            }
        });
    }
}


function GetCity_bind(no) {
    debugger;
    $("#addresscityid").empty();
    var stateid = $("#addressstateid").val();
    if (stateid != "" && stateid != "0") {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/Supplier/getCityNameList/',
            dataType: "json",
            data: "{'stateid':" + stateid + "}",
            success: function (citys) {
                debugger;
                $.each(citys, function (i, city) {
                    $("#addresscityid").append('<option value="' + city.ID + '">' + city.Name + '</option>');

                });
                $("#addresscityid").val(no);
            },
            error: function (result) {
                alert("Error" + result.status);
            }
        });
    }
}




function clearAll() {
    $("#billid").val("");
    $("#Billingperson").val("");
    $("#BillingUnit").val("");
    $("#contactpersonmobile").val("");
    $("#contactpersontelephone").val("");
    $("#contactpersonemail").val("");
    $("#contactpersonfaxno").val("");
    $("#customergstno").val("");
    $("#address").val("");
    $("#customerbank").val("");
    $("#customeraccountno").val("");
    $("#customerbankcode").val("");
    $("#branchname").val("");
    $("#companyname").val("");
}



function Getvalidate(msg, _type, _title) {
    swal({
        title: _title,
        text: msg,
        type: _type,
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false,
        closeOnCancel: false

    });
}




$(document).on("click", ".delete", function () {
    $(this).closest("tr").remove();// closest used to remove the respective 'tr' in which I have my controls   
    $(this).closest('tr').find('td:eq(16)').attr('rawdata', '0');
});

$(document).on("click", ".deleterawitem", function () {
    var id = $(this).closest("tr").attr('data');
    alert(id);
    $(this).closest("tr").remove(); // closest used to remove the respective 'tr' in which I have my controls

    // $('table#hiderawitemtable tr#' + id).remove();

});

$(document).on("click", ".checkDefaultAdd", function () {
    debugger;
    var active = $(this).closest("tr").attr('data');
    $('.checkDefaultAdd').prop('checked', false);
    $('#chkDefauldAddId_' + active).prop('checked', true);
    var checked = $("#chkDefauldAddId_" + active).is(":checked");
    if (checked == true) {
        $(".CheckRowData").attr('rawdata', '0');
        $(this).closest("tr td").attr('rawdata', '1');
    }

});


