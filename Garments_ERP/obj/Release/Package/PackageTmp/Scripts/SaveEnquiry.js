﻿


$('#hidestyletable').hide();
$('#hiderawitemtable').hide();


function Editstylesizedetail(id) {
    //alert("in editstylesize" + id);
    var number = id.match(/\d+/);
    var total = 0;
    var sizeper_ele = $('#sizeper' + number).val();
    var sizepcs_ele = $('#sizepcs' + number).val();
    var reqty = $('#reqqty').val();
    if (reqty == "") {
        // $('#qtymsg').text();
        swal("enter require qty.");

    }
    else {
        $('#qtymsg').text("");
        if (sizepcs_ele != "") {
            var sizeper = (parseFloat(sizepcs_ele) * 100) / parseFloat(reqty);
            $('#sizeper' + number).val(parseFloat(sizeper).toFixed(2));

            $('#pertd' + number).attr("sizedata", parseFloat(sizeper).toFixed(2));
            $('#pcstd' + number).attr("sizedata", parseFloat(sizepcs_ele).toFixed(2));
            // $('#sizeper' + number).attr('sizedata') = parseFloat(sizeper).toFixed(2);
        }
        else {

            var sizepcs = (parseFloat(sizeper_ele) * parseFloat(reqty) / 100);
            $('#sizepcs' + number).val(parseFloat(sizepcs).toFixed(2));

            $('#pcstd' + number).attr("sizedata", parseFloat(sizepcs).toFixed(2));
            $('#pertd' + number).attr("sizedata", parseFloat(sizeper_ele).toFixed(2));
            //  $('#sizepcs' + number).attr('sizedata') = parseFloat(sizepcs).toFixed(2);
        }
    }
}

function editsizeper(id) {
    //alert("in editstylesize" + id);
    var number = id.match(/\d+/);
    var total = 0;
    var sizeper_ele = $('#sizeper' + number).val();
    var sizepcs_ele = $('#sizepcs' + number).val();
    var reqty = $('#reqqty').val();
    if (reqty == "") {
        // $('#qtymsg').text();
        swal("enter require qty.");

    }

    else {
        $('#qtymsg').text("");
        if (sizeper_ele != "") {
            if (parseFloat(sizeper_ele) <= 100) {
                var sizepcs = (parseFloat(sizeper_ele) * parseFloat(reqty) / 100);
                $('#sizepcs' + number).val(parseFloat(sizepcs).toFixed(2));

                $('#pcstd' + number).attr("sizedata", parseFloat(sizepcs).toFixed(2));
                $('#pertd' + number).attr("sizedata", parseFloat(sizeper_ele).toFixed(2));
            }
            else {
                swal("enter valid percentage");
            }
        }
        // $('#sizepcs' + number).attr('sizedata') = parseFloat(sizepcs).toFixed(2);
        //}
    }
}

function editsizepcs(id) {
    var number = id.match(/\d+/);
    var total = 0;
    var sizeper_ele = $('#sizeper' + number).val();
    var sizepcs_ele = $('#sizepcs' + number).val();
    var reqty = $('#reqqty').val();
    if (reqty == "") {
        // $('#qtymsg').text();
        swal("enter require qty.");

    }
    else {
        // $('#qtymsg').text("");
        if (sizepcs_ele != "") {

            if (parseFloat(sizepcs_ele) <= parseInt(reqty)) {
                var sizeper = (parseFloat(sizepcs_ele) * 100) / parseFloat(reqty);
                $('#sizeper' + number).val(parseFloat(sizeper).toFixed(2));

                $('#pertd' + number).attr("sizedata", parseFloat(sizeper).toFixed(2));
                $('#pcstd' + number).attr("sizedata", parseFloat(sizepcs_ele).toFixed(2));
            }
            else {
                swal("enter valid quantity");
            }
        }

    }

}



$("#sampleyes").on("change", function () {
    //  var value = $("#Non-Taxable").val();
    if ($('#sampleyes').prop("checked") == true) {
        $("issampling").val("yes");
    }
});
$("#sampleno").on("change", function () {
    //  var value = $("#Non-Taxable").val();
    if ($('#sampleno').prop("checked") == true) {
        $("issampling").val("no");
    }
});

$(document).ready(function () {
    
    $("#enquirydate").datepicker({
        //setDate: currentDate,
        format: 'mm/dd/yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function (dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
        },
        maxDate: 0

    });

    $("#approvaldate").datepicker({
        //setDate: currentDate,
        format: 'mm/dd/yy'
        // maxDate: 0
    });
    $("#tentivedate").datepicker({
        //setDate: currentDate,
        format: 'mm/dd/yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        onClose: function (dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
        }

        //maxDate: 0
    });

    var tempdate = new Date();
    var today = new Date();

    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    // var today2 = today.setMonth(mm + 1);
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    today = mm+ '/' + dd  + '/' + yyyy;

    var actualDate = new Date();
    var nextMonth = new Date(actualDate.getFullYear(), actualDate.getMonth() + 1, actualDate.getDate());
    //alert(nextMonth);


    dd = nextMonth.getDate();
    mm = nextMonth.getMonth() + 1;
    yyyy = nextMonth.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var nexttentivedate = mm+ '/' + dd + '/' + yyyy;

    $("#enquirydate").val(today);
    $("#approvaldate").val(today);
    $("#tentivedate").val(nexttentivedate);

    // var tentivedate = new Date($('#enquirydate').val());



});




$(document).on("click", ".deletestyle", function () {
    var id = $(this).closest("tr").attr('data');
    // alert(id);
    $(this).closest("tr").remove(); // closest used to remove the respective 'tr' in which I have my controls
    // $('#hidestyletable tr').find('#' + id).remove();
    $('table#hidestyletable tr#' + id).remove();

});
$(document).on("click", ".deleterawitem", function () {
    var id = $(this).closest("tr").attr('data');
    // alert(id);
    $(this).closest("tr").remove(); // closest used to remove the respective 'tr' in which I have my controls
    //  $('#hiderawitemtable tr').find('#' + id).remove();
    $('table#hiderawitemtable tr#' + id).remove();

});


function validrawitem() {
    var valid = true;

    var itemcategoryid = $("#itemcategoryid").val();
    var itemsubcategoryid = $("#itemsubcategory").val();


    $('#rawitemtable tr').each(function () {

        if ($(this).find('.validitemcatrow').val() == itemcategoryid && $(this).find('.validitemsubrow').val() == itemsubcategoryid) {
            valid = false;
        }
    });

    return valid;
}



$(document).on("click", ".addrawitem", function () {

    //var itemcategory = document.getElementById("itemcategoryid");
    //var selecteditemcategory = itemcategory.options[itemcategory.selectedIndex].text;
    //var itemcategoryid = $("#itemcategoryid").val();

    var selecteditemcategory = $("#itemcatNameid").val();
    var itemcategoryid = $("#itemcategoryid").val();

    var itemsubcategoryid = $("#itemsubcategory").val();
    var selecteditemsubcategory = $("#itemsubcategoryNameid").val();
    
    var itemdesc_ = $('#itemdesc').val();


    
    var supplier = $("#supplier").val();
    
    var i = $('#rawitemtable tr').length;
    var rowCount = 0;

    
    // alert(itemcategoryid);
    //alert(itemsubcategoryid);
    //validation code
    if (itemcategoryid == "") {
        // $('#itemcategorymsg').text("select item category");
        swal({
            title: 'Error',
            text: "Please Select Item Category.",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        //alert(itemcategoryid);
    }
    else if (itemsubcategoryid == "") {
        // $('#itemsubcategorymsg').text("select itemsubcategory");
        swal({
            title: 'Error',
            text: "Please Select Item Subcategory.",
            type: "error",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            closeOnCancel: false

        });
        //  alert(itemsubcategoryid);
    }
    else if (validrawitem() == false) {
        swal("selected item category or item sub category already added.");
    }
        // else if (rawitemid == "") {
        //$('#rawitemmsg').text("select rawitem");
        //     swal("select rawitem");
        // alert(rawitemid);
        // }
    else {
      
        $('#itemsubcategorymsg').text("");
        $('#itemcategorymsg').text("");


        $("#itemcatNameid").val("");
        $("#itemsubcategoryNameid").val("");
        $("#itemsubcategoryNameid").val("");

        //itemcate subcategory item supploer
        var file1 = document.querySelector('#rawimg');
        var fcount = file1.files.length;
        var len1 = $("#rawitemtable tr").length;
        len1 = len1 + 1;
        
        
        rawdiv = '<tr class="data-contact-person odd" id="' + (i + 1) + '" value="' + (i + 1) + '" data="' + (i + 1) + '">' +
            '<td style="padding-right:10px;"><input type="hidden" value="' + itemcategoryid + '" class="form-control validitemcatrow"><input type="text" name="Item' + rowCount + '" id="Itemid' + rowCount + '" value="' + selecteditemcategory + '" class="form-control" disabled style="display:none"/>' + selecteditemcategory + '</td>' +
            '<td style="padding-right:10px;"><input type="hidden" value="' + itemsubcategoryid + '" class="form-control validitemsubrow"><p class="">' + selecteditemsubcategory + '</p></td>' +
            '<td style="padding-right:10px;"><input type="hidden" value="' + itemdesc_ + '" class="form-control "><p class="">' + itemdesc_ + '</p></td>' +
            '<td style="padding-right:10px;"><p class="">' + supplier + '</p></td><td style="padding-right:10px;">';
        if (parseInt(fcount) > 0) {
            rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="View Image" id="Rviewimage' + len1 + '"  onclick=GetImageUpload(this.id,"preview","RawItem") /><input type="button" class="btn btn-info" id="Ruploadimage' + len1 + '" value="Upload Image" style="display: none;" onclick=GetImageUpload(this.id,"upload","RawItem") />';
        }
        else {
            rawdiv = rawdiv + '<input type="button" class="btn btn-info" value="View Image" id="Rviewimage' + len1 + '" style="display: none;"  onclick=GetImageUpload(this.id,"preview","RawItem") /><input type="button" class="btn btn-info" id="Ruploadimage' + len1 + '" value="Upload Image"  onclick=GetImageUpload(this.id,"upload","RawItem") />';
        }
        rawdiv = rawdiv + '<input type="file" style="display:none;" id="rawitemimg' + len1 + '" name="rawitemimg" class="form-control" multiple /></td>';
        rawdiv = rawdiv + '<td><button type="button" id="btnDelete" class="deleterawitem btn btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></button></td>' +
              '</tr>';
        $('#rawitemtable').append(rawdiv);

            if (parseInt(fcount) > 0) {
                var file2 = document.querySelector('#rawitemimg' + len1);
                file2.files = file1.files;
                var emptyFile = document.createElement('input');
                emptyFile.type = 'file';
                file1.files = emptyFile.files;
            }

        hiderawdiv = '<tr class="data-contact-person odd" id="' + (i + 1) + '" value="' + (i + 1) + '" data="' + (i + 1) + '">' +
            '<td style="padding-right:10px;"><input type="hidden" value="' + itemcategoryid + '" class="form-control rawitemrow"></td>' +
            '<td style="padding-right:10px;"><input type="hidden" value="' + itemsubcategoryid + '" class="form-control rawitemrow"></td>' +
            '<td style="padding-right:10px;"><input type="hidden" value="' + itemdesc_ + '" class="form-control rawitemrow"></td>' +
            '<td style="padding-right:10px;"><input type="hidden" value="' + supplier + '" class="form-control rawitemrow"></td>' +
            '<td style="padding-right:10px;"><input type="hidden" value="' + fcount + '" class="form-control rawitemrow" id="rawitemcount' + len1 + '"></td>' +
              '<td style="padding-right:10px;"><button type="button" id="btnDelete" class="deleterawitem btn btn btn-danger btn-xs">Remove</button></td>' +
          '</tr>';

        $('#hiderawitemtable').append(hiderawdiv);
        //for clear the fields
        $('#itemcategoryid').val("0");
        $('#itemsubcategory').val("0");
        $("#supplier").val("");
        $("#supplierNameid").val("");
        $('#itemdesc').val("");






    }
});

//$(function () {
//    var supplier = ["Baseball", "Tennis", "Golf", "Cricket", "Football", "Hockey", "Badminton", "Volleyball", "Boxing", "Kabaddi", "Chess", "Long Jump", "High Jump", "Racing", "Handball", "Swimming", "Wrestling"];
//    $("#supplier").autocomplete({
//        source: supplier
//    });
//});


function ShowEntity(no) {
    debugger;
    $("#EntityModal").modal('toggle');
    $("#showEntno").val(no);
    $("#customertypeid option:selected").removeAttr("selected");
    $("#customertypeid option[value='" + no + "']").prop("selected", true).change();
}


$("#SaveEntity").on('click', function () {
    var Ledgername = $("#Ledger_Name").val();
    var customertypeid = $("#customertypeid").val();
    var mobno = $("#contactpersonmobile").val();
    var add = $("#address").val();
    var email = $("#contactpersonemail").val();
    if (Ledgername == "") {
        Getvalidate("Please Enter Ledger Name", "error", "Error");
        isvalid = false;
    }
    else if (customertypeid == "" || customertypeid == 0) {
        Getvalidate("Please Select Ledger Type", "error", "Error");
        isvalid = false;
    }
    else if (mobno == "") {
        Getvalidate("Enter Mobile No", 'error', 'Error');
    }
    else if (add == "") {
        Getvalidate("Enter Valid Address", 'error', 'Error');
    } else {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '/CustomerEnquiry/SaveEntity/',
            dataType: "json",
            data: "{'LedgerType':'" + customertypeid + "','Ledgername':'" + Ledgername + "','mobno':'" + mobno + "','add':'" + add + "','email':'" + email + "'}",
            success: function (result) {
                if (result != null) {
                    var no = $("#showEntno").val();
                    if (parseInt(no) == 1) {
                        $("#custnameid").val(result[0].Ledger_Name);
                        $("#customerid").val(result[0].Ledger_Id);

                        if (result.length == 2) {
                            $('#customeraddress').empty();
                            $("#customeraddress").append('<option value="0">Select</option>');
                            $.each(result[1], function (i, data) {
                                $("#customeraddress").append('<option value="' + data.Id + '">' + data.Address + '</option>');
                            });
                            $("#customeraddress").val($("#customeraddress option:eq(1)").val());
                        }
                    }
                    else if (parseInt(no) == 2) {
                        $("#supplierNameid").val(result[0].Ledger_Name);
                        $("#supplier").val(result[0].Ledger_Name);
                    }
                    else {
                        $("#BrokernameId").val(result[0].Ledger_Name);
                        $("#BrokerId").val(result[0].Ledger_Id);
                        getBrokerDetail();
                    }
                    $("#EntityModal").modal('hide');

                }
            },
            error: function (result) {
                Getvalidate("Error" + result.status, 'error', 'Error');
            }
        });
    }

});




function getBrokerDetail() {
    $('#BrokerPhone').val('');
    var brokerid = $('#BrokerId').val();
    if (brokerid != 0) {

        $.ajax({
            type: "POST",
            url: '/CustomerEnquiry/getaddress/',
            data: "{'custid':" + brokerid + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                result = JSON.parse(result);
                $.each(result, function (i, item) {
                    if (i == 0) {
                        $('#BrokerPhone').val(item.Contact_No);
                    }
                });

            },
            failure: function (response) {
                alert(response.d);
            }
        });

    }

}


function Getvalidate(msg, _type, _title) {
    swal({
        title: _title,
        text: msg,
        type: _type,
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false,
        closeOnCancel: false

    });
}

