﻿
$(document).ready(function () {
    $('.select2').select2();
    $('.select2').width("100%");
});

$('#saveWorkCenter').on('click', function () {
    var WorkCenterName = $("#WorkCenterName").val();
    var DepartmentId = $('#DepartmentId').val();
    var WorkCenterLocation = $("#WorkCenterLocation").val();
    var isvalid = true;
    //validations of Work Center details 
    if (WorkCenterName == "") {
        swal("Please enter work center name");
        isvalid = false;
    }
    else if (DepartmentId == "" || DepartmentId == 0) {
        swal("Please select department");
        isvalid = false;
    }
    else if (WorkCenterLocation == "") {
        swal("Please enter work center location");
        isvalid = false;
    }

    return isvalid;
});