﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class RoleController : Controller
    {
        //
        // GET: /Role/
        RoleService RoleSer = ServiceFactory.GetInstance().RoleService;

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Role";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var screenLst = RoleSer.get();
            return View(screenLst);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Role";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            return View();
        }

        [RBAC]
        public JsonResult ActiveDeactive(int id, string Status)
        {
            var status = RoleSer.ActiveDeactive(id, Status);
            return Json(status);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(FormCollection frm)
        {
            try
            {
                M_RoleEntity roleent = new M_RoleEntity();
                roleent.roleName = Convert.ToString(frm["RoleName"]);
                roleent.Discription = Convert.ToString(frm["RoleDis"]);
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                roleent.CreatedBy = Convert.ToInt32(collection.Id);
                roleent.ParentId = 0;
                long id = RoleSer.Insert(roleent);
                if (id > 0)
                {
                    TempData["alertmsg"] = "Record Save Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertmsg"] = "Error";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");
            }
        }

        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "Role";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            M_RoleEntity entity = RoleSer.get().Where(x => x.roleId == id).FirstOrDefault();
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            try
            {
                M_RoleEntity Screenentity = new M_RoleEntity();
                Screenentity.roleId = Convert.ToInt64(frm["Roleid"]);
                Screenentity.roleName = Convert.ToString(frm["RoleName"]);
                Screenentity.Discription = Convert.ToString(frm["RoleDis"]);
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                Screenentity.UpdateBy = Convert.ToInt32(collection.Id);

                bool id = RoleSer.Update(Screenentity);
                if (id == true)
                {
                    TempData["alertmsg"] = "Record Update Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertmsg"] = "Error";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");
            }
        }
    }
}
