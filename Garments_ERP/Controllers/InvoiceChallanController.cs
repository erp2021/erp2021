﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Garments_ERP.Controllers
{
    public class InvoiceChallanController : SuperController
    {
        //
        // GET: /InvoiceChallan/
        CustomerPOService customerpo = ServiceFactory.GetInstance().CustomerPOService;
        PaymentTermService paymenttermservice = ServiceFactory.GetInstance().PaymentTermService;
        InvoiceChallanService invoicechallanservice = ServiceFactory.GetInstance().InvoiceChallanService;

        [RBAC]
        public ActionResult Index()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var list = invoicechallanservice.Get();
            return View(list);
        }

        [RBAC]
        public ActionResult Create()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            T_Invoice_ChallanEntity entity = new T_Invoice_ChallanEntity();
            string[] InvoiceNo = invoicechallanservice.getnextInvoiceNo().Split('-');
            entity.InvoiceNo = InvoiceNo[0] + "-" + InvoiceNo[1] + "-" + InvoiceNo[2] + "-" + InvoiceNo[3] + "-XXXX";
            string[] ChallanNo = invoicechallanservice.getnextInvoiceNo().Split('-');
            entity.ChallanNo =   "DCH-" + ChallanNo[1] + "-" + ChallanNo[2] + "-" + ChallanNo[3] + "-XXXX";

            ViewBag.paymenttermlist = DropDownList<M_PaymentTermMasterEntity>.LoadItems(paymenttermservice.get(), "Id", "PaymentTerm", 0);
            ViewBag.OtherCharge = DropDownList<M_POOtherChargesMasterEntity>.LoadItems(customerpo.getOtherCharge(), "id", "POOtherChargeName", 0);
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(FormCollection frm)
        {
            try
            {
                T_Invoice_ChallanEntity entity = new T_Invoice_ChallanEntity();
                string invdatestr = Convert.ToString(frm["InvoiceDate"]);
                DateTime Invdate = DateTime.ParseExact(invdatestr, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.InvoiceDate = Invdate;

                string chalndatestr = Convert.ToString(frm["ChallanDate"]);
                DateTime Challandate = DateTime.ParseExact(chalndatestr, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.ChallanDate = Challandate;
                entity.PO_Id = Convert.ToInt64(frm["porefno"]);
                string podatestr = Convert.ToString(frm["PO_Date"]);
                DateTime POndate = DateTime.ParseExact(podatestr, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.PO_Date = POndate;
                entity.LedgerId = Convert.ToInt64(frm["customerid"]);
                entity.DeliveryAdd = Convert.ToString(frm["deliveryaddid"]);
                entity.Total = Convert.ToDecimal(frm["total"]);
                entity.GST = Convert.ToDecimal(frm["GSTCharge"]);
                entity.GSTAmount = Convert.ToDecimal(frm["GSTamount"]);
                entity.GrossTotal = Convert.ToDecimal(frm["Grandtotal"]);
                entity.PaymentTerm = Convert.ToInt32(frm["PaymentTerm"]);
                string Duodatestr = Convert.ToString(frm["DuoOn"]);
                DateTime Duodate = DateTime.ParseExact(Duodatestr, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.DuoOn = Duodate;
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.CreatedBy = Convert.ToInt32(collection.Id);
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.BranchId = Convert.ToInt32(collection.BranchId);
                string otherchargeStr = Convert.ToString(frm["otherchargestr"]);
                if (otherchargeStr != "")
                {
                    string[] OCSplit = otherchargeStr.Split('#');
                    List<T_POOtherChargeEntity> oclist = new List<T_POOtherChargeEntity>();
                    foreach (var ocdata in OCSplit)
                    {
                        if (ocdata != "")
                        {
                            var data = ocdata.Split('~');
                            T_POOtherChargeEntity ent = new T_POOtherChargeEntity();                           
                                ent.OtherChargeId = Convert.ToInt32(data[0]);
                                ent.OtherChargeValue = Convert.ToDecimal(data[1]);
                                ent.GST = Convert.ToDecimal(data[2]);
                                ent.InventoryModeId = 10009; //Invoice
                                ent.Company_ID = Convert.ToInt32(collection.Company_ID);
                                ent.BranchId = Convert.ToInt32(collection.BranchId);
                                ent.CreatedBy = Convert.ToInt32(collection.Id);
                                oclist.Add(ent);
                           
                        }
                    }
                    entity.POOtherChargeEntity = oclist;
                }


                var grnitemstr = frm["Invitemstr"].ToString();
                string[] itemarr = grnitemstr.Split('#');
                var grnitemPIstr = Convert.ToString(frm["InvitemPIstr"]);
                string[] itemPIarr = grnitemPIstr.Split('|');
                List<T_InvoiceChallan_ProductDetailsEntity> listitem = new List<T_InvoiceChallan_ProductDetailsEntity>();
                int i = 0;
                foreach (var item in itemarr)
                {
                    if (item != "")
                    {
                        T_InvoiceChallan_ProductDetailsEntity itementity = new T_InvoiceChallan_ProductDetailsEntity();
                        string[] grnitem = item.Split('~');
                        itementity.ItemId = Convert.ToInt64(grnitem[1]);
                        itementity.Item_Attribute_ID = Convert.ToInt32(grnitem[2]);
                        itementity.BaleNo = "";
                        itementity.UOM = Convert.ToInt32(grnitem[8]);
                        itementity.HSNNO = Convert.ToString(grnitem[3]);
                        itementity.Qty = Convert.ToDecimal(grnitem[5]);
                        itementity.Rate = Convert.ToDecimal(grnitem[7]);
                        itementity.Total = Convert.ToDecimal(grnitem[9]);
                        itementity.GST = Convert.ToDecimal(grnitem[10]);
                        itementity.CreatedBy = Convert.ToInt32(collection.Id);
                        itementity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        itementity.BranchId = Convert.ToInt32(collection.BranchId);
                        itementity.POQty = Convert.ToDecimal(grnitem[4]);
                        itementity.BalQty = Convert.ToDecimal(grnitem[6]);
                        grnitem[12] = Convert.ToString(grnitem[12]).Substring(0, 1) == "," ? Convert.ToString(grnitem[12]).Substring(1) : grnitem[12];
                        itementity.Batchid = Convert.ToString(grnitem[12]);
                        itementity.itemPIval = itemPIarr[i];
                        listitem.Add(itementity);
                        i++;
                    }
                }
                entity.TInv_ProdEntity = listitem;

                long id = invoicechallanservice.Insert(entity);
                if (id > 0)
                {

                    TempData["alertmsg"] = "Record Save Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";

                    return RedirectToAction("Index");

                }
                else
                {
                    TempData["alertmsg"] = "Record Not Save!";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-danger alert-dismissible";
                return RedirectToAction("Index");

            }
            return RedirectToAction("Index");

        }

        [RBAC]
        public JsonResult getPOListbycustomerid(long id)
        {
            var list = invoicechallanservice.getPOListbycustomerid(id);
            return Json(list);

        }

        [RBAC]
        [HttpPost]
        public JsonResult EditAttributeValue(string MyJson, int type, int id)
        {
            string myjson = "";
            myjson = "[{\"POId\":\"" + id + "\"}]";
            XmlDocument doc = new XmlDocument();
            doc = JsonConvert.DeserializeXmlNode("{\"Row\":" + myjson + "}", "root"); // JSON needs to be an objec
            var entity = customerpo.EditAttributeValue(MyJson, type, doc);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(entity);
            return Json(JSONString);
        }

        [RBAC]
        public JsonResult GetBatachLot(long ItemId,int ItemAttrId,long poid)
        {
            var list = invoicechallanservice.GetBatachLot(ItemId, ItemAttrId, poid);
            return Json(list);
        }

    }
}
