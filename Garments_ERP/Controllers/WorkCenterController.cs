﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class WorkCenterController : SuperController
    {
        //
        // GET: /WorkCenter/

        WorkCenterService workcenterservice = ServiceFactory.GetInstance().WorkCenterService;
        DepartmentService departmentservice = ServiceFactory.GetInstance().DepartmentService;
        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Work Center";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            var list = workcenterservice.get().Where(x=>x.Company_ID== ViewBag.companyid && x.Branch_ID== ViewBag.branchid);
            return View(list);
        }
        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Work Center";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0);
            return View();
        }
      
        [RBAC]
        [HttpPost]
        public ActionResult Create(M_WorkCenterMasterEntity model)
        {
            try
            {
                model.IsActive = true;
                model.CreatedDate = DateTime.Now;

                //New Code Added by Rahul on 09-01-2020
                //model.UpdatedDate = DateTime.Now;
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                model.CreatedBy = Convert.ToInt32(collection.Id);
                model.Company_ID = Convert.ToInt32(collection.Company_ID);
                model.Branch_ID = Convert.ToInt32(collection.BranchId);

                int insertedid = workcenterservice.Insert(model);
                if (insertedid > 0)
                {
                    TempData["alertmsg"] = "Record Save Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = Convert.ToString(ex);
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");

            }
            return RedirectToAction("Index");

        }
        [RBAC]
        public ActionResult Edit(int id)
        {
            ViewBag.title = "Work Center";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            M_WorkCenterMasterEntity model = new M_WorkCenterMasterEntity();
            model = workcenterservice.GetById(id);
            if (model.DepartmentId != null) { ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", (int)model.DepartmentId); }
            else { ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0); }
            return View(model);
        }
     
        [RBAC]
        [HttpPost]
        public ActionResult Edit(M_WorkCenterMasterEntity entity)
        {
            try
            {
                entity.IsActive = true;
                entity.UpdatedDate = DateTime.Now;

                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.UpdatedBy = Convert.ToInt32(collection.Id);
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.Branch_ID = Convert.ToInt32(collection.BranchId);

                bool updatedid = workcenterservice.Update(entity);
                if (updatedid == true)
                {
                    TempData["alertmsg"] = "Record Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = Convert.ToString(ex);
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");

            }
            return RedirectToAction("Index");
            
        }
     
        [RBAC]
        [HttpPost]
        public JsonResult DeleteWorkCenter(int Id)
        {
            bool ISDeleted = false;
            ISDeleted = workcenterservice.Delete(Id);
            return Json(ISDeleted);

        }

        [RBAC]
        public JsonResult CheckWorkCenter(string WorkCenterName, int? Id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            bool value = workcenterservice.CheckWorkCenterExistance(WorkCenterName, Id, companyid, branchid);
            return Json(value, JsonRequestBehavior.AllowGet);
        }
    }
}
