﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using Garments_ERP.Filters;
namespace Garments_ERP.Controllers
{
    public class ItemGroupController : SuperController
    {
        //
        // GET: /ItemGroup/

        ItemGroupService itemgroupservice = ServiceFactory.GetInstance().ItemGroupService;
        //[RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Item Group";
            if (System.Web.HttpContext.Current.Session["LoggedUser"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                ViewBag.roleid = Convert.ToInt32(collection.RoleID);
                ViewBag.deptid = Convert.ToInt32(collection.DeptID);
                ViewBag.userid = Convert.ToInt32(collection.Id);
                ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
                ViewBag.branchid = Convert.ToInt32(collection.BranchId);
                ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
                ViewBag.Username = Convert.ToString(collection.Username);
                var list = itemgroupservice.Get();
                return View(list);
            }
        }
        public JsonResult CheckItemGroup(string Itemgroup, int? Id)
        {
            bool value = itemgroupservice.CheckExistance(Itemgroup, Id);
            return Json(value, JsonRequestBehavior.AllowGet);
        }
        //[RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Item Group";
            if (System.Web.HttpContext.Current.Session["LoggedUser"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                ViewBag.roleid = Convert.ToInt32(collection.RoleID);
                ViewBag.deptid = Convert.ToInt32(collection.DeptID);
                ViewBag.userid = Convert.ToInt32(collection.Id);
                ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
                ViewBag.branchid = Convert.ToInt32(collection.BranchId);
                ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
                ViewBag.Username = Convert.ToString(collection.Username);

                M_ItemGroupEntity entity_ = new M_ItemGroupEntity();
                entity_.itemgrouplist = itemgroupservice.Get();
                return View(entity_);
            }
        }

        [HttpPost]
        public ActionResult Create(M_ItemGroupEntity entity)
        {
            try
            {
                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.Createdby= Convert.ToInt32(collection.Id);
                entity.Createddate = DateTime.Now;
                entity.Company_ID= Convert.ToInt32(collection.Company_ID);
                entity.Branch_ID= Convert.ToInt32(collection.BranchId);

                entity.IsActive = true;
                long id = itemgroupservice.Insert(entity);
                if (id > 0)
                {

                    TempData["alertmsg"] = "Record Save Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");

        }

        public ActionResult update(int id, string itemgroup, string itemgroupdesc)
        {
            try
            {
                M_ItemGroupEntity entity_ = new M_ItemGroupEntity();
                entity_.Id = id;
                entity_.Itemgroup = itemgroup;
                entity_.Itemgroupdesc = itemgroupdesc;
                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity_.Updatedby = Convert.ToInt32(collection.Id);
                entity_.Updateddate = DateTime.Now;
                entity_.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity_.Branch_ID = Convert.ToInt32(collection.BranchId);

                bool updated = itemgroupservice.Update(entity_);
                if (updated == true)
                {
                    M_ItemGroupEntity entity = new M_ItemGroupEntity();
                    entity.itemgrouplist = itemgroupservice.Get();
                    return View(entity);
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
            
        }

        [RBAC]
        public ActionResult Edit(int id)
        {
            ViewBag.title = "Item Group";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            M_ItemGroupEntity entity = itemgroupservice.Get().Where(x => x.Id == id).FirstOrDefault();
            return View(entity);
        }

        [HttpPost]
        public ActionResult Edit(M_ItemGroupEntity entity)
        {
            try
            {
                bool isupdate = itemgroupservice.Update(entity);
                if (isupdate == true)
                {
                    M_ItemGroupEntity entity_ = new M_ItemGroupEntity();
                    entity_.itemgrouplist = itemgroupservice.Get();
                    TempData["alertmsg"] = "Record Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult delete(int id)
        {
            bool delete = itemgroupservice.Delete(id);
            if (delete == true)
            {
                return Json(delete);
            }
            return Json(delete);
        }
        // public JsonResult delete(int id)
        //{

        //}

    }
}
