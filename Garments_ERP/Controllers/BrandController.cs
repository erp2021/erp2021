﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using Garments_ERP.Filters;

namespace Garments_ERP.Controllers
{
    public class BrandController : SuperController
    {
        //
        // GET: /Brand/
        BrandService brandservice = ServiceFactory.GetInstance().BrandService;

        [RBAC]
        public ActionResult Index()
        {
            //New Code Added by Rahul on 09-01-2020
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.title = "Brand";

            var list = brandservice.get();
            return View(list);
        }

        [RBAC]
        public ActionResult Create()
        {
            //New Code Added by Rahul on 09-01-2020
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.title = "Brand";

            return View();

        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(M_BrandMasterEntity entity)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    entity.IsActive = true;
                    //New Code Added by Rahul on 09-01-2020
                    var sessiondat = Session["LoggedUser"];
                    M_UserEntity collection = (M_UserEntity)sessiondat;
                    entity.Createdby = Convert.ToInt32(collection.Id);
                    entity.Createddate = DateTime.Now;
                    entity.companyid= Convert.ToInt32(collection.Company_ID);
                    entity.branchid = Convert.ToInt32(collection.BranchId);

                    long id = brandservice.Insert(entity);
                    if (id > 0)
                    {

                        TempData["alertmsg"] = "Record Save Successfully";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception ex)
                {
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    TempData["alertmsg"] = ex.Message;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");
        }
       
        
        [RBAC]
        public ActionResult Edit(int id)
        {
            //New Code Added by Rahul on 09-01-2020
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.title = "Brand";

            M_BrandMasterEntity entity = brandservice.get().Where(x => x.Id == id).FirstOrDefault();
            return View(entity);
        }
       
        [RBAC]
        [HttpPost]
        public ActionResult Edit(M_BrandMasterEntity entity)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var sessiondat = Session["LoggedUser"];
                    M_UserEntity collection = (M_UserEntity)sessiondat;
                    entity.UpdatedBy= Convert.ToInt32(collection.Id);
                    entity.Updateddate = DateTime.Now;
                    entity.companyid = Convert.ToInt32(collection.Company_ID);
                    entity.branchid = Convert.ToInt32(collection.BranchId);

                    bool isupdate = brandservice.Update(entity);
                    if (isupdate == true)
                    {
                        M_ItemColorEntity entity_ = new M_ItemColorEntity();
                        var list = brandservice.get();

                        TempData["alertmsg"] = "Record Update Successfully";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception ex)
                {
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    TempData["alertmsg"] = ex.Message;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");
        }
       
        
        [RBAC]
        public ActionResult Delete(int id)
        {
            bool delete = brandservice.Delete(id);
            if (delete == true)
            {
                return Json(delete);
            }
            return Json(delete);
        }

        [RBAC]
        public JsonResult CheckBrandName(String Brandname, int? Id)
        {
            bool value = brandservice.CheckBrandNameExistance(Brandname, Id);
            return Json(value, JsonRequestBehavior.AllowGet);
        }
    }
}
