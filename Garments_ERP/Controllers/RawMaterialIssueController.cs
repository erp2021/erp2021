﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class RawMaterialIssueController : Controller
    {
        //
        // GET: /RawMaterialIssue/
        WarehouseService warehouseservice = ServiceFactory.GetInstance().WarehouseService;
        EmployeeService empservice = ServiceFactory.GetInstance().EmployeeService;
        MaterialIssueService materialissueservice = ServiceFactory.GetInstance().MaterialIssueService;
        DepartmentService deptservice = ServiceFactory.GetInstance().DepartmentService;
        SRM_GRNInwardService srmservice = ServiceFactory.GetInstance().SRMGRNInwardService;
        WorkorderService workorderservice = ServiceFactory.GetInstance().WorkorderService;
        [RBAC]
        public ActionResult Index()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            return View();
        }

        public JsonResult GetWODetails(long woID)
        {
            var list = workorderservice.GetbyId(woID);
            return Json(list);
        }
        public JsonResult GetProcessMaterialDetails(long workorderID)
        {
            var list = materialissueservice.GetWOProcessMaterialDetails(workorderID);
            return Json(list);
        }
        [RBAC]
        public ActionResult Create()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            MFG_MaterialIssueMasterEntity entity = new MFG_MaterialIssueMasterEntity();
            ViewBag.employeelist = DropDownList<EmployeeEntity>.LoadItems(empservice.get(comid,branchid), "Id", "Name", 0);
            ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(deptservice.get(), "Id", "Departmentname", 0);
            ViewBag.warehouselist = DropDownList<M_WarehouseEntity>.LoadItems(warehouseservice.get(comid, branchid), "ID", "SHORT_NAME", 0);
            ViewBag.workorderlist = DropDownList<MFG_WORK_ORDEREntity>.LoadItems(workorderservice.Get(), "WorkOrderID", "WorkOrderNo", 0);
            string[] MaterialIssueNo = materialissueservice.getnextmino().Split('-');
            entity.MaterialIssueNo = MaterialIssueNo[0] + "-" + MaterialIssueNo[1] + "-" + MaterialIssueNo[2] + "-" + MaterialIssueNo[3] + "-XXXX";

            return View(entity);
        }
        [HttpPost]
        [RBAC]
        public ActionResult Create(MFG_MaterialIssueMasterEntity entity, FormCollection frm)
        {
            try
            {
                var warehouseid = frm["WarehouseID"];
                entity.WarehouseID = Convert.ToInt64(warehouseid);
                var empid = frm["EmpID"];
                entity.EmpID = Convert.ToUInt16(empid);
                var styleid = frm["StyleID"];
                entity.StyleID = Convert.ToInt32(styleid);
                string mino = frm["MaterialIssueNo"];
                string issuedate = frm["issuedate"];
                DateTime issue_date = DateTime.ParseExact(issuedate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                string wodate = frm["RefWODate"];
                DateTime wo_date = DateTime.ParseExact(wodate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                entity.CreatedBy = Helper.Helper.CurrentLoggedUser.Id;
                entity.RefWO = Convert.ToInt32(frm["RefWO"]);
                string DepartmentId = frm["DepartmentId"];
                entity.MaterialIssueNo = mino;
                entity.IssueDate = issue_date;
                entity.RefWODate = wo_date;
                entity.DeptId = Convert.ToInt32(DepartmentId);
                var rawitemstrlist = frm["rawitemdetails"].ToString();
                var unitid = frm["unitid1"];

                string[] rawarr = rawitemstrlist.Split('#');

                List<MFG_MaterialIssueItemDetailsEntity> list = new List<MFG_MaterialIssueItemDetailsEntity>();
                foreach (var item in rawarr)
                {
                    string[] rawitem = item.Split('~');
                    MFG_MaterialIssueItemDetailsEntity rawitementity = new MFG_MaterialIssueItemDetailsEntity();
                    if (rawitem[0] != "")
                    {
                        rawitementity.Process_ID = Convert.ToInt64(rawitem[0]);
                    }
                    if (rawitem[1] != "")
                    {
                        rawitementity.ItemSubCategoryID = Convert.ToInt32(rawitem[1]);
                    }
                    if (rawitem[2] != "")
                    {
                        rawitementity.ItemID = Convert.ToInt64(rawitem[2]);
                    }
                    if (rawitem[3] != "")
                    {
                        rawitementity.UOM_ID = Convert.ToInt32(rawitem[3]);
                    }
                    if (rawitem[4] != "")
                    {
                        rawitementity.ReqQty = Convert.ToDecimal(rawitem[4]);
                    }
                    if (rawitem[5] != "")
                    {
                        rawitementity.AvailableQty = Convert.ToDecimal(rawitem[5]);
                    }
                    if (rawitem[6] != "")
                    {
                        rawitementity.IssueQty = Convert.ToDecimal(rawitem[6]);
                    }
                    if (rawitem[7] != "")
                    {
                        rawitementity.BalanceQty = Convert.ToInt32(rawitem[7]);
                    }
                    rawitementity.loggedouser = Helper.Helper.CurrentLoggedUser.Id;

                    rawitementity.IsActive = true;
                    list.Add(rawitementity);
                }
                entity.issueitemlist = list;
                entity.IsActive = true;
                long id = materialissueservice.Insert(entity);
                // string Ischkchecked = frm[""];
                if (id > 0)
                {
                    string Ischkchecked = frm["Ischkpacking"];
                    if (Ischkchecked == "on")
                    {
                        var categories = entity.issueitemlist;
                        ViewData["PackingData"] = categories; // Send this list to the view
                        return RedirectToAction("Create", "Packing");
                    }
                    else
                    {
                        TempData["alertmimsg"] = "Material Issued successfully.";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
                return View(entity);
            }
            catch (Exception)
            {

                TempData["alertmimsg"] = "Something went wrong. Please try again later.";
                TempData["alertclass"] = "alert alert-danger alert-dismissible";
                return RedirectToAction("Index");
            }
        }
    }
}
