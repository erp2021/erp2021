﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using System.Globalization;
using Garments_ERP.Filters;
using Garments_ERP.Data.Data;
using System.IO;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.Net;
using Garments_ERP.Service.Service.MasterServices;

namespace Garments_ERP.Controllers
{
    public class LedgerAccountController : SuperController
    {
        // GET: /Customer/
        LedgerAccountServices ledgeraccountServices = ServiceFactory.GetInstance().LedgerAccountServices;
        StateService stateservice = ServiceFactory.GetInstance().StateService;
        CityService cityservice = ServiceFactory.GetInstance().CityService;
        DepartmentService departmentservice = ServiceFactory.GetInstance().DepartmentService;
        CustomerTypeService customertypeservice = ServiceFactory.GetInstance().CustomerTypeService;
        CustomerContactService customercontactservice = ServiceFactory.GetInstance().CustomerContactService;
        CustomerBankService customerbankservice = ServiceFactory.GetInstance().CustomerBankService;
        CountryService countryservice = ServiceFactory.GetInstance().CountryService;
        CompanyService companyservice = ServiceFactory.GetInstance().CompanyService;

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Entity";
            //New Code Added by Rahul on 09-01-2020
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var list = ledgeraccountServices.get().Where(x => x.Company_ID == ViewBag.companyid).ToList();
            return View(list);
        }

        [RBAC]
        public ActionResult BankIndex(long? id)
        {
            var list = customerbankservice.GetById((long)id);
            return View(list);
        }

        [RBAC]
        public ActionResult ContactIndex(long? id)
        {
            var list = customercontactservice.GetById((long)id);
            return View(list);
        }

        [RBAC]
        public JsonResult Getstate(int countryid)
        {
            var statelist = stateservice.get().Where(x => x.CountryID == countryid).ToList();
            return Json(statelist);
        }

        [RBAC]
        [HttpPost]
        public string verifyGSTIN(string gstin)
        {
            //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            //const SslProtocols _Tls12 = (SslProtocols)0x00000C00;
            //const SecurityProtocolType Tls12 = (SecurityProtocolType)_Tls12;
            //ServicePointManager.SecurityProtocol = Tls12;
            string gstData = null;// JObject.Parse("");

            try
            {
                ////added below twol lines to pass SSL/TLS secure channel
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                string result = "";
                string url = "https://ewaybill.nic.in/BillGeneration/BillGeneration.aspx/GetGSTNDetails";
                if (!string.IsNullOrEmpty(url))
                {
                    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.Method = "POST";
                    httpWebRequest.ContentType = "application/json";

                    httpWebRequest.UseDefaultCredentials = true;
                    httpWebRequest.PreAuthenticate = true;
                    httpWebRequest.Credentials = CredentialCache.DefaultCredentials;
                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        string json = new JavaScriptSerializer().Serialize(new
                        {
                            gstin = gstin
                        });
                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    // Enable UseUnsafeHeaderParsing
                  
                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        result = streamReader.ReadToEnd();
                    }
                }
                gstData = result;
            }
            catch (Exception ex)
            {
                gstData = null;
               
            }
            return gstData;    
        }

        [RBAC]
        public JsonResult Getstatecode(int stateid)
        {
            var statecode = stateservice.get().Where(x => x.ID == stateid).FirstOrDefault().StateCode;
            return Json(statecode);
        }
        

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Entity";
            //New Code Added by Rahul on 09-01-2020
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);


            ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name", 101);
            ViewBag.typelist = DropDownList<M_CustomerTypeMasterEntity>.LoadItems(customertypeservice.get(), "Id", "TypeName", 1);
            ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", 1646);
            ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0);
            //ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 1);
            ViewBag.complist = DropDownList<M_companyEntity>.LoadItems(companyservice.get(), "Company_ID", "Company_Name", 0);
            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(FormCollection frm, HttpPostedFileBase[] LedDocu)
        {
            try
            {
                var Name = Convert.ToString(frm["Ledger_Name"]);
                var entitytype = Convert.ToString(frm["customertypeid"]);
                var Department = Convert.ToString(frm["departmentid"]);

                // var customerGSTNo = frm["GSTNo"];
                var PANNO = Convert.ToString(frm["PANno"]);
                var Description = Convert.ToString(frm["description"]);

                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                //enquiryentity.CreatedBy = Convert.ToInt32(collection.Id);
                //enquiryentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                //enquiryentity.BranchId = Convert.ToInt32(collection.BranchId);


                M_LedgersEntity entity = new M_LedgersEntity();
                entity.Ledger_Name = Name;
                entity.LedgerType_Id = entitytype;
                entity.Department = Department == "" ? 0 : Convert.ToInt32(Department);
                entity.PAN_no = PANNO;
                entity.Ledger_Description = Description;
                entity.Is_Active = true;
                entity.Created_Date = DateTime.Now;
                //New Code Added by Rahul on 09-01-2020
                entity.Created_By= Convert.ToInt32(collection.Id);
               entity.Company_ID= Convert.ToInt32(collection.Company_ID);
                entity.Branch_ID= Convert.ToInt32(collection.BranchId);

                List<M_CompanyBranch_MappingEntity> CBMEntity = new List<M_CompanyBranch_MappingEntity>();
                var companycomb= frm["combranchList"].ToString();
                string[] combarr = companycomb.Split('#');
                if (companycomb != ",,")
                {
                    foreach (var CB in combarr)
                    {
                        string[] CBrr = CB.Split('~');
                        M_CompanyBranch_MappingEntity cbent = new M_CompanyBranch_MappingEntity();
                        cbent.Company_ID = CBrr[0] == "" ? 0 : Convert.ToInt32(CBrr[0]);
                        cbent.Branch_ID = CBrr[1] == "" ? 0 : Convert.ToInt32(CBrr[1]);
                        CBrr[2] = Convert.ToString(CBrr[2]) == "" ? "1" : CBrr[2];
                        cbent.Is_Active = Convert.ToInt32(CBrr[2]) == 1 ? true : false;
                        CBMEntity.Add(cbent);
                    }
                    entity.M_CompanyBranch_Mapping = CBMEntity;
                }

                    //code to save contact address details
                    //Added new filed company and branch by ,Rahul on,03-01-2020
                List<M_Ledger_BillingDetailsEntity> addresslistentity = new List<M_Ledger_BillingDetailsEntity>();
                var addresslist = frm["AddressList"].ToString();
                string[] addressarr = addresslist.Split('#');
                foreach (var adress in addressarr)
                {
                    if (adress != "")
                    {
                        string[] adresssrr = adress.Split('~');
                        M_Ledger_BillingDetailsEntity contactaddressentity = new M_Ledger_BillingDetailsEntity();

                        contactaddressentity.Billing_Name = Convert.ToString(adresssrr[1]);
                        contactaddressentity.Unit = Convert.ToString(adresssrr[2]);
                        contactaddressentity.Contact_No = Convert.ToString(adresssrr[3]);
                        contactaddressentity.Telephone_no = Convert.ToString(adresssrr[4]);
                        contactaddressentity.Email = Convert.ToString(adresssrr[5]);
                        contactaddressentity.Fax_no = Convert.ToString(adresssrr[6]);
                        contactaddressentity.Address = Convert.ToString(adresssrr[7]);
                        contactaddressentity.City_Id = Convert.ToString(adresssrr[8]) == "" ? 18258 : Convert.ToInt32(adresssrr[8]);
                        contactaddressentity.State_Id = Convert.ToString(adresssrr[9]) == "" ? 1646 : Convert.ToInt32(adresssrr[9]);
                        contactaddressentity.Country_Id = Convert.ToString(adresssrr[10]) == "" ? 101 : Convert.ToInt32(adresssrr[10]);
                        contactaddressentity.GSTIN = Convert.ToString(adresssrr[11]);
                        contactaddressentity.Bank_Name = Convert.ToString(adresssrr[13]);
                        contactaddressentity.Bank_code = Convert.ToString(adresssrr[15]);
                        contactaddressentity.Account_no = Convert.ToString(adresssrr[14]);
                        contactaddressentity.Is_SEZ = Convert.ToString(adresssrr[12]) == "1" ? true : false;
                        contactaddressentity.Company = Convert.ToString(adresssrr[16]);
                        contactaddressentity.Branch = Convert.ToString(adresssrr[17]);
                        //New Code Added by Rahul on 09-01-2020
                        contactaddressentity.Created_By = Convert.ToInt32(collection.Id);
                        contactaddressentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        contactaddressentity.Branch_ID = Convert.ToInt32(collection.BranchId);
                        contactaddressentity.Created_Date = DateTime.Now;

                        adresssrr[18] = Convert.ToString(adresssrr[18]) == "" ? "1" : adresssrr[18];
                        contactaddressentity.Is_Active = Convert.ToInt32(adresssrr[18]) == 1 ? true : false;
                        addresslistentity.Add(contactaddressentity);
                    }
                }

                entity.M_Ledger_BillingDetails = addresslistentity;

                List<M_LedgerDocumentMasterEntity> doclist = new List<M_LedgerDocumentMasterEntity>();
                if (LedDocu != null)
                {
                    foreach (var rimg in LedDocu)
                    {

                        if (rimg != null && rimg.ContentLength > 0)
                        {
                            var filename = Path.GetFileName(rimg.FileName);
                            var fileType = Path.GetExtension(rimg.FileName);
                            var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                            string isExistCompanypath = Server.MapPath("~/LedgerDocument/");
                            bool exists = System.IO.Directory.Exists(isExistCompanypath);


                            if (!exists)
                                System.IO.Directory.CreateDirectory(isExistCompanypath);


                            var path = Path.Combine(isExistCompanypath, newfilename);
                            rimg.SaveAs(path);
                            M_LedgerDocumentMasterEntity docent = new M_LedgerDocumentMasterEntity();
                            docent.Filename = newfilename;
                            docent.Filetype = fileType;
                            //New Code Added by Rahul on 09-01-2020
                            docent.CreatedBy= Convert.ToInt32(collection.Id);
                            docent.Createddate = DateTime.Now;
                            docent.Company_ID = Convert.ToInt32(collection.Company_ID);
                            docent.Branch_ID = Convert.ToInt32(collection.BranchId);
                            doclist.Add(docent);
                            //styleimgpath = newfilename;
                            //styleimglist.Add(styleimgpath);
                        }


                    }
                }

                entity.M_LedgerDocumentMaster = doclist;

                var Ldata = ledgeraccountServices.get().Where(x => x.LedgerType_Id.Contains(entitytype) && x.Ledger_Name.Contains(Name)).FirstOrDefault();
                long id = 0;
                if (Ldata!=null)
                {
                    id = ledgeraccountServices.Update(entity) == true ? 1 : 0;
                }
                else
                {
                     id = ledgeraccountServices.Insert(entity);
                }
                if(id>0)
                {
                    TempData["alertmsg"] = "Record Save Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");

            }
            return RedirectToAction("Index");
        }

        [RBAC]
        [HttpPost]
        public JsonResult CreateCustomer(string custname, int _customertypeid, string custphone, string email, string _remark, string cstno, string tinno, string bankname, string accountno, string bankcode, string contactlist, string addresslist)
        {
            if (contactlist != "" && addresslist != "")
            {
                var isinsert = false;

                var customername = custname;
                var customertypeid = _customertypeid;
                var customerphone = custphone;
                //var customeraddress = custaddress;
                // var customerstateid = stateid;
                // var customerGSTNo = gstno;
                var customeremail = email;
                var remark = _remark;
                var customerCSTNo = cstno;
                var customerTINNo = tinno;
                // var customercityid = cityid;
                //customer bank details
                var customerbank = bankname;
                var customeraccountno = accountno;
                var customerbankcode = bankcode;

                M_CustomerMasterEntity entity = new M_CustomerMasterEntity();
                entity.contactname = customername;
                entity.Customertype = (int)customertypeid;
                entity.Phone = customerphone;
                //entity.Address = customeraddress;
                // entity.Stateid = (int)customerstateid;
                //  entity.Cityid = (int)customercityid;
                // entity.GSTNo = customerGSTNo;
                entity.Email = customeremail;
                entity.Remark = remark;
                entity.CSTRegno = customerCSTNo;
                entity.Tinno = customerTINNo;
                entity.IsActive = true;
                entity.Createddate = DateTime.Now;
                entity.Updateddate = DateTime.Now;

                //code to save contact address details
                List<M_ContactAddressEntity> addresslistentity = new List<M_ContactAddressEntity>();
                var addresslist_ = addresslist.ToString();
                if (addresslist_ != null && addresslist_ != "")
                {
                    string[] addressarr = addresslist_.Split('#');
                    foreach (var adress in addressarr)
                    {
                        string[] adresssrr = adress.Split('~');
                        M_ContactAddressEntity contactaddressentity = new M_ContactAddressEntity();
                        contactaddressentity.Addess = adresssrr[0] + "~" + adresssrr[5];
                        contactaddressentity.Stateid = Convert.ToInt16(adresssrr[1]);
                        contactaddressentity.Cityid = Convert.ToInt32(adresssrr[2]);
                        contactaddressentity.Countryid = Convert.ToInt32(adresssrr[3]);
                        entity.GSTNo = Convert.ToString(adresssrr[4]);
                        if (Convert.ToString(adresssrr[5]) == "1")
                        {
                            entity.Address = adresssrr[0];
                            entity.Stateid = Convert.ToInt16(adresssrr[1]);
                            entity.Cityid = Convert.ToInt32(adresssrr[2]);
                            entity.Countryid = Convert.ToInt32(adresssrr[3]);
                        }
                        addresslistentity.Add(contactaddressentity);
                    }
                }

                entity.contactaddresslist = addresslistentity;

                long id = 0;// ledgeraccountServices.Insert(entity);

                if (id > 0)
                {

                    var contactslist = contactlist;
                    //code to split contactlist
                    string[] contactarr = contactslist.Split('#');
                    foreach (var arritem in contactarr)
                    {
                        string[] contact = arritem.Split('~');
                        M_CustomerContactDetailsEntity contactentity = new M_CustomerContactDetailsEntity();
                        contactentity.Customerid = id;
                        contactentity.Contactpersonname = contact[1];
                        contactentity.Mobileno = contact[2];
                        contactentity.Telephoneno = contact[3];
                        contactentity.Email = contact[4];
                        contactentity.Faxno = contact[5];
                        contactentity.Department = Convert.ToInt32(contact[6]);
                        contactentity.Remark = contact[7];
                        contactentity.IsActive = true;
                        contactentity.Createddate = DateTime.Now;
                        contactentity.Updateddate = DateTime.Now;
                        long insertedid = customercontactservice.Insert(contactentity);
                    }
                    //code to save customer bank details
                    M_CustomerBankDetailsEntity bankentity = new M_CustomerBankDetailsEntity();
                    bankentity.Customerid = id;
                    bankentity.Bankcode = customerbankcode;
                    bankentity.Bankname = customerbank;
                    bankentity.Accountno = customeraccountno;
                    bankentity.Createddate = DateTime.Now;
                    bankentity.Updateddate = DateTime.Now;
                    bankentity.IsActive = true;
                    long insertedbankid = customerbankservice.Insert(bankentity);
                    if (insertedbankid > 0)
                    {
                        isinsert = true;
                        // Response.Write("<script Language='JavaScript'>alert('Customer save successfully')</script>");
                        //  return RedirectToAction("Index");
                        return Json(isinsert);
                    }

                }
                ViewBag.typelist = DropDownList<M_CustomerTypeMasterEntity>.LoadItems(customertypeservice.get(), "Id", "TypeName", 1);
                ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", 1646);
                ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0);
                //ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 1);
                return Json(isinsert);
            }
            else
            {
                return Json(false);
            }
        }

        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "Entity";
            //New Code Added by Rahul on 09-01-2020
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);


            M_LedgersEntity model = new M_LedgersEntity();
            model = ledgeraccountServices.GetById(id);
            ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name", 101);
            ViewBag.typelist = DropDownList<M_CustomerTypeMasterEntity>.LoadItems(customertypeservice.get(), "Id", "TypeName", 0);
            ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", 1646);
            ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0);
            ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", Convert.ToInt32(model.Department));
            ViewBag.complist = DropDownList<M_companyEntity>.LoadItems(companyservice.get(), "Company_ID", "Company_Name", 0);
            return View(model);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(FormCollection frm, M_LedgersEntity model,HttpPostedFileBase[] LedDocu)
        {

            try
            {
                var Name = Convert.ToString(frm["Ledger_Name"]);
                var entitytype = Convert.ToString(frm["customertypeid"]);
                var Department = Convert.ToString(frm["departmentid"]);

                // var customerGSTNo = frm["GSTNo"];
                var PANNO = Convert.ToString(frm["PANno"]);
                var Description = Convert.ToString(frm["description"]);

                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                M_LedgersEntity entity = new M_LedgersEntity();
                entity.Ledger_Id = model.Ledger_Id;
                entity.Ledger_Name = Name;
                entity.LedgerType_Id = entitytype;
                if (Department != "")
                {
                    entity.Department = Convert.ToInt32(Department);
                }
                entity.PAN_no = PANNO;
                entity.Ledger_Description = Description;
                entity.Is_Active = true;
                //entity.Modified_Date = DateTime.Now;

                //New Code Added by Rahul on 09-01-2020
                entity.Modified_By = Convert.ToInt32(collection.Id);
                entity.Modified_Date = DateTime.Now; 
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.Branch_ID = Convert.ToInt32(collection.BranchId);

                
                List<M_CompanyBranch_MappingEntity> CBMEntity = new List<M_CompanyBranch_MappingEntity>();
                var companycomb = frm["combranchList"].ToString();
                string[] combarr = companycomb.Split('#');
                if (companycomb != ",,")
                {
                    foreach (var CB in combarr)
                    {
                        if (CB != "")
                        {
                            string[] CBrr = CB.Split('~');
                            M_CompanyBranch_MappingEntity cbent = new M_CompanyBranch_MappingEntity();
                            cbent.Company_ID = Convert.ToInt32(0);
                            cbent.Branch_ID = Convert.ToInt32(0);
                            cbent.Is_Active = Convert.ToInt32(0) == 1 ? true : false;
                            CBMEntity.Add(cbent);
                        }
                    }
                    entity.M_CompanyBranch_Mapping = CBMEntity;
                }

                List<M_Ledger_BillingDetailsEntity> addresslistentity = new List<M_Ledger_BillingDetailsEntity>();
                var addresslist = frm["AddressList"].ToString();
                string[] addressarr = addresslist.Split('#');
                foreach (var adress in addressarr)
                {
                    if (adress != "")
                    {
                        string[] adresssrr = adress.Split('~');
                        M_Ledger_BillingDetailsEntity contactaddressentity = new M_Ledger_BillingDetailsEntity();
                        contactaddressentity.Id = Convert.ToInt32(adresssrr[0]);
                        contactaddressentity.Billing_Name = Convert.ToString(adresssrr[1]);
                        contactaddressentity.Unit = Convert.ToString(adresssrr[2]);
                        contactaddressentity.Contact_No = Convert.ToString(adresssrr[3]);
                        contactaddressentity.Telephone_no = Convert.ToString(adresssrr[4]);
                        contactaddressentity.Email = Convert.ToString(adresssrr[5]);
                        contactaddressentity.Fax_no = Convert.ToString(adresssrr[6]);
                        contactaddressentity.Address = Convert.ToString(adresssrr[7]);
                        contactaddressentity.City_Id = Convert.ToInt32(adresssrr[8]);
                        contactaddressentity.State_Id = Convert.ToInt32(adresssrr[9]);
                        contactaddressentity.Country_Id = Convert.ToInt32(adresssrr[10]);
                        contactaddressentity.GSTIN = Convert.ToString(adresssrr[11]);
                        contactaddressentity.Bank_Name = Convert.ToString(adresssrr[13]);
                        contactaddressentity.Bank_code = Convert.ToString(adresssrr[15]);
                        contactaddressentity.Account_no = Convert.ToString(adresssrr[14]);
                        contactaddressentity.Company = Convert.ToString(adresssrr[16]);
                        contactaddressentity.Branch = Convert.ToString(adresssrr[17]);
                        contactaddressentity.Is_SEZ = Convert.ToString(adresssrr[12]) == "1" ? true : false;

                        //New Code Added by Rahul on 09-01-2020
                        contactaddressentity.Modified_By = Convert.ToInt32(collection.Id);
                        contactaddressentity.Modified_Date = DateTime.Now;
                        contactaddressentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        contactaddressentity.Branch_ID = Convert.ToInt32(collection.BranchId);

                        contactaddressentity.Is_Active = Convert.ToBoolean(true);

                        addresslistentity.Add(contactaddressentity);
                    }
                }

                entity.M_Ledger_BillingDetails = addresslistentity;

                List<string> doclist = new List<string>();
                if (LedDocu != null)
                {
                    foreach (var rimg in LedDocu)
                    {

                        if (rimg != null && rimg.ContentLength > 0)
                        {
                            var filename = Path.GetFileName(rimg.FileName);
                            var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                            string isExistCompanypath = Server.MapPath("~/LedgerDocument/");
                            bool exists = System.IO.Directory.Exists(isExistCompanypath);


                            if (!exists)
                                System.IO.Directory.CreateDirectory(isExistCompanypath);


                            var path = Path.Combine(isExistCompanypath, newfilename);
                            rimg.SaveAs(path);
                            doclist.Add(newfilename);
                            //styleimgpath = newfilename;
                            //styleimglist.Add(styleimgpath);
                        }
                        else
                        {
                            doclist.Add("");
                        }


                    }
                }
                //code to save contact address details
                List<M_LedgerDocumentMasterEntity> documntmstr = new List<M_LedgerDocumentMasterEntity>();
                if (doclist != null)
                {
                    foreach (var doc in doclist)
                    {
                        M_LedgerDocumentMasterEntity LedgerDocentity = new M_LedgerDocumentMasterEntity();
                        LedgerDocentity.Filename = Convert.ToString(doc);
                        //New Code Added by Rahul on 09-01-2020
                        LedgerDocentity.UpdatedBy = Convert.ToInt32(collection.Id);
                        LedgerDocentity.Updateddate = DateTime.Now;
                        LedgerDocentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        LedgerDocentity.Branch_ID = Convert.ToInt32(collection.BranchId);

                        documntmstr.Add(LedgerDocentity);

                        
                    }

                    entity.M_LedgerDocumentMaster = documntmstr;
                }
                bool id = ledgeraccountServices.Update(entity);
                if (id==true)
                {
                    TempData["alertmsg"] = "Record Update Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");

            }

            return RedirectToAction("Index");
        }

        [RBAC]
        [HttpPost]
        public JsonResult UpdateAddressdetail(long id, string address, int cityid, int stateid, int countryid)
        {
            M_ContactAddressEntity entity = new M_ContactAddressEntity();
            entity.Id = id;
            entity.Addess = address;
            entity.Cityid = cityid;
            entity.Stateid = stateid;
            entity.Countryid = countryid;
            var isupadated = ledgeraccountServices.Updateadddress(entity);
            return Json(isupadated);
        }

        [RBAC]
        public JsonResult deleteaddress(long id)
        {
            var isdeleted = ledgeraccountServices.deleteaddress(id);
            return Json(isdeleted);
        }

        [RBAC]
        public JsonResult getCustomerlist()
        {
            var list = ledgeraccountServices.get();
            return Json(list);
        }

        [RBAC]
        [HttpPost]
        public JsonResult DeleteLedgerAccount(long Id)
        {
            bool ISDeleted = false;
            ISDeleted = ledgeraccountServices.Delete(Id);
            return Json(ISDeleted);


        }

        [RBAC]
        public JsonResult getaddress(long custid)
        {
            var list = ledgeraccountServices.getAddresssbycustomerid(custid);
            return Json(list);
        }

        [RBAC]
        public JsonResult getDefaultAddressId(long custid)
        {
            List<M_CustomerMaster> list = new List<M_CustomerMaster>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_CustomerMaster.Where(x => x.Id == custid).ToList();
                foreach (var item in records)
                {
                    M_CustomerMaster entity = new M_CustomerMaster();
                    entity.Address = item.Address;
                    list.Add(entity);
                }

            }
            return Json(list);

        }

        [RBAC]
        [HttpPost]
        public JsonResult getCityNameList(int stateid)
        {
            List<M_CityMasterEntity> lis = cityservice.getByid(stateid);
            return Json(lis);
        }
        
    }
}
