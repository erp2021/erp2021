﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Service.Service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class StoreWorkOrderController : SuperController
    {
        //
        // GET: /StoreWorkOrder/
        StoreWorkOrderService SWO_ser = ServiceFactory.GetInstance().StoreWorkOrderService;

        [RBAC]
        public ActionResult Index()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            return View();
        }

        [RBAC]
        public ActionResult Create()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            string[] SWOno = SWO_ser.getnextSWOno().Split('-');
            ViewBag.SWOno = SWOno[0] + "-" + SWOno[1] + "-" + SWOno[2] + "-" + SWOno[3] + "-XXXX";

            return View();
        }

        public JsonResult CheckBOM(long StoreItemId,long ItemSizeId,decimal ItemQty)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            var BOM_MatList = SWO_ser.CheckBOM(StoreItemId, ItemSizeId, ItemQty, companyid, branchid);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(BOM_MatList);
            return Json(JSONString);
        }

        [HttpPost]
        public ActionResult AddUpdate(string data, string ItemData, string ItemBOM)
        {
            long id = 0;
            try
            {
                MFG_Store_WorkOrderEntity entity= JsonConvert.DeserializeObject<MFG_Store_WorkOrderEntity>(data);
                List<MFG_Store_WO_ItemDetailsEntity> SWO_ItemEntity= JsonConvert.DeserializeObject<List<MFG_Store_WO_ItemDetailsEntity>>(ItemData);
                List<MFG_Store_WO_BOM_ItemEntity> SWO_BOM_ItemEntity = JsonConvert.DeserializeObject<List<MFG_Store_WO_BOM_ItemEntity>>(ItemBOM);
                entity.IsActive = true;
                entity.Created_Date = DateTime.Now;
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.Created_By = Convert.ToInt32(collection.Id);
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.BranchId = Convert.ToInt32(collection.BranchId);
                int roleid = Convert.ToInt32(collection.RoleID);
                if (roleid != 4)
                {
                    entity.IsStatus = entity.Approvalstatus == true ? 3 : 5;
                    entity.ApproveDate = DateTime.Now;
                }
                else
                {
                    entity.IsStatus = 2;
                }
                List<SRM_PurchaseRequestItemDetailEntity> pritemlist = new List<SRM_PurchaseRequestItemDetailEntity>();
                foreach (var arritem in SWO_BOM_ItemEntity)
                {
                    if (arritem.RequiredQty > arritem.AvailableInStock)
                    {
                        decimal orderQty =Convert.ToDecimal(arritem.RequiredQty) -Convert.ToDecimal(arritem.AvailableInStock);

                        SRM_PurchaseRequestItemDetailEntity pritementity = new SRM_PurchaseRequestItemDetailEntity();
                        pritementity.ItemCategoryId =Convert.ToInt32(arritem.ItemSubCategoryId);
                        pritementity.ItemId = arritem.ItemId;
                        pritementity.Item_Attribute_ID =Convert.ToInt32(arritem.Item_Attribute_ID);
                        pritementity.UnitId = Convert.ToInt32(arritem.UnitId);
                        pritementity.orderQty = orderQty;
                        pritementity.RequiredQty = orderQty;
                        pritementity.IsActive = true;
                        pritementity.CreatedDate = DateTime.Now;
                        pritementity.CreatedBy = Convert.ToInt32(collection.Id);
                        pritementity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        pritementity.BranchId = Convert.ToInt32(collection.BranchId);
                        pritementity.BOMID = arritem.BOM_ID;
                        pritementity.Styleid = arritem.Styleid;
                        pritemlist.Add(pritementity);
                    }
                }

                id = SWO_ser.AddUpdate(entity, SWO_ItemEntity, pritemlist);

            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
            }
            return Json(id);
        }

        //Get Status Wise List
        [RBAC]
        public PartialViewResult GetStatusData(int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);

            var list = SWO_ser.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            return PartialView("_IndexList", list);
        }

        public JsonResult GetStoreWO_ItemData(long Store_WO_Id)
        {
            var list = SWO_ser.GetStoreWO_ItemData(Store_WO_Id);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(list);
            return Json(JSONString);
        }


    }
}
