﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class WarehouseController : SuperController
    {
        //
        // GET: /Warehouse/
        DepartmentService departmentservice = ServiceFactory.GetInstance().DepartmentService;
        WarehouseRoleService warehouserolesevice = ServiceFactory.GetInstance().WarehouseRoleService;
        WarehouseTypeService warehousetypeservice = ServiceFactory.GetInstance().WarehouseTypeService;
        EmployeeService employeeservice = ServiceFactory.GetInstance().EmployeeService;
        WarehouseService warehouservice = ServiceFactory.GetInstance().WarehouseService;
       
        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Warehouse";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var list = warehouservice.get(ViewBag.companyid, ViewBag.branchid);
            return View(list);
        }

        [RBAC]
        public JsonResult CheckwarehouseName(String SHORT_NAME, int? ID)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            List<M_WarehouseEntity> warehoue = warehouservice.get(ViewBag.companyid, ViewBag.branchid);
            bool value = true;
            if (ID == null)
                value = warehoue.Where(x => x.SHORT_NAME == SHORT_NAME).FirstOrDefault() == null;
            else
            {

                if (warehoue.Where(x => x.SHORT_NAME == SHORT_NAME && x.ID == ID).FirstOrDefault() == null)
                {
                    if (warehoue.Where(x => x.SHORT_NAME == SHORT_NAME).FirstOrDefault() == null)
                    {
                        value = true;
                    }
                    else
                    {
                        value = false;
                    }
                }
            }

            return Json(value, JsonRequestBehavior.AllowGet);
        }
       
        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Warehouse";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            ViewBag.ownerdeptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0);
            ViewBag.warehouserolelist = DropDownList<M_RoleEntity>.LoadItems(warehouserolesevice.get(), "roleId", "roleName", 0);
            ViewBag.warehousetypelist = DropDownList<M_WarehouseTypeMasterEntity>.LoadItems(warehousetypeservice.get(), "WAREHOUSE_TYPE_ID", "TypeName", 0);
            ViewBag.warehouseownerlist = DropDownList<EmployeeEntity>.LoadItems(employeeservice.get(comid, branchid), "UserId", "Name", 0);
            //ViewBag.owneraddresslist = DropDownList<EmployeeAddressDetailEntity>.LoadItems(employeeservice.getaddress(comid, branchid), "Id", "Addess", 0);

            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(M_WarehouseEntity model)
        {
            try
            {
                model.CREATED_ON = DateTime.Now;
                //model.UPDATED_ON = DateTime.Now;
                model.IS_ENABLED = true;

                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                model.CREATED_BY = Convert.ToInt32(collection.Id);
                model.Company_ID = Convert.ToInt32(collection.Company_ID);
                model.Branch_ID = Convert.ToInt32(collection.BranchId);

                long insertedid = warehouservice.Insert(model);
                if (insertedid > 0)
                {

                    TempData["alertmsg"] = "Record Save Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = Convert.ToString(ex);
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");

            }
            return RedirectToAction("Index");
            
        }
   
        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "Warehouse";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            M_WarehouseEntity model = new M_WarehouseEntity();
            model = warehouservice.getbyid(id);
            if (model.OWNER_DEPT_ID != null)
            {
                ViewBag.ownerdeptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", (int)model.OWNER_DEPT_ID);
            }
            else { ViewBag.ownerdeptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0); }
            if (model.WAREHOUSE_ROLE != null)
            {
                ViewBag.warehouserolelist = DropDownList<M_RoleEntity>.LoadItems(warehouserolesevice.get(), "roleId", "roleName", (int)model.WAREHOUSE_ROLE);
            }
            else { ViewBag.warehouserolelist = DropDownList<M_RoleEntity>.LoadItems(warehouserolesevice.get(), "roleId", "roleName", 0); }
            if (model.WAREHOUSE_TYPE_ID != null)
            {
                ViewBag.warehousetypelist = DropDownList<M_WarehouseTypeMasterEntity>.LoadItems(warehousetypeservice.get(), "WAREHOUSE_TYPE_ID", "TypeName", (int)model.WAREHOUSE_TYPE_ID);
            }
            else { ViewBag.warehousetypelist = DropDownList<M_WarehouseTypeMasterEntity>.LoadItems(warehousetypeservice.get(), "WAREHOUSE_TYPE_ID", "TypeName", 0); }
            
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            if (model.OWNER_ENTITY_ID != null)
            {
                ViewBag.warehouseownerlist = DropDownList<EmployeeEntity>.LoadItems(employeeservice.get(comid, branchid), "UserId", "Name", (int)model.OWNER_ENTITY_ID);
            }
            else { ViewBag.warehouseownerlist = DropDownList<EmployeeEntity>.LoadItems(employeeservice.get(comid, branchid), "UserId", "Name", 0); }
            //if (model.ADDRESS_ID != null)
            //{
            //    ViewBag.owneraddresslist = DropDownList<EmployeeAddressDetailEntity>.LoadItems(employeeservice.getaddress(), "Id", "Addess", (int)model.ADDRESS_ID);
            //}
            //else { ViewBag.owneraddresslist = DropDownList<EmployeeAddressDetailEntity>.LoadItems(employeeservice.getaddress(), "Id", "Addess", 0); }

            return View(model);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(long id, M_WarehouseEntity model)
        {
            try
            {
                model.UPDATED_ON = DateTime.Now;
                model.IS_ENABLED = true;

                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                model.UPDATED_BY = Convert.ToInt32(collection.Id);
                model.Company_ID = Convert.ToInt32(collection.Company_ID);
                model.Branch_ID = Convert.ToInt32(collection.BranchId);

                bool updatedid = warehouservice.Update(model);
                if (updatedid == true)
                {
                    TempData["alertmsg"] = "Record Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = Convert.ToString(ex);
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");

            }
            return RedirectToAction("Index");
        }

        [RBAC]
        [HttpPost]
        public JsonResult DeleteWarehouse(long Id)
        {
            bool ISDeleted = false;
            ISDeleted = warehouservice.Delete(Id);
            return Json(ISDeleted);

        }
    }
}
