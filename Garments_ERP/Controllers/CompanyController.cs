﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Garments_ERP.Service.Service.MasterServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class CompanyController : SuperController
    {
        //
        // GET: /Company/
        CompanyService companyservice = ServiceFactory.GetInstance().CompanyService;
        StateService stateservice = ServiceFactory.GetInstance().StateService;
        CityService cityservice = ServiceFactory.GetInstance().CityService;
      
        CustomerTypeService customertypeservice = ServiceFactory.GetInstance().CustomerTypeService;
       
        CountryService countryservice = ServiceFactory.GetInstance().CountryService;
      
        [RBAC]
        public ActionResult Index()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            return View();
        }

        [RBAC]
        public PartialViewResult CompanyList(int? Id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var list = companyservice.getComdetail();
            return PartialView("_CompanyList", list);
        }

        [RBAC]
        [HttpPost]
        public JsonResult GetBranchListById(int compid)
        {
            var entity = companyservice.GetBranchListById(compid);
            string JSONstring = string.Empty;
            JSONstring = JsonConvert.SerializeObject(entity);
            return Json(JSONstring);
        }

        [RBAC]
        public JsonResult DeleteRecord(long Id)
        {
            bool ISDeleted = false;
            ISDeleted = companyservice.Delete(Id);
            return Json(ISDeleted);


        }

        [RBAC]
        public ActionResult Create()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);


            ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name", 101);         
            ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", 1646);
            ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0);
            ViewBag.CompanyList = DropDownList<M_companyEntity>.LoadItems(companyservice.getComdetail().Where(x=>x.ParentId==0).ToList(), "Company_ID", "Company_Name", 0);

            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(FormCollection frm)
        {
            try
            {
                M_companyEntity entity = new M_companyEntity();
                
                entity.Company_Name =Convert.ToString(frm["comName"]);
                entity.Legal_Name =Convert.ToString(frm["LegalName"]);
                entity.UserName = Convert.ToString(frm["UserName"]);
                entity.Mobile_No =Convert.ToString(frm["mobile"]);
                entity.PhoneNumber =Convert.ToString(frm["phone"]);
                entity.FaxNo = Convert.ToString(frm["facno"]);
                entity.Company_Address = frm["address"];
                entity.Country_Code =Convert.ToString(frm["countryid"]);
                entity.State_Id = Convert.ToInt32(frm["addressstateid"]);
                entity.City_Id = Convert.ToInt32(frm["addresscityid"]);
                entity.Pincode = Convert.ToString(frm["PinCode"]);
                entity.EmailID = Convert.ToString(frm["cemail"]);
                entity.Pan_No = Convert.ToString(frm["PanNo"]);
                entity.GSTNo = Convert.ToString(frm["GSTNo"]);
                entity.Website = Convert.ToString(frm["Website"]);
                entity.Head_Office_Add =Convert.ToString(frm["HeadAdd"]);
                entity.Remark = Convert.ToString(frm["Remark"]);
                entity.Created_Date = DateTime.Now;
                entity.ParentId = Convert.ToString(frm["mainBranchid"]) == "" ? 0 : Convert.ToInt64(frm["mainBranchid"]);
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.Created_By = Convert.ToInt32(collection.Id);


                string password= Helper.Helper.Encrypt("23456");
                entity.Is_Active = true;

                string CName = companyservice.Insert(entity, password);
                if (CName !="")
                {
                    TempData["alertmsg"] = "Record Save Successfully!        " + CName;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";

                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertmsg"] = "Error";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                }
            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Edit(long id)
        {
            M_companyEntity entity = new M_companyEntity();
           
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                ViewBag.roleid = Convert.ToInt32(collection.RoleID);
                ViewBag.deptid = Convert.ToInt32(collection.DeptID);
                ViewBag.userid = Convert.ToInt32(collection.Id);
                ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
                ViewBag.branchid = Convert.ToInt32(collection.BranchId);
                ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
                ViewBag.Username = Convert.ToString(collection.Username);


                entity = companyservice.GetById(id);
                ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name",(string)entity.Country_Code);
                ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name",(int)entity.State_Id);
                ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name",(int) entity.City_Id);
                ViewBag.CompanyList = DropDownList<M_companyEntity>.LoadItems(companyservice.getComdetail().Where(x => x.ParentId == 0).ToList(), "Company_ID", "Company_Name",(int)entity.ParentId);


                return View(entity);
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            try
            {
                M_companyEntity entity = new M_companyEntity();
                entity.Company_ID = Convert.ToInt32(frm["Company_ID"]);
                entity.Company_Name = frm["comName"];
                entity.Legal_Name = frm["LegalName"];
                entity.Mobile_No = frm["mobile"];
                entity.PhoneNumber = frm["phone"];
                entity.Head_Office_Add = frm["HeadAdd"];
                entity.EmailID = frm["cemail"];
                entity.Pan_No = frm["PanNo"];
                entity.Company_Address = frm["address"];
                entity.Country_Code = frm["countryid"];
                entity.State_Id = Convert.ToInt32(frm["addressstateid"]);
                entity.City_Id = Convert.ToInt32(frm["addresscityid"]);
                entity.Pincode = frm["PinCode"];
                entity.Website = frm["Website"];
                entity.Modified_Date = DateTime.Now;
                var isupdated = companyservice.Update(entity);
              
                if (isupdated == true)
                {
                    TempData["alertmsg"] = "Record Update Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";

                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertmsg"] = "Error";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                }
            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");
            }
           
            return RedirectToAction("Index");
        }

    }
}
