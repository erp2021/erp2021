﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class UnitController : SuperController
    {
        //
        // GET: /Unit/
        UnitService unitservice = ServiceFactory.GetInstance().UnitService;
        DepartmentService departmentservice = ServiceFactory.GetInstance().DepartmentService;
        
        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Unit";
            if (System.Web.HttpContext.Current.Session["LoggedUser"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                ViewBag.roleid = Convert.ToInt32(collection.RoleID);
                ViewBag.deptid = Convert.ToInt32(collection.DeptID);
                ViewBag.userid = Convert.ToInt32(collection.Id);
                ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
                ViewBag.branchid = Convert.ToInt32(collection.BranchId);
                ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
                ViewBag.Username = Convert.ToString(collection.Username);
                var list = unitservice.get();
                return View(list);
            }
        }
        public JsonResult CheckUnit(string ItemUnit, int? Id)
        {
            bool value = unitservice.CheckUnitExistance(ItemUnit, Id);
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Unit";
            if (System.Web.HttpContext.Current.Session["LoggedUser"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                ViewBag.roleid = Convert.ToInt32(collection.RoleID);
                ViewBag.deptid = Convert.ToInt32(collection.DeptID);
                ViewBag.userid = Convert.ToInt32(collection.Id);
                ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
                ViewBag.branchid = Convert.ToInt32(collection.BranchId);
                ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
                ViewBag.Username = Convert.ToString(collection.Username);
                ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0);
                return View();
            }
        }

        [HttpPost]
        public ActionResult Create(M_UnitMasterEntity model)
        {
            model.IsActive = true;
            model.Createddate = DateTime.Now;
            //New Code Added by Rahul on 09-01-2020
            //model.Updateddate = DateTime.Now;
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            model.CreatedBy = Convert.ToInt32(collection.Id);
            model.Company_ID = Convert.ToInt32(collection.Company_ID);
            model.Branch_ID = Convert.ToInt32(collection.BranchId);

            int insertedid = unitservice.Insert(model);
            if (insertedid > 0)
            {
                TempData["alertmsg"] = "Record Save Successfully.";
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return View();
        }

        [RBAC]
        public ActionResult Edit(int id)
        {
            ViewBag.title = "Unit";
            if (System.Web.HttpContext.Current.Session["LoggedUser"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                ViewBag.roleid = Convert.ToInt32(collection.RoleID);
                ViewBag.deptid = Convert.ToInt32(collection.DeptID);
                ViewBag.userid = Convert.ToInt32(collection.Id);
                ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
                ViewBag.branchid = Convert.ToInt32(collection.BranchId);
                ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
                ViewBag.Username = Convert.ToString(collection.Username);
                M_UnitMasterEntity model = new M_UnitMasterEntity();
                model = unitservice.GetById(id);
                if (model.DepartmentId != null) { ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", (int)model.DepartmentId); }
                else { ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0); }
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(M_UnitMasterEntity entity)
        {
            entity.IsActive = true;          
            entity.Updateddate = DateTime.Now;
            //New Code Added by Rahul on 09-01-2020
            //model.Updateddate = DateTime.Now;
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            entity.Updatedby = Convert.ToInt32(collection.Id);
            entity.Company_ID = Convert.ToInt32(collection.Company_ID);
            entity.Branch_ID = Convert.ToInt32(collection.BranchId);

            bool updatedid = unitservice.Update(entity);
            if (updatedid == true)
            {
                TempData["alertmsg"] = "Record Updated Successfully.";
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            if (entity.DepartmentId != null) { ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", (int)entity.DepartmentId); }
            else { ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0); }
            return View(entity);
        }

        [HttpPost]
        public JsonResult DeleteUnit(int Id)
        {
            bool ISDeleted = false;
            ISDeleted = unitservice.Delete(Id);
            return Json(ISDeleted);

        }

    }
}
