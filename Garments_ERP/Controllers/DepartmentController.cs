﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using Garments_ERP.Filters;

namespace Garments_ERP.Controllers
{
    public class DepartmentController : SuperController
    {
        //
        // GET: /Department/
        DepartmentService departmentservice = ServiceFactory.GetInstance().DepartmentService;

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Department";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);


            var list = departmentservice.get();
            return View(list);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Department";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);


            ViewBag.ParentId = 0;
            var DepTList = departmentservice.get().Where(x => x.ParentId == 0).ToList();
            ViewBag.DepTList = DropDownList<M_DepartmentMasterEntity>.LoadItems(DepTList, "Id", "Departmentname", 0);
            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(FormCollection frm, M_DepartmentMasterEntity entity)
        {
           
                try
                {
                    if (ModelState.IsValid)
                    {
                        entity.ParentId = Convert.ToString(frm["parentid"]) == "" ? 0 : Convert.ToInt64(frm["parentid"]);
                        entity.IsActive = true;
                        var sessiondat = Session["LoggedUser"];
                        M_UserEntity collection = (M_UserEntity)sessiondat;
                        entity.Createdby = Convert.ToInt32(collection.Id);

                        long id = departmentservice.Insert(entity);
                        if (id > 0)
                        {
                            TempData["alertmsg"] = "Record Save Successfully";
                            TempData["alertclass"] = "alert alert-success alert-dismissible";
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    TempData["alertmsg"] = ex.Message;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Edit(int id)
        {
            ViewBag.title = "Department";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);


            M_DepartmentMasterEntity entity = departmentservice.get().Where(x => x.Id == id).FirstOrDefault();
            var parentId =Convert.ToInt32(entity.ParentId);
            var DepTList = departmentservice.get().Where(x => x.ParentId == 0).ToList();
            ViewBag.DepTList = DropDownList<M_DepartmentMasterEntity>.LoadItems(DepTList, "Id", "Departmentname", parentId);
            ViewBag.ParentId = parentId;
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(FormCollection frm, M_DepartmentMasterEntity entity)
        {
           
                try
                {
                    if (ModelState.IsValid)
                    {
                        entity.ParentId = Convert.ToString(frm["parentid"]) == "" ? 0 : Convert.ToInt64(frm["parentid"]);
                        entity.IsActive = true;
                        var sessiondat = Session["LoggedUser"];
                        M_UserEntity collection = (M_UserEntity)sessiondat;
                        entity.UpdatedBy = Convert.ToInt32(collection.Id);
                        entity.Updateddate = DateTime.Now;


                        bool isupdate = departmentservice.Update(entity);
                        if (isupdate == true)
                        {
                            M_ItemColorEntity entity_ = new M_ItemColorEntity();
                            var list = departmentservice.get();
                            TempData["alertmsg"] = "Record Update Successfully";
                            TempData["alertclass"] = "alert alert-success alert-dismissible";
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    TempData["alertmsg"] = ex.Message;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Delete(int id)
        {
            bool delete = departmentservice.Delete(id);
            if (delete == true)
            {
                return Json(delete);
            }
            return Json(delete);
        }

        public JsonResult CheckDepartmentName(String DepartmentName, int? Id)
        {
            bool value = departmentservice.CheckDepartmentNameExistance(DepartmentName, Id);
            return Json(value, JsonRequestBehavior.AllowGet);
        }

    }
}
