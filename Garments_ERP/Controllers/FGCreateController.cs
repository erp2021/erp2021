﻿using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;
using Garments_ERP.Entity.ProductionEntities;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Garments_ERP.Service.Service.ProductionServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Garments_ERP.Controllers
{
    public class FGCreateController : SuperController
    {
        // GET: /FGCreate/
        PackingTypeService packingtypeservice = ServiceFactory.GetInstance().PackingTypeService;
        ItemService itemservice = ServiceFactory.GetInstance().ItemService;
        SRM_GRNInwardService srmgrnservice = ServiceFactory.GetInstance().SRMGRNInwardService;
        FinishedGoodsService proceecysle = ServiceFactory.GetInstance().FinishedGoodsService;

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "FG Retail";
            ViewBag.packingtypelist = DropDownList<M_PackingTypeMasterEntity>.LoadItems(packingtypeservice.get(), "Id", "Packingtype", 0);
            ViewBag.ControlList = DropDownList<M_Dynamic_ControlsEntity>.LoadItems(srmgrnservice.GetControlList(), "Dynamic_Controls_Id", "Control_Name", 0);
            var list = proceecysle.get();
            return View(list);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "FG Retail";
            ViewBag.packingtypelist = DropDownList<M_PackingTypeMasterEntity>.LoadItems(packingtypeservice.get(), "Id", "Packingtype", 0);
            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(M_ItemMasterEntity model, FormCollection frm)
        {
            var processlist = frm["finishitemdetails"].ToString();

            int i = 0;

            try
            {

                var isActive = Convert.ToInt16(frm["isActive"]);

                if (isActive == 1)
                {
                    model.IsActive = true;
                }
                else
                {
                    model.IsActive = false;
                }
                var IsSet = Convert.ToInt16(frm["isset"]);

                model.Taxrate = (Convert.ToString(frm["GSTCharge"]) != "" && Convert.ToString(frm["GSTCharge"]) != "select") ? Convert.ToDecimal(frm["GSTCharge"]) : 0;
                model.ItemName = frm["fgitemname"].ToString();
                model.ItemCode = frm["fgitemcode"].ToString();
                model.HScode = frm["hsncode"].ToString();
                model.Packingtypeid = Convert.ToInt32(frm["Packingtypeid"].ToString());
                model.Itemsubcategoryid = 0;//Convert.ToInt32(frm["hiddenitemcategoryid"].ToString());


                //List for FinishedGoods
                List<M_FinishedGoodsEntity> tranlist = new List<M_FinishedGoodsEntity>();
                List<M_FinishedGoodsEntity> incatelist = new List<M_FinishedGoodsEntity>();
                if (processlist != "")
                {
                    string[] tranarray = processlist.Split('#');

                    foreach (var item in tranarray)
                    {
                        if (item != "")
                        {
                            string[] singleitem = item.Split('~');
                            //pro sequ icatin icatout commen
                            M_FinishedGoodsEntity itementity = new M_FinishedGoodsEntity();
                            itementity.Item_ID = Convert.ToInt16(singleitem[1]);
                            itementity.Item_Attr_Id = Convert.ToInt16(singleitem[2]);
                            itementity.Item_sub_catg_id = Convert.ToInt32(singleitem[3]);
                            itementity.itmUOMid = Convert.ToInt32(singleitem[4]);
                            itementity.itmUOM_val = singleitem[5];
                            itementity.Item_inward_id = Convert.ToInt16(singleitem[6]);
                            //itementity.Item_ID = Convert.ToInt16(singleitem[7]);
                            itementity.Batch_qty = Convert.ToDecimal(singleitem[7]);
                            itementity.IsuueQty = Convert.ToDecimal(singleitem[8]);
                            //itementity.Batch_qty = Convert.ToDecimal(singleitem[3]);


                            var sessiondat1 = Session["LoggedUser"];
                            M_UserEntity collection1 = (M_UserEntity)sessiondat1;
                            itementity.Createdby = Convert.ToInt32(collection1.Id);
                            itementity.companyid = Convert.ToInt32(collection1.Company_ID);
                            itementity.branchid = Convert.ToInt32(collection1.BranchId);
                            itementity.IsActive = true;
                            itementity.Createddate = DateTime.Now;
                            tranlist.Add(itementity);
                            i++;
                        }
                    }



                }
                //List for Inwarditem
                List<M_ItemInwardMasterEntity> inwardlist = new List<M_ItemInwardMasterEntity>();
                //List<M_ItemInwardMasterEntity> incatelist = new List<M_ItemInwardMasterEntity>();
                if (processlist != "")
                {
                    string[] tranarray = processlist.Split('#');

                    foreach (var item in tranarray)
                    {
                        if (item != "")
                        {
                            string[] singleitem = item.Split('~');
                            //pro sequ icatin icatout commen
                            M_ItemInwardMasterEntity itementity = new M_ItemInwardMasterEntity();
                            itementity.Item_ID = Convert.ToInt16(singleitem[1]);
                            itementity.Item_Attribute_ID = Convert.ToInt16(singleitem[2]);
                            //itementity.ite = Convert.ToInt32(frm["hiddenitemcategoryid"].ToString());
                            itementity.UOM = Convert.ToInt32(singleitem[4]);
                            //itementity.itmUOM_val = singleitem[5];
                            itementity.ItemInward_ID = Convert.ToInt16(singleitem[6]);
                            //itementity.Item_ID = Convert.ToInt16(singleitem[7]);
                            itementity.ItemQty = Convert.ToDecimal(singleitem[7]);
                            itementity.BalanceQty = Convert.ToDecimal(singleitem[9]);
                            itementity.InventoryModeId = 6;


                            var sessiondat1 = Session["LoggedUser"];
                            M_UserEntity collection1 = (M_UserEntity)sessiondat1;
                            itementity.CreatedBy = Convert.ToInt32(collection1.Id);
                            itementity.Company_ID = Convert.ToInt32(collection1.Company_ID);
                            itementity.BranchId = Convert.ToInt32(collection1.BranchId);
                            itementity.IsActive = true;
                            itementity.CreatedDate = DateTime.Now;
                            inwardlist.Add(itementity);
                            i++;
                        }
                    }


                    //model.finishitemlist = tranlist;

                }
                //List for Productionfo
                List<M_Product_InfoEntity> productinfolist1 = new List<M_Product_InfoEntity>();
                //List<M_ItemInwardMasterEntity> incatelist = new List<M_ItemInwardMasterEntity>();
                if (processlist != "")
                {
                    string[] tranarray = processlist.Split('#');

                    foreach (var item in tranarray)
                    {
                        if (item != "")
                        {
                            string[] singleitem = item.Split('~');
                            //pro sequ icatin icatout commen
                            M_Product_InfoEntity infoentity = new M_Product_InfoEntity();
                            infoentity.ItemInward_ID = Convert.ToInt16(singleitem[6]);
                            infoentity.Dynamic_Controls_Id = Convert.ToInt16(singleitem[4]);
                            //itementity.Item_sub_catg_id = 0;
                            infoentity.Dynamic_Controls_value = singleitem[5];
                            var sessiondat1 = Session["LoggedUser"];
                            M_UserEntity collection1 = (M_UserEntity)sessiondat1;
                            infoentity.Created_By = Convert.ToInt32(collection1.Id);
                            infoentity.Company_ID = Convert.ToInt32(collection1.Company_ID);
                            infoentity.BranchId = Convert.ToInt32(collection1.BranchId);

                            infoentity.Created_Date = DateTime.Now;
                            productinfolist1.Add(infoentity);
                            i++;
                        }
                    }

                }

                //List for Outwarditem
                List<M_ItemOutwardMasterEntity> outitemlist1 = new List<M_ItemOutwardMasterEntity>();
                //List<M_ItemInwardMasterEntity> incatelist = new List<M_ItemInwardMasterEntity>();
                if (processlist != "")
                {
                    string[] tranarray = processlist.Split('#');

                    foreach (var item in tranarray)
                    {
                        if (item != "")
                        {
                            string[] singleitem = item.Split('~');
                            //pro sequ icatin icatout commen
                            M_ItemOutwardMasterEntity infoentity = new M_ItemOutwardMasterEntity();
                            infoentity.Item_ID = Convert.ToInt16(singleitem[1]);
                            infoentity.Item_Attribute_ID = Convert.ToInt16(singleitem[2]);
                            infoentity.ItemSubCategoryID = Convert.ToInt32(singleitem[3]);
                            infoentity.Inward_ID = Convert.ToInt16(singleitem[6]);
                            infoentity.UOM = Convert.ToInt32(singleitem[4]);
                            //infoentity.itmUOM_val = singleitem[5];
                            //infoentity.Item_inward_id = Convert.ToInt16(singleitem[6]);
                            //itementity.Item_ID = Convert.ToInt16(singleitem[7]);

                            infoentity.OutwardDate = DateTime.Now;
                            infoentity.ItemQty = Convert.ToDecimal(singleitem[8]);
                            var sessiondat1 = Session["LoggedUser"];

                            M_UserEntity collection1 = (M_UserEntity)sessiondat1;
                            infoentity.OutwardBy = Convert.ToInt32(collection1.Id);
                            infoentity.UpdatedBy = Convert.ToInt32(collection1.Id);
                            infoentity.UpdatedDate = DateTime.Now;
                            infoentity.Company_ID = Convert.ToInt32(collection1.Company_ID);
                            infoentity.BranchId = Convert.ToInt32(collection1.BranchId);
                            outitemlist1.Add(infoentity);
                            i++;
                        }
                    }

                }
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                model.CreatedBy = Convert.ToInt32(collection.Id);
                model.Createddate = DateTime.Now;
                model.Company_ID = Convert.ToInt32(collection.Company_ID);
                model.Branch_ID = Convert.ToInt32(collection.BranchId);

                //model.
                model.productinfolist = productinfolist1;
                model.inwarditemlist = inwardlist;
                model.finishitemlist = tranlist;
                model.outwardlist = outitemlist1;

                int insertid = proceecysle.Insert(model);
                if (insertid > 0)
                {
                    TempData["alertmsg"] = "Record Save Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "FG Retail";
            if (System.Web.HttpContext.Current.Session["LoggedUser"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.packingtypelist = DropDownList<M_PackingTypeMasterEntity>.LoadItems(packingtypeservice.get(), "Id", "Packingtype", 0);
                M_ItemMasterEntity model = new M_ItemMasterEntity();
                model = proceecysle.GetById(id);

                return View(model);
            }
        }

        [RBAC]
        [HttpPost]
        public JsonResult GetInventory(string MyJson, int type, int id)
        {
            XmlDocument doc = new XmlDocument();
            doc = JsonConvert.DeserializeXmlNode("{\"Row\":" + MyJson + "}", "root"); // JSON needs to be an objec
            var entity = itemservice.GetInventory(string.Empty, type, doc);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(entity);
            return Json(JSONString);

        }

        [RBAC]
        public ActionResult InventoryReport()
        {
            return View();
        }

        //public JsonResult GetCustomerExtraFieldsDetails(string ItemChangeid)
        //{


        //    var entity = itemservice.GetInventoryItem(ItemChangeid);
        //    string JSONString = string.Empty;
        //    JSONString = JsonConvert.SerializeObject(entity);
        //    return Json(JSONString);

        //}

        [HttpPost]
        public JsonResult GetOutstandingById(int id)
        {

            var list = itemservice.GetOutstandingById(id);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(list);
            return Json(JSONString);

        }
    }
}
