﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using Garments_ERP.Filters;
namespace Garments_ERP.Controllers
{
    public class MachinetypeController : SuperController
    {
        //
        // GET: /Machinetype/
        MachineTypeService machineservice = ServiceFactory.GetInstance().MachineTypeService;

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Machine Type";
            //New Code Added by Rahul on 09-01-2020
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var list = machineservice.Get().Where(x => x.companyid == ViewBag.companyid && x.branchid == ViewBag.branchid);
            return View(list);
        }

        [RBAC]
        public JsonResult Checkmachinetypename(String Machine_TypeName, long? Machine_TypeId)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            bool value = true;
            if (Machine_TypeId == null)
                value = machineservice.Get().Where(x => x.Machine_TypeName == Machine_TypeName && x.companyid==companyid && x.branchid==branchid).FirstOrDefault() == null;
            else
            {

                if (machineservice.Get().Where(x => x.Machine_TypeName == Machine_TypeName && x.Machine_TypeId == Machine_TypeId && x.companyid == companyid && x.branchid == branchid).FirstOrDefault() == null)
                {
                    if (machineservice.Get().Where(x => x.Machine_TypeName == Machine_TypeName && x.companyid == companyid && x.branchid == branchid).FirstOrDefault() == null)
                    {
                        value = true;
                    }
                    else
                    {
                        value = false;
                    }
                }
            }

            return Json(value, JsonRequestBehavior.AllowGet);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Machine Type";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            M_MachineTypeMasterEntity entity_ = new M_MachineTypeMasterEntity();
          //  entity_.itemgrouplist = itemgroupservice.Get();
            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(M_MachineTypeMasterEntity entity)
        {
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.companyid = Convert.ToInt32(collection.Company_ID);
                entity.branchid = Convert.ToInt32(collection.BranchId);

                entity.IsActive = true;
                long id = machineservice.Insert(entity);
                if (id > 0)
                {

                    TempData["alertmsg"] = "Record Save Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");

        }

      
        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "Machine Type";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);


            M_MachineTypeMasterEntity entity = machineservice.Get().Where(x => x.Machine_TypeId == id).FirstOrDefault();
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(M_MachineTypeMasterEntity entity)
        {
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.companyid = Convert.ToInt32(collection.Company_ID);
                entity.branchid = Convert.ToInt32(collection.BranchId);
                bool isupdate = machineservice.Update(entity);
                if (isupdate == true)
                {
                    M_MachineTypeMasterEntity entity_ = new M_MachineTypeMasterEntity();

                    TempData["alertmsg"] = "Record Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult delete(long id)
        {
            bool delete = machineservice.Delete(id);
            if (delete == true)
            {
                return Json(delete);
            }
            return Json(delete);
        }
       

    }
}
