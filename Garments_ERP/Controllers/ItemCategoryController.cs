﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using Garments_ERP.Filters;
namespace Garments_ERP.Controllers
{
    public class ItemCategoryController : SuperController
    {
        //
        // GET: /ItemCategory/
        ItemGroupService itemgroupservice = ServiceFactory.GetInstance().ItemGroupService;
        ItemCategoryService itemcategoryservice = ServiceFactory.GetInstance().ItemCategoryService;

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Item Category";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var itemcategorylist = itemcategoryservice.get().Where(x=>x.Company_ID== ViewBag.companyid && x.Branch_ID== ViewBag.branchid);
            return View(itemcategorylist);
        }

        [RBAC]
        public JsonResult CheckItemCategory(string ItemCategory, int? Id, int Itemgroupid)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);


            bool value = itemcategoryservice.CheckExistance(ItemCategory, Id, Itemgroupid, companyid, branchid);
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Item Category";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            M_ItemCategoryMasterEntity entity = new M_ItemCategoryMasterEntity();
            ViewBag.Itemgrouplist = DropDownList<M_ItemGroupEntity>.LoadItems(itemgroupservice.Get(), "Id", "Itemgroup", 0);
            entity.itemcategorylist = itemcategoryservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList();
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(M_ItemCategoryMasterEntity entity)
        {
            try
            {
                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.CreatedBy = Convert.ToInt32(collection.Id);
                entity.Createddate = DateTime.Now;
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.Branch_ID = Convert.ToInt32(collection.BranchId);

                var id = itemcategoryservice.Insert(entity);

                TempData["alertmsg"] = "Record Save Successfully.";
                TempData["alertclass"] = "alert alert-success alert-dismissible";


            }
            catch(Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Edit(int id)
        {
            ViewBag.title = "Item Category";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            M_ItemCategoryMasterEntity entity = itemcategoryservice.get().Where(x => x.Id == id).FirstOrDefault();
            ViewBag.Itemgrouplist = DropDownList<M_ItemGroupEntity>.LoadItems(itemgroupservice.Get(), "Id", "Itemgroup", entity.Itemgroupid);
          
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(M_ItemCategoryMasterEntity entity)
        {
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.UpdatedBy = Convert.ToInt32(collection.Id);
                entity.Updateddate = DateTime.Now;
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.Branch_ID = Convert.ToInt32(collection.BranchId);

                bool isupdate = itemcategoryservice.Update(entity);
                if (isupdate == true)
                {

                    TempData["alertmsg"] = "Record Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";

                    return RedirectToAction("Index");
                }
            }
            catch(Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
        //public ActionResult delete(int id)
        //{
        //    if(id!=null)
        //    {
        //        var deleted=itemcategoryservice.Delete(id);
        //        if(deleted==true)
        //        {
        //            return RedirectToAction("Create");
        //        }
        //    }
        //    return RedirectToAction("Create");
        //}

        [RBAC]
        public JsonResult delete(int id)
        {
            var deleted = itemcategoryservice.Delete(id);
            return Json(deleted);
        }
    }
}
