﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class PaymentTermController : Controller
    {
        //
        // GET: /PaymentTerm/
        PaymentTermService paymenttermservice = ServiceFactory.GetInstance().PaymentTermService;

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Payment Term";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var list = paymenttermservice.get();
            return View(list);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Payment Term";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            return View();
        }

        [HttpPost]
        public ActionResult Create(M_PaymentTermMasterEntity entity)
        {
            
                try
                {
                    if (ModelState.IsValid)
                    {
                        //New Code Added by Rahul on 09-01-2020
                        var sessiondat = Session["LoggedUser"];
                        M_UserEntity collection = (M_UserEntity)sessiondat;
                        entity.Createdby = Convert.ToInt32(collection.Id);
                        entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        entity.Branch_ID = Convert.ToInt32(collection.BranchId);

                        entity.IsActive = true;
                        long id = paymenttermservice.Insert(entity);
                        if (id > 0)
                        {

                            TempData["alertmsg"] = "Record Save Successfully.";
                            TempData["alertclass"] = "alert alert-success alert-dismissible";
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception ex)
                {
                    TempData["alertmsg"] = "Something Went Wrong, Please Try Again.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                }
            
            return RedirectToAction("Index");

        }

        public JsonResult CheckPaymentTerm(string paymentterm, int? Id)
        {
            bool value = paymenttermservice.CheckPaymentTermExistance(paymentterm, Id);
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        [RBAC]
        public ActionResult Edit(int id)
        {
            ViewBag.title = "Payment Term";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            M_PaymentTermMasterEntity entity = paymenttermservice.get().Where(x => x.Id == id).FirstOrDefault();
            return View(entity);
        }

        [HttpPost]
        public ActionResult Edit(M_PaymentTermMasterEntity entity)
        {
            
                try
                {
                    if (ModelState.IsValid)
                    {
                        //New Code Added by Rahul on 09-01-2020
                        var sessiondat = Session["LoggedUser"];
                        M_UserEntity collection = (M_UserEntity)sessiondat;
                        entity.Updatedby = Convert.ToInt32(collection.Id);
                        entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        entity.Branch_ID = Convert.ToInt32(collection.BranchId);
                        entity.Updateddate = DateTime.Now;
                        bool isupdate = paymenttermservice.Update(entity);
                        if (isupdate == true)
                        {
                            M_PaymentTermMasterEntity entity_ = new M_PaymentTermMasterEntity();
                            var list = paymenttermservice.get();
                            TempData["alertmsg"] = "Record Updated Successfully.";
                            TempData["alertclass"] = "alert alert-success alert-dismissible";
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    TempData["alertmsg"] = ex.Message;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Delete(int id)
        {
            bool delete = paymenttermservice.Delete(id);
            if (delete == true)
            {
                return Json(delete);
            }
            return Json(delete);
        }
    }
}
