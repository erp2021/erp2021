﻿using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Garments_ERP.Service.Service.MasterServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class AttributeController : SuperController
    {
        //ItemService itemservice = ServiceFactory.GetInstance().ItemService;
        AttributeService attributeservice = ServiceFactory.GetInstance().AttributeService;
        //
        // GET: /Attribute/
        [RBAC]
        public ActionResult Index()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.title = "Attribute";

            var list = attributeservice.get().Where(x=>x.Company_ID== ViewBag.companyid && x.Branch_ID== ViewBag.branchid);
            return View(list);
        }

        [RBAC]
        public ActionResult Create()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.title = "Attribute";
            //ViewBag.itemcategorylist = DropDownList<M_ItemMasterEntity>.LoadItems(itemservice.get(), "Id", "ItemName", 0);
            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(M_Attribute_MasterEntity entity)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    entity.Is_Active = true;
                    //New Code Added by Rahul on 09-01-2020
                    var sessiondat = Session["LoggedUser"];
                    M_UserEntity collection = (M_UserEntity)sessiondat;                   
                   
                    entity.Created_Date = DateTime.Now;
                    entity.Created_By= Convert.ToInt32(collection.Id);
                    entity.Company_ID= Convert.ToInt32(collection.Company_ID);
                    entity.Branch_ID = Convert.ToInt32(collection.BranchId);

                    int id = attributeservice.Insert(entity);
                    if (id > 0)
                    {

                        TempData["alertmsg"] = "Record Save Successfully.";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception ex)
                {
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    TempData["alertmsg"] = ex.Message;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");

        }

        [RBAC]
        public ActionResult Edit(int id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.title = "Attribute";

            M_Attribute_MasterEntity model = new M_Attribute_MasterEntity();
            model = attributeservice.GetById(id);
            if (model.Status == true)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }

        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(M_Attribute_MasterEntity model)
        {
            try
            {
                //model.Modified_Date = DateTime.Now;
                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                model.Modified_Date = DateTime.Now;
                model.Modified_By = Convert.ToInt32(collection.Id);
                model.Company_ID = Convert.ToInt32(collection.Company_ID);
                model.Branch_ID = Convert.ToInt32(collection.BranchId);

                model.Is_Active = true;
                model.Modified_By = Helper.Helper.CurrentLoggedUser.Id;
                bool updatedId = attributeservice.Update(model);

                if (updatedId == true)
                {
                    TempData["alertmsg"] = "Record Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
                catch (Exception ex)
                {
                        Helper.ExceptionLogging.SendErrorToText(ex);
                    TempData["alertmsg"] = ex.Message;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");   
                }
            return RedirectToAction("Index");


        }

        [RBAC]
        [HttpPost]
        public JsonResult DeleteAttribute(int Id)
        {
            bool ISDeleted = false;
            ISDeleted = attributeservice.Delete(Id);
            return Json(ISDeleted);

        }
    }
}
