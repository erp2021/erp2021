﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class ProcessController : SuperController
    {
        //
        // GET: /Process/
        ItemSubCategoryService itemsubcategoryservice = ServiceFactory.GetInstance().ItemSubCategoryService;
        ItemCategoryService itemcategoryservice = ServiceFactory.GetInstance().ItemCategoryService;
        UnitService unitservice = ServiceFactory.GetInstance().UnitService;
        MachineTypeService machinetypeservice = ServiceFactory.GetInstance().MachineTypeService;
        ProcessService processservice = ServiceFactory.GetInstance().ProcesService;
        EmployeeService employeeservice = ServiceFactory.GetInstance().EmployeeService;

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Process";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            var list = processservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid);
            return View(list);
        }

        [RBAC]
        public JsonResult CheckProcessname(String SHORT_NAME, decimal? Process_Id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            bool value = true;
            if (Process_Id == null)
                value = processservice.get().Where(x => x.SHORT_NAME == SHORT_NAME && x.Company_ID==companyid && x.Branch_ID==branchid).FirstOrDefault() == null;
            else
            {

                if (processservice.get().Where(x => x.SHORT_NAME == SHORT_NAME && x.Process_Id == Process_Id && x.Company_ID == companyid && x.Branch_ID == branchid).FirstOrDefault() == null)
                {
                    if (processservice.get().Where(x => x.SHORT_NAME == SHORT_NAME && x.Company_ID == companyid && x.Branch_ID == branchid).FirstOrDefault() == null)
                    {
                        value = true;
                    }
                    else
                    {
                        value = false;
                    }
                }
            }

            return Json(value, JsonRequestBehavior.AllowGet);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Process";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            ViewBag.employeelist = DropDownList<EmployeeEntity>.LoadItems(employeeservice.get(comid, branchid), "UserId", "Name", 0);
            ViewBag.itemcategorylist = DropDownList<M_ItemCategoryMasterEntity>.LoadItems(itemcategoryservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Id", "Itemcategory", 0);
            ViewBag.durationunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", 0);
            ViewBag.machinetypelist = DropDownList<M_MachineTypeMasterEntity>.LoadItems(machinetypeservice.Get(), "Machine_TypeId", "Machine_TypeName", 0);
            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(FormCollection frm, M_ProcessMasterEntity model)
        {
            try
            {
                var jobwork = frm["jobwork"];
                var Barcode = frm["Barcode"];
                // var produration = Convert.ToDecimal(frm["produration"]);
                model.Process_Direction_Job = jobwork;
                model.Process_produce_BarCode = Barcode;
                // model.Process_Duration = produration;
                model.IS_ENABLED = true;
                model.CREATED_ON = DateTime.Now;

                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                model.CREATED_BY = Convert.ToInt32(collection.Id);
                model.Company_ID = Convert.ToInt32(collection.Company_ID);
                model.Branch_ID = Convert.ToInt32(collection.BranchId);

                //model.UPDATED_ON = DateTime.Now;
                decimal insertedid = processservice.Insert(model);
                if (insertedid > 0)
                {
                    //Response.Write("<script Language='JavaScript'>alert('Process creation successful.')</script>");
                    TempData["alertmsg"] = "Process creation successful.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }


        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "Process";
            M_ProcessMasterEntity model = new M_ProcessMasterEntity();
            model = processservice.GetById(id);
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            if (model.Process_Incharge_ID != null)
            {
                ViewBag.employeelist = DropDownList<EmployeeEntity>.LoadItems(employeeservice.get(comid, branchid), "UserId", "Name", (int)model.Process_Incharge_ID);
            }
            else
            {
                ViewBag.employeelist = DropDownList<EmployeeEntity>.LoadItems(employeeservice.get(comid, branchid), "UserId", "Name", 0);
            }
            if (model.ItemCategoryId != null)
            {
                ViewBag.itemcategorylist = DropDownList<M_ItemCategoryMasterEntity>.LoadItems(itemcategoryservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Id", "Itemcategory", (int)model.ItemCategoryId);
            }
            else
            {
                ViewBag.itemcategorylist = DropDownList<M_ItemCategoryMasterEntity>.LoadItems(itemcategoryservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Id", "Itemcategory", 0);
            }


            if (model.Process_Duration_Per_UOM != null)
            {
                ViewBag.durationunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", model.Process_Duration_Per_UOM);
            }
            else
            {
                ViewBag.durationunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", 0);
            }
            if (model.Process_Machine_Type_Id != null)
            {
                ViewBag.machinetypelist = DropDownList<M_MachineTypeMasterEntity>.LoadItems(machinetypeservice.Get(), "Machine_TypeId", "Machine_TypeName", (int)model.Process_Machine_Type_Id);
            }
            else
            {
                ViewBag.machinetypelist = DropDownList<M_MachineTypeMasterEntity>.LoadItems(machinetypeservice.Get(), "Machine_TypeId", "Machine_TypeName", 0);
            }

            return View(model);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(M_ProcessMasterEntity model, FormCollection frm)
        {
            try
            {
                var jobwork = frm["jobwork"];
                var Barcode = frm["Barcode"];
                // var produration = Convert.ToDecimal(frm["produration"]);
                model.Process_Direction_Job = jobwork;
                model.Process_produce_BarCode = Barcode;
                // model.Process_Duration = produration;
                model.IS_ENABLED = true;
                model.UPDATED_ON = DateTime.Now;

                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                model.UPDATED_BY = Convert.ToInt32(collection.Id);
                model.Company_ID = Convert.ToInt32(collection.Company_ID);
                model.Branch_ID = Convert.ToInt32(collection.BranchId);

                bool updatedId = processservice.Update(model);
                if (updatedId == true)
                {
                    TempData["alertmsg"] = "Process Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public JsonResult DeleteProcess(long Id)
        {
            bool ISDeleted = false;
            ISDeleted = processservice.Delete(Id);
            return Json(ISDeleted);
        }
    }
}
