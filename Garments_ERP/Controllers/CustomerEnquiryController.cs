﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using System.Globalization;
using System.IO;
using System.Web.Script.Serialization;
using Garments_ERP.Filters;
using Garments_ERP.Service.Service.MasterServices;
using Newtonsoft.Json;
using System.Xml;
using Garments_ERP.Entity.MasterEntities;

namespace Garments_ERP.Controllers
{

    public class CustomerEnquiryController : SuperController
    {
        //
        // GET: /CustomerEnquiry/
        CompanyService companyservice = ServiceFactory.GetInstance().CompanyService;
        EnquiryService enquiryservice = ServiceFactory.GetInstance().EnquiryService;
        EnquiryRawItemDetailService enquiryrawitemservice = ServiceFactory.GetInstance().EnquiryRawItemDetailService;
        EnquiryStyleDetailService enquirystyledetailservice = ServiceFactory.GetInstance().EnquiryStyleDetailService;
        CustomerService customerservice = ServiceFactory.GetInstance().CustomerService;
        ItemCategoryService itemcategoryservice = ServiceFactory.GetInstance().ItemCategoryService;
        ItemService itemservice = ServiceFactory.GetInstance().ItemService;
        ItemSizeService itemsizeservice = ServiceFactory.GetInstance().ItemSizeService;
        ItemSubCategoryService itemsubcategoryservice = ServiceFactory.GetInstance().ItemSubCategoryService;
        SegmentTypeService segmenttypeservice = ServiceFactory.GetInstance().SegmentTypeService;
        StyleService styleservice = ServiceFactory.GetInstance().StyleService;
        ItemColorService itemcolorservice = ServiceFactory.GetInstance().ItemColorService;
        ItemshadeService itemshadeservice = ServiceFactory.GetInstance().ItemshadeService;
        StyleSizeDetailService sizeservice = ServiceFactory.GetInstance().StyleSizeDetailService;
        UnitService unitservice = ServiceFactory.GetInstance().UnitService;
        PackingTypeService packingtypeservice = ServiceFactory.GetInstance().PackingTypeService;
        AttributeService attributeservice = ServiceFactory.GetInstance().AttributeService;
        AttributeValueService attributevalservice = ServiceFactory.GetInstance().AttributeValueService;
        ItemAttributeValueService itemattributevalservice = ServiceFactory.GetInstance().ItemAttributeValueService;

        StateService stateservice = ServiceFactory.GetInstance().StateService;
        CityService cityservice = ServiceFactory.GetInstance().CityService;
        DepartmentService departmentservice = ServiceFactory.GetInstance().DepartmentService;
        CustomerTypeService customertypeservice = ServiceFactory.GetInstance().CustomerTypeService;
        CustomerContactService customercontactservice = ServiceFactory.GetInstance().CustomerContactService;
        CustomerBankService customerbankservice = ServiceFactory.GetInstance().CustomerBankService;
        CountryService countryservice = ServiceFactory.GetInstance().CountryService;

        WarehouseService warehouse = ServiceFactory.GetInstance().WarehouseService;
        PackingTypeService packtype = ServiceFactory.GetInstance().PackingTypeService;
        RackService rack = ServiceFactory.GetInstance().RackService;
        UnitService unit = ServiceFactory.GetInstance().UnitService;
        BrokerService brokerservice = ServiceFactory.GetInstance().BrokerService;
        LedgerAccountServices ledgeraccountservices= ServiceFactory.GetInstance().LedgerAccountServices;

        // countryservice = ServiceFactory.GetInstance().CountryService;


        //  CustomerService customerservice = ServiceFactory.GetInstance().CustomerService;

        [RBAC]
        public JsonResult getCustomeraddress(long id)
        {
            var record = customerservice.get().Where(x => x.Id == id).FirstOrDefault().Address;

            return Json(record);
        }

        [RBAC]
        public JsonResult getItemSubcategory(long itemcategoryid)
        {
            var list = itemsubcategoryservice.get().Where(x => x.itemcategoryid == itemcategoryid).ToList();
            return Json(list);
        }

        [RBAC]
        public JsonResult GetItemShade()
        {
            var list = itemshadeservice.get();
            return Json(list);
        }

        [RBAC]
        public JsonResult GetItemShape()
        {
            var list = itemshadeservice.getShape();
            return Json(list);
        }

        [RBAC]
        public JsonResult GetPackingtype()
        {
            var list = packtype.get();
            return Json(list);
        }

        [RBAC]
        public JsonResult Getunit()
        {
            var list = unit.get();
            return Json(list);
        }

        [RBAC]
        public JsonResult getItemsize(long itemid, long styleid)
        {

            var records = itemsizeservice.get().Where(x => x.Itemid == itemid && x.Styleid== styleid).ToList();
            return Json(records);
        }

        [RBAC]
        public ActionResult Index()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.title = "Customer Enquiry";
            return View();
        }

        //Get Status Wise List
        [RBAC]
        public PartialViewResult GetStatusData(int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid= Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);

            var list = enquiryservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            return PartialView("_IndexList", list);
        }


        //Get Form date to Date Filter Status Wise List
        [RBAC]
        public PartialViewResult GetFilterStatusData(string fromdate, string todate, int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            if (fromdate != "" && todate!="")
            {
                
                var fdate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var tdate = DateTime.ParseExact(todate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var list = enquiryservice.get(fdate, tdate, IsStatus, Company_ID, BranchId, Userid, roleid);
                ViewBag.fromdate = fromdate;
                ViewBag.todate = todate;
                return PartialView("_IndexList", list);
            }
            else
            {
                var list = enquiryservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
                return PartialView("_IndexList", list);
            }
           
        }

        [RBAC]
        public ActionResult Create()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.title = "Customer Enquiry";

            var rawitemlist = itemservice.get().ToList();
            ViewBag.EnquiryType = DropDownList<M_EnquiryTypeMasterEntity>.LoadItems(enquiryservice.getEnqiryType(), "id", "EnquiryType", 0);
            ViewBag.segmenttypelist = DropDownList<M_SegmentTypeMasterEntity>.LoadItems(segmenttypeservice.get().Where(x=>x.Company_ID== ViewBag.companyid && x.Branch_ID== ViewBag.branchid).ToList(), "Id", "Segmenttype", 0);
            ViewBag.rawitemlist = DropDownList<M_ItemMasterEntity>.LoadItems(rawitemlist, "Id", "ItemName", 0);
            ViewBag.typelist = DropDownList<M_CustomerTypeMasterEntity>.LoadItems(customertypeservice.get(), "Id", "TypeName", 1);
            string[] enqno = enquiryservice.getnextenqno().Split('-');
            ViewBag.enqno = enqno[0] + "-" + enqno[1] + "-" + enqno[2] + "-" + enqno[3] + "-XXXX";
            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(FormCollection frm, List<HttpPostedFileBase> Style_Img, List<HttpPostedFileBase> RawItemImg)
        {
            
            string styleimgpath = string.Empty;
            List<string> styleimglist = new List<string>();
            List<string> rawimglist = new List<string>();
            List<string> AttributeImage = new List<string>();

            try
            {
                if (Request.Files.Count > 0)
                {
                    foreach (var stfile in Style_Img)
                    {
                        if (stfile != null && stfile.ContentLength > 0)
                        {
                            var filename = Path.GetFileName(stfile.FileName);
                            var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                            string isExistCompanypath = Server.MapPath("~/uploadedStyle/");
                            bool exists = System.IO.Directory.Exists(isExistCompanypath);
                            if (!exists)
                                System.IO.Directory.CreateDirectory(isExistCompanypath);

                            var path = Path.Combine(isExistCompanypath, newfilename);
                            stfile.SaveAs(path);
                            styleimgpath = newfilename;
                            styleimglist.Add(styleimgpath);
                        }
                    }

                    if (RawItemImg != null)
                    {
                        foreach (var rimg in RawItemImg)
                        {

                            if (rimg != null && rimg.ContentLength > 0)
                            {
                                var filename = Path.GetFileName(rimg.FileName);
                                var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                                string isExistCompanypath = Server.MapPath("~/uploadedrawitem/");
                                bool exists = System.IO.Directory.Exists(isExistCompanypath);

                                if (!exists)
                                    System.IO.Directory.CreateDirectory(isExistCompanypath);

                                var path = Path.Combine(isExistCompanypath, newfilename);
                                rimg.SaveAs(path);
                                rawimglist.Add(newfilename);
                            }
                        }
                    }
                }


                CRM_EnquiryMasterEntity enquiryentity = new CRM_EnquiryMasterEntity();
                enquiryentity.Enquiryno =Convert.ToString(frm["enquiryno"]);
                string invoicedate =Convert.ToString(frm["enquirydate"]);
                // var sam = frm["sampling"];
                DateTime enqdate = DateTime.ParseExact(invoicedate, "MM/dd/yyyy", CultureInfo.InvariantCulture);

                enquiryentity.Enquirydate = enqdate;
                enquiryentity.customerid = Convert.ToInt64(frm["customerid"]);
                enquiryentity.BrokerId = Convert.ToInt64(frm["BrokerId"]);
                enquiryentity.Addressid = Convert.ToInt64(frm["customeraddress"]);
                enquiryentity.narration =Convert.ToString(frm["narration"]);
                enquiryentity.Comment =Convert.ToString(frm["comment"]);
                enquiryentity.IsActive = true;
                enquiryentity.Createddate = DateTime.Now;
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                enquiryentity.CreatedBy = Convert.ToInt32(collection.Id);
                enquiryentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                enquiryentity.BranchId = Convert.ToInt32(collection.BranchId);
                enquiryentity.IsReadmade = Convert.ToString(frm["hidIsReadyMate"]) == "" ? 0 : Convert.ToInt32(frm["hidIsReadyMate"]);

                string tentivdatestr =Convert.ToString(frm["tentivedate"]);
                DateTime tentivedate = DateTime.ParseExact(tentivdatestr, "MM/dd/yyyy", CultureInfo.InvariantCulture);

                enquiryentity.styleimglist = styleimglist;

                enquiryentity.Tentivedate = tentivedate;
                var sampling =Convert.ToString(frm["sample"]);

                if (sampling == "yes")
                    enquiryentity.Sampling = true;
                else
                    enquiryentity.Sampling = false;

                int roleid= Convert.ToInt32(collection.RoleID);
                if (roleid != 4)
                {
                    enquiryentity.Approvalstatus = Convert.ToString(frm["approvalstatus"]) == "1" ? true : false;
                    enquiryentity.IsStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? 3 : 5;
                    enquiryentity.Approvaldate = DateTime.Now;
                }
                else
                {
                    enquiryentity.IsStatus = 2;
                }
                enquiryentity.Enquirytype = Convert.ToInt64(frm["EnquiryType"]);
                long id = 0;
                var styleitemstrlist = Convert.ToString(frm["styleliststr"]);
                if (styleitemstrlist != "" && styleitemstrlist != null)
                {
                    id = enquiryservice.Insert(enquiryentity);
                }
                if (id > 0)
                {
                    
                    string[] stylearr = styleitemstrlist.Split('#');

                    foreach (var arritem in stylearr)
                    {
                        if (arritem != "")
                        {
                            string[] styleitem = arritem.Split('~');

                            CRM_EnquiryStyleDetailEntity styleentity = new CRM_EnquiryStyleDetailEntity();
                            //segment  item style brand stydesc color shade image reqqty sizeratio 
                            styleentity.Enquiryid = id;
                            styleentity.Segmenttypeid = Convert.ToInt32(styleitem[0]);
                            styleentity.Itemid = Convert.ToInt32(styleitem[2]);
                            styleentity.Styleid = Convert.ToInt32(styleitem[1]);
                            styleentity.Brand = styleitem[4];
                            styleentity.Styledesc = styleitem[3];
                            styleentity.Styleno = Convert.ToString(frm["styleno"]);
                            styleentity.Totalqty = Convert.ToDecimal(frm["reqqty"]);
                            styleentity.Attribute_ID = Convert.ToString(styleitem[6]);
                            styleentity.Attribute_Value_ID = Convert.ToString(styleitem[7]);
                            styleentity.rate = Convert.ToDecimal(styleitem[9]);
                            styleentity.SizeId = Convert.ToInt64(styleitem[10]);
                            styleentity.Createdby = Convert.ToInt32(collection.Id);
                            styleentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                            styleentity.BranchId = Convert.ToInt32(collection.BranchId);
                            if (styleitem[8] != "")
                            {
                                decimal qty = Convert.ToDecimal(styleitem[8]);
                                styleentity.Reqqty = qty;
                            }
                            styleentity.IsActive = true;
                            int ICount = Convert.ToInt32(styleitem[11]);
                            int k = 0;
                            var Attrimgarray = AttributeImage.ToList();
                            List<string> imgdata = new List<string>();
                            foreach (var img in Attrimgarray)
                            {
                                if (k != ICount)
                                {
                                    if (img != "")
                                    {
                                        imgdata.Add(img.ToString());
                                        AttributeImage.RemoveAt(0);
                                    }
                                    else
                                    {
                                        AttributeImage.RemoveAt(0);
                                    }
                                }
                                else
                                {
                                    break;
                                }
                                k++;
                            }
                            styleentity.itemimg_list = imgdata;
                            long insertid = enquirystyledetailservice.Insert(styleentity);
                        }
                    }

                    var rimgarray = rawimglist.ToArray();
                    
                        //code to save raw item details
                        var rawitemstrlist =Convert.ToString(frm["rawitemliststr"]);
                        string[] rawarr = rawitemstrlist.Split('#');
                        int countofitem = 0;
                        foreach (var item in rawarr)
                        {
                            if (item != "")
                            {
                                string[] rawitem = item.Split('~');
                                CRM_EnquiryRawitemDetailEntity rawitementity = new CRM_EnquiryRawitemDetailEntity();
                                //var imglist = rawitem[4];
                                if (rawitemstrlist.Length != 0)
                                {

                                    rawitementity.Enquiryid = id;
                                    if (rawitem[0] != "")
                                    {
                                        rawitementity.Itemcategoryid = Convert.ToInt32(rawitem[0]);
                                    }
                                    if (rawitem[1] != null)
                                    {
                                        rawitementity.Itemsubcategoryid = Convert.ToInt32(rawitem[1]);
                                    }

                                    if (rawitem[2] != null)
                                    {
                                        rawitementity.Itemdesc = Convert.ToString(rawitem[2]);
                                    }
                                    //var imgrecord = rawitemstrlist;
                                    rawitementity.rawitemimg = rimgarray[countofitem];
                                    //rawitementity.rawitemimglist = rawimglist.ElementAtOrDefault(countofitem);
                                    rawitementity.Supplier = Convert.ToString(rawitem[3]);
                                    rawitementity.Createdate = DateTime.Now;
                                    rawitementity.IsActive = true;
                                    rawitementity.Createdby = Convert.ToInt32(collection.Id);
                                    rawitementity.Company_ID = Convert.ToInt32(collection.Company_ID);
                                    rawitementity.BranchId = Convert.ToInt32(collection.BranchId);
                                    int RICount= Convert.ToInt32(rawitem[4]);
                                    int k = 0;
                                    var Rawimgarray = rawimglist.ToList();
                                    List<string> rawimgdata = new List<string>();
                                    foreach (var img in Rawimgarray)
                                    {
                                        if (k != RICount)
                                        {
                                            if(img!="")
                                            {
                                                rawimgdata.Add(img.ToString());
                                                rawimglist.RemoveAt(0);
                                            }
                                            else
                                            {
                                                  rawimglist.RemoveAt(0);
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                        k++;
                                    }
                                rawitementity.rawitemimg_list = rawimgdata;
                                countofitem++;
                                    long rawitemid = enquiryrawitemservice.Insert(rawitementity);
                                }
                            }
                        }

                        TempData["alertmsg"] = "Record Save Successfully";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");

                }//close if 
                else
                {
                    TempData["alertmsg"] = "Record Error! Please Fill Proper Data";
                    return RedirectToAction("Index");
                }
                
            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");

            }
           

        }


        //New Code Add Update Enquery
        [HttpPost]
        public ActionResult AddUpdate(List<HttpPostedFileBase> Style_Img, List<HttpPostedFileBase> RawItemImg, string data,string RawItemData)
        {
            long id = 0;
            try
            {
                string styleimgpath = string.Empty;
                List<string> styleimglist = new List<string>();
                List<string> rawimglist = new List<string>();
                List<string> AttributeImage = new List<string>();
                if (Request.Files.Count > 0)
                {
                    foreach (var stfile in Style_Img)
                    {
                        if (stfile != null && stfile.ContentLength > 0)
                        {
                            var filename = Path.GetFileName(stfile.FileName);
                            var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                            string isExistCompanypath = Server.MapPath("~/uploadedStyle/");
                            bool exists = System.IO.Directory.Exists(isExistCompanypath);
                            if (!exists)
                                System.IO.Directory.CreateDirectory(isExistCompanypath);

                            var path = Path.Combine(isExistCompanypath, newfilename);
                            stfile.SaveAs(path);
                            styleimgpath = newfilename;
                            styleimglist.Add(styleimgpath);
                        }
                    }

                    if (RawItemImg != null)
                    {
                        foreach (var rimg in RawItemImg)
                        {

                            if (rimg != null && rimg.ContentLength > 0)
                            {
                                var filename = Path.GetFileName(rimg.FileName);
                                var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                                string isExistCompanypath = Server.MapPath("~/uploadedrawitem/");
                                bool exists = System.IO.Directory.Exists(isExistCompanypath);

                                if (!exists)
                                    System.IO.Directory.CreateDirectory(isExistCompanypath);

                                var path = Path.Combine(isExistCompanypath, newfilename);
                                rimg.SaveAs(path);
                                rawimglist.Add(newfilename);
                            }
                        }
                    }
                }


                List<CRM_EnquiryRawitemDetailEntity> RawItem= JsonConvert.DeserializeObject<List<CRM_EnquiryRawitemDetailEntity>>(RawItemData);
                CRM_EnquiryMasterEntity Entity = JsonConvert.DeserializeObject<CRM_EnquiryMasterEntity>(data);
                Entity.IsActive = true;
                Entity.Createddate = DateTime.Now;
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                Entity.CreatedBy = Convert.ToInt32(collection.Id);
                Entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                Entity.BranchId = Convert.ToInt32(collection.BranchId);
                int roleid = Convert.ToInt32(collection.RoleID);
                if (roleid != 4)
                {
                    Entity.IsStatus = Entity.Approvalstatus == true ? 3 : 5;
                    Entity.Approvaldate = DateTime.Now;
                }
                else
                {
                    Entity.IsStatus = 2;
                }
                id = enquiryservice.AddUpdate(styleimglist, rawimglist, Entity, RawItem);

            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
            }
            return Json(id);

        }


        [RBAC]
        public ActionResult Edit(long id)
        {

            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.title = "Customer Enquiry";

            int CBS = 1;//Customer ID
            int CB = 2;//Supplier ID
            CRM_EnquiryMasterEntity entity = new CRM_EnquiryMasterEntity();
            try
            {
                entity = enquiryservice.getbyid(id);

                long EnqTypeid = entity.Enquirytype == null ? 0 :(long)entity.Enquirytype;
                ViewBag.EnquiryType = DropDownList<M_EnquiryTypeMasterEntity>.LoadItems(enquiryservice.getEnqiryType(), "id", "EnquiryType", EnqTypeid);
                ViewBag.segmenttypelist = DropDownList<M_SegmentTypeMasterEntity>.LoadItems(segmenttypeservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Id", "Segmenttype", 0);
                return View(entity);
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(FormCollection frm, List<HttpPostedFileBase> styleimg, HttpPostedFileBase[] rawitemimg, HttpPostedFileBase[] ItemAttrimages_id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            int roleid = Convert.ToInt32(collection.RoleID);
            int deptid = Convert.ToInt32(collection.DeptID);
            int userid = Convert.ToInt32(collection.Id);
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            List<string> AttributeImage = new List<string>();
            List<string> rawimglist = new List<string>();
            List<CRM_EnquiryStyleImageEntity> styleimglist = new List<CRM_EnquiryStyleImageEntity>();
            string styleimgpath = "";
            try
            {

                if (Request.Files.Count > 0)
                {
                    if (styleimg != null)
                    {

                        foreach (var stfile in styleimg)
                        {
                            CRM_EnquiryStyleImageEntity imgentity = new CRM_EnquiryStyleImageEntity();
                            if (stfile != null && stfile.ContentLength > 0)
                            {
                                var filename = Path.GetFileName(stfile.FileName);
                                var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                                string isExistCompanypath = Server.MapPath("~/uploadedStyle/");
                                bool exists = System.IO.Directory.Exists(isExistCompanypath);


                                if (!exists)
                                    System.IO.Directory.CreateDirectory(isExistCompanypath);


                                var path = Path.Combine(isExistCompanypath, newfilename);
                                stfile.SaveAs(path);
                                imgentity.styleimg = newfilename;
                                styleimglist.Add(imgentity);
                            }
                        }
                    }
                    
                    if (rawitemimg != null)
                    {
                        foreach (var rimg in rawitemimg)
                        {

                            if (rimg != null && rimg.ContentLength > 0)
                            {
                                var filename = Path.GetFileName(rimg.FileName);
                                var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                                string isExistCompanypath = Server.MapPath("~/uploadedrawitem/");
                                bool exists = System.IO.Directory.Exists(isExistCompanypath);


                                if (!exists)
                                    System.IO.Directory.CreateDirectory(isExistCompanypath);


                                var path = Path.Combine(isExistCompanypath, newfilename);
                                rimg.SaveAs(path);
                                rawimglist.Add(newfilename);
                                //styleimgpath = newfilename;
                                //styleimglist.Add(styleimgpath);
                            }
                        }
                    }

                    if (ItemAttrimages_id != null)
                    {
                        foreach (var rimg in ItemAttrimages_id)
                        {
                            if (rimg != null && rimg.ContentLength > 0)
                            {
                                var filename = Path.GetFileName(rimg.FileName);
                                var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));
                                string isExistImagepath = Server.MapPath("~/ItemImages/");
                                bool exists = System.IO.Directory.Exists(isExistImagepath);
                                if (!exists)
                                    System.IO.Directory.CreateDirectory(isExistImagepath);
                                var path = Path.Combine(isExistImagepath, newfilename);
                                rimg.SaveAs(path);
                                AttributeImage.Add(newfilename);
                            }
                        }
                    }
                }

                var enquiryid = Convert.ToInt64(frm["Id"]);

                CRM_EnquiryMasterEntity enqentity = new CRM_EnquiryMasterEntity();
                enqentity.EnquiryId = enquiryid;
                enqentity.BrokerId = Convert.ToInt64(frm["BrokerId"]);
                enqentity.narration =Convert.ToString(frm["narration"]);
                enqentity.Comment = Convert.ToString(frm["comment"]);
                enqentity.Enquirytype = Convert.ToInt64(frm["EnquiryType"]);
                if (roleid!=4)
                {
                    enqentity.Approvalstatus = Convert.ToString(frm["approvalstatus"]) == "1" ? true : false;
                    enqentity.IsStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? 3 : 5;
                    enqentity.Approvaldate = DateTime.Now;
                }
                else
                {
                    enqentity.IsStatus = 2;
                }

                enqentity.UpdatedBy = userid;
                enqentity.Company_ID = companyid;
                enqentity.BranchId = branchid;

                var rvalue = frm["sample"];
                if (rvalue == "yes")
                    enqentity.Sampling = true;
                else
                    enqentity.Sampling = false;

                enqentity.imgelist = styleimglist;
                bool isenqupdate = enquiryservice.Update(enqentity);


                var styleitemstrlist = Convert.ToString(frm["styleliststr"]);

                //segment  item style brand stydesc color shade image reqqty sizeratio 
                string[] stylearr = styleitemstrlist.Split('#');
                foreach (var arritem in stylearr)
                {
                    if (arritem != "")
                    {
                        string[] styleitem = arritem.Split('~');
                        CRM_EnquiryStyleDetailEntity styleentity = new CRM_EnquiryStyleDetailEntity();

                        styleentity.Id = Convert.ToInt64(styleitem[0]);
                        styleentity.Segmenttypeid = Convert.ToInt32(styleitem[1]);
                        styleentity.Itemid = Convert.ToInt32(styleitem[3]);
                        styleentity.Styleid = Convert.ToInt32(styleitem[2]);
                        styleentity.Brand = styleitem[5];
                        styleentity.Styledesc = styleitem[4];
                        styleentity.Styleno = Convert.ToString(frm["styleno"]);
                        decimal val = Convert.ToDecimal(frm["reqqty"]);
                        styleentity.Totalqty = Convert.ToDecimal(val);
                        styleentity.Attribute_ID = Convert.ToString(styleitem[7]);
                        styleentity.Attribute_Value_ID = Convert.ToString(styleitem[8]);
                        styleentity.rate = Convert.ToDecimal(styleitem[10]);
                        styleentity.SizeId = Convert.ToInt64(styleitem[11]);
                        styleentity.Updatedby = userid;
                        styleentity.Updateddate = DateTime.Now;

                        if (styleitem[8] != "")
                        {
                            decimal qty = Convert.ToDecimal(styleitem[9]);
                            styleentity.Reqqty = qty;
                        }
                        styleentity.IsActive = true;
                        int ICount = Convert.ToInt32(styleitem[12]);
                        int k = 0;
                        List<string> imgdata = new List<string>();
                        var Attrimgarray = AttributeImage.ToList();
                        foreach (var img in Attrimgarray)
                        {
                            if (k != ICount)
                            {
                                if (img != "")
                                {
                                    imgdata.Add(img.ToString());
                                    AttributeImage.RemoveAt(0);
                                }
                                else
                                {
                                    AttributeImage.RemoveAt(0);
                                }
                            }
                            else
                            {
                                break;
                            }
                            k++;
                        }
                        styleentity.itemimg_list = imgdata;
                        bool isupdatestyle = enquirystyledetailservice.Update(styleentity);
                    }
                }

                var rawitemliststr =Convert.ToString(frm["rawitemliststr"]);

              
                    //code to update rawitem
                    string[] rawitemstr = rawitemliststr.Split('#');
                string[] rawimgarray = rawimglist.ToArray();
                int count = 0;

                foreach (var rawitem in rawitemstr)
                {
                    if (rawitem != "")
                    {
                        string[] raw = rawitem.Split('~');
                        //id itemcat itemsub itemdesc supplier
                        CRM_EnquiryRawitemDetailEntity rawentity = new CRM_EnquiryRawitemDetailEntity();
                        if (rawitemliststr.Length != 0)
                        {

                            rawentity.Enquiryid = enquiryid;
                            if (raw[0] != "")
                            {
                                rawentity.Id = Convert.ToInt32(raw[0]);
                            }
                            if (raw[1] != null)
                            {
                                rawentity.Itemcategoryid = Convert.ToInt32(raw[1]);
                            }

                            if (raw[2] != null)
                            {
                                rawentity.Itemsubcategoryid = Convert.ToInt32(raw[2]);
                            }
                            rawentity.Itemdesc = Convert.ToString(raw[3]);
                            rawentity.Supplier = Convert.ToString(raw[4]);
                           // rawentity.rawitemimg = rawimgarray[count];
                            rawentity.Updatedby = userid;
                            rawentity.Createdby = userid;
                            rawentity.Updateddate = DateTime.Now;
                            rawentity.Createdate = DateTime.Now;
                            int RICount = Convert.ToInt32(rawitem[5]);
                            int k = 0;
                            var Rawimgarray = rawimglist.ToList();
                            List<string> rawimgdata = new List<string>();
                            foreach (var img in Rawimgarray)
                            {
                                if (k != RICount)
                                {
                                    if (img != "")
                                    {
                                        rawimgdata.Add(img.ToString());
                                        rawimglist.RemoveAt(0);
                                    }
                                    else
                                    {
                                        rawimglist.RemoveAt(0);
                                    }
                                }
                                else
                                {
                                    break;
                                }
                                k++;
                            }
                            rawentity.rawitemimg_list = rawimgdata;
                            if (rawentity.Id != 0)
                            {
                                bool updated = enquiryrawitemservice.Update(rawentity);
                            }
                            else
                            {
                                long rinsertid = enquiryrawitemservice.Insert(rawentity);
                            }
                            count++;
                        }
                    }
                }


                TempData["alertmsg"] = "Record Update Successfully";
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                
                    return RedirectToAction("Index");

                
            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");
            }
        }

        [RBAC]
        public JsonResult editrawitem(long id, long itemcatid, int itemsubid, string itemdesc, string supplier)
        {
            CRM_EnquiryRawitemDetailEntity rawentity = new CRM_EnquiryRawitemDetailEntity();
            rawentity.Id = id;
            //rawentity.Enquiryid = enquiryid;
            rawentity.Itemcategoryid = itemcatid;
            rawentity.Itemsubcategoryid = itemsubid;
            rawentity.Itemdesc = itemdesc;
            rawentity.Supplier = supplier;
            bool isupdate = enquiryrawitemservice.Update(rawentity);
            return Json(isupdate);

        }

        [RBAC]
        public JsonResult deleterawitem(long id)
        {
            var isdelete = enquiryrawitemservice.Delete(id);
            return Json(isdelete);

        }

        [RBAC]
        public JsonResult delteenquiry(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Userid = Convert.ToInt32(collection.Id);
            var isdelete = enquiryservice.Delete(id, Userid);
            return Json(isdelete);
        }

        //Rejected Enquiry Code
        [RBAC]
        public JsonResult Rejectedenquiry(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Uid = Convert.ToInt32(collection.Id);

            var isdelete = enquiryservice.Rejectedenquiry(id, Uid);
            return Json(isdelete);
        }

        [RBAC]
        public JsonResult getenquiry(long custid)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            var records = enquiryservice.getEnquirynobycustomerid(custid, Company_ID, BranchId);
            return Json(records);
        }

        [RBAC]
        public JsonResult getenquirydetails(long id)
        {
            // JavaScriptSerializer objJavascript = new JavaScriptSerializer();
            CRM_EnquiryMasterEntity entity = new CRM_EnquiryMasterEntity();
            entity = enquiryservice.getbyid(id);
            entity.StyleEnt = enquiryservice.GetEnqryStyleData(id);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(entity);
            return Json(JSONString);
        }

        //public PartialViewResult CreateCustomer()
        //{
        //    return PartialView();
        //}

        [RBAC]
        public PartialViewResult _CreateEntity()
        {
           
            return PartialView();
        }

        [RBAC]
        [HttpPost]
        public JsonResult EditAttributeValue(string MyJson, int type, int id)
        {
            string myjson = "";
            myjson = "[{\"EnqId\":\"" + id + "\"}]";
            XmlDocument doc = new XmlDocument();
            doc = JsonConvert.DeserializeXmlNode("{\"Row\":" + myjson + "}", "root"); // JSON needs to be an objec
            var entity = enquirystyledetailservice.EditAttributeValue(MyJson, type, doc);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(entity);
            return Json(JSONString);
        }

        [RBAC]
        [HttpPost]
        public JsonResult getaddress(long custid)
        {
            var list = ledgeraccountservices.GetAddressById(custid);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(list);
            return Json(JSONString);
        }

        [RBAC]
        [HttpPost]
        public JsonResult GetStyleData(long styleid)
        {
            var list = styleservice.get().Where(x => x.Id == styleid).FirstOrDefault();
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(list);
            return Json(JSONString);
            
        }

        [RBAC]
        public JsonResult SearchList(string MyJson, int type,string id, long screenId)
        {
            XmlDocument doc = new XmlDocument();
            doc = JsonConvert.DeserializeXmlNode("{\"Row\":" + MyJson + "}", "root"); // JSON needs to be an objec
            var entity = enquiryservice.SearchList(doc, type, id, screenId);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(entity);
            return Json(JSONString);
        }

        [RBAC]
        [HttpPost]
        public JsonResult StatusCount(int Type,string attribute)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            int roleid = Convert.ToInt32(collection.RoleID);
            int deptid = Convert.ToInt32(collection.DeptID);
            int userid = Convert.ToInt32(collection.Id);
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            string myjson = "";
            myjson = "[{\"companyid\":\"" + companyid + "\",\"branchid\":\"" + branchid + "\",\"userid\":\"" + userid + "\",\"roleid\":\"" + roleid + "\"}]";
            XmlDocument doc = new XmlDocument();
            doc = JsonConvert.DeserializeXmlNode("{\"Row\":" + myjson + "}", "root");

            var entity = enquiryservice.StatusCount(Type, attribute, doc);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(entity);
            return Json(JSONString);
        }
        

        [RBAC]
        public JsonResult SaveEntity(string LedgerType,string Ledgername,string mobno,string add,string email)
        {
            long id = 0;
            List<object> listdata = new List<object>();
            var Ldata = ledgeraccountservices.get().Where(x=>x.LedgerType_Id.Contains(LedgerType) && x.Ledger_Name.Contains(Ledgername)).FirstOrDefault();
            if (Ldata != null)
            {
                id = Ldata.Ledger_Id;
            }
            else
            {
                M_LedgersEntity entity = new M_LedgersEntity();

                var Name = Ledgername;
                var entitytype = LedgerType;
                var Department = 0;

                // var customerGSTNo = frm["GSTNo"];
                var PANNO = "";
                var Description = "";

                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.Ledger_Name = Name;
                entity.LedgerType_Id = entitytype;
                entity.PAN_no = PANNO;
                entity.Ledger_Description = Description;
                entity.Is_Active = true;
                entity.Created_Date = DateTime.Now;
                //New Code Added by Rahul on 09-01-2020
                entity.Created_By = Convert.ToInt32(collection.Id);
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.Branch_ID = Convert.ToInt32(collection.BranchId);

                //code to save contact address details
                //Added new filed company and branch by ,Rahul on,03-01-2020
                List<M_Ledger_BillingDetailsEntity> addresslistentity = new List<M_Ledger_BillingDetailsEntity>();
                M_Ledger_BillingDetailsEntity contactaddressentity = new M_Ledger_BillingDetailsEntity();

                contactaddressentity.Billing_Name = "";
                contactaddressentity.Unit = "";
                contactaddressentity.Contact_No = mobno;
                contactaddressentity.Telephone_no = "";
                contactaddressentity.Email = email;
                contactaddressentity.Fax_no = "";
                contactaddressentity.Address = add;
                contactaddressentity.City_Id = 18258;
                contactaddressentity.State_Id = 1646;
                contactaddressentity.Country_Id = 101;
                contactaddressentity.GSTIN = "";
                contactaddressentity.Bank_Name = "";
                contactaddressentity.Bank_code = "";
                contactaddressentity.Account_no = "";
                contactaddressentity.Is_SEZ = false;
                contactaddressentity.Company = "";
                contactaddressentity.Branch = "";
                //New Code Added by Rahul on 09-01-2020
                contactaddressentity.Created_By = Convert.ToInt32(collection.Id);
                contactaddressentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                contactaddressentity.Branch_ID = Convert.ToInt32(collection.BranchId);
                contactaddressentity.Created_Date = DateTime.Now;
                contactaddressentity.Is_Active = true;
                addresslistentity.Add(contactaddressentity);
                entity.M_Ledger_BillingDetails = addresslistentity;
                id = ledgeraccountservices.Insert(entity);
            }
            if (id > 0)
            {
                var model = ledgeraccountservices.GetById(id);
                listdata.Add(model);
                var addlist = ledgeraccountservices.GetAddressById(id);
                listdata.Add(addlist);
            }
            return Json(listdata);

        }

        //New Code Start 29-12-2020
        public PartialViewResult MaterialDetails(ItemEntityModel ItemEntity)
        {
            return PartialView("_MaterialDetails");
        }

        public JsonResult GetEnqryStyleData(long EnquiryId)
        {
            var list = enquiryservice.GetEnqryStyleData(EnquiryId);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(list);
            return Json(JSONString);
        }

        public JsonResult GetEnqryStyleRawItemData(long EnquiryId,long styleid)
        {
            var list = enquiryservice.GetEnqryStyleRawItemData(EnquiryId, styleid);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(list);
            return Json(JSONString);
        }

    }
}
