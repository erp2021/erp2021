﻿using Garments_ERP.Entity;
using Garments_ERP.Entity.CommonEntities;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Newtonsoft.Json;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{

    public class SupplierPOController : SuperController
    {
        //
        // GET: /SupplierPO/
        CustomerPOService customerpo = ServiceFactory.GetInstance().CustomerPOService;
        SupplierService supplierservice = ServiceFactory.GetInstance().SupplierService;
        SupplierContactService suppliercontactservice = ServiceFactory.GetInstance().SupplierContactService;
        SupplierQuotationService supplierquotationservice = ServiceFactory.GetInstance().SupplierQuotationService;
        SupplierQuotationItemDetailService supplierquotationitemdetailservice = ServiceFactory.GetInstance().SupplierQuotationItemDetailService;
        SupplierPOService supplierposervice = ServiceFactory.GetInstance().SupplierPOService;
        RateIndicatorService rateindicatorservice = ServiceFactory.GetInstance().RateIndicatorService;
        TaxService taxservice = ServiceFactory.GetInstance().TaxService;
        PaymentTermService paymenttermservice = ServiceFactory.GetInstance().PaymentTermService;
        WarehouseService warehouseservice = ServiceFactory.GetInstance().WarehouseService;
        LedgerAccountServices ledgeraccountservices = ServiceFactory.GetInstance().LedgerAccountServices;

        [RBAC]
        public JsonResult getQuotaionItemDetails(long id)
        {
            SRM_PurchaseOrderMasterEntity entity = new SRM_PurchaseOrderMasterEntity();
            entity.IsReadymade = supplierquotationservice.getbyid(id).IsReadymade;
            entity.POOtherChargeEntity = supplierquotationservice.getbyid(id).POOtherChargeEntity;
            entity.rawitemlist = supplierquotationitemdetailservice.getbyquotationid(id);
            entity.taxlist = taxservice.get();
            // entity.rateindicatorlist = rateindicatorservice.get();
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(entity);
            return Json(JSONString);
        }

        [RBAC]
        public JsonResult getSupplierlist(long id)
        {
            // JavaScriptSerializer objJavascript = new JavaScriptSerializer();
            SRM_QuotationMasterEntity entity = new SRM_QuotationMasterEntity();
            entity = supplierquotationservice.getbyid(id);
            return Json(entity);
        }

        [RBAC]
        public JsonResult getTaxValue(long id)
        {
            SRM_PurchaseOrderMasterEntity entity = new SRM_PurchaseOrderMasterEntity();
            entity.taxvaluelist = taxservice.getbyid(id);
            return Json(entity);
        }

        [RBAC]
        public JsonResult getConatctlist(long id)
        {
            SRM_PurchaseOrderMasterEntity entity = new SRM_PurchaseOrderMasterEntity();
            entity.M_SupplierContacts = suppliercontactservice.GetById(id);
            return Json(entity);
        }

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Supplier PO";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            return View();
        }


        //Delete Supplier PO Code
        [RBAC]
        [HttpPost]
        public JsonResult DeletePO(long Id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Userid = Convert.ToInt32(collection.Id);

            bool ISDeleted = false;
            ISDeleted = supplierposervice.Delete(Id, Userid);
            return Json(ISDeleted);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Supplier PO";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            SRM_PurchaseOrderMasterEntity entity = new SRM_PurchaseOrderMasterEntity();
            string[] PO_NO = supplierposervice.getnextPONo().Split('-');
            entity.PO_NO = PO_NO[0] + "-" + PO_NO[1] + "-" + PO_NO[2] + "-" + PO_NO[3] + "-XXXX";

            ViewBag.OtherCharge = DropDownList<M_POOtherChargesMasterEntity>.LoadItems(customerpo.getOtherCharge(), "id", "POOtherChargeName", 0);
            entity.rateindicatorlist = rateindicatorservice.get();
            ViewBag.dellocationlist = DropDownList<M_WarehouseEntity>.LoadItems(warehouseservice.get(ViewBag.companyid, ViewBag.branchid), "ID", "SHORT_NAME", 0);
            ViewBag.paymenttermlist = DropDownList<M_PaymentTermMasterEntity>.LoadItems(paymenttermservice.get(), "Id", "PaymentTerm", 0);
            //ViewBag.quotationnolist = DropDownList<SRM_QuotationMasterEntity>.LoadItems(supplierquotationservice.get().Where(x => x.POStatus == false && x.IsActive == true && x.Company_ID == ViewBag.companyid && x.BranchId == ViewBag.branchid && x.IsStatus == 3).ToList(), "Id", "QuotationNo", 0);
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(SRM_PurchaseOrderMasterEntity entity, FormCollection frm)
        {
            try
            {
                // var POType = frm["POType"];
                var Itemtotal = frm["ItemTotal"];
                var taxtotal = frm["TaxAmount"];
                var totalCost = frm["total"];
                var GrandTotal = frm["Grandtotal"];
                var contactperson = frm["contactperson"];
                var Quodt = Convert.ToString(frm["QuotationDate"]);
                entity.QuotationDate = DateTime.ParseExact(Quodt, "MM/dd/yyyy", CultureInfo.InvariantCulture);

                var podt = Convert.ToString(frm["PO_Date"]);
                entity.PO_Date = DateTime.ParseExact(podt, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var vadif = Convert.ToString(frm["POValidityFrom"]);
                entity.POValidityFrom = DateTime.ParseExact(vadif, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var vadt = Convert.ToString(frm["POValidityTo"]);
                entity.POValidityTo = DateTime.ParseExact(vadt, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                // entity.POType = POType;

                string approvdate = frm["approvaldate"].ToString();
                DateTime appdate = DateTime.ParseExact(approvdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.Approvaldate = appdate;

                entity.ItemTotal = Itemtotal == "" ? 0 : Convert.ToDecimal(Itemtotal);
                entity.TaxTotal = taxtotal==""?0: Convert.ToDecimal(taxtotal);
                entity.totalcost = totalCost == "" ? 0: Convert.ToDecimal(totalCost);
                entity.GrandTotal = GrandTotal==""?0: Convert.ToDecimal(GrandTotal);
                entity.ContactPersonId = Convert.ToInt64(contactperson);
                entity.Createddate = DateTime.Now;
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.Craetedby = Convert.ToInt32(collection.Id);
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.BranchId = Convert.ToInt32(collection.BranchId);
                entity.Comment = Convert.ToString(frm["comment"]);
                entity.IsReadymade = Convert.ToString(frm["IsReadymate"]) == "" ? 0 : Convert.ToInt32(frm["IsReadymate"]);

                int roleid = Convert.ToInt32(collection.RoleID);
                if (roleid != 4)
                {
                    entity.Approvalstatus = Convert.ToString(frm["approvalstatus"]) == "1" ? true : false;
                    entity.IsStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? 3 : 5;
                    entity.Approvaldate = DateTime.Now;
                }
                else
                {
                    entity.Approvalstatus = false;
                    entity.IsStatus = 2;
                }
                entity.IsActive = true;

                string otherchargeStr = Convert.ToString(frm["otherchargestr"]);
                if (otherchargeStr != "")
                {
                    string[] OCSplit = otherchargeStr.Split('#');
                    List<T_POOtherChargeEntity> oclist = new List<T_POOtherChargeEntity>();
                    foreach (var ocdata in OCSplit)
                    {
                        if (ocdata != "")
                        {
                            var data = ocdata.Split('~');
                            T_POOtherChargeEntity ent = new T_POOtherChargeEntity();
                            ent.OtherChargeId = Convert.ToInt32(data[0]);
                            ent.OtherChargeValue = Convert.ToDecimal(data[1]);
                            ent.GST = Convert.ToDecimal(data[2]);
                            ent.InventoryModeId = 10008; //Supplier PO
                            ent.Company_ID = Convert.ToInt32(collection.Company_ID);
                            ent.BranchId = Convert.ToInt32(collection.BranchId);
                            ent.CreatedBy = Convert.ToInt32(collection.Id);
                            oclist.Add(ent);

                        }
                    }
                    entity.POOtherChargeEntity = oclist;
                }
                
                string politelist = frm["POItemList"].ToString();
                string[] poitemarr = politelist.Split('#');
                List<SRM_PurchaseOrderItemDetailEntity> poitemlist = new List<SRM_PurchaseOrderItemDetailEntity>();
                foreach (var arritem in poitemarr)
                {
                    if (arritem != "")
                    {
                        string[] item = arritem.Split('~');
                        SRM_PurchaseOrderItemDetailEntity poitementity = new SRM_PurchaseOrderItemDetailEntity();
                        poitementity.ItemId = Convert.ToInt64(item[0]);
                        poitementity.Item_Attribute_ID = Convert.ToInt32(item[1]);
                        poitementity.UnitId = Convert.ToInt32(item[2]);
                        poitementity.TotalQty = Convert.ToDecimal(item[3]);
                        poitementity.Rate = Convert.ToDecimal(item[4]);
                        poitementity.RateIndicatorId = Convert.ToInt32(item[5]);
                        poitementity.Provision = Convert.ToDecimal(item[6]);
                        poitementity.Wastage = Convert.ToDecimal(item[7]);
                        poitementity.StockQty = Convert.ToDecimal(item[8]);
                        poitementity.POQty = Convert.ToDecimal(item[9]);
                        poitementity.TaxId = Convert.ToInt32(item[10]);
                        poitementity.TaxValue = Convert.ToDecimal(item[11]);
                        poitementity.Amount = Convert.ToDecimal(item[12]);
                        poitementity.IsActive = true;
                        poitementity.CreatedBy = Convert.ToInt32(collection.Id);
                        poitementity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        poitementity.BranchId = Convert.ToInt32(collection.BranchId);
                        poitemlist.Add(poitementity);
                    }
                }
                entity.SRM_PurchaseOrderItemDetail = poitemlist;

                //Code to add PO delivery details   -Ramesh Take on 24-Jan-2020
                string poschedules = frm["po_delivery_schedules"];
                string[] scheduleArr = poschedules.Split('#');
                List<MFG_PO_DeliveryDetail_Entity> po_delivery_Schedules = new List<MFG_PO_DeliveryDetail_Entity>();
                foreach (var schedule in scheduleArr)
                {
                    if (schedule != "")
                    {
                        string[] record_ = schedule.Split('~');
                        MFG_PO_DeliveryDetail_Entity po_delentity = new MFG_PO_DeliveryDetail_Entity();
                        po_delentity.Ledger_Id = Convert.ToInt32(entity.SupplierId);//Convert.ToInt32(frm["supplierid"]);
                        DateTime delDate_ = DateTime.ParseExact(record_[0], "MM/dd/yyyy", CultureInfo.InvariantCulture);
                        po_delentity.Delivery_Date = delDate_;
                        po_delentity.Delivery_Qty = Convert.ToDecimal(record_[1]);
                        po_delentity.Comments = record_[2];
                        po_delentity.Item_ID = Convert.ToInt64(record_[4]);
                        po_delentity.Item_Attribute_Id = Convert.ToInt32(record_[5]);
                        po_delentity.Created_By = Convert.ToInt32(collection.Id);
                        po_delentity.Created_Date = DateTime.Now;
                        po_delentity.IsActive = true;
                        //po_delentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        //po_delentity.BranchId = Convert.ToInt32(collection.BranchId);
                        po_delivery_Schedules.Add(po_delentity);
                    }
                }
                entity.MFG_PO_DeliveryDetail = po_delivery_Schedules;


                long insertedid = supplierposervice.Insert(entity);
                if (insertedid > 0)
                {
                    TempData["alertmsg"] = "Puchase Order Creation Successful.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");


        }

        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "Supplier PO";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            SRM_PurchaseOrderMasterEntity entity = new SRM_PurchaseOrderMasterEntity();
            entity = supplierposervice.getbyid(id);
            entity.taxlist = taxservice.get();
            if (entity.ContactPersonId != null)
            {
                ViewBag.contactslist = DropDownList<M_Ledger_BillingDetailsEntity>.LoadItems(ledgeraccountservices.GetAddressById((long)entity.SupplierId), "Id", "Billing_Name", (int)entity.ContactPersonId);
            }
            else
            {
                ViewBag.contactslist = DropDownList<M_SupplierContactDetailsEntity>.LoadItems(suppliercontactservice.GetById((long)entity.SupplierId), "Id", "Contactpersonname", 0);
            }
            if (entity.DeliveryLoaction != null)
            {
                ViewBag.dellocationlist = DropDownList<M_WarehouseEntity>.LoadItems(warehouseservice.get(ViewBag.companyid, ViewBag.branchid), "ID", "SHORT_NAME", (int)entity.DeliveryLoaction);
            }
            else
            { ViewBag.dellocationlist = DropDownList<M_WarehouseEntity>.LoadItems(warehouseservice.get(ViewBag.companyid, ViewBag.branchid), "ID", "SHORT_NAME", 0); }
            if (entity.PaymentTerm != null)
            {
                ViewBag.paymenttermlist = DropDownList<M_PaymentTermMasterEntity>.LoadItems(paymenttermservice.get(), "Id", "PaymentTerm", (int)entity.PaymentTerm);
            }
            else
            {
                ViewBag.paymenttermlist = DropDownList<M_PaymentTermMasterEntity>.LoadItems(paymenttermservice.get(), "Id", "PaymentTerm", 0);
            }
            ViewBag.OtherCharge = DropDownList<M_POOtherChargesMasterEntity>.LoadItems(customerpo.getOtherCharge(), "id", "POOtherChargeName", 0);
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(long id, SRM_PurchaseOrderMasterEntity entity, FormCollection frm)
        {
            try
            {
                // var POType = frm["POType"];
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                int roleid = Convert.ToInt32(collection.RoleID);
                int deptid = Convert.ToInt32(collection.DeptID);
                int userid = Convert.ToInt32(collection.Id);
                int companyid = Convert.ToInt32(collection.Company_ID);
                int branchid = Convert.ToInt32(collection.BranchId);

                var Itemtotal = frm["ItemTotal"];
                var taxtotal = frm["TaxAmount"];
                var totalCost = frm["total"];
                var GrandTotal = frm["Grandtotal"];
                // entity.POType = POType;
                var podt = Convert.ToString(frm["PO_Date"]);
                entity.PO_Date = DateTime.ParseExact(podt, "MM/dd/yyyy", CultureInfo.InvariantCulture);

                var vadif = Convert.ToString(frm["POValidityFrom"]);
                entity.POValidityFrom = DateTime.ParseExact(vadif, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var vadt = Convert.ToString(frm["POValidityTo"]);
                entity.POValidityTo = DateTime.ParseExact(vadt, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.ItemTotal = Itemtotal == "" ? 0 : Convert.ToDecimal(Itemtotal);
                entity.TaxTotal = taxtotal == "" ? 0 : Convert.ToDecimal(taxtotal);
                entity.totalcost = totalCost == "" ? 0 : Convert.ToDecimal(totalCost);
                entity.GrandTotal = GrandTotal == "" ? 0 : Convert.ToDecimal(GrandTotal);
                entity.Updateddate = DateTime.Now;
                entity.IsActive = true;
                entity.Comment = frm["comment"].ToString();

                string approvdate = frm["approvaldate"].ToString();
                DateTime appdate = DateTime.ParseExact(approvdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.Approvaldate = appdate;

                if (roleid != 4)
                {
                    entity.Approvalstatus = Convert.ToString(frm["approvalstatus"]) == "1" ? true : false;
                    entity.IsStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? 3 : 5;
                    entity.Approvaldate = DateTime.Now;
                }
                else
                {
                    entity.IsStatus = 2;
                }
                entity.Updatedby = userid;
                entity.Company_ID = companyid;
                entity.BranchId = branchid;


                string otherchargeStr = Convert.ToString(frm["otherchargestr"]);
                if (otherchargeStr != "")
                {
                    string[] OCSplit = otherchargeStr.Split('#');
                    List<T_POOtherChargeEntity> oclist = new List<T_POOtherChargeEntity>();
                    foreach (var ocdata in OCSplit)
                    {
                        if (ocdata != "")
                        {
                            var data = ocdata.Split('~');
                            T_POOtherChargeEntity ent = new T_POOtherChargeEntity();
                            ent.OtherChargeId = Convert.ToInt32(data[0]);
                            ent.OtherChargeValue = Convert.ToDecimal(data[1]);
                            ent.GST = Convert.ToDecimal(data[2]);
                            ent.InventoryModeId = 10008; //Supplier PO
                            ent.Company_ID = Convert.ToInt32(collection.Company_ID);
                            ent.BranchId = Convert.ToInt32(collection.BranchId);
                            ent.CreatedBy = Convert.ToInt32(collection.Id);
                            ent.Id = Convert.ToInt64(data[5]);
                            oclist.Add(ent);

                        }
                    }
                    entity.POOtherChargeEntity = oclist;
                }



                string politelist = frm["POItemList"].ToString();
                string[] poitemarr = politelist.Split('#');
                List<SRM_PurchaseOrderItemDetailEntity> poitemlist = new List<SRM_PurchaseOrderItemDetailEntity>();
                foreach (var arritem in poitemarr)
                {
                    if (arritem != "")
                    {
                        string[] item = arritem.Split('~');
                        SRM_PurchaseOrderItemDetailEntity poitementity = new SRM_PurchaseOrderItemDetailEntity();
                        poitementity.ItemId = Convert.ToInt64(item[0]);
                        poitementity.Item_Attribute_ID = Convert.ToInt32(item[1]);
                        poitementity.UnitId = Convert.ToInt32(item[2]);
                        poitementity.TotalQty = Convert.ToDecimal(item[3]);
                        poitementity.Rate = Convert.ToDecimal(item[4]);
                        poitementity.RateIndicatorId = Convert.ToInt32(item[5]);
                        poitementity.Provision = Convert.ToDecimal(item[6]);
                        poitementity.Wastage = Convert.ToDecimal(item[7]);
                        poitementity.StockQty = Convert.ToDecimal(item[8]);
                        poitementity.POQty = Convert.ToDecimal(item[9]);
                        poitementity.TaxId = Convert.ToInt32(item[10]);
                        poitementity.TaxValue = Convert.ToDecimal(item[11]);
                        poitementity.Amount = Convert.ToDecimal(item[12]);
                        poitementity.Id = Convert.ToInt64(item[13]);
                        poitementity.IsActive = true;
                        poitementity.UpdatedBy = userid;
                        poitemlist.Add(poitementity);
                    }
                }
                entity.SRM_PurchaseOrderItemDetail = poitemlist;

                //Code to add PO delivery details   -Ramesh Take on 24-Jan-2020
                string poschedules = frm["po_delivery_schedules"];
                string[] scheduleArr = poschedules.Split('#');
                List<MFG_PO_DeliveryDetail_Entity> po_delivery_Schedules = new List<MFG_PO_DeliveryDetail_Entity>();
                foreach (var schedule in scheduleArr)
                {
                    if (schedule != "")
                    {
                        string[] record_ = schedule.Split('~');
                        MFG_PO_DeliveryDetail_Entity po_delentity = new MFG_PO_DeliveryDetail_Entity();
                        po_delentity.Ledger_Id = Convert.ToInt32(entity.SupplierId);
                        DateTime delDate_ = DateTime.ParseExact(record_[0], "MM/dd/yyyy", CultureInfo.InvariantCulture);
                        po_delentity.Delivery_Date = delDate_;
                        po_delentity.Delivery_Qty = Convert.ToDecimal(record_[1]);
                        po_delentity.Comments = record_[2];
                        po_delentity.Item_ID = Convert.ToInt64(record_[4]);
                        po_delentity.Item_Attribute_Id = Convert.ToInt32(record_[5]);
                        po_delentity.PO_DelSchedule_ID = Convert.ToInt64(record_[3]);
                        po_delentity.Created_By = Convert.ToInt32(collection.Id);
                        po_delentity.Modified_By = Convert.ToInt32(collection.Id);
                        po_delentity.Modified_Date = DateTime.Now;
                        po_delentity.IsActive = true;
                        //po_delentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        //po_delentity.BranchId = Convert.ToInt32(collection.BranchId);
                        po_delivery_Schedules.Add(po_delentity);
                    }
                }
                entity.MFG_PO_DeliveryDetail = po_delivery_Schedules;


                bool updatedid = supplierposervice.Update(entity);
                if (updatedid == true)
                {
                    TempData["alertmsg"] = "Puchase Order Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");

        }

        [RBAC]
        public ActionResult SupplierPOReport(long Id)
        {

            var list = supplierposervice.getbyPONO(Convert.ToString(Id));
            return View(list);

        }

        [HttpPost]
        [ValidateInput(false)]
        public FileResult Export(string GridHtml)
        {

            // read parameters from the webpage
            string htmlString = "<html><head><title>SupplierPO</title><link rel='stylesheet' href='http://localhost:2423/Content/dist/css/AdminLTE.min.css'><link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'></head><body><div class='row'><div class='col-sm-12 text-center'><h3>Purchase Order</h3></div></div>" + GridHtml + "</body></html>";
            string baseUrl = "";

            string pdf_page_size = "A4";
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                pdf_page_size, true);

            string pdf_orientation = "Portrait";
            PdfPageOrientation pdfOrientation =
                (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                pdf_orientation, true);

            int webPageWidth = 1024;
            int webPageHeight = 0;


            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;

            converter.Options.DisplayFooter = true;
            converter.Options.MarginTop = 10;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;
            converter.Options.MarginBottom = 10;
            converter.Options.PdfCompressionLevel = PdfCompressionLevel.Best;
            converter.Options.PdfPageSize = PdfPageSize.Letter;
            converter.Options.PdfStandard = PdfStandard.Full;
            converter.Options.JavaScriptEnabled = true;
            converter.Options.DrawBackground = false;
            converter.Options.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
            converter.Options.CssMediaType = HtmlToPdfCssMediaType.Print;
            PdfTextSection text = new PdfTextSection(0, 10, "Page: {page_number} of {total_pages} ", new System.Drawing.Font("Arial", 8));
            text.HorizontalAlign = PdfTextHorizontalAlign.Right;

            // create a new pdf document converting an url
            SelectPdf.PdfDocument doc = converter.ConvertHtmlString(htmlString, baseUrl);

            // save pdf document
            byte[] pdf = doc.Save();

            // close pdf document
            doc.Close();

            // return resulted pdf document
            FileResult fileResult = new FileContentResult(pdf, "application/pdf");
            fileResult.FileDownloadName = "Document.pdf";
            return fileResult;

        }


        //Rejected Supplier PO Code
        [RBAC]
        public JsonResult RejectedPO(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Uid = Convert.ToInt32(collection.Id);

            var isdelete = supplierposervice.RejectedPO(id, Uid);
            return Json(isdelete);
        }

        //Get Status Wise List
        [RBAC]
        public PartialViewResult GetStatusData(int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            var list = supplierposervice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            return PartialView("_IndexList", list);
        }

        //Form Date And To Date Wise Filter with Status List
        [RBAC]
        public PartialViewResult GetFilterStatusData(string fromdate, string todate, int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            if (fromdate != "" && todate != "")
            {

                var fdate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var tdate = DateTime.ParseExact(todate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var list = supplierposervice.get(fdate, tdate, IsStatus, Company_ID, BranchId, Userid, roleid);
                ViewBag.fromdate = fromdate;
                ViewBag.todate = todate;
                return PartialView("_IndexList", list);
            }
            else
            {
                var list = supplierposervice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
                return PartialView("_IndexList", list);
            }

        }

        [RBAC]
        public JsonResult GetQuoList(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);

            var list = supplierquotationservice.get().Where(x => x.POStatus == false && x.IsActive == true && x.Company_ID == ViewBag.companyid && x.BranchId == ViewBag.branchid && x.IsStatus == 3 && x.SupplierId== id).ToList();
            return Json(list);
        }
    }
}
