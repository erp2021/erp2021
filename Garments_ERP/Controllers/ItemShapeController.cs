﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class ItemShapeController : Controller
    {
        //
        // GET: /ItemShape/
        ItemshadeService itemshadeservice = ServiceFactory.GetInstance().ItemshadeService;
        [RBAC]
        public ActionResult Index()
        {
            var list = itemshadeservice.getShape();
            return View(list);
        }
        [RBAC]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(M_ItemShapeMasterEntity entity)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    entity.IsActive = true;
                    long id = itemshadeservice.InsertShape(entity);
                    if (id > 0)
                    {

                        TempData["alertmsg"] = "Record Save Successfully.";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception ex)
                {
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    TempData["alertmsg"] = ex.Message;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");

        }
        public JsonResult CheckItemShape(string Itemshape, int? Id)
        {
            bool value = itemshadeservice.CheckItemShapeExistance(Itemshape, Id);
            return Json(value, JsonRequestBehavior.AllowGet);
        }
        [RBAC]
        public ActionResult Edit(int id)
        {

            M_ItemShapeMasterEntity entity = itemshadeservice.getShape().Where(x => x.Id == id).FirstOrDefault();
            return View(entity);
        }
        [HttpPost]
        public ActionResult Edit(M_ItemShapeMasterEntity entity)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    entity.Updateddate = DateTime.Now;
                    bool isupdate = itemshadeservice.UpdateShape(entity);
                    if (isupdate == true)
                    {
                        M_ItemShapeMasterEntity entity_ = new M_ItemShapeMasterEntity();
                        var list = itemshadeservice.getShape();
                        TempData["alertmsg"] = "Record Updated Successfully.";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception ex)
                {
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    TempData["alertmsg"] = ex.Message;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");
        }
        [RBAC]
        public ActionResult Delete(int id)
        {
            bool delete = itemshadeservice.DeleteShape(id);
            if (delete == true)
            {
                return Json(delete);
            }
            return Json(delete);
        }
    }
}
