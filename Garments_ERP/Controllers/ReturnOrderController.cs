﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using System.Globalization;
using Garments_ERP.Filters;
using Garments_ERP.Entity.ProductionEntities;
using System.Xml;
using Newtonsoft.Json;

namespace Garments_ERP.Controllers
{
    public class ReturnOrderController : SuperController
    {
        CustomerPOService customerpo = ServiceFactory.GetInstance().CustomerPOService;
        ReturnOrderService ROservice = ServiceFactory.GetInstance().ReturnOrderService;
        InvoiceChallanService invoicechallanservice = ServiceFactory.GetInstance().InvoiceChallanService;
        SupplierPOService supplierposervice = ServiceFactory.GetInstance().SupplierPOService;
        WorkorderService woservice = ServiceFactory.GetInstance().WorkorderService;
        MaterialIssueService materialissueservice = ServiceFactory.GetInstance().MaterialIssueService;
        ProcessInchargeService ProcessInchargeservice = ServiceFactory.GetInstance().ProcessInchargeService;

        //
        // GET: /ReturnOrder/
        [RBAC]
        public ActionResult Index()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            return View();
        }

        //Get Status Wise List
        [RBAC]
        public PartialViewResult GetStatusData()
        {
            var list = ROservice.get();
            return PartialView("_IndexList", list);
        }


        [RBAC]
        public ActionResult Create()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            string[] rono = ROservice.getnextROno().Split('-');
            ViewBag.rono = rono[0] + "-" + rono[1] + "-" + rono[2] + "-" + rono[3] + "-XXXX";
            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(FormCollection frm)
        {
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                T_ISSUE_RETURNSEntity entity = new T_ISSUE_RETURNSEntity();

                string reuturndate = Convert.ToString(frm["reuturndate"]);
                DateTime returndates = DateTime.ParseExact(reuturndate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.RETURNS_DATE = returndates;
                entity.RETURNS_DESC = Convert.ToString(frm["remark"]);
                entity.CREATED_BY = Convert.ToDecimal(collection.Id);
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.BranchId = Convert.ToInt32(collection.BranchId);
                entity.STYPE = Convert.ToString(frm["typeid"]);
                entity.BUYER_SELLER = Convert.ToInt64(frm["BSOid"]);
                entity.PO_ID = Convert.ToInt64(frm["POid"]);
                string POdate = Convert.ToString(frm["podate"]);
                DateTime POdates = DateTime.ParseExact(POdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.PO_DATE = POdates;
                string woid = Convert.ToString(frm["WOid"]);
                entity.WO_ID = woid != null ? Convert.ToInt64(frm["WOid"]) : 0;
                if (woid != null)
                {
                    string WOdate = Convert.ToString(frm["wodate"]);
                    DateTime WOdates = WOdate != "" ? DateTime.ParseExact(WOdate, "MM/dd/yyyy", CultureInfo.InvariantCulture) : DateTime.Now;
                    entity.WO_DATE = WOdates;
                }
                string invid = Convert.ToString(frm["invoicenoid"]);
                entity.INVOICE_ID = invid != null ? Convert.ToInt64(frm["invoicenoid"]) : 0;
                if (invid != null)
                {
                    string invoicedate = Convert.ToString(frm["invoicedate"]);
                    DateTime invoicedates = invoicedate != "" ? DateTime.ParseExact(invoicedate, "MM/dd/yyyy", CultureInfo.InvariantCulture) : DateTime.Now;
                    entity.INV_DATE = invoicedates;
                }
                string MIid = Convert.ToString(frm["MInoid"]);
                entity.MATERIAL_ISSUE_ID = MIid != null ? Convert.ToInt64(frm["MInoid"]) : 0;

                string MTnoid = Convert.ToString(frm["MTnoid"]);
                entity.TRANSFER_ID = MTnoid != null ? Convert.ToInt64(frm["MTnoid"]) : 0;
                if (MTnoid != null)
                {
                    string transferdate = Convert.ToString(frm["transferdate"]);
                    DateTime transferdates = transferdate != "" ? DateTime.ParseExact(transferdate, "MM/dd/yyyy", CultureInfo.InvariantCulture) : DateTime.Now;
                    entity.TRANSFER_DATE = transferdates;
                }
                entity.MODE_ID = Convert.ToString(frm["typeid"]) == "customer" ? 10009 : (Convert.ToString(frm["typeid"]) == "supplier" ? 10008 : 2);

                string itemstr = Convert.ToString(frm["returnItemstr"]);
                string[] spltitemstr = itemstr.Split('#');

                List<T_ISSUE_RETURNS_DETAILEntity> itementity = new List<T_ISSUE_RETURNS_DETAILEntity>();

                foreach (var arritem in spltitemstr)
                {
                    if (arritem != "")
                    {
                        string[] rtnitem = arritem.Split('~');
                        if (Convert.ToDecimal(rtnitem[7]) > 0)
                        {
                            T_ISSUE_RETURNS_DETAILEntity ient = new T_ISSUE_RETURNS_DETAILEntity();
                            ient.ITEM_ID = Convert.ToInt64(rtnitem[0]);
                            ient.ITEM_ATTRIBUTE_ID = Convert.ToInt32(rtnitem[1]);
                            ient.HSN_CODE = Convert.ToString(rtnitem[2]) == "hsn" ? "" : Convert.ToString(rtnitem[2]);
                            ient.UNIT = Convert.ToString(rtnitem[3]);
                            ient.PO_TRANSFER_QTY = Convert.ToDecimal(rtnitem[4]);
                            ient.ISSUED_QTY = Convert.ToDecimal(rtnitem[5]);
                            ient.BATCH_LOT_ID = Convert.ToString(rtnitem[6]) == "hsn" ? 0 : Convert.ToInt64(rtnitem[6]);
                            ient.RETURNED_QTY = Convert.ToDecimal(rtnitem[7]);
                            ient.BALANCED_QTY = Convert.ToDecimal(rtnitem[8]);
                            ient.Company_ID = Convert.ToInt32(collection.Company_ID);
                            ient.BranchId = Convert.ToInt32(collection.BranchId);
                            itementity.Add(ient);
                        }
                    }
                }

                entity.T_ISSUE_RTRN_DETAILEntity = itementity;
                long id = ROservice.insert(entity);

                if(id>0)
                {
                    TempData["alertmsg"] = "Record Save Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertmsg"] = "Record Error! Please Fill Proper Data";
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");

            }
            
        }


        [RBAC]
        public ActionResult Edit(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            var ReturnData = ROservice.GetById(id);
            return View(ReturnData);
        }

        [RBAC]
        public JsonResult GetInvoiceList(long id)
        {
            List<object> list = new List<object>();
            var podate= customerpo.getbyid(id).PODate;
            list.Add(podate);
            var invoicedata= invoicechallanservice.Get().Where(x=>x.PO_Id== id).ToList();
            list.Add(invoicedata);
            return Json(list);
        }


        [RBAC]
        public JsonResult GetInvoiceItem(long id)
        {
            List<object> list = new List<object>();
            var INVdate = invoicechallanservice.Get().Where(x => x.InvoiceId == id).Select(x => x.InvoiceDate).FirstOrDefault();
            list.Add(INVdate);
            var invoicedata = ROservice.GetInvoiceItem(id);
            list.Add(invoicedata);
            return Json(list);
        }


        [RBAC]
        public JsonResult GetPOData(long id)
        {
            List<object> list = new List<object>();
            var podate = supplierposervice.getbyid(id).PO_Date;
            list.Add(podate);
            var poitem = ROservice.GetSPOitem(id);
            list.Add(poitem);
            return Json(list);
        }

        [RBAC]
        public JsonResult GetOperatorPOData(long id)
        {
            List<object> list = new List<object>();
            var podate = customerpo.getbyid(id).PODate;
            list.Add(podate);
            var WOdata = woservice.Get().Where(x=>x.POID==id).ToList();
            list.Add(WOdata);
            return Json(list);
        }

        [RBAC]
        public JsonResult GetWOData(long id)
        {
            List<object> list = new List<object>();
            var wodate = woservice.GetbyId(id).Date;
            list.Add(wodate);
            var MIdata = materialissueservice.get().Where(x => x.RefWO == id).ToList();
            list.Add(MIdata);
            return Json(list);
        }

        [RBAC]
        public JsonResult GetTransferData(long id,long oprtid)
        {
            var MIdata = ProcessInchargeservice.get().Where(x => x.INVENTORY_ID == id && x.RECEIVED_PERSON== oprtid).ToList();
            return Json(MIdata);
        }


        [RBAC]
        public JsonResult GetTransferItem(long id)
        {
            List<object> list = new List<object>();
            var Tdate = ProcessInchargeservice.getbyid(id).STOCK_TRANSFER_DATE;
            list.Add(Tdate);
            var poitem = ROservice.GetTransferitem(id);
            list.Add(poitem);
            return Json(list);
        }

        [RBAC]
        public ActionResult getOperatorPOlist(long id)
        {
            var MIdata = ProcessInchargeservice.get().Where(x => x.RECEIVED_PERSON == id).ToList();
            List<CRM_CustomerPOMasterentity> finalPOlist = new List<CRM_CustomerPOMasterentity>();
            foreach (var mid in MIdata)
            {
                var wodata = woservice.Get().Where(x => x.WorkOrderID == mid.WO_ID).ToList();
                foreach (var data in wodata)
                {
                    var podata = customerpo.Get().Where(x => x.Id == data.POID && x.IsActive == true).ToList();
                    foreach (var dt in podata)
                    {
                        CRM_CustomerPOMasterentity ent = new CRM_CustomerPOMasterentity();
                        ent.POno = dt.POno;
                        ent.Id = dt.Id;
                        finalPOlist.Add(ent);
                    }
                }
            }
            return Json(finalPOlist);
        }
    }
}
