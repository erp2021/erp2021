﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using System.Globalization;
using Garments_ERP.Filters;
namespace Garments_ERP.Controllers
{
    public class MachineController : SuperController
    {
        //
        // GET: /Machine/
        UnitService uniservice = ServiceFactory.GetInstance().UnitService;
        MachineService machineservice = ServiceFactory.GetInstance().MachineService;
        InspectionTypeService inspectionservice = ServiceFactory.GetInstance().InspectionTypeService;
        MachineTypeService machinetypeservice = ServiceFactory.GetInstance().MachineTypeService;

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Machine";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var list = machineservice.Get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid);
            return View(list);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Machine";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            ViewBag.inspectionlist = DropDownList<M_InspectiontypeEntity>.LoadItems(inspectionservice.Get(), "Id", "Inspectiontype", 0);
            ViewBag.unitlist = DropDownList<M_UnitMasterEntity>.LoadItems(uniservice.get(), "Id", "ItemUnit", 0);
            // Viewbag.vendortypelist=DropDownList<M_CustomerMasterEntity>.LoadItems(customerservice.get(), "Id", "contactname", 0);;
            ViewBag.machinetypelist = DropDownList<M_MachineTypeMasterEntity>.LoadItems(machinetypeservice.Get().Where(x => x.companyid == ViewBag.companyid && x.branchid == ViewBag.branchid).ToList(), "Machine_TypeId", "Machine_TypeName", 0);

            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(FormCollection frm)
        {
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                MachineEntity entity = new MachineEntity();
                entity.Capacityduration =Convert.ToString(frm["Capacityduration"]);
                entity.CapacityUOMid = Convert.ToString(frm["CapacityUOMid"]) == "" ? 0 : Convert.ToInt32(frm["CapacityUOMid"]);
                entity.Durationnos =Convert.ToString(frm["Durationnos"]);
                //entity.Id=frm[""];
                string inspectdate = frm["Inspectiondate"];
                DateTime Inspectiondate = DateTime.ParseExact(inspectdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.Inspectiondate = Inspectiondate;

                entity.Inspectionduration =Convert.ToString(frm["Inspectionduration"]);

                string Inspectionnextdatestr = frm["Inspectionnextdate"];
                DateTime Inspectionnextdate = DateTime.ParseExact(Inspectionnextdatestr, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.Inspectionnextdate = Inspectionnextdate;

                int inspectid = Convert.ToString(frm["Inspectiontypeid"]) == "" ? 0 : Convert.ToInt32(frm["Inspectiontypeid"]);
                entity.Inspectiontypeid = inspectid;
                entity.LwhUOMid = Convert.ToString(frm["LwhUOMid"]) == "" ? 0 : Convert.ToInt32(frm["LwhUOMid"]);
                entity.Machinedesc =Convert.ToString(frm["Machinedesc"]);
                entity.Machinename =Convert.ToString(frm["Machinename"]);
                entity.MachineSn =Convert.ToString(frm["MachineSn"]);
                entity.Machinetypeid = Convert.ToString(frm["Machinetypeid"]) == "" ? 0 : Convert.ToInt32(frm["Machinetypeid"]);
                entity.Pricecurrency =Convert.ToString(frm["Pricecurrency"]);
                entity.Productioncapacity = Convert.ToString(frm["Productioncapacity"]) == "" ? 0 : Convert.ToInt32(frm["Productioncapacity"]);

                string Purchasedatestr = frm["Purchasedate"];
                DateTime Purchasedate = DateTime.ParseExact(Purchasedatestr, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.Purchasedate = Purchasedate;

                entity.Purchaseprice = Convert.ToString(frm["Purchaseprice"]) == "" ? 0 : Convert.ToDecimal(frm["Purchaseprice"]);
                entity.Remark = frm["Remark"];
                entity.Runheight = Convert.ToString(frm["Runheight"]) == "" ? 0 : Convert.ToInt32(frm["Runheight"]);
                entity.Runlength = Convert.ToString(frm["Runlength"]) == "" ? 0 : Convert.ToInt32(frm["Runlength"]);
                entity.Runwidth = Convert.ToString(frm["Runwidth"]) == "" ? 0 : Convert.ToInt32(frm["Runwidth"]);
                entity.Vendortypeid = Convert.ToString(frm["Vendortypeid"]) == "" ? 0 : Convert.ToInt32(frm["Vendortypeid"]);
                entity.IsActive = true;

                //New Code Added by Rahul on 09-01-2020
                entity.Createdby= Convert.ToInt32(collection.Id);
                entity.Createddate = DateTime.Now;
                entity.Company_ID= Convert.ToInt32(collection.Company_ID);
                entity.Branch_ID = Convert.ToInt32(collection.BranchId);

                long id = machineservice.Insert(entity);
                if (id > 0)
                {
                    TempData["alertmsg"] = "Record Save Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                   
                   return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertmsg"] = "Error";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "Machine";
            MachineEntity entity = new MachineEntity();
            var unitlist = uniservice.get();
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                ViewBag.roleid = Convert.ToInt32(collection.RoleID);
                ViewBag.deptid = Convert.ToInt32(collection.DeptID);
                ViewBag.userid = Convert.ToInt32(collection.Id);
                ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
                ViewBag.branchid = Convert.ToInt32(collection.BranchId);
                ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
                ViewBag.Username = Convert.ToString(collection.Username);

                entity = machineservice.GetById(id);
                ViewBag.inspectionlist = DropDownList<M_InspectiontypeEntity>.LoadItems(inspectionservice.Get(), "Id", "Inspectiontype", (int)(entity.Inspectiontypeid!=null? entity.Inspectiontypeid:0));

                ViewBag.capacityunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitlist, "Id", "ItemUnit", (int)(entity.CapacityUOMid!=null? entity.CapacityUOMid:0));
                ViewBag.Lwhunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitlist, "Id", "ItemUnit", (int)(entity.LwhUOMid != null ? entity.LwhUOMid : 0));
                // Viewbag.vendortypelist=DropDownList<M_CustomerMasterEntity>.LoadItems(customerservice.get(), "Id", "contactname", 0);;
                ViewBag.machinetypelist = DropDownList<M_MachineTypeMasterEntity>.LoadItems(machinetypeservice.Get().Where(x => x.companyid == ViewBag.companyid && x.branchid == ViewBag.branchid).ToList(), "Machine_TypeId", "Machine_TypeName", (long)(entity.Machinetypeid!=null? entity.Machinetypeid:0));
                return View(entity);
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            try
            {
                MachineEntity entity = new MachineEntity();
                entity.Capacityduration =Convert.ToString(frm["Capacityduration"]);
                entity.CapacityUOMid = Convert.ToString(frm["CapacityUOMid"]) == "" ? 0 : Convert.ToInt32(frm["CapacityUOMid"]);
                entity.Durationnos =Convert.ToString(frm["Durationnos"]);
                //entity.Id=frm[""];
                // entity.Inspectiondate = frm["Inspectiondate"];
                string inspectdate =Convert.ToString(frm["Inspectiondate"]);
                DateTime Inspectiondate = DateTime.ParseExact(inspectdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.Inspectiondate = Inspectiondate;
               
                entity.Inspectionduration =Convert.ToString(frm["Inspectionduration"]);

                // entity.Inspectionnextdate = frm["Inspectionnextdate"];
                string Inspectionnextdatestr =Convert.ToString(frm["Inspectionnextdate"]);
                DateTime Inspectionnextdate = DateTime.ParseExact(Inspectionnextdatestr, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.Inspectionnextdate = Inspectionnextdate;

                entity.Id = Convert.ToInt64(frm["Id"]);
                entity.Inspectiontypeid = Convert.ToString(frm["Inspectiontypeid"]) == "" ? 0 : Convert.ToInt32(frm["Inspectiontypeid"]);
                entity.LwhUOMid = Convert.ToString(frm["LwhUOMid"]) == "" ? 0 : Convert.ToInt32(frm["LwhUOMid"]);
                entity.Machinedesc =Convert.ToString(frm["Machinedesc"]);
                entity.Machinename =Convert.ToString(frm["Machinename"]);
                entity.MachineSn =Convert.ToString(frm["MachineSn"]);
                entity.Machinetypeid = Convert.ToString(frm["Machinetypeid"]) == "" ? 0 : Convert.ToInt32(frm["Machinetypeid"]);
                entity.Pricecurrency =Convert.ToString(frm["Pricecurrency"]);
                entity.Productioncapacity = Convert.ToString(frm["Productioncapacity"])==""?0:Convert.ToInt32(frm["Productioncapacity"]);

                string Purchasedatestr =Convert.ToString(frm["Purchasedate"]);
                DateTime Purchasedate = DateTime.ParseExact(Purchasedatestr, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.Purchasedate = Purchasedate;
                //entity.Purchasedate = frm["Purchasedate"];

                entity.Purchaseprice = Convert.ToString(frm["Purchaseprice"]) == "" ? 0 : Convert.ToDecimal(frm["Purchaseprice"]);
                entity.Remark =Convert.ToString(frm["Remark"]);
                entity.Runheight = Convert.ToString(frm["Runheight"]) == "" ? 0 : Convert.ToInt32(frm["Runheight"]);
                entity.Runlength = Convert.ToString(frm["Runlength"]) == "" ? 0 : Convert.ToInt32(frm["Runlength"]);
                entity.Runwidth = Convert.ToString(frm["Runwidth"]) == "" ? 0 : Convert.ToInt32(frm["Runwidth"]);
                entity.Vendortypeid = Convert.ToString(frm["Vendortypeid"]) == "" ? 0 : Convert.ToInt32(frm["Vendortypeid"]);

                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.Updatedby= Convert.ToInt32(collection.Id);
                entity.Updateddate = DateTime.Now;
                entity.Company_ID= Convert.ToInt32(collection.Company_ID);
                entity.Branch_ID= Convert.ToInt32(collection.BranchId);

                var isupdated = machineservice.Update(entity);
                if (isupdated == true)
                {
                    TempData["alertmsg"] = "Record Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                }
                else
                {
                    TempData["alertmsg"] = "Error";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public JsonResult delete(long id)
        {
            bool deleted = false;
            deleted = machineservice.Delete(id);


            return Json(deleted);
        }
    }
}
