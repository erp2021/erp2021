﻿using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Garments_ERP.Service.Service.MasterServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class AttributeValueController : SuperController
    {
        //
        // GET: /AttributeValue/
        AttributeValueService attributevalservice = ServiceFactory.GetInstance().AttributeValueService;
        AttributeService attributeservice = ServiceFactory.GetInstance().AttributeService;
        //
        // GET: /Attribute/
        [RBAC]
        public ActionResult Index()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.title = "Attribute Value";

            var list = attributevalservice.get().Where(x=>x.Company_ID== ViewBag.companyid && x.Branch_ID== ViewBag.branchid);
            return View(list);
        }

        [RBAC]
        public ActionResult Create()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.title = "Attribute Value";

            ViewBag.attributelist = DropDownList<M_Attribute_MasterEntity>.LoadItems(attributeservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Attribute_ID", "Attribute_Name", 0);
            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(M_Attribute_Value_MasterEntity entity)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    entity.Is_Active = true;
                    entity.Created_Date = DateTime.Now;
                    entity.Created_By = Helper.Helper.CurrentLoggedUser.Id;
                    entity.Company_ID = Helper.Helper.CurrentLoggedUser.Company_ID;
                    entity.Branch_ID = Helper.Helper.CurrentLoggedUser.BranchId;
                    int id = attributevalservice.Insert(entity);
                    if (id > 0)
                    {

                        TempData["alertmsg"] = "Record Save Successfully.";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception ex)
                {
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    TempData["alertmsg"] = ex.Message;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");

        }

        [RBAC]
        public ActionResult Edit(int id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.title = "Attribute Value";

            M_Attribute_Value_MasterEntity model = new M_Attribute_Value_MasterEntity();
            model = attributevalservice.GetById(id);

            if (model.Attribute_ID != null)
            {
                ViewBag.attributelist = DropDownList<M_Attribute_MasterEntity>.LoadItems(attributeservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Attribute_ID", "Attribute_Name", (int)model.Attribute_ID);
            }
            else
            {
                ViewBag.attributelist = DropDownList<M_Attribute_MasterEntity>.LoadItems(attributeservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Attribute_ID", "Attribute_Name", 0);
            }
            if (model.Status == true)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(M_Attribute_Value_MasterEntity model)
        {
            try
            {
                model.Modified_Date = DateTime.Now;
                model.Is_Active = true;
                model.Modified_By = Helper.Helper.CurrentLoggedUser.Id;

                //New Code Added by Rahul on 09-01-2020
                model.Company_ID = Helper.Helper.CurrentLoggedUser.Company_ID;
                model.Branch_ID = Helper.Helper.CurrentLoggedUser.BranchId;

                bool updatedId = attributevalservice.Update(model);

                if (updatedId == true)
                {
                    TempData["alertmsg"] = "Record Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
        
            return RedirectToAction("Index");
            
        }

        [RBAC]
        [HttpPost]
        public JsonResult DeleteAttribute(int Id)
        {
            bool ISDeleted = false;
            ISDeleted = attributevalservice.Delete(Id);
            return Json(ISDeleted);

        }

    }
}
