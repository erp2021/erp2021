﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class SupplierController : SuperController
    {
        //
        // GET: /Supplier/
        SupplierTypeService suppliertypeservice = ServiceFactory.GetInstance().SupplierTypeService;
        SupplierContactService suppliercontactservice = ServiceFactory.GetInstance().SupplierContactService;
        SupplierBankService supplierbankservice = ServiceFactory.GetInstance().SupplierBankService;
        StateService stateservice = ServiceFactory.GetInstance().StateService;
        CityService cityservice = ServiceFactory.GetInstance().CityService;
        SupplierService supplierservice = ServiceFactory.GetInstance().SupplierService;
        DepartmentService departmentservice = ServiceFactory.GetInstance().DepartmentService;
        CountryService countryservice = ServiceFactory.GetInstance().CountryService;
        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Supplier";
            var list = supplierservice.get();
            return View(list);
        }
        public ActionResult ContactIndex(long? id)
        {
            var list = suppliercontactservice.GetById((long)id);
            return View(list);
        }
        public ActionResult BankIndex(long? id)
        {
            var list = supplierbankservice.GetById((long)id);
            return View(list);
        }
        [RBAC]
        public ActionResult Create()
        {
            ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name", 101);
            ViewBag.typelist = DropDownList<M_SupplierTypeMasterEntity>.LoadItems(suppliertypeservice.get(), "Id", "TypeName", 1);
            ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", 1646);
            ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0);
            ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 1);
            return View();
        }
        [HttpPost]
        public ActionResult Create(FormCollection frm)
        {
            try
            {
                var suppliername = frm["Suppliername"];
                var suppliercode = frm["suppliercode"];
                var suppliertypeid = Convert.ToInt32(frm["suppliertypeid"]);
                var supplierphone = frm["supplierphone"];
                var supplieraddress = frm["supplieraddress"];
                var supplierstateid = Convert.ToInt32(frm["supplierstateid"]);
                var supplierGSTNo = frm["GSTNo"];
                var supplieremail = frm["Email"];
                var remark = frm["remark"];
                var supplierCSTNo = frm["supplierCSTNo"];
                var supplierTINNo = frm["supplierTINNo"];
                var suppliercityid = Convert.ToInt32(frm["suppliercityid"]);
                var countryid = Convert.ToInt32(frm["Countryid"]);
                //supplier bank details
                var supplierbank = frm["supplierbank"];
                var supplieraccountno = frm["supplieraccountno"];
                var supplierbankcode = frm["supplierbankcode"];
                M_SupplierMasterEntity entity = new M_SupplierMasterEntity();
                entity.Suppliername = suppliername;
                entity.Suppliercode = suppliercode;
                entity.Suppliertype = (int)suppliertypeid;
                entity.Phone = supplierphone;
                entity.Address = supplieraddress;
                entity.Countryid = countryid;
                entity.Stateid = supplierstateid;
                entity.Cityid = suppliercityid;
                entity.GSTNo = supplierGSTNo;
                entity.Email = supplieremail;
                entity.Remark = remark;
                entity.CSTRegno = supplierCSTNo;
                entity.Tinno = supplierTINNo;
                entity.IsActive = true;
                entity.Createddate = DateTime.Now;
                entity.Updateddate = DateTime.Now;
                long id = supplierservice.Insert(entity);
                if (id > 0)
                {

                    var contactslist = frm["ContactList"].ToString();
                    //code to split contactlist
                    string[] contactarr = contactslist.Split('#');
                    foreach (var arritem in contactarr)
                    {
                        string[] contact = arritem.Split('~');
                        M_SupplierContactDetailsEntity contactentity = new M_SupplierContactDetailsEntity();

                        contactentity.Supplierid = id;
                        contactentity.Contactpersonname = contact[1];
                        contactentity.Mobileno = contact[2];
                        contactentity.Telephoneno = contact[3];
                        contactentity.Email = contact[4];
                        contactentity.Faxno = contact[5];
                        contactentity.Department = Convert.ToInt32(contact[6]);
                        contactentity.Remark = contact[7];
                        contactentity.IsActive = true;
                        contactentity.Createddate = DateTime.Now;
                        contactentity.Updateddate = DateTime.Now;
                        long insertedid = suppliercontactservice.Insert(contactentity);
                    }
                }
                //code to save supplier bank details
                if (id > 0)
                {
                    M_SupplierBankDetailsEntity bankentity = new M_SupplierBankDetailsEntity();
                    bankentity.Supplierid = id;
                    bankentity.Bankcode = supplierbankcode;
                    bankentity.Bankname = supplierbank;
                    bankentity.Accountno = supplieraccountno;
                    bankentity.Createddate = DateTime.Now;
                    bankentity.Updateddate = DateTime.Now;
                    bankentity.IsActive = true;
                    long insertedbankid = supplierbankservice.Insert(bankentity);
                    if (insertedbankid > 0)
                    {
                        Response.Write("<script Language='JavaScript'>alert('Supplier save successfully')</script>");
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Edit(long id)
        {
            M_SupplierMasterEntity model = new M_SupplierMasterEntity();
            model = supplierservice.GetById(id);
            ViewBag.typelist = DropDownList<M_SupplierTypeMasterEntity>.LoadItems(suppliertypeservice.get(), "Id", "TypeName", (int)model.Suppliertype);
            ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", (long)model.Stateid);
            if (model.Cityid != null)
            {
                ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", (long)model.Cityid);
            }
            else
            {
                ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0);
            }
            List<M_SupplierContactDetailsEntity> contactentity = new List<M_SupplierContactDetailsEntity>();
            contactentity = suppliercontactservice.GetById(id);
            model.M_SupplierContactDetails = contactentity;
            var deptlist=departmentservice.get();
            ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(deptlist, "Id", "Departmentname", 0);

            model.deptlist = deptlist;

            //ViewBag.editdeptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname",contactentity.de);
            M_SupplierBankDetailsEntity bankentity = new M_SupplierBankDetailsEntity();
            bankentity = supplierbankservice.GetById(id);
            model.M_SupplierBankDetails = bankentity;
            ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name", 101);

            return View(model);

        }

        [HttpPost]
        public ActionResult Edit(M_SupplierMasterEntity model, FormCollection frm)
        {
            try
            {
                var suppliertypeid = Convert.ToInt32(frm["suppliertypeid"]);
                var supplierstateid = Convert.ToInt32(frm["supplierstateid"]);
                var suppliercityid = Convert.ToInt32(frm["suppliercityid"]);
                model.Updateddate = DateTime.Now;
                model.IsActive = true;
                model.Suppliertype = (int)suppliertypeid;
                model.Stateid = (int)supplierstateid;
                model.Cityid = (int)suppliercityid;
                bool isUpdated = supplierservice.Update(model);
                var contactslist = frm["ContactList"].ToString();
                //code to split contactlist
                string[] contactarr = contactslist.Split('#');
                foreach (var arritem in contactarr)
                {
                    string[] contact = arritem.Split('~');
                    M_SupplierContactDetailsEntity contactentity = new M_SupplierContactDetailsEntity();
                    contactentity.Supplierid = model.Id;
                    contactentity.Id = Convert.ToInt64(contact[0]);
                    contactentity.Contactpersonname = contact[1];
                    contactentity.Mobileno = contact[2];
                    contactentity.Telephoneno = contact[3];
                    contactentity.Email = contact[4];
                    contactentity.Faxno = contact[5];
                    contactentity.Department = Convert.ToInt32(contact[6]);
                    contactentity.Remark = contact[7];
                    contactentity.IsActive = true;
                    contactentity.Updateddate = DateTime.Now;
                    if (contactentity.Id != 0)
                    {
                        bool updatedid = suppliercontactservice.Update(contactentity);
                    }

                    else
                    {
                        long insertedid = suppliercontactservice.Insert(contactentity);
                    }
                }
                M_SupplierBankDetailsEntity bankentity = new M_SupplierBankDetailsEntity();
                bankentity.Id = model.M_SupplierBankDetails.Id;
                bankentity.Supplierid = model.Id;
                bankentity.Bankcode = model.M_SupplierBankDetails.Bankcode;
                bankentity.Bankname = model.M_SupplierBankDetails.Bankname;
                bankentity.Accountno = model.M_SupplierBankDetails.Accountno;
                bankentity.Updateddate = DateTime.Now;
                bankentity.IsActive = true;
                if (bankentity.Id != 0)
                {
                    bool updatedbankid = supplierbankservice.Update(bankentity);
                    if (updatedbankid == true)
                    {
                        Response.Write("<script Language='JavaScript'>alert('Supplier updated successfully')</script>");
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    long insertedbankid = supplierbankservice.Insert(bankentity);
                    if (insertedbankid > 0)
                    {
                        Response.Write("<script Language='JavaScript'>alert('Supplier updated successfully')</script>");
                        return RedirectToAction("Index");
                    }
                }

            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult getCityNameList(int stateid)
        {
            List<M_CityMasterEntity> lis = cityservice.getByid(stateid);
            return Json(lis);
        }


        public JsonResult CheckSupplierName(String Suppliername, long? Id)
        {
            bool value = true;
            if (Id == null)
                value = supplierservice.get().Where(x => x.Suppliername == Suppliername).FirstOrDefault() == null;
            else
            {

                if (supplierservice.get().Where(x => x.Suppliername == Suppliername && x.Id == Id).FirstOrDefault() == null)
                {
                    if (supplierservice.get().Where(x => x.Suppliername == Suppliername).FirstOrDefault() == null)
                    {
                        value = true;
                    }
                    else
                    {
                        value = false;
                    }
                }
            }

            return Json(value, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsEmailExist(string strEmail, long? Id)
        {
            var contact = supplierservice.get().Where(x => x.Email == strEmail).FirstOrDefault();
            if (contact != null && contact.Id != Id)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }
       
        [HttpPost]
        public JsonResult DeleteSupplier(long Id)
        {
            bool ISDeleted = false;
            ISDeleted = supplierservice.Delete(Id);
            return Json(ISDeleted);

        }
    }
}
