﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using System.IO;
using Garments_ERP.Filters;
using Garments_ERP.Service.Service.MasterServices;
using Garments_ERP.Entity.MasterEntities;
using Newtonsoft.Json;
using System.Xml;
using System.Text;
using Garments_ERP.Data.Data;

namespace Garments_ERP.Controllers
{
    public class ItemController : SuperController
    {
        //
        // GET: /Item/

        ItemService itemservice = ServiceFactory.GetInstance().ItemService;
        WarehouseService warehouseservice = ServiceFactory.GetInstance().WarehouseService;
        ItemCategoryService itemcategoryservice = ServiceFactory.GetInstance().ItemCategoryService;
        ItemSubCategoryService itemsubcategoryservice = ServiceFactory.GetInstance().ItemSubCategoryService;
        UnitService unitservice = ServiceFactory.GetInstance().UnitService;
        ItemColorService itemcolorservice = ServiceFactory.GetInstance().ItemColorService;
        ItemshadeService itemshadeservice = ServiceFactory.GetInstance().ItemshadeService;
        RackService rackservice = ServiceFactory.GetInstance().RackService;
        PackingTypeService packingtypeservice = ServiceFactory.GetInstance().PackingTypeService;
        AttributeService attributeservice = ServiceFactory.GetInstance().AttributeService;
        AttributeValueService attributevalservice = ServiceFactory.GetInstance().AttributeValueService;
        ItemAttributeValueService itemattributevalservice = ServiceFactory.GetInstance().ItemAttributeValueService;
        ItemGroupService itemgroupservice = ServiceFactory.GetInstance().ItemGroupService;
        StyleService styleservice = ServiceFactory.GetInstance().StyleService;
        SRM_GRNInwardService srmgrnservice = ServiceFactory.GetInstance().SRMGRNInwardService;


        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Item";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var list = itemservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid);
            return View(list);
        }

        [RBAC]
        public JsonResult CheckItemName(String ItemName, int? Id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);



            bool value = true;
            if (Id == null)
                value = itemservice.get().Where(x => x.ItemName == ItemName && x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).FirstOrDefault() == null;
            else
            {

                if (itemservice.get().Where(x => x.ItemName == ItemName && x.Id == Id && x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).FirstOrDefault() == null)
                {
                    if (itemservice.get().Where(x => x.ItemName == ItemName && x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).FirstOrDefault() == null)
                    {
                        value = true;
                    }
                    else
                    {
                        value = false;
                    }
                }
            }

            return Json(value, JsonRequestBehavior.AllowGet);
        }


        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Item";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            ViewBag.ControlList = DropDownList<M_Dynamic_ControlsEntity>.LoadItems(srmgrnservice.GetControlList(), "Dynamic_Controls_Id", "Control_Name", 0);
            ViewBag.itemcategorylist = DropDownList<M_ItemCategoryMasterEntity>.LoadItems(itemcategoryservice.get().Where(x=>x.Company_ID== ViewBag.companyid && x.Branch_ID== ViewBag.branchid).ToList(), "Id", "Itemcategory", 0);
            //ViewBag.itemsubcategorylist = DropDownList<M_ItemSubCategoryMasterEntity>.LoadItems(itemsubcategoryservice.get(), "Id", "Itemsubcategory", 0);
            ViewBag.itemunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", 0);
            ViewBag.itemcolorlist = DropDownList<M_ItemColorEntity>.LoadItems(itemcolorservice.get(), "Id", "ItemColor", 0);
            ViewBag.itemshadelist = DropDownList<M_ItemShadeMasterEntity>.LoadItems(itemshadeservice.get(), "Id", "Itemshade", 0);
            ViewBag.itemshapelist = DropDownList<M_ItemShapeMasterEntity>.LoadItems(itemshadeservice.getShape(), "Id", "Itemshape", 0);
            ViewBag.packingtypelist = DropDownList<M_PackingTypeMasterEntity>.LoadItems(packingtypeservice.get(), "Id", "Packingtype", 0);
            ViewBag.Attribute = DropDownList<M_Attribute_MasterEntity>.LoadItems(attributeservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Attribute_ID", "Attribute_Name", 0);
            ViewBag.Itemgrouplist = DropDownList<M_ItemGroupEntity>.LoadItems(itemgroupservice.Get(), "Id", "Itemgroup", 0);
            ViewBag.stylelist = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Id", "Stylename", 0);
            return View();
        }
        
        [RBAC]
        [HttpPost]
        public ActionResult Create(M_ItemMasterEntity model, FormCollection frm, HttpPostedFileBase[] LedDocu, HttpPostedFileBase[] ItemAttrimages_id)
        {
            try
            {
                var Style = Convert.ToString(frm["styleid"]) == "" ? 0 : Convert.ToInt32(frm["styleid"]);
                var itemgroup = Convert.ToInt32(frm["Itemgroupid"]);
                var itemcat = Convert.ToInt32(frm["itemcategoryid"]);
                var itemSubcat = Convert.ToInt32(frm["itemsubcategory"]);
                var isQCRequired = Convert.ToInt16(frm["isQCRequired"]);
                var isActive = Convert.ToInt16(frm["isActive"]);
                model.Taxrate = (Convert.ToString(frm["GSTCharge"]) != "" && Convert.ToString(frm["GSTCharge"]) != "select") ? Convert.ToDecimal(frm["GSTCharge"]) : 0;
                model.Itemgroupid = itemgroup;
                model.ItemStyleid = Style;
                model.ItemCategoryid = itemcat;
                model.Itemsubcategoryid = itemSubcat;
                var Unit = Convert.ToInt32(frm["Unit"]);

                List<string> rawimglist = new List<string>();
                if (isQCRequired == 1)
                {
                    model.QCrequired = true;
                }
                else
                {
                    model.QCrequired = false;

                }
                if (isActive == 1)
                {
                    model.IsActive = true;
                }
                else
                {
                    model.IsActive = false;
                }
                model.Createddate = DateTime.Now;
                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                model.CreatedBy = Convert.ToInt32(collection.Id);
                model.Company_ID = Convert.ToInt32(collection.Company_ID);
                model.Branch_ID = Convert.ToInt32(collection.BranchId);

                //model.Updateddate = DateTime.Now;
                if (LedDocu != null)
                {
                    foreach (var rimg in LedDocu)
                    {
                        if (rimg != null && rimg.ContentLength > 0)
                        {
                            var filename = Path.GetFileName(rimg.FileName);
                            var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));
                            string isExistImagepath = Server.MapPath("~/ItemImages/");
                            bool exists = System.IO.Directory.Exists(isExistImagepath);
                            if (!exists)
                                System.IO.Directory.CreateDirectory(isExistImagepath);
                            var path = Path.Combine(isExistImagepath, newfilename);
                            rimg.SaveAs(path);
                            rawimglist.Add(newfilename);
                        }
                        else
                        {
                            rawimglist.Add("");
                        }
                    }
                }


                List<string> AttributeImage = new List<string>();
                if (ItemAttrimages_id != null)
                {
                    foreach (var rimg in ItemAttrimages_id)
                    {
                        if (rimg != null && rimg.ContentLength > 0)
                        {
                            var filename = Path.GetFileName(rimg.FileName);
                            var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));
                            string isExistImagepath = Server.MapPath("~/ItemImages/");
                            bool exists = System.IO.Directory.Exists(isExistImagepath);
                            if (!exists)
                                System.IO.Directory.CreateDirectory(isExistImagepath);
                            var path = Path.Combine(isExistImagepath, newfilename);
                            rimg.SaveAs(path);
                            AttributeImage.Add(newfilename);
                        }
                        else
                        {
                            AttributeImage.Add("");
                        }
                    }
                }
                
                var AttributeValList = Convert.ToString(frm["AttributList"]);
                model.Length = 0;
                model.Width = 0;
                model.Height = 0;
                model.ShadeId = null;
                long insertedId = itemservice.Insert(model, AttributeValList);
                if (insertedId > 0)
                {
                    var imgarray = rawimglist.ToArray();
                    //code to save raw item details      
                    if (imgarray != null)
                    {
                        foreach (var itemimg in imgarray)
                        {
                            M_ItemMasterImageEntity itemimgentity = new M_ItemMasterImageEntity();
                            itemimgentity.ItemId = insertedId;
                            itemimgentity.image = itemimg;
                            itemimgentity.Createddate = DateTime.Now;
                            //New Code Added by Rahul on 09-01-2020
                            //itemimgentity.Updateddate = DateTime.Now;
                            itemimgentity.Createdby = Convert.ToInt32(collection.Id);
                            itemimgentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                            itemimgentity.Branch_ID = Convert.ToInt32(collection.BranchId);

                            itemimgentity.IsActive = true;
                            long rawitemid = itemservice.InsertImage(itemimgentity);
                        }
                    }


                    if (AttributeValList != "")
                    {
                        string myattr = Convert.ToString(frm["Attribute_ID"]);
                        string[] myattribtab = AttributeValList.Split('#');

                        // string myjson = "";
                        for (int i = 0; i < myattribtab.Length; i++)
                        {
                            string[] myattribtabval = myattribtab[i].Split('~');
                            var lenval = myattribtabval.Length - 2;
                            string myval = "";
                            for (int j = 0; j < myattribtabval.Length - 6; j++)
                            {
                                if (j == 0)
                                {
                                    myval = myattribtabval[j];
                                }
                                else
                                {
                                    myval = myval + "~" + myattribtabval[j];
                                }
                            }

                            var blen = myattribtabval.Length;

                            string[] BatchInfo = myattribtabval[blen-1].Split('|');

                            string myjson = "{\"MyValue\":\"" + myval + "\",\"openingstock\":\"" + myattribtabval[lenval - 4] + "\",\"rate\":\"" + myattribtabval[lenval - 3] + "\",\"ROL\":\"" + myattribtabval[lenval- 2] + "\",\"ID\":\"" + insertedId + "\",\"AttributeID\":\"" + myattr + "\",\"IsActive\":\"1\",\"CompanyId\":\"" + Convert.ToInt32(collection.Company_ID) + "\",\"BranchId\":\"" + Convert.ToInt32(collection.BranchId) + "\",\"UserId\":\"" + Convert.ToInt32(collection.UserId) + "\",\"Unit\":\"" + Unit + "\",\"EOQ\":\"" + myattribtabval[lenval - 1] + "\"}";

                            //if (i == 0)
                            //{
                            //    myjson = "{\"MyValue\":\"" + myval + "\",\"openingstock\":\"" + myattribtabval[lenval - 2] + "\",\"rate\":\"" + myattribtabval[lenval - 1] + "\",\"ROL\":\"" + myattribtabval[lenval] + "\",\"ID\":\"" + insertedId + "\",\"AttributeID\":\"" + myattr + "\",\"IsActive\":\"1\",\"CompanyId\":\"" + Convert.ToInt32(collection.Company_ID) + "\",\"BranchId\":\"" + Convert.ToInt32(collection.BranchId) + "\",\"UserId\":\"" + Convert.ToInt32(collection.UserId) + "\",\"Unit\":\"" + Unit + "\"}";
                            //}
                            //else
                            //{
                            //    myjson = myjson + ",{\"MyValue\":\"" + myval + "\",\"openingstock\":\"" + myattribtabval[lenval - 2] + "\",\"rate\":\"" + myattribtabval[lenval - 1] + "\",\"ROL\":\"" + myattribtabval[lenval] + "\",\"ID\":\"" + insertedId + "\",\"AttributeID\":\"" + myattr + "\",\"IsActive\":\"1\",\"CompanyId\":\"" + Convert.ToInt32(collection.Company_ID) + "\",\"BranchId\":\"" + Convert.ToInt32(collection.BranchId) + "\",\"UserId\":\"" + Convert.ToInt32(collection.UserId) + "\",\"Unit\":\"" + Unit + "\"}";
                            //}
                            myjson = "[" + myjson + "]";
                            XmlDocument doc = new XmlDocument();
                            doc = JsonConvert.DeserializeXmlNode("{\"Row\":" + myjson + "}", "root"); // JSON needs to be an objec

                            long itematt = itemservice.InsertItemAttrib(doc);


                            using (var db = new GarmentERPDBEntities())
                            {
                                long inwarditemid = 0;
                                string itemPIval = Convert.ToString(BatchInfo[0]);
                                string itemPIId = Convert.ToString(BatchInfo[1]);
                                if (itemPIval != "" && itemPIId != "")
                                {
                                    string[] itemPI = itemPIval.Split('<');
                                    string PIids = itemPIId;

                                    foreach (var myPIdata in itemPI)
                                    {
                                        if (insertedId > 0)
                                        {
                                            string[] PIsplt = myPIdata.Split('>');
                                            string[] PIidsplt = PIids.Split(',');
                                            int len = PIsplt.Length;
                                            M_ItemInwardMaster inwardrecord = new M_ItemInwardMaster();
                                            inwardrecord.ItemInward_ID = insertedId;
                                            inwardrecord.Jobwork_ID = inwarditemid;
                                            inwardrecord.Item_ID = insertedId;
                                            inwardrecord.Item_Attribute_ID = Convert.ToInt32(itematt);
                                            inwardrecord.ItemQty = Convert.ToDecimal(PIsplt[len - 1]);
                                            inwardrecord.UOM = Convert.ToInt32(Unit);
                                            inwardrecord.BalanceQty = 0;
                                            inwardrecord.IsActive = true;
                                            inwardrecord.InventoryModeId = 5;
                                            inwardrecord.CreatedDate = DateTime.Now;
                                            inwardrecord.CreatedBy = Convert.ToInt32(collection.Id);
                                            inwardrecord.Company_ID = model.Company_ID;
                                            inwardrecord.BranchId = model.Branch_ID;


                                            db.M_ItemInwardMaster.Add(inwardrecord);
                                            db.SaveChanges();
                                            long IIid = inwardrecord.ID;
                                            int k = 0;
                                            foreach (var pidata in PIidsplt)
                                            {
                                                M_Product_Info prodinfo = new M_Product_Info();
                                                prodinfo.ItemInward_ID = IIid;
                                                prodinfo.Dynamic_Controls_Id = Convert.ToInt64(pidata);
                                                prodinfo.Dynamic_Controls_value = Convert.ToString(PIsplt[k]);
                                                prodinfo.Created_Date = DateTime.Now;
                                                prodinfo.Created_By = Convert.ToInt32(collection.Id);
                                                prodinfo.Company_ID = model.Company_ID;
                                                prodinfo.BranchId = model.Branch_ID;
                                                db.M_Product_Info.Add(prodinfo);
                                                db.SaveChanges();
                                                k++;
                                            }
                                        }
                                    }

                                }
                               
                            }

                            var Attrimgarray = AttributeImage.ToList();
                            //code to save raw item details      
                            if (Attrimgarray != null)
                            {
                                int imgcnt = Convert.ToInt32(myattribtabval[4]);
                                int k = 0;
                                foreach (var itemimg in Attrimgarray)
                                {
                                    if (k != imgcnt)
                                    {
                                        if (itemimg != "")
                                        {
                                            M_ImageMasterEntity imgentity = new M_ImageMasterEntity();
                                            imgentity.ItemId = insertedId;
                                            imgentity.Item_Attribute_ID = Convert.ToInt32(itematt);
                                            imgentity.SubCategoryId = itemSubcat;
                                            imgentity.image = itemimg;
                                            imgentity.Createddate = DateTime.Now;
                                            //New Code Added by Rahul on 09-01-2020
                                            //itemimgentity.Updateddate = DateTime.Now;
                                            imgentity.Createdby = Convert.ToInt32(collection.Id);
                                            imgentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                                            imgentity.BranchId = Convert.ToInt32(collection.BranchId);
                                            imgentity.IsCustomize = 0;
                                            imgentity.IsActive = true;
                                            long rawitemid = itemservice.InsertImageData(imgentity);
                                            AttributeImage.RemoveAt(0);
                                        }
                                        else
                                        {
                                            AttributeImage.RemoveAt(0);
                                        }
                                    }
                                    else
                                    {
                                        break;
                                    }
                                    k++;
                                }
                            }

                        }

                    }
                    TempData["alertmsg"] = "Item Save Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "Item";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            M_ItemMasterEntity model = new M_ItemMasterEntity();
            model = itemservice.GetById(id);
            ViewBag.ControlList = DropDownList<M_Dynamic_ControlsEntity>.LoadItems(srmgrnservice.GetControlList(), "Dynamic_Controls_Id", "Control_Name", 0);
            if (model.M_Item_Attribute_Master != null)
            {
                foreach (var name in model.M_Item_Attribute_Master)
                {
                    ViewBag.AttibId = name.Attribute_ID;
                }
            }
            if (model.Itemgroupid != null)
            {
                ViewBag.Itemgrouplist = DropDownList<M_ItemGroupEntity>.LoadItems(itemgroupservice.Get(), "Id", "Itemgroup", (int)model.Itemgroupid);
            }
            if (model.ItemStyleid != null)
            {
                ViewBag.stylelist = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", (int)model.ItemStyleid);
            }

            if (model.ItemCategoryid != null)
            {
                ViewBag.itemcategorylist = DropDownList<M_ItemCategoryMasterEntity>.LoadItems(itemcategoryservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Id", "Itemcategory", (int)model.ItemCategoryid);
            }
            else
            {
                ViewBag.itemcategorylist = DropDownList<M_ItemCategoryMasterEntity>.LoadItems(itemcategoryservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Id", "Itemcategory", 0);
            }
            if (model.Itemsubcategoryid != null)
            {
                ViewBag.itemsubcategorylist = DropDownList<M_ItemSubCategoryMasterEntity>.LoadItems(itemsubcategoryservice.GetById((long)model.ItemCategoryid).Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Id", "Itemsubcategory", (int)model.Itemsubcategoryid);
            }
            else
            {
                ViewBag.itemsubcategorylist = DropDownList<M_ItemSubCategoryMasterEntity>.LoadItems(itemsubcategoryservice.GetById((long)model.ItemCategoryid).Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Id", "Itemsubcategory", 0);
            }
            if (model.Unit != null)
            {
                ViewBag.itemunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", (int)model.Unit);
            }
            else
            {
                ViewBag.itemunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", 0);
            }
            if (model.Itemcolorid != null)
            {
                ViewBag.itemcolorlist = DropDownList<M_ItemColorEntity>.LoadItems(itemcolorservice.get(), "Id", "ItemColor", (int)model.Itemcolorid);
            }
            else
            {
                ViewBag.itemcolorlist = DropDownList<M_ItemColorEntity>.LoadItems(itemcolorservice.get(), "Id", "ItemColor", 0);
            }
            if (model.Shapeid != null)
            {
                ViewBag.itemshapelist = DropDownList<M_ItemShapeMasterEntity>.LoadItems(itemshadeservice.getShape(), "Id", "Itemshape", (int)model.Shapeid);
            }
            else
            {
                ViewBag.itemshapelist = DropDownList<M_ItemShapeMasterEntity>.LoadItems(itemshadeservice.getShape(), "Id", "Itemshape", 0);
            }
            if (model.ShadeId != null)
            {
                ViewBag.itemshadelist = DropDownList<M_ItemShadeMasterEntity>.LoadItems(itemshadeservice.get(), "Id", "Itemshade", (int)model.ShadeId);
            }
            else
            {
                ViewBag.itemshadelist = DropDownList<M_ItemShadeMasterEntity>.LoadItems(itemshadeservice.get(), "Id", "Itemshade", 0);
            }
            if (model.Packingtypeid != null)
            {
                ViewBag.packingtypelist = DropDownList<M_PackingTypeMasterEntity>.LoadItems(packingtypeservice.get(), "Id", "Packingtype", (int)model.Packingtypeid);
            }
            else
            {
                ViewBag.packingtypelist = DropDownList<M_PackingTypeMasterEntity>.LoadItems(packingtypeservice.get(), "Id", "Packingtype", 0);
            }
            ViewBag.Attribute = DropDownList<M_Attribute_MasterEntity>.LoadItems(attributeservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList(), "Attribute_ID", "Attribute_Name", 0);
            ViewBag.Itemgrouplist = DropDownList<M_ItemGroupEntity>.LoadItems(itemgroupservice.Get(), "Id", "Itemgroup", 0);
            return View(model);
        }


        [RBAC]
        [HttpPost]
        public ActionResult Edit(M_ItemMasterEntity model, FormCollection frm, HttpPostedFileBase[] LedDocu, HttpPostedFileBase[] ItemAttrimages_id)
        {
            try
            {
                var Style = Convert.ToInt32(frm["styleid"]);
                var itemgroup = Convert.ToInt32(frm["Itemgroupid"]);
                var isQCRequired = Convert.ToInt16(frm["isQCRequired"]);
                var itemcat = Convert.ToInt32(frm["itemcategoryid"]);
                var itemSubcat = Convert.ToInt32(frm["itemsubcategory"]);
                var isActive = Convert.ToInt16(frm["isActive"]);
                model.Taxrate = (Convert.ToString(frm["GSTCharge"]) != "" && Convert.ToString(frm["GSTCharge"]) != "select") ? Convert.ToDecimal(frm["GSTCharge"]) : 0;
                model.ItemCategoryid = itemcat;
                model.Itemsubcategoryid = itemSubcat;
                model.Itemgroupid = itemgroup;
                model.ItemStyleid = Style;
                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                model.UpdatedBy = Convert.ToInt32(collection.Id);
                model.Company_ID = Convert.ToInt32(collection.Company_ID);
                model.Branch_ID = Convert.ToInt32(collection.BranchId);
                model.Updateddate = DateTime.Now;

                List<string> rawimglist = new List<string>();
                if (isQCRequired == 1)
                {
                    model.QCrequired = true;
                }
                else
                {
                    model.QCrequired = false;
                }
                if (isActive == 1)
                {
                    model.IsActive = true;
                }
                else
                {
                    model.IsActive = false;
                }
                //model.Updateddate = DateTime.Now;
                var Unit = Convert.ToInt32(frm["Unit"]);
                var AttributeValList = Convert.ToString(frm["AttributList"]);
                bool updatedId = itemservice.Update(model, AttributeValList);
                long itemid = model.Id;

                if (LedDocu != null)
                {
                    foreach (var rimg in LedDocu)
                    {
                        if (rimg != null && rimg.ContentLength > 0)
                        {
                            var filename = Path.GetFileName(rimg.FileName);
                            var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));
                            string isExistImagepath = Server.MapPath("~/ItemImages/");
                            bool exists = System.IO.Directory.Exists(isExistImagepath);
                            if (!exists)
                                System.IO.Directory.CreateDirectory(isExistImagepath);
                            var path = Path.Combine(isExistImagepath, newfilename);
                            rimg.SaveAs(path);
                            rawimglist.Add(newfilename);
                        }
                        else
                        {
                            rawimglist.Add("");
                        }
                    }
                }


                var imgarray = rawimglist.ToArray();
                //code to save raw item details      
                if (imgarray != null)
                {
                    foreach (var itemimg in imgarray)
                    {
                        M_ItemMasterImageEntity itemimgentity = new M_ItemMasterImageEntity();
                        itemimgentity.ItemId = itemid;
                        itemimgentity.image = itemimg;
                        itemimgentity.Createddate = DateTime.Now;
                        //New Code Added by Rahul on 09-01-2020
                        //itemimgentity.Updateddate = DateTime.Now;
                        itemimgentity.Createdby = Convert.ToInt32(collection.Id);
                        itemimgentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        itemimgentity.Branch_ID = Convert.ToInt32(collection.BranchId);

                        itemimgentity.IsActive = true;
                        long rawitemid = itemservice.InsertImage(itemimgentity);
                    }
                }


                List<string> AttributeImage = new List<string>();
                if (ItemAttrimages_id != null)
                {
                    foreach (var rimg in ItemAttrimages_id)
                    {
                        if (rimg != null && rimg.ContentLength > 0)
                        {
                            var filename = Path.GetFileName(rimg.FileName);
                            var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));
                            string isExistImagepath = Server.MapPath("~/ItemImages/");
                            bool exists = System.IO.Directory.Exists(isExistImagepath);
                            if (!exists)
                                System.IO.Directory.CreateDirectory(isExistImagepath);
                            var path = Path.Combine(isExistImagepath, newfilename);
                            rimg.SaveAs(path);
                            AttributeImage.Add(newfilename);
                        }
                        else
                        {
                            AttributeImage.Add("");
                        }
                    }
                }

                if (AttributeValList != "")
                {
                    string myattr = Convert.ToString(frm["Attribute_ID"]);
                    string[] myattribtab = AttributeValList.Split('#');

                    string myjson = "";
                    for (int i = 0; i < myattribtab.Length; i++)
                    {
                        string[] myattribtabval = myattribtab[i].Split('~');
                        var lenval = myattribtabval.Length - 2;
                        string myval = "";
                        string clonemyval = "";

                        for (int j = 0; j < myattribtabval.Length - 7; j++)
                        {
                            if (j == 0)
                            {
                                myval = myattribtabval[j];
                            }
                            else
                            {
                                myval = myval + "~" + myattribtabval[j];
                            }
                        }

                        var blen = myattribtabval.Length;

                        string[] BatchInfo = myattribtabval[blen - 1].Split('|');

                        myjson = "{\"MyValue\":\"" + myval + "\",\"openingstock\":\"" + myattribtabval[lenval - 5] + "\",\"rate\":\"" + myattribtabval[lenval - 4] + "\",\"ROL\":\"" + myattribtabval[lenval-3] + "\",\"ID\":\"" + itemid + "\",\"AttributeID\":\"" + myattr + "\",\"IsActive\":\"" + myattribtabval[lenval] + "\",\"CompanyId\":\"" + Convert.ToInt32(collection.Company_ID) + "\",\"BranchId\":\"" + Convert.ToInt32(collection.BranchId) + "\",\"UserId\":\"" + Convert.ToInt32(collection.UserId) + "\",\"Unit\":\"" + Unit + "\",\"EOQ\":\"" + myattribtabval[lenval - 2] + "\"}";
                        myjson = "[" + myjson + "]";
                        XmlDocument doc = new XmlDocument();
                        doc = JsonConvert.DeserializeXmlNode("{\"Row\":" + myjson + "}", "root"); // JSON needs to be an objec

                        long itematt = itemservice.InsertItemAttrib(doc);
                        var Attrimgarray = AttributeImage.ToList();
                        using (var db = new GarmentERPDBEntities())
                            {
                                if (Convert.ToInt32(myattribtabval[lenval]) > 0)
                                {
                                    long inwarditemid = 0;
                                    if (BatchInfo.Length > 1)
                                    {
                                        string itemPIval = Convert.ToString(BatchInfo[0]);
                                        string itemPIId = Convert.ToString(BatchInfo[1]);
                                        if (itemPIval != "" && itemPIId != "")
                                        {
                                            string[] itemPI = itemPIval.Split('<');
                                            string PIids = itemPIId;

                                            foreach (var myPIdata in itemPI)
                                            {
                                                var Inid = db.M_ItemInwardMaster.Where(x => x.ItemInward_ID == model.Id).FirstOrDefault();

                                                if (model.Id > 0 && Inid==null)
                                                {
                                                    string[] PIsplt = myPIdata.Split('>');
                                                    string[] PIidsplt = PIids.Split(',');
                                                    int len = PIsplt.Length;
                                                    M_ItemInwardMaster inwardrecord = new M_ItemInwardMaster();
                                                    inwardrecord.ItemInward_ID = model.Id;
                                                    inwardrecord.Jobwork_ID = inwarditemid;
                                                    inwardrecord.Item_ID = model.Id;
                                                    inwardrecord.Item_Attribute_ID = Convert.ToInt32(itematt);
                                                    inwardrecord.ItemQty = Convert.ToDecimal(PIsplt[len - 1]);
                                                    inwardrecord.UOM = Convert.ToInt32(Unit);
                                                    inwardrecord.BalanceQty = 0;
                                                    inwardrecord.IsActive = true;
                                                    inwardrecord.InventoryModeId = 5;
                                                    inwardrecord.CreatedDate = DateTime.Now;
                                                    inwardrecord.CreatedBy = Convert.ToInt32(collection.Id);
                                                    inwardrecord.Company_ID = model.Company_ID;
                                                    inwardrecord.BranchId = model.Branch_ID;
                                                    db.M_ItemInwardMaster.Add(inwardrecord);
                                                    db.SaveChanges();
                                                    long IIid = inwardrecord.ID;
                                                    int k = 0;
                                                    foreach (var pidata in PIidsplt)
                                                    {
                                                        M_Product_Info prodinfo = new M_Product_Info();
                                                        prodinfo.ItemInward_ID = IIid;
                                                        prodinfo.Dynamic_Controls_Id = Convert.ToInt64(pidata);
                                                        prodinfo.Dynamic_Controls_value = Convert.ToString(PIsplt[k]);
                                                        prodinfo.Created_Date = DateTime.Now;
                                                        prodinfo.Created_By = Convert.ToInt32(collection.Id);
                                                        prodinfo.Company_ID = model.Company_ID;
                                                        prodinfo.BranchId = model.Branch_ID;
                                                        db.M_Product_Info.Add(prodinfo);
                                                        db.SaveChanges();
                                                        k++;
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                                if (Attrimgarray != null && Convert.ToInt32(myattribtabval[lenval]) > 0)
                                {
                                    var imgid = db.M_ImageMaster.Where(x => x.ItemId == model.Id && x.Item_Attribute_ID == itematt).Select(x => x.Id).FirstOrDefault();

                                    var record = db.M_ImageMaster.Find(imgid);
                                    record.IsActive = false;
                                    record.Updatedby = Convert.ToInt32(collection.Id);
                                    record.Updateddate = DateTime.Now;
                                    db.SaveChanges();
                                }
                            }
                        
                        //code to save raw item details      
                        if (Attrimgarray != null && Convert.ToInt32(myattribtabval[lenval]) >0)
                        {
                           // int imgcnt = Convert.ToInt32(myattribtabval[4]);
                            int k = 0;
                            foreach (var itemimg in Attrimgarray)
                            {
                                if (itemimg != "")
                                {
                                    M_ImageMasterEntity imgentity = new M_ImageMasterEntity();
                                    imgentity.ItemId = model.Id;
                                    imgentity.Item_Attribute_ID = Convert.ToInt32(itematt);
                                    imgentity.SubCategoryId = itemSubcat;
                                    imgentity.image = itemimg;
                                    imgentity.Createddate = DateTime.Now;
                                    //New Code Added by Rahul on 09-01-2020
                                    //itemimgentity.Updateddate = DateTime.Now;
                                    imgentity.Createdby = Convert.ToInt32(collection.Id);
                                    imgentity.Company_ID = Convert.ToInt32(collection.Company_ID);
                                    imgentity.BranchId = Convert.ToInt32(collection.BranchId);
                                    imgentity.IsCustomize = 0;
                                    imgentity.IsActive = true;
                                    long rawitemid = itemservice.InsertImageData(imgentity);
                                    AttributeImage.RemoveAt(0);
                                }
                                else
                                {
                                    AttributeImage.RemoveAt(0);
                                }
                                k++;
                            }
                        }


                        //if (i == 0)
                        //{
                        //    if (myjson != "")
                        //    {
                        //        myjson = myjson + ",{\"MyValue\":\"" + myval + "\",\"openingstock\":\"" + myattribtabval[lenval - 2] + "\",\"rate\":\"" + myattribtabval[lenval - 1] + "\",\"ROL\":\"" + myattribtabval[lenval] + "\",\"ID\":\"" + itemid + "\",\"AttributeID\":\"" + myattr + "\",\"IsActive\":\"1\",\"CompanyId\":\"" + Convert.ToInt32(collection.Company_ID) + "\",\"BranchId\":\"" + Convert.ToInt32(collection.BranchId) + "\",\"UserId\":\"" + Convert.ToInt32(collection.UserId) + "\",\"Unit\":\"" + Unit + "\"}";
                        //    }
                        //    else
                        //    {
                        //        
                        //    }
                        //}
                        //else
                        //{
                        //    myjson = myjson + ",{\"MyValue\":\"" + myval + "\",\"openingstock\":\"" + myattribtabval[lenval - 2] + "\",\"rate\":\"" + myattribtabval[lenval - 1] + "\",\"ROL\":\"" + myattribtabval[lenval] + "\",\"ID\":\"" + itemid + "\",\"AttributeID\":\"" + myattr + "\",\"IsActive\":\"1\",\"CompanyId\":\"" + Convert.ToInt32(collection.Company_ID) + "\",\"BranchId\":\"" + Convert.ToInt32(collection.BranchId) + "\",\"UserId\":\"" + Convert.ToInt32(collection.UserId) + "\",\"Unit\":\"" + Unit + "\"}";
                        //}

                    }

                    
                }

                if (updatedId == true)
                {
                    TempData["alertmsg"] = "Item Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
            
        }

        [RBAC]
        [HttpPost]
        public JsonResult DeleteItem(long Id)
        {
            bool ISDeleted = false;
            ISDeleted = itemservice.Delete(Id);
            return Json(ISDeleted);

        }

        [RBAC]
        public JsonResult CreateItem(long itemcateid, int itemsubcateid, string itemcode, string compname, string itemname, string brand, string desc, string shortname, string hsncode, string hsnchapter, int unitid, int openingqty, int colorid, int shadeid, int? itemshapeid, string eoq, string qcrequire, string isactive, int packtype)
        {
            bool isinserted = false;

            M_ItemMasterEntity entity = new M_ItemMasterEntity();
            entity.ItemCategoryid = itemcateid;
            entity.Itemsubcategoryid = itemsubcateid;
            entity.ItemCode = itemcode;
            entity.Companyname = compname;
            entity.ItemName = itemname;
            entity.Brand = brand;
            entity.Description = desc;
            entity.Shortname = shortname;
            entity.HScode = hsncode;
            entity.HSchapter = hsnchapter;
            entity.Unit = unitid;
            entity.OpeningStock = openingqty;
            entity.EOQ = eoq;
            entity.Packingtypeid = packtype;
            entity.Shapeid = itemshapeid;
            entity.ShadeId = shadeid;
            entity.Itemcolorid = colorid;
            if (isactive == "1")
                entity.IsActive = true;
            else
                entity.IsActive = false;
            entity.Createddate = DateTime.Now;
            string attrib = "";
            long id = itemservice.Insert(entity, attrib);
            if (id > 0)
            {
                isinserted = true;
                //return Json(false);
            }
            //ViewBag.itemlist = DropDownList<M_ItemMasterEntity>.LoadItems(itemservice.get(), "Id", "ItemName", 0);
            return Json(isinserted);
        }

        [RBAC]
        [HttpPost]
        public JsonResult getAttributeValue(string MyJson,int type)
        {
            var entity = attributevalservice.getAttributeValue(MyJson, type);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(entity);
            return Json(JSONString);
        }
    }
}
