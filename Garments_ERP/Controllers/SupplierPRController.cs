﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Garments_ERP.Controllers
{
    public class SupplierPRController : SuperController
    {
        UnitService unitservice = ServiceFactory.GetInstance().UnitService;
        SupplierPRServices prservice = ServiceFactory.GetInstance().SupplierPRServices;
        DepartmentService departmentservice = ServiceFactory.GetInstance().DepartmentService;
        EmployeeService employeeservice = ServiceFactory.GetInstance().EmployeeService;
        //
        // GET: /SupplierPR/
        [RBAC]
        public ActionResult Index()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            return View();
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "PR";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            SRM_PurchaseRequestEntity entity = new SRM_PurchaseRequestEntity();
            string[] PR_No = prservice.getnextprno().Split('-');
            entity.PR_No = PR_No[0] + "-" + PR_No[1] + "-" + PR_No[2] + "-" + PR_No[3] + "-XXXX";
            ViewBag.itemunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", 0);
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(SRM_PurchaseRequestEntity entity, FormCollection frm, HttpPostedFileBase[] rawitemimg)
        {

            try
            {
                List<string> rawimglist = new List<string>();
                if (rawitemimg != null)
                {
                    foreach (var rimg in rawitemimg)
                    {

                        if (rimg != null && rimg.ContentLength > 0)
                        {
                            var filename = Path.GetFileName(rimg.FileName);
                            var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                            string isExistCompanypath = Server.MapPath("~/uploadedrawitem/");
                            bool exists = System.IO.Directory.Exists(isExistCompanypath);

                            if (!exists)
                                System.IO.Directory.CreateDirectory(isExistCompanypath);

                            var path = Path.Combine(isExistCompanypath, newfilename);
                            rimg.SaveAs(path);
                            rawimglist.Add(newfilename);
                        }
                        else
                        {
                            rawimglist.Add("");
                        }
                    }
                }

                string PR_Date = frm["PR_Date"];
                DateTime prdate_ = DateTime.ParseExact(PR_Date, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.PR_Date = prdate_;
                var priority = frm["priority"].ToString();
                entity.Priority = priority;
                entity.IsActive = true;
                entity.CreatedDate = DateTime.Now;
                string approvdate = frm["approvaldate"].ToString();
                DateTime appdate = DateTime.ParseExact(approvdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.ApprovalDate = appdate;
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.CreatedBy = Convert.ToInt32(collection.Id);
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.BranchId = Convert.ToInt32(collection.BranchId);
                int roleid = Convert.ToInt32(collection.RoleID);
                if (roleid != 4)
                {
                    entity.ApprovalStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? true : false;
                    entity.IsStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? 3 : 5;
                    entity.ApprovalDate = DateTime.Now;
                }
                else
                {
                    entity.ApprovalStatus = false;
                    entity.IsStatus = 2;
                }
                var contactslist = Convert.ToString(frm["PRItemList"]);
                long id = 0;
                if (contactslist != "")
                {
                    string[] contactarr = contactslist.Split('#');
                    List<SRM_PRItemDetailEntity> PRitem = new List<SRM_PRItemDetailEntity>();
                    foreach (var arritem in contactarr)
                    {
                        if (arritem != "")
                        {
                            string[] contact = arritem.Split('~');
                            SRM_PRItemDetailEntity pritementity = new SRM_PRItemDetailEntity();
                            pritementity.PR_ID = id;
                            pritementity.ItemSubCategoryId = Convert.ToInt32(contact[0]);
                            pritementity.ItemId = Convert.ToInt32(contact[1]);
                            pritementity.Item_Attribute_ID = Convert.ToInt32(contact[2]);
                            pritementity.HSNCode = Convert.ToString(contact[3]);
                            pritementity.UnitId = Convert.ToInt32(contact[4]);
                            pritementity.RequiredQty = Convert.ToDecimal(contact[5]);
                            pritementity.Isreadymade = 1;
                            pritementity.IsActive = true;
                            pritementity.CreatedDate = DateTime.Now;
                            pritementity.CreatedBy = Convert.ToInt32(collection.Id);
                            pritementity.Company_ID = Convert.ToInt32(collection.Company_ID);
                            pritementity.BranchId = Convert.ToInt32(collection.BranchId);
                            int RICount = Convert.ToInt32(contact[6]);
                            int k = 0;
                            var Rawimgarray = rawimglist.ToList();
                            List<string> rawimgdata = new List<string>();
                            if (Rawimgarray.Count > 0 && RICount > 0)
                            {
                                foreach (var img in Rawimgarray)
                                {
                                    if (k != RICount)
                                    {
                                        if (img != "")
                                        {
                                            rawimgdata.Add(img.ToString());
                                            rawimglist.RemoveAt(0);
                                        }
                                        else
                                        {
                                            rawimglist.RemoveAt(0);
                                        }
                                    }
                                    else
                                    {
                                        break;
                                    }
                                    k++;
                                }
                            }


                            pritementity.rawitemimg_list = rawimgdata;
                            PRitem.Add(pritementity);
                        }
                    }
                    entity.ItemList = PRitem;
                    id = prservice.Insert(entity);
                }
                if (id > 0)
                {
                    TempData["alertmsg"] = "Supplier purchase request added successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertmsg"] = "Item Data Not Save. Please Check Code";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }

        }



        [RBAC]
        public ActionResult Edit(long id)
        {
            SRM_PurchaseRequestEntity entity = new SRM_PurchaseRequestEntity();
            try
            {
                ViewBag.title = "PR";
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                ViewBag.roleid = Convert.ToInt32(collection.RoleID);
                ViewBag.deptid = Convert.ToInt32(collection.DeptID);
                ViewBag.userid = Convert.ToInt32(collection.Id);
                ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
                ViewBag.branchid = Convert.ToInt32(collection.BranchId);
                ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
                ViewBag.Username = Convert.ToString(collection.Username);

                entity = prservice.getbyid(id);
                entity.ItemList = prservice.getbyPRItemid(id);
                //var rawitemlist = itemservice.get().Where(x => x.Itemsubcategoryid == entity).ToList();
                if (entity.DepartmentId != null)
                {
                    ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", (int)entity.DepartmentId);
                }
                else { ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0); }

                if (entity.AccountHeadId != null)
                {
                    ViewBag.employeelist = DropDownList<EmployeeEntity>.LoadItems(employeeservice.get(ViewBag.companyid, ViewBag.branchid), "Id", "Name", (int)entity.AccountHeadId);
                }
                else { ViewBag.employeelist = DropDownList<EmployeeEntity>.LoadItems(employeeservice.get(ViewBag.companyid, ViewBag.branchid), "Id", "Name", 0); }
                
                return View(entity);
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
        }










        [RBAC]
        public JsonResult InventoryPR(string Jobject, int type,string Attribute)
        {

            var entity = prservice.InventoryPR(type,Jobject, Attribute);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(entity);
            return Json(JSONString);
        }

        //Get Account Head With Deprtment Wise
        [RBAC]
        public JsonResult GetAccountHead(int deptid)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            var list = prservice.GetAccountHead(deptid, Company_ID, BranchId);
            return Json(list);
        }

        [RBAC]
        [HttpPost]
        public JsonResult StatusCount(int Type, string attribute)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            int roleid = Convert.ToInt32(collection.RoleID);
            int deptid = Convert.ToInt32(collection.DeptID);
            int userid = Convert.ToInt32(collection.Id);
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            string myjson = "";
            myjson = "[{\"companyid\":\"" + companyid + "\",\"branchid\":\"" + branchid + "\",\"userid\":\"" + userid + "\",\"roleid\":\"" + roleid + "\"}]";
            XmlDocument doc = new XmlDocument();
            doc = JsonConvert.DeserializeXmlNode("{\"Row\":" + myjson + "}", "root");

            var entity = prservice.StatusCount(Type, attribute, doc);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(entity);
            return Json(JSONString);
        }

        [RBAC]
        [HttpPost]
        public JsonResult DeletePR(long Id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Userid = Convert.ToInt32(collection.Id);
            bool ISDeleted = false;
            ISDeleted = prservice.Delete(Id, Userid);
            return Json(ISDeleted);
        }

        //Rejected PR Code
        [RBAC]
        public JsonResult RejectedPR(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Uid = Convert.ToInt32(collection.Id);

            var isdelete = prservice.RejectedPR(id, Uid);
            return Json(isdelete);
        }


        //Get Status Wise List
        [RBAC]
        public PartialViewResult GetStatusData(int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            var list = prservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            return PartialView("_IndexList", list);
        }

        //Form Date And To Date Wise Filter with Status List
        [RBAC]
        public PartialViewResult GetFilterStatusData(string fromdate, string todate, int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            if (fromdate != "" && todate != "")
            {

                var fdate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var tdate = DateTime.ParseExact(todate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var list = prservice.get(fdate, tdate, IsStatus, Company_ID, BranchId, Userid, roleid);
                ViewBag.fromdate = fromdate;
                ViewBag.todate = todate;
                return PartialView("_IndexList", list);
            }
            else
            {
                var list = prservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
                return PartialView("_IndexList", list);
            }

        }
    }
}
