﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using Garments_ERP.Filters;
namespace Garments_ERP.Controllers
{
    public class ItemColorController : SuperController
    {
        //
        // GET: /ItemColor/
        ItemColorService itemcolorservice = ServiceFactory.GetInstance().ItemColorService;
        [RBAC]
        public ActionResult Index()
        {
            var list = itemcolorservice.get();
            return View(list);
        }
        [RBAC]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(M_ItemColorEntity entity)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    entity.IsActive = true;
                    long id = itemcolorservice.Insert(entity);
                    if (id > 0)
                    {

                        TempData["alertmsg"] = "Record Save Successfully.";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception ex)
                {
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    TempData["alertmsg"] = ex.Message;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");

        }

        public JsonResult CheckItemColor(string Itemcolor, int? Id)
        {
            bool value = itemcolorservice.CheckItemColorExistance(Itemcolor, Id);
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult update(int id, string itemgroup, string itemgroupdesc)
        //{
        //    M_ItemColorEntity entity_ = new M_ItemColorEntity();
        //    entity_.Id = id;
        //    entity_.ItemColor = itemgroup;
        //    entity_.ItemColordesc = itemgroupdesc;
        //    bool updated = itemcolorservice.Update(entity_);
        //    if (updated == true)
        //    {
        //        M_ItemShadeMasterEntity entity = new M_ItemShadeMasterEntity();
        //        var list = itemcolorservice.get();
        //        return View(list);
        //    }
        //    return View();
        //}
        [RBAC]
        public ActionResult Edit(int id)
        {

            M_ItemColorEntity entity = itemcolorservice.get().Where(x => x.Id == id).FirstOrDefault();
            return View(entity);
        }

        [HttpPost]
        public ActionResult Edit(M_ItemColorEntity entity)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    bool isupdate = itemcolorservice.Update(entity);
                    if (isupdate == true)
                    {
                        M_ItemColorEntity entity_ = new M_ItemColorEntity();
                        var list = itemcolorservice.get();
                        TempData["alertmsg"] = "Record Updated Successfully.";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception ex)
                {
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    TempData["alertmsg"] = ex.Message;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");
        }
        [RBAC]
        public ActionResult Delete(int id)
        {
            bool delete = itemcolorservice.Delete(id);
            if (delete == true)
            {
                return Json(delete);
            }
            return Json(delete);
        }
    }
}
