﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using System.Web.Security;
namespace Garments_ERP.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/
        UserService userservice = ServiceFactory.GetInstance().UserService;
        AssignScreenService AssignScrSer = ServiceFactory.GetInstance().AssignScreenService;
        public ActionResult Index()
        {
            ViewBag.title = "Login";
            return View();
        }

        [HttpPost]
        public ActionResult Index(M_UserEntity user)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (user.Password == "" || user.Password == null)
                    {
                        TempData["LogMessage"] = "Please Check Password ";
                        TempData["alertclass"] = "alert alert-danger alert-dismissible";
                        var c = new HttpCookie("Login");
                        c.Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies.Add(c);
                        return RedirectToAction("Index");
                    }
                    if (user.Username == "" || user.Username == null)
                    {
                        TempData["LogMessage"] = "Please Check UserName ";
                        TempData["alertclass"] = "alert alert-danger alert-dismissible";

                        var c = new HttpCookie("Login");
                        c.Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies.Add(c);

                        return RedirectToAction("Index");
                    }
                    user.Password = Helper.Helper.Encrypt(user.Password);
                  
                  //  var records = Service.Authenticate(user);
                    var records = userservice.Authenticateuser(user);
                    if (records.Id != 0)
                    {
                        records.permissionlist = userservice.getbyUserid(records.Id);
                        long roleid = Convert.ToInt64(records.RoleID);
                        var list = AssignScrSer.GetScreenData(roleid).Where(x => x.CHKsatus == true).ToList();
                        Session.Add("LoggedUser", records);
                        Session.Add("RoleScreen", list);
                        if (user.RememberMe)
                        {
                            HttpCookie cookie = new HttpCookie("Login");
                            cookie.Values.Add("UserName", user.Username);
                            cookie.Values.Add("Password", Helper.Helper.Decrypt(user.Password));
                            cookie.Expires = DateTime.Now.AddDays(15);
                            Response.Cookies.Add(cookie);

                        }
                        return RedirectToAction("Index", "Dashboard");

                    }
                    else
                    {
                        TempData["LogMessage"] = "Login Failed..Please Check UserName and Password ";
                        TempData["alertclass"] = "alert alert-danger alert-dismissible";
                        return RedirectToAction("Index");
                    }

                }
                else
                {
                    TempData["LogMessage"] = "Please Check UserName and Password ";
                    TempData["alertclass"] = "alert alert-danger alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["LogMessage"] = ex.Message;
                TempData["alertclass"] = "alert alert-danger alert-dismissible";
                ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index", "Login");
            }

        }
        public ActionResult SignOut()
        {
            Session.Abandon();
            Session.RemoveAll();
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Login");
        }
        public ActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
       
        public ActionResult ResetPassword(M_UserEntity LoginEntity)
        {
            try
            {
                if (LoginEntity != null)
                {
                    var isvalid = userservice.IsEmailValid(LoginEntity.Email);
                    if (isvalid.Email != null)
                    {
                        //To Genrate Random Hash Token

                        //Create Link For Reset Password
                        var callbackUrl = Url.Action(
                   "SetPassword", "Login",
                   new { userid = Helper.Helper.EncryptURL(isvalid.UserId.ToString()), Token = Helper.Helper.EncryptURL(isvalid.HashToken) },
                   protocol: Request.Url.Scheme);

                        Email mailservice = new Email();
                        var IsEmailSent = mailservice.SendMail(
                           "Please confirm your account by clicking this link: <a href=\""
                                                         + callbackUrl + "\">link</a>", LoginEntity.Email);
                        if (IsEmailSent != true)
                        {
                            TempData["resetMessage"] = "Email Not Sent. Please Try Again";
                            return RedirectToAction("ResetPassword");
                        }
                        TempData["resetMessage"] = "Email Sent..Please Check Your Email..!";
                        return RedirectToAction("ResetPassword");
                    }
                    else
                    {
                        TempData["resetMessage"] = "Email Not Matched!";
                        return RedirectToAction("ResetPassword");
                    }
                }
            }
            catch (Exception ex)
            {

                ExceptionLogging.SendErrorToText(ex);
            }


            return RedirectToAction("ResetPassword");
        }

        public ActionResult SetPassword(string userid, string Token)
        {

            M_UserEntity model = new M_UserEntity();

            model.UserId = Helper.Helper.DecryptURL(userid.ToString());
            model.HashToken = Helper.Helper.DecryptURL(Token);
            var IsValid = userservice.ValidateResetPasswordLink(model);
            if (IsValid != true)
            {
                TempData["LogMessage"] = "Invalid Link";
                TempData["alertclass"] = "warning";
                return RedirectToAction("Index", "Login");
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(M_UserEntity model)
        {
            model.Password = Helper.Helper.Encrypt(model.Password);
            model.UserId = Helper.Helper.DecryptURL(model.UserId.ToString());
            int i = userservice.ChangePassword(model);
            if (i != 0)
            {
                return RedirectToAction("Index");
            }
            return RedirectToAction("ResetPassword");
        }


    }
}
