﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Garments_ERP.Service.Service.MasterServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class CompanyBranchController : SuperController
    {
        //
        // GET: /CompanyBranch/
        CompanyBranchService companyBranch = ServiceFactory.GetInstance().CompanyBranchService;
        CompanyService companyservice = ServiceFactory.GetInstance().CompanyService;
        StateService stateservice = ServiceFactory.GetInstance().StateService;
        CityService cityservice = ServiceFactory.GetInstance().CityService;   
        CountryService countryservice = ServiceFactory.GetInstance().CountryService;

        public ActionResult Index()
        {
            var list = companyBranch.getComdetail();
            return View(list);
        }
        [RBAC]
        public ActionResult Create()
        {
            ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name", 101);
            ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", 1646);
            ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0);
            ViewBag.complist = DropDownList<M_companyEntity>.LoadItems(companyservice.get(), "Company_ID", "Company_Name", 0);
            return View();
        }
        [HttpPost]
        public ActionResult Create(FormCollection frm)
        {
            try
            {
               M_BranchEntity entity = new M_BranchEntity();
                entity.Branch_Name = frm["comName"];
                entity.Company_ID = Convert.ToInt32(frm["companyid"]);
                entity.mob_No = frm["mobile"];
                entity.Tel_No = frm["phonenum"];
                entity.GSTIN_No = frm["GstNo"];
                entity.Branch_Address = frm["address"];
                entity.Country_Code = frm["countryid"];
                entity.State_ID = Convert.ToInt32(frm["addressstateid"]);
                entity.City_ID = Convert.ToInt32(frm["addresscityid"]);
                entity.Pincode = frm["PinCode"];
                entity.Email = frm["Email"];
                entity.Created_Date = DateTime.Now;


                entity.Is_Active = true;

                long id = companyBranch.Insert(entity);
                if (id > 0)
                {
                    TempData["alertmsg"] = "Record Save Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";

                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertmsg"] = "Error";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                }
            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
        [RBAC]
        public ActionResult Edit(long id)
        {
            M_BranchEntity entity = new M_BranchEntity();

            try
            {

                entity = companyBranch.GetById(id);
                ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name", (string)entity.Country_Code);
                ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", (int)entity.State_ID);
                ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", (int)entity.City_ID);
                ViewBag.complist = DropDownList<M_companyEntity>.LoadItems(companyservice.get(), "Company_ID", "Company_Name",(int)entity.Company_ID);

                return View(entity);
            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");
            }
            
        }
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            try
            {
                M_BranchEntity entity = new M_BranchEntity();
                entity.Branch_ID = Convert.ToInt32(frm["Branch_ID"]);
                entity.Branch_Name = frm["comName"];
                entity.Company_ID = Convert.ToInt32(frm["companyid"]);
                entity.mob_No = frm["mobile"];
                entity.Tel_No = frm["phonenum"];
                entity.GSTIN_No = frm["GstNo"];
                entity.Branch_Address = frm["address"];
                entity.Country_Code = frm["countryid"];
                entity.State_ID = Convert.ToInt32(frm["addressstateid"]);
                entity.City_ID = Convert.ToInt32(frm["addresscityid"]);
                entity.Pincode = frm["PinCode"];
                entity.Email = frm["Email"];
                entity.Modified_Date = DateTime.Now;
                var isupdated = companyBranch.Update(entity);

                if (isupdated == true)
                {
                    TempData["alertmsg"] = "Record Save Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";

                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertmsg"] = "Error";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                }
            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }
        public JsonResult DeleteRecord(long Id)
        {
            bool ISDeleted = false;
            ISDeleted = companyBranch.Delete(Id);
            return Json(ISDeleted);
        }

        [HttpPost]
        public JsonResult GetPanNo(int CompanyId)
        {
            string panno=companyBranch.GetPanNo(CompanyId);
            return Json(panno);
        }

    }
}
