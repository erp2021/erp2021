﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using System.Globalization;
using Garments_ERP.Filters;
using Garments_ERP.Data.Data;

namespace Garments_ERP.Controllers
{
    public class CustomerController : SuperController
    {
        
        // GET: /Customer/
        CustomerService customerservice = ServiceFactory.GetInstance().CustomerService;
        StateService stateservice = ServiceFactory.GetInstance().StateService;
        CityService cityservice = ServiceFactory.GetInstance().CityService;
        DepartmentService departmentservice = ServiceFactory.GetInstance().DepartmentService;
        CustomerTypeService customertypeservice = ServiceFactory.GetInstance().CustomerTypeService;
        CustomerContactService customercontactservice = ServiceFactory.GetInstance().CustomerContactService;
        CustomerBankService customerbankservice = ServiceFactory.GetInstance().CustomerBankService;
        CountryService countryservice = ServiceFactory.GetInstance().CountryService;
        [RBAC]
        public ActionResult Index()
        {
            var list = customerservice.get();
            return View(list);
        }
        public ActionResult BankIndex(long? id)
        {
            var list = customerbankservice.GetById((long)id);
            return View(list);
        }
        public ActionResult ContactIndex(long? id)
        {
            var list = customercontactservice.GetById((long)id);
            return View(list);
        }

        public JsonResult Getstate(int countryid)
        {
            var statelist = stateservice.get().Where(x => x.CountryID == countryid).ToList();
            return Json(statelist);
        }
        //public JsonResult verifyGSTIN(string gstin)
        //{           
        //    //return Json();
        //}
        
        public JsonResult Getstatecode(int stateid)
        {
            var statecode = stateservice.get().Where(x => x.ID == stateid).FirstOrDefault().StateCode;
            return Json(statecode);
        }
        public JsonResult CheckCustomerName(String contactname, int? Id)
        {
            bool value = true;
            if (Id == null)
                value = customerservice.get().Where(x => x.contactname == contactname).FirstOrDefault() == null;
            else
            {

                if (customerservice.get().Where(x => x.contactname == contactname && x.Id == Id).FirstOrDefault() == null)
                {
                    if (customerservice.get().Where(x => x.contactname == contactname).FirstOrDefault() == null)
                    {
                        value = true;
                    }
                    else
                    {
                        value = false;
                    }
                }
            }

            return Json(value, JsonRequestBehavior.AllowGet);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name", 101);
            ViewBag.typelist = DropDownList<M_CustomerTypeMasterEntity>.LoadItems(customertypeservice.get(), "Id", "TypeName", 1);
            ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", 1646);
            ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0);
            ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 1);
            return View();
        }
        [HttpPost]
        public ActionResult Create(FormCollection frm)
        {
            var customername = frm["contactname"];
            var customertypeid = Convert.ToInt32(frm["customertypeid"]);
            var customerphone = frm["custpmerphone"];


            // var customerGSTNo = frm["GSTNo"];
            var customeremail = frm["Email"];
            var remark = frm["remark"];
            var customerCSTNo = frm["customerCSTNo"];
            var customerTINNo = frm["cutomerTINno"];

            //  var customeraddress = frm["customeraddress"];
            // var customerstateid = Convert.ToInt32(frm["customerstateid"]);
            // var customercityid = Convert.ToInt32(frm["customercityid"]);

            //customer bank details
            var customerbank = frm["customerbank"];
            var customeraccountno = frm["customeraccountno"];
            var customerbankcode = frm["customerbankcode"];
            M_CustomerMasterEntity entity = new M_CustomerMasterEntity();
            entity.contactname = customername;
            entity.Customertype = (int)customertypeid;
            entity.Phone = customerphone;

            // entity.Address = customeraddress;
            // entity.Stateid = (int)customerstateid;
            //  entity.Cityid = (int)customercityid;


            // entity.GSTNo = customerGSTNo;
            entity.Email = customeremail;
            entity.Remark = remark;
            entity.CSTRegno = customerCSTNo;
            entity.Tinno = customerTINNo;
            entity.IsActive = true;
            entity.Createddate = DateTime.Now;
            entity.Updateddate = DateTime.Now;


            //code to save contact address details
            List<M_ContactAddressEntity> addresslistentity = new List<M_ContactAddressEntity>();
            var addresslist = frm["AddressList"].ToString();
            string[] addressarr = addresslist.Split('#');
            foreach (var adress in addressarr)
            {
                string[] adresssrr = adress.Split('~');
                M_ContactAddressEntity contactaddressentity = new M_ContactAddressEntity();
                contactaddressentity.Addess = adresssrr[0];
                contactaddressentity.Stateid = Convert.ToInt16(adresssrr[1]);
                contactaddressentity.Cityid = Convert.ToInt32(adresssrr[2]);
                contactaddressentity.Countryid = Convert.ToInt32(adresssrr[3]);
                addresslistentity.Add(contactaddressentity);
            }

            entity.contactaddresslist = addresslistentity;

            long id = customerservice.Insert(entity);
            if (id > 0)
            {

                var contactslist = frm["ContactList"].ToString();
                //code to split contactlist
                string[] contactarr = contactslist.Split('#');
                foreach (var arritem in contactarr)
                {
                    string[] contact = arritem.Split('~');
                    M_CustomerContactDetailsEntity contactentity = new M_CustomerContactDetailsEntity();
                    contactentity.Customerid = id;
                    contactentity.Contactpersonname = contact[1];
                    contactentity.Mobileno = contact[2];
                    contactentity.Telephoneno = contact[3];
                    contactentity.Email = contact[4];
                    contactentity.Faxno = contact[5];
                    contactentity.Department = Convert.ToInt32(contact[6]);
                    contactentity.Remark = contact[7];
                    contactentity.IsActive = true;
                    contactentity.Createddate = DateTime.Now;
                    contactentity.Updateddate = DateTime.Now;
                    long insertedid = customercontactservice.Insert(contactentity);
                }



                //code to save customer bank details
                M_CustomerBankDetailsEntity bankentity = new M_CustomerBankDetailsEntity();
                bankentity.Customerid = id;
                bankentity.Bankcode = customerbankcode;
                bankentity.Bankname = customerbank;
                bankentity.Accountno = customeraccountno;
                bankentity.Createddate = DateTime.Now;
                bankentity.Updateddate = DateTime.Now;
                bankentity.IsActive = true;
                long insertedbankid = customerbankservice.Insert(bankentity);
                if (insertedbankid > 0)
                {
                    TempData["alertcustmsg"] = "Record Inserted Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }

            }
            ViewBag.typelist = DropDownList<M_CustomerTypeMasterEntity>.LoadItems(customertypeservice.get(), "Id", "TypeName", 1);
            ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", 1646);
            ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0);
            ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 1);
            return View();
        }


        [HttpPost]
        public JsonResult CreateCustomer(string custname, int _customertypeid, string custphone,  string email, string _remark, string cstno, string tinno, string bankname, string accountno, string bankcode, string contactlist, string addresslist)
        {
            if (contactlist != "" && addresslist != "")
            {
                var isinsert = false;

                var customername = custname;
                var customertypeid = _customertypeid;
                var customerphone = custphone;
                //var customeraddress = custaddress;
                // var customerstateid = stateid;
                // var customerGSTNo = gstno;
                var customeremail = email;
                var remark = _remark;
                var customerCSTNo = cstno;
                var customerTINNo = tinno;
                // var customercityid = cityid;
                //customer bank details
                var customerbank = bankname;
                var customeraccountno = accountno;
                var customerbankcode = bankcode;

                M_CustomerMasterEntity entity = new M_CustomerMasterEntity();
                entity.contactname = customername;
                entity.Customertype = (int)customertypeid;
                entity.Phone = customerphone;
                //entity.Address = customeraddress;
                // entity.Stateid = (int)customerstateid;
                //  entity.Cityid = (int)customercityid;
                // entity.GSTNo = customerGSTNo;
                entity.Email = customeremail;
                entity.Remark = remark;
                entity.CSTRegno = customerCSTNo;
                entity.Tinno = customerTINNo;
                entity.IsActive = true;
                entity.Createddate = DateTime.Now;
                entity.Updateddate = DateTime.Now;

                //code to save contact address details
                List<M_ContactAddressEntity> addresslistentity = new List<M_ContactAddressEntity>();
                var addresslist_ = addresslist.ToString();
                if (addresslist_ != null && addresslist_ != "")
                {
                    string[] addressarr = addresslist_.Split('#');
                    foreach (var adress in addressarr)
                    {
                        string[] adresssrr = adress.Split('~');
                        M_ContactAddressEntity contactaddressentity = new M_ContactAddressEntity();
                        contactaddressentity.Addess = adresssrr[0] + "~" + adresssrr[5];
                        contactaddressentity.Stateid = Convert.ToInt16(adresssrr[1]);
                        contactaddressentity.Cityid = Convert.ToInt32(adresssrr[2]);
                        contactaddressentity.Countryid = Convert.ToInt32(adresssrr[3]);
                        entity.GSTNo = Convert.ToString(adresssrr[4]);
                        if (Convert.ToString(adresssrr[5]) == "1")
                        {
                            entity.Address = adresssrr[0];
                            entity.Stateid = Convert.ToInt16(adresssrr[1]);
                            entity.Cityid = Convert.ToInt32(adresssrr[2]);
                            entity.Countryid = Convert.ToInt32(adresssrr[3]);
                        }
                        addresslistentity.Add(contactaddressentity);
                    }
                }

                entity.contactaddresslist = addresslistentity;

                long id = customerservice.Insert(entity);

                if (id > 0)
                {

                    var contactslist = contactlist;
                    //code to split contactlist
                    string[] contactarr = contactslist.Split('#');
                    foreach (var arritem in contactarr)
                    {
                        string[] contact = arritem.Split('~');
                        M_CustomerContactDetailsEntity contactentity = new M_CustomerContactDetailsEntity();
                        contactentity.Customerid = id;
                        contactentity.Contactpersonname = contact[1];
                        contactentity.Mobileno = contact[2];
                        contactentity.Telephoneno = contact[3];
                        contactentity.Email = contact[4];
                        contactentity.Faxno = contact[5];
                        contactentity.Department = Convert.ToInt32(contact[6]);
                        contactentity.Remark = contact[7];
                        contactentity.IsActive = true;
                        contactentity.Createddate = DateTime.Now;
                        contactentity.Updateddate = DateTime.Now;
                        long insertedid = customercontactservice.Insert(contactentity);
                    }
                    //code to save customer bank details
                    M_CustomerBankDetailsEntity bankentity = new M_CustomerBankDetailsEntity();
                    bankentity.Customerid = id;
                    bankentity.Bankcode = customerbankcode;
                    bankentity.Bankname = customerbank;
                    bankentity.Accountno = customeraccountno;
                    bankentity.Createddate = DateTime.Now;
                    bankentity.Updateddate = DateTime.Now;
                    bankentity.IsActive = true;
                    long insertedbankid = customerbankservice.Insert(bankentity);
                    if (insertedbankid > 0)
                    {
                        isinsert = true;
                        // Response.Write("<script Language='JavaScript'>alert('Customer save successfully')</script>");
                        //  return RedirectToAction("Index");
                        return Json(isinsert);
                    }

                }
                ViewBag.typelist = DropDownList<M_CustomerTypeMasterEntity>.LoadItems(customertypeservice.get(), "Id", "TypeName", 1);
                ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", 1646);
                ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0);
                ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 1);
                return Json(isinsert);
            }
            else
            {
                return Json(false);
            }
        }
        [RBAC]
        public ActionResult Edit(long id)
        {
            M_CustomerMasterEntity model = new M_CustomerMasterEntity();
            model = customerservice.GetById(id);
            if (model.Customertype != null)
            {
                var typelist = customertypeservice.get();
                ViewBag.typelist = DropDownList<M_CustomerTypeMasterEntity>.LoadItems(typelist, "Id", "TypeName", (int)model.Customertype);
            }
            else
            {
                ViewBag.typelist = DropDownList<M_CustomerTypeMasterEntity>.LoadItems(customertypeservice.get(), "Id", "TypeName", 1);
            }
            if (model.Stateid != null)
            {
                var statelist = stateservice.get();
                ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(statelist, "ID", "Name", (long)model.Stateid);
            }
            else
            {
                ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", 0);
                model.statelist = stateservice.get();
            }
            if (model.Cityid != null)
            {
                ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", (long)model.Cityid);
                model.citylist = cityservice.getByid(1646);
            }
            else
            {
                ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0);
                model.citylist = cityservice.getByid(1646);
            }
            var countrylist = countryservice.get();
            ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countrylist, "ID", "Name", 0);
            model.countrylist = countrylist;
            List<M_CustomerContactDetailsEntity> contactentity = new List<M_CustomerContactDetailsEntity>();
            contactentity = customercontactservice.GetById(id);
            model.M_CustomerContactDetails = contactentity;

            ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 1);
            //model.deptlist departmentservice.get();
            model.deptlist = departmentservice.get();

            M_CustomerBankDetailsEntity bankentity = new M_CustomerBankDetailsEntity();
            bankentity = customerbankservice.GetById(id);
            model.M_CustomerBankDetails = bankentity;
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(FormCollection frm, M_CustomerMasterEntity model)
        {
            var customertypeid = Convert.ToInt32(frm["Customertypeid"]);

            //    var customerstateid = Convert.ToInt32(frm["Customerstateid"]);
            //   var customercityid = Convert.ToInt32(frm["Customercityid"]);
            model.Updateddate = DateTime.Now;
            model.IsActive = true;
            model.Customertype = (int)customertypeid;

            ///   model.Stateid = (int)customerstateid;
            //  model.Cityid = (int)customercityid;


            //code to save contact address details
            List<M_ContactAddressEntity> addresslistentity = new List<M_ContactAddressEntity>();
            var addresslist = frm["AddressList"].ToString();
            if (addresslist != "" && addresslist != null)
            {
                string[] addressarr = addresslist.Split('#');
                foreach (var adress in addressarr)
                {
                    string[] adresssrr = adress.Split('~');
                    M_ContactAddressEntity contactaddressentity = new M_ContactAddressEntity();
                    contactaddressentity.Addess = adresssrr[0];
                    contactaddressentity.Stateid = Convert.ToInt16(adresssrr[1]);
                    contactaddressentity.Cityid = Convert.ToInt32(adresssrr[2]);

                    addresslistentity.Add(contactaddressentity);
                }
            }
            model.contactaddresslist = addresslistentity;

            bool isUpdated = customerservice.Update(model);



            //code to split contactlist
            var contactslist = frm["ContactList"].ToString();
            string[] contactarr = contactslist.Split('#');
            foreach (var arritem in contactarr)
            {
                string[] contact = arritem.Split('~');
                M_CustomerContactDetailsEntity contactentity = new M_CustomerContactDetailsEntity();
                contactentity.Customerid = model.Id;
                contactentity.Id = Convert.ToInt64(contact[0]);
                contactentity.Contactpersonname = contact[1];
                contactentity.Mobileno = contact[2];
                contactentity.Telephoneno = contact[3];
                contactentity.Email = contact[4];
                contactentity.Faxno = contact[5];
                contactentity.Department = Convert.ToInt32(contact[6]);
                contactentity.Remark = contact[7];
                contactentity.IsActive = true;
                contactentity.Updateddate = DateTime.Now;
                if (contactentity.Id != 0)
                {
                    bool updatedid = customercontactservice.Update(contactentity);
                }
                else
                {
                    long insertedid = customercontactservice.Insert(contactentity);
                }
            }
            M_CustomerBankDetailsEntity bankentity = new M_CustomerBankDetailsEntity();
            bankentity.Customerid = model.Id;
            bankentity.Bankcode = model.M_CustomerBankDetails.Bankcode;
            bankentity.Bankname = model.M_CustomerBankDetails.Bankname;
            bankentity.Accountno = model.M_CustomerBankDetails.Accountno;
            bankentity.Updateddate = DateTime.Now;
            bankentity.IsActive = true;
            if (bankentity.Id != 0)
            {
                bool updatedbankid = customerbankservice.Update(bankentity);
                if (updatedbankid == true)
                {
                    Response.Write("<script Language='JavaScript'>alert('Customer updated successfully')</script>");
                    return RedirectToAction("Index");
                }
            }
            else
            {
                long insertedbankid = customerbankservice.Insert(bankentity);
                if (insertedbankid > 0)
                {
                    Response.Write("<script Language='JavaScript'>alert('Customer inserted successfully')</script>");
                    return RedirectToAction("Index");
                }
            }

            if (isUpdated == true)
            {
                TempData["alertcustmsg"] = "Record Updated Successfully.";
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
                if (model.Customertype != null)
                {
                    ViewBag.typelist = DropDownList<M_CustomerTypeMasterEntity>.LoadItems(customertypeservice.get(), "Id", "TypeName", (int)model.Customertype);
                }
                else
                {
                    ViewBag.typelist = DropDownList<M_CustomerTypeMasterEntity>.LoadItems(customertypeservice.get(), "Id", "TypeName", 1);
                }

            ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", (long)model.Stateid);
            if (model.Cityid != null)
            {
                ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", (long)model.Cityid);
            }
            else
            {
                ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0);
            }
            ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 1);

            return View(model);
        }

        [HttpPost]
        public JsonResult UpdateAddressdetail(long id, string address, int cityid, int stateid, int countryid)
        {
            M_ContactAddressEntity entity = new M_ContactAddressEntity();
            entity.Id = id;
            entity.Addess = address;
            entity.Cityid = cityid;
            entity.Stateid = stateid;
            entity.Countryid = countryid;
            var isupadated = customerservice.Updateadddress(entity);
            return Json(isupadated);
        }


        public JsonResult deleteaddress(long id)
        {
            var isdeleted = customerservice.deleteaddress(id);
            return Json(isdeleted);
        }
        public JsonResult getCustomerlist()
        {
            var list = customerservice.get();
            return Json(list);
        }
        [HttpPost]
        public JsonResult DeleteCustomer(long Id)
        {
            bool ISDeleted = false;
            ISDeleted = customerservice.Delete(Id);
            return Json(ISDeleted);


        }

        public JsonResult getaddress(long custid)
        {
            var list = customerservice.getAddresssbycustomerid(custid);
            return Json(list);
        }

        public JsonResult getDefaultAddressId(long custid)
        {
            List<M_CustomerMaster> list = new List<M_CustomerMaster>();
            using (var db = new GarmentERPDBEntities())
            {
                var records = db.M_CustomerMaster.Where(x => x.Id == custid).ToList();
                foreach (var item in records)
                {
                    M_CustomerMaster entity = new M_CustomerMaster();
                    entity.Address = item.Address;
                    list.Add(entity);
                }

            }
            return Json(list);

        }

        [HttpPost]
        public JsonResult getCityNameList(int stateid)
        {
            List<M_CityMasterEntity> lis = cityservice.getByid(stateid);
            return Json(lis);
        }
        [HttpPost]
        public JsonResult IsEmailExist(string strEmail, long? Id)
        {
            var contact = customerservice.get().Where(x => x.Email == strEmail).FirstOrDefault();
            if (contact != null && contact.Id != Id)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }
    }
}