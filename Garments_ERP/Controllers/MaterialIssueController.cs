﻿using Garments_ERP.Entity;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using System.IO;
using Garments_ERP.Data.Data;
using Newtonsoft.Json;
using System.Xml;
using System.Data;

namespace Garments_ERP.Controllers
{
    public class MaterialIssueController : SuperController
    {
        //
        // GET: /MaterialIssue/
        WarehouseService warehouseservice = ServiceFactory.GetInstance().WarehouseService;
        ItemService itemservice = ServiceFactory.GetInstance().ItemService;
        UnitService unitservice = ServiceFactory.GetInstance().UnitService;
        EmployeeService empservice = ServiceFactory.GetInstance().EmployeeService;
        StyleService styleservice = ServiceFactory.GetInstance().StyleService;
        MaterialIssueService materialissueservice = ServiceFactory.GetInstance().MaterialIssueService;
        ProductionPlanService ppservice = ServiceFactory.GetInstance().ProductionPlanService;
        DepartmentService deptservice = ServiceFactory.GetInstance().DepartmentService;
        SRM_GRNInwardService srmservice = ServiceFactory.GetInstance().SRMGRNInwardService;
        WorkorderService woservice= ServiceFactory.GetInstance().WorkorderService;
        BillOfMaterialService billofmaterialservice = ServiceFactory.GetInstance().BillOfMaterialService;
        SRM_GRNInwardService srmgrnservice = ServiceFactory.GetInstance().SRMGRNInwardService;

        [RBAC]
        public JsonResult getSRMitemdetail(long itemid, long MIid)
        {
            var itemdetail = materialissueservice.getbyitemid(itemid, MIid);
            return Json(itemdetail);
        }

        [RBAC]
        [HttpPost]
        public JsonResult SaveOutItem(string MyJson, int MyItemJson,string MyPIJson)
        {
            
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            int roleid = Convert.ToInt32(collection.RoleID);
            int deptid = Convert.ToInt32(collection.DeptID);
            int userid = Convert.ToInt32(collection.Id);
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            bool isinserted = false;
            dynamic json = JsonConvert.DeserializeObject(MyJson);

            dynamic json2 = JsonConvert.DeserializeObject(MyPIJson);

            M_ItemOutwardMaster entity = new M_ItemOutwardMaster();
            T_ITEM_TRANSFER inentity = new T_ITEM_TRANSFER();
            long id = 0;
            long Tid = 0;
            using (var db = new GarmentERPDBEntities())
            {
                //foreach (var data in json)
                //{
                //    string batchdata = Convert.ToString(data.BatchInfo);
                //    string[] batcharr = batchdata.Split('#');
                //    foreach(var btdata in batcharr)
                //    {
                //        var BT = btdata.Split('^');
                //        entity.Inward_ID = Convert.ToInt64(BT[8]);
                //        entity.ItemSubCategoryID = Convert.ToInt32(data.ItemSubCat);
                //        entity.Process_ID = Convert.ToDecimal(data.ProcessID);
                //        entity.MI_ID = Convert.ToInt64(data.MIid);
                //        entity.Item_ID = Convert.ToInt64(data.itemid);
                //        entity.UOM = Convert.ToInt32(data.UOM);
                //        entity.ItemQty = Convert.ToDecimal(BT[10]);
                //        entity.IsActive = true;
                //        entity.OutwardDate = DateTime.Now;
                //        entity.Item_Outid = Convert.ToInt32(MyItemJson);
                //        entity.Item_Attribute_ID = Convert.ToInt32(data.IAid);
                //        entity.Company_ID = companyid;
                //        entity.BranchId = branchid;
                //        entity.IsStatus = 3;
                //        entity.OutwardBy = userid;
                //  id = materialissueservice.Insert(entity);
                //    }
                //}

                foreach (var data in json2)
                {
                    string[] itemTI=Convert.ToString(data.TransferInfo).Split('~');
                    inentity.INVENTORY_ID = Convert.ToInt64(data.MIid);
                    inentity.STOCK_TRANSFER_NO = Convert.ToString(materialissueservice.getnextTno());
                    inentity.STOCK_TRANSFER_DATE = DateTime.ParseExact(Convert.ToString(itemTI[9]), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    inentity.WAREHOUSE_FROM = Convert.ToInt64(itemTI[5]);
                    inentity.WAREHOUSE_TO = Convert.ToInt64(itemTI[7]);
                    inentity.MODE_OF_DISPATCH= Convert.ToInt64(itemTI[1]);
                    inentity.DISPATCH_DATE = DateTime.ParseExact(Convert.ToString(itemTI[10]), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    inentity.TRANSPOTER_NAME = Convert.ToString(itemTI[2]);
                    inentity.PERSON_NAME = Convert.ToString(itemTI[3]);
                    inentity.SENDER_BY = Convert.ToInt64(userid);
                    inentity.WO_ID = Convert.ToInt64(data.WO);
                    inentity.CREATED_ON = DateTime.Now;
                    inentity.CREATED_BY =Convert.ToString(userid);
                    inentity.RECEIVED_PERSON =Convert.ToInt32(data.InchargeID);
                    inentity.IsStatus = 3;
                    inentity.Company_ID = companyid;
                    inentity.BranchId = branchid;
                    inentity.INVENTORY_MODE = 3;
                    db.T_ITEM_TRANSFER.Add(inentity);
                    db.SaveChanges();
                    Tid = inentity.TRANSFER_ID;
                    foreach (var itendata in json)
                    {
                        string batchdata = Convert.ToString(itendata.BatchInfo);
                        string[] batcharr = batchdata.Split('#');
                        foreach (var datastr in batcharr)
                        {
                            var BT = datastr.Split('^');
                            T_ITEM_TRANSFER_DETAIL TTdetails = new T_ITEM_TRANSFER_DETAIL();
                            TTdetails.TRANSFER_ID = Convert.ToInt64(Tid);
                            TTdetails.ITEM_ID = Convert.ToInt64(itendata.itemid);
                            TTdetails.TRANSFER_QTY = Convert.ToDecimal(BT[10]);
                            TTdetails.BATCH_NO_ID = Convert.ToInt64(BT[8]);
                            TTdetails.UNIT_ID = Convert.ToInt64(itendata.UOM);
                            TTdetails.ITEM_ATTRIBUTE_ID = Convert.ToInt64(itendata.IAid);
                            TTdetails.QC = false;
                            TTdetails.CREATED_ON = DateTime.Now;
                            TTdetails.CREATED_BY = Convert.ToString(userid);
                            TTdetails.Company_ID = companyid;
                            TTdetails.BranchId = branchid;
                            db.T_ITEM_TRANSFER_DETAIL.Add(TTdetails);
                            db.SaveChanges();
                        }
                    }
                }
            }
            if (Tid > 0)
            {
                isinserted = true;
            }
            return Json(isinserted);
        }

        public JsonResult getwarehouselist()
        {

            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            var list = warehouseservice.get(companyid, branchid);
            return Json(list);
        }

        public JsonResult GetProdPlanDetails(long planID)
        {
            var list = ppservice.Getbyid(planID);
            return Json(list);
        }
        public JsonResult GetWorkOrderDetails(long workorderID, long planID, long MIid)
        {
            var list = materialissueservice.GetWorkOrderDetails(workorderID, planID, MIid);
            JsonResult res = Json(list);
            return res;
        }
        public JsonResult getitemlist()
        {
            var list = itemservice.get();
            return Json(list);
        }
        public JsonResult getitemlistbyId(long id)
        {
            var list = itemservice.GetById(id);
            return Json(list);
        }
        public JsonResult getemplist()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            var list = empservice.get(comid, branchid);
            return Json(list);
        }
        public JsonResult getstylelist()
        {
            var list = styleservice.get();
            return Json(list);
        }
        public JsonResult getuomlist()
        {
            var list = unitservice.get();
            return Json(list);
        }

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Material Issue";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            var list = materialissueservice.get();
            return View(list);
        }

        
        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Material Issue";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            MFG_MaterialIssueMasterEntity entity = new MFG_MaterialIssueMasterEntity();
            ViewBag.employeelist = DropDownList<EmployeeEntity>.LoadItems(empservice.get(comid, branchid), "Id", "Name", 0);
            ViewBag.itemunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", 0);
            ViewBag.stylelist = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", 0);
            ViewBag.warehouselist = DropDownList<M_WarehouseEntity>.LoadItems(warehouseservice.get(comid, branchid), "ID", "SHORT_NAME", 0);
            ViewBag.WOlist = DropDownList<MFG_WORK_ORDEREntity>.LoadItems(woservice.GetWOMI(comid, branchid), "WorkOrderID", "WorkOrderNo", 0);
            ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(deptservice.get(), "Id", "Departmentname", 0);

            string[] MaterialIssueNo = materialissueservice.getnextmino().Split('-');
            entity.MaterialIssueNo = MaterialIssueNo[0] + "-" + MaterialIssueNo[1] + "-" + MaterialIssueNo[2] + "-" + MaterialIssueNo[3] + "-XXXX";
            return View(entity);
        }
        
        [RBAC]
        public ActionResult Create2(long id)
        {
            ViewBag.title = "Process Cycle Material Issue";
            MFG_MaterialIssueMasterEntity entity = new MFG_MaterialIssueMasterEntity();
            entity = materialissueservice.getById(id);
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            ViewBag.employeelist = DropDownList<EmployeeEntity>.LoadItems(empservice.get(comid, branchid), "Id", "Name", 0);
            ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(deptservice.get(), "Id", "Departmentname", 0);
            ViewBag.warehouselist = DropDownList<M_WarehouseEntity>.LoadItems(warehouseservice.get(comid, branchid), "ID", "SHORT_NAME", 0);
            ViewBag.itemunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", 0);
            ViewBag.stylelist = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", entity.StyleID);
            ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(deptservice.get(), "Id", "Departmentname", (int)entity.DeptId);
            ViewBag.ControlList = DropDownList<M_Dynamic_ControlsEntity>.LoadItems(srmgrnservice.GetControlList(), "Dynamic_Controls_Id", "Control_Name", 0);
            string[] TransferNo = materialissueservice.getnextTno().Split('-');
            ViewBag.TransferNo = TransferNo[0] + "-" + TransferNo[1] + "-" + TransferNo[2] + "-" + TransferNo[3] + "-XXXX";
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(MFG_MaterialIssueMasterEntity entity, FormCollection frm, List<HttpPostedFileBase> styleimg)
        {
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                int roleid = Convert.ToInt32(collection.RoleID);
                int deptid = Convert.ToInt32(collection.DeptID);
                int userid = Convert.ToInt32(collection.Id);
                int companyid = Convert.ToInt32(collection.Company_ID);
                int branchid = Convert.ToInt32(collection.BranchId);

                string styleimgpath = string.Empty;
                List<string> styleimglist = new List<string>();
                var warehouseid = frm["WarehouseID"];
                entity.WarehouseID = Convert.ToInt64(warehouseid);
                var empid = frm["EmpID"];
                entity.EmpID = Convert.ToUInt16(empid);
                var styleid = frm["StyleID"];
                entity.StyleID = Convert.ToInt32(styleid);
                string mino = Convert.ToString(frm["mino"]);
                string issuedate = frm["issuedate"];
                DateTime issue_date = DateTime.ParseExact(issuedate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                string ppdate = frm["ppdate"];
                DateTime pp_date = DateTime.ParseExact(ppdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                string wodate = frm["wodate"];
                DateTime wo_date = DateTime.ParseExact(wodate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.CreatedBy = Helper.Helper.CurrentLoggedUser.Id;
                entity.PPNo = Convert.ToInt32(frm["ppnoid"]);
                entity.RefWO = Convert.ToInt32(frm["RefWO"]);
                entity.MaterialIssueNo = mino;
                entity.IssueDate = issue_date;
                entity.PPDate = pp_date;
                entity.RefWODate = wo_date;
                entity.IsStatus = 3;
                entity.Company_ID = companyid;
                entity.BranchId = branchid;
                entity.DeptId = Convert.ToInt32(frm["DepartmentId"]);
                var rawitemstrlist = Convert.ToString(frm["rawitemdetails"]);
                var unitid = frm["unitid1"];
                string[] rawarr = rawitemstrlist.Split('#');

                int countofitem = 0;

                List<MFG_MaterialIssueItemDetailsEntity> list = new List<MFG_MaterialIssueItemDetailsEntity>();
                foreach (var item in rawarr)
                {
                    if (item != "")
                    {

                        string[] rawitem = item.Split('~');

                        decimal IssqQty = 0;

                        if (rawitem[6] != "")
                        {
                            IssqQty = Convert.ToDecimal(rawitem[6]);
                        }

                        if (IssqQty != 0)
                        {

                            MFG_MaterialIssueItemDetailsEntity rawitementity = new MFG_MaterialIssueItemDetailsEntity();
                            rawitementity.stocktype = Convert.ToString(rawitem[0]);
                            rawitementity.ItemSubCategoryID = Convert.ToInt32(rawitem[1]);
                            rawitementity.Company_ID = companyid;
                            rawitementity.BranchId = branchid;
                            if (rawitem[2] != "")
                            {
                                rawitementity.ItemID = Convert.ToInt64(rawitem[2]);
                            }

                            if (rawitem[3] != "")
                            {
                                rawitementity.Item_Attribute_ID = Convert.ToInt32(rawitem[3]);
                            }
                            if (rawitem[4] != "")
                            {
                                rawitementity.ReqQty = Convert.ToDecimal(rawitem[4]);
                            }
                            if (rawitem[5] != "")
                            {
                                rawitementity.AvailableQty = Convert.ToDecimal(rawitem[5]);
                            }
                            if (rawitem[6] != "")
                            {
                                rawitementity.IssueQty = Convert.ToDecimal(rawitem[6]);
                            }
                            if (rawitem[7] != "")
                            {
                                rawitementity.UOM_ID = Convert.ToInt16(rawitem[7]);
                            }
                            if (rawitem[8] != "")
                            {
                                rawitementity.BalanceQty = Convert.ToDecimal(rawitem[8]);
                            }

                            rawitementity.IsActive = true;

                            countofitem++;
                            list.Add(rawitementity);
                        }
                    }
                }
                List<MFG_MaterialIssueStyleImageEntity> stylelist = new List<MFG_MaterialIssueStyleImageEntity>();
                MFG_MaterialIssueStyleImageEntity styleenetity = new MFG_MaterialIssueStyleImageEntity();
                if (styleimg != null)
                {
                    foreach (var stfile in styleimg)
                    {

                        if (stfile != null && stfile.ContentLength > 0)
                        {
                            var filename = Path.GetFileName(stfile.FileName);
                            var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                            string isExistCompanypath = Server.MapPath("~/uploadedMaterialIssueStyle/");
                            bool exists = System.IO.Directory.Exists(isExistCompanypath);


                            if (!exists)
                                System.IO.Directory.CreateDirectory(isExistCompanypath);


                            var path = Path.Combine(isExistCompanypath, newfilename);
                            stfile.SaveAs(path);
                            styleimgpath = newfilename;
                            styleimglist.Add(styleimgpath);

                            styleenetity.StyleImgName = filename;
                        }

                        styleenetity.IsActive = true;
                        stylelist.Add(styleenetity);

                    }
                }
                entity.issueitemlist = list;
                entity.styleimglist = stylelist;
                entity.IsActive = true;
                long id = materialissueservice.Insert(entity);

                if (id > 0)
                {
                    TempData["alertmsg"] = "Material Issue Added Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }


        [RBAC]
        [HttpPost]
        public ActionResult Create2(MFG_MaterialIssueMasterEntity entity, FormCollection frm, List<HttpPostedFileBase> styleimg)
        {
            string styleimgpath = string.Empty;
            List<string> styleimglist = new List<string>();

            var warehouseid = frm["WarehouseID"];
            entity.WarehouseID = Convert.ToInt64(warehouseid);
            var empid = frm["EmpID"];
            entity.EmpID = Convert.ToUInt16(empid);
            var styleid = frm["StyleID"];
            entity.StyleID = Convert.ToInt32(styleid);
            string mino = frm["mino"];
            string issuedate = frm["issuedate"];
            DateTime issue_date = DateTime.ParseExact(issuedate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            string ppdate = frm["ppdate"];
            DateTime pp_date = DateTime.ParseExact(ppdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            string wodate = frm["WorkOrderDate"];
            DateTime wo_date = DateTime.ParseExact(wodate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            entity.CreatedBy = Helper.Helper.CurrentLoggedUser.Id;
            entity.PPNo = Convert.ToInt32(frm["PPNo"]);
            entity.RefWO = Convert.ToInt32(frm["WorkOrderID"]);
            entity.MaterialIssueNo = mino;
            entity.IssueDate = issue_date;
            entity.PPDate = pp_date;
            entity.RefWODate = wo_date;
            var dept = frm["DepartmentId"];
            entity.DeptId =Convert.ToInt32(dept);
            //var rawitemstrlist = frm["rawitemdetails"].ToString();
            //var unitid = frm["InItemUnit"];
            //string[] rawarr = rawitemstrlist.Split('#');
            //int countofitem = 0;

            //List<MFG_MaterialIssueItemDetailsEntity> list = new List<MFG_MaterialIssueItemDetailsEntity>();
            //foreach (var item in rawarr)
            //{
            //    string[] rawitem = item.Split('~');
            //    MFG_MaterialIssueItemDetailsEntity rawitementity = new MFG_MaterialIssueItemDetailsEntity();
            //    if (rawitem[0] != "")
            //    {
            //        rawitementity.Id = Convert.ToInt64(rawitem[0]);
            //    }
            //    if (rawitem[1] != "")
            //    {
            //        rawitementity.Process_ID = Convert.ToInt32(rawitem[1]);
            //    }
            //    if (rawitem[2] != "")
            //    {
            //        rawitementity.ItemID = Convert.ToInt32(rawitem[2]);
            //    }
            //    if (rawitem[3] != "")
            //    {
            //        rawitementity.ItemSubCategoryID = Convert.ToInt32(rawitem[3]);
            //    }
            //    if (unitid != "")
            //    {
            //        rawitementity.UOM_ID = Convert.ToInt16(unitid);
            //    }
            //    if (unitid != "")
            //    {
            //        rawitementity.ReqQty = Convert.ToDecimal(rawitem[4]);
            //    }
            //    if (rawitem[5] != "")
            //    {
            //        rawitementity.IssueQty = Convert.ToInt32(rawitem[5]);
            //    }


            //    rawitementity.IsActive = true;
            //    countofitem++;
            //    list.Add(rawitementity);

            //    }
                //List<MFG_MaterialIssueStyleImageEntity> stylelist = new List<MFG_MaterialIssueStyleImageEntity>();
                //MFG_MaterialIssueStyleImageEntity styleenetity = new MFG_MaterialIssueStyleImageEntity();
                //foreach (var stfile in styleimg)
                //{

                //    if (stfile != null && stfile.ContentLength > 0)
                //    {
                //        var filename = Path.GetFileName(stfile.FileName);
                //        var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                //        string isExistCompanypath = Server.MapPath("~/uploadedMaterialIssueStyle/");
                //        bool exists = System.IO.Directory.Exists(isExistCompanypath);


                //        if (!exists)
                //            System.IO.Directory.CreateDirectory(isExistCompanypath);


                //        var path = Path.Combine(isExistCompanypath, newfilename);
                //        stfile.SaveAs(path);
                //        styleimgpath = newfilename;
                //        styleimglist.Add(styleimgpath);

                //        styleenetity.StyleImgName = filename;
                //    }

                //    styleenetity.IsActive = true;
                //    stylelist.Add(styleenetity);

                //}

                // entity.issueitemlist = list;
                // entity.styleimglist = stylelist;
                entity.IsActive = true;
            long id = materialissueservice.Insert1(entity);

            if (id > 0)
            {
                TempData["alertmsg"] = "Material Issue Added Successfully.";
                TempData["alertclass"] = "alert alert-success alert-dismissible";
               // return RedirectToAction("Index");
            }

            return View(entity);
        }


        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "Material Issue";
            MFG_MaterialIssueMasterEntity entity = new MFG_MaterialIssueMasterEntity();

            entity = materialissueservice.getById(id);
            entity.itemmaster = itemservice.get();
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            ViewBag.employeelist = DropDownList<EmployeeEntity>.LoadItems(empservice.get(comid, branchid), "Id", "Name", entity.EmpID);
            ViewBag.itemunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", 0);
            ViewBag.stylelist = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", entity.StyleID);
            ViewBag.warehouselist = DropDownList<M_WarehouseEntity>.LoadItems(warehouseservice.get(comid, branchid), "ID", "SHORT_NAME", 1);
            ViewBag.deptlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(deptservice.get(), "Id", "Departmentname",(int)entity.DeptId);

            var ApprovalstatusList = new List<StockTypePair>
            {
               new StockTypePair { ID="Full", Name="Full" },
               new StockTypePair { ID="Per Day", Name="Per Day" },
               new StockTypePair { ID="Custom", Name="Custom" }
            };

            ViewBag.StockTypeList = DropDownList<StockTypePair>.LoadItems(ApprovalstatusList, "ID", "Name", 0);

            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(MFG_MaterialIssueMasterEntity entity, FormCollection frm, List<HttpPostedFileBase> styleimg)
        {
            try
            {
                string styleimgpath = string.Empty;
                List<string> styleimglist = new List<string>();

                var warehouseid = frm["WarehouseID"];
                entity.WarehouseID = Convert.ToInt64(warehouseid);
                var empid = frm["EmpID"];
                entity.EmpID = Convert.ToUInt16(empid);
                var styleid = frm["Style_ID"];
                entity.StyleID = Convert.ToInt32(styleid);
                //string mino = frm["mino"];
                string issuedate = frm["issuedate"];
                DateTime issue_date = DateTime.ParseExact(issuedate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                string ppdate = frm["ppdate"];
                DateTime pp_date = DateTime.ParseExact(ppdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                string wodate = frm["wodate"];
                DateTime wo_date = DateTime.ParseExact(wodate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.CreatedBy = Helper.Helper.CurrentLoggedUser.Id;
                //entity.PPNo = Convert.ToInt32(frm["ppno"]);
                //entity.RefWO = Convert.ToInt32(frm["wono"]);
                //entity.MaterialIssueNo = mino;
                entity.IssueDate = issue_date;
                entity.PPDate = pp_date;
                entity.RefWODate = wo_date;
                entity.DeptId = Convert.ToInt32(frm["DepartmentId"]);

                var rawitemstrlist = frm["rawitemdetails"].ToString();
                //var unitid = frm["unitid1"];
                string[] rawarr = rawitemstrlist.Split('#');
                int countofitem = 0;

                List<MFG_MaterialIssueItemDetailsEntity> list = new List<MFG_MaterialIssueItemDetailsEntity>();
                foreach (var item in rawarr)
                {
                    string[] rawitem = item.Split('~');
                    MFG_MaterialIssueItemDetailsEntity rawitementity = new MFG_MaterialIssueItemDetailsEntity();
                    rawitementity.ItemSubCategoryID = Convert.ToInt32(rawitem[0]);
                    if (rawitem[1] != "")
                    {
                        rawitementity.ItemID = Convert.ToInt64(rawitem[1]);
                    }
                    if (rawitem[2] != "")
                    {
                        rawitementity.Item_Attribute_ID = Convert.ToInt32(rawitem[2]);
                    }

                    if (rawitem[3] != "")
                    {
                        rawitementity.ReqQty = Convert.ToDecimal(rawitem[3]);
                    }
                    if (rawitem[4] != "")
                    {
                        rawitementity.AvailableQty = Convert.ToDecimal(rawitem[4]);
                    }
                    if (rawitem[5] != "")
                    {
                        rawitementity.IssueQty = Convert.ToDecimal(rawitem[5]);
                    }
                    if (rawitem[6] != "")
                    {
                        rawitementity.UOM_ID = Convert.ToInt16(rawitem[6]);
                    }
                    if (rawitem[7] != "")
                    {
                        rawitementity.BalanceQty = Convert.ToInt32(rawitem[7]);
                    }
                    rawitementity.Id = Convert.ToInt64(rawitem[8]);


                    rawitementity.IsActive = true;
                    countofitem++;
                    list.Add(rawitementity);

                }
                List<MFG_MaterialIssueStyleImageEntity> stylelist = new List<MFG_MaterialIssueStyleImageEntity>();
                MFG_MaterialIssueStyleImageEntity styleenetity = new MFG_MaterialIssueStyleImageEntity();
                if (styleimg != null)
                {
                    foreach (var stfile in styleimg)
                    {

                        if (stfile != null && stfile.ContentLength > 0)
                        {
                            var filename = Path.GetFileName(stfile.FileName);
                            var newfilename = string.Concat(Path.GetFileNameWithoutExtension(filename), DateTime.Now.ToString("yyyyMMddHHmmssfff"), Path.GetExtension(filename));

                            string isExistCompanypath = Server.MapPath("~/uploadedMaterialIssueStyle/");
                            bool exists = System.IO.Directory.Exists(isExistCompanypath);


                            if (!exists)
                                System.IO.Directory.CreateDirectory(isExistCompanypath);


                            var path = Path.Combine(isExistCompanypath, newfilename);
                            stfile.SaveAs(path);
                            styleimgpath = newfilename;
                            styleimglist.Add(styleimgpath);

                            styleenetity.StyleImgName = filename;
                            styleenetity.IsActive = true;
                            stylelist.Add(styleenetity);
                        }



                    }
                }
                entity.issueitemlist = list;
                entity.styleimglist = stylelist;
                entity.IsActive = true;
                bool id = materialissueservice.Update(entity);

                if (id == true)
                {
                    TempData["alertmsg"] = "Material Issue Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
            
        }

        [RBAC]
        [HttpPost]
        //Delete Material Issue
        public JsonResult Delete(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Uid = Convert.ToInt32(collection.Id);
            bool ISDeleted = false;
            ISDeleted = materialissueservice.Delete(id, Uid);
            return Json(ISDeleted);
        }

        public JsonResult deleterawitem(long id)
        {
            var isdelete = materialissueservice.deleterawitem(id);
            return Json(isdelete);
        }

        [RBAC]
        [HttpPost]
        public JsonResult getWODetials(int WOID)
        {
            var result = ppservice.getWODetials(WOID);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(result);
            return Json(JSONString);
        }

        [RBAC]
        [HttpPost]
        public JsonResult GetPerDayQty(int WOID,int ItemId,int iAttr)
        {
            var result = ppservice.GetPerDayQty(WOID, ItemId, iAttr);
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(result);
            return Json(JSONString);
        }


        //Get Status Wise List
        [RBAC]
        public PartialViewResult GetStatusData(int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            var list = materialissueservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            return PartialView("_IndexList", list);
        }

        //Form Date And To Date Wise Filter with Status List
        [RBAC]
        public PartialViewResult GetFilterStatusData(string fromdate, string todate, int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            if (fromdate != "" && todate != "")
            {

                var fdate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var tdate = DateTime.ParseExact(todate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var list = materialissueservice.get(fdate, tdate, IsStatus, Company_ID, BranchId, Userid, roleid);
                ViewBag.fromdate = fromdate;
                ViewBag.todate = todate;
                return PartialView("_IndexList", list);
            }
            else
            {
                var list = materialissueservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
                return PartialView("_IndexList", list);
            }

        }
    }
}
