﻿using Garments_ERP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Helper;
using System.IO;
using Garments_ERP.Filters;

namespace Garments_ERP.Controllers
{
    public class ItemSizeController : Controller
    {
        
        // GET: /SizeMaster/
        ItemService itemservice = ServiceFactory.GetInstance().ItemService;
        ItemSizeService itemsizeservice = ServiceFactory.GetInstance().ItemSizeService;
        StyleService styleservice = ServiceFactory.GetInstance().StyleService;

        [RBAC]
        public ActionResult Index()
        {
            var list = itemsizeservice.get();
            return View(list);
            
        }
        
        public JsonResult CheckItemSize(String Sizetype, int? Id)
        {
            bool value = true;
            if (Id == null)
                value = itemsizeservice.get().Where(x => x.Sizetype == Sizetype).FirstOrDefault() == null;
            else
            {

                if (itemsizeservice.get().Where(x => x.Sizetype == Sizetype && x.Id == Id).FirstOrDefault() == null)
                {
                    if (itemsizeservice.get().Where(x => x.Sizetype == Sizetype).FirstOrDefault() == null)
                    {
                        value = true;
                    }
                    else
                    {
                        value = false;
                    }
                }
            }
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.itemlist = DropDownList<M_ItemMasterEntity>.LoadItems(itemservice.get(), "Id", "ItemName", 0);
            ViewBag.stylename = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", 0);
            return View();
        }
        [RBAC]
   
        public ActionResult delete(int id)
        {
            bool delete = itemsizeservice.Delete(id);
            if (delete == true)
            {
                return Json(delete);
            }
            return Json(delete);
        }
        [HttpPost]
        public ActionResult Create(M_ItemSizeMasterEntity entity)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    entity.IsActive = true;
                    long id = itemsizeservice.InsertSize(entity);
                    if (id > 0)
                    {

                        TempData["alertmsg"] = "Record Save Successfully.";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception ex)
                {
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    TempData["alertmsg"] = ex.Message;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");
            
        }

        public ActionResult Edit (int id)
        {
           
            M_ItemSizeMasterEntity model = new M_ItemSizeMasterEntity();
            model = itemsizeservice.GetById(id);
            if (model.Itemid != null)
            {
                ViewBag.itemlist = DropDownList<M_ItemMasterEntity>.LoadItems(itemservice.get(), "Id", "ItemName", (int)model.Itemid);
            }
            else
            {
                ViewBag.itemlist = DropDownList<M_ItemMasterEntity>.LoadItems(itemservice.get(), "Id", "ItemName", 0);
            }
            if (model.Styleid != null)
            {
                ViewBag.stylename = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", (int)model.Styleid);
            }
            else
            {
                ViewBag.stylename = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", 0);
            }
                return View(model);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(M_ItemSizeMasterEntity model)
        {
            model.Updateddate = DateTime.Now;
            model.IsActive = true;
            bool updatedId = itemsizeservice.Update(model);            

            if (updatedId == true)
            {


                TempData["alertmsg"] = "Record Updated Successfully.";
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            if (model.Itemid != null)
            {
                ViewBag.itemlist = DropDownList<M_ItemMasterEntity>.LoadItems(itemservice.get(), "Id", "ItemName", (int)model.Itemid);
            }
            else
            {
                ViewBag.itemlist = DropDownList<M_ItemMasterEntity>.LoadItems(itemservice.get(), "Id", "ItemName", 0);
            }
            if (model.Styleid != null)
            {
                ViewBag.stylename = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", (int)model.Styleid);
            }
            else
            {
                ViewBag.stylename = DropDownList<M_StyleMasterEntity>.LoadItems(styleservice.get(), "Id", "Stylename", 0);
            }
            return View(model);


        }
    }
}
