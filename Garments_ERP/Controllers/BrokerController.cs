﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Garments_ERP.Service.Service.MasterServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class BrokerController : Controller
    {
        //
        // GET: /Broker/
        BrokerService brokerservice = ServiceFactory.GetInstance().BrokerService;
        StateService stateservice = ServiceFactory.GetInstance().StateService;
        CityService cityservice = ServiceFactory.GetInstance().CityService;
        CountryService countryservice = ServiceFactory.GetInstance().CountryService;
        [RBAC]
        public ActionResult Index()
        {
            var list = brokerservice.GetAll();
            return View(list);
        }
        public JsonResult getBrokerDetail(long brokerid)
        {
            var list = brokerservice.GetById(brokerid);
            return Json(list);
        }
        [RBAC]
        public ActionResult Create()
        {
            ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name", 101);
            ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", 1646);
            ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0);
            return View();
        }
        [RBAC]
        [HttpPost]
        public ActionResult Create(FormCollection frm, M_BrokerMasterEntity entity)
        {
            entity.BrokerName = Convert.ToString(frm["BrokerName"]);
            entity.Phone = Convert.ToString(frm["Phone"]);
            entity.Address = Convert.ToString(frm["Address"]);
            entity.Countryid = Convert.ToInt32(frm["Countryid"]);
            entity.Stateid = Convert.ToInt32(frm["Stateid"]);
            entity.Cityid = Convert.ToInt32(frm["Cityid"]);
            entity.Email = Convert.ToString(frm["Email"]);
            entity.PANNo = Convert.ToString(frm["PANNo"]);
            entity.GSTNo = Convert.ToString(frm["GSTNo"]);
            entity.Remark = Convert.ToString(frm["Remark"]);
            entity.IsActive = true;
            entity.Createddate = DateTime.Now;
            entity.Updateddate = DateTime.Now;
            long id = brokerservice.Insert(entity);
            if (id > 0)
            {

                TempData["alertmsg"] = "Record Save Successfully.";
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Create");
            }
            ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name", 101);
            ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", 1646);
            ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0);
            return View();
        }
        [RBAC]
        public ActionResult Edit(long id)
        {
            M_BrokerMasterEntity model = new M_BrokerMasterEntity();
            model = brokerservice.GetById(id);
            if (model.Countryid != null)
            {
                ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name", (long)model.Countryid);
            }
            else { ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name", 101); }
            if (model.Stateid != null)
            {
                ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.getBycountryid((long)model.Countryid), "ID", "Name", (long)model.Stateid);
            }
            else { ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", 1646); }
            if (model.Cityid != null)
            {
                ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid((long)model.Stateid), "ID", "Name", (long)model.Cityid);
            }
            else { ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0); }
            return View(model);
        }
        [RBAC]
        [HttpPost]
        public ActionResult Edit(long id, FormCollection frm, M_BrokerMasterEntity model)
        {
            model.BrokerName = Convert.ToString(frm["BrokerName"]);
            model.Phone = Convert.ToString(frm["Phone"]);
            model.Address = Convert.ToString(frm["Address"]);
            model.Countryid = Convert.ToInt32(frm["Countryid"]);
            model.Stateid = Convert.ToInt32(frm["Stateid"]);
            model.Cityid = Convert.ToInt32(frm["Cityid"]);
            model.Email = Convert.ToString(frm["Email"]);
            model.PANNo = Convert.ToString(frm["PANNo"]);
            model.GSTNo = Convert.ToString(frm["GSTNo"]);
            model.Remark = Convert.ToString(frm["Remark"]);
            model.Updateddate = DateTime.Now;
            model.IsActive = true;
            bool updatedid = brokerservice.Update(model);
            if (updatedid == true)
            {
                TempData["alertmsg"] = "Record Updated Successfully.";
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Create");
            }
            model = brokerservice.GetById(id);
            if (model.Countryid != null)
            {
                ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name", (long)model.Countryid);
            }
            else { ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name", 101); }
            if (model.Stateid != null)
            {
                ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.getBycountryid((long)model.Countryid), "ID", "Name", (long)model.Stateid);
            }
            else { ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", 1646); }
            if (model.Cityid != null)
            {
                ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid((long)model.Stateid), "ID", "Name", (long)model.Cityid);
            }
            else { ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0); }
            return View(model);
        }
        [HttpPost]
        public JsonResult DeleteBroker(long Id)
        {
            bool ISDeleted = false;
            ISDeleted = brokerservice.Delete(Id);
            return Json(ISDeleted);

        }
    }
}
