﻿using Garments_ERP.Filters;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class InventoryController : Controller
    {
        //
        // GET: /Inventory/
        WorkorderService woservice = ServiceFactory.GetInstance().WorkorderService;
        [RBAC]
        public ActionResult Index()
        {


            var list = woservice.GetWOProcessDetails();
            return View(list);
        }

    }
}
