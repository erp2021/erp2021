﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using Garments_ERP.Filters;
using Garments_ERP.Entity.MasterEntities;

namespace Garments_ERP.Controllers
{
    public class ProcessCycleController : SuperController
    {
        //
        // GET: /ProcessCycle/
        ItemCategoryService itemcategoryservice = ServiceFactory.GetInstance().ItemCategoryService;
        ProcessService process = ServiceFactory.GetInstance().ProcesService;
        ProcessCycleService proceecysle = ServiceFactory.GetInstance().ProcessCycleService;
        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Process Cycle";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            var list = proceecysle.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid);

            return View(list);
        }
        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Process Cycle";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var catelist = itemcategoryservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList();
            ViewBag.itemcategorylist_in = DropDownList<M_ItemCategoryMasterEntity>.LoadItems(catelist, "Id", "Itemcategory", 0);
            ViewBag.itemcategorylist_out = DropDownList<M_ItemCategoryMasterEntity>.LoadItems(catelist, "Id", "Itemcategory", 0);
            ViewBag.processlist = DropDownList<M_ProcessMasterEntity>.LoadItems(process.get(), "Process_Id", "SHORT_NAME", 0);
            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(FormCollection frm)
        {
            var processlist = frm["processlist"].ToString();
            var incategoryids = frm["incategoryids"].ToString();
            int i = 0;
          
            try
            {
                //Process_cycle_Name
                //Processcysledesc

                M_ProcessCycleEntity entity = new M_ProcessCycleEntity();
                entity.Process_cycle_Name = frm["Process_cycle_Name"].ToString();
                entity.Processcysledesc = frm["Processcysledesc"].ToString();

                List<ProcessCycleTransactionEntity> tranlist = new List<ProcessCycleTransactionEntity>();
                List<M_ProcessInCategoryDetailsEntity> incatelist = new List<M_ProcessInCategoryDetailsEntity>();
                if (processlist != "")
                {
                    string[] tranarray = processlist.Split('#');
                    string[] idsarray = incategoryids.Split('#');
                    foreach (var item in tranarray)
                    {
                        if (item != "")
                        {
                            string[] singleitem = item.Split('~');
                            //pro sequ icatin icatout commen
                            ProcessCycleTransactionEntity itementity = new ProcessCycleTransactionEntity();
                            itementity.Processid = Convert.ToInt16(singleitem[0]);
                            itementity.Processsequence = Convert.ToInt32(singleitem[1]);
                            itementity.incategeories = idsarray[i];
                            itementity.ItemcategoryOut = Convert.ToInt64(singleitem[2]);
                            itementity.comment = singleitem[3];
                            var sessiondat1 = Session["LoggedUser"];
                            M_UserEntity collection1 = (M_UserEntity)sessiondat1;
                            itementity.Createdby = Convert.ToInt32(collection1.Id);
                            itementity.Comapany_ID = Convert.ToInt32(collection1.Company_ID);
                            itementity.Branch_ID = Convert.ToInt32(collection1.BranchId);
                            itementity.IsActive = true;
                            itementity.Createddate = DateTime.Now;
                            tranlist.Add(itementity);
                            i++;
                        }
                    }

                    var sessiondat = Session["LoggedUser"];
                    M_UserEntity collection = (M_UserEntity)sessiondat;
                    entity.Createdby = Convert.ToInt32(collection.Id);
                    entity.Createddate = DateTime.Now;
                    entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                    entity.Branch_ID = Convert.ToInt32(collection.BranchId);

                    entity.processcyclelist = tranlist;
                    //int j = 0;
                    //foreach (var item in idsarray)
                    //{
                    //    string[] values = item.Split(',');
                    //    foreach (var value in values)
                    //    {
                    //        M_ProcessInCategoryDetailsEntity incateentity = new M_ProcessInCategoryDetailsEntity();
                    //        incateentity.ItemcategoryIn = Convert.ToInt32(value);
                    //        incateentity.IsActive = true;
                    //        incatelist.Add(incateentity);
                    //        j++;
                    //    }

                    //}
                    //entity.M_ProcessInCategoryDetails = incatelist;
                }

                int insertid = proceecysle.Insert(entity);
                if (insertid > 0)
                {
                    TempData["alertmsg"] = "Record Save Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Edit(int id)
        {
            ViewBag.title = "Process Cycle";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var catelist = itemcategoryservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid).ToList();
            M_ProcessCycleEntity entity = new M_ProcessCycleEntity();
            ViewBag.itemcategorylist_in = DropDownList<M_ItemCategoryMasterEntity>.LoadItems(catelist, "Id", "Itemcategory", 0);
            ViewBag.itemcategorylist_out = DropDownList<M_ItemCategoryMasterEntity>.LoadItems(catelist, "Id", "Itemcategory", 0);
            ViewBag.processlist = DropDownList<M_ProcessMasterEntity>.LoadItems(process.get(), "Process_Id", "SHORT_NAME", 0);
            entity = proceecysle.getbyid(id);
            // entity=
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {


            //  var processlist = frm["processlist"].ToString();
            // M_ProcessCycleEntity entity = new M_ProcessCycleEntity();
            // entity.Process_cycle_Name = frm["Process_cycle_Name"].ToString();
            // entity.Processcysledesc = frm["Processcysledesc"].ToString();

            //  List<ProcessCycleTransactionEntity> tranlist = new List<ProcessCycleTransactionEntity>();
            //  if (processlist != "")
            //{
            //    string[] tranarray = processlist.Split('#');
            //    foreach (var item in tranarray)
            //    {
            //        string[] singleitem = item.Split('~');
            //        //pro sequ icatin icatout commen
            //        ProcessCycleTransactionEntity itementity = new ProcessCycleTransactionEntity();
            //        itementity.Processid = Convert.ToInt16(singleitem[0]);
            //        itementity.Processsequence = Convert.ToInt32(singleitem[1]);
            //        itementity.ItemcategoryIn = Convert.ToInt64(singleitem[2]);
            //        itementity.ItemcategoryOut = Convert.ToInt64(singleitem[3]);
            //        itementity.comment = singleitem[4];
            //        itementity.IsActive = true;
            //        itementity.Createddate = DateTime.Now;

            //        tranlist.Add(itementity);
            //    }

            //    entity.processcyclelist = tranlist;
            //}


            //int insertid = proceecysle.Insert(entity);
            //if (insertid > 0)
            //{
            //    return RedirectToAction("Index");
            //}
            TempData["alertmsg"] = "Record Updated Successfully.";
            TempData["alertclass"] = "alert alert-success alert-dismissible";
            return RedirectToAction("Index");
        }

        [RBAC]
        public JsonResult delete(int id)
        {
            var isdelted = proceecysle.Delete(id);
            return Json(isdelted);

        }

        [RBAC]
        public JsonResult CheckProcesscycleName(String Process_cycle_Name, int? Prooce_Cycle_id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            bool value = true;
            if (Prooce_Cycle_id == null)
                value = proceecysle.get().Where(x => x.Process_cycle_Name == Process_cycle_Name && x.Company_ID==companyid && x.Branch_ID==branchid).FirstOrDefault() == null;
            else
            {

                if (proceecysle.get().Where(x => x.Process_cycle_Name == Process_cycle_Name && x.Prooce_Cycle_id == Prooce_Cycle_id && x.Company_ID == companyid && x.Branch_ID == branchid).FirstOrDefault() == null)
                {
                    if (proceecysle.get().Where(x => x.Process_cycle_Name == Process_cycle_Name && x.Company_ID == companyid && x.Branch_ID == branchid).FirstOrDefault() == null)
                    {
                        value = true;
                    }
                    else
                    {
                        value = false;
                    }
                }
            }

            return Json(value, JsonRequestBehavior.AllowGet);
        }

        [RBAC]
        public JsonResult edititem(long id, decimal proceid, long itemcat_inid, long itemcat_outid, int sequenceid, string comment)
        {
            M_ProcessCycleEntity entity = new M_ProcessCycleEntity();
            List<ProcessCycleTransactionEntity> transaentitylist = new List<ProcessCycleTransactionEntity>();
            ProcessCycleTransactionEntity transaentity = new ProcessCycleTransactionEntity();
            transaentity.Id = id;
            transaentity.ItemcategoryIn = itemcat_inid;
            transaentity.ItemcategoryOut = itemcat_outid;
            transaentity.Processid = (decimal)proceid;
            transaentity.Processsequence = sequenceid;
            transaentity.Updateddate = DateTime.Now;
            transaentity.comment = comment;
            transaentitylist.Add(transaentity);
            entity.processcyclelist = transaentitylist;
            var isupdate = proceecysle.Update(entity);
            return Json(isupdate);
        }


    }
}
