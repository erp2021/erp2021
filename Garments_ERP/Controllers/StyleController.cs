﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using Garments_ERP.Filters;
namespace Garments_ERP.Controllers
{
    public class StyleController : SuperController
    {
        //
        // GET: /Style/
        StyleService styleservice = ServiceFactory.GetInstance().StyleService;

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Style";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var list = styleservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid);
            return View(list);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Style";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(M_StyleMasterEntity entity)
        {
            
                try
                {
                    if (ModelState.IsValid)
                    {

                        entity.IsActive = true;
                        //New Code Added by Rahul on 09-01-2020
                        var sessiondat = Session["LoggedUser"];
                        M_UserEntity collection = (M_UserEntity)sessiondat;
                        entity.Createdby = Convert.ToInt32(collection.Id);
                        entity.Createddate = DateTime.Now;
                        entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        entity.Branch_ID = Convert.ToInt32(collection.BranchId);
                        long id = styleservice.Insert(entity);
                        if (id > 0)
                        {

                            TempData["alertmsg"] = "Record Save Successfully.";
                            TempData["alertclass"] = "alert alert-success alert-dismissible";
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    TempData["alertmsg"] = ex.Message;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            
            return RedirectToAction("Index");
        }

        [RBAC]
        public JsonResult createstyle(string styleno,string styname,string shortname,string styledesc)
        {
            var isinserted = false;

            M_StyleMasterEntity entity = new M_StyleMasterEntity();
            entity.Stylename = styname;
            entity.Styledescription = styledesc;
            entity.IsActive = true;
            entity.Createddate = DateTime.Now;
            long id = styleservice.Insert(entity);

            if (id > 0)
                isinserted = true;

            return Json(isinserted);
        }

        [RBAC]
        public ActionResult Edit(int id)
        {
            ViewBag.title = "Style";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            M_StyleMasterEntity entity = styleservice.get().Where(x => x.Id == id).FirstOrDefault();
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(M_StyleMasterEntity entity)
        {
            
                try
                {
                    if (ModelState.IsValid)
                    {
                        //New Code Added by Rahul on 09-01-2020
                        var sessiondat = Session["LoggedUser"];
                        M_UserEntity collection = (M_UserEntity)sessiondat;
                        entity.UpdatedBy = Convert.ToInt32(collection.Id);
                        entity.Updateddate = DateTime.Now;
                        entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        entity.Branch_ID = Convert.ToInt32(collection.BranchId);

                        bool isupdate = styleservice.Update(entity);
                        if (isupdate == true)
                        {
                            M_StyleMasterEntity entity_ = new M_StyleMasterEntity();
                            var list = styleservice.get();
                            TempData["alertmsg"] = "Record Updated Successfully.";
                            TempData["alertclass"] = "alert alert-success alert-dismissible";
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    TempData["alertmsg"] = ex.Message;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Delete(int id)
        {
            bool delete = styleservice.Delete(id);
            if (delete == true)
            {
                return Json(delete);
            }
            return Json(delete);
        }

        [RBAC]
        public JsonResult CheckStyle(string Stylename, int? Id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);
            bool value = styleservice.CheckStyleNameExistance(Stylename, Id, companyid, branchid);
            return Json(value, JsonRequestBehavior.AllowGet);
        }

    }
}
