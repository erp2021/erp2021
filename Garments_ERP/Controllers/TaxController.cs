﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using Garments_ERP.Filters;

namespace Garments_ERP.Controllers
{
    public class TaxController : SuperController
    {
        //
        // GET: /Tax/
        TaxService taxservice = ServiceFactory.GetInstance().TaxService;
        [RBAC]
        public ActionResult Index()
        {
            var list = taxservice.get();
            return View(list);
        }
        [RBAC]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(M_TaxMasterEntity entity)
        {
            
                try
                {
                    if (ModelState.IsValid)
                    {
                        entity.IsActive = true;
                        int id = taxservice.Insert(entity);
                        if (id > 0)
                        {

                            TempData["alertmsg"] = "Record Save Successfully.";
                            TempData["alertclass"] = "alert alert-success alert-dismissible";
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception ex)
                {
                    TempData["alertmsg"] = "Something Went Wrong, Please Try Again.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                }
            
            return RedirectToAction("Index");
        }
        [RBAC]
        public ActionResult Edit(int id)
        {

            M_TaxMasterEntity entity = taxservice.get().Where(x => x.Id == id).FirstOrDefault();
            return View(entity);
        }

        [HttpPost]
        public ActionResult Edit(M_TaxMasterEntity entity)
        {
            
                try
                {
                    if (ModelState.IsValid)
                    {
                        bool isupdate = taxservice.Update(entity);
                        if (isupdate == true)
                        {
                            M_ItemColorEntity entity_ = new M_ItemColorEntity();
                            var list = taxservice.get();
                            TempData["alertmsg"] = "Record Updated Successfully.";
                            TempData["alertclass"] = "alert alert-success alert-dismissible";
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception ex)
                {
                    TempData["alertmsg"] = "Something Went Wrong, Please Try Again.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                }
            
            return RedirectToAction("Index");
        }
          [RBAC]
        public ActionResult Delete(int id)
        {
            bool delete = taxservice.Delete(id);
            if (delete == true)
            {
                return Json(delete);
            }
            return Json(delete);
        }
        public JsonResult CheckTaxName(String Taxname, int? Id)
        {
            bool value = taxservice.CheckTaxNameExistance(Taxname, Id);
            return Json(value, JsonRequestBehavior.AllowGet);
        }

    }
}
