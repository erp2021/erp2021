﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class DashBoardController : SuperController
    {
        //
        // GET: /DashBoard/
        DashboardService dashboardservice = ServiceFactory.GetInstance().DashboardService;
        [RBAC]
        public ActionResult Index()
        {

            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.title = "Dashboard";

            var data = dashboardservice.Get(ViewBag.companyid, ViewBag.branchid, ViewBag.userid);
            ViewBag.MediaData = data;       
            return View();
        }

    }
}
