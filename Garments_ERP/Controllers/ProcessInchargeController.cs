﻿using Garments_ERP.Entity;
using Garments_ERP.Entity.MasterEntities;
using Garments_ERP.Filters;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class ProcessInchargeController : SuperController
    {
        //
        // GET: /ProcessIncharge/
        ProcessInchargeService ProcessInchargeservice = ServiceFactory.GetInstance().ProcessInchargeService;

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Process In Charge";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            return View();
        }

        [RBAC]
        public ActionResult Create(long id)
        {
            ViewBag.title = "Process In Charge";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            var entity = ProcessInchargeservice.getbyid(id);
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(T_ITEM_TRANSFEREntity entity,FormCollection frm)
        {   
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                int roleid = Convert.ToInt32(collection.RoleID);
                int deptid = Convert.ToInt32(collection.DeptID);
                int userid = Convert.ToInt32(collection.Id);
                int companyid = Convert.ToInt32(collection.Company_ID);
                int branchid = Convert.ToInt32(collection.BranchId);

                entity.MODIFY_BY =Convert.ToString(userid);
                entity.MODIFY_ON = DateTime.Now;
                entity.IsStatus = 4;

                string itemData = Convert.ToString(frm["rawitem"]);
                string[] splititem = itemData.Split('#');
                List<T_ITEM_TRANSFER_DETAILEntity> itemDetails = new List<T_ITEM_TRANSFER_DETAILEntity>();
                foreach (var data in splititem)
                {
                    if(data!="")
                    {
                        T_ITEM_TRANSFER_DETAILEntity ent = new T_ITEM_TRANSFER_DETAILEntity();
                        string[] rawitem = data.Split('~');
                        ent.ID = Convert.ToInt64(rawitem[0]);
                        ent.InspectedQty = Convert.ToDecimal(rawitem[2]);
                        ent.RejectedQty = Convert.ToDecimal(rawitem[3]);
                        ent.QC = Convert.ToInt32(rawitem[4]) == 1 ? true : false;
                        itemDetails.Add(ent);
                    }
                }
                entity.ITDEntiy = itemDetails;

                bool isbool = ProcessInchargeservice.Update(entity);
                if (isbool==true)
                {
                    TempData["alertmsg"] = "Quantity Accept Successful.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertmsg"] = "Please Check Log";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
                
            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = Convert.ToString(ex.ToString());
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index");

            }


        }


        //Rejected Material Code
        public JsonResult RejectedMaterial(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Uid = Convert.ToInt32(collection.Id);

            var isdelete = ProcessInchargeservice.RejectedMaterial(id, Uid);
            return Json(isdelete);
        }

        //Get Status Wise List
        public PartialViewResult GetStatusData(int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            var list = ProcessInchargeservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            return PartialView("_IndexList", list);
        }

        //Form Date And To Date Wise Filter with Status List
        [RBAC]
        public PartialViewResult GetFilterStatusData(string fromdate, string todate, int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            if (fromdate != "" && todate != "")
            {
                var fdate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var tdate = DateTime.ParseExact(todate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var list = ProcessInchargeservice.get(fdate, tdate, IsStatus, Company_ID, BranchId, Userid, roleid);
                ViewBag.fromdate = fromdate;
                ViewBag.todate = todate;
                return PartialView("_IndexList", list);
            }
            else
            {
                var list = ProcessInchargeservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
                return PartialView("_IndexList", list);
            }

        }

        

    }
}
