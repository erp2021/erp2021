﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using System.Globalization;
using Garments_ERP.Filters;

namespace Garments_ERP.Controllers
{
    public class ProductionPlanController : SuperController
    {
        WorkorderService woservice = ServiceFactory.GetInstance().WorkorderService;
        CustomerService customerservice;
        ProductionPlanService planservice=ServiceFactory.GetInstance().ProductionPlanService;
        ItemService itemservice = ServiceFactory.GetInstance().ItemService;
        SegmentTypeService segmenttypeservice = ServiceFactory.GetInstance().SegmentTypeService;
        StyleService styleservice = ServiceFactory.GetInstance().StyleService;
        ItemColorService itemcolorservice = ServiceFactory.GetInstance().ItemColorService;
        ItemshadeService itemshadeservice = ServiceFactory.GetInstance().ItemshadeService;
        WorkCenterService workCenterService = ServiceFactory.GetInstance().WorkCenterService;
        EmployeeService emloyeeService = ServiceFactory.GetInstance().EmployeeService;
        LedgerAccountServices ledgeraccountservices = ServiceFactory.GetInstance().LedgerAccountServices;
        EnquiryService enqser = ServiceFactory.GetInstance().EnquiryService;

        public void Initialize(MFG_PRODUCTION_PLANEntity productionPlan)
        {
            customerservice = ServiceFactory.GetInstance().CustomerService;
             long wrkCenterID = 0, OprtorID = 0, Apprvd = 1, TlID = 0;

            if (productionPlan.WorkCenterID != null)
                wrkCenterID = (long)productionPlan.WorkCenterID;

            if (productionPlan.OperatorID != null)
                OprtorID = (long)productionPlan.OperatorID;

            if (productionPlan.Approved != null)
                Apprvd = (int)productionPlan.Approved;

            if (productionPlan.ToolID != null)
                TlID = (int)productionPlan.ToolID;

            ViewBag.workCenterList = DropDownList<M_WorkCenterMasterEntity>.LoadItems(workCenterService.get(), "Id", "WorkCenterName", wrkCenterID);
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.operatorList = DropDownList<EmployeeEntity>.LoadItems(emloyeeService.get(comid,branchid), "ID", "Name", OprtorID);

            var ApprovalstatusList = new List<NameValuePair>
            {
               new NameValuePair { ID=1, Name="Yes" },
               new NameValuePair { ID=0, Name="No" }
            };
            ViewBag.ApprovalstatusList = DropDownList<NameValuePair>.LoadItems(ApprovalstatusList, "ID", "Name", Apprvd);

            var RequiredToolsList = new List<NameValuePair>
            {
               new NameValuePair { ID=2000, Name="Sewing Tools" },
               new NameValuePair { ID=2001, Name="Cutting Tools" },
               new NameValuePair { ID=2002, Name="Weaving Tools" },
               new NameValuePair { ID=2003, Name="Stiching Tools" }
            };
            ViewBag.requiredToolsList = DropDownList<NameValuePair>.LoadItems(RequiredToolsList, "ID", "Name", TlID);
            
        }

        public JsonResult GetWorkOrderDetails(long workorderID,long planID)
        {
            planservice = ServiceFactory.GetInstance().ProductionPlanService;
            var list = planservice.GetWorkOrderDetails(workorderID, planID);
            JsonResult res = Json(list);
            return res;
        }

        [RBAC]
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.title = "Production Plan";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            planservice = ServiceFactory.GetInstance().ProductionPlanService;
            var ProdPlanList = planservice.Get();

            return View(ProdPlanList);
        }

        [RBAC]
        [HttpGet]
        public ActionResult Edit(long planID)
        {
            ViewBag.title = "Production Plan";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            planservice = ServiceFactory.GetInstance().ProductionPlanService;
            var productionPlan = planservice.Getbyid(planID);
            customerservice = ServiceFactory.GetInstance().CustomerService;
            foreach (var custlist in ledgeraccountservices.getCustomerByWO(productionPlan.MFG_WORK_ORDER.WorkOrderID))
            {
                ViewBag.custid = custlist.Ledger_Id;
                ViewBag.custName = custlist.Ledger_Name;
            }
            Initialize(productionPlan);
            return View(productionPlan);
        }

        [RBAC]
        static string GetPlanNo()
        {
            DateTime dt = DateTime.Now;
            Random random = new Random();
            int randoNo = random.Next(1, 1000);

            return "PL-" + dt.Day + "" + dt.Month + "" + dt.Year + "" + "XXXX";
        }

        [RBAC]
        public ActionResult Create2(long WorkOrderID)
        {
            ViewBag.title = "Production Plan";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            ViewBag.WOid = WorkOrderID;
            ViewBag.PlanNo = GetPlanNo();
            customerservice = ServiceFactory.GetInstance().CustomerService;

            var enqid = woservice.GetbyId(WorkOrderID).EnquiryID;

            string[] refno = enqser.getbyid(Convert.ToInt64(enqid)).Enquiryno.Split('-');
            ViewBag.Refno = refno[4];

            foreach (var custlist in ledgeraccountservices.getCustomerByWO(WorkOrderID))
            {
                ViewBag.custid = custlist.Ledger_Id;
                ViewBag.custName = custlist.Ledger_Name;
            }
            long customerID = Convert.ToInt64(ViewBag.custid);
            foreach (var WoData in woservice.GetbyCustomerId(customerID))
            {
                if (WoData.WorkOrderID == WorkOrderID)
                {
                    ViewBag.WOno = WoData.WorkOrderNo;
                    break;
                }
            }
            Initialize(new MFG_PRODUCTION_PLANEntity());
            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create2(FormCollection frm)
        {

            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                int roleid = Convert.ToInt32(collection.RoleID);
                int deptid = Convert.ToInt32(collection.DeptID);
                int userid = Convert.ToInt32(collection.Id);
                int companyid = Convert.ToInt32(collection.Company_ID);
                int branchid = Convert.ToInt32(collection.BranchId);

                int customerID = Convert.ToInt32(frm["customerid"]);
                MFG_PRODUCTION_PLANEntity entity = new MFG_PRODUCTION_PLANEntity();
                entity.POID = Convert.ToDecimal(frm["poID"]);
                entity.CustomerID = customerID;
                entity.SampleReferenceNo = Convert.ToDecimal(frm["planSampleRefNo"]);
                entity.PlanDate = DateTime.ParseExact(Convert.ToString(frm["planDate"]), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.StartDate = DateTime.ParseExact(Convert.ToString(frm["planStartDate"]), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.TargetDate = DateTime.ParseExact(Convert.ToString(frm["planTargetDate"]), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.Target_Qty = Convert.ToDecimal(frm["TargetQty"]);
                
                entity.ToolID =(long?)null;
                entity.WorkCenterID = (frm["workCenterID"] != null) ? Convert.ToInt64(frm["workCenterID"]) : (long?)null;  //Convert.ToInt64(frm["workCenterID"]);
                entity.OperatorID =(int?)null;  //Convert.ToInt32(frm["operatorID"]);

                entity.WorkOrderID = Convert.ToInt64(frm["customerWorkOrders"]);
                
                entity.StyleID = Convert.ToInt64(frm["styleID"]);
                entity.CreatedBy = Convert.ToInt64(Helper.Helper.CurrentLoggedUser.Id);
                string approvdate = frm["approvaldate"].ToString();
                DateTime appdate = DateTime.ParseExact(approvdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.ApprovedDate = appdate;
                entity.CreatedBy = Convert.ToInt32(collection.Id);
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.BranchId = Convert.ToInt32(collection.BranchId);
                entity.Comment = Convert.ToString(frm["Comment"]);
                entity.IsActive = true;
                roleid = Convert.ToInt32(collection.RoleID);
                if (roleid != 4)
                {
                    entity.Approved = Convert.ToString(frm["approvalstatus"]) == "1" ? 1 : 0;
                    entity.IsStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? 3 : 5;
                    entity.ApprovedDate = appdate;
                }
                else
                {
                    entity.Approved = 0;
                    entity.IsStatus = 2;
                }
                entity.PerDayProduction = Convert.ToInt32(frm["PPDProd"]);
                List<MFG_PRODUCTION_PLAN_DETAILSEntity> StyleSizeDetailsList = new List<MFG_PRODUCTION_PLAN_DETAILSEntity>();
                string styleSizeDetailString = frm["styleSizeDetails"].ToString(); /// Collection of rows       
                if (styleSizeDetailString.Length > 0)
                {
                    string[] styleSizeRowDetails = styleSizeDetailString.Split('#');
                    foreach (string rwdetail in styleSizeRowDetails) // loop through each row
                    {
                        if (rwdetail != string.Empty)
                        {
                            string[] colDetails = rwdetail.Split('~'); // spit columns in a row

                            MFG_PRODUCTION_PLAN_DETAILSEntity StyleSizeDetails = new MFG_PRODUCTION_PLAN_DETAILSEntity();
                            StyleSizeDetails.Target_Qty = Convert.ToDecimal(colDetails[8].ToString());
                            StyleSizeDetails.Actual_Qty = Convert.ToDecimal(colDetails[7].ToString());
                            StyleSizeDetails.Attribute_ID = colDetails[5].ToString();
                            StyleSizeDetails.Attribute_Value_ID = colDetails[6].ToString();
                            StyleSizeDetails.rate = Convert.ToDecimal(colDetails[9].ToString());
                            StyleSizeDetails.SizeID = Convert.ToInt64(colDetails[10].ToString());
                            StyleSizeDetails.Company_ID= Convert.ToInt32(collection.Company_ID);
                            StyleSizeDetails.BranchId = Convert.ToInt32(collection.BranchId);

                            // StyleSizeDetails.PlanID = planID;
                            StyleSizeDetailsList.Add(StyleSizeDetails);
                        }
                    }
                }

                entity.detaillist = StyleSizeDetailsList;

                ///////////////////////////////////////////
                planservice = ServiceFactory.GetInstance().ProductionPlanService;
                long result = planservice.Insert(entity);
                
                if (result > 0)
                {
                    /// Redirect to Index Page
                    TempData["alertmsg"] = "Production Plan Create Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = "Error! Please Check Log.";
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                Helper.ExceptionLogging.SendErrorToText(ex);
                return RedirectToAction("Index", "WorkOrder");
            }
            return RedirectToAction("Index", "WorkOrder");
            
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            int roleid = Convert.ToInt32(collection.RoleID);
            int deptid = Convert.ToInt32(collection.DeptID);
            int userid = Convert.ToInt32(collection.Id);
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            try
            {
                int customerID = Convert.ToInt32(frm["customerid"]);
                MFG_PRODUCTION_PLANEntity entity = new MFG_PRODUCTION_PLANEntity();
                entity.ID= Convert.ToInt64(frm["hdnPlanID"]);
                entity.POID = Convert.ToDecimal(frm["poID"]);
                entity.CustomerID = customerID;
                entity.SampleReferenceNo = Convert.ToDecimal(frm["planSampleRefNo"]);
                entity.PlanDate = DateTime.ParseExact(Convert.ToString(frm["planDate"]), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.StartDate = DateTime.ParseExact(Convert.ToString(frm["planStartDate"]), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.TargetDate = DateTime.ParseExact(Convert.ToString(frm["planTargetDate"]), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.Target_Qty = Convert.ToDecimal(frm["TargetQty"]);
                entity.ToolID = null;// (frm["requiredToolID"] != null) ? Convert.ToInt64(frm["requiredToolID"]) : (long?)null;
                entity.WorkCenterID = (frm["workCenterID"] != null) ? Convert.ToInt64(frm["workCenterID"]) : (long?)null;  //Convert.ToInt64(frm["workCenterID"]);
                entity.OperatorID = null; //(frm["operatorID"] != null && frm["operatorID"] != "") ? Convert.ToInt32(frm["operatorID"]) : (int?)null;  //Convert.ToInt32(frm["operatorID"]);

                entity.WorkOrderID = Convert.ToInt64(frm["customerWorkOrders"]);
                entity.UpdatedBy = Convert.ToInt64(Helper.Helper.CurrentLoggedUser.Id);
                string upddt;
                upddt = DateTime.Now.ToShortDateString();
                entity.UpdatedDate = Convert.ToDateTime(upddt);
                entity.StyleID = Convert.ToInt64(frm["styleID"]);
                entity.CreatedBy = Convert.ToInt64(Helper.Helper.CurrentLoggedUser.Id);
                entity.CreatedDate = Convert.ToDateTime(upddt);
                entity.Approved = 1;
                entity.Comment = frm["Comment"];
                entity.PerDayProduction = Convert.ToInt32(frm["PPDProd"]);
                entity.UpdatedBy = Convert.ToInt64(collection.Id);
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.BranchId = Convert.ToInt32(collection.BranchId);
                entity.Comment = Convert.ToString(frm["comment"]);
                roleid = Convert.ToInt32(collection.RoleID);
                string approvdate = frm["approvaldate"].ToString();
                DateTime appdate = DateTime.ParseExact(approvdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.ApprovedDate = appdate;

                if (roleid != 4)
                {
                    entity.Approved = Convert.ToString(frm["approvalstatus"]) == "1" ? 1 : 0;
                    entity.IsStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? 3 : 5;
                    entity.ApprovedDate = appdate;
                }
                else
                {
                    entity.Approved = 0;
                    entity.IsStatus = 2;
                }

                entity.UpdatedDate = appdate;
                planservice = ServiceFactory.GetInstance().ProductionPlanService;
                bool result = planservice.Update(entity);

                if (result)
                {
                    /// Redirect to Index Page
                    TempData["alertmsg"] = "Record Update Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = Convert.ToString(ex);
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index", "WorkOrder");

            }
            return RedirectToAction("Index", "WorkOrder");  
        }


        public class NameValuePair
        {
            public string Name { get; set; }
            public int ID { get; set; }
        }

        //Delete ProductionPlan Code
        public JsonResult delete(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Uid = Convert.ToInt32(collection.Id);
            var deleted = planservice.Delete(id, Uid);
            return Json(deleted);

        }

        //Rejected PP Code
        public JsonResult RejectedPP(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Uid = Convert.ToInt32(collection.Id);

            var isdelete = planservice.RejectedPP(id, Uid);
            return Json(isdelete);
        }


        //Get Status Wise List
        public PartialViewResult GetStatusData(int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);


            var list = planservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            return PartialView("_IndexList", list);
        }

        //Form Date And To Date Wise Filter with Status List
        [RBAC]
        public PartialViewResult GetFilterStatusData(string fromdate, string todate, int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            if (fromdate != "" && todate != "")
            {

                var fdate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var tdate = DateTime.ParseExact(todate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var list = planservice.get(fdate, tdate, IsStatus, Company_ID, BranchId, Userid, roleid);
                ViewBag.fromdate = fromdate;
                ViewBag.todate = todate;
                return PartialView("_IndexList", list);
            }
            else
            {
                var list = planservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
                return PartialView("_IndexList", list);
            }

        }
    }


}
