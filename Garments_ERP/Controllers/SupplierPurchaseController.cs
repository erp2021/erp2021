﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class SupplierPurchaseController : SuperController
    {
        //
        // GET: /SupplierPurchase/
        UnitService unitservice = ServiceFactory.GetInstance().UnitService;
        ItemService itemservice = ServiceFactory.GetInstance().ItemService;
        DepartmentService departmentservice = ServiceFactory.GetInstance().DepartmentService;
        EmployeeService employeeservice = ServiceFactory.GetInstance().EmployeeService;
        SupplierPRService supplierprservice = ServiceFactory.GetInstance().SupplierPRService;
        SupplierPRItemDetailService supplierpritemdetailservice = ServiceFactory.GetInstance().SupplierPRItemDetailService;
        BillOfMaterialService bomservice = ServiceFactory.GetInstance().BillOfMaterialService;
        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "PR";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            return View();
        }

        
        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "PR";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            SRM_PurchaseRequestMasterEntity entity = new SRM_PurchaseRequestMasterEntity();
            var rawitemlist = itemservice.get().Where(x => x.ItemCategoryid == 2).ToList();
            ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0);
            ViewBag.employeelist = DropDownList<EmployeeEntity>.LoadItems(employeeservice.get(comid,branchid), "Id", "Name", 0);
            ViewBag.rawitemlist = DropDownList<M_ItemMasterEntity>.LoadItems(rawitemlist, "Id", "ItemName", 0);
            ViewBag.itemunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", 0);
            string[] PR_No = supplierprservice.getnextprno().Split('-');
            entity.PR_No = PR_No[0] + "-" + PR_No[1] + "-" + PR_No[2] + "-" + PR_No[3] + "-XXXX";
            
            return View(entity);
        }
        [RBAC]
        public ActionResult Create2()
        {
            ViewBag.title = "PR";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            SRM_PurchaseRequestMasterEntity entity = new SRM_PurchaseRequestMasterEntity();
            // var rawitemlist = itemservice.get().Where(x => x.ItemCategoryid == 2).ToList();
            ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0);
            ViewBag.employeelist = DropDownList<EmployeeEntity>.LoadItems(employeeservice.get(ViewBag.companyid, ViewBag.branchid), "Id", "Name", 0);
            ViewBag.bomlist = DropDownList<M_BillOfMaterialMasterEntity>.LoadItems(bomservice.getbyBOM(ViewBag.companyid, ViewBag.branchid), "BOM_ID", "BOM_No", 0);
            // ViewBag.itemunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", 0);
            string[] PR_No = supplierprservice.getnextprno().Split('-');
            entity.PR_No = PR_No[0] + "-" + PR_No[1] + "-" + PR_No[2] + "-" + PR_No[3] + "-XXXX";
            
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create2(SRM_PurchaseRequestMasterEntity entity, FormCollection frm)
        {
            
            try
            {
                string PR_Date = frm["PR_Date"];
                DateTime prdate_ = DateTime.ParseExact(PR_Date, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.PR_Date = prdate_;
                string BOMDate_ = frm["BOMDate"];
                DateTime bomdate_ = DateTime.ParseExact(BOMDate_, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.BOM_Date = bomdate_;
                var EnqID = frm["hdnEnqID"];
                var StyleId = frm["hdnStyleId"];
                if (EnqID != null && EnqID != "")
                {
                    entity.EnquiryId = Convert.ToInt32(EnqID);
                }
                else
                {
                    entity.POid = Convert.ToInt32(frm["hdnPOID"].ToString());
                }
                entity.StyleId = Convert.ToInt32(StyleId);
                var priority = frm["priority"].ToString();
                entity.Priority = priority;
                entity.IsActive = true;
                entity.CreatedDate = DateTime.Now;
                string approvdate = frm["approvaldate"].ToString();
                DateTime appdate = DateTime.ParseExact(approvdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.Approvaldate = appdate;
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                entity.CreatedBy = Convert.ToInt32(collection.Id);
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.BranchId = Convert.ToInt32(collection.BranchId);
                entity.IsReadymade = Convert.ToString(frm["IsReadymate"]) == "" ? 0 : Convert.ToInt32(frm["IsReadymate"]);
                int roleid = Convert.ToInt32(collection.RoleID);
                if (roleid != 4)
                {
                    entity.ApprovalStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? true : false;
                    entity.IsStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? 3 : 5;
                    entity.Approvaldate = DateTime.Now;
                }
                else
                {
                    entity.ApprovalStatus = false;
                    entity.IsStatus = 2;
                }
                M_BillOfMaterialMasterEntity bomentity = new M_BillOfMaterialMasterEntity();
                bomentity.BOM_ID = Convert.ToInt32(entity.BOM_Id);
                var contactslist = Convert.ToString(frm["PRItemList"]);
                long id = 0;
                if (contactslist != "")
                {
                    id = supplierprservice.Insert(entity);
                }
                long insertedid = 0;
                if (id > 0)
                {

                    //var contactslist =Convert.ToString(frm["PRItemList"]);
                    //code to split raw items
                    string[] contactarr = contactslist.Split('#');
                    foreach (var arritem in contactarr)
                    {
                        if (arritem != "")
                        {
                            string[] contact = arritem.Split('~');
                            SRM_PurchaseRequestItemDetailEntity pritementity = new SRM_PurchaseRequestItemDetailEntity();
                            pritementity.PR_ID = id;
                            pritementity.ItemCategoryId = Convert.ToInt32(contact[0]);
                            pritementity.ItemId = Convert.ToInt32(contact[1]);
                            pritementity.Item_Attribute_ID = Convert.ToInt32(contact[2]);
                            pritementity.UnitId = Convert.ToInt32(contact[3]);
                            pritementity.orderQty = Convert.ToDecimal(contact[4]);
                            pritementity.RequiredQty = Convert.ToDecimal(contact[5]);
                            pritementity.IsActive = true;
                            pritementity.CreatedDate = DateTime.Now;
                            pritementity.CreatedBy = Convert.ToInt32(collection.Id);
                            pritementity.Company_ID = Convert.ToInt32(collection.Company_ID);
                            pritementity.BranchId = Convert.ToInt32(collection.BranchId);
                            insertedid = supplierpritemdetailservice.Insert(pritementity);
                        }
                    }
                    if (insertedid > 0)
                    {
                        bool updatestatus = false;
                        bomentity.IsPRDone = true;
                        updatestatus = bomservice.UpdatePRDone(bomentity);
                        TempData["alertmsg"] = "Supplier purchase request added successfully.";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    TempData["alertmsg"] = "Item Data Not Save. Please Check Code";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            
            return RedirectToAction("Index");
        }

        [RBAC]
        public JsonResult getBOMDetails(long bomid)
        {
            string JSONString = string.Empty;
            try
            {
                var list = bomservice.GetDetailsForPRByBOM(bomid);
                
                JSONString = JsonConvert.SerializeObject(list);
                return Json(JSONString);
            }
            catch (Exception ex)
            {
                TempData["alertprmsg"] = "Something went wrong, Please try again.";
                TempData["alertclass"] = "alert alert-danger alert-dismissible";
                JSONString = JsonConvert.SerializeObject(ex);
                return Json(JSONString);
            }
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(SRM_PurchaseRequestMasterEntity entity, FormCollection frm)
        {
            string PR_Date = frm["PR_Date"];
            DateTime prdate = DateTime.ParseExact(PR_Date, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            entity.PR_Date = prdate;
            var priority = frm["priority"].ToString();
            entity.Priority = priority;
            entity.IsActive = true;
            entity.CreatedDate = DateTime.Now;
            long id = supplierprservice.Insert(entity);
            long insertedid = 0;
            if (id > 0)
            {

                var contactslist = frm["PRItemList"].ToString();
                //code to split raw items
                string[] contactarr = contactslist.Split('#');
                foreach (var arritem in contactarr)
                {
                    string[] contact = arritem.Split('~');
                    SRM_PurchaseRequestItemDetailEntity pritementity = new SRM_PurchaseRequestItemDetailEntity();
                    pritementity.PR_ID = id;
                    pritementity.ItemCategoryId = Convert.ToInt32(contact[0]);
                    pritementity.ItemId = Convert.ToInt32(contact[1]);
                    pritementity.Item_Attribute_ID = Convert.ToInt32(contact[2]);
                    pritementity.UnitId = Convert.ToInt32(contact[3]);
                    pritementity.ItemDesc = contact[4];
                    pritementity.Comment = contact[5];
                    pritementity.RequiredDate = DateTime.Now;
                    pritementity.RequiredQty = Convert.ToDecimal(contact[6]);
                    
                    pritementity.IsActive = true;
                    pritementity.CreatedDate = DateTime.Now;
                    pritementity.UpdatedDate = DateTime.Now;
                    insertedid = supplierpritemdetailservice.Insert(pritementity);
                }
                if (insertedid > 0)
                {
                    TempData["alertmsg"] = "Supplier purchase request added successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            var rawitemlist = itemservice.get().Where(x => x.ItemCategoryid == 2).ToList();
            ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0);
            ViewBag.employeelist = DropDownList<EmployeeEntity>.LoadItems(employeeservice.get(comid,branchid), "Id", "Name", 0);
            ViewBag.rawitemlist = DropDownList<M_ItemMasterEntity>.LoadItems(rawitemlist, "Id", "ItemName", 0);
            ViewBag.itemunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", 0);
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public JsonResult DeletePR(long Id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Userid = Convert.ToInt32(collection.Id);
            bool ISDeleted = false;
            ISDeleted = supplierprservice.Delete(Id, Userid);
            return Json(ISDeleted);
        }

        [RBAC]
        public JsonResult deleterawitem(long id)
        {
            var isdelete = supplierpritemdetailservice.Delete(id);
            return Json(isdelete);

        }

        [RBAC]
        public JsonResult editrawitem(long id, long itemid, decimal itemqty, string itemdesc, string comment, int unitid)
        {
            SRM_PurchaseRequestItemDetailEntity rawentity = new SRM_PurchaseRequestItemDetailEntity();
            rawentity.Id = id;
            rawentity.ItemId = itemid;
            rawentity.RequiredQty = itemqty;
            rawentity.ItemDesc = itemdesc;
            rawentity.Comment = comment;            
            rawentity.UnitId = unitid;        
            rawentity.IsActive = true;
            bool isupdate = supplierpritemdetailservice.Update(rawentity);
            return Json(isupdate);

        }

        [RBAC]
        public ActionResult Edit(long id)
        {
            SRM_PurchaseRequestMasterEntity entity = new SRM_PurchaseRequestMasterEntity();
            try
            {
                ViewBag.title = "PR";
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                ViewBag.roleid = Convert.ToInt32(collection.RoleID);
                ViewBag.deptid = Convert.ToInt32(collection.DeptID);
                ViewBag.userid = Convert.ToInt32(collection.Id);
                ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
                ViewBag.branchid = Convert.ToInt32(collection.BranchId);
                ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
                ViewBag.Username = Convert.ToString(collection.Username);

                entity = supplierprservice.getbyid(id);
                entity.SRM_PurchaseRequestItemDetail = supplierpritemdetailservice.getbyid(id);
                //var rawitemlist = itemservice.get().Where(x => x.Itemsubcategoryid == entity).ToList();
                if (entity.DepartmentId != null)
                {
                    ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", (int)entity.DepartmentId);
                }
                else { ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0); }
                
                if (entity.AccountHeadId != null)
                {
                    ViewBag.employeelist = DropDownList<EmployeeEntity>.LoadItems(employeeservice.get(ViewBag.companyid, ViewBag.branchid), "Id", "Name", (int)entity.AccountHeadId);
                }
                else { ViewBag.employeelist = DropDownList<EmployeeEntity>.LoadItems(employeeservice.get(ViewBag.companyid, ViewBag.branchid), "Id", "Name", 0); }
                if (entity.BOM_Id != null)
                {
                    ViewBag.bomlist = DropDownList<M_BillOfMaterialMasterEntity>.LoadItems(bomservice.get().OrderByDescending(x => x.BOM_ID).ToList(), "BOM_ID", "BOM_No", (long)entity.BOM_Id);
                }
                else { ViewBag.bomlist = DropDownList<M_BillOfMaterialMasterEntity>.LoadItems(bomservice.get().OrderByDescending(x => x.BOM_ID).ToList(), "BOM_ID", "BOM_No", 0); }
                //entity.unitlist_ = unitservice.get();
                //entity.itemlist_ = rawitemlist;
                return View(entity);
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(long id, SRM_PurchaseRequestMasterEntity entity, FormCollection frm)
        {
            
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                int roleid = Convert.ToInt32(collection.RoleID);
                int deptid = Convert.ToInt32(collection.DeptID);
                int userid = Convert.ToInt32(collection.Id);
                int companyid = Convert.ToInt32(collection.Company_ID);
                int branchid = Convert.ToInt32(collection.BranchId);

                string PR_Date = frm["PR_Date"];
                DateTime prdate = DateTime.ParseExact(PR_Date, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                entity.PR_Date = prdate;
                var priority = frm["priority"].ToString();
                entity.Priority = priority;
                entity.IsActive = true;
                entity.Approvaldate = DateTime.Now;
                if (roleid != 4)
                {
                    entity.ApprovalStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? true : false;
                    entity.IsStatus = Convert.ToString(frm["approvalstatus"]) == "1" ? 3 : 5;
                    entity.Approvaldate = DateTime.Now;
                }
                else
                {
                    entity.IsStatus = 2;
                }
                entity.UpdatedBy = userid;
                entity.Company_ID = companyid;
                entity.BranchId = branchid;
                bool isUpdated = supplierprservice.Update(entity);

                if(isUpdated==true)
                {
                    var contactslist = frm["PRItemList"].ToString();
                    //code to split raw items
                    string[] contactarr = contactslist.Split('#');
                    foreach (var arritem in contactarr)
                    {
                        if (arritem != "")
                        {
                            string[] contact = arritem.Split('~');
                            SRM_PurchaseRequestItemDetailEntity pritementity = new SRM_PurchaseRequestItemDetailEntity();
                            pritementity.PR_ID = id;
                            pritementity.ItemCategoryId = Convert.ToInt32(contact[0]);
                            pritementity.ItemId = Convert.ToInt32(contact[1]);
                            pritementity.Item_Attribute_ID = Convert.ToInt32(contact[2]);
                            pritementity.UnitId = Convert.ToInt32(contact[3]);
                            pritementity.RequiredQty = Convert.ToDecimal(contact[5]);
                            pritementity.Id = Convert.ToInt64(contact[7]);
                            pritementity.IsActive = true;
                            pritementity.CreatedDate = DateTime.Now;
                            pritementity.UpdatedDate = DateTime.Now;
                            bool insertedid = supplierpritemdetailservice.Update(pritementity);
                        }
                    }
                }
                    
                if (isUpdated == true)
                {
                    TempData["alertmsg"] = "Supplier purchase request updated successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
                
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult DemoPR()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            SRM_PurchaseRequestMasterEntity entity = new SRM_PurchaseRequestMasterEntity();
            var rawitemlist = itemservice.get().Where(x => x.ItemCategoryid == 2).ToList();
            ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0);
            ViewBag.employeelist = DropDownList<EmployeeEntity>.LoadItems(employeeservice.get(comid,branchid), "Id", "Name", 0);
            ViewBag.rawitemlist = DropDownList<M_ItemMasterEntity>.LoadItems(rawitemlist, "Id", "ItemName", 0);
            ViewBag.itemunitlist = DropDownList<M_UnitMasterEntity>.LoadItems(unitservice.get(), "Id", "ItemUnit", 0);
            entity.PR_No = supplierprservice.getnextprno();
            return View(entity);
        }

        //Rejected PR Code
        [RBAC]
        public JsonResult RejectedPR(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Uid = Convert.ToInt32(collection.Id);

            var isdelete = supplierprservice.RejectedPR(id, Uid);
            return Json(isdelete);
        }

        //Get Status Wise List
        [RBAC]
        public PartialViewResult GetStatusData(int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            var list = supplierprservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            return PartialView("_IndexList", list);
        }

        //Form Date And To Date Wise Filter with Status List
        [RBAC]
        public PartialViewResult GetFilterStatusData(string fromdate, string todate, int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            if (fromdate != "" && todate != "")
            {

                var fdate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var tdate = DateTime.ParseExact(todate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var list = supplierprservice.get(fdate, tdate, IsStatus, Company_ID, BranchId, Userid, roleid);
                ViewBag.fromdate = fromdate;
                ViewBag.todate = todate;
                return PartialView("_IndexList", list);
            }
            else
            {
                var list = supplierprservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
                return PartialView("_IndexList", list);
            }

        }

        //Get Account Head With Deprtment Wise
        [RBAC]
        public JsonResult GetAccountHead(int deptid)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            var list = supplierprservice.GetAccountHead(deptid, Company_ID, BranchId);
            return Json(list);
        }
    }
}
