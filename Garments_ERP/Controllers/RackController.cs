﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class RackController : SuperController
    {
        //
        // GET: /Rack/
        RackService rackservice = ServiceFactory.GetInstance().RackService;
        DepartmentService departmentservice = ServiceFactory.GetInstance().DepartmentService;
        WarehouseService warehouseservice = ServiceFactory.GetInstance().WarehouseService;
        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Rack";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            var list = rackservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid);
            return View(list);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Rack";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0);
            ViewBag.warehouselist = DropDownList<M_WarehouseEntity>.LoadItems(warehouseservice.get(comid, branchid), "ID", "SHORT_NAME", 0);
            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(M_RackMasterEntity model)
        {
            try
            {
                model.IsActive = true;
                model.Createdate = DateTime.Now;
                //model.Updatedate = DateTime.Now;
                //New Code Added by Rahul on 09-01-2020
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                model.Createdby = Convert.ToInt32(collection.Id);
                // model.Createddate = DateTime.Now;
                model.Company_ID = Convert.ToInt32(collection.Company_ID);
                model.Branch_ID = Convert.ToInt32(collection.BranchId);

                int insertedid = rackservice.Insert(model);
                if (insertedid > 0)
                {

                    TempData["alertmsg"] = "Rack Save Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = Convert.ToString(ex);
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");

            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Edit(int id)
        {
            ViewBag.title = "Rack";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            M_RackMasterEntity model = new M_RackMasterEntity();
            model = rackservice.GetById(id);
            if (model.DepartmentId != null) { ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", (int)model.DepartmentId); }
            else { ViewBag.departmentlist = DropDownList<M_DepartmentMasterEntity>.LoadItems(departmentservice.get(), "Id", "Departmentname", 0); }
            if (model.WarehouseId != null)
            {
                ViewBag.warehouselist = DropDownList<M_WarehouseEntity>.LoadItems(warehouseservice.get(comid, branchid), "ID", "SHORT_NAME", (int)model.WarehouseId);
            }
            else { ViewBag.warehouselist = DropDownList<M_WarehouseEntity>.LoadItems(warehouseservice.get(comid, branchid), "ID", "SHORT_NAME", 0); }
            return View(model);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(M_RackMasterEntity entity)
        {
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                int comid = Convert.ToInt32(collection.Company_ID);
                int branchid = Convert.ToInt32(collection.BranchId);

                entity.IsActive = true;
                //entity.Createdate = DateTime.Now;
                entity.Updatedate = DateTime.Now;
                entity.Updatedby = Convert.ToInt32(collection.Id);
                // model.Createddate = DateTime.Now;
                entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                entity.Branch_ID = Convert.ToInt32(collection.BranchId);
                bool updatedid = rackservice.Update(entity);
                if (updatedid == true)
                {
                    TempData["alertmsg"] = "Rack Updated Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = Convert.ToString(ex);
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");

            }
            return RedirectToAction("Index");
            
        }
       
        [RBAC]
        [HttpPost]
        public JsonResult DeleteRack(int Id)
        {
            bool ISDeleted = false;
            ISDeleted = rackservice.Delete(Id);
            return Json(ISDeleted);

        }
    }
}
