﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Garments_ERP.Service.Service.MasterServices;
using Newtonsoft.Json;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class SupplierGRNController : SuperController
    {
        //
        // GET: /SupplierGRN/
        WarehouseService warehouseservice = ServiceFactory.GetInstance().WarehouseService;
        SupplierService supplierservice = ServiceFactory.GetInstance().SupplierService;
        DepartmentService departmentservice = ServiceFactory.GetInstance().DepartmentService;
        ItemService itemservice = ServiceFactory.GetInstance().ItemService;
        UnitService unitservice = ServiceFactory.GetInstance().UnitService;
        SRM_GRNInwardService srmgrnservice = ServiceFactory.GetInstance().SRMGRNInwardService;
        SupplierPOService poservice = ServiceFactory.GetInstance().SupplierPOService;
        ProcessingStatusService processingstatusservice = ServiceFactory.GetInstance().ProcessingStatusService;
        InwardItemService inwarditemservice = ServiceFactory.GetInstance().InwardItemService;
        LedgerAccountServices ledgeraccountservices = ServiceFactory.GetInstance().LedgerAccountServices;
        RackService rackservice = ServiceFactory.GetInstance().RackService;

        [RBAC]
        public JsonResult getwarehouselist()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);

            var list = warehouseservice.get(ViewBag.companyid, ViewBag.branchid);
            return Json(list);
        }

        [RBAC]
        public JsonResult processingStatuslist()
        {
            var list = processingstatusservice.get();
            return Json(list);
        }

        [RBAC]
        public JsonResult getsupplierlist()
        {
            int CBS = 2;//Supplier ID
            List<M_LedgersEntity> list = new List<M_LedgersEntity>();
            list = ledgeraccountservices.getCustList(CBS);
            return Json(list);
        }

        [RBAC]
        public JsonResult getdepartmentlist()
        {
            var list = departmentservice.get(); 
            return Json(list);
        }

        [RBAC]
        public JsonResult getitemlist()
        {
            var list = itemservice.get();
            return Json(list);
        }

        [RBAC]
        public JsonResult getPOlist(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            int roleid = Convert.ToInt32(collection.RoleID);
            int deptid = Convert.ToInt32(collection.DeptID);
            int userid = Convert.ToInt32(collection.Id);
            int companyid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);

            var list = poservice.Getbysupplierid(id).Where(x=>x.IsStatus==3 && x.Company_ID==companyid && x.BranchId==branchid).OrderByDescending(x=>x.Id).ToList();
            return Json(list);
        }

        [RBAC]
        public JsonResult getPOAdd(long id)
        {
            var list = srmgrnservice.GetbysupplierAdd(id);
            return Json(list);
        }

        [RBAC]
        public JsonResult getItemBal(long poid,long itemid,int IAttr)
        {
            var list = srmgrnservice.getItemBal(poid,itemid, IAttr);
            return Json(list);
        }

        [RBAC]
        public JsonResult UpdateGRNitem(long id, long itemid, decimal poqty, decimal balanceqty, decimal chalanqty, decimal grnqty, int unitid,int BatchNO,string SerialNO)
        {
            SRM_GRNItemEntity itementity = new SRM_GRNItemEntity();
            itementity.ID = id;
            itementity.ItemId = itemid;
            itementity.BatchNO = BatchNO;
            itementity.SerialNo = SerialNO;
            itementity.PO_Qty = poqty;
            itementity.Balance_Qty = balanceqty;
            itementity.Challan_Qty = chalanqty;
           // itementity.Accepted_Qty = acceptqty;
            itementity.GRN_Qty = grnqty;
            itementity.UnitId = unitid;
            var update = srmgrnservice.UpdateGRNItem(itementity);
            return Json(update);
        }

        [RBAC]
        public JsonResult DeleteGRNitem(long id)
        {
            var deleted = srmgrnservice.DeleteGRNItem(id);
            return Json(deleted);
        }

        [RBAC]
        public JsonResult GetPOdetails(long id)
        {
            var record = poservice.getbyid(id);
            // record.Items = itemservice.get();
            //record.Unitlist = unitservice.get();
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(record);
            return Json(JSONString);
        }
       
        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "GRN";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var list = srmgrnservice.get();
            return View(list);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Index(string fromdate, string todate)
        {
            if (fromdate != "" && todate != "")
            {


                var fdate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var tdate = DateTime.ParseExact(todate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var list = srmgrnservice.get(fdate, tdate);
                ViewBag.fromdate = fromdate;
                ViewBag.todate = todate;
                return View(list);
            }
            else
            {
                var list = srmgrnservice.get();
                return View(list);
            }
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "GRN";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            string[] grnno = srmgrnservice.getnextgrnno().Split('-');
            ViewBag.grnno = grnno[0] + "-" + grnno[1] + "-" + grnno[2] + "-" + grnno[3] + "-XXXX";

            //string[] chnno = srmgrnservice.getnextchnno().Split('-');
            //ViewBag.chnno = chnno[0] + "-" + chnno[1] + "-" + chnno[2] + "-" + chnno[3] + "-XXXX";

            ViewBag.chnno = srmgrnservice.getnextchnno();


            string[] invno = srmgrnservice.getnextinvno().Split('-');
            ViewBag.invno = invno[0] + "-" + invno[1] + "-" + invno[2] + "-" + invno[3] + "-XXXX";

            ViewBag.ControlList = DropDownList<M_Dynamic_ControlsEntity>.LoadItems(srmgrnservice.GetControlList(), "Dynamic_Controls_Id", "Control_Name", 0);

            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(FormCollection frm, SRM_GRNInwardEntity inwardentity)
        {
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                int roleid = Convert.ToInt32(collection.RoleID);
                int userid = Convert.ToInt32(collection.Id);
                int companyid = Convert.ToInt32(collection.Company_ID);
                int branchid = Convert.ToInt32(collection.BranchId);

                inwardentity.WarehouseId = Convert.ToInt32(frm["warehouseid"]);
                inwardentity.SupplierId = Convert.ToInt32(frm["supplierid"]);
                inwardentity.POid = Convert.ToInt64(frm["pono"]);
                inwardentity.GRNNo = frm["grnno"];
                string inwarddate = frm["inwardDate"].ToString();
                DateTime inwadate = DateTime.ParseExact(inwarddate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                inwardentity.InwardDate = inwadate;
                inwardentity.ChallanNO = frm["challanno"];
                string challanddate = frm["challanDate"].ToString();
                DateTime chldate = DateTime.ParseExact(challanddate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                inwardentity.ChallanDate = chldate;                
                inwardentity.InvoiceNo = frm["invoiceno"];
                string invoicedate = frm["invoiceDate"].ToString();
                DateTime invdate = DateTime.ParseExact(invoicedate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                inwardentity.InvoiceDate = invdate;
                int deptid = Convert.ToInt32(frm["departmentid"]);
                if (deptid != 0)
                {
                    inwardentity.DepartmentId = deptid;
                }
                inwardentity.ProcessingStatusId = Convert.ToInt32(frm["processingid"]);
                inwardentity.CreatedDate = DateTime.Now;
                inwardentity.IsActive = true;
                inwardentity.IsStatus = 3;
                inwardentity.CreatedBy = userid;
                inwardentity.InwardBy = userid;
                inwardentity.Company_ID = companyid;
                inwardentity.BranchId = branchid;
                inwardentity.IsReadymade = Convert.ToString(frm["IsReadymate"]) == "" ? 0 : Convert.ToInt32(frm["IsReadymate"]);
                //save grn item details

                var grnitemstr = frm["grnitemstr"].ToString();
                string[] itemarr = grnitemstr.Split('#');
                var grnitemPIstr = Convert.ToString(frm["grnitemPIstr"]);
                string[] itemPIarr = grnitemPIstr.Split('|');
                List<SRM_GRNItemEntity> listitem = new List<SRM_GRNItemEntity>();
                int i = 0;
                foreach (var item in itemarr)
                {
                    if (item != "")
                    {
                        SRM_GRNItemEntity itementity = new SRM_GRNItemEntity();
                        string[] grnitem = item.Split('~');
                        if (Convert.ToString(grnitem[7])!="" && Convert.ToString(grnitem[7]) != "0")
                        {
                            itementity.ItemId = Convert.ToInt64(grnitem[1]);
                            itementity.Item_Attribute_ID = (Convert.ToInt32(grnitem[2]) > 0) ? Convert.ToInt32(grnitem[2]) : 0;
                            itementity.HSNCode = Convert.ToString(grnitem[3]);
                            itementity.gstRate = Convert.ToDecimal(grnitem[4]);
                            itementity.PO_Qty = Convert.ToDecimal(grnitem[5]);
                            itementity.Challan_Qty = Convert.ToDecimal(grnitem[6]);
                            itementity.Accepted_Qty = Convert.ToDecimal(grnitem[7]);
                            itementity.GRN_Qty = Convert.ToDecimal(grnitem[8]);
                            itementity.Balance_Qty = Convert.ToDecimal(grnitem[9]);
                            itementity.UnitId = Convert.ToInt32(grnitem[10]);
                            itementity.IsActive = true;
                            itementity.CreatedBy = userid;
                            itementity.Company_ID = companyid;
                            itementity.BranchId = branchid;
                            string[] grnitem1 = itemPIarr[i].Split('^');
                            itementity.itemPIval = grnitem1[0];
                            itementity.itemPIId = grnitem1[1];
                            listitem.Add(itementity);
                        }
                        i++;
                    }
                }
                
                inwardentity.SRM_GRNItemlist = listitem;
                long id = 0;
                if (listitem.Count > 0)
                {
                     id = srmgrnservice.Insert(inwardentity);
                }
                if (id > 0)
                {
                    TempData["alertmsg"] = "Record Save Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
                //else
                //{
                //    Response.Write("<script Language='JavaScript'>alert('Something went wrong.')</script>");
                //}
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");

            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Edit(long id)
        {
            ViewBag.title = "GRN";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            var record = srmgrnservice.getbyid(id);
            //record.itemlist = itemservice.get();
            record.unitlist = unitservice.get();
            if (record.ProcessingStatusId != null)
            {
                ViewBag.processingstatuslist = DropDownList<M_ProcessingStatusEntity>.LoadItems(processingstatusservice.get(), "Id", "ProcessingStatusName", (int)record.ProcessingStatusId);
            }
            else { ViewBag.processingstatuslist = DropDownList<M_ProcessingStatusEntity>.LoadItems(processingstatusservice.get(), "Id", "ProcessingStatusName", 0); }
            ViewBag.ControlList = DropDownList<M_Dynamic_ControlsEntity>.LoadItems(srmgrnservice.GetControlList(), "Dynamic_Controls_Id", "Control_Name", 0);
            return View(record);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(FormCollection frm, SRM_GRNInwardEntity inwardentity)
        {
            try
            {
                inwardentity.ChallanNO = frm["challanno"];
                string challanddate = frm["challanDate"].ToString();
                DateTime chldate = DateTime.ParseExact(challanddate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                inwardentity.ChallanDate = chldate;
                inwardentity.InvoiceNo = frm["invoiceno"];

                string invoicedate = frm["invoiceDate"].ToString();
                DateTime invdate = DateTime.ParseExact(invoicedate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                inwardentity.InvoiceDate = invdate;
                inwardentity.IsActive = true;
                inwardentity.UpdatedDate = DateTime.Now;

                bool status = srmgrnservice.Update(inwardentity);
                if (status == true)
                {

                    TempData["alertmsg"] = "Record Update Successfully.";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            }
            catch(Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        //Delete Supplier GRN Code
        [RBAC]
        [HttpPost]
        public JsonResult Delete(long id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int Userid = Convert.ToInt32(collection.Id);
            var deleted = srmgrnservice.Delete(id, Userid);
            return Json(deleted);
        }

        [RBAC]
        public ActionResult SupplierGRNReport(long id)
        {

            var list = srmgrnservice.GRNReportGet(id);
            return View(list);

        }

        [HttpPost]
        [ValidateInput(false)]
        public FileResult Export(string GridHtml)
        {

            // read parameters from the webpage
            string htmlString = "<html><head><title>GRN Report</title><link rel='stylesheet' href='http://localhost:2423/Content/dist/css/AdminLTE.min.css'><link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'></head><body><div class='row'><div class='col-sm-12 text-center'><h3>GRN Report</h3></div></div>" + GridHtml + "</body></html>";
            string baseUrl = "";

            string pdf_page_size = "A4";
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                pdf_page_size, true);

            string pdf_orientation = "Portrait";
            PdfPageOrientation pdfOrientation =
                (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                pdf_orientation, true);

            int webPageWidth = 1024;
            int webPageHeight = 0;


            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;

            converter.Options.DisplayFooter = true;
            converter.Options.MarginTop = 10;
            converter.Options.MarginLeft = 10;
            converter.Options.MarginRight = 10;
            converter.Options.MarginBottom = 10;
            converter.Options.PdfCompressionLevel = PdfCompressionLevel.Best;
            converter.Options.PdfPageSize = PdfPageSize.Letter;
            converter.Options.PdfStandard = PdfStandard.Full;
            converter.Options.JavaScriptEnabled = true;
            converter.Options.DrawBackground = false;
            converter.Options.AutoFitWidth = HtmlToPdfPageFitMode.AutoFit;
            converter.Options.CssMediaType = HtmlToPdfCssMediaType.Print;
            PdfTextSection text = new PdfTextSection(0, 10, "Page: {page_number} of {total_pages} ", new System.Drawing.Font("Arial", 8));
            text.HorizontalAlign = PdfTextHorizontalAlign.Right;

            // create a new pdf document converting an url
            SelectPdf.PdfDocument doc = converter.ConvertHtmlString(htmlString, baseUrl);

            // save pdf document
            byte[] pdf = doc.Save();

            // close pdf document
            doc.Close();

            // return resulted pdf document
            FileResult fileResult = new FileContentResult(pdf, "application/pdf");
            fileResult.FileDownloadName = "Document.pdf";
            return fileResult;

        }

        //Get Status Wise List
        [RBAC]
        public PartialViewResult GetStatusData(int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            var list = srmgrnservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
            return PartialView("_IndexList", list);
        }

        //Form Date And To Date Wise Filter with Status List
        [RBAC]
        public PartialViewResult GetFilterStatusData(string fromdate, string todate, int IsStatus)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);
            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.Status = Convert.ToInt32(IsStatus);

            if (fromdate != "" && todate != "")
            {

                var fdate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var tdate = DateTime.ParseExact(todate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var list = srmgrnservice.get(fdate, tdate, IsStatus, Company_ID, BranchId, Userid, roleid);
                ViewBag.fromdate = fromdate;
                ViewBag.todate = todate;
                return PartialView("_IndexList", list);
            }
            else
            {
                var list = srmgrnservice.GetStatusData(IsStatus, Company_ID, BranchId, Userid, roleid);
                return PartialView("_IndexList", list);
            }

        }

        [RBAC]
        public JsonResult GetGRNData(long Id)
        {
            var record = srmgrnservice.getbyid(Id);
            return Json(record);
        }

        [RBAC]
        public JsonResult GetRackData(long Id)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            var list = rackservice.get().Where(x => x.Company_ID == ViewBag.companyid && x.Branch_ID == ViewBag.branchid && x.WarehouseId== Id);
            return Json(list);
        }


        [RBAC]
        public JsonResult SaveRackAllocation(string RackStr,long WarehouseId,long InwardId,int type)
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            int roleid = Convert.ToInt32(collection.RoleID);
            int Userid = Convert.ToInt32(collection.Id);
            int Company_ID = Convert.ToInt32(collection.Company_ID);
            int BranchId = Convert.ToInt32(collection.BranchId);

            long id = rackservice.SaveRackAllocation(RackStr,WarehouseId, InwardId, type, Userid, Company_ID, BranchId);
            return Json(id);
        }
    }
}
