﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Garments_ERP.Filters;
namespace Garments_ERP.Controllers
{
    public class UserController : SuperController
    {
        //
        // GET: /User/
        UserService userservice = ServiceFactory.GetInstance().UserService;
        [RBAC]
        public ActionResult Index()
        {
            var list = userservice.get();
            return View(list);
        }
        [RBACAttribute]
        public ActionResult Create()
        {
            ViewBag.usertypelist = DropDownList<M_UserTypeEntity>.LoadItems(userservice.getUsertype(), "Id", "UserType", 0);
            return View();
        
        }

        public JsonResult Checkusername(String Username, int? Id)
        {
            bool value = true;
            if (Id == null)
                value = userservice.get().Where(x => x.Username == Username).FirstOrDefault() == null;
            else
            {

                if (userservice.get().Where(x => x.Username == Username && x.Id == Id).FirstOrDefault() == null)
                {
                    if (userservice.get().Where(x => x.Username == Username).FirstOrDefault() == null)
                    {
                        value = true;
                    }
                    else
                    {
                        value = false;
                    }
                }
            }

            return Json(value, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(M_UserEntity entity)
        {
            entity.Password = Helper.Helper.Encrypt(entity.Password);

            //New Code Added by Rahul on 09-01-2020
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            entity.Createdby = Convert.ToInt32(collection.Id);
            entity.Company_ID = Convert.ToInt32(collection.Company_ID);
            entity.BranchId = Convert.ToInt32(collection.BranchId);

            var resultid = userservice.Insert(entity);
            if(resultid>0)
            {
                TempData["alertmsg"] = "Record Save Successfully!";
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult AssignPermissions(int id)
        {
           
            M_UserPermissionsEntity permissions = new M_UserPermissionsEntity();
            bool IsCompanyUser = false;

            try
            {
               // if (Helper.Helper.CurrentLoggedUser.UserTypeId == 3)
               // {
               //     IsCompanyUser = true;
               //     permissions = userservice.getbyUserid(Helper.Helper.CurrentLoggedUser.Id).Where(x => x.ModuleId == 11).FirstOrDefault(); //for permissions Master
              //  }

                List<M_UserPermissionsEntity> list = userservice.getbyUserid(id);
                ViewBag.UserId = id;
               // ViewBag.username = userservice.GetById(id).UserName;
                return View(list);
            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = "Somthing Went's Wrong Please Try Again";
                TempData["MessageClass"] = "alert-danger";
                ExceptionLogging.SendErrorToText(ex);

                return RedirectToAction("Index");
            }
        }


        [HttpPost]
        public ActionResult AssignPermissions(FormCollection frm, int UserId)
        {
            try
            {
                var a = frm["IsRead_1"];
                var modules = userservice.GetModules();
                List<M_UserPermissionsEntity> UserList = new List<M_UserPermissionsEntity>();
                foreach (var item in modules)
                {
                    M_UserPermissionsEntity model = new M_UserPermissionsEntity();
                    model.UserId = UserId;
                    model.ModuleId = item.ModuleId;
                 //   model.CompanyId = Helper.Helper.CurrentLoggedUser.CompanyID;
                    bool IsRead = false, IsCreate = false, IsEdit = false, IsDelete = false;
                    if (frm["IsRead_" + item.ModuleId] != null)
                    {
                        IsRead = true;
                    }
                    if (frm["IsCreate_" + item.ModuleId] != null)
                    {
                        IsCreate = true;
                    }
                    if (frm["IsEdit_" + item.ModuleId] != null)
                    {
                        IsEdit = true;
                    }
                    if (frm["IsDelete_" + item.ModuleId] != null)
                    {
                        IsDelete = true;
                    }
                    model.IsRead = IsRead;
                    model.IsCreate = IsCreate;
                    model.IsDelete = IsDelete;
                    model.IsEdit = IsEdit;

                    UserList.Add(model);
                }
                int i = userservice.Update(UserList);
                if (i > 0)
                {
                    TempData["alertmsg"] = "Record Save Successfully";
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["alertmsg"] = "Something Went Worng... Please Try Again..!!!";
                    TempData["alertclass"] = "alert alert-danger alert-dismissible";
                    return RedirectToAction("Index");
                }
                List<M_UserPermissionsEntity> list = userservice.getbyUserid(UserId);
                ViewBag.UserId = UserId;
                return View(list);
            }
            catch (Exception ex)
            {
                TempData["alertmsg"] = "Somthing Went's Wrong Please Try Again";
                TempData["alertclass"] = "alert alert-danger alert-dismissible";
                ExceptionLogging.SendErrorToText(ex);
                List<M_UserPermissionsEntity> list = userservice.getbyUserid(UserId);
                ViewBag.UserId = UserId;
                return View(list);

            }
        }
          [RBAC]
        public ActionResult Edit(int id)
        {
            var record = userservice.GetByid(id);
            record.Password = Helper.Helper.Decrypt(record.Password);
            record.ConfirmPassword = record.Password;
            ViewBag.usertypelist = DropDownList<M_UserTypeEntity>.LoadItems(userservice.getUsertype(), "Id", "UserType",0);

            return View(record);
        }

        public JsonResult Delete(int id)
        {
            var result = userservice.DeleteUser(id);
            return Json(result);

        }
        [HttpPost]
        public ActionResult Edit(M_UserEntity entity)
        {
            entity.Password = Helper.Helper.Encrypt(entity.Password);
            //New Code Added by Rahul on 09-01-2020
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;
            entity.Updatedby = Convert.ToInt32(collection.Id);
            entity.Company_ID = Convert.ToInt32(collection.Company_ID);
            entity.BranchId = Convert.ToInt32(collection.BranchId);

            var updated = userservice.UpdateUser(entity);
            if(updated==true)
            {
                TempData["alertmsg"] = "Record Update Successfully!";
                return RedirectToAction("Index");
            }
            return View();
        }
    }
}
