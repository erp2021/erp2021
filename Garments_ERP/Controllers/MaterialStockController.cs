using Garments_ERP.Entity;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using Newtonsoft.Json;
using Garments_ERP.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Garments_ERP.Controllers
{
    public class MaterialStockController : SuperController
    {
        //
        // GET: /MaterialStock/
        ItemService itemservice = ServiceFactory.GetInstance().ItemService;
        SRM_GRNInwardService srmgrnservice = ServiceFactory.GetInstance().SRMGRNInwardService;
        ItemGroupService itemgroupservice = ServiceFactory.GetInstance().ItemGroupService;


        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Material Stock";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            ViewBag.ControlList = DropDownList<M_Dynamic_ControlsEntity>.LoadItems(srmgrnservice.GetControlList(), "Dynamic_Controls_Id", "Control_Name", 0);
            ViewBag.MaterialType = DropDownList<M_ItemGroupEntity>.LoadItems(itemgroupservice.Get(), "Id", "Itemgroup", 1);
            return View();
        }

        [RBAC]
        public PartialViewResult GetInventory(int GroupId, int VendorId, int StyleID, int BatchId, int LotNo, string FromDate, string ToDate, int companyid, int branchid,long ItemId,int ItemAttr, long WarehouseId)
        {
            string MyJson = "[{\"GroupId\":\"" + GroupId + "\",\"VendorId\":\"" + VendorId + "\",\"StyleID\":\"" + StyleID + "\",\"BatchId\":\"" + BatchId + "\",\"LotNo\":\"" + LotNo + "\",\"FromDate\":\"" + FromDate + "\",\"ToDate\":\"" + ToDate + "\",\"companyid\":\"" + companyid + "\",\"branchid\":\"" + branchid + "\",\"ItemId\":\"" + ItemId + "\",\"ItemAttr\":\"" + ItemAttr + "\",\"WarehouseId\":\"" + WarehouseId + "\"}]";
            XmlDocument doc = new XmlDocument();
            doc = JsonConvert.DeserializeXmlNode("{\"Row\":" + MyJson + "}", "root"); // JSON needs to be an objec
            var entity = itemservice.GetInventory(string.Empty, 1, doc);
            entity = ItemId > 0 ? entity.Where(x => x.Id == ItemId && x.Item_Attribute_ID == ItemAttr).ToList() : entity;
            return PartialView("MaterialList", entity);
        }

        [RBAC]
        public JsonResult GetRackData(long ItemId,int ItemAttr)
        {
            var entity = itemservice.GetRackData(ItemId,  ItemAttr);
            return Json(entity);
        }

        [RBAC]
        public ActionResult InventoryReport()
        {
            return View();
        }
    }
}
