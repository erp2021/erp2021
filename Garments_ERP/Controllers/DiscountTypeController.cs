﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using Garments_ERP.Filters;

namespace Garments_ERP.Controllers
{
    public class DiscountTypeController : SuperController
    {
        //
        // GET: /DiscountType/
        DiscountTypeService discounttypeservice = ServiceFactory.GetInstance().DiscountTypeService;

        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Discount Type";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var list = discounttypeservice.get();
            return View(list);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Discount Type";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(M_DiscountTypeMasterEntity entity)
        {
            
                try
                {
                    if (ModelState.IsValid)
                    {
                        entity.IsActive = true;
                        //New Code Added by Rahul on 09-01-2020
                        var sessiondat = Session["LoggedUser"];
                        M_UserEntity collection = (M_UserEntity)sessiondat;
                        entity.CreatedBy = Convert.ToInt32(collection.Id);
                        entity.CreatedDate = DateTime.Now;
                        entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        entity.Branch_ID = Convert.ToInt32(collection.BranchId);

                        long id = discounttypeservice.Insert(entity);
                        if (id > 0)
                        {

                            TempData["alertmsg"] = "Record Save Successfully.";
                            TempData["alertclass"] = "alert alert-success alert-dismissible";
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    TempData["alertmsg"] = ex.Message;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            
            return RedirectToAction("Index");
        }
         
        [RBAC]
        public ActionResult Edit(int id)
        {
            ViewBag.title = "Discount Type";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            M_DiscountTypeMasterEntity entity = discounttypeservice.get().Where(x => x.Id == id).FirstOrDefault();
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(M_DiscountTypeMasterEntity entity)
        {
            
                try
                {
                    if (ModelState.IsValid)
                    {
                        var sessiondat = Session["LoggedUser"];
                        M_UserEntity collection = (M_UserEntity)sessiondat;
                        entity.UpdatedBy = Convert.ToInt32(collection.Id);
                        entity.UpdatedDate = DateTime.Now;
                        entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        entity.Branch_ID = Convert.ToInt32(collection.BranchId);

                        bool isupdate = discounttypeservice.Update(entity);
                        if (isupdate == true)
                        {
                            M_StyleMasterEntity entity_ = new M_StyleMasterEntity();
                            var list = discounttypeservice.get();
                            TempData["alertmsg"] = "Record Updated Successfully.";
                            TempData["alertclass"] = "alert alert-success alert-dismissible";
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    TempData["alertmsg"] = ex.Message;
                    TempData["alertclass"] = "alert alert-success alert-dismissible";
                    return RedirectToAction("Index");
                }
            
            return RedirectToAction("Index");
        }
        
        [RBAC]
        public ActionResult Delete(int id)
        {
            bool delete = discounttypeservice.Delete(id);
            if (delete == true)
            {
                return Json(delete);
            }
            return Json(delete);
        }

        public JsonResult CheckDiscountType(string DiscountTypeName, int? Id)
        {
            bool value = discounttypeservice.CheckDiscountTypeNameExistance(DiscountTypeName, Id);
            return Json(value, JsonRequestBehavior.AllowGet);
        }

    }
}
