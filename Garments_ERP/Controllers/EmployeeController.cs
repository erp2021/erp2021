﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class EmployeeController : SuperController
    {
        //
        // GET: /Employee/
        CountryService countryservice = ServiceFactory.GetInstance().CountryService;
        EmployeeService empservice = ServiceFactory.GetInstance().EmployeeService;
        WorkorderService woservice = ServiceFactory.GetInstance().WorkorderService;
        StateService stateservice = ServiceFactory.GetInstance().StateService;
        CityService cityservice = ServiceFactory.GetInstance().CityService;
        RoleService roleservice= ServiceFactory.GetInstance().RoleService;
        DepartmentService deptService= ServiceFactory.GetInstance().DepartmentService;
        UserService userservice = ServiceFactory.GetInstance().UserService;

        [RBAC]
        public ActionResult Dashboard()
        {
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            int inId = 2;
            var list = woservice.GetWOProcessDetailsForIncharge(inId);
            return View(list);
        }
        
        [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Employee";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);
            int comid = Convert.ToInt32(collection.Company_ID);
            int branchid = Convert.ToInt32(collection.BranchId);
            var list = empservice.get(comid, branchid);
            return View(list);
        }

        [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Employee";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name", 101);
            ViewBag.EmployeeType= DropDownList<M_Employee_TypeEntity>.LoadItems(empservice.getEmployeeType(), "EmpTypeId", "ShortName", 0);
            ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", 1646);
            ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", 0);
            
            var comid = Convert.ToInt32(collection.Company_ID);
            if (comid > 0)
            {
                ViewBag.Role = DropDownList<M_RoleEntity>.LoadItems(roleservice.get().Where(x => x.roleId != 1 && x.roleId != 2).ToList(), "roleId", "roleName", 0);
            }
            else
            {
                ViewBag.Role = DropDownList<M_RoleEntity>.LoadItems(roleservice.get(), "roleId", "roleName", 0);
            }
            ViewBag.DepartMent = DropDownList<M_DepartmentMasterEntity>.LoadItems(deptService.get(), "Id", "Departmentname", 0);
            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(FormCollection frm)
        {
            try
            {
                M_UserEntity Userent = new M_UserEntity();
                Userent.Username = Convert.ToString(frm["EmpID"]);
                Userent.Password = Helper.Helper.Encrypt("23456");
                Userent.Createddate = DateTime.Now;
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;
                Userent.Createdby = Convert.ToInt32(collection.Id);
                Userent.IsActive = true;
                Userent.Email = Convert.ToString(frm["Email"]);
                Userent.Company_ID = Convert.ToInt32(collection.Company_ID);
                Userent.BranchId = Convert.ToInt32(collection.BranchId);
                Userent.APassword = "23456";
                Userent.DeptID = Convert.ToInt32(frm["EmployeeDept"]);
                Userent.RoleID = Convert.ToInt32(frm["EmployeeRole"]);
                int Userid = userservice.SaveUser(Userent);
                if (Userid > 0)
                {
                    EmployeeEntity emp = new EmployeeEntity();
                    emp.Name = Convert.ToString(frm["Name"]);
                    emp.Gender = Convert.ToString(frm["Gender"]);
                    emp.EmpID = Convert.ToString(frm["EmpID"]);
                    var dob = Convert.ToString(frm["dob"]);
                    emp.DOB = dob;
                    DateTime _dob = DateTime.ParseExact(Convert.ToString(dob), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    DateTime curdate = DateTime.Now;
                    TimeSpan diff = curdate.Subtract(_dob);
                    DateTime age = DateTime.MinValue + diff;
                    emp.Age = age.Year - 1;
                    emp.CREATED_BY= Convert.ToDecimal(collection.Id);
                    emp.CREATED_ON = DateTime.Now;
                    emp.IsActive = true;
                    emp.PlaceOfBirth = Convert.ToString(frm["placeofbirth"]);
                    emp.BloodGroup = Convert.ToString(frm["booldgroup"]);
                    emp.MaritalStatus = Convert.ToString(frm["maritalstatus"]);
                    emp.Nationality = Convert.ToString(frm["Nationality"]);
                    emp.Religion = Convert.ToString(frm["Religion"]);
                    emp.MotherTongue = Convert.ToString(frm["MotherTongue"]);
                    emp.Caste = Convert.ToString(frm["Cast"]);
                    emp.UserId = Userid;
                    emp.EmpTypeId = Convert.ToInt64(frm["EmployeeType"]);
                    int empid=empservice.Insert(emp);

                    if (empid > 0)
                    {
                        EmployeeAddressDetailEntity add = new EmployeeAddressDetailEntity();
                        add.UserID = Userid;
                        add.Address1 = Convert.ToString(frm["address1"]);
                        add.Address2 = Convert.ToString(frm["Address2"]);
                        add.City = Convert.ToInt32(frm["addresscityid"]);
                        add.State = Convert.ToInt32(frm["addressstateid"]);
                        add.Country = Convert.ToInt32(frm["countryid"]);
                        add.Pincode = Convert.ToString(frm["pincode"]);
                        add.MobileNo = Convert.ToString(frm["MobileNo"]);
                        add.PhoneNo = Convert.ToString(frm["PhoneNo"]);
                        add.Email = Convert.ToString(frm["Email"]);
                        add.CreatedBy = Convert.ToInt32(collection.Id);
                        add.CreatedOn = DateTime.Now;
                        add.IsActive = true;
                        long addinsertid = empservice.InsertAddress(add);
                        if (addinsertid > 0)
                        {
                            TempData["alertmsg"] = "Record Save Successfully.";
                            TempData["alertclass"] = "alert alert-success alert-dismissible";
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            TempData["alertmsg"] = "Record Not Save!";
                            TempData["alertclass"] = "alert alert-success alert-dismissible";
                            return RedirectToAction("Index");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }
            
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Edit(int id)
        {
            ViewBag.title = "Employee";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var list = empservice.getIdByUserData(id);
            ViewBag.countrylist = DropDownList<M_CountryMasterEntity>.LoadItems(countryservice.get(), "ID", "Name", Convert.ToInt32(list.empaddEnity.Country));
            ViewBag.EmployeeType = DropDownList<M_Employee_TypeEntity>.LoadItems(empservice.getEmployeeType(), "EmpTypeId", "ShortName", Convert.ToInt64(list.EmpTypeId));
            ViewBag.statelist = DropDownList<M_StateMasterEntity>.LoadItems(stateservice.get(), "ID", "Name", Convert.ToInt32(list.empaddEnity.State));
            ViewBag.citylist = DropDownList<M_CityMasterEntity>.LoadItems(cityservice.getByid(1646), "ID", "Name", Convert.ToInt32(list.empaddEnity.City));

            var comid = Convert.ToInt32(collection.Company_ID);
            if (comid > 0)
            {
                ViewBag.Role = DropDownList<M_RoleEntity>.LoadItems(roleservice.get().Where(x => x.roleId != 1 && x.roleId != 2).ToList(), "roleId", "roleName", Convert.ToInt32(list.roleEntity.roleId));
            }
            else
            {
                ViewBag.Role = DropDownList<M_RoleEntity>.LoadItems(roleservice.get(), "roleId", "roleName", Convert.ToInt32(list.roleEntity.roleId));
            }
            ViewBag.DepartMent = DropDownList<M_DepartmentMasterEntity>.LoadItems(deptService.get(), "Id", "Departmentname", Convert.ToInt32(list.DeptEntity.Id));
            return View(list);
        }


        [RBAC]
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            try
            {
                var sessiondat = Session["LoggedUser"];
                M_UserEntity collection = (M_UserEntity)sessiondat;

                EmployeeEntity emp = new EmployeeEntity();
                int Userid = Convert.ToInt32(frm["UserID"]);
                emp.Name = Convert.ToString(frm["Name"]);
                emp.Gender = Convert.ToString(frm["Gender"]);
                emp.EmpID = Convert.ToString(frm["EmpID"]);
                var dob = Convert.ToString(frm["dob"]);
                emp.DOB = dob;
                DateTime _dob = DateTime.ParseExact(Convert.ToString(dob), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                DateTime curdate = DateTime.Now;
                TimeSpan diff = curdate.Subtract(_dob);
                DateTime age = DateTime.MinValue + diff;
                emp.Age = age.Year - 1;
                emp.UPDATED_BY = Convert.ToDecimal(collection.Id);
                emp.UPDATED_ON = DateTime.Now;
                emp.IsActive = true;
                emp.PlaceOfBirth = Convert.ToString(frm["placeofbirth"]);
                emp.BloodGroup = Convert.ToString(frm["booldgroup"]);
                emp.MaritalStatus = Convert.ToString(frm["maritalstatus"]);
                emp.Nationality = Convert.ToString(frm["Nationality"]);
                emp.Religion = Convert.ToString(frm["Religion"]);
                emp.MotherTongue = Convert.ToString(frm["MotherTongue"]);
                emp.Caste = Convert.ToString(frm["Cast"]);
                emp.UserId = Userid;
                emp.EmpTypeId = Convert.ToInt64(frm["EmployeeType"]);
                int DeptID = Convert.ToInt32(frm["EmployeeDept"]);
                int RoleID = Convert.ToInt32(frm["EmployeeRole"]);

                bool empid = empservice.Update(emp, DeptID, RoleID);

                if (empid== true)
                {
                    EmployeeAddressDetailEntity add = new EmployeeAddressDetailEntity();
                    add.UserID = Userid;
                    add.Address1 = Convert.ToString(frm["address1"]);
                    add.Address2 = Convert.ToString(frm["Address2"]);
                    add.City = Convert.ToInt32(frm["addresscityid"]);
                    add.State = Convert.ToInt32(frm["addressstateid"]);
                    add.Country = Convert.ToInt32(frm["countryid"]);
                    add.Pincode = Convert.ToString(frm["pincode"]);
                    add.MobileNo = Convert.ToString(frm["MobileNo"]);
                    add.PhoneNo = Convert.ToString(frm["PhoneNo"]);
                    add.Email = Convert.ToString(frm["Email"]);
                    add.UpdatedBy = Convert.ToInt32(collection.Id);
                    add.UpdatedOn = DateTime.Now;
                    add.IsActive = true;
                    bool addinsertid = empservice.UpdateAddress(add);
                    if (addinsertid== true)
                    {
                        TempData["alertmsg"] = "Record Update Successfully.";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["alertmsg"] = "Record Not Update!";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ExceptionLogging.SendErrorToText(ex);
                TempData["alertmsg"] = ex.Message;
                TempData["alertclass"] = "alert alert-success alert-dismissible";
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }


        public JsonResult DeleteEmployee(int Id)
        {
            bool ISDeleted = false;
            ISDeleted = empservice.Delete(Id);
            return Json(ISDeleted);
        }
    }
}
