﻿using Garments_ERP.Entity;
using Garments_ERP.Filters;
using Garments_ERP.Helper;
using Garments_ERP.Service.Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Controllers
{
    public class ReportsController : Controller
    {
        //
        // GET: /Reports/
        ReportsService reportsservice = ServiceFactory.GetInstance().ReportsService;
        ItemCategoryService itemcategoryservice = ServiceFactory.GetInstance().ItemCategoryService;
        public ActionResult Index()
        { 
            return View();
        }
        [RBAC]
        public ActionResult WarehouseStockReport()
        {
            ViewBag.itemcategorylist = DropDownList<M_ItemCategoryMasterEntity>.LoadItems(itemcategoryservice.get(), "Id", "Itemcategory", 0);
            var list = reportsservice.GetWarehouseStock();
            return View(list);
        }
        [RBAC]
        [HttpPost]
        public ActionResult WarehouseStockReport(long Itemcategory)
        {
            var list = reportsservice.GetDateWiseWarehouseStock(Itemcategory);
            ViewBag.itemcategorylist = DropDownList<M_ItemCategoryMasterEntity>.LoadItems(itemcategoryservice.get(), "Id", "Itemcategory", 0);
            return View(list);
        }
        [RBAC]
        public ActionResult ItemInwardReport(long itemname)
        {
            var list = reportsservice.GetItemInwardDetail(itemname);
            return View(list);
        }
        [RBAC]
        [HttpPost]
        public ActionResult ItemInwardReport(string fromdate, string todate)
        {
            var fdate = DateTime.ParseExact(fromdate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var tdate = DateTime.ParseExact(todate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var list = reportsservice.GetDateWiseInwardDetail(fdate, tdate);
            return View(list);
        }
        [RBAC]
        public ActionResult InventoryReport()
        {
            var list = reportsservice.GetInventoryDetails();
            return View(list);
        }
        [RBAC]
        [HttpPost]
        public ActionResult InventoryReport(string fromdate, string todate)
        {
            return View();
        }

    }
}
