﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Garments_ERP.Service.Service;
using Garments_ERP.Entity;
using Garments_ERP.Helper;
using Garments_ERP.Filters;


namespace Garments_ERP.Controllers
{
    public class ProcessingStatusController : SuperController
    {
        //
        // GET: /ProcessingStatus/
        ProcessingStatusService processingstatusservice = ServiceFactory.GetInstance().ProcessingStatusService;
         [RBAC]
        public ActionResult Index()
        {
            ViewBag.title = "Processing Status";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            var list = processingstatusservice.get();
            return View(list);
        }
         [RBAC]
        public ActionResult Create()
        {
            ViewBag.title = "Processing Status";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            return View();
        }

        [RBAC]
        [HttpPost]
        public ActionResult Create(M_ProcessingStatusEntity entity)
        {
            
                try
                {
                    if (ModelState.IsValid)
                    {

                        entity.IsActive = true;
                        //New Code Added by Rahul on 09-01-2020
                        var sessiondat = Session["LoggedUser"];
                        M_UserEntity collection = (M_UserEntity)sessiondat;
                        entity.Createdby = Convert.ToInt32(collection.Id);
                        entity.Createddate = DateTime.Now;
                        entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                        entity.Branch_ID = Convert.ToInt32(collection.BranchId);

                        long id = processingstatusservice.Insert(entity);
                        if (id > 0)
                        {

                            TempData["alertmsg"] = "Record Save Successfully.";
                            TempData["alertclass"] = "alert alert-success alert-dismissible";
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (Exception ex)
                {
                    TempData["alertmsg"] = Convert.ToString(ex.ToString());
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    return RedirectToAction("Index");
                }
            
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Edit(int id)
        {
            ViewBag.title = "Processing Status";
            var sessiondat = Session["LoggedUser"];
            M_UserEntity collection = (M_UserEntity)sessiondat;

            ViewBag.roleid = Convert.ToInt32(collection.RoleID);
            ViewBag.deptid = Convert.ToInt32(collection.DeptID);
            ViewBag.userid = Convert.ToInt32(collection.Id);
            ViewBag.companyid = Convert.ToInt32(collection.Company_ID);
            ViewBag.branchid = Convert.ToInt32(collection.BranchId);
            ViewBag.EmpName = Convert.ToString(collection.empentity.Name);
            ViewBag.Username = Convert.ToString(collection.Username);

            M_ProcessingStatusEntity entity = processingstatusservice.get().Where(x => x.Id == id).FirstOrDefault();
            return View(entity);
        }

        [RBAC]
        [HttpPost]
        public ActionResult Edit(M_ProcessingStatusEntity entity)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //New Code Added by Rahul on 09-01-2020
                    var sessiondat = Session["LoggedUser"];
                    M_UserEntity collection = (M_UserEntity)sessiondat;
                    entity.UpdatedBy = Convert.ToInt32(collection.Id);
                    entity.Updateddate = DateTime.Now;
                    entity.Company_ID = Convert.ToInt32(collection.Company_ID);
                    entity.Branch_ID = Convert.ToInt32(collection.BranchId);
                    bool isupdate = processingstatusservice.Update(entity);
                    if (isupdate == true)
                    {
                        M_ItemColorEntity entity_ = new M_ItemColorEntity();
                        var list = processingstatusservice.get();
                        TempData["alertmsg"] = "Record Updated Successfully.";
                        TempData["alertclass"] = "alert alert-success alert-dismissible";
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception ex)
                {
                    TempData["alertmsg"] = Convert.ToString(ex.ToString());
                    Helper.ExceptionLogging.SendErrorToText(ex);
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");
        }

        [RBAC]
        public ActionResult Delete(int id)
        {
            bool delete = processingstatusservice.Delete(id);
            if (delete == true)
            {
                return Json(delete);
            }
            return Json(delete);
        }
         public JsonResult CheckProcessingStatusExistance(string ProcessingStatusName, int? Id)
        {
            bool value = processingstatusservice.CheckProcessingStatusExistance(ProcessingStatusName, Id);
            return Json(value, JsonRequestBehavior.AllowGet);
        }

    }
}
