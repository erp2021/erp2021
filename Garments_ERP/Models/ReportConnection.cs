﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Garments_ERP.Models
{
    public class ReportConnection
    {
        public CrystalDecisions.Shared.ConnectionInfo connectionInfo()
        {
            CrystalDecisions.Shared.ConnectionInfo connectionInfo = new CrystalDecisions.Shared.ConnectionInfo();
            connectionInfo.ServerName = ConfigurationManager.AppSettings["ServerName"];
            connectionInfo.DatabaseName = ConfigurationManager.AppSettings["DatabaseName"];
            connectionInfo.UserID = ConfigurationManager.AppSettings["UserID"];
            connectionInfo.Password = ConfigurationManager.AppSettings["Password"];

            return connectionInfo;
        } 
    }
}