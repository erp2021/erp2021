﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Garments_ERP.Helper
{
    public static class DropDownList<T>
    {
        public static SelectList LoadItems(IList<T> collection, string value, string text, short selected)
        {
            return new SelectList(collection, value, text, selected);
        }
        public static SelectList LoadItems(IList<T> collection, string value, string text, int selected)
        {
            return new SelectList(collection, value, text, selected);
        }
        public static SelectList LoadItems(IList<T> collection, string value, string text, long selected)
        {
            return new SelectList(collection, value, text, selected);
        }
        public static SelectList LoadItems(IList<T> collection, string value, string text, string selected)
        {
            return new SelectList(collection, value, text, selected);
        }


    }
}